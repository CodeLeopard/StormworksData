﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/


using System;
using System.Text;
using System.Text.RegularExpressions;

using Extrusion;

using LuaIntegration.Properties;

using NLua;
using NLua.Exceptions;

using Shared;
using Shared.Exceptions;
using Shared.Validation;

namespace LuaIntegration
{
	public class CurveSpecLoader
	{
		private static Regex BOMBullshitDetector = new Regex("unexpected symbol near '<\\\\239>'");


		public static CurveSpec Load(string relativePath)
		{
			if(relativePath == null) throw new ArgumentNullException(nameof(relativePath));

			// Escape backslash.
			relativePath = relativePath.Replace("\\\\", "/");
			relativePath = relativePath.Replace("\\", "/");

			if (! relativePath.EndsWith(".lua"))
			{
				relativePath += ".lua";
			}

			try
			{
				try
				{
					using (var lua = GetLuaState())
					{
						// sandbox of path is done in Lua because we need require to work there anyway.
						var results = lua.DoString($"return dofile('{relativePath}')", "LoadCurveSpec_Expr");
						if (results == null)
						{
							throw new LuaException("The Lua script did not 'return' any value.");
						}

						if (results.Length != 1)
						{
							throw new LuaException
								($"The Lua script returned {results.Length} values but only 1 value was expected.");
						}

						if (results[0] == null)
						{
							throw new LuaException
								(
								 $"The Lua script returned nil, it should return a '{nameof(CurveSpec)}'"
								);
						}

						if (! (results[0] is CurveSpec spec))
						{
							throw new LuaException
								(
								 $"The Lua script returned a value of invalid type: '{results[0].GetType().Name}' but expected '{nameof(CurveSpec)}'"
								);
						}

						var v = new Validator();
						using (_ = v.PushContext($"The {nameof(CurveSpec)} returned by '{relativePath}'"))
						{
							spec.Validate(v);
						}

						v.ThrowErrors();

						if (string.IsNullOrEmpty(spec.name))
						{
							spec.name = relativePath;
						}

						return spec;
					}
				}
				catch (LuaScriptException e)
				{
					if (BOMBullshitDetector.IsMatch(e.Message))
					{
						throw new ByteOrderMarkException(message:
							  $"Detected file encoding error: 'UTF-8-BOM' (UTF-8 with Byte Order Mark) is not supported.\n"
							+ $"      To fix: Inspect the error message below to find the affected file and re-save it with UTF-8 encoding (UTF-8 without Byte Order Mark).\n"
							+ $"      In VSCode: Open the affected file, then: [F1] 'Change File Encoding' [Enter] 'Save With Encoding' [Enter] 'UTF-8' [Enter].\n"
							+ $"{e.Source}{e.Message}", e.InnerException);
					}
					if (null != e.InnerException)
					{
						// Needed because it's ToString() does not contain the InnerException.
						throw new LuaException(e.Source + e.Message, e.InnerException);
					}
					throw;
				}
			}
			catch (Exception e)
			{
				throw new FileInteractionException(relativePath, e);
			}
		}

		/// <summary>
		/// This should match the version on the sandbox file.
		/// Visual Studio doesn't seem to update it in all cases when building.
		/// </summary>
		public const string SandBoxVersion = "0.0.16";

		public static Lua GetLuaState()
		{
			var lua = new Lua();
			var state = lua.State;
			lua.UseTraceback = true; // Requires debug library
			lua.LoadCLRPackage();
			state.Encoding = Encoding.UTF8;

			string init = $@"
Rom = '{StormworksPaths.rom.Replace('\\', '/')}'
LuaRoot = '{StormworksPaths.CreatorToolKitData.curveSpec.Replace('\\', '/')}'
";
			lua.DoString(init, "init");

			var initScript = Encoding.UTF8.GetString(Resources.sandbox);
			lua.DoString(initScript, "sandbox.lua");

			string version = (string)lua.DoString("return SandBoxVersion", "VersionCheck_Expr")[0];

			return lua;
		}
	}
}
