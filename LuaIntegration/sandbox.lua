-- Copyright 2022 CodeLeopard
-- License: LGPL-3.0-or-later

--[[ This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
]]


-- This should increment for every change 
-- And the corresponding value n CurveSpecLoader should be updated to match.
-- This is needed because the build process does not seem to
-- load this file into the .dll as often as it is updated.
SandBoxVersion = "0.0.16"

local LuaRoot = LuaRoot or error("LuaRoot not defined")
local Rom = Rom or error("Rom not defined")


local function deepCopy(data, seen, target)
	if(type(data) ~= "table") then
		return data
	end
	seen = seen or {}
	target = target or {}
	if seen[data] then
		return seen[data]
	end

	seen[data] = target
	for k, v in pairs(data) do
		target[deepCopy(k, seen)] = deepCopy(v, seen)
	end
	return target
end


-- Import the Extrusion Assembly
SpecLib = CLRPackage('Extrusion')
SpecLib.Extrusion  = CLRPackage('Extrusion', 'Extrusion.Extrusion')
SpecLib.Periodic   = CLRPackage('Extrusion', 'Extrusion.Periodic')
SpecLib.Prefab     = CLRPackage('Extrusion', 'Extrusion.Prefab')
SpecLib.Support    = CLRPackage('Extrusion', 'Extrusion.Support')
SpecLib.Train      = CLRPackage('Extrusion', 'Extrusion.Train')
SpecLib.BinaryData = CLRPackage('BinaryDataModel', 'BinaryDataModel.DataTypes')
SpecLib.MathData   = CLRPackage('OpenToolkit.Mathematics')

-- Put things globally accessible
CurveSpec = SpecLib.CurveSpec

Extrusion = SpecLib.Extrusion.ExtrusionSpec
LoopSet   = SpecLib.Extrusion.LoopSet
Loop      = SpecLib.Extrusion.Loop

PeriodicItemSpec = SpecLib.Periodic.PeriodicItemSpec
PeriodicItem     = SpecLib.Periodic.PeriodicItem
PeriodicGroup    = SpecLib.Periodic.PeriodicGroup
MeshSpec         = SpecLib.Periodic.MeshSpecification

InterpolationSettings = SpecLib.Support.InterpolationSettings
MeshSharing           = SpecLib.Support.MeshSharing
PhysSharing           = SpecLib.Support.PhysSharing
Transformation        = SpecLib.Support.Transformation

TrainPhysics = SpecLib.Train.TrainPhysicsSpec

VertexRecord = SpecLib.BinaryData.VertexRecord

Vector2      = SpecLib.MathData.Vector2
Vector3      = SpecLib.MathData.Vector3
Vector4      = SpecLib.MathData.Vector4

Quaternion   = SpecLib.MathData.Quaternion

Matrix3      = SpecLib.MathData.Matrix3
Matrix4      = SpecLib.MathData.Matrix4

Color        = SpecLib.MathData.Color4
function Color255(R, G, B, A)
	return Color(R / 255, G / 255, B / 255, (A or 255) / 255)
end

ShaderID =
{
	Opaque      = 0,
	Transparent = 1,
	Emissive    = 2,
}


-- No additional modules need to be loaded, and it could provide IO etc through C# libraries.
import     = nil
CLRPackage = nil


---Check that the user provided path is safe and error otherwise. Return the input for convenience.
---@param userPath string
---@return string userPath
local function CheckUserProvidedPath(userPath)
	assert(type(userPath) == "string", "Path must be a string.")

	if string.find(userPath, "..", 1, true) then
		error("Malicious path: '..' is not allowed. '"..userPath.."'")
	end
	if (userPath:find('^[A-Za-z]:[/\\]') or userPath:find('^[/\\]')) then
		error("Malicious path: rooted paths are not allowed. '"..userPath.."'")
	end
	return userPath
end


local _load = load
function load(chunk, name, mode, env)
	if mode ~= "t" then
		error("Loading binary chunks is not allowed.")
	end

	return _load(chunk, name, mode, env)
end

local _io = io
local _loadfile = loadfile
function loadfile(filename, mode, env)
	if filename == nil then error("Cannot load from stdin") end
	CheckUserProvidedPath(filename)

	local fullPath = LuaRoot.."/"..filename

	local file, emsg = _io.open(fullPath, "r")

	if not file then error(emsg) end

	return load(file:read("*a"), filename, mode, env)
end


function dofile(filename)
	local func, err = loadfile(filename, "t", _G)
	if not func then
		error(err)
	end

	return func()
end

package =
{
	loaded  = package.loaded  or {},
	preload = package.preload or {},
}


function require(name)
	local result = package.loaded[name]
	if result then return result end

	result = package.preload[name]
	if result then
		result = result() or true

		package.loaded[name] = result
		return result
	end

	local path = name:gsub("%.", "/")..".lua"

	result = dofile(path) or true
	package.loaded[name] = result
	return result
end


-- hide various things that aren't needed to exist.
-- Can interact with the environment outside the sandbox.
getfenv = nil
setfenv = nil

-- Protect the metatable of string, because we depend on it.
local stringMeta = getmetatable("")
stringMeta.__metatable = false

-- We don't care about the global environment breaking.
--getmetatable = nil

-- There is no legitimate purpose to using the safe parts of os.
os = nil

-- todo: only allow opening, and only in safe places.
io = nil


local _debug = debug
debug =
{
	traceback = _debug.traceback,
	debug = _debug.debug,
	getinfo = debug.getinfo,
	gethook = _debug.gethook,
	sethook = _debug.sethook,
}


-- todo: provide access to Meshes etc.