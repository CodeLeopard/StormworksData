﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/





using System;
using System.Collections.Generic;
using System.IO;

using Extrusion;

using Shared;

namespace LuaIntegration
{
	public class CurveSpecCache
	{
		public static readonly CurveSpecCache Instance = new CurveSpecCache();

		private readonly LazyCache<string, Entry> cache;

		private static string root => Path.GetFullPath(StormworksPaths.CreatorToolKitData.curveSpec);

		/// <summary>
		/// Signature fo the handler that is called when loading causes an exception.
		/// </summary>
		/// <param name="key">The key/file path that was being loaded.</param>
		/// <param name="e">The exception that occurred.</param>
		public delegate void LoadExceptionDelegate(string key, Exception e);

		/// <summary>
		/// Event to raise when an Exception occurs.
		/// </summary>
		public event LoadExceptionDelegate OnException;

		public CurveSpecCache()
		{
			cache = new LazyCache<string, Entry>(CreateEntry);
		}

		private Entry CreateEntry(string key)
		{
			try
			{
				string file = key + ".lua";

				string fullPath = Path.GetFullPath(Path.Combine(root, file));

				if (! fullPath.StartsWith(root))
				{
					throw new FileNotFoundException("The specified path is above the root.", key);
				}

				var info = new FileInfo(fullPath);
				if (! info.Exists)
				{
					throw new FileNotFoundException($"File '{key}' not found.", fullPath);
				}

				var spec = CurveSpecLoader.Load(relativePath: key);

				return new Entry(spec, info.LastWriteTime);
			}
			catch (Exception e)
			{
				try
				{
					OnException?.Invoke(key, e);
				}
				catch(Exception e2)
				{
					Console.WriteLine(e2.ToString());
				}

				return new Entry(e);
			}
		}


		public CurveSpec GetOrLoad(string relPath)
		{
			return GetOrLoadEntry(relPath).Spec;
		}

		public Entry GetOrLoadEntry(string relPath)
		{
			return cache.GetOrAdd(relPath);
		}

		public bool TryGetEntry(string relPath, out Entry result)
		{
			return cache.TryGet(relPath, out result);
		}


		public bool Remove(string key)
		{
			return cache.TryRemove(key, out _);
		}

		public void Clear()
		{
			cache.Clear();
		}

		public void ClearOutdated()
		{
			var buffer = new List<string>();
			foreach (var kvp in cache)
			{
				string key = kvp.Key;
				string file = key + ".lua";
				var entry = kvp.Value;

				string fullPath = Path.GetFullPath(Path.Combine(root, file));

				var info = new FileInfo(fullPath);
				if (! info.Exists)
				{
					buffer.Add(key);
					continue;
				}

				// Updated since last read.
				if (info.LastWriteTime > entry.LastWriteTime)
				{
					buffer.Add(key);
					continue;
				}

				// Failed, could be caused by things not tracked for changes here.
				if (entry.Spec == null)
				{
					buffer.Add(key);
				}
			}

			foreach (string key in buffer)
			{
				cache.TryRemove(key, out _);
			}
		}


		public class Entry
		{
			/// <summary>
			/// The result or <see langword="null"/> if loading failed.
			/// </summary>
			public readonly CurveSpec Spec;

			/// <summary>
			/// The LastWriteTime of the file when it was last loaded.
			/// </summary>
			public readonly DateTime LastWriteTime;

			/// <summary>
			/// <see langword="null"/>, unless loading failed, in which case it holds the <see cref="Exception"/> that was thrown.
			/// </summary>
			public readonly Exception Exception;

			internal Entry(CurveSpec spec, DateTime lastWriteTime)
			{
				Spec = spec;
				LastWriteTime = lastWriteTime;
			}

			internal Entry(Exception e)
			{
				Exception = e;
			}
		}
	}
}
