﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/


using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;

using CurveGraph.Interface;
using CustomSerialization.Attributes;
using Extrusion;

using Shared;

namespace LuaIntegration
{
	// Originally lived in the Unity repo.
	[Serializable] // BinaryFormatter
	[DataContract] // DataContractSerializer
	[SerializerExplicit] // CustomSerializer
	public class ConnectionCustomData : IConnectionCustomData, IEquatable<ConnectionCustomData>
	{
		/// <summary>
		/// Id used for keeping serialization and export data consistent.
		/// </summary>
		[VisibleProperty(isReadonly: true)]
		[SerializerInclude(optional:true)]
		[DataMember(Name = "id")]
		public uint _id;

		/// <summary>
		/// <see cref="CurveSpecFilePath"/> but this property does not perform validation nor raise events.
		/// </summary>
		[SerializerInclude(optional:true, nameOverride: "CurveSpecFilePath")]
		[DataMember(Name = "CurveSpecFilePath")]
		public string _curveSpecFilePath;

		/// <summary>
		/// The path, relative to <see cref="StormworksPaths.CurveSpec"/> to load the <see cref="LuaIntegration.CurveSpec"/> from.
		/// </summary>
		[VisibleProperty]
		public string CurveSpecFilePath
		{
			get => _curveSpecFilePath;
			set => SetXmlPathPropertyCommon(ref _curveSpecFilePath, ref _curveSpec, value);
		}

		/// <summary>
		/// <see cref="CurveSpec"/> but this property does not perform validation nor raise events.
		/// </summary>
		[NonSerialized]
		public CurveSpec _curveSpec;

		/// <summary>
		/// The <see cref="LuaIntegration.CurveSpec"/> of the connection.
		/// </summary>
		public CurveSpec CurveSpec
		{
			get => GetCurveSpecPropertyCommon(ref _curveSpec, _curveSpecFilePath);
			set => SetCurveSpecPropertyCommon(ref _curveSpec, ref _curveSpecFilePath, value);
		}

		private void SetXmlPathPropertyCommon(ref string current, ref CurveSpec spec, string value)
		{
			if (value != current)
			{
				if (value == "")
				{
					// Allow the user to unset.
					current = value;
					spec = null; // Gets fetched when next used.

					RaiseChanged();
					return;
				}

				var fileName = value;
				if (! fileName.EndsWith(".lua")) fileName += ".lua";
				string fullPath = Path.Combine(StormworksPaths.CreatorToolKitData.curveSpec, fileName);
				if (! File.Exists(fullPath))
				{
					// This will trigger for every character typed in.
					//Console.WriteLine($"CurveSpec file does not exist: '{value}'.");
					return;
				}

				try
				{
					GetCurveSpecPropertyCommon(ref spec, in value);
				}
				catch (Exception e)
				{
					Console.WriteLine(e); // todo: better logging? No swallow?
					return;
				}

				current = value;
				spec = null; // Gets fetched when next used.

				RaiseChanged();
			}
		}

		private CurveSpec GetCurveSpecPropertyCommon(ref CurveSpec current, in string pathProperty)
		{
			if (null == current && !string.IsNullOrWhiteSpace(pathProperty))
			{
				current = CurveSpecCache.Instance.GetOrLoad(pathProperty);
			}
			return current;
		}

		private void SetCurveSpecPropertyCommon(ref CurveSpec current, ref string currentPathProperty, CurveSpec value)
		{
			current = value;

			RaiseChanged();
		}

		public ConnectionCustomData()
		{

		}

		public ConnectionCustomData Clone()
		{
			var clone = new ConnectionCustomData();
			clone._curveSpecFilePath = _curveSpecFilePath;
			return clone;
		}

		/// <inheritdoc />
		IConnectionCustomData ICloneForMTR<IConnectionCustomData>.CloneForMultiThreadedReading()
		{
			return Clone();
		}

		public void RaiseChanged()
		{
			Changed?.Invoke(this);
		}

		public override bool Equals(object obj)
		{
			return Equals(obj as ConnectionCustomData);
		}

		public bool Equals(ConnectionCustomData other)
		{
			return other != null && _curveSpecFilePath == other._curveSpecFilePath;
		}

		public override int GetHashCode()
		{
			unchecked
			{
				int hashCode = -2019201954;
				hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_curveSpecFilePath);
				return hashCode;
			}
		}

		/// <inheritdoc />
		public event Action<IConnectionCustomData> Changed;
	}
}
