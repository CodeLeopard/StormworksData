﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/


using System;
using System.Drawing;
using System.Linq;
using System.Text;

using Extrusion;

using NLua;
using NLua.Exceptions;

using Pastel;

namespace LuaIntegration
{
	public class Program
	{
		/// <summary>
		/// Starts an interactive Lua session in the sandbox created for loading <see cref="CurveSpec"/>.
		/// </summary>
		/// <param name="args"></param>
		public static void Main(string[] args)
		{
			using (var lua = CurveSpecLoader.GetLuaState())
			{
				while (true)
				{
					string line = Console.ReadLine();
					if (line == "exit") break;

					try
					{
						var result = lua.DoString(line, "stdin");

						if (null != result && result.Length > 0)
						{
							Console.WriteLine(string.Join(", ", result.Select(o => o?.ToString() ?? "nil")));
						}
					}
					catch (LuaScriptException e)
					{
						Console.Write(e.ToString().Pastel(Color.Red));
						if (null != e.InnerException)
						{
							Console.WriteLine(":".Pastel(Color.Red));
							Console.Write(e.InnerException.ToString().Pastel(Color.Red));
						}
						Console.WriteLine();
					}
					catch (Exception e)
					{
						Console.WriteLine(e.ToString().Pastel(Color.Red));
					}
				}
			}
		}
	}
}
