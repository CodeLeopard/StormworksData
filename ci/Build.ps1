# Copyright 2022 CodeLeopard
# License: LGPL-3.0-or-later

<# This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
#>

#! This script is used by parent git module to trigger build without needing to care about the details.


Param
(
	[Parameter(Mandatory, HelpMessage="The configuration used for the build. [Debug | Deploy | Release]")] [String] $configuration,
	[switch] $Unity_Relevant_Subset
)
$ErrorActionPreference = "Stop" # Yes we have to tell powershell to behave normally.

$result = $true

if ($Unity_Relevant_Subset) {
	$projects = @("Shared", "CustomSerialization", "BinaryDataModel", "DataModel", "CurveGraph", "Extrusion", "LuaIntegration")
	$failed = $false
	$projects | ForEach-Object {
		& dotnet build $_ --configuration $configuration --nologo -property:WarningLevel=0

		if ($LASTEXITCODE -ne 0) {
			$script:failed = $true
		}
	}

	$result = -not $failed
	
} else{
	& dotnet build --configuration $configuration --nologo -property:WarningLevel=0
	$result = $?
}

if($result -ne $True)
{
	throw "Build failed."
}