// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml.Linq;

using OpenToolkit.Mathematics;

using DataModel.DataTypes;
using DataModel.Definitions.Definitions;
using DataModel.Definitions.NestedTypes;

using Shared.Exceptions;
using Shared.Serialization;
using Shared.Serialization.DataTypes;

namespace DataModel.Definitions
{

	public class Definition
	{
		public string fileName;

		#region AttributeData

		/// <summary>
		/// Name that is shown in the User interface.
		/// </summary>
		public string name;

		/// <summary>
		/// Inventory category
		/// </summary>
		public DefinitionCategory category;

		public DefinitionType type;

		public float mass;
		/// <summary>
		/// Money value
		/// </summary>
		public int value;

		public DefinitionFlags flags;


		/// <summary>
		/// Static Mesh
		/// </summary>
		public string mesh_data_name;
		/// <summary>
		/// Moving Element 1 Mesh
		/// </summary>
		/// <remarks>
		/// Rotor/Propeller Hub
		/// </remarks>
		public string mesh_0_name;
		/// <summary>
		/// Moving Element 2 Mesh
		/// </summary>
		/// <remarks>
		/// Rotor/Propeller Blade
		/// </remarks>
		public string mesh_1_name;
		/// <summary>
		/// Editor Only Arrows
		/// </summary>
		public string mesh_editor_only_name;



		public float buoy_radius;
		public float buoy_factor;
		public float buoy_force;

		public string tags;

		public int reward_tier = 0;
		public int reward_number_rewarded = 0;

		#endregion AttributeData

		#region ElementData

		public Surface[] surfaces;

		public Surface[] buoyancy_surfaces;

		public Voxel[] voxels;

		public Vector3i voxel_min;
		public Vector3i voxel_max;

		public Vector3i voxel_physics_min;
		public Vector3i voxel_physics_max;


		public string tooltip_properties_description = "";
		public string tooltip_properties_short_description = "";


		#endregion ElementData

		/// <summary>
		/// Is the block a single Voxel
		/// </summary>
		public bool IsSingleVoxel;


		private bool isCompletelyDefined;
		private bool isCompletelyDefinedSet;
		/// <summary>
		/// Is the definition for this block complete
		/// (not missing info / implementation for the specific type)
		/// </summary>
		public bool IsCompletelyDefined
		{
			get => isCompletelyDefined;
			protected set
			{
				if (isCompletelyDefinedSet)
				{
					// Has been set already by a base class.
					// We need to ensure consistency in the counters.
					if (isCompletelyDefined == value) return;
					if (isCompletelyDefined)
					{
						implementedDefinitionCount--;
					}
					else
					{
						unImplementedDefinitionCount--;
					}
				}

				isCompletelyDefined = value;
				isCompletelyDefinedSet = true;

				if (value)
				{
					implementedDefinitionCount++;
				}
				else
				{
					unImplementedDefinitionCount++;
				}
			}
		}

		/// <summary>
		/// The transform, to place <see cref="mesh_0_name"/> correctly relative to <see cref="mesh_data_name"/>.
		/// </summary>
		public Matrix4 mesh_0_transform = Matrix4.Identity;


		/// <summary>
		/// The transform, to place <see cref="mesh_1_name"/> correctly relative to <see cref="mesh_data_name"/>
		/// </summary>
		public Matrix4 mesh_1_transform = Matrix4.Identity;


		#region Constructors


		private Definition()
		{
			name = "Empty";
			category = DefinitionCategory.Structural;
			type = DefinitionType.Structural;
			mass = 0;
			value = 0;
			flags = DefinitionFlags.None;
			tags = null;
		}

		public Definition(string romPath, XElement definitionElement, Dictionary<string, string> attributes)
		{
			this.fileName = Path.GetFileNameWithoutExtension(romPath);
			name = attributes["name"];
			category = (DefinitionCategory) attributes.GetInt32("category");
			{
				attributes.TryGetInt32("type", out int temp);
				type = (DefinitionType)temp;
			}
			attributes.TryGetFloat("mass", out mass);
			attributes.TryGetInt32("value", out value);
			{
				attributes.TryGetUInt32("flags", out uint temp);
				flags = (DefinitionFlags)temp;
			}

			attributes.TryGetValue("tags", out tags); // will become null when no tags.
			attributes.TryGetValue("mesh_data_name", out mesh_data_name);
			if (string.IsNullOrWhiteSpace(mesh_data_name)) mesh_data_name = null;

			attributes.TryGetValue("mesh_0_name", out mesh_0_name);
			if (string.IsNullOrWhiteSpace(mesh_0_name)) mesh_0_name = null;

			attributes.TryGetValue("mesh_1_name", out mesh_1_name);
			if (string.IsNullOrWhiteSpace(mesh_1_name)) mesh_1_name = null;

			attributes.TryGetValue("mesh_editor_only_name", out mesh_editor_only_name);
			if (string.IsNullOrWhiteSpace(mesh_editor_only_name)) mesh_editor_only_name = null;


			// ...
			ParseSurfaces(definitionElement);
			OptimizeEnclosedPipeSurfaces();
			ParseBuoyancy(definitionElement);
			ParseVoxels(definitionElement);
			// TODO

			voxel_min = VectorSer.Parse3i(definitionElement.Element("voxel_min"), allowNull: true);
			voxel_max = VectorSer.Parse3i(definitionElement.Element("voxel_max"), allowNull: true);

			voxel_physics_min = VectorSer.Parse3i(definitionElement.Element("voxel_physics_min"), allowNull: true);
			voxel_physics_max = VectorSer.Parse3i(definitionElement.Element("voxel_physics_max"), allowNull: true);

			// End of XML data

			IsSingleVoxel = voxels.Length == 1;
		}

		#endregion Constructors

		#region Parsing

		#region AttributesWithDefaults
		private bool TryGetParseValue(Dictionary<string, string> attributes, string key, out int value)
		{
			if (! attributes.TryGetValue(key, out string strValue))
			{
				value = default;
				return false;
			}

			value = int.Parse(strValue);
			return true;
		}

		private bool TryGetParseValue(Dictionary<string, string> attributes, string key, out uint value)
		{
			if (!attributes.TryGetValue(key, out string strValue))
			{
				value = default;
				return false;
			}

			value = uint.Parse(strValue);
			return true;
		}
		#endregion AttributesWithDefaults

		private void ParseSurfaces(XElement definition)
		{
			GetGenericSurfaces(definition, "surfaces", ref surfaces);
		}

		private void ParseBuoyancy(XElement definition)
		{
			GetGenericSurfaces(definition, "buoyancy_surfaces", ref buoyancy_surfaces);
		}

		private void GetGenericSurfaces(XElement definition, string elementName, ref Surface[] array)
		{
			var surfaceElements = definition.Element(elementName).Elements().ToArray();
			array = new Surface[surfaceElements.Length];
			int i = 0;
			foreach (XElement element in surfaceElements)
			{
				array[i++] = new Surface(element);
			}
		}

		private void ParseVoxels(XElement definition)
		{
			var elements = definition.Element("voxels").Elements().ToArray();
			voxels = new Voxel[elements.Length];
			int i = 0;
			foreach (XElement element in elements)
			{
				voxels[i++] = new Voxel(element);
			}
		}


		private static readonly Regex trans_block_mesh_regex =
			new Regex("^meshes/component_transmission_block_", RegexOptions.Compiled);

		/// <summary>
		/// Enclosed pipes use a mesh for the pipe hole surface, which means that
		/// those faces won't be optimized when generating the surface mesh.
		/// This is an oversight by the Stormworks Devs.
		/// We will fix that by detecting such a mesh, removing it,
		/// and patching the surfaces with a new shape.
		/// </summary>
		private void OptimizeEnclosedPipeSurfaces()
		{
			if (null != mesh_data_name && trans_block_mesh_regex.IsMatch(mesh_data_name))
			{
				mesh_data_name = null;

				foreach (Surface surface in surfaces)
				{
					if (surface.shape == Surface.Shape.Pipe_Hole)
					{
						surface.shape = Surface.Shape.Square_With_Pipe_Hole;
					}
				}
			}
		}
		#endregion Parseing




		#region Overrides

		/// <inheritdoc />
		public override string ToString()
		{
			return fileName;
		}

		#endregion


		#region Static

		/// <summary>
		/// An empty definition
		/// </summary>
		public static readonly Definition Empty = new Definition();

		protected static int implementedDefinitionCount = 0;
		internal static int unImplementedDefinitionCount = 0;
		public static int UnImplementedDefinitionCount => unImplementedDefinitionCount;

		#region Parsing

		// This is the primary parser for definitions.
		// It is responsible for discovering what kind of definition it is, and
		// Delegating the Parsing to the responsible class.
		public static Definition Parse(string filePath)
		{
			XDocument document = XMLHelper.LoadFromFile(filePath);

			return Parse(document, filePath);
		}

		public static Definition Parse(XDocument doc, string filePath)
		{
			var element = doc.Element("definition");
			return Parse(element, filePath);
		}

		public static Definition Parse(XElement element, string filePath)
		{
			try
			{
				var attributes = element.Attributes().XAttributeToDictionary();

				DefinitionType type;
				{
					attributes.TryGetInt32("type", out int temp);
					type = (DefinitionType)temp;
				}

				switch (type)
				{
					case DefinitionType.Structural: return new Structural(filePath, element, attributes);
					case DefinitionType.Pipe:       return new Transmission(filePath, element, attributes);
					case DefinitionType.TrainBogie: return new TrainBogie(filePath, element, attributes);
					case DefinitionType.Weapon:     return Weapon.Parse(filePath, element, attributes);
					case DefinitionType.RocketPart: return RocketPart.Parse(filePath, element, attributes);
					case DefinitionType.Sign:       return new PaintableThingy(filePath, element, attributes);
					default:                        return new UnImplemented(filePath, element, attributes);
				}
			}
			catch (Exception e)
			{
				throw new XmlDataException(element, e);
			}
		}
		#endregion Parsing

		#region Dictoinary

		public static bool Ready { get; private set; }

		private static Dictionary<string, Definition> definitions;

		private static ReadOnlyDictionary<string, Definition> readOnlyDefinitions;
		public static IReadOnlyDictionary<string, Definition> Definitions { get; private set; }

		/// <summary>
		/// Parse all Component Definitions in the folder.
		/// </summary>
		/// <param name="folderPath"></param>
		/// <exception cref="FileInteractionException"></exception>
		public static void ParseDefinitions(string folderPath)
		{
			definitions = new Dictionary<string, Definition>();
			readOnlyDefinitions = new ReadOnlyDictionary<string, Definition>(definitions);


			var fileNameList = Directory.GetFiles(folderPath, "*.xml");
			foreach (string path in fileNameList)
			{
				try
				{
					var definition = Parse(path);
					definitions.Add(definition.fileName, definition);
				}
				catch (Exception e)
				{
					throw new FileInteractionException(path, e);
				}
			}

			Console.WriteLine($"Parsed {definitions.Count} files resulting in {implementedDefinitionCount}"
			                + $" complete definitions, and {unImplementedDefinitionCount} partial definitions.");

			Definitions = readOnlyDefinitions;

			Ready = true;
		}

		/// <summary>
		/// Parse Component Definitions on the fly, as they are requested.
		/// </summary>
		/// <param name="folderPath"></param>
		public static void UseOnTheFlyDefinitions(string folderPath)
		{
			Console.WriteLine("Definitions will be read on the fly as requested.");
			var otf = new ReadOnTheFlyDefinitions(folderPath);
			definitions = otf.definitions;
			Definitions = otf;

			Ready = true;
		}

		/// <summary>
		/// Throw an Exception when definitions are not loaded and there is no on-the-fly provider either.
		/// </summary>
		/// <exception cref="DefinitionsNotLoadedException"></exception>
		public static void AssertDefinitionsReady()
		{
			if (Definitions != null) return;

			throw new DefinitionsNotLoadedException();
		}

		#endregion Dictionary

		#endregion Static
	}
}
