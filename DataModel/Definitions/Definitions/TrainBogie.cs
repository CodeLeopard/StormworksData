﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;

using OpenToolkit.Mathematics;

namespace DataModel.Definitions
{
	public class TrainBogie : Definition
	{
		/// <inheritdoc />
		public TrainBogie(string romPath, XElement definitionElement, Dictionary<string, string> attributes) : base
			(romPath, definitionElement, attributes)
		{
			IsCompletelyDefined = true;

			mesh_0_transform = Matrix4.Identity;
			mesh_0_transform.Column3 = new Vector4(0, 0.5381f, 0, 1); // todo: this not quite right but close enough for now...
		}
	}
}
