﻿// Copyright 2022-2024 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System.Collections.Generic;
using System.Xml.Linq;

using DataModel.DataTypes;

namespace DataModel.Definitions
{
	/// <summary>
	/// Base class for Rocket related parts.
	/// </summary>
	public abstract class RocketPart : Definition
	{
		public RocketType RocketType;

		/// <inheritdoc />
		protected RocketPart(string romPath, XElement definitionElement, Dictionary<string, string> attributes)
			: base(romPath, definitionElement, attributes)
		{
			IsCompletelyDefined = true;

			RocketType = (RocketType)byte.Parse(attributes["rocket_type"]);
		}

		internal static RocketPart Parse(string romPath, XElement element, Dictionary<string, string> attributes)
		{
			var rocketType = (RocketType)byte.Parse(attributes["rocket_type"]);
			switch (rocketType)
			{
				case RocketType.Body:   return new RocketBody(romPath, element, attributes);
				case RocketType.Nozzle: return new RocketThruster(romPath, element, attributes);
				case RocketType.Fins:   return new RocketBody(romPath, element, attributes); // todo: bandaid: fix properly.
				default:
					throw new CustomSerialization.Exceptions.XmlDataException
						($"Unexpected value '{rocketType}' for attribute: 'rocket_type'", element);
			}
		}
	}
}
