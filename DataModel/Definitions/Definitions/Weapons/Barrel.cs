﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System.Collections.Generic;
using System.Xml.Linq;

using Shared.Serialization;

namespace DataModel.Definitions.Weapons
{
	/// <summary>
	/// Barrel of a gun (also base class for <see cref="Gun"/>).
	/// </summary>
	public class Barrel : Weapon
	{
		/// <summary>
		/// The length of the barrel measured in voxels from 0,0,0 in +y direction.
		/// </summary>
		public int BarrelLength;
		/// <inheritdoc />
		public Barrel(string romPath, XElement definitionElement, Dictionary<string, string> attributes)
			: base(romPath, definitionElement, attributes)
		{
			IsCompletelyDefined = true;

			BarrelLength = attributes.GetInt32("weapon_barrel_length_voxels");
		}
	}
}
