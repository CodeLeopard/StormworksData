﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/

#if DEBUG
using System.Collections.Generic;
using System.Xml.Linq;

using DataModel.DataTypes;

using OpenToolkit.Mathematics;

namespace DataModel.Definitions
{
	/// <summary>
	/// Raw definition that simply holds on to all the values of all attributes and elements.
	/// Not meant for general usage.
	/// </summary>
	public class RawDefinition : Definition
	{
		#region AttributeData

		public string audio_filename_start;
		public string audio_filename_loop;
		public string audio_filename_end;
		public float audio_gain;


		public string child_name;

		public string extender_name;

		public byte constraint_type;
		public byte constraint_axis;
		public float constraint_range_of_motion;

		public float max_motor_force;


		public byte seat_type;


		public float force_emitter_max_force;
		public float force_emitter_max_vector;

		public float engine_max_force;

		public byte trans_conn_type;
		public byte trans_type;

		public float wheel_radius;

		public byte button_type;

		public float light_intensity;
		public float light_range;
		public string light_ies_map;
		public float light_fov;
		public byte light_type;

		public float door_lower_limit ;
		public float door_upper_limit;
		public bool door_flipped;
		public float door_side_dist;
		public float door_up_dist;

		public CustomDoorType custom_door_type;

		public float dynamic_min_rotation;
		public float dynamic_max_rotation;

		public LogicGateType logic_gate_type;
		public LogicGateSubType logic_gate_subtype;

		public IndicatorType indicator_type;

		public ConnectorType connector_type;

		public float magnet_force;

		public GyroType gyro_type;

		public float rudder_surface_area;



		#endregion AttributeData


		#region ElementData


		/// <summary>
		/// Used to add physics to seats. Units in meters.
		/// </summary>
		public Vector3 bb_physics_min = Vector3.Zero;

		/// <summary>
		/// Used to add physics to seats. Units in meters.
		/// </summary>
		public Vector3 bb_physics_max = Vector3.Zero;

		public Vector3 constraint_pos_parent = Vector3.Zero;
		public Vector3 constraint_pos_child = Vector3.Zero;

		public Vector3i voxel_location_child = new Vector3i(0, 0, 0);

		public Vector3 force_dir = Vector3.Zero;

		public Vector3i light_position = new Vector3i(0, 0, 0);
		public Vector3 light_color = Vector3.One;


		public Vector3 door_size = Vector3.One;
		public Vector3 door_normal = Vector3.UnitZ;
		public Vector3 door_side = Vector3.UnitX;
		public Vector3 door_up = Vector3.UnitY;
		public Vector3 door_base_pos = Vector3.Zero;


		public Vector3i dynamic_body_position = new Vector3i(0, 0, 0);
		public Vector3 dynamic_rotation_axes = new Vector3(0, 1, 0);
		public Vector3 dynamic_side_axis = new Vector3(1,     0, 0);

		public Vector3 magnet_offset = Vector3.Zero;


		#endregion ElementData

		/// <inheritdoc />
		public RawDefinition(string romPath, XElement definitionElement, Dictionary<string, string> attributes)
			: base(romPath, definitionElement, attributes)
		{
			IsCompletelyDefined = false;
			// todo: Read values.
		}
	}
}
#endif