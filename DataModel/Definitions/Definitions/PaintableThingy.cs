﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;

using DataModel.Definitions.NestedTypes;

namespace DataModel.Definitions.Definitions
{
	/// <summary>
	/// Represents paintable sign and paintable indicator.
	/// They have a single face where you can color a 9X9 grid of squares.
	/// In the case of the paintable inidcator there is an additional emissive layer that can be enabled with a boolean input.
	/// </summary>
	public class PaintableThingy : Definition
	{
		/// <inheritdoc />
		public PaintableThingy(string romPath, XElement definitionElement, Dictionary<string, string> attributes)
			: base(romPath, definitionElement, attributes)
		{
			// Remove the surface where the paintable faces will be on.
			surfaces[2].shape = Surface.Shape.None;
		}
	}
}
