﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;

namespace DataModel.Definitions.Definitions
{
	/// <summary>
	/// Test class for messing with the emissive part of buttons etc.
	/// </summary>
	public class Scaled2ndMesh : Definition
	{
		/// <inheritdoc />
		public Scaled2ndMesh(string romPath, XElement definitionElement, Dictionary<string, string> attributes)
			: base
			(romPath, definitionElement, attributes)
		{

		}
	}
}
