﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/


using System;
using System.Collections.Generic;
using System.Xml.Linq;

using DataModel.DataTypes;
using DataModel.Definitions.Weapons;

namespace DataModel.Definitions
{
	public abstract class Weapon : Definition
	{
		public WeaponType WeaponType;
		public WeaponClass WeaponClass;

		/// <inheritdoc />
		protected Weapon(string romPath, XElement definitionElement, Dictionary<string, string> attributes)
			: base(romPath, definitionElement, attributes)
		{
			IsCompletelyDefined = false;

			WeaponType = (WeaponType)byte.Parse(attributes["weapon_type"]);
			WeaponClass = (WeaponClass)byte.Parse(attributes["weapon_class"]);
		}


		internal static Weapon Parse(string romPath, XElement element, Dictionary<string, string> attributes)
		{
			var weaponType = (WeaponType)byte.Parse(attributes["weapon_type"]);
			switch (weaponType)
			{
				case WeaponType.WarheadEMP:     return new WarheadEMP(romPath, element, attributes);
				case WeaponType.Warhead:        return new Warhead(romPath, element, attributes);
				case WeaponType.Gun:            return new Gun(romPath, element, attributes);
				case WeaponType.Belt:           return new Belt(romPath, element, attributes);
				case WeaponType.Barrel:         return new Barrel(romPath, element, attributes);
				case WeaponType.Drum:           return new Drum(romPath, element, attributes);
				case WeaponType.RocketLauncher: return new RocketLauncher(romPath, element, attributes);
				case WeaponType.Muzzle:         return new Muzzle(romPath, element, attributes);

				default: throw new ArgumentException($"Unknown value '{weaponType}' for attribute 'weapon_type'");
			}
		}
	}
}
