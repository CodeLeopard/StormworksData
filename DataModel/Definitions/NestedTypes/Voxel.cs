﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;

using DataModel.DataTypes;

using OpenToolkit.Mathematics;

using Shared.Exceptions;
using Shared.Serialization;
using Shared.Serialization.DataTypes;

namespace DataModel.Definitions.NestedTypes
{
	public class Voxel
	{
		#region Enums

		[Flags]
		public enum Flags : byte
		{
			/// <summary>
			/// Voxel is empty.
			/// </summary>
			Empty        = 0,         // 0

			/// <summary>
			/// Voxel has physics as defined by <see cref="PhysicsShape"/>
			/// </summary>
			Physics      = 1 << 0,    // 1

			/// <summary>
			/// Voxel is empty, but blocks (mouse) RayCasts.
			/// </summary>
			RaycastBlock = 1 << 1,    // 2
			// Physics + RaycastBlock // 3

			/// <summary>
			/// Voxel contains the opening of a door.
			/// Blocks RayCast too.
			/// </summary>
			DoorOpening  = 1 << 2,    // 4
		}

		enum PhysicsShape : byte
		{
			Cube            = 0,
			Wedge           = 1,
			Pyramid         = 2,
			InvertedPyramid = 3,
			Wedge2x1_Front  = 4,
			Wedge2x1_Back   = 5,
			Wedge4x1_Front    = 6,
			Wedge4x1_MidFront = 7,
			Wedge4x1_MidBack  = 8,
			Wedge4x1_Back     = 9,
			// Gap 10
			// Gap 11
			// todo: there are loads of them

		}

		/// <summary>
		/// The directions in which a pipe connection can be made.
		/// </summary>
		[Flags]
		public enum BuoyPipe : byte
		{
			None = 0,
			XPositive = 1 << 0, //  1
			YPositive = 1 << 1, //  2
			ZPositive = 1 << 2, //  4
			XNegative = 1 << 3, //  8
			YNegative = 1 << 4, // 16
			ZNegative = 1 << 5, // 32
		}


		#endregion Enums




		public Flags flags;
		public byte physics_shape;
		public BuoyPipe buoy_pipes;

		public Vector3i position;

		public Matrix3? physics_shape_rotation; // Todo: Integers only?

		public Voxel(XElement voxelElement)
		{
			try
			{
				var attributes = voxelElement.Attributes().XAttributeToDictionary();
				if (attributes.TryGetValue("flags", out var flagsStr)) flags = (Flags) byte.Parse(flagsStr);

				if (attributes.TryGetValue("physics_shape", out var physics_shapeStr))
					physics_shape = byte.Parse(physics_shapeStr);

				if (attributes.TryGetValue("buoy_pipes", out var buoy_pipesStr)) buoy_pipes = (BuoyPipe) byte.Parse(buoy_pipesStr);
				position = VectorSer.Parse3i(voxelElement.Element("position"));

				var psr_element = voxelElement.Element("physics_shape_rotation");
				if (null != psr_element)
				{
					physics_shape_rotation = MatrixSer.ParseMatrix3(psr_element, allowNull: true);
				}
			}
			catch (Exception e)
			{
				throw new XmlDataException(voxelElement, e);
			}
		}
	}
}
