﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Xml.Linq;

using OpenToolkit.Mathematics;

using BinaryDataModel;
using BinaryDataModel.DataTypes;

using Shared;
using Shared.Exceptions;
using Shared.Serialization;
using Shared.Serialization.DataTypes;

namespace DataModel.Definitions.NestedTypes
{
	public class Surface
	{
		#region Enums

		public enum Orientation : byte
		{
			XPositive = 0,
			XNegative = 1,
			YPositive = 2,
			YNegative = 3,
			ZPositive = 4,
			ZNegative = 5,
		}

		public enum Shape : UInt16
		{
			// todo: use or at least document the game-internal names for these.

			None      = 0,
			Square    = 1,
			Triangle  = 2,

			// todo: investigate what this really is
			// Used only for standard pipes, not enclosed pipes.
			Pipe_Hole = 3,

			Square_Static = 4,
			/// <summary>
			/// Has a diamond in the center.
			/// </summary>
			Square_Weight            = 5,
			Wedge                    = 6,
			Pyramid                  = 7,
			Pyramid_Inv              = 8,
			Triangle_a_1x2_Top       = 9,
			Triangle_a_1x2_Bottom    = 10,
			Triangle_b_1x2_Top       = 11,
			Triangle_b_1x2_Bottom    = 12,
			Triangle_a_1x4_Top       = 13,
			Triangle_a_1x4_MidTop    = 14,
			Triangle_a_1x4_MidBottom = 15,
			Triangle_a_1x4_Bottom    = 16,
			Triangle_b_1x4_Top       = 17,
			Triangle_b_1x4_MidTop    = 18,
			Triangle_b_1x4_MidBottom = 19,
			Triangle_b_1x4_Bottom    = 20,
			Wedge_1x2_Top         = 21,
			Wedge_1x2_Bottom      = 22,
			Wedge_1x4_Top         = 23,
			Wedge_1x4_MidTop      = 24,
			Wedge_1x4_MidBottom   = 25,
			Wedge_1x4_Bottom      = 26,
			Pyramid_1x2_Top       = 27,
			Pyramid_1x2_Bottom    = 28,
			Pyramid_1x4_Top       = 29,
			Pyramid_1x4_MidTop    = 30,
			Pyramid_1x4_MidBottom = 31,
			Pyramid_1x4_Bottom    = 32,
			Pyramid_2x2_Top       = 33,
			Pyramid_2x2_Edge      = 34,
			Pyramid_2x4_Top       = 35,
			Pyramid_2x4_MidTop    = 36,
			Pyramid_2x4_MidBottom = 37,
			Pyramid_2x4_End       = 38,
			// 39..42 not used?
			Pyramid_4x4_Top           = 43,
			Pyramid_4x4_MidTop        = 44,
			Pyramid_4x4_MidBottom     = 45,
			Pyramid_4x4_End           = 46,
			Pyramid_Inv_1x2_Triangle  = 47,
			Pyramid_Inv_1x2_Trapezoid = 48,
			Pyramid_Inv_1x4_Point     = 49,
			Pyramid_Inv_1x4_MidPoint  = 50,
			Pyramid_Inv_1x4_MidEdge   = 51,
			Pyramid_Inv_1x4_Edge      = 52,
			Pyramid_Inv_2x2_Point     = 53,
			Pyramid_Inv_2x2_Edge      = 54,
			Pyramid_Inv_2x4_Point     = 55,
			Pyramid_Inv_2x4_TopPoint  = 56,
			Pyramid_Inv_2x4_TopEdge   = 57,
			Pyramid_Inv_2x4_MidEdge   = 58,
			// 59..62 not used?
			Pyramid_Inv_4x4_BotPoint = 63,
			Pyramid_Inv_4x4_MidPoint = 64,
			Pyramid_Inv_4x4_MidEdge  = 65,
			Pyramid_Inv_4x4_TopEdge  = 66,


			// This is an optimization made by DataModel,
			// so that the enclosed pipes can be optimized better.
			Square_With_Pipe_Hole = 101,
		}


		/// <summary>
		/// Defines the kind of pipe (fluid||shaftPower) this surface is.
		/// </summary>
		public enum TransType : byte
		{
			/// <summary>
			/// No connection.
			/// </summary>
			None = 0,
			/// <summary>
			/// Power (shaft) connection.
			/// </summary>
			Power = 1,
			/// <summary>
			/// Fluid connection.
			/// </summary>
			Fluid = 2,
		}
		#endregion Enums


		#region Xml Data
		/// <summary>
		/// Orientation of the surface around the voxel.
		/// </summary>
		public Orientation orientation;

		/// <summary>
		/// Rotation of the shape
		/// </summary>
		public byte rotation;

		/// <summary>
		/// The shape of the surface
		/// </summary>
		public Shape shape;

		/// <summary>
		/// The pipe type of this surface.
		/// </summary>
		public TransType trans_type;

		/// <summary>
		/// Voxel position
		/// </summary>
		public Vector3i position;

		#endregion Xml Data

		#region Derived Data

		/// <summary>
		/// Transform from Component-Voxel-Space to Component-Mesh-Space
		/// </summary>
		public Matrix4 surfaceTransform;

		/// <summary>
		/// The Mesh for this Surface
		/// </summary>
		/// <remarks>
		/// Can be null, not all shapes are defined yet.
		/// </remarks>
		public Mesh surfaceMesh;

		#endregion Derived Data

		#region MeshGeneratorData
		/// <summary>
		/// When getting surfaces from a component a color can be assigned.
		/// </summary>
		public Color4 surfaceColor;

		public bool reverseTriangles;

		#endregion MeshGeneratorData

		const float blockSize = 0.25f;
		const float surfaceOffset = blockSize / 2;


		#region Mesh generation constants
		const float a = surfaceOffset;
		const float b = -surfaceOffset;
		const float c = 0;
		const float d = a / 2;
		const float e = b / 2;


		const float top = a;
		const float bot = b;
		const float hlf = c;
		const float tph = d;
		const float bth = e;
		#endregion Mesh generation constants


		public Surface(XElement surfaceElement)
		{
			try
			{
				var attributes = surfaceElement.Attributes().XAttributeToDictionary();
				{
					attributes.TryGetByte("orientation", out byte temp);
					orientation = (Orientation)temp;
				}
				attributes.TryGetByte("rotation", out rotation);
				{
					attributes.TryGetByte("shape", out byte temp);
					shape = (Shape)temp;
				}
				{
					attributes.TryGetByte("trans_type", out byte temp);
					trans_type = (TransType)temp;
				}
				position = VectorSer.Parse3i(surfaceElement.Element("position"));

				if (! orientationRotationTransforms.TryGetValue((orientation, rotation), out surfaceTransform))
				{
					// Todo: Complain
				}

				var fPosition = new Vector3(position.X, position.Y, position.Z) * 0.25f;
				surfaceTransform.Column3 = new Vector4(fPosition, 1);

				if (! shapes.TryGetValue(shape, out surfaceMesh))
				{
					// todo: Complain
				}
			}
			catch (Exception e)
			{
				throw new XmlDataException(surfaceElement, e);
			}
		}

		public Surface(Surface toCopy)
		{
			orientation = toCopy.orientation;
			rotation = toCopy.rotation;
			shape = toCopy.shape;
			trans_type = toCopy.trans_type;
			position = toCopy.position;
			surfaceTransform = toCopy.surfaceTransform;
			surfaceMesh = toCopy.surfaceMesh;
			surfaceColor = toCopy.surfaceColor;
			reverseTriangles = toCopy.reverseTriangles;
		}

		public Surface Clone()
		{
			return new Surface(this);
		}

		/// <inheritdoc />
		public override string ToString()
		{
			return $"{{Surface O:{orientation}, R:{rotation}, S:{shape} at Voxel: {position}}}";
		}

		/// <summary>
		/// True if the given surfaces are opposing each other,
		/// that is they are in the same (mesh-space) position
		/// and are facing each other.
		///
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <returns></returns>
		public static bool AreOpposingSurfaces(Surface a, Surface b)
		{
			// Test that a faces b and is adjacent to it.
			Vector3i fromA = a.position + OrientationToVector3i(a.orientation);
			if (fromA != b.position) return false;

			// Test that b also faces a.
			Vector3i fromB = b.position + OrientationToVector3i(b.orientation);
			if (fromB != a.position) return false;

			return true;
		}

		/// <summary>
		/// Returns true if the given surfaces are inside each other in such a way
		/// that they should be eliminated for rendering.
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <returns></returns>
		public static bool AreMeshGenEliminatedSurfaces(Surface a, Surface b, bool assumeOpposing = false)
		{
			if (!assumeOpposing && ! AreOpposingSurfaces(a, b))
			{
				return false;
			}

			// Easy case: both are a square shape -> rotation does not matter.
			if (IsSquareShape(a.shape) && IsSquareShape(b.shape))
			{
				return true;
			}

			// Pipe hole is not compatible with other shapes,
			// but it is rotation invariant with itself.
			if (a.shape == Shape.Pipe_Hole             && b.shape == Shape.Pipe_Hole
			 || a.shape == Shape.Square_With_Pipe_Hole && b.shape == Shape.Square_With_Pipe_Hole)
			{
				return true;
			}

			// For the other shapes the rotation also matters.
			// Opposing but rotated triangles could be visible.
			// todo: implement.
			return false;
		}

		/// <summary>
		/// Returns true if surface <paramref name="a"/> is
		/// eliminated for rendering by the presence of <paramref name="b"/>.
		/// Note: always check <see cref="AreMeshGenEliminatedSurfaces"/> first.
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <returns></returns>
		public static bool IsMeshGenEliminatedSurface(Surface a, Surface b, bool assumeOpposing = false)
		{
			if (!assumeOpposing && !AreOpposingSurfaces(a, b))
			{
				return false;
			}

			// Easy case: a is flat and b is a square shape -> rotation does not matter and a is always eliminated.
			if (IsFlatShape(a.shape) && IsSquareShape(b.shape))
			{
				return true;
			}

			// For the other shapes the rotation also matters.
			// Opposing but rotated triangles could be visible.
			// todo: implement.
			return false;
		}

		/// <summary>
		/// Returns true for any <see cref="Shape"/> that is a Square (covers the entire face).
		/// </summary>
		/// <param name="s"></param>
		/// <returns></returns>
		public static bool IsSquareShape(Shape s)
		{
			return s == Shape.Square
			    || s == Shape.Square_Static
			    || s == Shape.Square_Weight
			    || s == Shape.Square_With_Pipe_Hole;
		}

		public static bool IsFlatShape(Shape s)
		{
			int index = (int)s;
			return isFlatShapeLookup[index];
		}


		#region Static


		private static Dictionary<Shape, Mesh> shapes = InitShapes();
		private static Dictionary<(Orientation, byte), Matrix4> orientationRotationTransforms = InitOrientationRotationTransforms();
		private static bool[] isFlatShapeLookup; // initialized by InitShapes.



		private static Dictionary<Shape, Mesh> InitShapes()
		{
			void ComputeNormal(VertexRecord[] data, UInt32[] indices)
			{
				var p1 = data[indices[0]].position;
				var p2 = data[indices[1]].position;
				var p3 = data[indices[2]].position;

				var l1 = p1 - p2;
				var l2 = p1 - p3;

				var norm = - Vector3.Cross(l1, l2).Normalized();

				for(int i = 0; i < data.Length; i++)
				{
					data[i].normal = norm;
				}
			}



			var result = new Dictionary<Shape, Mesh>();
			var flatMapTemp = new Dictionary<Shape, bool>();


			var prc = MeshCombiner.paintZoneColors[0];

			var normal = Vector3.UnitX;

			var trivialIndices = new UInt32[3]
			{
				0, 1, 2
			};

			Mesh RotateCrappyFix(Mesh mesh, float amount = 1)
			{
				float faceRotationAmount = amount * 90f * (float)(Math.PI / 180);
				Matrix4 faceRotationTransform = Matrix4.CreateFromAxisAngle(Vector3.UnitX, faceRotationAmount);

				return MeshCombiner.Combine(new[] { new MeshCombiner.DataItem(mesh, faceRotationTransform), });
			}

			void MkEntry(Shape id, bool flat, Vector3[] vertices, UInt32[] indices = null, Color4[] colors = null, float? rotate = null)
			{
				flatMapTemp.Add(id, flat);

				string name = id.ToString();

				if (null == indices) indices = trivialIndices;

				var vertexRecords = new VertexRecord[vertices.Length];
				var bounds = new Bounds3(true);
				for (int i = 0; i < vertexRecords.Length; i++)
				{
					bounds.Inflate(vertices[i]);
					vertexRecords[i] = new VertexRecord(vertices[i], colors?[i] ?? prc, normal);
				}
				ComputeNormal(vertexRecords, indices);

				var mesh = new Mesh(vertexRecords, indices, bounds);
				mesh.fileName = $"__Surface_{id}_{name}";

				if (rotate.HasValue) mesh = RotateCrappyFix(mesh, rotate.Value);

				result[id] = mesh;
			}


			void MkMirror(Shape id, Shape mirrorFromID, byte mirrorAxis, float? rotate = null)
			{
				var from = result[mirrorFromID];

				var vertices = new Vector3[from.vertices.Count];
				var indices = new UInt32[from.indices.Count];

				for (int i = 0; i < vertices.Length; i++)
				{
					var fromRecord = from.vertices[i];
					var newPos = fromRecord.position;
					switch (mirrorAxis)
					{
						case 0:
						{
							newPos.X = -newPos.X;
							break;
						}
						case 1:
						{
							newPos.Y = -newPos.Y;
							break;
						}
						case 2:
						{
							newPos.Z = -newPos.Z;
							break;
						}
						default:
						{
							throw new ArgumentOutOfRangeException(nameof(mirrorAxis), mirrorAxis, "Should be in [0, 1, 2].");
						}
					}
					vertices[i] = new Vector3(newPos);
				}

				for (int i = 0; i < indices.Length / 3; i++)
				{
					var i0 = i * 3 + 0;
					var i1 = i * 3 + 1;
					var i2 = i * 3 + 2;

					indices[i0] = from.indices[i2];
					indices[i1] = from.indices[i1];
					indices[i2] = from.indices[i0];
				}

				MkEntry(id, flatMapTemp[mirrorFromID], vertices, indices, rotate: rotate);
			}


			void AfterMakingEntries()
			{
				// Initialize the isFlatShapeLookup array.
				int highest = 0;
				foreach (var shape in flatMapTemp.Keys)
				{
					int shapeId = (int)shape;

					if (shapeId > flatMapTemp.Count * 1.1f + 40) // Some margin for gaps
					{
						Debugger.Break();
						// Something is probably out of whack.
						// There are some gaps in the ShapeIDs, but not huge ones.
					}

					highest = Math.Max(highest, shapeId);
				}

				isFlatShapeLookup = new bool[highest + 1];
				foreach (var kvp in flatMapTemp)
				{
					int index = (int)kvp.Key;
					isFlatShapeLookup[index] = kvp.Value;
				}
			}

			// After this point there are only calls to MkEntry, MkMirror
			// After those AfterMakingEntries is called and the result is returned.
			////////////////////////////////////////////////////////////////////////////
			#region MkEntry

			#region 1x1 Shapes
			MkEntry(Shape.Square, flat: true, new[] {
					 new Vector3(a, b, a),
					 new Vector3(a, a, a),
					 new Vector3(a, a, b),
					 new Vector3(a, b, b)
				 }
			   , new UInt32[] { 0, 1, 2, 2, 3, 0 }
				);

			MkEntry(Shape.Triangle, flat: true, new[] {
				        new Vector3(a, a, a),
				        new Vector3(a, a, b), // 90 Corner
				        new Vector3(a, b, b)
			        }
			       );


			#region Pipes

			MkEntry
				(
				 Shape.Pipe_Hole, flat: true, new[]
				 {
					 new Vector3(a, 0.000000f, 0.000000f)
				   , // Center
					 new Vector3(a, -0.053261f, -0.022062f)
				   , new Vector3(a, -0.022062f, -0.053261f)
				   , new Vector3(a,  0.022062f, -0.053261f)
				   , new Vector3(a,  0.053261f, -0.022062f)
				   , new Vector3(a,  0.053261f,  0.022062f)
				   , new Vector3(a,  0.022061f,  0.053261f)
				   , new Vector3(a, -0.022062f,  0.053261f)
				   , new Vector3(a, -0.053261f,  0.022062f)
				   , new Vector3(a, -0.053261f, -0.022062f)
				   , new Vector3(a, -0.022062f, -0.053261f)
				   , new Vector3(a,  0.022062f, -0.053261f)
				   , new Vector3(a,  0.053261f, -0.022062f)
				   , new Vector3(a,  0.053261f,  0.022062f)
				   , new Vector3(a,  0.022061f,  0.053261f)
				   , new Vector3(a, -0.022062f,  0.053261f)
				   , new Vector3(a, -0.053261f,  0.022062f)
				   , new Vector3(a, -0.062500f, -0.025888f)
				   , new Vector3(a, -0.025888f, -0.062500f)
				   , new Vector3(a,  0.025888f, -0.062500f)
				   , new Vector3(a,  0.062500f, -0.025888f)
				   , new Vector3(a,  0.062500f,  0.025888f)
				   , new Vector3(a,  0.025888f,  0.062500f)
				   , new Vector3(a, -0.025888f,  0.062500f)
				   , new Vector3(a, -0.062500f,  0.025888f)
					,
				 }
			   , new UInt32[]
				 {
					  0,  2,  1
				   , 10, 17,  9
				   , 10, 18, 17
				   ,  0,  3,  2
				   , 11, 18, 10
				   , 11, 19, 18
				   ,  0,  4,  3
				   , 12, 19, 11
				   , 12, 20, 19
				   ,  0,  5,  4
				   , 13, 20, 12
				   , 13, 21, 20
				   ,  0,  6,  5
				   , 14, 21, 13
				   , 14, 22, 21
				   ,  0,  7,  6
				   , 15, 22, 14
				   , 15, 23, 22
				   ,  0,  8,  7
				   , 16, 23, 15
				   , 16, 24, 23
				   ,  0,  1,  8
				   ,  9, 24, 16
				   ,  9, 17, 24
				 }
			   , new Color4[]
				 {
					 new Color4( 64,  64,  64, 255)
				   , new Color4( 64,  64,  64, 255)
				   , new Color4( 64,  64,  64, 255)
				   , new Color4( 64,  64,  64, 255)
				   , new Color4( 64,  64,  64, 255)
				   , new Color4( 64,  64,  64, 255)
				   , new Color4( 64,  64,  64, 255)
				   , new Color4( 64,  64,  64, 255)
				   , new Color4( 64,  64,  64, 255)
				   , new Color4(255, 163,   0, 255)
				   , new Color4(255, 163,   0, 255)
				   , new Color4(255, 163,   0, 255)
				   , new Color4(255, 163,   0, 255)
				   , new Color4(255, 163,   0, 255)
				   , new Color4(255, 163,   0, 255)
				   , new Color4(255, 163,   0, 255)
				   , new Color4(255, 163,   0, 255)
				   , new Color4(255, 163,   0, 255)
				   , new Color4(255, 163,   0, 255)
				   , new Color4(255, 163,   0, 255)
				   , new Color4(255, 163,   0, 255)
				   , new Color4(255, 163,   0, 255)
				   , new Color4(255, 163,   0, 255)
				   , new Color4(255, 163,   0, 255)
				   , new Color4(255, 163,   0, 255)
					,
				 }
				);

			MkEntry
				(
				 Shape.Square_With_Pipe_Hole, flat: true
			   ,                              new Vector3[]
				 {
					 new Vector3(0.125000f,  0.000000f,  0.000000f)
				   , new Vector3(0.125000f, -0.022062f, -0.053261f)
				   , new Vector3(0.125000f, -0.053261f, -0.022062f)
				   , new Vector3(0.125000f, -0.022062f, -0.053261f)
				   , new Vector3(0.125000f, -0.062500f, -0.025888f)
				   , new Vector3(0.125000f, -0.053261f, -0.022062f)
				   , new Vector3(0.125000f, -0.025888f, -0.062500f)
				   , new Vector3(0.125000f,  0.022062f, -0.053261f)
				   , new Vector3(0.125000f,  0.022062f, -0.053261f)
				   , new Vector3(0.125000f,  0.025888f, -0.062500f)
				   , new Vector3(0.125000f,  0.053261f, -0.022062f)
				   , new Vector3(0.125000f,  0.053261f, -0.022062f)
				   , new Vector3(0.125000f,  0.062500f, -0.025888f)
				   , new Vector3(0.125000f,  0.053261f,  0.022062f)
				   , new Vector3(0.125000f,  0.053261f,  0.022062f)
				   , new Vector3(0.125000f,  0.062500f,  0.025888f)
				   , new Vector3(0.125000f,  0.022061f,  0.053261f)
				   , new Vector3(0.125000f,  0.022061f,  0.053261f)
				   , new Vector3(0.125000f,  0.025888f,  0.062500f)
				   , new Vector3(0.125000f, -0.022062f,  0.053261f)
				   , new Vector3(0.125000f, -0.022062f,  0.053261f)
				   , new Vector3(0.125000f, -0.025888f,  0.062500f)
				   , new Vector3(0.125000f, -0.053261f,  0.022062f)
				   , new Vector3(0.125000f, -0.053261f,  0.022062f)
				   , new Vector3(0.125000f, -0.062500f,  0.025888f)
				   , new Vector3(0.125000f, -0.125000f, -0.125000f)
				   , new Vector3(0.125000f, -0.025888f, -0.062500f)
				   , new Vector3(0.125000f,  0.125000f, -0.125000f)
				   , new Vector3(0.125000f, -0.025888f, -0.062500f)
				   , new Vector3(0.125000f, -0.125000f, -0.125000f)
				   , new Vector3(0.125000f, -0.062500f, -0.025888f)
				   , new Vector3(0.125000f, -0.062500f, -0.025888f)
				   , new Vector3(0.125000f, -0.125000f, -0.125000f)
				   , new Vector3(0.125000f, -0.125000f,  0.125000f)
				   , new Vector3(0.125000f, -0.062500f, -0.025888f)
				   , new Vector3(0.125000f, -0.125000f,  0.125000f)
				   , new Vector3(0.125000f, -0.062500f,  0.025888f)
				   , new Vector3(0.125000f, -0.062500f,  0.025888f)
				   , new Vector3(0.125000f, -0.125000f,  0.125000f)
				   , new Vector3(0.125000f, -0.025888f,  0.062500f)
				   , new Vector3(0.125000f, -0.025888f,  0.062500f)
				   , new Vector3(0.125000f, -0.125000f,  0.125000f)
				   , new Vector3(0.125000f,  0.025888f,  0.062500f)
				   , new Vector3(0.125000f,  0.125000f, -0.125000f)
				   , new Vector3(0.125000f,  0.062500f, -0.025888f)
				   , new Vector3(0.125000f,  0.125000f,  0.125000f)
				   , new Vector3(0.125000f,  0.062500f, -0.025888f)
				   , new Vector3(0.125000f,  0.125000f, -0.125000f)
				   , new Vector3(0.125000f,  0.025888f, -0.062500f)
				   , new Vector3(0.125000f,  0.025888f, -0.062500f)
				   , new Vector3(0.125000f,  0.125000f, -0.125000f)
				   , new Vector3(0.125000f, -0.025888f, -0.062500f)
				   , new Vector3(0.125000f,  0.125000f,  0.125000f)
				   , new Vector3(0.125000f,  0.062500f, -0.025888f)
				   , new Vector3(0.125000f,  0.062500f,  0.025888f)
				   , new Vector3(0.125000f,  0.125000f,  0.125000f)
				   , new Vector3(0.125000f,  0.062500f,  0.025888f)
				   , new Vector3(0.125000f,  0.025888f,  0.062500f)
				   , new Vector3(0.125000f,  0.125000f,  0.125000f)
				   , new Vector3(0.125000f,  0.025888f,  0.062500f)
				   , new Vector3(0.125000f, -0.125000f,  0.125000f)
					,
				 }
			   , new uint[]
				 {
					  0,  1,  2
				   ,  3,  4,  5
				   ,  3,  6,  4
				   ,  0,  7,  1
				   ,  8,  6,  3
				   ,  8,  9,  6
				   ,  0, 10,  7
				   , 11,  9,  8
				   , 11, 12,  9
				   ,  0, 13, 10
				   , 14, 12, 11
				   , 14, 15, 12
				   ,  0, 16, 13
				   , 17, 15, 14
				   , 17, 18, 15
				   ,  0, 19, 16
				   , 20, 18, 17
				   , 20, 21, 18
				   ,  0, 22, 19
				   , 23, 21, 20
				   , 23, 24, 21
				   ,  0,  2, 22
				   ,  5, 24, 23
				   ,  5,  4, 24
				   , 25, 26, 27
				   , 28, 29, 30
				   , 31, 32, 33
				   , 34, 35, 36
				   , 37, 38, 39
				   , 40, 41, 42
				   , 43, 44, 45
				   , 46, 47, 48
				   , 49, 50, 51
				   , 52, 53, 54
				   , 55, 56, 57
				   , 58, 59, 60
				 }
			   , new Color4[]
				 {
					 new Color4( 64,  64, 64, 255)
				   , new Color4( 64,  64, 64, 255)
				   , new Color4( 64,  64, 64, 255)
				   , new Color4(255, 163,  0, 255)
				   , new Color4(255, 163,  0, 255)
				   , new Color4(255, 163,  0, 255)
				   , new Color4(255, 163,  0, 255)
				   , new Color4( 64,  64, 64, 255)
				   , new Color4(255, 163,  0, 255)
				   , new Color4(255, 163,  0, 255)
				   , new Color4( 64,  64, 64, 255)
				   , new Color4(255, 163,  0, 255)
				   , new Color4(255, 163,  0, 255)
				   , new Color4( 64,  64, 64, 255)
				   , new Color4(255, 163,  0, 255)
				   , new Color4(255, 163,  0, 255)
				   , new Color4( 64,  64, 64, 255)
				   , new Color4(255, 163,  0, 255)
				   , new Color4(255, 163,  0, 255)
				   , new Color4( 64,  64, 64, 255)
				   , new Color4(255, 163,  0, 255)
				   , new Color4(255, 163,  0, 255)
				   , new Color4( 64,  64, 64, 255)
				   , new Color4(255, 163,  0, 255)
				   , new Color4(255, 163,  0, 255)
				   , prc // Rest of mesh uses the 'Paint Replace Color'
				   , prc
				   , prc
				   , prc
				   , prc
				   , prc
				   , prc
				   , prc
				   , prc
				   , prc
				   , prc
				   , prc
				   , prc
				   , prc
				   , prc
				   , prc
				   , prc
				   , prc
				   , prc
				   , prc
				   , prc
				   , prc
				   , prc
				   , prc
				   , prc
				   , prc
				   , prc
				   , prc
				   , prc
				   , prc
				   , prc
				   , prc
				   , prc
				   , prc
				   , prc
				   , prc
				 }
				);


			#endregion Pipes

			// Todo: Why is this even a separate shape?
			MkEntry(Shape.Square_Static, flat: true, new[] {
				        new Vector3(a, b, a),
				        new Vector3(a, a, a),
				        new Vector3(a, a, b),
				        new Vector3(a, b, b)
			        }
			      , new UInt32[] { 0, 1, 2, 2, 3, 0 }
			       );

			MkEntry
				(
				 Shape.Square_Weight, flat: true
			   ,                      new[]
				 {
					 new Vector3(0.125000f,  0.125000f,  0.125000f)
				   , new Vector3(0.125000f,  0.125000f, -0.125000f)
				   , new Vector3(0.125000f,  0.041667f,  0.000000f)
				   , new Vector3(0.125000f,  0.000000f,  0.041667f)
				   , new Vector3(0.125000f, -0.125000f, -0.125000f)
				   , new Vector3(0.125000f,  0.000000f, -0.041667f)
				   , new Vector3(0.125000f, -0.125000f,  0.125000f)
				   , new Vector3(0.125000f, -0.041667f,  0.000000f)
				   , new Vector3(0.125000f,  0.000000f,  0.041667f)
				   , new Vector3(0.125000f,  0.041667f,  0.000000f)
				   , new Vector3(0.125000f,  0.000000f, -0.041667f)
				   , new Vector3(0.125000f, -0.041667f,  0.000000f)
					,
				 }
			   , new UInt32[]
				 {
					  0,  1,  2
				   ,  2,  3,  0
				   ,  1,  4,  5
				   ,  5,  2,  1
				   ,  4,  6,  7
				   ,  7,  5,  4
				   ,  6,  0,  3
				   ,  3,  7,  6
				   ,  8,  9, 10
				   , 10, 11,  8
				 }
			   , new Color4[]
				 {
					 prc
				   , prc
				   , prc
				   , prc
				   , prc
				   , prc
				   , prc
				   , prc
				   , new Color4(150f, 150f, 150f, 255)
				   , new Color4(150f, 150f, 150f, 255)
				   , new Color4(150f, 150f, 150f, 255)
				   , new Color4(150f, 150f, 150f, 255)
					,
				 }
				);

			MkEntry(Shape.Wedge, flat: false, new[] {
					 new Vector3(a, b, a),
					 new Vector3(b, a, a),
					 new Vector3(b, a, b),
					 new Vector3(a, b, b)
				 }
			   , new UInt32[6] { 0, 1, 2, 2, 3, 0 }
			      , rotate: -1
				);

			MkEntry(Shape.Pyramid /* 7 */, flat: false, new[] {
					new Vector3(b, a, a),
					new Vector3(b, b, b),
					new Vector3(a, b, a)
				}, rotate: -1
				);

			MkEntry(Shape.Pyramid_Inv /* 8 */, flat: false, new[] {
					new Vector3(b, a, b),
					new Vector3(a, b, b),
					new Vector3(a, a, a)
				}, rotate: -1);
			#endregion 1x1 Shapes


			#region 1x2 Triangle
			MkEntry(Shape.Triangle_a_1x2_Top /* 9 */, flat: true, new[] {
					 new Vector3(a, a, a), // Slope Top
					 new Vector3(a, a, b), // 90 Corner
					 new Vector3(a, b, b), // Pointy Halfway bottom
					 new Vector3(a, b, c)  // Pointy Halfway Top
				 }
			   , new UInt32[6] { 0, 1, 2, 2, 3, 0 }
				);
			MkEntry(Shape.Triangle_a_1x2_Bottom /* 10 */, flat: true, new[] {
					 new Vector3(a, a, c), // Slope Top
					 new Vector3(a, a, b), // 90 corner
					 new Vector3(a, b, b) // Slope End
				 }
				);
			MkMirror(Shape.Triangle_b_1x2_Top /* 11 */,    Shape.Triangle_a_1x2_Top /* 9 */,     2, rotate: -2);
			MkMirror(Shape.Triangle_b_1x2_Bottom /* 12 */, Shape.Triangle_a_1x2_Bottom /* 10 */, 2, rotate: -2);
			#endregion 1x2 Triangle


			#region 1x4 Triangle
			MkEntry(Shape.Triangle_a_1x4_Top /* 13 */, flat: true, new[] {
					 new Vector3(a, a, a), // Slope Top
					 new Vector3(a, a, b), // 90 Corner
					 new Vector3(a, b, b), // Pointy Halfway bottom
					 new Vector3(a, b, d)  // Pointy Halfway Top
				 }
			   , new UInt32[6] { 0, 1, 2, 2, 3, 0 }
				);
			MkEntry(Shape.Triangle_a_1x4_MidTop /* 14 */, flat: true, new[] {
					 new Vector3(a, a, d), // Slope Top
					 new Vector3(a, a, b), // 90 Corner
					 new Vector3(a, b, b), // Pointy Halfway bottom
					 new Vector3(a, b, c)  // Pointy Halfway Top
				 }
			   , new UInt32[6] { 0, 1, 2, 2, 3, 0 }
				);
			MkEntry(Shape.Triangle_a_1x4_MidBottom /* 15 */, flat: true, new[] {
					 new Vector3(a, a, c), // Slope Top
					 new Vector3(a, a, b), // 90 Corner
					 new Vector3(a, b, b), // Pointy Halfway bottom
					 new Vector3(a, b, e)  // Pointy Halfway Top
				 }
			   , new UInt32[6] { 0, 1, 2, 2, 3, 0 }
				);
			MkEntry(Shape.Triangle_a_1x4_Bottom /* 16 */, flat: true, new[] {
					 new Vector3(a, a, e), // Slope Top
					 new Vector3(a, a, b), // 90 corner
					 new Vector3(a, b, b) // Slope End
				 }
				);
			MkMirror(Shape.Triangle_b_1x4_Top /* 17 */,       Shape.Triangle_a_1x4_Top /* 13 */,       2, rotate: -2);
			MkMirror(Shape.Triangle_b_1x4_MidTop /* 18 */,    Shape.Triangle_a_1x4_MidTop /* 14 */,    2, rotate: -2);
			MkMirror(Shape.Triangle_b_1x4_MidBottom /* 19 */, Shape.Triangle_a_1x4_MidBottom /* 15 */, 2, rotate: -2);
			MkMirror(Shape.Triangle_b_1x4_Bottom /* 20 */,    Shape.Triangle_a_1x4_Bottom /* 16 */,    2, rotate: -2);
			#endregion 1x4 Triangle

			#region 1x2 Wedge
			MkEntry(Shape.Wedge_1x2_Top /* 21 */, flat: false, new[] {
				        new Vector3(a, c, a), // Bottom
				        new Vector3(b, a, a), // Top
				        new Vector3(b, a, b), // Top
				        new Vector3(a, c, b)  // Bottom
			        }
			      , new UInt32[6] { 0, 1, 2, 2, 3, 0 }
			      , rotate: -1
				   );
			MkEntry(Shape.Wedge_1x2_Bottom /* 22 */, flat: false, new[] {
				        new Vector3(a, b, a),
				        new Vector3(b, c, a),
				        new Vector3(b, c, b),
				        new Vector3(a, b, b)
			        }
			      , new UInt32[6] { 0, 1, 2, 2, 3, 0 }
			      , rotate: -1
				   );
			#endregion 1x2 Wedge

			#region 1x4 Wedge
			MkEntry(Shape.Wedge_1x4_Top /* 23 */, flat: false, new[] {
					 new Vector3(a, d, a), // Bottom
					 new Vector3(b, a, a), // Top
					 new Vector3(b, a, b), // Top
					 new Vector3(a, d, b)  // Bottom
				 }
			   , new UInt32[6] { 0, 1, 2, 2, 3, 0 }
			      , rotate: -1
				);
			MkEntry(Shape.Wedge_1x4_MidTop /* 24 */, flat: false, new[] {
					 new Vector3(a, c, a), // Bottom
					 new Vector3(b, d, a), // Top
					 new Vector3(b, d, b), // Top
					 new Vector3(a, c, b)  // Bottom
				 }
			   , new UInt32[6] { 0, 1, 2, 2, 3, 0 }
			      , rotate: -1
				);
			MkEntry(Shape.Wedge_1x4_MidBottom /* 25 */, flat: false, new[] {
					 new Vector3(a, e, a), // Bottom
					 new Vector3(b, c, a), // Top
					 new Vector3(b, c, b), // Top
					 new Vector3(a, e, b)  // Bottom
				 }
			   , new UInt32[6] { 0, 1, 2, 2, 3, 0 }
			      , rotate: -1
				);
			MkEntry(Shape.Wedge_1x4_Bottom /* 26 */, flat: false, new[] {
					 new Vector3(a, b, a),
					 new Vector3(b, e, a),
					 new Vector3(b, e, b),
					 new Vector3(a, b, b)
				 }
			   , new UInt32[6] { 0, 1, 2, 2, 3, 0 }
			      , rotate: -1
				);
			#endregion 1x4 Wedge

			#region 1x2 Pyramid
			MkEntry(Shape.Pyramid_1x2_Top /* 27 */, flat: false, new[] {
				        new Vector3(a, c, a), // Slope Mid Top
				        new Vector3(a, b, c), // Slope Mid Bottom
				        new Vector3(b, b, b), // Slope End Top
				        new Vector3(b, a, a)  // Slope End Bottom
			        }
			      , new UInt32[6] { 2, 1, 0, 0, 3, 2 }
			      , rotate: -1
				   );
			MkEntry(Shape.Pyramid_1x2_Bottom /* 28 */, flat: false, new[] {
				        new Vector3(a, b, a), // Pointy end
				        new Vector3(b, c, a), // Slope Mid Top
				        new Vector3(b, b, c), // Slope Mid Bottom
			        }
			      , rotate: -1
				   );
			#endregion 1x2 Pyramid

			#region 1x4 Pyramid
			MkEntry(Shape.Pyramid_1x4_Top /* 29 */, flat: false, new[] {
					 new Vector3(a, d, a), // Slope Mid Top
					 new Vector3(a, b, e), // Slope Mid Bottom
					 new Vector3(b, b, b), // Slope End Top
					 new Vector3(b, a, a)  // Slope End Bottom
				 }
			   , new UInt32[6] { 2, 1, 0, 0, 3, 2 }
			      , rotate: -1
				);
			MkEntry(Shape.Pyramid_1x4_MidTop /* 30 */, flat: false, new[] {
					 new Vector3(a, c, a), // Slope Mid Top
					 new Vector3(a, b, c), // Slope Mid Bottom
					 new Vector3(b, b, e), // Slope End Top
					 new Vector3(b, d, a)  // Slope End Bottom
				 }
			   , new UInt32[6] { 2, 1, 0, 0, 3, 2 }
			      , rotate: -1
				);
			MkEntry(Shape.Pyramid_1x4_MidBottom /* 31 */, flat: false, new[] {
					 new Vector3(a, e, a), // Slope Mid Top
					 new Vector3(a, b, d), // Slope Mid Bottom
					 new Vector3(b, c, a), // Slope End Top
					 new Vector3(b, b, c)  // Slope End Bottom
				 }
			   , new UInt32[6] { 2, 1, 0, 2, 3, 1 }
			      , rotate: -1
				);
			MkEntry(Shape.Pyramid_1x4_Bottom /* 32 */, flat: false, new[] {
					 new Vector3(a, b, a), // Pointy end
					 new Vector3(b, e, a), // Slope Mid Top
					 new Vector3(b, b, d), // Slope Mid Bottom
				 }
			      , rotate: -1
				);
			#endregion 1x4 Pyramid

			#region 2x2 Pyramid
			MkEntry(Shape.Pyramid_2x2_Top /* 33 */, flat: false, new[] {
				        //           Grn  Red  Blu
				        new Vector3(a, b, a), // Top
				        new Vector3(c, a, a), // Left
				        new Vector3(c, b, b), // Right
				        new Vector3(b, a, b)  // Bottom
			        }
			      , new UInt32[6] { 0, 1, 2, 2, 1, 3 }
				   );
			MkEntry(Shape.Pyramid_2x2_Edge /* 34 */, flat: false, new[] {
				        //           Grn  Red  Blu
				        new Vector3(c, b, a), // Top
				        new Vector3(b, a, a), // Left
				        new Vector3(b, b, b), // Right
			        }
				   );
			#endregion 2x2 Pyramid

			#region 2x4 Pyramid
			MkEntry(Shape.Pyramid_2x4_Top /* 35 */, flat: false, new[] {
				        //            Grn   -Red    Blu
				        new Vector3(top, bot, top), // Top
				        new Vector3(hlf, top, top), // EdgeBot
				        new Vector3(tph, bot, bot), // EdgeTop
				        new Vector3(bth, top, bot)  // Bottom
			        }
			      , new UInt32[6] { 0, 1, 2, 2, 1, 3 }
				   );
			MkEntry(Shape.Pyramid_2x4_MidTop /* 36 */, flat: false, new[] {
				        //            Grn   -Red    Blu
				        new Vector3(tph, bot, top), // Top
				        new Vector3(bth, top, top), // EdgeBot
				        new Vector3(hlf, bot, bot), // EdgeTop
				        new Vector3(bot, top, bot)  // Bottom
			        }
			      , new UInt32[6] { 0, 1, 2, 2, 1, 3 }
				   );
			MkEntry(Shape.Pyramid_2x4_MidBottom /* 37 */, flat: false, new[] {
				        //            Grn   -Red    Blu
				        new Vector3(hlf, bot, top), // Top
				        new Vector3(bot, top, top), // EdgeBot
				        new Vector3(bth, bot, bot), // EdgeTop
				        new Vector3(bot, hlf, bot)  // Bottom
			        }
			      , new UInt32[6] { 0, 1, 2, 2, 1, 3 }
				   );
			MkEntry(Shape.Pyramid_2x4_End /* 38 */, flat: false, new[] {
				        //            Grn   -Red    Blu
				        new Vector3(bth, bot, top), // Top
				        new Vector3(bot, hlf, top), // Edge
				        new Vector3(bot, bot, bot), // Tip/End
			        }
				   );
			#endregion 2x4 Pyramid

			// 39..42 not used?

			#region 4x4 Pyramid
			MkEntry(Shape.Pyramid_4x4_Top /* 43 */, flat: false, new[] {
				        //            Red    Grn    Blu
				        new Vector3(top, bot, top), // Top
				        new Vector3(tph, top, top), // Left
				        new Vector3(tph, bot, bot), // Right
				        new Vector3(hlf, top, bot)  // Bottom
			        }
			      , new UInt32[6] { 0, 1, 2, 2, 1, 3 }
				   );
			MkEntry(Shape.Pyramid_4x4_MidTop /* 44 */, flat: false, new[] {
						//            Red    Grn    Blu
						new Vector3(tph, bot, top), // Top
						new Vector3(hlf, top, top), // Left
						new Vector3(hlf, bot, bot), // Right
						new Vector3(bth, top, bot)  // Bottom
					}
			      , new UInt32[6] { 0, 1, 2, 2, 1, 3 }
				   );
			MkEntry(Shape.Pyramid_4x4_MidBottom /* 45 */, flat: false, new[] {
						//            Red    Grn    Blu
						new Vector3(hlf, bot, top), // Top
						new Vector3(bth, top, top), // Left
						new Vector3(bth, bot, bot), // Right
						new Vector3(bot, top, bot)  // Bottom
					}
			      , new UInt32[6] { 0, 1, 2, 2, 1, 3 }
				   );
			MkEntry(Shape.Pyramid_4x4_End /* 46 */, flat: false, new[] {
				        //            Red    Grn    Blu
				        new Vector3(bth, bot, top), // Top
				        new Vector3(bot, top, top), // Left
				        new Vector3(bot, bot, bot), // Right
			        }
				   );
			#endregion 4x4 Pyramid

			#region 1x2 Inv_Pyramid
			MkEntry(Shape.Pyramid_Inv_1x2_Triangle /* 47 */, flat: false, new[] {
				        //           -Blu    
				        new Vector3(bot, top, bot), // Pointy end
				        new Vector3(top, top, hlf), // Slope Mid Top
				        new Vector3(top, hlf, bot), // Slope Mid Bottom
			        }, new UInt32[] { 2, 1, 0 }
			      , rotate: -1
				   );
			MkEntry(Shape.Pyramid_Inv_1x2_Trapezoid /* 48 */, flat: false, new[] {
				        //           -Blu   
				        new Vector3(bot, top, hlf), // Slope Mid Top
				        new Vector3(bot, hlf, bot), // Slope Mid Bottom
				        new Vector3(top, bot, bot), // Slope End Top
				        new Vector3(top, top, top)  // Slope End Bottom
			        }, new UInt32[] { 0, 1, 2, 2, 3, 0 }
			      , rotate: -1
				   );
			#endregion 1x2 Inv_Pyramid

			#region 1x4 Inv_Pyramid
			MkEntry(Shape.Pyramid_Inv_1x4_Point /* 49 */, flat: false, new[] {
				        //           -Blu   +Grn   +Red
				        new Vector3(bot, top, bot), // Point
				        new Vector3(top, top, bth), // Edge Green
				        new Vector3(top, tph, bot), // Edge -Red
			        }, new UInt32[3] { 2, 1, 0 }
			      , rotate: -1
				   );
			MkEntry(Shape.Pyramid_Inv_1x4_MidPoint /* 50 */, flat: false, new[] {
				        //           -Blu   +Grn   +Red
				        new Vector3(bot, top, bth), // Pointy Grn
				        new Vector3(bot, tph, bot), // Pointy -Red
				        new Vector3(top, top, hlf), // Edge Grn
				        new Vector3(top, hlf, bot)  // Edge -Red
			        }, new UInt32[6] { 0, 1, 2, 2, 1, 3 }
			      , rotate: -1
				   );
			MkEntry(Shape.Pyramid_Inv_1x4_MidEdge /* 51 */, flat: false, new[] {
				        //           -Blu   +Grn   +Red
				        new Vector3(bot, top, hlf), // Pointy Grn
				        new Vector3(bot, hlf, bot), // Pointy -Red
				        new Vector3(top, top, tph), // Edge Grn
				        new Vector3(top, bth, bot)  // Edge -Red
			        }, new UInt32[6] { 0, 1, 2, 2, 1, 3 }
			      , rotate: -1
				   );
			MkEntry(Shape.Pyramid_Inv_1x4_Edge /* 52 */, flat: false, new[] {
				        //           -Blu   +Grn   +Red
				        new Vector3(bot, top, tph), // Pointy Grn
				        new Vector3(bot, bth, bot), // Pointy -Red
				        new Vector3(top, top, top), // Edge Grn
				        new Vector3(top, bot, bot)  // Edge -Red
			        }, new UInt32[6] { 0, 1, 2, 2, 1, 3 }
			      , rotate: -1
				   );
			#endregion 1x4 Inv_Pyramid

			#region 2x2 Inv_Pyramid
			MkEntry(Shape.Pyramid_Inv_2x2_Point /* 53 */, flat: false, new[] {
				        //           +Grn   -Red   +Blu
				        new Vector3(bot, top, bot), // Point
				        new Vector3(hlf, bot, bot), // Edge Green
				        new Vector3(hlf, top, top), // Edge -Red
				        new Vector3(top, bot, top), // Top
			        }, new UInt32[6] { 0, 1, 2, 2, 1, 3 }
				   );
			MkEntry(Shape.Pyramid_Inv_2x2_Edge /* 54 */, flat: false, new[] {
				        //           +Grn   -Red   +Blu
				        new Vector3(hlf, top, bot), // Pointy
				        new Vector3(top, bot, bot), // Edge Grn
				        new Vector3(top, top, top)  // Edge -Red
			        }, new UInt32[3] { 0, 1, 2 }
				   );
			#endregion 2x2 Inv_Pyramid

			#region 2x4 Inv_Pyramid
			MkEntry(Shape.Pyramid_Inv_2x4_Point /* 55 */, flat: false, new[] {
				        //           +Grn   -Red   +Blu
				        new Vector3(bot, top, bot), // Point (Bottom)
				        new Vector3(hlf, bot, bot), // Edge High
				        new Vector3(bth, top, top), // Edge Low
				        new Vector3(tph, bot, top), // Top
			        }, new UInt32[6] { 0, 1, 2, 2, 1, 3 }
				   );
			MkEntry(Shape.Pyramid_Inv_2x4_TopPoint /* 56 */, flat: false, new[] {
				        //           +Grn   -Red   +Blu
				        new Vector3(bth, top, bot), // Point (Bottom)
				        new Vector3(tph, bot, bot), // Edge High
				        new Vector3(hlf, top, top), // Edge Low
				        new Vector3(top, bot, top), // Top
			        }, new UInt32[6] { 0, 1, 2, 2, 1, 3 }
				   );
			MkEntry(Shape.Pyramid_Inv_2x4_TopEdge /* 57 */, flat: false, new[] {
				        //           +Grn   -Red   +Blu
				        new Vector3(hlf, top, bot), // Point (Bottom)
				        new Vector3(top, bot, bot), // Edge High
				        new Vector3(tph, top, top), // Edge Low
				        new Vector3(top, hlf, top), // Top
			        }, new UInt32[6] { 0, 1, 2, 2, 1, 3 }
				   );
			MkEntry(Shape.Pyramid_Inv_2x4_MidEdge /* 58 */, flat: false, new[] {
						//           +Grn   -Red   +Blu
						new Vector3(top, top, top), // Top (Pointy)
						new Vector3(top, hlf, bot), // Edge High
						new Vector3(tph, top, bot)  // Edge Low
				}, new UInt32[3] { 2, 1, 0 }
				   );
			#endregion 2x4 Inv_Pyramid

			// 59..62 not used?

			#region 4x4 Inv_Pyramid
			MkEntry(Shape.Pyramid_Inv_4x4_BotPoint /* 63 */, flat: false, new[] {
				        //           +Grn   -Red   +Blu
				        new Vector3(bot, top, bot), // Point (Bottom)
				        new Vector3(bth, bot, bot), // Edge High
				        new Vector3(bth, top, top), // Edge Low
				        new Vector3(hlf, bot, top), // Top
			        }, new UInt32[6] { 0, 1, 2, 2, 1, 3 }
				   );
			MkEntry(Shape.Pyramid_Inv_4x4_MidPoint /* 64 */, flat: false, new[] {
				        //           +Grn   -Red   +Blu
				        new Vector3(bth, top, bot), // Point (Bottom)
				        new Vector3(hlf, bot, bot), // Edge High
				        new Vector3(hlf, top, top), // Edge Low
				        new Vector3(tph, bot, top), // Top
			        }, new UInt32[6] { 0, 1, 2, 2, 1, 3 }
				   );
			MkEntry(Shape.Pyramid_Inv_4x4_MidEdge /* 65 */, flat: false, new[] {
				        //           +Grn   -Red   +Blu
				        new Vector3(hlf, top, bot), // Point (Bottom)
				        new Vector3(tph, bot, bot), // Edge High
				        new Vector3(tph, top, top), // Edge Low
				        new Vector3(top, bot, top), // Top
			        }, new UInt32[6] { 0, 1, 2, 2, 1, 3 }
				   );
			MkEntry(Shape.Pyramid_Inv_4x4_TopEdge /* 66 */, flat: false, new[] {
				        //           +Grn   -Red   +Blu
				        new Vector3(top, top, top), // Edge
				        new Vector3(top, bot, bot), // Edge
				        new Vector3(tph, top, bot)  // Bottom
			        }, new UInt32[3] { 2, 1, 0 }
				   );
			#endregion 2x4 Inv_Pyramid

			#endregion MkEntry

			AfterMakingEntries();
			return result;
		}

		private static Dictionary<(Orientation, byte), Matrix4> InitOrientationRotationTransforms()
		{
			var result = new Dictionary<(Orientation, byte), Matrix4>();

			for (byte rotation = 0; rotation <= 3; rotation++)
			{
				for (byte orientation = 0; orientation <= 5; orientation++)
				{
					result[((Orientation) orientation, rotation)] = CreateSurfaceTransform(orientation, rotation);
				}
			}

			return result;
		}

		private static (Vector3, Orientation)[] vectorToOrientation = new (Vector3, Orientation)[]
		{
			(  Vector3.UnitX, Orientation.XPositive )
		  , ( -Vector3.UnitX, Orientation.XNegative )
		  , (  Vector3.UnitY, Orientation.YPositive )
		  , ( -Vector3.UnitY, Orientation.YNegative )
		  , (  Vector3.UnitZ, Orientation.ZPositive )
		  , ( -Vector3.UnitZ, Orientation.ZNegative )
		};


		public static Orientation DirectionToOrientation(Vector3 v)
		{
			v.NormalizeFast();

			Orientation bestOrientation = Orientation.XPositive;
			float bestDistance = float.PositiveInfinity;

			foreach ((Vector3 refVec, Orientation o) in vectorToOrientation)
			{
				var distVec = refVec - v;
				float dist = distVec.LengthFast;

				if (dist < bestDistance)
				{
					bestDistance = dist;
					bestOrientation = o;
				}
			}

			return bestOrientation;
		}

		public static Vector3 OrientationToVector3(Orientation o)
		{
			int i = (int)o;
			return vectorToOrientation[i].Item1;
		}
		public static Vector3i OrientationToVector3i(Orientation o)
		{
			int i = (int)o;
			var floatVec = vectorToOrientation[i].Item1;
			return new Vector3i((int)floatVec.X, (int)floatVec.Y, (int)floatVec.Z);
		}



		private static Matrix4 CreateSurfaceTransform(byte orientation, byte rotation)
		{
			/*
				Orientations (In-Game Vehicle Editor Directions, Axes and colors):
				0 = +x Right, Red
				1 = -X Left todo: (wrong orientation by 180)
				
				2 = +Y Up, Green
				3 = -Y Down
				
				4 = +Z Forward, Blue
				5 = -Z Backward
			*/

			const float Deg2Rad = (float) Math.PI / 180;

			// Do we need to rotate the reference mesh to be in the correct orientation?
			bool needsChangeOrientation;
			Vector3 changeOrientationAxis;

			switch (orientation)
			{
				case 0: // +X
				case 1: // -X
					needsChangeOrientation = false;
					changeOrientationAxis = Vector3.UnitY;
					break;
				case 2: // +Y
				case 3: // -Y
					needsChangeOrientation = true;
					changeOrientationAxis = Vector3.UnitZ;
					break;
				case 4: // +Z
				case 5: // -Z
					needsChangeOrientation = true;
					changeOrientationAxis = Vector3.UnitY;
					break;
				default: throw new ArgumentOutOfRangeException(nameof(orientation), "[0..5]");
			}



			// Rotate to face correct direction (radians)
			float orientationAmount = 0;
			if (needsChangeOrientation)
			{
				orientationAmount += 90f * Deg2Rad;
			}
			if (orientation % 2 != 0)
			{
				needsChangeOrientation = true;
				orientationAmount += 180f * Deg2Rad;
			}
			if (orientation == 2)
			{
				needsChangeOrientation = true;
				orientationAmount = -90f * Deg2Rad;
			}
			if (orientation == 3)
			{
				orientationAmount += 180f * Deg2Rad;
			}

			Matrix4 orientationTransform = Matrix4.Identity;
			if (needsChangeOrientation)
			{
				orientationTransform = Matrix4.CreateFromAxisAngle(changeOrientationAxis, orientationAmount);
			}

			///////////////////////////////////////////////////////////////////
			// Face rotation
			Matrix4 faceRotationTransform = Matrix4.Identity;
			float faceRotationAmount = 0;
			if (rotation > 0)
			{
				faceRotationAmount = rotation * -90f * Deg2Rad;
			}

			#region Special cases
			// Special cases verified.
			if (orientation == 0) // +X
				faceRotationAmount += 00f * Deg2Rad;
			else if (orientation == 1) // -X
				faceRotationAmount += 180f * Deg2Rad;
			else if (orientation == 2) // +Y
				faceRotationAmount += 0f * Deg2Rad;
			else if (orientation == 3) // -Y
				faceRotationAmount += 0f * Deg2Rad;
			else if (orientation == 4) // +Z
				faceRotationAmount -= 90f * Deg2Rad;
			else if (orientation == 5) // -Z
				faceRotationAmount += 90f * Deg2Rad;
			#endregion Special cases


			if (faceRotationAmount != 0)
			{
				faceRotationTransform = Matrix4.CreateFromAxisAngle(Vector3.UnitX, faceRotationAmount);
			}

			Matrix4 result = orientationTransform * faceRotationTransform;
			return result;
		}


		#endregion Static
	}
}
