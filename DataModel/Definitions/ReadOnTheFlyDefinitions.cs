﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace DataModel.Definitions
{
	/// <summary>
	/// Definitions Dictionary that reads definitions on the fly as they are requested rather than upfront.
	/// </summary>
	internal class ReadOnTheFlyDefinitions : IReadOnlyDictionary<string, Definition>
	{
		private readonly string folderPath;
		internal readonly Dictionary<string, Definition> definitions = new Dictionary<string, Definition>();


		internal ReadOnTheFlyDefinitions(string folderPath)
		{
			this.folderPath = folderPath;
		}

		private Definition Load(string name)
		{
			// todo: handle exceptions from parse, currently it just explodes in the caller's face.
			// Perhaps add a record to the Dict that stores the definition or the exception.

			var path = Path.Combine(folderPath, name + ".xml");
			if (File.Exists(path))
			{
				return Definition.Parse(path);
			}
			else
			{
				return null;
			}
		}

		#region Dictionary Implementation
		/// <inheritdoc />
		public IEnumerator<KeyValuePair<string, Definition>> GetEnumerator()
		{
			return definitions.GetEnumerator();
		}

		/// <inheritdoc />
		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		/// <inheritdoc />
		public int Count => definitions.Count;

		/// <inheritdoc />
		public bool ContainsKey(string key)
		{
			return TryGetValue(key, out _);
		}

		/// <inheritdoc />
		public bool TryGetValue(string key, out Definition value)
		{
			if (definitions.TryGetValue(key, out value)) return null != value;

			// Try to load the Definition
			lock (definitions)
			{
				// Re-Check after aquireing the lock.
				if (definitions.TryGetValue(key, out value)) return null != value;

				// Not loaded before, do so now.
				value = Load(key);

				// Store null if not found, to signal non-existance. (Don't check file system again each time).
				definitions.Add(key, value);

				return null != value;
			}
		}

		/// <inheritdoc />
		public Definition this[string key]
		{
			get
			{
				if (TryGetValue(key, out var definition)) return definition;

				throw new KeyNotFoundException(key);
			}
		}

		/// <inheritdoc />
		public IEnumerable<string> Keys => definitions.Keys;

		/// <inheritdoc />
		public IEnumerable<Definition> Values => definitions.Values;

		#endregion Dictionary Implementation
	}
}
