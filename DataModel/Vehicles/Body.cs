﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Xml.Linq;

using BinaryDataModel.DataTypes;

using OpenToolkit.Mathematics;

using Shared;
using Shared.Exceptions;
using Shared.Serialization;
using Shared.Serialization.DataTypes;


namespace DataModel.Vehicles
{
	/// <summary>
	/// Represents a physics body of a <see cref="Vehicles.Vehicle"/>.
	/// </summary>
	public class Body
	{
		public Vehicle Vehicle;

		public int unique_id;
		public Matrix4 initial_transform = Matrix4.Identity;  // Todo: Integers only?
		public Matrix4 local_transform = Matrix4.Identity; // Todo: Integers only?

		public List<Component> components = new List<Component>();


		#region Meta

		/// <summary>
		/// The order of this element in the parent's list of elements.
		/// It is set automatically when loading from file.
		/// </summary>
		public int? ElementOrder { get; set; } = null;

		#endregion Meta


		/////////////////////////////////////////////////////////////////////////////
		// End of xml data



		public float mass { get; private set; }


		public Matrix4 final_transform;


		public readonly Lazy<Mesh> _mesh;

		/// <summary>
		/// <see cref="Mesh"/> for this <see cref="Body"/>.
		/// Will be generated exactly once if does not exist using <see cref="Lazy{Mesh}"/>.
		/// Can be <see langword="null"/> if a failure occurred,
		/// in that case you can find the <see cref="Exception"/> in <see cref="meshGenerationException"/>.
		/// </summary>
		public Mesh mesh => _mesh.Value;

		/// <summary>
		/// If an <see cref="Exception"/> occurred during <see cref="Mesh"/> generation it will be stored
		/// here for reference, because by default it is swallowed by <see cref="SafeGenerateMesh"/>.
		/// </summary>
		public Exception meshGenerationException { get; private set; }

		/// <summary>
		/// Generate the <see cref="Mesh"/> for this <see cref="Body"/>.
		/// Note: This is independent of the property <see cref="mesh"/>.
		/// </summary>
		/// <returns>A new <see cref="Mesh"/></returns>
		public Mesh GenerateMesh()
		{
			meshGenerationException = null;
			try
			{
				Mesh m = MeshGenerator.GenerateMesh(this);
				m.fileName = $"<Generated: Vehicle '{Vehicle.fileName}' body#{unique_id}>";

				foreach (SubMesh mSubMesh in m.subMeshes)
				{
					Vehicle.meshBounds.Inflate(mSubMesh.bounds.Min);
					Vehicle.meshBounds.Inflate(mSubMesh.bounds.Max);
				}

				return m;
			}
			catch (Exception e)
			{
				var f = new AggregateException
					($"Failed to generate Mesh for Vehicle '{Vehicle.meta_filePath ?? "unknown"}' Body#{unique_id}", e);
				meshGenerationException = f;
				throw f;
			}
		}

		/// <summary>
		/// Calls <see cref="GenerateMesh"/> but swallows and logs any <see cref="Exception"/> that may occur.
		/// </summary>
		/// <returns></returns>
		public Mesh SafeGenerateMesh()
		{
			try
			{
				return GenerateMesh();
			}
			catch (Exception e)
			{
				Console.WriteLine($"[{nameof(Body)}]/SafeGenerateMesh: {e}");
				return null;
			}
		}

		// todo: BuoyancyMesh

		private Body()
		{
			_mesh = new Lazy<Mesh>(SafeGenerateMesh, LazyThreadSafetyMode.ExecutionAndPublication);
		}

		public Body(Vehicle parent) : this()
		{
			Vehicle = parent ?? throw new ArgumentNullException(nameof(parent));
			unique_id = Vehicle.NextBodyID;
			parent.bodies.Add(this);
		}

		public Body(Vehicle parent, XElement bodyElement) : this(parent)
		{
			try
			{
				if (null == bodyElement) throw new ArgumentNullException(nameof(bodyElement));

				var attributes = bodyElement.Attributes().XAttributeToDictionary();

				unique_id = attributes.GetInt32("unique_id", 0);

				initial_transform = MatrixSer.ParseMatrix4(bodyElement.Element("initial_local_transform"), allowNull: true);
				local_transform = MatrixSer.ParseMatrix4(bodyElement.Element("local_transform"), allowNull: true);

				final_transform = local_transform * initial_transform;

				ParseComponents(bodyElement);
			}
			catch (Exception e)
			{
				throw XmlDataException.AutoFormatDataName("Body", bodyElement, e);
			}
		}

		private void ParseComponents(XElement bodyElement)
		{
			try
			{
				var componentsElement = bodyElement.Element("components");

				int order = 0;

				foreach (XElement element in componentsElement.Elements())
				{
					var c = Component.Parse(this, element);
					c.ElementOrder = order++;

					if (null != c.Definition)
					{
						mass += c.Definition.mass;
					}
				}
			}
			catch (Exception e)
			{
				throw XmlDataException.AutoFormatDataName("Components", bodyElement, e);
			}
		}

		public IEnumerable<(Vector3i, Component)> GetVoxels()
		{
			foreach (var c in components)
			{
				foreach (var vox in c.GetVoxels())
				{
					// todo: check if this is needed
					//var vox4 = new Vector4(vox.X, vox.Y, vox.Z, 1);
					//var vox4t = vox4 * final_transform;
					//var voxt = new Vector3i((int) vox4t.X, (int) vox4t.Y, (int) vox4t.Z);
					//yield return (voxt, c);
					yield return (vox, c);
				}
			}
		}

		public XElement ToXElement()
		{
			var body = new XElement("body");
			body.SetAttributeValue("unique_id", unique_id);
			if (initial_transform != Matrix4.Identity)
			{
				body.Add(initial_transform.ToXElement("initial_local_transform"));
			}

			if (local_transform != Matrix4.Identity)
			{
				body.Add(local_transform.ToXElement("local_transform"));
			}

			body.Add(ComponentsElement());

			return body;
		}

		private XElement ComponentsElement()
		{
			var container = new XElement("components");
			foreach (Component component in components.OrderBy(c => c.ElementOrder, NullLastComparer<int>.Comparer))
			{
				container.Add(component.ToXElement());
			}

			return container;
		}

		/// <inheritdoc />
		public override string ToString()
		{
			return $"VehicleBody #{unique_id} of '{Vehicle.fileName}' with {components.Count} components.";
		}
	}
}
