// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;
using System.Xml.Serialization;

using BinaryDataModel;
using BinaryDataModel.DataTypes;

using DataModel.Definitions;
using DataModel.Definitions.NestedTypes;
using DataModel.Vehicles.Components;

using OpenToolkit.Mathematics;

using Shared.Exceptions;
using Shared.Serialization;
using Shared.Serialization.DataTypes;

namespace DataModel.Vehicles
{
	/// <summary>
	/// Represents a single Component (a block or device) on a <see cref="Vehicle"/>.
	/// </summary>
	[XmlType(TypeName = "c")]
	public class Component
	{
		/// <summary>
		/// The definition that will be used when it's not specified due to compression.
		/// </summary>
		public const string defaultDefinitionName = "01_block";


		[XmlIgnore]
		public Definition Definition { get; private set; }

		[XmlIgnore]
		public Body Body { get; internal set; }

		[XmlIgnore]
		public Vehicle Vehicle => Body.Vehicle;

		/// <summary>
		/// The parser for colors. This varies by <see cref="Vehicle.data_version"/>.
		/// </summary>
		[XmlIgnore]
		public Vehicle.ColorParserDelegate ColorParser => Vehicle.ColorParser;


		#region Xml Data


		[XmlAttribute(AttributeName = "d")]
		public string definitionName
		{
			get => Definition?.fileName ?? rawDefinitionName;
			set
			{
				rawDefinitionName = value;
				Definition.AssertDefinitionsReady();
				Definition.Definitions.TryGetValue(value, out var definition);
				Definition = definition;
			}
		}

		[XmlIgnore]
		public string rawDefinitionName;


		/// <summary>
		/// Symmetry flags.
		/// </summary>
		[XmlAttribute(AttributeName = "t")]
		public byte t; // Symmetry flags

		// <o>
		public Matrix3 rotation; // Todo: Integers only!

		private uint _bc;
		/// <summary>
		/// Primary paint zone 1
		/// </summary>
		public uint bc
		{
			get => _bc;
			set
			{
				_bc = value;
				paintArray[0] = ColorParser(_bc);
			}
		}

		private uint _bc2;
		/// <summary>
		/// Primary paint zone 2
		/// </summary>
		public uint bc2
		{
			get => _bc2;
			set
			{
				_bc2 = value;
				paintArray[1] = ColorParser(_bc2);
			}
		}

		private uint _bc3;
		/// <summary>
		/// Primary paint zone 3
		/// </summary>
		public uint bc3
		{
			get => _bc3;
			set
			{
				_bc3 = value;
				paintArray[2] = ColorParser(_bc3);
			}
		}


		private uint _ac;
		/// <summary>
		/// Emissive colour.
		/// </summary>
		public uint ac
		{
			get => _ac;
			set
			{
				_ac = value;
				emissiveColorRGBA = ColorParser(_ac);
			}
		}


		private string _sc;
		/// <summary>
		/// Surface colours
		/// </summary>
		public string sc
		{
			get => _sc;
			set
			{
				_sc = value;
				surfaceColours = ParseSurfaceColours(value);
			}
		}


		//[XmlAttribute(AttributeName = "custom_name")]
		public string custom_name;

		//[XmlAttribute(AttributeName = "blade_count")]
		public byte blade_count;

		//[XmlAttribute(AttributeName = "blade_pitch")]
		public byte blade_pitch;

		//[XmlAttribute(AttributeName = "blade_length")]
		public float blade_length;

		/// <summary>
		/// Voxel position
		/// </summary>
		//[XmlElement(ElementName = "vp")]
		public Vector3i position;


		//public List<LogicSlot> logic_slots = new List<LogicSlot>();

		// </o>


		#region Meta

		/// <summary>
		/// The order of this element in the parent's list of elements.
		/// It is set automatically when loading from file.
		/// </summary>
		public int? ElementOrder { get; set; } = null;

		#endregion Meta

		#endregion Xml Data
		#region Derived information

		private static Color4[] InitPaintArray()
		{
			var result = new Color4[MeshCombiner.paintZoneColors.Length + 1 /* emissive is also stored in this array */];
			for (int i = 0; i < result.Length; i++)
			{
				result[i] = Color4.White;
			}

			return result;
		}

		[XmlIgnore]
		public readonly Color4[] paintArray = InitPaintArray();


		[XmlIgnore]
		public Color4 paintColorRGBA
		{
			get => paintArray[0];
			set => paintArray[0] = value;
		}

		[XmlIgnore]
		public Color4 paintColor2RGBA
		{
			get => paintArray[1];
			set => paintArray[1] = value;
		}

		[XmlIgnore]
		public Color4 paintColor3RGBA
		{
			get => paintArray[2];
			set => paintArray[2] = value;
		}

		[XmlIgnore]
		public Color4 emissiveColorRGBA
		{
			get => paintArray[MeshCombiner.paintZoneColors.Length];
			set => paintArray[MeshCombiner.paintZoneColors.Length] = value;
		}

		/// <summary>
		/// Surface Colours
		/// </summary>
		[XmlIgnore]
		public Color4[] surfaceColours;

		//[XmlIgnore]
		//public Matrix4 ComponentTransform;

		private Matrix4 _componentTransform;
		private Matrix4 _voxelTransform;
		private bool _reverseTriangles;

		/// <summary>
		/// Load-Time generated <see cref="Mesh"/>.
		/// </summary>
		public virtual Mesh LoadTimeGeneratedMesh => null;

		/// <summary>
		/// Load-Time generated <see cref="Mesh"/> transform.
		/// </summary>
		public Matrix4 LoadTimeGeneratedMeshTransform = Matrix4.Identity;


		#endregion Derived information

		#region Construction

		/// <summary>
		/// The value of the orientation ('o' element) when no value is specified in the xml.
		/// </summary>
		public static readonly Matrix3 defaultRotation = new Matrix3(0, 0, 1, -1, 0, 0, 0, -1, 0);



		public Component(Body body)
		{
			Body = body ?? throw new ArgumentNullException(nameof(body));

			body.components.Add(this);
		}

		public Component(Body body, XElement componentElement)
			: this(body, componentElement, componentElement.Attributes().XAttributeToDictionary())
		{ }

		public Component(Body body, XElement componentElement, Dictionary<string, string> attributes) : this(body)
		{
			if (null == componentElement) throw new ArgumentNullException(nameof(componentElement));
			try
			{
				if (attributes.TryGetValue("d", out rawDefinitionName))
				{
					definitionName = rawDefinitionName;
				}
				else
				{
					definitionName = defaultDefinitionName;
				}


				if (null == Definition)
				{
					Console.WriteLine($"Unknown component definition: {definitionName}");
				}

				attributes.TryGetByte("t", out t);
				attributes.TryGetValue("custom_name", out custom_name);
				attributes.TryGetByte("blade_count", out blade_count);
				attributes.TryGetByte("blade_pitch", out blade_pitch);
				attributes.TryGetFloat("blade_length", out blade_length);


				// <o>
				var oElement = componentElement.Element("o");
				var oAttributes = oElement.Attributes().XAttributeToDictionary();

				{
					var e = oElement.Attribute("r");
					if (e == null)
					{
						rotation = defaultRotation;
					}
					else
					{
						rotation = MatrixSer.ParseMatrix3(e, false);
					}
				}

				bc = oAttributes.GetUInt32("bc", defaultValue: UInt32.MaxValue, style: NumberStyles.AllowHexSpecifier);
				bc2 = oAttributes.GetUInt32("bc2", defaultValue: bc, style: NumberStyles.AllowHexSpecifier);
				bc3 = oAttributes.GetUInt32("bc3", defaultValue: bc, style: NumberStyles.AllowHexSpecifier);
				ac = oAttributes.GetUInt32("ac", defaultValue: UInt32.MaxValue, style: NumberStyles.AllowHexSpecifier);
				sc = oAttributes.GetString("sc");


				position = VectorSer.Parse3i(oElement.Element("vp"), allowNull: true);

				//FillLogicSlots(oElement);
				// </o>



				(_componentTransform, _reverseTriangles) = GetComponentTransform();
				_voxelTransform = _componentTransform;

				// Convert from world space to voxel space.
				_voxelTransform.Column3 = _componentTransform.Column3 * 4;
				_voxelTransform.M44 = 1;

				// todo: component type specific stuff.
			}
			catch (Exception e)
			{
				throw XmlDataException.AutoFormatDataName("Component", componentElement, e);
			}
		}

		/*
		private void FillLogicSlots(XElement oElement)
		{
			var slotsElement = oElement.Element("logic_slots");
			logic_slots.Clear();
			if(null == slotsElement) return;

			foreach (XElement slotElement in slotsElement.Elements())
			{
				logic_slots.Add(new LogicSlot(slotElement));
			}
		}*/


		protected static readonly Color4[] whateverColors = new[] { new Color4(1f, 1f, 1f, 1f) };

		protected Color4[] ParseSurfaceColours(
			string input,
			Color4[] compressedPaintSource = null,
			bool countFromFirstValue = true)
		{
			if (input == null) return null;
			if (!input.Contains(",")) return null; // All surfaces use the block color.

			if (compressedPaintSource == null)
			{
				compressedPaintSource = paintArray;
			}


			var values = input.Split(',');

			int count;

			if (countFromFirstValue)
			{
				count = byte.Parse(values[0]);
			}
			else
			{
				count = values.Length;
			}

			var result = new Color4[count];

			int i = 0;
			foreach (string s in countFromFirstValue ? values.Skip(1) : values)
			{
				if (s == "x")
				{
					result[i++] = compressedPaintSource[0];
				}
				else if (false && s == "y") // Expected/obvious case but not actually used by SW
				{
					result[i++] = compressedPaintSource[1];
				}
				else if (false && s == "z") // Expected/obvious case but not actually used by SW
				{
					result[i++] = compressedPaintSource[2];
				}
				else if (s == "")
				{
					result[i++] = ColorParser(0);
				}
				else
				{
					var intColor = uint.Parse(s, NumberStyles.AllowHexSpecifier);
					result[i++] = ColorParser(intColor);
				}
			}

			return result;
		}


		#endregion Construction


		#region Saving

		public virtual XElement ToXElement()
		{
			var elem = new XElement("c");
			elem.SetAttributeValueIND("d",            definitionName, defaultDefinitionName);
			elem.SetAttributeValueIND("t",            t,              0);
			elem.SetAttributeValueIND("custom_name",  custom_name,    null);
			elem.SetAttributeValueIND("blade_count",  blade_count,    0);
			elem.SetAttributeValueIND("blade_pitch",  blade_pitch,    0);
			elem.SetAttributeValueIND("blade_length", blade_length,   0.0f);

			{ // <o>
				var o = new XElement("o");

				if (rotation != defaultRotation)
				{
					o.AddAttribute("r", rotation);
				}

				o.SetAttributeValueIND_Hex("bc",  bc,  UInt32.MaxValue);
				o.SetAttributeValueIND_Hex("bc2", bc2, bc);
				o.SetAttributeValueIND_Hex("bc3", bc3, bc);
				o.SetAttributeValueIND_Hex("ac",  ac,  UInt32.MaxValue);

				o.SetAttributeValueIND("sc", sc, null);

				o.Add(position.ToXElement("vp", excludeDefaultValues:true));

				/*
				if (logic_slots.Count > 0)
				{
					var slotsElem = new XElement("logic_slots");
					foreach (LogicSlot logicSlot in logic_slots)
					{
						slotsElem.Add(logicSlot.ToXElement());
					}
					o.Add(slotsElem);
				}*/

				elem.Add(o);
			} // </o>


			// todo: component type specific stuff.
			return elem;
		}

		#endregion Saving


		/// <summary>
		/// Get <see cref="Surface"/> data for the <see cref="MeshCombiner"/> from this <see cref="Component"/>.
		/// </summary>
		/// <returns></returns>
		public MeshCombiner.DataItem[] GetComponentSurfaceData()
		{
			if (null == Definition) return Array.Empty<MeshCombiner.DataItem>();

			var result = new List<MeshCombiner.DataItem>();

			var surfaces = Definition.surfaces;


			for (int i = 0; i < surfaces.Length; i++)
			{
				var surface = surfaces[i];

				var mesh = surface.surfaceMesh;

				if (null == mesh) continue;

				// Power and Fluid connections render differently.
				if (surface.trans_type != 0) continue;


				Matrix4 resultTransform;
				{
					var ma = surface.surfaceTransform;
					var mb = _componentTransform;

					var trans = ma.ExtractTranslation();
					var trto = Matrix4.CreateTranslation(-trans); // Move to origin
					var trfo = Matrix4.CreateTranslation(trans);  // Move back to position


					resultTransform = trfo * mb * trto * ma; // OpenTK4
					//resultTransform = ma * trto * mb * trfo; // OpenTK5?
				}

				var surfaceColour = surfaceColours?[i] ?? paintColorRGBA;

				var item = new MeshCombiner.DataItem(mesh, resultTransform, surfaceColour, reverseTriangles: _reverseTriangles);

				result.Add(item);
			}
			return result.ToArray();
		}

		/// <summary>
		/// Get Surfaces transformed to Vehicle space
		/// </summary>
		/// <returns></returns>
		public IEnumerable<Surface> GetVehicleSpaceSurfaces()
		{
			if (null == Definition) yield break;

			// todo: should not be needed to compute each time.
			(var _componentTransform, var _reverseTriangles) = GetComponentTransform();

			for (int i = 0; i < Definition.surfaces.Length; i++)
			{
				if (Definition.surfaces[i].shape == Surface.Shape.None) continue;

				var surface = Definition.surfaces[i].Clone();

				if (surfaceColours != null && i < surfaceColours.Length)
				{
					surface.surfaceColor = surfaceColours[i];
				}
				else
				{
					// todo: complain
					surface.surfaceColor = paintColorRGBA;
				}

				Matrix4 resultTransform;
				{
					// Transform 
					var voxel_to_mesh = surface.surfaceTransform;
					var component_to_vehicle = _componentTransform;

					var trans = voxel_to_mesh.ExtractTranslation();
					var trto = Matrix4.CreateTranslation(-trans); // Move to origin
					var trfo = Matrix4.CreateTranslation(trans);  // Move back to position

					const bool otk4 = true;
					if (otk4)
					{
						resultTransform = trfo * component_to_vehicle * trto * voxel_to_mesh; // OpenTK4
					}
					else
					{
						resultTransform = voxel_to_mesh * trto * component_to_vehicle * trfo; // OpenTK5?
					}
				}

				var forwardVector = resultTransform.Column0.Xyz;

				surface.orientation = Surface.DirectionToOrientation(forwardVector);

				var posVector = resultTransform.Column3.Xyz * 4; // todo: don't loose precision.

				surface.position = new Vector3i((int)posVector.X, (int)posVector.Y, (int)posVector.Z);

				//todo: surface.rotation = 

				surface.surfaceTransform = resultTransform;
				surface.reverseTriangles = _reverseTriangles;

				yield return surface;
			}
		}


		/// <summary>
		/// Get a Component to VehicleBody transform, and a bool if triangles should be reversed.
		/// </summary>
		/// <returns></returns>
		public (Matrix4 transform, bool reverseTriangles) GetComponentTransform()
		{
			const float factor = 0.25f;
			var rotationAndScale = rotation;
			rotationAndScale.Transpose(); // Stormworks

			var transform = new Matrix4(rotationAndScale);
			bool do_ReverseTriangles = false;

			// the 't' attribute (mirroring).
			if (t <= 7)
			{
				transform = transform * mirrorMatrices[t];
				do_ReverseTriangles = mirrorReverseTriangles[t];
			}
			else
			{
				// todo: Complain
			}

			var translation = new Vector4(position.X * factor, position.Y * factor, position.Z * factor, 1);
			transform.Column3 = translation;

			return (transform, do_ReverseTriangles);
		}



		internal IEnumerable<Vector3i> GetVoxels()
		{
			if (null == Definition) yield break;

			foreach (var voxel in Definition.voxels)
			{
				var vs = new Vector4(voxel.position.X, voxel.position.Y, voxel.position.Z, 1);
				var bs = _voxelTransform * vs;
				var bsi = new Vector3i((int) bs.X, (int) bs.Y, (int) bs.Z);

				yield return bsi;
			}
		}


		/// <inheritdoc />
		public override string ToString()
		{
			return $"{{Component '{Definition.fileName}' at {position}";
		}


		#region Static

		private static Matrix4[] mirrorMatrices;
		private static bool[] mirrorReverseTriangles;


		private static Dictionary<string, Func<Body, XElement, Dictionary<string, string>, Component>> constructionMap = new Dictionary<string, Func<Body, XElement, Dictionary<string, string>, Component>>();

		private static void InitTypeMap()
		{
			constructionMap.Add("sign_na", (b, e, a) => new PaintableSign(b, e, a));
			constructionMap.Add("sign",    (b, e, a) => new PaintableIndicator(b, e, a));
		}


		public static Component Parse(Body body, XElement element)
		{
			var attributes = element.Attributes().XAttributeToDictionary();

			string definitionName;
			if (!attributes.TryGetValue("d", out definitionName))
			{
				definitionName = defaultDefinitionName;
			}

			if (constructionMap.TryGetValue(definitionName, out var constructor))
			{
				return constructor.Invoke(body, element, attributes);
			}
			else
			{
				return new Component(body, element, attributes);
			}
		}


		static Component()
		{
			InitTypeMap();
			InitMirrorMatrices();
		}





		private static void InitMirrorMatrices()
		{
			mirrorMatrices = new Matrix4[8];
			mirrorReverseTriangles = new bool[8];

			for (int i = 0; i < mirrorMatrices.Length; i++)
			{
				mirrorMatrices[i] = Matrix4.Identity;
			}

			mirrorMatrices[1].M11 = -1;
			mirrorReverseTriangles[1] = true;

			mirrorMatrices[2].M22 = -1;
			mirrorReverseTriangles[2] = true;

			mirrorMatrices[3].M11 = -1;
			mirrorMatrices[3].M22 = -1;

			mirrorMatrices[4].M33 = -1;
			mirrorReverseTriangles[4] = true;

			mirrorMatrices[5].M11 = -1;
			mirrorMatrices[5].M33 = -1;
			//mirrorReverseTriangles[5] = true;

			mirrorMatrices[6].M22 = -1;
			mirrorMatrices[6].M33 = -1;

			mirrorMatrices[7].M11 = -1;
			mirrorMatrices[7].M22 = -1;
			mirrorMatrices[7].M33 = -1;
			mirrorReverseTriangles[7] = true;
		}

		#endregion Static
	}
}
