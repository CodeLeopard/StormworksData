﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Xml.Linq;

using Shared.Exceptions;
using Shared.Serialization;

namespace DataModel.Vehicles
{
	public class Author
	{
		public UInt64 steam_id;
		public string username;

		#region Meta

		/// <summary>
		/// The order of this element in the parent's list of elements.
		/// It is set automatically when loading from file.
		/// </summary>
		public int? ElementOrder { get; set; } = null;

		#endregion Meta

		public Author(XElement element)
		{
			try
			{
				var att = element.Attributes().XAttributeToDictionary();
				steam_id = UInt64.Parse(att["steam_id"]);
				username = att["username"];
			}
			catch (Exception e)
			{
				throw XmlDataException.AutoFormatDataName("Author", element, e);
			}
		}

		public XElement ToXElement()
		{
			var elem = new XElement("author");
			elem.SetAttributeValue("steam_id", steam_id);
			elem.SetAttributeValue("username", username);
			return elem;
		}
	}
}
