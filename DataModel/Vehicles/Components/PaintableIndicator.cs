﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;

using BinaryDataModel;
using BinaryDataModel.DataTypes;
using BinaryDataModel.DataTypes.Enums;
using OpenToolkit.Mathematics;

using Shared.Serialization;

namespace DataModel.Vehicles.Components
{
	public class PaintableIndicator : PaintableSign
	{
		private string _gca;

		public string gca
		{
			get => _gca;
			set
			{
				_gca = value;
				faceEmissive = ParseSurfaceColours(value, countFromFirstValue: false);
			}
		}


		public Color4[] faceEmissive;

		/// <inheritdoc />
		public PaintableIndicator(Body body) : base(body) { }

		/// <inheritdoc />
		public PaintableIndicator(Body body, XElement componentElement) : base(body, componentElement) { }

		/// <inheritdoc />
		public PaintableIndicator(Body body, XElement componentElement, Dictionary<string, string> attributes)
			: base(body, componentElement, attributes)
		{
			// <o>
			var oElement = componentElement.Element("o");
			gca = oElement.Attribute("gca")?.Value;

			if (body.Vehicle.data_version <= 2)
			{
				faceEmissive = ParseColors_DV2(oElement, "ca");
			}
			else
			{
				gca = oElement.Attribute("gca")?.Value;
			}
		}

		private const float emissiveLowThreshold = 37f / 255f;

		protected override Mesh GenerateFaceMesh()
		{
			var opaque = base.GenerateFaceMesh();

			var emissive = CreateColorGridMesh(faceEmissive, Shader.Emissive, emissiveLowThreshold);

			if (emissive == null)
				return opaque;

			var mat = Matrix4.Identity;

			const float offset = 0.005f;
			mat.Column3 = new Vector4(0, 0, offset, 1);

			var result = MeshCombiner.Combine
				(
				 new MeshCombiner.DataItem[]
				 {
					 new MeshCombiner.DataItem(opaque, Matrix4.Identity),
					 new MeshCombiner.DataItem(emissive, mat)
				 }
				);
			return result;
		}

		/// <inheritdoc />
		public override XElement ToXElement()
		{
			var componentElement = base.ToXElement();
			var oElement = componentElement.Element("o");
			oElement.SetAttributeValueIND("gca", gca);
			return componentElement;
		}
	}
}
