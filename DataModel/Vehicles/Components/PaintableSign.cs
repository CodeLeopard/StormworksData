﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Xml.Linq;

using BinaryDataModel.DataTypes;
using BinaryDataModel.DataTypes.Enums;
using OpenToolkit.Mathematics;

using Shared;
using Shared.Serialization;
using Shared.Serialization.DataTypes;

namespace DataModel.Vehicles.Components
{
	public class PaintableSign : Component
	{

		private string _gc;

		public string gc
		{
			get => _gc;
			set
			{
				_gc = value;
				faceColors = ParseSurfaceColours(value, whateverColors, countFromFirstValue: false);
			}
		}

		public Color4[] faceColors;

		private Lazy<Mesh> faceMesh;

		/// <inheritdoc />
		public override Mesh LoadTimeGeneratedMesh => faceMesh.Value;

		/// <inheritdoc />
		public PaintableSign(Body body)
			: base(body)
		{
			ConstructorAlways();
		}

		/// <inheritdoc />
		public PaintableSign(Body body, XElement componentElement)
			: this(body, componentElement, componentElement.Attributes().XAttributeToDictionary())
		{ }

		/// <inheritdoc />
		public PaintableSign(Body body, XElement componentElement, Dictionary<string, string> attributes)
			: base(body, componentElement, attributes)
		{
			ConstructorAlways();
			// <o>
			var oElement = componentElement.Element("o");

			if (body.Vehicle.data_version <= 2)
			{
				faceColors = ParseColors_DV2(oElement, "cc");
			}
			else
			{
				gc = oElement.Attribute("gc")?.Value;
			}
		}

		private void ConstructorAlways()
		{
			faceMesh = new Lazy<Mesh>(GenerateFaceMesh);

			// todo: why is this needed?
			LoadTimeGeneratedMeshTransform =
				Matrix4.CreateFromAxisAngle(Vector3.UnitX, (float)(90f * Math.PI / 180f))
				* Matrix4.CreateFromAxisAngle(Vector3.UnitZ, (float)(90f * Math.PI / 180f));
		}

		protected virtual Mesh GenerateFaceMesh()
		{
			// todo: handle no colors
			var m = CreateColorGridMesh(faceColors, Shader.Opaque);

			return m;
		}

		/// <inheritdoc />
		public override XElement ToXElement()
		{
			var componentElement = base.ToXElement();
			var oElement = componentElement.Element("o");
			oElement.SetAttributeValueIND("gc", gc);
			return componentElement;
		}

		protected static Color4[] ParseColors_DV2(XElement oElement, string filter)
		{
			const int numColors = 9 * 9;
			var result = new Color4[numColors];
			for (int i = 0; i < numColors; i++)
			{
				var name = $"{filter}{i}";
				var elem = oElement.Element(name);
				var color = ColorSer.ParseRGBA(elem);
				result[i] = color;
			}

			return result;
		}

		protected static Mesh CreateColorGridMesh(Color4[] colors, Shader shader, float colorIncludeThreshold = 0f)
		{
			const float blockScale = 0.25f; // 0.25f;
			const float sideExtent = blockScale / 2f;

			var side = (int)Math.Sqrt(colors.Length);
			if (side * side != colors.Length)
			{
				throw new ArgumentException($"{nameof(colors.Length)} should be a squared number.", nameof(colors));
			}

			if (colors.All
				    (c => c.R < colorIncludeThreshold && c.G < colorIncludeThreshold && c.B < colorIncludeThreshold))
				return null;

			var m = new Mesh();

			var v = new VertexRecord();
			v.normal = Vector3.UnitZ;

			float step = blockScale / side;

			int colorIndex = 0;
			for (int j = 0; j < side; j++)
			{
				for (int k = 0; k < side; k++)
				{
					var color = colors[colorIndex++];

					if (color.R < colorIncludeThreshold
					   && color.G < colorIncludeThreshold
					   && color.B < colorIncludeThreshold)
					{
						continue;
					}


					v.color = color;

					float x = -sideExtent + j * step;
					float y = -sideExtent + k * step;
					float z = sideExtent;

					uint firstVertexIndex = m.vertexCount;

					v.position = new Vector3(x, y, z);
					m.vertices.Add(v);

					v.position = new Vector3(x + step, y, z);
					m.vertices.Add(v);

					v.position = new Vector3(x + step, y + step, z);
					m.vertices.Add(v);

					v.position = new Vector3(x, y + step, z);
					m.vertices.Add(v);

					m.indices.Add(firstVertexIndex + 2);
					m.indices.Add(firstVertexIndex + 1);
					m.indices.Add(firstVertexIndex);

					m.indices.Add(firstVertexIndex + 3);
					m.indices.Add(firstVertexIndex + 2);
					m.indices.Add(firstVertexIndex);
				}
			}


			var sm = new SubMesh
				(
				 m
			   , "face"
			   , new Bounds3
					 (
					  new Vector3(-sideExtent, -sideExtent, sideExtent)
					, new Vector3(sideExtent, sideExtent, sideExtent)
					 )
				);
			sm.shaderId = shader;
			m.subMeshes.Add(sm);
			return m;
		}
	}
}
