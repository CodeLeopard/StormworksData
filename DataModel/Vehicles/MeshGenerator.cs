﻿// Copyright 2022-2024 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Diagnostics;

using BinaryDataModel;
using BinaryDataModel.DataTypes;

using DataModel.Definitions.NestedTypes;

using OpenToolkit.Mathematics;

using Shared;

namespace DataModel.Vehicles
{
	public static class MeshGenerator
	{
		/// <summary>
		/// Method to fetch Mesh based on it's path relative to ROM (as used in the definition xml).
		/// Returning <see langword="null"/> is acceptable.
		/// </summary>
		public static Func<string, Mesh> GetOrLoadMesh = LoadNoMesh;


		private static Mesh LoadNoMesh(string _) => null;

		private static bool warnedForMeshLoading;


		public static IEnumerable<MeshCombiner.DataItem> GetMeshesForComponent(Component component)
		{
			var (componentTransform, reverseTriangles) = component.GetComponentTransform();
			var paintArray = component.paintArray;
			if (null != component.Definition?.mesh_data_name)
			{
				var mesh = GetOrLoadMesh(component.Definition.mesh_data_name);

				if (null != mesh)
				{
					yield return new MeshCombiner.DataItem
						(mesh, componentTransform, paintArray, reverseTriangles: reverseTriangles, forceEmissiveColor: true);
				}
			}

			if (null != component.Definition?.mesh_0_name)
			{
				// todo: these may need to be on a separate GameObject, because they are moving parts
				var mesh = GetOrLoadMesh(component.Definition.mesh_0_name);

				if (null != mesh)
				{
					var finalTransform = componentTransform * component.Definition.mesh_0_transform;
					yield return new MeshCombiner.DataItem
						(mesh, finalTransform, paintArray, reverseTriangles: reverseTriangles, forceEmissiveColor: true);
				}
			}

			if (null != component.Definition?.mesh_1_name)
			{
				// todo: these may need to be on a separate GameObject, because they are moving parts
				var mesh = GetOrLoadMesh(component.Definition.mesh_1_name);

				if (null != mesh)
				{
					var finalTransform = componentTransform * component.Definition.mesh_1_transform;
					yield return new MeshCombiner.DataItem
						(mesh, finalTransform, paintArray, reverseTriangles: reverseTriangles, forceEmissiveColor: true);
				}
			}

			if (null != component.LoadTimeGeneratedMesh)
			{
				var finalTransform = componentTransform * component.LoadTimeGeneratedMeshTransform;
				yield return new MeshCombiner.DataItem
					(component.LoadTimeGeneratedMesh, finalTransform, reverseTriangles: reverseTriangles);
			}
		}


		public static Mesh GenerateMesh(Body body)
		{
			if (null == body) throw new ArgumentNullException(nameof(body));

			if (! warnedForMeshLoading && GetOrLoadMesh == LoadNoMesh)
			{
				warnedForMeshLoading = true;
				Console.WriteLine
					(
					 $"[MeshGenerator] Note: Meshes mentioned in the definition of a component will not be loaded "
				   + $"because the generator does not know where to fetch them from."
				   + $"\nAssign a Method to {nameof(MeshGenerator)}.{nameof(GetOrLoadMesh)} to use meshes."
					);
			}

			var timer = new Stopwatch();
			List<Surface> surfaces =
				Surfaces(body, timer,
				         out TimeSpan rawSurfaceTime,
				         out int deleteCount,
				         out TimeSpan optimizeTime,
				         out List<MeshCombiner.DataItem> list,
				         out TimeSpan combinerPrepareSurfaceTime);
			timer.Restart();

			int beforeComponentAddCount = list.Count;
			foreach (var component in body.components)
			{
				var results = GetMeshesForComponent(component);
				list.AddRange(results);
			}

			var combinerPrepareComponentTime = timer.Elapsed;
			timer.Restart();

			var result = MeshCombiner.Combine(list.ToArray(), mergeSubMeshes: true);

			var combinerTime = timer.Elapsed;
			timer.Stop();
			var totalTime = rawSurfaceTime + optimizeTime + combinerPrepareSurfaceTime + combinerPrepareComponentTime + combinerTime;

			Console.WriteLine($"[{nameof(MeshGenerator)}] Stats for {body}\n"
			                + $"\t{surfaces.Count                      ,6} raw Surfaces processed in                                 {rawSurfaceTime}.\n"
			                + $"\t{deleteCount                         ,6} of those ware deleted by the Optimizer in                 {optimizeTime}.\n"
			                + $"\t{surfaces.Count - deleteCount        ,6} Surfaces ware sent to the combiner for the SurfaceMesh in {combinerPrepareSurfaceTime}.\n"
			                + $"\t{list.Count - beforeComponentAddCount,6} Component Meshes ware sent to Combiner in                 {combinerPrepareComponentTime}.\n"
			                + $"\t{list.Count                          ,6} total entries ware combined in                            {combinerTime}.\n"
			                + $"\tResult: {result.vertexCount,7} Vertices and {result.indexCount,7} Indices, about {StringExt.PrintBytesReadable(result.SizeEstimate),8}, total {totalTime} elapsed.");


			return result;
		}

		private static List<Surface> Surfaces
		(
			Body                            body
		  , Stopwatch                       timer
		  , out TimeSpan                    rawSurfaceTime
		  , out int                         deleteCount
		  , out TimeSpan                    optimizeTime
		  , out List<MeshCombiner.DataItem> list
		  , out TimeSpan                    combinerPrepareSurfaceTime
		)
		{
			timer.Start();

			var surfaces = new List<Surface>();
			foreach (var component in body.components)
			{
				surfaces.AddRange(component.GetVehicleSpaceSurfaces());
			}

			rawSurfaceTime = timer.Elapsed;
			timer.Restart();

			var lookup = new RWLookup<Vector3i, Surface>(surfaces, s => s.position);
			deleteCount = 0;
			foreach (var innerBucket in lookup)
			{
				foreach (var nearPos in VectorExt.Neighbours(innerBucket.Key))
				{
					var outerBucket = lookup[nearPos];
					for (int outerIndex = outerBucket.Count - 1; outerIndex >= 0; outerIndex--)
					{
						var outerSurface = outerBucket[outerIndex];

						// Check for opposing surfaces only between neighbours.
						for (int innerIndex = innerBucket.Count - 1; innerIndex >= 0; innerIndex--)
						{
							var innerSurface = innerBucket[innerIndex];
							if (Surface.AreOpposingSurfaces(innerSurface, outerSurface))
							{
								if (Surface.AreMeshGenEliminatedSurfaces(innerSurface, outerSurface, true))
								{
									innerBucket.RemoveAt(innerIndex);
									outerBucket.RemoveAt(outerIndex);

									deleteCount++;
									deleteCount++;

									break;
								}

								if (Surface.IsMeshGenEliminatedSurface(innerSurface, outerSurface, true))
								{
									innerBucket.RemoveAt(innerIndex);
									deleteCount++;
								}
							}
						}
					}
				}
			}

			optimizeTime = timer.Elapsed;
			timer.Restart();

			list = new List<MeshCombiner.DataItem>();

			foreach (Surface surface in lookup.GetValueEnumerator())
			{
				if (null == surface.surfaceMesh) continue;

				var item = new MeshCombiner.DataItem
					(
					 surface.surfaceMesh
				   , surface.surfaceTransform
				   , surface.surfaceColor
				   , reverseTriangles: surface.reverseTriangles
					);
				list.Add(item);
			}


			combinerPrepareSurfaceTime = timer.Elapsed;
			return surfaces;
		}
	}
}
