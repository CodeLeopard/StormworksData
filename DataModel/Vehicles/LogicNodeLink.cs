﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Linq;
using System.Xml.Linq;

using DataModel.DataTypes;

using OpenToolkit.Mathematics;

using Shared.Exceptions;
using Shared.Serialization;
using Shared.Serialization.DataTypes;

namespace DataModel.Vehicles
{
	public struct LogicNodeLink
	{
		public LogicNodeType type;
		public Vector3i voxel_pos_0;
		public Vector3i voxel_pos_1;


		#region Meta

		/// <summary>
		/// The order of this element in the parent's list of elements.
		/// It is set automatically when loading from file.
		/// </summary>
		public int? ElementOrder;

		#endregion Meta


		public LogicNodeLink(XElement element, int? elementOrder = null)
		{
			try
			{
				var attributes = element.Attributes().XAttributeToDictionary();
				{
					attributes.TryGetByte("type", out byte temp);
					type = (LogicNodeType)temp;
				}
				{
					var e = element.Element("voxel_pos_0");
					if (null != e)
						voxel_pos_0 = VectorSer.Parse3i(e);
					else
						voxel_pos_0 = new Vector3i();
				}
				{
					var e = element.Element("voxel_pos_1");
					if (null != e)
						voxel_pos_1 = VectorSer.Parse3i(e);
					else
						voxel_pos_1 = new Vector3i();
				}

				if (element.Elements().Count() != 2)
				{
					// For debugger breakpoint.
					// todo: complain.
				}

				ElementOrder = elementOrder;
			}
			catch (Exception e)
			{
				throw XmlDataException.AutoFormatDataName(nameof(LogicNodeLink), element, e);
			}
		}

		public XElement ToXElement()
		{
			var elem = new XElement("logic_node_link");
			elem.SetAttributeValueIND("type", (byte)type, (byte) LogicNodeType.OnOff);

			elem.Add(voxel_pos_0.ToXElement("voxel_pos_0", excludeDefaultValues:true));
			elem.Add(voxel_pos_1.ToXElement("voxel_pos_1", excludeDefaultValues: true));
			return elem;
		}
	}
}
