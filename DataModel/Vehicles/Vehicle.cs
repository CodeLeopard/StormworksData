﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Xml.Linq;

using DataModel.DataTypes;

using OpenToolkit.Mathematics;

using Shared;
using Shared.Exceptions;
using Shared.Serialization;
using Shared.Serialization.DataTypes;

namespace DataModel.Vehicles
{
	public class Vehicle
	{
		/// <summary>
		/// The serialized data version used to load the vehicle.
		/// Versions >= 3 use a limited form of compression.
		/// </summary>
		public int data_version;

		public const int save_data_version = 3;
		//public bool is_advanced;

		/// <summary>
		/// Static vehicles cannot move and do not compute collisions with other static vehicles.
		/// </summary>
		public bool is_static;

		/// <summary>
		/// The highest <see cref="Body.unique_id"/> that is in use.
		/// </summary>
		public int bodies_id;

		public int NextBodyID => bodies_id++;

		/// <summary>
		/// The offset inside the VehicleEditor's workbench where the vehicle is placed.
		/// </summary>
		public Vector3 editor_placement_offset;

		public List<Author> authors = new List<Author>();

		public List<Body> bodies = new List<Body>();
		public List<LogicNodeLink> logic_node_links = new List<LogicNodeLink>();



		//#### #### #### #### #### #### #### #### #### #### #### #### #### ####
		//#### #### End of xml data ### #### #### #### #### #### #### #### ####
		//#### #### #### #### #### #### #### #### #### #### #### #### #### ####

		public string meta_filePath;
		public string fileName;


		public float mass;

		/// <summary>
		/// Bounds of the voxels of the vehicle.
		/// Note: current implementation does not keep the bounds up to date with modifications.
		/// Note: the size of these bounds can be 0 on some or all axis.
		/// </summary>
		public Bounds3i voxelBounds = new Bounds3i(true);

		/// <summary>
		/// <see cref="voxelBounds"/> but in world coordinates.
		/// Size will be at least 0.25 in size.
		/// </summary>
		public Bounds3 voxelBoundsF
		{
			get
			{
				var temp = new Bounds3(true);

				var min = voxelBounds.Min;
				var max = voxelBounds.Max;
				temp.Inflate(new Vector3(min.X, min.Y, min.Z) * 0.25f);
				temp.Inflate(new Vector3(max.X, max.Y, max.Z) * 0.25f);

				// Increae the size by one block's worth.
				temp.Size += new Vector3(0.25f, 0.25f, 0.25f);
				return temp;
			}
		}

		/// <summary>
		/// The bounds containing the Meshes of all bodies.
		/// </summary>
		public Bounds3 meshBounds = new Bounds3(true);

		public const float VoxelSpaceToMeshSpace = 0.25f;


		public UInt64 ComponentCount
		{
			get
			{
				UInt64 result = 0;
				foreach (Body body in bodies)
				{
					result += (UInt64) body.components.LongCount();
				}

				return result;
			}
		}

		private readonly Dictionary<Vector3i, Component> _voxels = new Dictionary<Vector3i, Component>();
		private readonly ReadOnlyDictionary<Vector3i, Component> _voxels_ro;

		/// <summary>
		/// Enumerates all the components on the vehicle by enumerating over the vehicle's bodies.
		/// </summary>
		/// <returns></returns>
		public IEnumerable<Component> AllComponents => bodies.SelectMany(b => b.components);

		public IReadOnlyDictionary<Vector3i, Component> Voxels => _voxels_ro;


		public delegate Color4 ColorParserDelegate(uint intColor);

		/// <summary>
		/// The method to be used to parse colors from xml.
		/// The way colors are stored changed with <see cref="data_version"/> 3.
		/// </summary>
		public ColorParserDelegate ColorParser;


		#region Construction

		public Vehicle()
		{
			_voxels_ro = new ReadOnlyDictionary<Vector3i, Component>(_voxels);
		}

		public Vehicle(XDocument document, string metaFilePath = null) : this(document.Element("vehicle"), metaFilePath)
		{}


		public Vehicle(XElement vehicleElement, string metaFilePath = null) : this()
		{
			try
			{
				this.meta_filePath = metaFilePath;
				if (null != meta_filePath) fileName = Path.GetFileNameWithoutExtension(meta_filePath);
				var attributes = vehicleElement.Attributes().XAttributeToDictionary();

				data_version = int.Parse(attributes["data_version"]);
				if (data_version >= 3)
				{
					ColorParser = VectorSer.FromRGB;
				}
				else
				{
					ColorParser = VectorSer.FromRGBA;
				}

				is_static = attributes.GetBool("is_static", false);
				bodies_id = attributes.GetInt32("bodies_id", 0);

				{
					var elem = vehicleElement.Element("editor_placement_offset");
					editor_placement_offset = VectorSer.Parse3f(elem, true, Vector3.Zero);
				}

				ParseAuthors(vehicleElement);

				ParseBodies(vehicleElement);

				ParseLogicNodeLinks(vehicleElement);
			}
			catch (Exception e)
			{
				throw XmlDataException.AutoFormatDataName("Vehicle", vehicleElement, e);
			}
		}

		private void ParseAuthors(XElement vehicle)
		{
			try
			{
				var authorsElement = vehicle.Element("authors");
				if (null == authorsElement)
				{
					//Debugger.Break();
					return;
				}

				int order = 0;
				foreach (XElement element in authorsElement.Elements())
				{
					var author = new Author(element);
					author.ElementOrder = order++;
					authors.Add(author);
				}
			}
			catch (Exception e)
			{
				throw XmlDataException.AutoFormatDataName("Authors", vehicle, e);
			}
		}

		private void ParseBodies(XElement vehicle)
		{
			try
			{
				var bodiesElement = vehicle.Element("bodies");

				int order = 0;

				foreach (XElement element in bodiesElement.Elements())
				{
					var b = new Body(this, element);
					b.ElementOrder = order++;

					mass += b.mass;

					foreach (var (v, c) in b.GetVoxels())
					{
						if (_voxels.TryGetValue(v, out var existingComponent))
						{
							// todo: Complain using existingComponent
						}
						else
						{
							_voxels[v] = c;

							voxelBounds.Inflate(v);
						}
					}
				}
			}
			catch (Exception e)
			{
				throw XmlDataException.AutoFormatDataName("Bodies", vehicle, e);
			}
		}

		private void ParseLogicNodeLinks(XElement vehicle)
		{
			try
			{
				var nodesElement = vehicle.Element("logic_node_links");
				if (null == nodesElement)
				{
					Console.WriteLine("Warning: Vehicle missing logic_node_links.");
					return;
				}
				int order = 0;
				foreach (XElement element in nodesElement.Elements())
				{
					logic_node_links.Add(new LogicNodeLink(element, order++));
				}
			}
			catch (Exception e)
			{
				throw XmlDataException.AutoFormatDataName("logic node links", vehicle, e);
			}
		}

		#endregion Construction

		#region Saving

		public XDocument ToXDocument()
		{
			var document = new XDocument();
			var elem = ToXElement();
			document.Add(elem);
			return document;
		}

		public XElement ToXElement()
		{
			var elem = new XElement("vehicle");
			elem.SetAttributeValue("data_version", save_data_version);
			elem.SetAttributeValueIND("is_static", is_static, false);
			elem.SetAttributeValue("bodies_id", bodies_id);

			elem.Add(editor_placement_offset.ToXElement("editor_placement_offset"));

			elem.Add(AuthorsElement());
			elem.Add(BodiesElement());
			elem.Add(LogicNodesElement());

			return elem;
		}


		private XElement AuthorsElement()
		{
			var container = new XElement("authors");
			foreach (var author in authors.OrderBy(a => a.ElementOrder, NullLastComparer<int>.Comparer))
			{
				container.Add(author.ToXElement());
			}
			return container;
		}

		private XElement BodiesElement()
		{
			var container = new XElement("bodies");
			foreach (Body body in bodies.OrderBy(b => b.ElementOrder, NullLastComparer<int>.Comparer))
			{
				container.Add(body.ToXElement());
			}
			return container;
		}

		private XElement LogicNodesElement()
		{
			var container = new XElement("logic_node_links");
			foreach (LogicNodeLink link in logic_node_links.OrderBy(l => l.ElementOrder, NullLastComparer<int>.Comparer))
			{
				container.Add(link.ToXElement());
			}
			return container;
		}

		#endregion Saving

		/// <inheritdoc />
		public override string ToString()
		{
			return $"Vehicle '{fileName}' {bodies.Count} bodies, {ComponentCount} components total.";
		}
	}
}
