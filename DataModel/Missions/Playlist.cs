﻿// Copyright 2022-2024 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Runtime.CompilerServices;
using System.Xml.Linq;

using CustomSerialization;
using CustomSerialization.Attributes;

using Shared.Exceptions;
using Shared.Serialization;

[assembly: InternalsVisibleTo("Tests")]

namespace DataModel.Missions
{
	[SerializerExplicit(xmlTypeName:"playlist")]
	[SerializerKnownType(typeof(Location), typeof(List<Location>))]
	public class Playlist : ISerializationEventReceiver
	{
		/// <summary>
		/// Metadata: The file this data was deserialized from.
		/// </summary>
		[NonSerialized] public string meta_filePath;

		[SerializerInclude(xmlAsAttribute: true, optional: true)] public string path_id;

		[SerializerInclude(xmlAsAttribute: true, optional: true)] public string folder_path;

		[SerializerInclude(xmlAsAttribute: true)] public byte file_store; //todo: enum

		[SerializerInclude(xmlAsAttribute: true)] public string name;


		#region Scuffed location indirection


		public int location_id_counter
		{
			get;
			private set;
		} = 1;

		public int component_id_counter
		{
			get;
			private set;
		} = 1;

		private List<Location> locations = new List<Location>();

		[AugmentDeserialize]
		private void OnMembersDeserialized
			(Serializer serializer, XElement element, Dictionary<string, string> attributes)
		{
			var locationsElement = element.Element("locations") ?? throw new Exception("todo: missing root locations element");
			var locationsAttributes = locationsElement.Attributes().XAttributeToDictionary();

			location_id_counter  = locationsAttributes.GetInt32("location_id_counter",  1);
			component_id_counter = locationsAttributes.GetInt32("component_id_counter", 1);

			var locationsListElement = locationsElement.Element("locations") ?? throw new Exception("todo: missing root locations element");

			var tempLocations = serializer.Deserialize<List<Location>>
				(locationsListElement, serializer.graphSpec.GetSpec(typeof(List<Location>)), null, false);

			locations.AddRange(tempLocations); // Needed because readonly collections have been created by constructor.
		}

		[AugmentSerialize]
		private void BeforeMembersSerialized(Serializer serializer, XElement element)
		{
			var locationsElement = new XElement("locations");
			element.Add(locationsElement);

			locationsElement.SetAttributeValue("location_id_counter", location_id_counter);
			locationsElement.SetAttributeValue("component_id_counter", component_id_counter);

			var listElement = new XElement("locations");
			locationsElement.Add(listElement);

			serializer.SerializeType(locations, serializer.graphSpec.GetSpec(typeof(List<Location>)), listElement);
		}


		#endregion Scuffed location indirection

		/// <summary>
		/// Indicates that this <see cref="Playlist"/> contains at least two <see cref="Location"/>s on the same tile and same mode (environment/mission). Only the first one will appear in <see cref="MapTileNameToLocation"/> or <see cref="MapTileNameToEnvLocation"/>.
		/// Saving the <see cref="Playlist"/> in this state is safe and will work as expected.
		/// </summary>
		public bool HasDuplicateLocations { get; private set; }

		private ReadOnlyCollection<Location> locationsReadonly;


		public IReadOnlyList<Location> Locations => locationsReadonly;




		public int NextLocationID => location_id_counter++;
		public int NextComponentID => component_id_counter++;

		private readonly Dictionary<string, Location> mapTileNameToLocation = new Dictionary<string, Location>();
		private readonly ReadOnlyDictionary<string, Location> mapTileNameToLocation_readonly;
		public IReadOnlyDictionary<string, Location> MapTileNameToLocation => mapTileNameToLocation_readonly;

		private readonly Dictionary<string, Location> mapTileNameToEnvLocation = new Dictionary<string, Location>();
		private readonly ReadOnlyDictionary<string, Location> mapTileNameToEnvLocation_readonly;
		public IReadOnlyDictionary<string, Location> MapTileNameToEnvLocation => mapTileNameToEnvLocation_readonly;

		[SerializerInclude]
		public Playlist()
		{
			HasDuplicateLocations = false;
			locationsReadonly = new ReadOnlyCollection<Location>(locations);
			mapTileNameToLocation_readonly = new ReadOnlyDictionary<string, Location>(mapTileNameToLocation);
			mapTileNameToEnvLocation_readonly = new ReadOnlyDictionary<string, Location>(mapTileNameToEnvLocation);
		}

		public Location CreateLocation(string tilePath, bool isEnvMod)
		{
			var l = new Location(this, tilePath, isEnvMod);
			// Note: constructor calls AddLocation.

			return l;
		}

		/// <summary>
		/// Try to retrieve a location that matches the cirteria. If none exists it is created.
		/// </summary>
		/// <param name="tileName"></param>
		/// <param name="isEnvironment"></param>
		/// <returns></returns>
		public Location GetOrCreateLocation(string tilePath, string tileName, bool isEnvironment)
		{
			Location location = null;
			if (isEnvironment && mapTileNameToEnvLocation.TryGetValue(tileName, out location))
			{
				return location;
			}
			if (! isEnvironment && mapTileNameToLocation.TryGetValue(tileName, out location))
			{
				return location;
			}

			location = CreateLocation(tilePath, isEnvironment);
			return location;
		}

		public void AddLocation(Location l)
		{
			l.parent = this;
			l.id = NextLocationID;
			locations.Add(l);

			if (l.is_env_mod)
			{
				mapTileNameToEnvLocation.Add(l.tileName, l);
			}
			else
			{
				mapTileNameToLocation.Add(l.tileName, l);
			}
		}

		public bool TryAddLocation(Location l)
		{
			if (l.is_env_mod && ! mapTileNameToEnvLocation.TryAdd(l.tileName, l))
			{
				return false;
			}
			if(! l.is_env_mod && ! mapTileNameToLocation.TryAdd(l.tileName, l))
			{
				return false;
			}

			l.parent = this;
			l.id = NextLocationID;
			locations.Add(l);

			return true;
		}

		public bool RemoveLocation(Location l)
		{
			bool exists;
			if (l.is_env_mod)
			{
				exists = mapTileNameToEnvLocation.Remove(l.tileName);
			}
			else
			{
				exists = mapTileNameToLocation.Remove(l.tileName);
			}

			if (exists)
			{
				locations.Remove(l);
			}

			return exists;
		}

		/// <summary>
		/// All <see cref="Component"/>s in the <see cref="Playlist"/>
		/// </summary>
		/// <returns></returns>
		public IEnumerable<Component> GetComponentIEnumerable()
		{
			foreach (var location in Locations)
			{
				foreach (var component in location.components)
				{
					yield return component;
				}
			}
		}

		public IEnumerable<Component> Components => GetComponentIEnumerable();


		void ISerializationEventReceiver.BeforeSerialize()
		{
			//ReAssignLocationIds();
			//ReAssignComponentIds(); // Breaks vehicles.


			// todo: re-assign ids for vehicles too
		}

		private void ReAssignLocationIds()
		{
			int id = 1; // Starts at one apparently.
			foreach (var location in locations)
			{
				location.id = id++;
			}

			location_id_counter = id - 1; // The id value is that for the next component, not the last one assigned.
		}

		private void ReAssignComponentIds()
		{
			int id = 1; // Starts at one apparently.
			foreach (var component in Components)
			{
				component.id = id++;
			}

			component_id_counter = id - 1; // The id value is that for the next component, not the last one assigned.
		}

		void ISerializationEventReceiver.AfterDeserialize(string filePath)
		{
			meta_filePath = filePath;

			locationsReadonly = new ReadOnlyCollection<Location>(locations);

			mapTileNameToLocation.Clear();


			foreach (var location in locations)
			{
				location.parent = this;

				bool duplicate = false;

				if (location.tile != null) 
				{
					if (location.is_env_mod)
					{
						duplicate |= ! mapTileNameToEnvLocation.TryAdd(location.tileName, location);
					}
					else
					{
						duplicate |= ! mapTileNameToLocation.TryAdd(location.tileName, location);
					}
				}
				else
				{
					// This is allowed by Stormworks, for example for a collection of vehicles to be spanwed at a position determined by script.
				}

				if (duplicate)
				{
					HasDuplicateLocations = true;
					Console.WriteLine($"Encountered duplicate location: same tile: '{location.tileName}' and mode: {(location.is_env_mod ? "environment" : "mission")}.\n" +
						$"The duplicated location with id {location.id} will not appear in the tile_name to location mappings, but otherwise should work normally. " +
						$"Saving the location/playlist will also work normally.");
				}

				foreach (var component in location.components)
				{
					component.parentLocation = location;
				}
			}
		}

		/// <summary>
		/// Convert a vehicle filepath as stored in mission xml to a full absolute path pointing to the actual file.
		/// </summary>
		/// <param name="vehicleFilePath"></param>
		/// <returns></returns>
		public string GetVehiclePath(string vehicleFilePath)
		{
			if (null == meta_filePath)
				throw new NullReferenceException($"{nameof(meta_filePath)} must be assigned to use this method.");

			string playlistFolder = Path.GetDirectoryName(meta_filePath);
			string vehicleName = Path.GetFileName(vehicleFilePath);

			string result = Path.Combine(playlistFolder, vehicleName);
			return result;
		}
	}
}
