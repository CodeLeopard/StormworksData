// Autogenerated code, edits will not be preserved.
// See: CodeGenerators.DataModel.Missions.Wrappers for generator.

using System;

using Shared;

namespace DataModel.Missions
{
	public class Loot : ComponentWrapper
	{

		[VisibleProperty]
		public Boolean RelativeToSeaFloor
		{
			get => WrappedComponent.is_sea_floor_object;
			set => WrappedComponent.is_sea_floor_object = value;
		}

		[VisibleProperty]
		public Byte Type
		{
			get => WrappedComponent.loot_type;
			set
			{
				if (WrappedComponent.loot_type != value)
				{
					WrappedComponent.loot_type = value;
					RaiseVisualChanged();
				}
			}
		}

		[VisibleProperty(large: true)]
		public String Tags
		{
			get => WrappedComponent.name;
			set => WrappedComponent.name = value;
		}

		public Loot(Component toWrap)
			: base(toWrap,
			       ComponentType.Loot,
			       true,
			       true)
		{
			if(toWrap.component_type != ComponentType)
				throw new ArgumentException($"Component has type {toWrap.component_type} but this wrapper can only wrap {ComponentType}");
		}

		public Loot(Location toAddTo)
			: base(new Component(toAddTo),
			       ComponentType.Loot,
			       true,
			       true)
		{
			WrappedComponent.component_type = ComponentType;
			toAddTo.Add(WrappedComponent);
		}

		static Loot()
		{
			ComponentWrapper.componentTypeMap[ComponentType.Loot] = typeof(Loot);
			{
				var ctorI = typeof(Loot).GetConstructor(ComponentWrapper.constructorTypes_Component);
				ComponentWrapper.constructorCache_Component[typeof(Loot)] = ctorI;
			}
			{
				var ctorI = typeof(Loot).GetConstructor(ComponentWrapper.constructorTypes_Location);
				ComponentWrapper.constructorCache_Location[typeof(Loot)] = ctorI;
			}
		}
		// Dummy method to trigger the static constructor, to ensure the static values of ComponentWrapper are initialized before use.
		public static void Initialize() {}
	}
}
// Autogenerated code, edits will not be preserved.
// See: CodeGenerators.DataModel.Missions.Wrappers for generator.
