// Autogenerated code, edits will not be preserved.
// See: CodeGenerators.DataModel.Missions.Wrappers for generator.

using System;

using Shared;

namespace DataModel.Missions
{
	public class Creature : ComponentWrapper
	{

		[VisibleProperty]
		public Boolean RelativeToSeaFloor
		{
			get => WrappedComponent.is_sea_floor_object;
			set => WrappedComponent.is_sea_floor_object = value;
		}

		[VisibleProperty]
		public CreatureType CreatureType
		{
			get => WrappedComponent.creature_type;
			set
			{
				if (WrappedComponent.creature_type != value)
				{
					WrappedComponent.creature_type = value;
					RaiseVisualChanged();
				}
			}
		}

		[VisibleProperty(large: true)]
		public String Tags
		{
			get => WrappedComponent.name;
			set => WrappedComponent.name = value;
		}

		public Creature(Component toWrap)
			: base(toWrap,
			       ComponentType.Creature,
			       true,
			       true)
		{
			if(toWrap.component_type != ComponentType)
				throw new ArgumentException($"Component has type {toWrap.component_type} but this wrapper can only wrap {ComponentType}");
		}

		public Creature(Location toAddTo)
			: base(new Component(toAddTo),
			       ComponentType.Creature,
			       true,
			       true)
		{
			WrappedComponent.component_type = ComponentType;
			toAddTo.Add(WrappedComponent);
		}

		static Creature()
		{
			ComponentWrapper.componentTypeMap[ComponentType.Creature] = typeof(Creature);
			{
				var ctorI = typeof(Creature).GetConstructor(ComponentWrapper.constructorTypes_Component);
				ComponentWrapper.constructorCache_Component[typeof(Creature)] = ctorI;
			}
			{
				var ctorI = typeof(Creature).GetConstructor(ComponentWrapper.constructorTypes_Location);
				ComponentWrapper.constructorCache_Location[typeof(Creature)] = ctorI;
			}
		}
		// Dummy method to trigger the static constructor, to ensure the static values of ComponentWrapper are initialized before use.
		public static void Initialize() {}
	}
}
// Autogenerated code, edits will not be preserved.
// See: CodeGenerators.DataModel.Missions.Wrappers for generator.
