﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

using CustomSerialization.Attributes;

namespace DataModel.Missions
{
	[SerializerExplicit(xmlTypeName: "l")]
	public class Location //: IEnumerable<Component>
	{
		/// <summary>
		/// Metadata: The parent <see cref="Playlist"/>
		/// </summary>
		public Playlist parent;

		/// <summary>
		/// id is assigned automatically
		/// </summary>
		[SerializerInclude(xmlAsAttribute: true)] public int id;

		private string _tile;
		/// <summary>
		/// Path to the tile.xml file this location defines components for.
		/// </summary>
		[SerializerInclude(xmlAsAttribute: true, optional: true)]
		public string tile
		{
			get => _tile;
			set
			{
				_tile = value?.Replace('\\', '/');
				_tileName = Path.GetFileNameWithoutExtension(_tile);
			}
		}

		private string _tileName;
		/// <summary>
		/// The file name (without extension) of the tile.
		/// </summary>
		public string tileName => _tileName;

		/// <summary>
		/// todo: not visible in-game?
		/// </summary>
		[SerializerInclude(xmlAsAttribute: true, optional:true)] public string name;

		[SerializerInclude(xmlAsAttribute: true, omitIfDefault: true)] public bool is_env_mod;

		/// <summary>
		/// For tiles that may spawn multiple times, the amount that this environment mod will spawn on it.
		/// </summary>
		[SerializerInclude(xmlAsAttribute: true, omitIfDefault: true)] public int env_mod_spawn_num;

		[SerializerInclude]
		public List<Component> components = new List<Component>();


		public void Add(Component c)
		{
			if (is_env_mod)
			{
				if( ! Component.EnvironmentComponents[c.component_type])
					throw new ArgumentException($"Component type {c.component_type} is not allowed in Environment location");
			}
			else
			{
				if (!Component.MissionComponents[c.component_type])
					throw new ArgumentException($"Component type {c.component_type} is not allowed in Mission location");
			}

			components.Add(c);
			c.parentLocation = this;
			//c.is_environment = is_env_mod; // todo: when format changes
			if (c.id < 0)
			{
				c.id = parent.NextComponentID;
			}
		}

		public void Add(ComponentWrapper w)
		{
			Add(w.WrappedComponent);
		}

		public bool Remove(Component component)
		{
			var index = components.IndexOf(component);

			if (index >= 0)
			{
				component.parentLocation = null;
				components.RemoveAt(index);
				return true;
			}

			return false;
		}

		public bool Remove(ComponentWrapper w)
		{
			return Remove(w.WrappedComponent);
		}

		[SerializerInclude]
		protected Location()
		{

		}

		public Location(Playlist parent, string tilePath, bool is_env_mod)
		{
			this.tile = tilePath;
			this.name = tileName;

			this.is_env_mod = is_env_mod;

			parent.AddLocation(this);
		}
		/*
		/// <inheritdoc />
		public IEnumerator<Component> GetEnumerator()
		{
			return components.GetEnumerator();
		}

		/// <inheritdoc />
		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}*/

		public override string ToString()
		{
			return $"{{Location #{id} '{name}' @'{tile}'}}";
		}
	}
}
