﻿// Copyright 2022-2024 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;
using CustomSerialization;
using CustomSerialization.Attributes;

using OpenToolkit.Mathematics;

using Shared;

namespace DataModel.Missions
{
	[SerializerExplicit(xmlTypeName: "c")]
	public class Component
	{
		/// <summary>
		/// Metadata: The parent <see cref="Playlist"/>
		/// </summary>
		public Playlist parentPlaylist => parentLocation.parent;

		/// <summary>
		/// Metadata: The parent <see cref="Location"/>
		/// </summary>
		public Location parentLocation;

		[SerializerInclude(nameOverride: "component_type", xmlAsAttribute: true, omitIfDefault: true)]
		[EditorBrowsable(EditorBrowsableState.Never), Browsable(false)]
		public int __component_type
		{
			get => (int) component_type;
			set => component_type = (ComponentType) value;
		}


		public ComponentType component_type;

		/// <summary>
		/// Assigned automatically.
		/// </summary>
		[SerializerInclude(xmlAsAttribute: true, omitIfDefault: true)]
		public int id;

		/// <summary>
		/// Is this Component always present in the environment (otherwise spawns with mission activation)
		/// </summary>
		// todo: when format changes -> implement properly
		public bool is_environment
		{
			get => parentLocation.is_env_mod;
			set => SetIsEnvironment(value);
		}

		/// <summary>
		/// Parent vehicle ( = <see cref="Component"/>) of this <see cref="Component"/>.
		/// </summary>
		public Component vehicleParent; // todo: assign id properly

		/// <summary>
		/// Based on automatically assigned id, use <see cref="vehicleParent"/> to manipulate.
		/// </summary>
		[SerializerInclude(xmlAsAttribute: true, omitIfDefault: true)] public int vehicle_parent_id;


		[SerializerInclude(xmlAsAttribute: true, omitIfDefault: true)] public string cargo_zone_import_cargo_type; //todo?: enum

		/// <summary>
		/// Name, in-game-editor called Tags.
		/// </summary>
		[SerializerInclude(xmlAsAttribute: true, omitIfDefault: true)] public string name;

		[SerializerInclude(xmlAsAttribute: true, omitIfDefault: true)] public string display_name;

		[SerializerInclude(xmlAsAttribute: true, omitIfDefault: true)] public bool is_sea_floor_object;

		[SerializerInclude(xmlAsAttribute: true, omitIfDefault: true)] public bool show_zone_on_map;
		[SerializerInclude(xmlAsAttribute: true, omitIfDefault: true)] public byte zone_type;
		[SerializerInclude(xmlAsAttribute: true, omitIfDefault: true)] public float zone_radius;

		[SerializerInclude(nameOverride: "dynamic_object_type", xmlAsAttribute: true, omitIfDefault: true)]
		[EditorBrowsable(EditorBrowsableState.Never), Browsable(false)]
		private int __dynamic_object_type
		{
			get => (int)dynamic_object_type;
			set => dynamic_object_type = (ObjectType)value;
		}

		public ObjectType dynamic_object_type;

		[SerializerInclude(xmlAsAttribute: true, omitIfDefault: true)] public byte loot_type;

		[SerializerInclude(xmlAsAttribute: true, omitIfDefault: true)] public byte character_outfit_category; //todo: enum
		[SerializerInclude(xmlAsAttribute: true, omitIfDefault: true)] public byte character_type;           //todo: enum
		[SerializerInclude(xmlAsAttribute: true, omitIfDefault: true)] public byte character_special_outfit; //todo: enum

		[SerializerInclude(xmlAsAttribute: true, omitIfDefault: true)] public string vehicle_file_name;
		[SerializerInclude(xmlAsAttribute: true, omitIfDefault: true)] public byte vehicle_file_store; //todo: enum
		[SerializerInclude(xmlAsAttribute: true, omitIfDefault: true)] public bool vehicle_is_static;
		[SerializerInclude(xmlAsAttribute: true, omitIfDefault: true)] public bool vehicle_is_invulnerable;
		[SerializerInclude(xmlAsAttribute: true, omitIfDefault: true)] public bool vehicle_is_no_sleep;
		[SerializerInclude(xmlAsAttribute: true, omitIfDefault: true)] public bool vehicle_is_editable;
		[SerializerInclude(xmlAsAttribute: true, omitIfDefault: true)] public bool vehicle_is_show_on_map;
		[SerializerInclude(xmlAsAttribute: true, omitIfDefault: true)] public bool vehicle_is_transponder_active;

		[SerializerInclude(xmlAsAttribute: true, omitIfDefault: true)] public bool is_fire_lit;
		[SerializerInclude(xmlAsAttribute: true, omitIfDefault: true)] public bool is_explosion;
		[SerializerInclude(xmlAsAttribute: true, omitIfDefault: true)] public float explosion_magnitude;

		[SerializerInclude(xmlAsAttribute: true, omitIfDefault: true)] public float flare_hue;
		[SerializerInclude(xmlAsAttribute: true, omitIfDefault: true)] public bool is_flare_lit;
		


		[SerializerInclude(xmlAsAttribute: true, omitIfDefault: true)] public string vehicle_button_name;

		[SerializerInclude(xmlAsAttribute: true, omitIfDefault: true)] public byte ice_type; //todo: enum

		[SerializerInclude(xmlAsAttribute: true, omitIfDefault: true)] public float ice_scale;

		[SerializerInclude(nameOverride: "animal_type", xmlAsAttribute: true, omitIfDefault: true)]
		[EditorBrowsable(EditorBrowsableState.Never), Browsable(false)]
		private byte __animal_type
		{
			get => (byte) animal_type;
			set => animal_type = (AnimalType) value;
		}

		public AnimalType animal_type;

		[SerializerInclude(nameOverride: "creature_type", xmlAsAttribute: true, omitIfDefault: true)]
		[EditorBrowsable(EditorBrowsableState.Never), Browsable(false)]
		private byte __creature_type
		{
			get => (byte) creature_type;
			set => creature_type = (CreatureType) value;
		}

		public CreatureType creature_type;


		[SerializerInclude(omitIfDefault: true)] public Matrix4 spawn_transform;
		[SerializerInclude] public Bounds3 spawn_bounds;
		[SerializerInclude(omitIfDefault: true)] public Matrix4 vehicle_parent_transform;
		[SerializerInclude(omitIfDefault: true)] public Vector3 spawn_local_offset;

		[SerializerInclude("graph_links", optional: true)]
		public List<GraphLink> graph_links = new List<GraphLink>();


		protected Component()
		{
			id = -1;
		}

		public Component(ComponentType type): this()
		{
			component_type = type;
		}

		public Component(Location parent) : this()
		{
			parent.Add(this);
		}


		[OnDeserialized]
		public void OnDeserialized(StreamingContext dontCare)
		{
			graph_links = graph_links ?? new List<GraphLink>();
		}

		/// <summary>
		/// Remove the component from the current  <see cref="Location"/>, and add it to the given  <see cref="Location"/> (which can be null).
		/// </summary>
		/// <param name="newLocation"></param>
		public void MoveToLocation(Location newLocation)
		{
			if (newLocation == parentLocation) return;

			parentLocation?.Remove(this);
			newLocation?.Add(this);
		}

		/// <summary>
		/// Set the value if <see cref="is_environment"/>. A new <see cref="Location"/> will be created if a suitable one does not exist yet.
		/// </summary>
		/// <param name="new_is_environment"></param>
		/// <returns></returns>
		public void SetIsEnvironment(bool new_is_environment)
		{
			if (is_environment == new_is_environment) return;

			var new_location = parentPlaylist.GetOrCreateLocation(parentLocation.tile, parentLocation.tileName, new_is_environment);

			MoveToLocation(new_location);
		}

		public void Remove()
		{
			parentLocation.Remove(this);
		}

		/// <inheritdoc />
		public override string ToString()
		{
			return $"{component_type} #{id} '{display_name}'";
		}


		public static Dictionary<ComponentType, bool> MissionComponents = new Dictionary<ComponentType, bool>()
		{
			{ComponentType.MissionZone    , true  },
			{ComponentType.Object         , true  },
			{ComponentType.Character      , true  },
			{ComponentType.Vehicle        , true  },
			{ComponentType.Flare          , true  },
			{ComponentType.Fire           , true  },
			{ComponentType.Loot           , true  },
			{ComponentType.Button         , true  },
			{ComponentType.Animal         , true  },
			{ComponentType.Ice            , true  },
			{ComponentType.EnvironmentZone, false },
			{ComponentType.GraphNode      , false },
			{ComponentType.Creature       , true  },
		};

		public static Dictionary<ComponentType, bool> EnvironmentComponents = new Dictionary<ComponentType, bool>()
		{
			{ComponentType.MissionZone    , false },
			{ComponentType.Object         , true  },
			{ComponentType.Character      , true  },
			{ComponentType.Vehicle        , true  },
			{ComponentType.Flare          , true  },
			{ComponentType.Fire           , true  },
			{ComponentType.Loot           , true  },
			{ComponentType.Button         , false },
			{ComponentType.Animal         , true  },
			{ComponentType.Ice            , true  },
			{ComponentType.EnvironmentZone, true  },
			{ComponentType.GraphNode      , true  },
			{ComponentType.Creature       , true  },
		};

		[PatchSerializationSpec]
		public static void PatchSerializationSpecification(TypeSerializationSpecification spec)
		{
			spec.Members.Find(m => m.RealName == nameof(spawn_transform)).DefaultValue = Matrix4.Identity;
			spec.Members.Find(m => m.RealName == nameof(vehicle_parent_transform)).DefaultValue = Matrix4.Identity;
		}
	}
}
