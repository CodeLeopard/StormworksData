﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



namespace DataModel.Missions
{
	public enum ComponentType
	{
		MissionZone     =  0,
		Object          =  1,
		Character       =  2,
		Vehicle         =  3, //also ENV_Vehicle
		Flare           =  4,
		Fire            =  5,
		Loot            =  6,
		Button          =  7,
		Animal          =  8,
		Ice             =  9,
		EnvironmentZone = 10,
		GraphNode       = 11,
		Creature        = 12,
	}

	public enum ObjectType
	{
		None             = 0,
		Character        ,
		CrateSmall       ,
		Collectable      ,
		Basketball       ,
		Television       ,
		Barrel           ,
		Schematic        ,
		Debris           ,
		Chair            ,
		TrolleyFood      ,
		TrolleyMed       ,
		Clothing         ,
		OfficeChair      ,
		Book             ,
		Bottle           ,
		Fryingpan        ,
		Mug              ,
		Saucepan         ,
		Stool            ,
		Telescope        ,
		Log              ,
		Bin              ,
		Book2            ,
		Loot             ,
		BlueBarrel       ,
		BuoyancyRing     ,
		Container        ,
		GasCanister      ,
		Pallet           ,
		StorageBin       ,
		FireExtinguisher ,
		TrolleyTool      ,
		Cafetiere        ,
		DrawersTools     ,
		Glass            ,
		Microwave        ,
		Plate            ,
		BoxClosed        ,
		BoxOpen          ,
		DeskLamp         ,
		EraserBoard      ,
		Folder           ,
		Funnel           ,
		Lamp             ,
		Microscope       ,
		Notebook         ,
		PenMarker        ,
		Pencil           ,
		Scales           ,
		ScienceBeaker    ,
		ScienceCylinder  ,
		ScienceFlask     ,
		Tub_1            ,
		Tub_2            ,
		FileStack        ,
		BarrelToxic      ,
		Flare            ,
		Fire             ,
		Animal           ,
		MapLabel         ,
		Iceberg          ,
		SmallFlare       ,
		BigFlare         ,
	}

	public enum AnimalType
	{
		Shark    = 0,
		Whale       ,
		Seal        ,
		Penguin     ,
		Megalodon   ,
		//Kraken      , // Rumored to be impossible to spawn.
	}

	public enum ZoneShape
	{
		Box    = 0,
		Radius = 1,
		Sphere = 2, //todo: does this exist?
	}

	public enum CharacterOutfit
	{
		Worker = 1,
		Fishing,
		Waiter,
		Swimsuit,
		Military,
		Office,
		Police,
		Scientist,
		Medical,
		Wetsuit,
		Civilian,
	}

	public enum CreatureType
	{
		Badger_Common =  0,
		Bear_Grizzly =  1,
		Bear_Black =  2,
		Bear_Polar =  3,
		Chicken_Barnevelder =  4,
		Chicken_Marans =  5,
		Chicken_Orpington_fowl =  6,
		Chicken_Sussex_fowl =  7,
		Cow_Angus =  8,
		Cow_Hereford =  9,
		Cow_Highland = 10,
		Cow_Holstein = 11,
		Sasquatch = 12,
		Yeti = 13,
		Deer_Red_F = 14,
		Deer_Red_M = 15,
		Deer_Sika_F = 16,
		Deer_Sika_M = 17,
		Dog_Beagle = 18,
		Dog_Border_collie = 19,
		Dog_Boxer = 20,
		Dog_Corgi = 21,
		Dog_Dachschund = 22,
		Dog_Dalmatian = 23,
		Dog_Dobermann = 24,
		Dog_German_shepherd = 25,
		Dog_Greyhound = 26,
		Dog_Jack_russell = 27,
		Dog_Labrador = 28,
		Dog_Newfoundland = 29,
		Dog_Pug = 30,
		Dog_Shiba = 31,
		Dog_Siberian_Husky = 32,
		Dog_St_Bernard = 33,
		Dog_Vizsla = 34,
		Dog_Yorkshire_Terrier = 35,
		Fox_Red = 36,
		Fox_Arctic = 37,
		Goat_Alpine = 38,
		Goat_Bengal = 39,
		Goat_Oberhasli = 40,
		Goat_Saanen = 41,
		Hare_Arctic = 42,
		Hare_Irish = 43,
		Hare_Mountain = 44,
		Horse_Clydesdale = 45,
		Horse_Friesian = 46,
		Horse_Haflinher = 47,
		Horse_Welara = 48,
		Muntjac_F = 49,
		Muntjac_M = 50,
		Penguin_Gentoo = 51,
		Pig_Angeln_Saddleback = 52,
		Pig_Old_Spot = 53,
		Pig_Tamworth = 54,
		Pig_Yorkshire = 55,
		Seal_Polar = 56,
		Sheep_Black_Nose = 57,
		Sheep_Highlander = 58,
		Sheep_Mule = 59,
		Wildcat_Dcottish = 60,
		Wolf_Srctic = 61,
		Wolf_Costal = 62,
		Wolf_Plains = 63,
	}
}
