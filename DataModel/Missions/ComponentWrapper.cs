﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using DataModel.Vehicles;
using OpenToolkit.Mathematics;

using Shared;

namespace DataModel.Missions
{
	public abstract class ComponentWrapper
	{
		protected Component wrappedComponent;
		public Component WrappedComponent => wrappedComponent;

		public readonly ComponentType componentType;

		// todo: I was too lazy to implement getting fields on the Unity side, so it's a property now.
		[VisibleProperty(isReadonly: true)]
		public ComponentType ComponentType => componentType;

		public readonly bool AllowedInMission;

		public readonly bool AllowedInEnvironment;


		[VisibleProperty]
		public bool IsEnvironment
		{
			get => wrappedComponent.is_environment;
			set => wrappedComponent.is_environment = value;
		}


		[VisibleProperty(isReadonly: true)]
		public int Id => wrappedComponent.id;

		[VisibleProperty]
		public string DisplayName
		{
			get => WrappedComponent.display_name;
			set => WrappedComponent.display_name = value;
		}

		// In-Editor the transform of the GameObject is used.
		public ref Matrix4 Transform => ref wrappedComponent.spawn_transform;




		/// <summary>
		/// Event that fires when the visual may need to change.
		/// </summary>
		public event Action VisualChanged;

		/// <summary>
		/// Raise <see cref="VisualChanged"/>.
		/// </summary>
		protected void RaiseVisualChanged()
		{
			VisualChanged?.Invoke();
		}

		/// <summary>
		/// Remove the component from the current location, and add it to the given location (which can be null).
		/// </summary>
		/// <param name="newLocation"></param>
		public void MoveToLocation(Location newLocation)
		{
			wrappedComponent.MoveToLocation(newLocation);
		}

		public void Remove()
		{
			wrappedComponent.Remove();
		}

		protected ComponentWrapper
		(
			Component     wrappedComponent
		  , ComponentType componentType
		  , bool          allowedInMission
		  , bool          allowedInEnvironment
		)
		{
			this.wrappedComponent = wrappedComponent ?? throw new ArgumentNullException(nameof(wrappedComponent));
			this.componentType = componentType;
			AllowedInMission = allowedInMission;
			AllowedInEnvironment = allowedInEnvironment;
		}

		protected static readonly Dictionary<ComponentType, Type> componentTypeMap;

		protected static readonly Type[] constructorTypes_Component = { typeof(Component) };
		protected static readonly Dictionary<Type, ConstructorInfo> constructorCache_Component = new Dictionary<Type, ConstructorInfo>();

		protected static readonly Type[] constructorTypes_Location = { typeof(Location) };
		protected static readonly Dictionary<Type, ConstructorInfo> constructorCache_Location = new Dictionary<Type, ConstructorInfo>();


		static ComponentWrapper()
		{
			// For the non-generic static Create() we need to ensure that the derived classes have run their static constructor.
			// Otherwise the dictionaries are empty, and the type will not be found.

			componentTypeMap = new Dictionary<ComponentType, Type>(); // So that if the static constructor didn't run Create will throw.

			var baseType = typeof(ComponentWrapper);
			var types = Assembly.GetExecutingAssembly()
			                    .GetTypes()
			                    .Where(t => t != baseType && t.IsSubclassOf(baseType));

			foreach (var type in types)
			{
				var initMethod = type.GetMethod("Initialize", BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Static);
				initMethod.Invoke(null, null);
			}

			if (componentTypeMap.Count == 0)
			{
				throw new Exception("Failed to initialize ComponentWrappers. You may need to run the CodeGenerator project and re-import the DataModel .dll files.");
			}
		}


		public static ComponentWrapper Create(Component c)
		{
			if (componentTypeMap.TryGetValue(c.component_type, out Type wrapperType))
			{
				return constructorCache_Component[wrapperType].Invoke(new object[] { c }) as ComponentWrapper;
			}
			else
			{
				throw new Exception($"Could not find wrapper for Component [{c.id}] '{c.display_name}' of type: {c.component_type}.");
			}
		}

		/// <summary>
		/// Create a <see cref="ComponentWrapper"/> around the given <see cref="Component"/>
		/// </summary>
		/// <typeparam name="TWrapper"></typeparam>
		/// <param name="c"></param>
		/// <returns></returns>
		public static TWrapper Create<TWrapper>(Component c) where TWrapper : ComponentWrapper
		{
			return constructorCache_Component[typeof(TWrapper)].Invoke(new object[] { c }) as TWrapper;
		}

		/// <summary>
		/// Create a new <see cref="Component"/> in the given <see cref="Location"/> and wrap it with <typeparamref name="TWrapper"/>.
		/// </summary>
		/// <typeparam name="TWrapper"></typeparam>
		/// <param name="l"></param>
		/// <returns></returns>
		public static TWrapper Create<TWrapper>(Location l) where TWrapper : ComponentWrapper
		{
			return constructorCache_Location[typeof(TWrapper)].Invoke(new object[] { l }) as TWrapper;
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public const string RaiseVisualChangedName = nameof(RaiseVisualChanged);
	}
}
