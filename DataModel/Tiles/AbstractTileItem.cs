// Copyright 2022-2024 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Xml.Linq;

using OpenToolkit.Mathematics;

using Shared;
using Shared.Exceptions;
using Shared.Serialization;
using Shared.Serialization.DataTypes;

namespace DataModel.Tiles
{
	/// <summary>
	/// The base for all items in a <see cref="Tile"/>.
	/// </summary>
	[Serializable]
	public abstract class AbstractTileItem
	{
		#region DataTypes
		/// <summary>
		/// Used to make objects only appear for special events.
		/// </summary>
		[Flags]
		public enum SeasonFlags : byte
		{
			/// <summary>
			/// The object should appear always.
			/// </summary>
			Always    = 0,
			/// <summary>
			/// The object should appear only during Halloween.
			/// </summary>
			Halloween = 1 << 0,
			/// <summary>
			/// The object should appear only during Christmas.
			/// </summary>
			Xmas      = 1 << 1,

			// This is a band-aid for the Editor not supporting flags enums properly.
			// By defining this member it'll work because all possible combinations of valid flags are now covered.
			/// <summary>
			/// Appear only during <see cref="Halloween"/> and <see cref="Xmas"/>.
			/// </summary>
			Both = Halloween | Xmas
		}
		#endregion DataTypes


		private string _id;

		/// <summary>
		/// The Unique ID of this object.
		/// </summary>
		[VisibleProperty]
		public string id
		{
			get => _id;
			set => SetID(value);
		}

		/// <summary>
		/// Set the <see cref="id"/> of the object to a new <paramref name="value"/>, while preventing name collisions.
		/// </summary>
		/// <param name="value"></param>
		/// <param name="forceRaiseEvent"></param>
		/// <exception cref="ArgumentException"></exception>
		public void SetID(string value, bool forceRaiseEvent = false)
		{
			if (null == tile)
			{
				_id = value;
				return;
			}

			if (! tile.TryChangeID(this, value))
			{
				return;

				// todo: configure this behaviour, looking also at LinkedTileProperty

				throw new ArgumentException
					($"Cannot change {nameof(id)} to '{value}' because that would cause a name collision.");
			}

			ChangedSinceLastSaved = true;
			RaiseIdChanged();
		}

		/// <summary>
		/// Set the id without any validation or event being raised.
		/// </summary>
		/// <param name="newValue"></param>
		internal void SetID_NoValidationNoEvent(string newValue)
		{
			_id = newValue;
		}

		public bool Removed { get; private set; } = false;

		#region Parent
		private LinkedTileProperty __parent;

		[VisibleProperty]
		public string parent_id
		{
			get => __parent.Name;
			set => __parent.Name = value;
		}

		/// <summary>
		/// Does not throw when not yet linked.
		/// </summary>
		public AbstractTileItem _parent => __parent._value;
		public AbstractTileItem parent
		{
			get => __parent.Value;
			set => __parent.Value = value;
		}

		public bool ParentHierarchyCycle => __parent.CycleDetected;

		private void ThrowIfSetParentWouldCycle(AbstractTileItem newParent)
		{
			if(ParentCycleDetector(this, newParent))
				throw new InvalidOperationException($"Setting parent of '{id}' to '{newParent.id}' would cause cycle.");
		}

		private static bool ParentCycleDetector(AbstractTileItem target, AbstractTileItem newParent)
		{
			_ = target ?? throw new ArgumentNullException(nameof(target));

			AbstractTileItem current = newParent;
			while (current != null)
			{
				if (current == target)
					return true;

				current = current.__parent._value;
			}
			return false;
		}

		#endregion Parent



		#region Purchase_Interactible
		private LinkedTileProperty __purchase_interactable;

		[VisibleProperty]
		public string purchase_interactable_id
		{
			get => __purchase_interactable.Name;
			set => __purchase_interactable.Name = value;
		}

		public AbstractTileItem _purchase_interactable => __purchase_interactable._value;
		public AbstractTileItem purchase_interactable
		{
			get => __purchase_interactable.Value;
			set => __purchase_interactable.Value = value;
		}

		#endregion Purchase_Interactible




		[VisibleProperty]
		public SeasonFlags seasonal_flags = SeasonFlags.Always;

		private Matrix4 lTransform = Matrix4.Identity;

#if DEBUG
		private static float debugLengthSquaredLimit = 0.001f;
#endif
		/// <summary>
		/// The Transform relative to <see cref="parent"/>.
		/// </summary>
		public Matrix4 LTransform
		{
			get => lTransform;
			set
			{
				if (value == lTransform) return;

#if DEBUG
				if ((value.ExtractScale() - Vector3.One).LengthSquared > debugLengthSquaredLimit
				 && this.GetType() == typeof(EditArea))
				{
					// For debugger to break here.
				}
#endif

				lTransform = value;
			}
		}

		/// <summary>
		/// The Transform relative to the World.
		/// It includes the <see cref="parent"/> hierarchy.
		/// </summary>
		public Matrix4 GTransform
		{
			get
			{
				if (parent != null && !__parent.CycleDetected)
				{
					return LTransform * parent.GTransform;
				}
				return LTransform;
			}
		}


		#region Name Based Linked Objects


		private readonly HashSet<AbstractTileItem> _children = new HashSet<AbstractTileItem>();

		public IReadOnlyCollection<AbstractTileItem> Children => _children;

		private readonly HashSet<AbstractTileItem> _sameObjectLinks = new HashSet<AbstractTileItem>();

		public IReadOnlyCollection<AbstractTileItem> SameObjectLinks => _sameObjectLinks;


		#endregion Name Based Linked Objects


		#region Meta Information

		/// <summary>
		/// The order of this element in the tile's list of elements (which is per type).
		/// It is set automatically when loading from file.
		/// <seealso cref="TileItemOrderComparer"/>.
		/// </summary>
		public int? ElementOrder { get; set; } = null;



		internal bool changedSinceLastSaved = false;

		/// <summary>
		/// Note: this does not track changes by itself (yet), it's up to the user to set the flag.
		/// Setting the flag will also set it on the parent <see cref="tile"/>.
		/// </summary>
		public bool ChangedSinceLastSaved
		{
			get => changedSinceLastSaved;
			set
			{
				if (value == changedSinceLastSaved) return;

				if (value && null != tile)
				{
					tile.ChangedSinceLastSaved = true;
				}
			}
		}


		private Tile _tile;

		public Tile tile
		{
			get => _tile;
			internal set
			{
				if (_tile == value) return;

				if (_tile != null)
				{
					var oldTile = _tile;
					_tile = null;
					oldTile.Remove(this);
				}

				_tile = value;
				value?.Add(this);

				Removed = value != null;

				ChangedSinceLastSaved = true;
			}
		}

		[VisibleProperty(isReadonly: true, nameOverride: "Tile")]
		public string TileName => _tile?.tileName;


		/// <summary>
		/// The name (used as type) of the XmlElement.
		/// </summary>
		public abstract string meta_elementName { get; }

		/// <summary>
		/// The name of the container that will contain this element.
		/// </summary>
		public abstract string meta_containerName { get; }

		/// <summary>
		/// Contains the attributes for use during construction. It is NOT kept up-to-date.
		/// </summary>
		internal readonly Dictionary<string, string> meta_constructor_attributes;
		#endregion Meta Information


		#region ToolkitData

		private bool loadIgnore = false;
		/// <summary>
		/// Used by the Editor program to detect items that it should not load,
		/// because those items ware generated by the editor using data outside of the Tile.
		/// </summary>
		public bool LoadIgnore
		{
			get => loadIgnore;
			set
			{
				if (value != loadIgnore)
				{
					ChangedSinceLastSaved = true;
				}
				loadIgnore = value;
			}
		}

		private static Regex autoIdFormatRegex = new Regex("(\\d+)$", RegexOptions.Compiled);

		private string _autoIDFormat;

		/// <summary>
		/// A string that is suffixed with a automatically generated number to create a unique <see cref="id"/>.
		/// </summary>
		[VisibleProperty]
		public string AutoIDFormat
		{
			get => _autoIDFormat;
			set
			{
				if(value == _autoIDFormat) return;

				if (string.IsNullOrWhiteSpace(value))
				{
					_autoIDFormat = null;
					return;
				}

				_autoIDFormat = value;

				if (tile != null)
				{
					id = tile.IdTransformation("", autoIdFormatRegex, _autoIDFormat + "0");
				}
			}
		}


		private bool? isTerrain = null;
		/// <summary>
		/// When defined signals to StormworksEditor to override the default 'is this a terrain object?'
		/// behaviour and override it with the provided value.
		/// This is useful if the automatic detection is wrong.
		/// When an object is considered 'terrain' it will not be selectable unless specifically enabled.
		/// </summary>
		/// <remarks>
		/// The game will ignore this attribute.
		/// </remarks>
		//[VisibleProperty] // todo: make the Editor support Nullable<T>, then uncomment this.
		public bool? IsTerrain
		{
			get => isTerrain;
			set
			{
				if (isTerrain != value)
				{
					ChangedSinceLastSaved = true;
				}

				isTerrain = value;
			}
		}


		private bool? autoSelectParentTile = null;
		/// <summary>
		/// When defined signals to StormworksEditor to set the automatic assignment of parent tole behaviour on or off (default is on).
		/// </summary>
		/// <remarks>
		/// The game will ignore this attribute.
		/// </remarks>
		//[VisibleProperty] // todo: make the Editor support Nullable<T>, then uncomment this.
		public bool? AutoSelectParentTile
		{
			get => autoSelectParentTile;
			set
			{
				if (autoSelectParentTile != value)
				{
					ChangedSinceLastSaved = true;
				}

				autoSelectParentTile = value;
			}
		}


		#endregion ToolkitData

		#region Construction

		public const string attr_id                       = "id";
		public const string attr_parent_id                = "parent_id";
		public const string attr_purchase_interactable_id = "purchase_interactable_id";
		public const string attr_seasonal_flags           = "seasonal_flags";
		public const string attr_autoIDFormat             = "_CT_autoIDFormat";
		public const string attr_isTerrain                = "_CT_isTerrain";
		public const string attr_loadIgnore               = "_CT_loadIgnore";
		public const string attr_autoSelectParentTile     = "_CT_autoSelectParentTile";


		protected AbstractTileItem()
		{
			__parent = new LinkedTileProperty(this,                nameof(__parent), cycleDetector: ParentCycleDetector);
			__purchase_interactable = new LinkedTileProperty(this, nameof(__purchase_interactable));
		}

		protected AbstractTileItem(XElement element)
			: this(element, element.Attributes().XAttributeToDictionary())
		{ }

		protected AbstractTileItem(XElement element, Dictionary<string, string> attributes)
			: this()
		{
			try
			{
				meta_constructor_attributes = attributes;
				id                       = attributes.GetString(attr_id) ?? throw new Exception($"Missing required attribute '{attr_id}'");
				parent_id                = attributes.GetString(attr_parent_id);
				purchase_interactable_id = attributes.GetString(attr_purchase_interactable_id);
				seasonal_flags           = (SeasonFlags) attributes.GetByte(attr_seasonal_flags);

				LTransform = MatrixSer.ParseMatrix4(element.Element("transform"), allowNull: true);

				_autoIDFormat = attributes.GetString(attr_autoIDFormat, null);

				isTerrain = attributes.GetNullableBool(attr_isTerrain);
				loadIgnore = attributes.GetBool(attr_loadIgnore);
				autoSelectParentTile = attributes.GetNullableBool(attr_autoSelectParentTile);
			}
			catch (Exception e)
			{
				throw new XmlDataException(element, e);
			}
		}

		/// <summary>
		/// Register the parent <see cref="Tile"/>.
		/// </summary>
		/// <param name="parentTile"></param>
		internal virtual void Register(Tile parentTile)
		{
			this._tile = parentTile;
		}

		/// <summary>
		/// Assign references to <see cref="AbstractTileItem"/>s that ware referenced in the xml by string id
		/// and could not be fetched because they might not have been loaded yet before.
		/// </summary>
		internal void LinkStringIdPropertiesToInstances()
		{
			__parent.Fetch();
			if (null != __parent.Name && null == __parent._value)
			{
				Console.WriteLine($"[Warning] {meta_elementName} '{id}' in {tile.tileName} could not find parent '{__parent.Name}'.");
			}
			__purchase_interactable.Fetch();
			if (null != __purchase_interactable.Name && null == __purchase_interactable._value)
			{
				Console.WriteLine($"[Warning] {meta_elementName} '{id}' in {tile.tileName} could not find purchase_interactable '{__purchase_interactable.Name}'.");
			}

			DerivedLinkStringIdPropertiesToInstances();
		}

		/// <inheritdoc cref="LinkStringIdPropertiesToInstances"/>
		protected virtual void DerivedLinkStringIdPropertiesToInstances()
		{

		}

		/// <summary>
		/// Add ourselves to the list of children of our <see cref="parent"/>.
		/// </summary>
		internal void SetupParent()
		{
			if (null != __parent._value)
			{
				//ThrowIfSetParentWouldCycle(__parent._value);
			}

			__parent._value?.AddChild(this);
		}

		internal void SetupSameObjectLinks()
		{
			FindSameObjectLinks();
		}

		internal void AfterInitialLinking()
		{
			DerivedAfterInitialLinking();
		}

		protected virtual void DerivedAfterInitialLinking()
		{

		}

		#endregion Construction


		private void RaiseIdChanged()
		{
			FindSameObjectLinks(force: true);

			IDChanged?.Invoke(this);
		}

		public event Action<AbstractTileItem> IDChanged;

		/// <summary>
		/// Remove this object from the <see cref="Tile"/>, making sure the state of the tile remains valid.
		/// This will call <see cref="RemoveSelf"/> on <see cref="Children"/>.
		/// Complex objects should also remove references to other objects so that the state of the tile remains valid.
		/// </summary>
		public void RemoveSelf()
		{
			if (Removed)
			{
				Console.WriteLine($"Object {id} already removed.");
				return;
			}
			OverrideRemoveSelf();

			foreach(var child in Children)
			{
				child.RemoveSelf();
			}
			tile.Remove(this);
			Removed = true;
		}

		/// <summary>
		/// Override to run code before <see cref="RemoveSelf()"/>
		/// </summary>
		protected virtual void OverrideRemoveSelf()
		{
			// virtual
		}


		#region Child Management

		public bool AddChild(AbstractTileItem child)
		{
			if (child.parent == null)
			{
				child.parent = this;
			}
			else if (child.parent != this)
			{
				child.parent.RemoveChild(child);
				child.parent = this;
			}
			child.tile = tile;
			return _children.Add(child);
		}

		public bool RemoveChild(AbstractTileItem child)
		{
			child.parent = null;
			return _children.Remove(child);
		}

		#endregion Child Management
		#region NameLinks
		/*
		Options:
		mesh\d+
			physics_mesh\d+


		




		 */

		/// <summary>
		/// Interactable Linking does not work well enough to be reliable.
		/// Note we use a const bool rather than a #if
		/// because an #if allows your code to break in refactorings such as renames.
		/// Doing it like this ensure that that bit of code must compile,
		/// even if it will be stripped from the build by the compiler in the end.
		/// </summary>
		protected const bool NoInteractableSameObjectLink = true;

		private static readonly string[] sameObjectLinkPrefixes = new[] { "", "physics_", };
		public static readonly IReadOnlyCollection<string> ObjectLinkPrefixes;

		private static readonly string[] sameObjectLinkSuffixes = new[] { "","_mesh", "_phys", "_physics", "_interact", };
		public static readonly IReadOnlyCollection<string> ObjectLinkSuffixes;


		/// <summary>
		/// Prevent frequent allocations related to the names.
		/// </summary>
		private static readonly ThreadLocal<List<AbstractTileItem>> __buffer
			= new ThreadLocal<List<AbstractTileItem>>(() => new List<AbstractTileItem>());

		/// <summary>
		/// Search for Items with the same name (allowing for some defined suffixes),
		/// for the purpose of linking these items together.
		/// Example: <see cref="MeshInfo"/> and <see cref="PhysInfo"/> should be linked so that they stay together.
		/// </summary>
		protected void FindSameObjectLinks(bool force = false)
		{
			if (! (force || this is MeshInfo)) return;


			ClearSameLinks();

			foreach (var item in _children)
			{
				//AddLink(item);
			}

			// Generate a id to search without any suffix.
			string searchId = GetCleanId(_id);


			void TryLink(AbstractTileItem item)
			{
				if (item == this)
					return;

				if (item is Interactable inter)
				{
#pragma warning disable 0162 // We keep this code alive so that renames and other refactorings don't break things.
					// ReSharper disable once ConditionIsAlwaysTrueOrFalse
					if (NoInteractableSameObjectLink) return;

					// The InteractZone is just a box, and thus does not need to align or scale with the mesh.

					float distance = (GTransform.Row3.Xyz - item.GTransform.Row3.Xyz).LengthSquared;

					if (distance > 2f * 2f)
						return;
#pragma warning restore 0162
				}
				else
				{
					if (! GTransform.ApproxEquals(item.GTransform, 1f))
						return;
				}

				AddSameObjectLink(item);
			}


			foreach (string nameSuffix in sameObjectLinkSuffixes)
			{
				if(tile.Elements.TryGetValue(searchId + nameSuffix, out var item))
				{
					TryLink(item);
				}
			}

			foreach (string namePrefix in sameObjectLinkPrefixes)
			{
				if (tile.Elements.TryGetValue(namePrefix + searchId, out var item))
				{
					TryLink(item);
				}
			}

			// Derived class may define additional mechanisms to use for matching.
			FindSameObjectLinks(searchId);

			if (this is MeshInfo && _sameObjectLinks.Count < 1)
			{
				//Console.WriteLine($"MeshInfo '{id}' in '{tile.fileName}' did not find matching Phys");
			}
		}

		protected virtual void FindSameObjectLinks(string searchId)
		{

		}

		/// <summary>
		/// Returns a new <see cref="string"/> with the prefixes and suffixes used for SameObjectLinking removed.
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		private string GetCleanId(string id)
		{
			foreach (string nameSuffix in sameObjectLinkSuffixes)
			{
				if (nameSuffix == string.Empty) continue;

				id = id.Replace(nameSuffix, string.Empty);
			}

			foreach (string namePrefix in sameObjectLinkPrefixes)
			{
				if (namePrefix == string.Empty) continue;

				id = id.Replace(namePrefix, string.Empty);
			}

			return id;
		}


		public bool AddSameObjectLink(AbstractTileItem item)
		{
			bool added = _sameObjectLinks.Add(item);
			if(added) item._sameObjectLinks.Add(this);
			return added;
		}

		public bool HasSameObjectLinkTo(AbstractTileItem item)
		{
			return _sameObjectLinks.Contains(item);
		}

		public bool RemoveSameObjectLink(AbstractTileItem item)
		{
			bool removed = _sameObjectLinks.Remove(item);
			if(removed) item._sameObjectLinks.Remove(this);
			return removed;
		}

		public void ClearSameLinks()
		{
			__buffer.Value.Clear();
			foreach (AbstractTileItem item in _sameObjectLinks)
			{
				__buffer.Value.Add(item);
			}
			foreach (AbstractTileItem item in __buffer.Value)
			{
				RemoveSameObjectLink(item);
			}
			__buffer.Value.Clear();
		}



		#endregion NameLinks

		/// <summary>
		/// Create a new XElement to represent this.
		/// </summary>
		/// <returns></returns>
		public XElement ToXElement()
		{
			var element = new XElement(meta_elementName);
			element.SetAttributeValue(attr_id,                       id);
			element.SetAttributeValue(attr_parent_id,                parent_id);
			element.SetAttributeValue(attr_purchase_interactable_id, purchase_interactable_id);
			element.SetAttributeValueIND(attr_seasonal_flags,        (byte)seasonal_flags);
			element.SetAttributeValue(attr_autoIDFormat,             AutoIDFormat);
			if (IsTerrain.HasValue)
			{
				element.SetAttributeValue(attr_isTerrain, isTerrain.Value);
			}

			if (loadIgnore)
			{
				element.SetAttributeValue(attr_loadIgnore, loadIgnore);
			}

			if (autoSelectParentTile.HasValue)
			{
				element.SetAttributeValue(attr_autoSelectParentTile, autoSelectParentTile.Value);
			}

			element.Add(LTransform.ToXElement());

			DerivedToXElement(element);

			return element;
		}

		/// <summary>
		/// Add the data stored in the derived class into the XElement.
		/// </summary>
		/// <param name="baseElement"></param>
		protected abstract void DerivedToXElement(XElement baseElement);

		/// <summary>
		/// Appends the common elements like <see cref="id"/>, <see cref="parent_id"/>, etc. to <paramref name="sb"/>.
		/// </summary>
		/// <param name="sb"></param>
		/// <param name="open">Include the opening curly brace.</param>
		public void ToStringCommonStart(StringBuilder sb, bool open = true)
		{
			if(open)
				sb.Append("{");

			sb.Append($" {attr_id}:'{id}'");

			if(! string.IsNullOrEmpty(parent_id))
				sb.Append($" {attr_parent_id}:'{parent_id}'");

			if(! string.IsNullOrEmpty(purchase_interactable_id))
				sb.Append($" {attr_purchase_interactable_id}:'{purchase_interactable_id}'");
		}

		/// <summary>
		/// Appends the <see cref="LTransform"/> as position, scale and rotation to <paramref name="sb"/> and finally a closing curly brace.
		/// </summary>
		/// <param name="sb"></param>
		/// <param name="close">Include the closing curly brace.</param>
		public void ToStringCommonEnd(StringBuilder sb, bool close = true)
		{
			sb.Append($" @{LTransform.ExtractTranslation()}");
			Vector3 scale = LTransform.ExtractScale();
			if (scale != Vector3.One)
			{
				sb.Append($" scale:{scale}");
			}

			Quaternion rotation = LTransform.ExtractRotation(true);
			if (rotation != Quaternion.Identity)
			{
				sb.Append($" rotation: {rotation.ToEulerAngles() * 180 / (float)Math.PI}�");
			}

			if(close)
				sb.Append("}");
		}

		/// <inheritdoc />
		public override string ToString()
		{
			var sb = new StringBuilder();
			ToStringCommonStart(sb);
			ToStringCommonEnd(sb);
			return sb.ToString();
		}


		#region Static

		/// <summary>
		/// Map from a <see cref="Type"/> to a dummy instance of that type, used to fetch 'static' elements of the
		/// Concrete type that can be required to exist by an interface/inheritance
		/// but can't be static properties themselves because C# decided
		/// that there will never be a use-case for that.
		/// </summary>
		private static readonly Dictionary<Type, AbstractTileItem> typeMap = new Dictionary<Type, AbstractTileItem>();


		static AbstractTileItem()
		{
			ObjectLinkPrefixes = new ReadOnlyCollection<string>(sameObjectLinkPrefixes);
			ObjectLinkSuffixes = new ReadOnlyCollection<string>(sameObjectLinkSuffixes);

			foreach (var type in Assembly.GetExecutingAssembly()
			                             .GetTypes()
			                             .Where(
			                                    t => t.IsSubclassOf(typeof(AbstractTileItem))
			                                      && ! t.IsAbstract
			                                   ))
			{
				typeMap.Add(type, (AbstractTileItem)Activator.CreateInstance(type));
			}
		}


		public static string GetXmlElementName<TConcreteType>()
			where TConcreteType : AbstractTileItem
		{
			return typeMap[typeof(TConcreteType)].meta_elementName;
		}

		public static string GetXmlElementName(Type t)
		{
			return typeMap[t].meta_elementName;
		}

		public static string GetXmlContainerName<TConcreteType>()
			where TConcreteType : AbstractTileItem
		{
			return typeMap[typeof(TConcreteType)].meta_containerName;
		}

		public static string GetXmlContainerName(Type t)
		{
			return typeMap[t].meta_containerName;
		}


		#endregion Static

		#region Sorting

		/// <summary>
		/// Sorts <seealso cref="AbstractTileItem"/>s such that the resulting order is:
		/// <br/> Elements with <see cref="ElementOrder"/>, sorted ascending and then by <see cref="id"/>.
		/// <br/> Elements without <see cref="ElementOrder"/>, sorted by <see cref="id"/>.
		/// <br/> Elements with <see cref="loadIgnore"/>, sorted separately but using all the previous rules.
		/// </summary>
		public static readonly IComparer<AbstractTileItem> OrderComparer = new TileItemOrderComparer();

		private class TileItemOrderComparer : IComparer<AbstractTileItem>
		{
			public int Compare(AbstractTileItem x, AbstractTileItem y)
			{
				if (ReferenceEquals(x,    y)) return 0;
				if (ReferenceEquals(null, y)) return 1;
				if (ReferenceEquals(null, x)) return -1;

				// LoadIgnore items are sorted behind non-LoadIgnore items.
				if (x.LoadIgnore && ! y.loadIgnore)
				{
					return 1;
				}
				if (! x.LoadIgnore && y.loadIgnore)
				{
					return -1;
				}

				int elementOrderComparison = OrderComparison(x.ElementOrder, y.ElementOrder);
				if (elementOrderComparison != 0) return elementOrderComparison;

				return string.Compare(x._id, y._id, StringComparison.Ordinal);
			}

			private static int OrderComparison(int? x, int? y)
			{
				// Items with defined order are sorted before items without defined order.
				if (! x.HasValue && y.HasValue)
				{
					return 1;
				}

				if (x.HasValue && ! y.HasValue)
				{
					return -1;
				}

				return Nullable.Compare(x, y);
			}
		}



		#endregion Sorting
	}
}