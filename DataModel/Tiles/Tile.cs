﻿// Copyright 2022-2023 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using Shared.Exceptions;
using Shared.Serialization;

[assembly: InternalsVisibleTo("Tests")]
namespace DataModel.Tiles
{
	public class Tile
	{
		/// <summary>
		/// The path to the tile.xml file as it's used in the Game (relative to the 'rom' folder)
		/// </summary>
		public string meta_romPath;

		/// <summary>
		/// The path to the tile_instances.xml file as it's used in the Game (relative to 'rom' folder).
		/// Created on the fly based on <see cref="meta_romPath"/>.
		/// </summary>
		public string meta_instancePath => GetInstancesPath(meta_romPath);


		#region Attributes

		/// <summary>
		/// The name of the file this definition was read from (without extension)
		/// </summary>
		public string fileName;

		/// <summary>
		/// The name of the tile, this returns <see cref="fileName"/> because no such property is implemented in the xml.
		/// </summary>
		public string tileName => fileName;

		public bool is_island;
		public int tile_type;
		public bool is_purchasable;
		public int purchase_cost;
		public float sea_floor_height;


		#endregion Attributes


		#region Content


		public IEnumerable<MeshInfo> meshes => elements.Values.OfType<MeshInfo>();
		// physics_objects
		public IEnumerable<PhysInfo> physicsMeshes => elements.Values.OfType<PhysInfo>();
		// physics_dynamics
		public IEnumerable<OmniLight> lights_omni => elements.Values.OfType<OmniLight>();
		public IEnumerable<SpotLight> lights_spot => elements.Values.OfType<SpotLight>();
		public IEnumerable<TubeLight> lights_tube => elements.Values.OfType<TubeLight>();
		// screens
		public IEnumerable<EditArea> edit_areas => elements.Values.OfType<EditArea>();
		public IEnumerable<Interactable> interactables => elements.Values.OfType<Interactable>();
		// door_logic_nodes
		// light_logic_nodes
		// rotation_logic_nodes
		// hangar_door_logic_nodes
		// audio_logic_nodes
		// tile_markers
		// mission_zones
		// dynamic_markers
		// ladders
		// perches
		// fires
		public IEnumerable<TrainTrackNode> trackNodes => elements.Values.OfType<TrainTrackNode>();
		// iceberg_markers


		#endregion Content

		private readonly Dictionary<string, AbstractTileItem> elements = new Dictionary<string, AbstractTileItem>();

		public readonly IReadOnlyDictionary<string, AbstractTileItem> Elements;


		/// <summary>
		/// Enumerates all currently implemented collections of <see cref="AbstractTileItem"/>
		/// </summary>
		/// <returns></returns>
		public IEnumerable<AbstractTileItem> AllTileItems() => Elements.Values;

		/// <summary>
		/// Enumerates all currently implemented collections of <see cref="AbstractTileItem"/>
		/// Giving a ValueTuple of the Type that is in the collection.
		/// </summary>
		/// <returns></returns>
		public IEnumerable<(Type, IEnumerable<AbstractTileItem>)> AllTileItemsTyped()
		{
			yield return (typeof(MeshInfo), meshes);
			yield return (typeof(PhysInfo), physicsMeshes);
			yield return (typeof(OmniLight), lights_omni);
			yield return (typeof(SpotLight), lights_spot);
			yield return (typeof(TubeLight), lights_tube);
			yield return (typeof(EditArea), edit_areas);
			yield return (typeof(Interactable), interactables);
			yield return (typeof(TrainTrackNode), trackNodes);
		}

		#region ChangedEvents

		internal bool changedSinceLastSaved = false;


		/// <summary>
		/// Note: this does not track all changes by itself (yet), it's up to the user to set the flag.
		/// Setting the flag false will also apply that to all <see cref="AbstractTileItem"/> in <see cref="Elements"/>.
		/// </summary>
		public bool ChangedSinceLastSaved
		{
			get => changedSinceLastSaved;
			set
			{
				if (value == changedSinceLastSaved) return;

				changedSinceLastSaved = value;
				if (! value)
				{
					MarkUnChanged();
				}

				ChangedSinceLastSavedChanged?.Invoke(this, value);
			}
		}

		private void MarkUnChanged()
		{
			foreach (var item in elements.Values)
			{
				item.changedSinceLastSaved = false;
			}
		}

		/// <summary>
		/// Event that triggers when the value of <see cref="ChangedSinceLastSaved"/> changes.
		/// </summary>
		public event Action<Tile, bool> ChangedSinceLastSavedChanged;

		#endregion ChangedEvents
		#region Construction

		private List<AbstractTileItem> _constructionBuffer;

		protected Tile()
		{
			Elements = new ReadOnlyDictionary<string, AbstractTileItem>(elements);
		}

		public Tile(XDocument document, string meta_romPath = null) : this(document.Element("definition"), meta_romPath) { }

		public Tile(XElement tileElement, string meta_romPath = null) : this()
		{
			try
			{
				this.meta_romPath = meta_romPath;
				if (null != meta_romPath)
				{
					this.fileName = Path.GetFileNameWithoutExtension(meta_romPath);
				}

				var attributes = tileElement.Attributes().XAttributeToDictionary();

				is_island        = attributes.GetBool("is_island");
				tile_type        = attributes.GetInt32("tile_type");
				is_purchasable   = attributes.GetBool("is_purchasable");
				purchase_cost    = attributes.GetInt32("purchase_cost");
				sea_floor_height = attributes.GetFloat("sea_floor_height", -6);

				_constructionBuffer = new List<AbstractTileItem>();

				ParseTileItemCommon<MeshInfo>(tileElement);
				ParseTileItemCommon<PhysInfo>(tileElement);
				ParseTileItemCommon<OmniLight>(tileElement);
				ParseTileItemCommon<SpotLight>(tileElement);
				ParseTileItemCommon<TubeLight>(tileElement);
				ParseTileItemCommon<EditArea>(tileElement);
				ParseTileItemCommon<Interactable>(tileElement);
				ParseTileItemCommon<TrainTrackNode>(tileElement);

				SetupBookKeeping();

				ChangedSinceLastSaved = false;
			}
			catch (Exception e)
			{
				throw XmlDataException.AutoFormatDataName(nameof(Tile), tileElement, e);
			}
		}

		private void ParseTileItemCommon<TTileItem>(XElement tileElement)
			where TTileItem : AbstractTileItem, new()
		{
			string collectionElementName = AbstractTileItem.GetXmlContainerName<TTileItem>();
			XElement collectionElement = null;
			try
			{
				collectionElement = tileElement.Element(collectionElementName);

				int order = 0;
				foreach (XElement element in collectionElement.Elements())
				{
					// todo? performance.
					var item = (TTileItem)Activator.CreateInstance(typeof(TTileItem), element);

					item.ElementOrder = order++;
					_constructionBuffer.Add(item);
				}
			}
			catch (Exception e)
			{
				if (collectionElement != null)
				{
					throw new XmlDataException(collectionElement, e);
				}

				throw new XmlDataException(tileElement, e);
			}
		}


		private void SetupBookKeeping()
		{
			try
			{
				foreach (var item in _constructionBuffer)
				{
					item.Register(this);

					string oldId = item.id;
					string id = IdTransformation(IdConflictSuffix, IdConflictRegex, oldId);

					if (id != item.id)
					{
						Console.WriteLine($"[{nameof(Tile)}]::[{nameof(SetupBookKeeping)}] Tile '{tileName}' encountered duplicated id '{item.id}', will assign '{id}' instead.");
					}

					elements.Add(id, item);

					// ID is already checked to be valid.
					// At this point we should not trigger events.
					item.SetID_NoValidationNoEvent(id);
				}

				_constructionBuffer.Clear();
				_constructionBuffer = null;

				foreach (var item in AllTileItems())
				{
					item.LinkStringIdPropertiesToInstances();
				}

				foreach (var item in AllTileItems())
				{
					item.SetupParent();
				}

				foreach (var item in AllTileItems())
				{
					item.SetupSameObjectLinks();
				}

				foreach (var item in AllTileItems())
				{
					item.AfterInitialLinking();
				}
			}
			catch (Exception e)
			{
				throw new InternalLinkingFailedException("Failed to setup internal BookKeeping.", e);
			}
		}

		#endregion Construction

		/// <summary>
		/// Change <paramref name="original_id"/> by appending <paramref name="suffix"/> and a unique number, using <paramref name="regex"/> to find the number, which must be in capture group 1.
		/// </summary>
		/// <param name="suffix"></param>
		/// <param name="regex"></param>
		/// <param name="original_id"></param>
		/// <returns></returns>
		internal string IdTransformation(string suffix, Regex regex, string original_id)
		{
			string id = original_id;
			while (elements.ContainsKey(id))
			{
				var match = regex.Match(id);

				if (! match.Success)
				{
					id = id + suffix + "0";
					continue;
				}

				string sub = id.Substring(0, id.Length - match.Groups[1].Length);

				id = sub + (int.Parse(match.Groups[1].Value) + 1);
			}

			return id;
		}


		#region Modification

		/// <summary>
		/// The suffix, followed by an number, that will be added to an items id
		/// when it conflicts with an existing id.
		/// Only used during loading.
		/// </summary>
		public const string IdConflictSuffix = "_IdConflicted_";

		/// <summary>
		/// <see cref="AbstractTileItem"/>s that had a conflicting <see cref="AbstractTileItem.id"/>
		/// during loading will match this <see cref="Regex"/>.
		/// Capture group 1 holds the number that is added to the suffix (<see cref="IdConflictSuffix"/>).
		/// </summary>
		public static readonly Regex IdConflictRegex
			= new Regex(IdConflictSuffix + "(\\d+)$", RegexOptions.Compiled);

		/// <summary>
		/// The suffix (followed by a number not included here) that will be added to
		/// <see cref="AbstractTileItem.id"/> when it is <see cref="Add"/>ed and there is a name conflict.
		/// </summary>
		public const string MovedInSuffix = "_Transferred_";

		/// <summary>
		/// <see cref="AbstractTileItem"/>s that ware <see cref="Add"/>ed and had a conflicting <see cref="AbstractTileItem.id"/>
		/// will match this <see cref="Regex"/>.
		/// Capture group 1 holds the number that is added to the suffix (<see cref="MovedInSuffix"/>).
		/// </summary>
		public static readonly Regex MovedInRegex
			= new Regex(MovedInSuffix+"(\\d+)$", RegexOptions.Compiled);

		/// <summary>
		/// Add <paramref name="item"/> to this <see cref="Tile"/>.
		/// If there is a <see cref="AbstractTileItem.id"/> conflict <paramref name="item"/> will be renamed.
		/// </summary>
		/// <param name="item">The <see cref="AbstractTileItem"/> to add.</param>
		/// <returns><see langword="true"/> when the item was added, <see langword="false"/> when it was already present.</returns>
		public bool Add(AbstractTileItem item)
		{
			bool isTransfer = false;
			if (null != item.tile)
			{
				if (item.tile == this) return false;

				isTransfer = item.tile.Remove(item);
			}

			string id = item.id;
			if (elements.ContainsKey(id) || isTransfer)
			{
				id = IdTransformation(MovedInSuffix, MovedInRegex, item.id);
			}

			elements.Add(id, item);
			item.tile = this;

			// First set the ID without validation or event.
			// Because normally we will do name change validation.
			// But because we are new we already validated the id.
			item.SetID_NoValidationNoEvent(id);
			// Now set the ID again to force trigger any events
			// that should happen when the id changes.
			item.SetID(id, forceRaiseEvent:true);


			ChangedSinceLastSaved = true;

			return true;
		}

		/// <summary>
		/// Add all items to the tile using <see cref="Add"/>.
		/// </summary>
		/// <param name="items"></param>
		public void AddMany(IEnumerable<AbstractTileItem> items)
		{
			foreach (var item in items)
			{
				Add(item);
			}
		}

		/// <summary>
		/// This method is only intended for use by <see cref="AbstractTileItem.SetID"/>.
		/// </summary>
		/// <param name="item"></param>
		/// <param name="newID"></param>
		/// <returns><see langword="true"/> when the id was changed, <see langword="false"/> when it was not.</returns>
		internal bool TryChangeID(AbstractTileItem item, string newID)
		{
			if (item.id == newID)
				return true;

			if (elements.ContainsKey(newID))
			{
				return false;
			}

			elements.Remove(item.id);

			item.SetID_NoValidationNoEvent(newID);

			elements.Add(item.id, item);
			return true;
		}

		/// <summary>
		/// Remove the given <paramref name="item"/> from this <see cref="Tile"/>.
		/// </summary>
		/// <param name="item">The <see cref="AbstractTileItem"/> to remove</param>
		/// <returns><see langword="true"/> when the item was removed, <see langword="false"/> when it was not present.</returns>
		public bool Remove(AbstractTileItem item)
		{
			if (elements.Remove(item.id))
			{
				item.ChangedSinceLastSaved = true;
				item.tile = null;

				return true;
			}

			return false;
		}


		#endregion Modification



		#region Saving

		private const string ToDocumentExceptionMessage =
			"Not all data of a Tile is implemented, so we cannot convert to a full XDocument or XElement. "
		  + "Use SaveDataToDocument instead to save the data held in this instance to an existing Document.";
		public XDocument ToXDocument()
		{
			throw new NotImplementedException(ToDocumentExceptionMessage);
		}

		public XElement ToXElement()
		{
			throw new NotImplementedException(ToDocumentExceptionMessage);
		}

		/// <summary>
		/// Save the information in this instance into <paramref name="doc"/>
		/// by replacing the attributes and (implemented) containers.
		/// Unimplemented parts are not modified.
		/// </summary>
		/// <param name="doc"></param>
		public void SaveDataToDocument(XDocument doc)
		{
			var def = doc.Element("definition");

			SaveAttributes(def);

			SaveElements(def);

			ChangedSinceLastSaved = false;
		}

		private void SaveAttributes(XElement e)
		{
			e.RemoveAttributes();

			e.SetAttributeValue("is_island", is_island);
			e.SetAttributeValue("tile_type", tile_type);
			e.SetAttributeValueIND("is_purchasable", is_purchasable, false);
			e.SetAttributeValueIND("purchase_cost", purchase_cost, 0);
			e.SetAttributeValueIND("sea_floor_height", sea_floor_height, -6);
		}


		private void SaveElements(XElement def)
		{
			var containerMap = new Dictionary<string, XElement>();

			// Remove any elements stored in the xml.
			foreach (var tuple in AllTileItemsTyped())
			{
				var type = tuple.Item1;
				var container = def.Element(AbstractTileItem.GetXmlContainerName(type));
				container.RemoveNodes();

				containerMap.Add(container.Name.LocalName, container);
			}

			// Save the elements stored in the tile to the xml.
			foreach (AbstractTileItem item in Elements.Values.OrderBy(
			                                                          i => i,
			                                                          AbstractTileItem.OrderComparer))
			{
				if (! containerMap.TryGetValue(item.meta_containerName, out XElement container))
				{
					// If there is no container for the given item make a new one.

					container = new XElement(item.meta_containerName);
					def.Add(container);
					containerMap.Add(container.Name.LocalName, container);
				}
				container.Add(item.ToXElement());
			}
		}


		#endregion Saving

		/// <inheritdoc />
		public override string ToString()
		{
			return $"{{{nameof(Tile)} '{fileName}'}}";
		}

		#region Static

		/// <summary>
		/// Regex to identify (tree) instances files, which only contain trees.
		/// </summary>
		public static Regex InstancesRegex = new Regex("_instances(\\.xml)?$", RegexOptions.Compiled);

		/// <summary>
		/// Regex to identify rotations of tiles that the game likes to save for whatever reason.
		/// </summary>
		public static Regex RotationsRegex = new Regex("_rot-\\d(\\.xml)?$", RegexOptions.Compiled);

		/// <summary>
		/// Returns the path to the (tree) instances file for the given <paramref name="path"/> to a tile.
		/// Only transforms the given path: does not interact with the filesystem.
		/// </summary>
		/// <param name="path"></param>
		/// <returns></returns>
		public static string GetInstancesPath(string path) => $"{Path.ChangeExtension(path, null)}_instances.xml";


		#endregion Static
	}
}
