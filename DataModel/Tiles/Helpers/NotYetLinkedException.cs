﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace DataModel.Tiles
{
	/// <summary>
	/// Exception that is thrown when attempting to access the value of a <see cref="LinkedTileProperty"/> while
	/// that is not yet initialized (because the <see cref="AbstractTileItem.tile"/> value has not been assigned yet.
	/// </summary>
	[Serializable]
	public class NotYetLinkedException : Exception
	{
		private NotYetLinkedException()
		{
		}

		public NotYetLinkedException(string message)
			: base($"Cannot get the value of '{message}' because value can't be fetched until the parent tile is assigned. ")
		{
		}

		protected NotYetLinkedException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
