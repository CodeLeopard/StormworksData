﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Text;

namespace DataModel.Tiles
{
	/// <summary>
	/// Represents a value on a <see cref="AbstractTileItem"/> that is a
	/// <see cref="string"/> that links to another <see cref="AbstractTileItem"/>.
	/// The <see cref="LinkedTileProperty"/> will automatically keep that
	/// <see cref="string"/> value and linked <see cref="AbstractTileItem"/> in sync with each other.
	/// What happens when a <see cref="AbstractTileItem"/> with the given name cannot be found is configurable
	/// by specifying <see cref="SetNameOrUnchanged"/>, <see cref="SetNameOrThrow"/> or <see cref="SetNameRegardless"/>
	/// to the constructor.
	/// </summary>
	public sealed class LinkedTileProperty
	{
		private readonly string myPropertyName;
		private readonly AbstractTileItem parentElement;
		private readonly Action<string> nameSetter;
		private readonly CycleDetector cycleDetector;

		private bool wasInitialized = false;

		public delegate bool CycleDetector(AbstractTileItem item, AbstractTileItem newItem);

		/// <summary>
		/// Creates a new <see cref="LinkedTileProperty"/>.
		/// </summary>
		/// <param name="parent">The <see cref="AbstractTileItem"/> that will contain this <see cref="LinkedTileProperty"/>.</param>
		/// <param name="propertyName">The name of the property to be wrapped, will be used in error messages.</param>
		/// <param name="nameSetterOption">Chooses which method to use as the NameSetter, see the documentation of <see cref="NameSetterOption"/>.
		/// Defaults to <see cref="DefaultNameSetterOption"/>.
		/// </param>
		/// <exception cref="ArgumentNullException"></exception>
		internal LinkedTileProperty(AbstractTileItem parent, string propertyName, NameSetterOption nameSetterOption = DefaultNameSetterOption, CycleDetector cycleDetector = null)
		{
			parentElement = parent        ?? throw new ArgumentNullException(nameof(parent));
			myPropertyName = propertyName ?? throw new ArgumentNullException(nameof(propertyName));
			nameSetter = GetKnownNameSetterOption(nameSetterOption);
			this.cycleDetector = cycleDetector;
		}

		/// <summary>
		/// Creates a new <see cref="LinkedTileProperty"/> using the provided <paramref name="nameSetter"/>.
		/// </summary>
		/// <param name="parent">The <see cref="AbstractTileItem"/> that will contain this <see cref="LinkedTileProperty"/>.</param>
		/// <param name="propertyName">The name of the property to be wrapped, will be used in error messages.</param>
		/// <param name="nameSetter"></param>
		/// <exception cref="ArgumentNullException"></exception>
		internal LinkedTileProperty(AbstractTileItem parent, string propertyName, Action<string> nameSetter, CycleDetector cycleDetector = null)
		{
			parentElement   = parent       ?? throw new ArgumentNullException(nameof(parent));
			myPropertyName  = propertyName ?? throw new ArgumentNullException(nameof(propertyName));
			this.nameSetter = nameSetter   ?? throw new ArgumentNullException(nameof(nameSetter));
			this.cycleDetector = cycleDetector;
		}

		private string _name = null;

		/// <summary>
		/// The name/id of the linked <see cref="AbstractTileItem"/>.
		/// <br/>
		/// Writing to this property will invoke <see cref="nameSetter"/>, which could ignore the provided value, or throw,
		/// if the matching <see cref="AbstractTileItem"/> is not found.
		/// </summary>
		public string Name
		{
			get => _value != null ? _value.id : _name;
			set => nameSetter(value);
		}


		#region SetName

		internal enum NameSetterOption : byte
		{
			/// <summary>
			/// <see cref="LinkedTileProperty.SetNameOrThrow"/>
			/// </summary>
			SetOrThrow,
			/// <summary>
			/// <see cref="LinkedTileProperty.SetNameOrUnchanged"/>
			/// </summary>
			SetOrUnchanged,
			/// <summary>
			/// <see cref="LinkedTileProperty.SetNameRegardless"/>
			/// </summary>
			SetRegardless,
		}

		internal const NameSetterOption DefaultNameSetterOption = NameSetterOption.SetOrUnchanged;

		private Action<string> GetKnownNameSetterOption(NameSetterOption option)
		{
			switch (option)
			{
				case NameSetterOption.SetOrThrow: return SetNameOrThrow;
				case NameSetterOption.SetOrUnchanged: return SetNameOrUnchanged;
				case NameSetterOption.SetRegardless: return SetNameRegardless;
				default: throw new ArgumentOutOfRangeException(nameof(option), option, "Unknown option.");
			}
		}

		/// <summary>
		/// Set <see cref="Name"/> to <paramref name="newValue"/> if it is valid
		/// or leave the value unchanged and throw an <see cref="ElementNotFoundException"/>.
		/// </summary>
		/// <exception cref="ElementNotFoundException"></exception>
		/// <param name="newValue"></param>
		public void SetNameOrThrow(string newValue)
		{
			if (! TrySetName(newValue))
			{
				throw new ElementNotFoundException(parentElement, myPropertyName, newValue);
			}
		}

		/// <summary>
		/// Set <see cref="Name"/> to <paramref name="newValue"/> if it is valid,
		/// or leave it unchanged.
		/// </summary>
		/// <param name="newValue"></param>
		public void SetNameOrUnchanged(string newValue)
		{
			TrySetName(newValue);
		}

		/// <summary>
		/// Set <see cref="Name"/> to <paramref name="newValue"/> regardless of it's validity.
		/// </summary>
		/// <param name="newValue"></param>
		public void SetNameRegardless(string newValue)
		{
			if (! TrySetName(newValue))
			{
				_name = newValue;
			}
		}

		/// <summary>
		/// Try to set <see cref="Name"/> to <paramref name="newValue"/>, returning true if successful,
		/// false otherwise.
		/// </summary>
		/// <param name="newValue"></param>
		/// <returns></returns>
		public bool TrySetName(string newValue)
		{
			if (string.IsNullOrWhiteSpace(newValue))
			{
				_name = null;
				_value = null;
				return true;
			}

			if (parentElement.tile == null)
			{
				_name = newValue;

				// No parent tile assigned yet, can't fetch the object with that id from it.
				_value = null;
				return true;
			}

			if (parentElement.tile.Elements.TryGetValue(newValue, out var result))
			{
				_name = newValue;
				_value = result;
				return true;
			}
			else
			{
				return false;
			}
		}

		#endregion SetName

		public bool CycleDetected { get; private set; }

		private AbstractTileItem __value;

		public AbstractTileItem _value
		{
			get => __value;
			private set
			{
				if (null != cycleDetector)
				{
					CycleDetected = cycleDetector(parentElement, value);
				}

				__value = value;
			}
		}

		/// <summary>
		/// The linked <see cref="AbstractTileItem"/> which could be null, even if <see cref="Name"/> has a value.
		/// Setting this property will change <see cref="Name"/> to match the assigned value.
		/// Setting <see langword="null"/> is allowed.
		/// </summary>
		public AbstractTileItem Value
		{
			get
			{
				if (_name == null)
				{
					return null;
				}

				if (_value == null && ! wasInitialized)
				{
					// Only throw if nt yet initialized.
					// It is possible we have a _name assigned, but _value wasn't found.
					throw new NotYetLinkedException(myPropertyName);
				}

				return _value;
			}
			set
			{
				_name = value?.id;
				_value = value;
			}
		}

		/// <summary>
		/// During deserialization of a <see cref="Tile"/>s elements the element linked to could not yet be deserialized.
		/// Therefore this method is called afterwards to fetch the linked element.
		/// </summary>
		internal void Fetch()
		{
			// When parentElement.tile == null we cannot yet check if that _name will be set to a valid id
			// So we just have to accept it.

			wasInitialized = true;

			// Now we are commanded to actually fetch it, so try to set it now.
			nameSetter(_name);
		}

		/// <summary>
		/// During deserialization of a <see cref="Tile"/>s elements the element linked to could not yet be deserialized.
		/// Therefore this method is called afterwards to fetch the linked element.
		/// </summary>
		internal void FetchOrThrow()
		{
			// When parentElement.tile == null we cannot yet check if that _name will be set to a valid id
			// So we just have to accept it.

			wasInitialized = true;

			// Now we are commanded to actually fetch it, so try to set it now.
			SetNameOrThrow(_name);
		}
	}
}
