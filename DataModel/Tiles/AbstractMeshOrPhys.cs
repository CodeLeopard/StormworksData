// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Text;
using System.Xml.Linq;

using Shared;
using Shared.Exceptions;
using Shared.Serialization;

namespace DataModel.Tiles
{
	/// <summary>
	/// Shared parts of <see cref="MeshInfo"/> and <see cref="PhysInfo"/>.
	/// </summary>
	public abstract class AbstractMeshOrPhys : AbstractTileItem
	{
		private string _file_name = "";

		/// <summary>
		/// The file name of the .mesh or .phys file of this object.
		/// </summary>
		[VisibleProperty]
		public string file_name
		{
			get => _file_name;
			set
			{
				if(value == _file_name) return;

				if (value == null)
				{
					_file_name = value;

					return;
				}

				_file_name = PathHelper.NormalizeDirectorySeparator(value);
			}
		}

		protected AbstractMeshOrPhys() { }

		protected AbstractMeshOrPhys(AbstractMeshOrPhys mesh)
		{
			this.id = mesh.id;
			this.parent_id = mesh.parent_id;
			this.purchase_interactable_id = mesh.purchase_interactable_id;
			this.seasonal_flags = mesh.seasonal_flags;
			this._file_name = mesh.file_name;
			this.LTransform = mesh.LTransform;
		}

		protected AbstractMeshOrPhys(XElement meshElement) : base(meshElement)
		{
			try
			{
				_file_name = meta_constructor_attributes.GetString("file_name") ?? throw new Exception("Missing required attribute 'file_name'.");
			}
			catch (Exception e)
			{
				throw new XmlDataException(meshElement, e);
			}
		}

		/// <inheritdoc />
		protected override void DerivedToXElement(XElement baseElement)
		{
			baseElement.SetAttributeValue("file_name", file_name);
		}

		/// <inheritdoc />
		public override string ToString()
		{
			var sb = new StringBuilder();
			ToStringCommonStart(sb);
			sb.Append($" file_name:'{file_name}'");
			ToStringCommonEnd(sb);
			return sb.ToString();
		}
	}
}
