﻿// Copyright 2022-2024 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.IO;
using System.Linq;
using System.Text;

using BinaryDataModel.Converters;

using Shared;
using Shared.Exceptions;

namespace DataModel.Tiles
{
	public static class Validator
	{
		[Flags]
		public enum ValidationResult : byte
		{
			Pass = 0,
			Warn = 1 << 0,
			Fail = 1 << 1,
		}

		public static uint meshFileBytes = 0;


		public static ValidationResult Validate(StringBuilder sb, Tile t)
		{
			ValidationResult validationResult = ValidationResult.Pass;

			foreach (var kvp in t.Elements)
			{
				string key = kvp.Key;
				var item = kvp.Value;

				if (Tile.IdConflictRegex.IsMatch(key))
				{
					validationResult |= ValidationResult.Warn;

					// Already logged by loader.
					//sb.AppendLine($"Encountered {item.meta_elementName} with duplicated id '{item.id}'");
				}

				validationResult |= CheckParentWasFound(sb, item);
				validationResult |= CheckParentCycle(sb, item);

				validationResult |= CheckPurchase_InteractibleWasFound(sb, item);
			}

			foreach (var item in t.meshes)
			{
				validationResult |= CheckFilePathSlash(sb, item, nameof(item.file_name), item.file_name);
				validationResult |= CheckFileExists(sb, item, nameof(item.file_name), item.file_name);
				validationResult |= CheckMesh(sb, item, nameof(item.file_name), item.file_name);
				validationResult |= CheckSameObjectLink(sb, item);
			}

			foreach (var item in t.physicsMeshes)
			{
				validationResult |= CheckFilePathSlash(sb, item, nameof(item.file_name), item.file_name);
				validationResult |= CheckFileExists(sb, item, nameof(item.file_name), item.file_name);
				validationResult |= CheckPhys(sb, item, nameof(item.file_name), item.file_name);
				validationResult |= CheckSameObjectLink(sb, item);
			}

			foreach (var item in t.edit_areas)
			{
				if (! string.IsNullOrEmpty(item.default_vehicle))
				{
					validationResult |= CheckFilePathSlash(sb, item, nameof(item.default_vehicle), item.default_vehicle);
					validationResult |= CheckDefaultVehicle(sb, item, nameof(item.default_vehicle), item.default_vehicle);
				}

				validationResult |= CheckSize(sb, item);
			}

			foreach (var item in t.interactables)
			{
				validationResult |= CheckSize(sb, item);
			}

			foreach(var item in t.trackNodes)
			{
				validationResult |= CheckTrackItem(sb, t, item);
			}

			return validationResult;
		}

		private static ValidationResult CheckFilePathSlash
			(StringBuilder sb, AbstractTileItem i, string attributeName, string romPath)
		{
			if (romPath.Contains('\\'))
			{
				sb.AppendLine($"{i.meta_elementName} '{i.id}' attribute '{attributeName}' ('{romPath}') contains a '\\' which will not work on Linux based operating systems. Paths should be separated using forward slash '/' to be compatible.");
				return ValidationResult.Warn;
			}

			return ValidationResult.Pass;
		}

		private static ValidationResult CheckFileExists(
			StringBuilder sb,
			AbstractTileItem i,
			string attributeName,
			string romPath,
			string searchRoot = null,
			ValidationResult valueInvalidValidationResilt = ValidationResult.Fail,
			ValidationResult notFoundValidationResult = ValidationResult.Fail)
		{
			if (string.IsNullOrWhiteSpace(romPath))
			{
				sb.AppendLine($"{i.meta_elementName} '{i.id}' attribute '{attributeName}' was null, empty, or otherwise invalid but was expected to point to an existing file.");
				return valueInvalidValidationResilt;
			}

			string searchpath;
			if (null != searchRoot)
				searchpath = Path.Combine(searchRoot, romPath);
			else
				searchpath = romPath;

			string fullPath = Path.Combine(StormworksPaths.rom, searchpath);
			if (! File.Exists(fullPath))
			{
				sb.AppendLine($"{i.meta_elementName} '{i.id}' attribute '{attributeName}' ('{searchpath}') could not be found.\n\t{fullPath}");
				return notFoundValidationResult;
			}

			return ValidationResult.Pass;
		}

		private static ValidationResult CheckDefaultVehicle(StringBuilder sb, EditArea i, string attributeName, string romPath)
		{
			if (romPath == null || romPath == "")
				return ValidationResult.Pass;

			return CheckFileExists(sb, i, attributeName, Path.Combine("data\\debris", romPath + ".xml"), notFoundValidationResult: ValidationResult.Warn);
		}

		private static ValidationResult CheckMesh(StringBuilder sb, AbstractTileItem i, string attributeName, string romPath)
		{
			try
			{
				string fullPath = Path.Combine(StormworksPaths.rom, romPath);
				var m = Binary.LoadMesh(fullPath);
				meshFileBytes += m.SizeEstimate;
			}
			catch (Exception e)
			{
				e = StripFileInteractionException(e);
				sb.AppendLine($"{i.meta_elementName} '{i.id}' file '{romPath}' failed to load: {e.Message}");
				return ValidationResult.Fail;
			}

			return ValidationResult.Pass;
		}

		private static ValidationResult CheckPhys(StringBuilder sb, AbstractTileItem i, string attributeName, string romPath)
		{
			try
			{
				string fullPath = Path.Combine(StormworksPaths.rom, romPath);
				var m = Binary.LoadPhys(fullPath);
			}
			catch (Exception e)
			{
				e = StripFileInteractionException(e);
				sb.AppendLine($"{i.meta_elementName} '{i.id}' file '{romPath}' failed to load: {e.Message}");
				return ValidationResult.Fail;
			}

			return ValidationResult.Pass;
		}

		private static ValidationResult CheckSameObjectLink(StringBuilder sb, AbstractTileItem i)
		{
			if (i.SameObjectLinks.Any()) return ValidationResult.Pass;

			foreach (AbstractTileItem linkedObject in i.SameObjectLinks)
			{
				var pm = i.LTransform.ExtractTranslation();
				var pp = linkedObject.LTransform.ExtractTranslation();

				float distance = (pm - pp).Length;
				if (distance > 1)
				{
					sb.AppendLine
						(
						 $"Mesh '{i.id}' with link to same object '{linkedObject.id}' are {distance} away from each other."
						);
					return ValidationResult.Warn;
				}
			}

			return ValidationResult.Pass;
		}

		private static ValidationResult CheckSize(StringBuilder sb, EditArea i)
		{
			int zeroCount = 0;
			if (i.size.X == 0) zeroCount++;
			if (i.size.Y == 0) zeroCount++;
			if (i.size.Z == 0) zeroCount++;

			if (zeroCount == 1)
			{
				if (string.IsNullOrWhiteSpace(i.default_vehicle))
				{
					sb.AppendLine($"EditArea '{i.id}' has size with a single 0 component: {i.size}. It cannot be used as an EditArea when it has no volume.");
					return ValidationResult.Fail;
				}
				// Only the position really matters when spawning a default_vehicle
				// In the Editor it will still be visible as a 2D shape.
				return ValidationResult.Pass;
			}
			else if (zeroCount > 1)
			{
				sb.AppendLine($"EditArea '{i.id}' has size with a multiple 0 components: {i.size}. The base game does not do this and in the Editor it will not be visible.");
				return ValidationResult.Fail;
			}

			return ValidationResult.Pass;
		}

		private static ValidationResult CheckSize(StringBuilder sb, Interactable i)
		{
			int zeroCount = 0;
			if (i.size.X == 0) zeroCount++;
			if (i.size.Y == 0) zeroCount++;
			if (i.size.Z == 0) zeroCount++;

			if (zeroCount == 1)
			{
				// Flat / 2D. Probably fine
				sb.AppendLine($"Interactable '{i.id}' has size with a single 0 component: {i.size}. The area will be 2D and may not be usable.");
				return ValidationResult.Fail;
			}
			else if (zeroCount > 1)
			{
				sb.AppendLine($"Interactable '{i.id}' has size with a multiple 0 components: {i.size}. The area will not be usable.");
				return ValidationResult.Fail;
			}

			return ValidationResult.Pass;
		}

		private static ValidationResult CheckParentWasFound(StringBuilder sb, AbstractTileItem item)
		{
			if (item.parent_id != null && item._parent == null)
			{
				sb.AppendLine($"{item.meta_elementName} '{item.id}' did not find its parent '{item.parent_id}'.");
				return ValidationResult.Warn;
			}

			return ValidationResult.Pass;
		}

		private static ValidationResult CheckPurchase_InteractibleWasFound(StringBuilder sb, AbstractTileItem item)
		{
			if (item.purchase_interactable_id != null && item._purchase_interactable == null)
			{
				sb.AppendLine($"{item.meta_elementName} '{item.id}' did not find its purchase_interactable '{item.purchase_interactable_id}'.");
				return ValidationResult.Warn;
			}

			return ValidationResult.Pass;
		}

		private static ValidationResult CheckParentCycle(StringBuilder sb, AbstractTileItem item)
		{
			if (item.ParentHierarchyCycle)
			{
				sb.AppendLine($"{item.meta_elementName} '{item.id}' has a cycle in its parent hierarchy.");
				return ValidationResult.Warn; // The game does not crash so its not a fail.
			}

			return ValidationResult.Pass;
		}
		private static ValidationResult CheckTrackItem(StringBuilder sb, Tile t, TrainTrackNode item)
		{
			if (item.LinkIds.Count() < 1)
			{
				sb.AppendLine($"{item.meta_elementName} '{item.id}' has 0 links.");
				return ValidationResult.Warn;
			}

			var result = ValidationResult.Pass;

			foreach (var link in item.TrackLinks.Values)
			{
				var linkId = link.toNodeId;
				if (linkId == item.id)
				{
					sb.AppendLine($"{item.meta_elementName} '{item.id}' links to itself.");
					result |= ValidationResult.Warn;
				}

				bool missing = true;
				foreach (var other in t.trackNodes)
				{
					if(other.id == linkId)
					{
						missing = false;
						break;
					}
				}
				if (missing)
				{
					sb.AppendLine($"{item.meta_elementName} '{item.id}' links to node: '{linkId}' but that node is not found.");
					result |= ValidationResult.Warn;
				}
			}

			return result;
		}

		private static Exception StripFileInteractionException(Exception e)
		{
			if (e is FileInteractionException f) return f.NonFileException;
			return e;
		}
	}
}
