// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Text;
using System.Xml.Linq;

using OpenToolkit.Mathematics;

using Shared;
using Shared.Exceptions;
using Shared.Serialization;
using Shared.Serialization.DataTypes;

namespace DataModel.Tiles
{
	public class Interactable : AbstractTileItem
	{
		#region DataTypes
		/// <summary>
		/// The 'use_case' of an <see cref="Interactable"/>.
		/// Does not appear to be actually used for anything.
		/// </summary>
		public enum UseCase : byte
		{
			/// <summary>
			/// Use this unless you know otherwise.
			/// </summary>
			Default = 0,
			/// <summary>
			/// Used in some places, but no pattern could be found.
			/// </summary>
			Unknown1 = 1,
			/// <summary>
			/// Used only on tile_test.xml
			/// </summary>
			Testing = 2,
		}

		/// <summary>
		/// The type of interaction to be performed.
		/// </summary>
		public enum InteractType : byte
		{
			/// <summary>
			/// Generic interaction to trigger logic for lights, doors, etc.
			/// </summary>
			Logic = 0,

			/// <summary>
			/// Not used anywhere.
			/// </summary>
			NeverUsed1 = 1,

			/// <summary>
			/// For opening the vehicle editor.
			/// </summary>
			Workbench = 2,

			/// <summary>
			/// For purchasing an island.
			/// </summary>
			Purchase = 3,

			/// <summary>
			/// For opening the character customization (wardrobe).
			/// </summary>
			Wardrobe = 4
		}
		#endregion DataTypes


		private LinkedTileProperty _target_object;
		/// <summary>
		/// The target for the logic interaction, for example an <see cref="EditArea"/>.
		/// </summary>
		[VisibleProperty]
		public string target_object_id
		{
			get => _target_object.Name;
			set => _target_object.Name = value;
		}

		public AbstractTileItem target_object
		{
			get => _target_object.Value;
			set => _target_object.Value = value;
		}

		/// <summary>
		/// The label shown to the user.
		/// </summary>
		[VisibleProperty]
		public string label;

		/// <summary>
		/// Does not appear to be used for anything.
		/// </summary>
		[VisibleProperty]
		public UseCase use_case;

		/// <summary>
		/// The type of interaction to be performed.
		/// </summary>
		[VisibleProperty]
		public InteractType interact_type;

		/// <summary>
		/// Presumably only used in save files.
		/// </summary>
		public bool is_purchased = false;


		[VisibleProperty]
		public bool is_ignore_purchased = false;

		// already have Transform from Base

		/// <summary>
		/// Size of the Interactable area.
		/// </summary>
		public Vector3 size;


		/// <inheritdoc />
		public override string meta_elementName => "interactable";
		/// <inheritdoc />
		public override string meta_containerName => "interactables";


		private void ConstructorCommon()
		{
			_target_object = new LinkedTileProperty(this, nameof(_target_object));
		}

		public Interactable()
		{
			ConstructorCommon();
		}

		public Interactable(XElement element) : base(element)
		{
			ConstructorCommon();
			try
			{
				target_object_id    = meta_constructor_attributes.GetString("target_object_id", "");
				label               = meta_constructor_attributes.GetString("label", "");
				use_case            = (UseCase) meta_constructor_attributes.GetByte("use_case");
				interact_type       = (InteractType)meta_constructor_attributes.GetByte("interact_type");
				is_purchased        = meta_constructor_attributes.GetBool("is_purchased");
				is_ignore_purchased = meta_constructor_attributes.GetBool("is_ignore_purchased");

				size = VectorSer.Parse3f(element.Element("size"), allowNull: true, nullValue: Vector3.One);
			}
			catch (Exception e)
			{
				throw new XmlDataException(element, e);
			}
		}

		/// <inheritdoc />
		protected override void DerivedLinkStringIdPropertiesToInstances()
		{
			_target_object.Fetch();
		}

		/// <inheritdoc />
		protected override void DerivedToXElement(XElement baseElement)
		{
			baseElement.SetAttributeValue("target_object_id",    target_object_id);
			baseElement.SetAttributeValue("label",               label);
			baseElement.SetAttributeValueIND("use_case",            (byte)use_case);
			baseElement.SetAttributeValueIND("interact_type",       (byte)interact_type);
			baseElement.SetAttributeValueIND("is_purchased",        is_purchased);
			baseElement.SetAttributeValueIND("is_ignore_purchased", is_ignore_purchased);
			baseElement.Add(size.ToXElement("size"));
		}

		/// <inheritdoc />
		public override string ToString()
		{
			var sb = new StringBuilder();
			ToStringCommonStart(sb);
			sb.Append($" target_object_id:'{target_object_id}'");
			sb.Append($" label:{label}");
			sb.Append($" use_case:{use_case}");
			sb.Append($" interact_type:{interact_type}");
			sb.Append($" is_purchased:{is_purchased}");
			sb.Append($" is_ignore_purchased:{is_ignore_purchased}");
			ToStringCommonEnd(sb);
			return sb.ToString();
		}
	}
}