// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.ObjectModel;
using System.Text;
using System.Xml.Linq;

using OpenToolkit.Mathematics;

using Shared;
using Shared.Exceptions;
using Shared.Serialization;
using Shared.Serialization.DataTypes;

namespace DataModel.Tiles
{
	[Serializable]
	public class EditArea : AbstractTileItem
	{
		#region Enums

		public enum GridSize : byte
		{
			/// <summary>
			/// See <see cref="EditArea.size"/> for the size of the <see cref="EditArea"/>.
			/// </summary>
			Custom      = 0,
			SmallDock   = 1,
			Dock        = 2,
			SmallHangar = 3,
			Hangar      = 4,
			Bed         = 5,
			Static      = 6,
			Train       = 7,
		}

		private static readonly Vector3[] gridSizes = new Vector3[]
		{
			new Vector3( 1.00f,  1.00f,  1.00f), // 0 Custom, see size property. Must have a sensible value to prevent NaN etc.
			new Vector3( 7.50f,  7.50f,  7.50f), // 1 Small Dock
			new Vector3(22.50f, 20.50f, 57.25f), // 2 Dock
			new Vector3(16.50f,  6.00f, 18.50f), // 3 Small Hangar
			new Vector3(36.50f, 10.00f, 35.00f), // 4 Standard Hangar
			new Vector3( 0.75f,  0.25f,  1.75f), // 5 Bed
			new Vector3(36.50f, 20.50f, 36.50f), // 6 Standard Static
			new Vector3( 5.00f,  6.00f, 25.00f), // 7 Train
		};

		public static ReadOnlyCollection<Vector3> GridSizes
			// Clone because Vector3 is a mutable struct.
			// ReadOnlyCollection will only prevent replacing elements with new ones
			// but not prevent changing the value of a field in the struct.
			=> Array.AsReadOnly((Vector3[])gridSizes.Clone());

		#endregion Enums


		private GridSize _grid_size;

		/// <summary>
		/// The pre-Defined <see cref="GridSize"/> to use for this area.
		/// If <see cref="GridSize.Custom"/> is specified read the size from <see cref="size"/> instead.
		/// Note: <see cref="size"/> and <see cref="grid_size"/> keep each other in sync.
		/// </summary>
		[VisibleProperty]
		public GridSize grid_size
		{
			get => _grid_size;
			set
			{
				_grid_size = value;

				int index = (int)value;

				if (index > 0 && index < gridSizes.Length)
				{
					_size = gridSizes[index];
				}
			}
		}

		[VisibleProperty]
		public bool is_static = false;

		[VisibleProperty]
		private string _default_vehicle = "";

		[VisibleProperty]
		public string default_vehicle
		{
			get => _default_vehicle;
			set
			{
				if(value == _default_vehicle) return;
				if (value == null)
				{
					_default_vehicle = "";
					return;
				}

				_default_vehicle = PathHelper.NormalizeDirectorySeparator(value);
			}
		}

		[VisibleProperty]
		public int spawned_static_vehicle_id = 0;
		[VisibleProperty]
		public bool is_invisible_vehicle = false;

		/// <inheritdoc />
		public override string meta_elementName => "edit_area";

		/// <inheritdoc />
		public override string meta_containerName => "edit_areas";


		// already have Transform from Base


		private Vector3 _size;

		/// <summary>
		/// Size of the EditArea.
		/// Note: Setting <see cref="size"/> can cause <see cref="grid_size"/> to change to maintain consistency.
		/// </summary>
		public Vector3 size
		{
			get => _size;
			set
			{
				_size = value;

				int index = Array.IndexOf(gridSizes, value);
				if (! (index < 0))
				{
					_grid_size = (GridSize)index;
				}
				else
				{
					_grid_size = 0;
				}
			}
		}


		public EditArea() { }

		public EditArea(XElement element) : base(element)
		{
			try
			{
				_size = VectorSer.Parse3f(element.Element("size"), allowNull: true, nullValue: Vector3.One);

				// Parse size first, so that parsing grid_size will override it just like the game does.
				grid_size                 = (GridSize)meta_constructor_attributes.GetByte("grid_size");
				is_static                 = meta_constructor_attributes.GetBool("is_static");
				_default_vehicle           = meta_constructor_attributes.GetString("default_vehicle", "");
				spawned_static_vehicle_id = meta_constructor_attributes.GetInt32("spawned_static_vehicle_id");
				is_invisible_vehicle      = meta_constructor_attributes.GetBool("is_invisible_vehicle");
			}
			catch (Exception e)
			{
				throw new XmlDataException(element, e);
			}
		}

		protected override void DerivedToXElement(XElement baseElement)
		{
			baseElement.SetAttributeValueIND("grid_size",          (byte) grid_size);
			baseElement.SetAttributeValueIND("is_static",                 is_static);
			baseElement.SetAttributeValueIND("default_vehicle",           default_vehicle, "");
			baseElement.SetAttributeValueIND("spawned_static_vehicle_id", spawned_static_vehicle_id);
			baseElement.SetAttributeValueIND("is_invisible_vehicle",      is_invisible_vehicle);
			baseElement.Add(size.ToXElement("size"));
		}


		/// <inheritdoc />
		public override string ToString()
		{
			var sb = new StringBuilder();
			ToStringCommonStart(sb);
			sb.Append($" grid_size:'{grid_size}'");
			sb.Append($" is_static:{is_static}");
			sb.Append($" default_vehicle:'{default_vehicle}'");
			sb.Append($" spawned_static_vehicle_id:'{spawned_static_vehicle_id}'");
			sb.Append($" is_invisible_vehicle:'{is_invisible_vehicle}'");
			ToStringCommonEnd(sb);
			return sb.ToString();
		}
	}
}