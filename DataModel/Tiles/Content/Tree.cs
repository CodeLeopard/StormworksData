﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Xml.Linq;

using DataModel.DataTypes;

using OpenToolkit.Mathematics;

using Shared.Exceptions;
using Shared.Serialization.DataTypes;

namespace DataModel.Tiles
{
	public struct Tree
	{
		public Vector3 position;
		public float scale;

		public Tree(Vector3 position, float scale)
		{
			this.position = position;
			this.scale = scale;
		}

		public Tree(Vector4 vector)
		{
			position = new Vector3(vector.X, vector.Y, vector.Z);
			scale = vector.W;
		}

		#region Parsing
		/// <summary>
		/// Parse a list of <see cref="Tree"/>s from a <see cref="XDocument"/> from an instances.xml file.
		/// </summary>
		/// <param name="doc"></param>
		/// <returns></returns>
		public static List<Tree> ParseTrees(XDocument doc)
		{
			return ParseTrees(doc.Element("Trees"));
		}

		/// <summary>
		/// Parse a list of <see cref="Tree"/>s from a 'Trees' <see cref="XElement"/>.
		/// </summary>
		/// <param name="treesElement"></param>
		/// <returns></returns>
		public static List<Tree> ParseTrees(XElement treesElement)
		{
			try
			{
				var trees = new List<Tree>();
				foreach (var xElement in treesElement.Elements("T"))
				{
					try
					{
						var p = xElement.Element("p");

						if (null == p)
							throw new NullReferenceException("Missing required <p/> (position) element.");

						try
						{
							var vec = VectorSer.Parse4f(p);
							trees.Add(new Tree(vec));
						}
						catch (Exception e)
						{
							throw new XmlDataException(p, e);
						}
					}
					catch (Exception e)
					{
						throw new XmlDataException(xElement, e);
					}
				}

				return trees;
			}
			catch (Exception e)
			{
				throw XmlDataException.AutoFormatDataName(nameof(Tree), treesElement, e);
			}
		}

		/// <summary>
		/// Write a <see cref="XDocument"/> for a list of trees. The document can be written to an instances file.
		/// </summary>
		/// <param name="trees"></param>
		/// <returns></returns>
		public static XDocument WriteTreesDocument(ICollection<Tree> trees)
		{
			var d = new XDocument();
			d.Add(WriteTrees(trees, null));
			return d;
		}

		/// <summary>
		/// Write the list of trees to a Trees <see cref="XElement"/>.
		/// </summary>
		/// <param name="trees"></param>
		/// <param name="container">Optional container to place the trees in, if not provided will be created.</param>
		/// <returns>XElement containing a list of trees</returns>
		public static XElement WriteTrees(ICollection<Tree> trees, XElement container = null)
		{
			container = container ?? new XElement("Trees");
			foreach (Tree tree in trees)
			{
				container.Add(WriteTree(tree));
			}

			return container;
		}

		/// <summary>
		/// Write a single <see cref="Tree"/> to an <see cref="XElement"/>.
		/// </summary>
		/// <param name="tree"></param>
		/// <returns></returns>
		public static XElement WriteTree(Tree tree)
		{
			var p = new XElement("p");

			p.SetAttributeValue("x", tree.position.X);
			p.SetAttributeValue("y", tree.position.Y);
			p.SetAttributeValue("z", tree.position.Z);
			p.SetAttributeValue("w", tree.scale);

			var t = new XElement("T");
			t.Add(p);
			return t;
		}


		/// <inheritdoc />
		public override string ToString()
		{
			return $"{{{nameof(Tree)} {position} scale: {scale}}}";
		}

		#endregion Parsing
	}
}

