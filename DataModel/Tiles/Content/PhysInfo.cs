﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Xml.Linq;

namespace DataModel.Tiles
{
	public class PhysInfo : AbstractMeshOrPhys
	{
		/// <inheritdoc />
		public override string meta_elementName => "physics_mesh";
		/// <inheritdoc />
		public override string meta_containerName => "physics_meshes";

		public PhysInfo() : base() { }

		/// <summary>
		/// Copy a <see cref="AbstractMeshOrPhys"/> instance.
		/// </summary>
		/// <param name="mesh"></param>
		public PhysInfo(AbstractMeshOrPhys mesh) : base(mesh) { }

		/// <summary>
		/// Copy a <see cref="AbstractMeshOrPhys"/> instance and override the <paramref name="id"/> and <paramref name="file_name"/>.
		/// </summary>
		/// <param name="mesh"></param>
		/// <param name="id"></param>
		/// <param name="file_name"></param>
		public PhysInfo(AbstractMeshOrPhys mesh, string id, string file_name) : this(mesh)
		{
			this.id = id;
			this.file_name = file_name;
		}

		/// <inheritdoc />
		public PhysInfo(XElement meshElement) : base(meshElement) { }
	}
}
