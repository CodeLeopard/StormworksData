﻿// Copyright 2023 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Text;
using System.Xml.Linq;

using OpenToolkit.Mathematics;

using Shared;
using Shared.Exceptions;
using Shared.Serialization;
using Shared.Serialization.DataTypes;

namespace DataModel.Tiles
{
	public class OmniLight : BaseLight
	{
		[VisibleProperty]
		public float radius = 1f;

		[VisibleProperty]
		public bool is_render_halo = false;

		public Vector3 color = Vector3.One;

		// already have Transform from Base

		/// <inheritdoc />
		public override string meta_elementName => "light";
		/// <inheritdoc />
		public override string meta_containerName => "lights_omni";

		public OmniLight() { }

		public OmniLight(XElement element) : base(element)
		{
			try
			{
				radius = meta_constructor_attributes.GetFloat(nameof(radius), 1);
				is_render_halo = meta_constructor_attributes.GetBool(nameof(is_render_halo), false);

				color = VectorSer.Parse3f(element.Element("color"), allowNull: true, nullValue: Vector3.One);
			}
			catch (Exception e)
			{
				throw new XmlDataException(element, e);
			}
		}

		/// <inheritdoc />
		protected override void DerivedToXElement(XElement baseElement)
		{
			base.DerivedToXElement(baseElement);

			baseElement.SetAttributeValue(nameof(radius), radius);
			baseElement.SetAttributeValue(nameof(is_render_halo), is_render_halo);
		}

		/// <inheritdoc />
		public override string ToString()
		{
			var sb = new StringBuilder();
			ToStringCommonStart(sb);
			sb.Append($" radius:'{radius}'");
			sb.Append($" is_render_halo:{is_render_halo}");
			sb.Append($" color:{color}");
			ToStringCommonEnd(sb);
			return sb.ToString();
		}
	}
}