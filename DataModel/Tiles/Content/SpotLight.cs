﻿// Copyright 2023 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Text;
using System.Xml.Linq;

using Shared;
using Shared.Exceptions;
using Shared.Serialization;

namespace DataModel.Tiles
{
	public class SpotLight : BaseLight
	{
		[VisibleProperty]
		public float range = 1f;

		public float fov = 0.2f;

		[VisibleProperty(nameOverride: "fov (rad)")]
		public float fov_rad
		{
			get => fov * 2;
			set => fov = value / 2;
		}

		[VisibleProperty(nameOverride: "fov (deg)")]
		public float fov_deg
		{
			get => fov_rad * (180f / (float)Math.PI);
			set => fov_rad = value * ((float)Math.PI / 180f);
		}

		[VisibleProperty]
		public string texture = "graphics/ies/ies_default.txtr";

		[VisibleProperty]
		public bool is_render_halo = false;

		// already have Transform from Base

		/// <inheritdoc />
		public override string meta_elementName => "light";
		/// <inheritdoc />
		public override string meta_containerName => "lights_spot";

		public SpotLight() { }

		public SpotLight(XElement element) : base(element)
		{
			try
			{
				range          = meta_constructor_attributes.GetFloat(nameof(range), 1);
				fov_rad        = meta_constructor_attributes.GetFloat(nameof(fov_rad), 1);
				texture        = meta_constructor_attributes.GetString(nameof(texture), null);
				is_render_halo = meta_constructor_attributes.GetBool(nameof(is_render_halo), false);
			}
			catch (Exception e)
			{
				throw new XmlDataException(element, e);
			}
		}

		/// <inheritdoc />
		protected override void DerivedToXElement(XElement baseElement)
		{
			base.DerivedToXElement(baseElement);

			baseElement.SetAttributeValue(nameof(range), range);
			baseElement.SetAttributeValue(nameof(fov), fov);
			baseElement.SetAttributeValue(nameof(texture), texture);
			baseElement.SetAttributeValue(nameof(is_render_halo), is_render_halo);
		}

		/// <inheritdoc />
		public override string ToString()
		{
			var sb = new StringBuilder();
			ToStringCommonStart(sb);
			sb.Append($" range:'{range}'");
			sb.Append($" fov:'{fov_deg}' (deg)");
			sb.Append($" texture:'{texture}'");
			sb.Append($" is_render_halo:{is_render_halo}");
			sb.Append($" color:{color}");
			ToStringCommonEnd(sb);
			return sb.ToString();
		}
	}
}