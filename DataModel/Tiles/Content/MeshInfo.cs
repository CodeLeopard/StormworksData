﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Xml.Linq;

using Shared;

namespace DataModel.Tiles
{
	public class MeshInfo : AbstractMeshOrPhys
	{
		/// <inheritdoc />
		public override string meta_elementName => "mesh";
		/// <inheritdoc />
		public override string meta_containerName => "meshes";

		public MeshInfo() : base() { }

		public MeshInfo(AbstractMeshOrPhys mesh) : base(mesh) { }

		public MeshInfo(XElement meshElement) : base(meshElement) { }

		/// <inheritdoc />
		protected override void FindSameObjectLinks(string searchId)
		{
			// performs an additional search based on the file name.
			string a = file_name.Replace(".mesh", ".phys");
			string b = file_name.Replace(".mesh", "_phys.phys");
			foreach (PhysInfo item in tile.physicsMeshes)
			{
				if (item.file_name == a || item.file_name == b)
				{
					if (GTransform.ApproxEquals(item.GTransform, 0.1f))
					{
						AddSameObjectLink(item);
					}
				}
			}
		}
	}
}
