﻿// Copyright 2022-2025 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using DataModel.Tiles.Content;
using OpenToolkit.Mathematics;
using Shared;
using Shared.Exceptions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace DataModel.Tiles
{
	public class TrainTrackNode : AbstractTileItem
	{
		/// <summary>
		/// The <see cref="string"/> ids of <see cref="LinkedNodes"/> in sorted order.
		/// </summary>
		public IEnumerable<string> LinkIds => TrackLinks.Keys.OrderBy(s => s);

		/// <summary>
		/// The linked <see cref="TrainTrackNode"/>s. Use <see cref="AddLink"/>, and <see cref="RemoveLink"/> to edit.
		/// </summary>
		public IEnumerable<TrainTrackNode> LinkedNodes => TrackLinks.Values.OrderBy(l => l.toNodeId).Select(l => l.ToNode);

		/// <summary>
		/// The linked <see cref="TrainTrackNode"/>s. Use <see cref="AddLink"/>, and <see cref="RemoveLink"/> to edit.
		/// </summary>
		public IEnumerable<TrainTrackNode> LinkedNodesUnordered => TrackLinks.Values.Select(l => l.ToNode);

		private readonly Dictionary<string, TrainTrackLink> trackLinks;

		/// <summary>
		/// Mapping from <see cref="AbstractTileItem.id"/> of a <see cref="TrainTrackNode"/> to the <see cref="TrainTrackLink"/> that goes there.
		/// </summary>
		public readonly ReadOnlyDictionary<string, TrainTrackLink> TrackLinks;

		/// <summary>
		/// Get or Set the Position stored in <see cref="AbstractTileItem.LTransform"/>.
		/// </summary>
		public Vector3 LPosition
		{
			get => LTransform.Row3.Xyz;
			set
			{
				var t = LTransform;
				t.Row3 = value.WithW(1);
				LTransform = t;
			}
		}

		/// <summary>
		/// Get or Set the Position stored in <see cref="AbstractTileItem.GTransform"/>.
		/// </summary>
		public Vector3 GPosition
		{
			get => GTransform.Row3.Xyz;
		}


		/// <inheritdoc />
		public override string meta_elementName => "track";
		/// <inheritdoc />
		public override string meta_containerName => "train_tracks";


		#region Constion and Initialization

		/// <summary>
		/// Create a new <see cref="TrainTrackNode"/> ready to be modifed.
		/// </summary>
		public TrainTrackNode()
		{
			trackLinks = new Dictionary<string, TrainTrackLink>();
			TrackLinks = new ReadOnlyDictionary<string, TrainTrackLink>(trackLinks);
		}

		/// <summary>
		/// Create a new <see cref="TrainTrackNode"/> ready to be modifed.
		/// </summary>
		/// <param name="id">Unique id</param>
		/// <param name="position">Position</param>
		public TrainTrackNode(string id, Vector3 position)
		: this()
		{
			if (string.IsNullOrEmpty(id))
			{
				throw new ArgumentNullException(nameof(id), "must not be null or empty.");
			}
			this.id = id;

			LTransform = Matrix4.CreateTranslation(position);
		}

		/// <summary>
		/// Create a new Track element and automatically setup links to and from the provided <paramref name="previous"/>
		/// </summary>
		/// <param name="id">unique id</param>
		/// <param name="position"></param>
		/// <param name="previous">Track element to link</param>
		public TrainTrackNode(string id, Vector3 position, TrainTrackNode previous)
		: this(id, position)
		{
			AddLink(previous);
		}

		/// <summary>
		/// Read a <see cref="TrainTrackNode"/> from <see cref="XElement"/> <paramref name="element"/>.
		/// </summary>
		/// <param name="element"></param>
		/// <exception cref="XmlDataException"></exception>
		public TrainTrackNode(XElement element) : base(element)
		{
			trackLinks = new Dictionary<string, TrainTrackLink>();
			TrackLinks = new ReadOnlyDictionary<string, TrainTrackLink>(trackLinks);
			try
			{
				var linksElement = element.Element("links") ?? throw new Exception("Missing required element 'links'.");

				var links = linksElement.Elements("link");
				foreach (var linkElement in links)
				{
					try
					{
						var link = new TrainTrackLink(linkElement);
						link.FromNode = this;

						if (! trackLinks.TryAdd(link.toNodeId, link))
						{
							Console.WriteLine($"Ignoring invalid duplicate link on Node: {id} to: '{link.toNodeId}'");
						}
					}
					catch (Exception e)
					{
						throw new XmlDataException(linkElement, e);
					}
				}
			}
			catch (Exception e)
			{
				throw new XmlDataException(element, e);
			}
		}


		/// <inheritdoc />
		protected override void DerivedLinkStringIdPropertiesToInstances()
		{
			foreach (var link in TrackLinks.Values)
			{
				if (! tile.Elements.TryGetValue(link.toNodeId, out AbstractTileItem item))
				{
					continue;
				}

				if (! (item is TrainTrackNode otherNode))
				{
					continue;
				}

				if (item == this)
				{
					continue;
				}

				link.ToNode = otherNode;
			}
		}

		protected virtual void DerivedAfterInitialLinking()
		{
			foreach(var link in TrackLinks.Values)
			{
				if (!tile.Elements.TryGetValue(link.toNodeId, out AbstractTileItem item))
				{
					// todo: complain better with diagnostics or something
					Console.WriteLine($"TrainTrackNode '{id}' Could not find linked TrainTrackNode '{link.toNodeId}'.");
					continue;
				}

				if (!(item is TrainTrackNode otherNode))
				{
					// todo: complain better with diagnostics or something
					Console.WriteLine($"TrainTrackNode '{id}' Link '{link.toNodeId}' resolves to a {item.meta_elementName} instead of {this.meta_elementName}.");
					continue;
				}

				if (item == this)
				{
					// todo: complain better with diagnostics or something
					Console.WriteLine($"TrainTrackNode '{id}' has a Link that refers to self.");
					continue;
				}

				this.TrackLinks.TryGetValue(id, out var outGoingLink);
				otherNode.TrackLinks.TryGetValue(this.id, out var reverseLink);

				if (outGoingLink != null && reverseLink == null)
				{
					Console.WriteLine($"TrainTrackNode '{id}' Fixking one-directional link incoming from '{otherNode.id}'.");

					otherNode.AddLink(this);

					if(otherNode.TrackLinks.TryGetValue(this.id, out reverseLink))
					{
						reverseLink.CopyExtraProperties(outGoingLink);
					}
				}
			}
		}

		#endregion Constion and Initialization

		protected override void OverrideRemoveSelf()
		{
			ClearLinks();
		}

		/// <summary>
		/// Check that the data in this object makes (at least a little) sense.
		/// </summary>
		public bool Valid => TrackLinks.Values.All(x => x.Valid);

		#region Link

		/// <summary>
		/// The result of adding a <see cref="TrainTrackLink"/> to another <see cref="TrainTrackNode"/>.
		/// </summary>
		public readonly struct AddLinkResult
		{
			/// <summary>
			/// Was a <see cref="TrainTrackLink"/> acutally added.
			/// </summary>
			public readonly bool WasAdded;

			/// <summary>
			/// The new or existing <see cref="TrainTrackLink"/>.
			/// </summary>
			public readonly TrainTrackLink ForwardLink;

			/// <summary>
			/// The new or existing <see cref="TrainTrackLink"/> in the opposite direction.
			/// </summary>
			public TrainTrackLink ReverseLink => ForwardLink.ReverseLink;

			internal AddLinkResult(bool wasAdded)
			{
				WasAdded = wasAdded;
				ForwardLink = null;
			}

			internal AddLinkResult(bool wasAdded, TrainTrackLink forwardLink)
			{
				WasAdded = wasAdded;
				ForwardLink = forwardLink;
			}
		}

		/// <summary>
		/// Add a link to the given <see cref="TrainTrackNode"/>.
		/// </summary>
		/// <param name="other"></param>
		/// <returns></returns>
		/// <exception cref="ArgumentNullException"></exception>
		/// <exception cref="ArgumentException"></exception>
		public AddLinkResult AddLink(TrainTrackNode other)
		{
			if (other == null) throw new ArgumentNullException(nameof(other));
			if (other == this) throw new ArgumentException("Cannot link to self.", nameof(other));

			if (trackLinks.TryGetValue(other.id, out var existingLink))
			{
				return new AddLinkResult(false, existingLink);
			}

			var newLink = new TrainTrackLink(this, other);
			trackLinks.Add(other.id, newLink);

			other.AddLink(this);

			return new AddLinkResult(true, newLink);
		}

		/// <summary>
		/// Remove the <see cref="TrainTrackLink"/> to <paramref name="other"/> if it exists.
		/// </summary>
		/// <param name="other">Node to remove</param>
		/// <returns></returns>
		/// <exception cref="ArgumentNullException"></exception>
		public bool RemoveLink(TrainTrackNode other)
		{
			if (other == null) throw new ArgumentNullException(nameof(other));
			if (other == this) return false;

			bool removed = trackLinks.Remove(other.id);

			if (removed)
			{
				other.RemoveLink(this);
			}

			return removed;
		}

		/// <summary>
		/// Replace <paramref name="toRemove"/> with <paramref name="replacement"/> in <see cref="LinkedNodes"/>.
		/// </summary>
		/// <param name="toRemove"></param>
		/// <param name="replacement"></param>
		/// <returns></returns>
		/// <exception cref="ArgumentNullException"></exception>
		public bool ReplaceLink(TrainTrackNode toRemove, TrainTrackNode replacement)
		{
			if (toRemove    == null) throw new ArgumentNullException(nameof(toRemove));
			if (replacement == null) throw new ArgumentNullException(nameof(replacement));

			if (toRemove   == this) return false;
			if (replacement == this) return false;

			trackLinks.TryGetValue(toRemove.id, out var removedLink);
			bool removed = RemoveLink(toRemove);

			if (removed)
			{
				var result = AddLink(replacement);
				if (removedLink != null)
				{
					var forwardLink = result.ForwardLink;
					if (forwardLink != null)
					{
						forwardLink.CopyExtraProperties(removedLink);

						var reverseLink = forwardLink.ReverseLink;
						if (reverseLink != null)
						{
							reverseLink.CopyExtraProperties(removedLink);
						}
					}
				}
			}

			return removed;
		}

		/// <summary>
		/// Absorb the given Node, that means to move all links of <paramref name="toAbsorb"/> to this node.
		/// </summary>
		/// <param name="toAbsorb"></param>
		/// <exception cref="ArgumentNullException"></exception>
		public bool Absorb(TrainTrackNode toAbsorb)
		{
			if (toAbsorb == null) throw new ArgumentNullException(nameof(toAbsorb));
			if (toAbsorb == this) return false;

			foreach (var toRemoveLink in toAbsorb.TrackLinks.Values)
			{
				var toRelinkNode = toRemoveLink.ToNode;
				if (toRelinkNode == this) continue;

				var result = AddLink(toRelinkNode);
				if (result.ForwardLink != null)
				{
					result.ForwardLink.CopyExtraProperties(toRemoveLink);

					if (result.ForwardLink.ReverseLink != null)
					{
						result.ForwardLink.ReverseLink.CopyExtraProperties(toRemoveLink);
					}
				}
			}
			toAbsorb.ClearLinks();
			return true;
		}

		/// <summary>
		/// Prepare to remove <paramref name="other"/> while preserving the links
		/// by replacing <paramref name="other"/> with <see langword="this"/> in all <paramref name="other"/>'s links.
		/// </summary>
		/// <param name="other"></param>
		/// <returns></returns>
		public bool RemoveButPreserveLinkPast(TrainTrackNode other)
		{
			if(other == this) return false;
			if (! HasLink(other)) return false;

			foreach (TrainTrackNode link in other.LinkedNodes.ToArray())
			{
				link.ReplaceLink(other, this);
			}

			RemoveLink(other);
			return true;
		}

		/// <summary>
		/// Remove all <see cref="LinkedNodes"/>.
		/// </summary>
		public void ClearLinks()
		{
			foreach (TrainTrackNode other in LinkedNodesUnordered.ToArray())
			{
				RemoveLink(other);
			}
		}

		/// <summary>
		/// Returns true if there is a link to <paramref name="other"/>.
		/// </summary>
		/// <param name="other"></param>
		/// <returns></returns>
		public bool HasLink(TrainTrackNode other)
		{
			return LinkedNodesUnordered.Contains(other);
		}

		/// <summary>
		/// Enumerate all <see cref="TrainTrackNode"/>s reachable from this one.
		/// </summary>
		/// <param name="seen">Optional container of nodes that will not be visited, if provided it will be modified.</param>
		/// <returns></returns>
		public IEnumerable<TrainTrackNode> AllReachableTrackNodes(HashSet<TrainTrackNode> seen = null)
		{
			seen = seen ?? new HashSet<TrainTrackNode>();
			seen.Add(this);

			var fringe = new Queue<TrainTrackNode>();

			foreach (TrainTrackNode track in LinkedNodes)
			{
				if(seen.Contains(track)) continue;

				fringe.Enqueue(track);
			}

			while (fringe.Count > 0)
			{
				var expand = fringe.Dequeue();

				seen.Add(expand);

				yield return expand;

				foreach (TrainTrackNode expandLink in expand.LinkedNodes)
				{
					if (! seen.Add(expandLink))
						continue;

					fringe.Enqueue(expandLink);
					//yield return expandLink;
				}
			}
		}

		/// <summary>
		/// For mainline nodes (exactly 2 links) find the next Node along the line.
		/// </summary>
		/// <param name="previous"></param>
		/// <returns></returns>
		public TrainTrackNode NextNode(ref TrainTrackNode previous)
		{
			if (LinkedNodes.Count() != 2) return null;

			foreach (var link in LinkedNodes)
			{
				if (link != previous)
				{
					previous = this;
					return link;
				}
			}

			return null;
		}

		#endregion Link

		protected override void DerivedToXElement(XElement baseElement)
		{
			var linkContainer = new XElement("links");
			foreach (var link in TrackLinks.Values.OrderBy(l => l.toNodeId))
			{
				var linkElement = link.ToXElement();
				linkContainer.Add(linkElement);
			}
			baseElement.Add(linkContainer);
		}

		/// <inheritdoc />
		public override string ToString()
		{
			var sb = new StringBuilder();
			ToStringCommonStart(sb);
			sb.Append($" @{LTransform.ExtractTranslation()}");

			if (LinkedNodes.Count() > 0)
			{
				sb.Append(" Links: '");
				sb.Append(string.Join("', '", LinkIds));
				sb.Append("'");
			}
			else
			{
				sb.Append(" Links: none");
			}
			if (!Valid)
			{
				sb.Append(" !Invalid!");
			}
			sb.Append("}");
			return sb.ToString();
		}
	}
}
