﻿// Copyright 2023 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using OpenToolkit.Mathematics;
using Shared.Exceptions;
using Shared.Serialization.DataTypes;
using System;
using System.Xml.Linq;

namespace DataModel.Tiles
{
	/// <summary>
	/// Base class for all lights.
	/// Exists because that is convenient for Editor, not really neccecary otherwise.
	/// </summary>
	public abstract class BaseLight : AbstractTileItem
	{
		public Vector3 color = Vector3.One;

		public BaseLight() { }

		public BaseLight(XElement element) : base(element)
		{
			try
			{
				color = VectorSer.Parse3f(element.Element("color"), allowNull: true, nullValue: Vector3.One);
			}
			catch (Exception e)
			{
				throw new XmlDataException(element, e);
			}
		}

		/// <inheritdoc />
		protected override void DerivedToXElement(XElement baseElement)
		{
			baseElement.Add(color.ToXElement(nameof(color)));
		}
	}
}