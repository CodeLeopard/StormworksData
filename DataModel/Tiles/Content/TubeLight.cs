﻿// Copyright 2023 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using Shared;
using Shared.Exceptions;
using Shared.Serialization;
using System;
using System.Text;
using System.Xml.Linq;

namespace DataModel.Tiles
{
	public class TubeLight : BaseLight
	{
		[VisibleProperty]
		public float length = 1f;

		[VisibleProperty]
		public float radius = 1f;

		// already have Transform from Base

		/// <inheritdoc />
		public override string meta_elementName => "light";
		/// <inheritdoc />
		public override string meta_containerName => "lights_tube";

		public TubeLight() { }

		public TubeLight(XElement element) : base(element)
		{
			try
			{
				length = meta_constructor_attributes.GetFloat(nameof(length), 1);
				radius = meta_constructor_attributes.GetFloat(nameof(radius), 1);
			}
			catch (Exception e)
			{
				throw new XmlDataException(element, e);
			}
		}

		/// <inheritdoc />
		protected override void DerivedToXElement(XElement baseElement)
		{
			base.DerivedToXElement(baseElement);

			baseElement.SetAttributeValue(nameof(length), length);
			baseElement.SetAttributeValue(nameof(radius), radius);
		}

		/// <inheritdoc />
		public override string ToString()
		{
			var sb = new StringBuilder();
			ToStringCommonStart(sb);
			sb.Append($" radius:'{radius}'");
			sb.Append($" color:{color}");
			ToStringCommonEnd(sb);
			return sb.ToString();
		}
	}
}