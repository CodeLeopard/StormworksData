﻿// Copyright 2025 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using Shared.Serialization;
using System;
using System.Text;
using System.Xml.Linq;

namespace DataModel.Tiles.Content
{
	/// <summary>
	/// Represents a link from one <see cref="TrainTrackNode"/> to another.
	/// That other Node will have a different link pointing in the opposite direction.
	/// </summary>
	public class TrainTrackLink
	{
		/// <summary>
		/// The <see cref="TrainTrackNode"/> this link is defined on.
		/// </summary>
		public TrainTrackNode FromNode { get; internal set; }

		/// <summary>
		/// The <see cref="TrainTrackNode"/> this link points to.
		/// </summary>
		public TrainTrackNode ToNode { get; internal set; }

		/// <summary>
		/// Our corresponding link that goes from <see cref="ToNode"/> to <see cref="FromNode"/>.
		/// </summary>
		public TrainTrackLink ReverseLink => ToNode.TrackLinks[FromNode.id];

		/// <summary>
		/// The id of the <see cref="TrainTrackNode"/> this links to.
		/// </summary>
		public readonly string toNodeId;

		#region ExtraProperties

		/// <summary>
		/// [ExtraProperty] Track Speed in m/s
		/// </summary>
		public float? TrackSpeed_ms;

		/// <summary>
		/// [ExtraProperty] SuperElevation in milimeters.
		/// </summary>
		public float? SuperElevation_mm;

		/// <summary>
		/// [ExtraProperty] Arbitrary name for the track.
		/// </summary>
		public string? TrackName;

		/// <summary>
		/// [ExtraProperty] Defines the catenary groups that apply to this track (Format TBD).
		/// </summary>
		public string? CatenarySpec;

		#endregion ExtraProperties

		#region Attribute Ids

		internal const string elem_self = "link";

		internal const string attr_id = "id";
		internal const string attr_trackSpeed = "__CT_TrackSpeed_ms";
		internal const string attr_superElevation = "__CT_SuperElevation_mm";

		internal const string attr_trackName = "__CT_TrackName";
		internal const string attr_catenary  = "__CT_catenary";

		#endregion Attribute Ids

		internal TrainTrackLink(string id)
		{
			this.toNodeId = id ?? throw new ArgumentNullException(nameof(id));
		}

		internal TrainTrackLink(TrainTrackNode fromNode, TrainTrackNode toNode)
		{
			FromNode = fromNode ?? throw new ArgumentNullException(nameof(fromNode));
			ToNode   = toNode   ?? throw new ArgumentNullException(nameof (toNode));

			toNodeId = toNode.id;
		}

		internal TrainTrackLink(XElement element)
		{
			var attrs = element.Attributes().XAttributeToDictionary();

			toNodeId = attrs.GetString(attr_id, null) ?? throw new Exception($"Missing required attribute '{attr_id}'.");

			TrackSpeed_ms     = attrs.GetNullableFloat(attr_trackSpeed, null);
			SuperElevation_mm = attrs.GetNullableFloat(attr_superElevation, null);

			TrackName    = attrs.GetString(attr_trackName, null);
			CatenarySpec = attrs.GetString(attr_catenary, null);
		}

		/// <summary>
		/// Copy the properties from <paramref name="other"/> into this object.
		/// </summary>
		/// <param name="other"></param>
		public void CopyExtraProperties(TrainTrackLink other)
		{
			this.TrackSpeed_ms     = other.TrackSpeed_ms;
			this.SuperElevation_mm = other.SuperElevation_mm;

			this.TrackName    = other.TrackName;
			this.CatenarySpec = other.CatenarySpec;
		}

		/// <summary>
		/// Check that the data in this object makes (at least a little) sense.
		/// </summary>
		public bool Valid => FromNode != null && ToNode != null;

		/// <summary>
		/// Equals for HashSet or Dictionary: Only compares <see cref="toNodeId"/>
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		public override bool Equals(object obj)
		{
			var link = obj as TrainTrackLink;
			if (link == null) return false;
			return this.toNodeId == link.toNodeId;
		}

		/// <summary>
		/// GetHashCode for HashSet or Dictionary: Only compares <see cref="toNodeId"/>
		/// </summary>
		/// <returns></returns>
		public override int GetHashCode()
		{
			return toNodeId.GetHashCode();
		}

		/// <summary>
		/// Create an <see cref="XElement"/> containing the data of this <see cref="TrainTrackLink"/>.
		/// </summary>
		/// <returns></returns>
		public XElement ToXElement()
		{
			var e = new XElement(elem_self);
			e.SetAttributeValue(attr_id, this.toNodeId);

			e.SetAttributeValue(attr_trackSpeed, TrackSpeed_ms);
			e.SetAttributeValue(attr_superElevation, SuperElevation_mm);

			e.SetAttributeValue(attr_trackName, TrackName);
			e.SetAttributeValue(attr_catenary, CatenarySpec);

			return e;
		}

		/// <inheritdoc />
		public override string ToString()
		{
			var sb = new StringBuilder();

			sb.Append($"{typeof(TrainTrackLink).Name} [{toNodeId}] <- [{FromNode?.id ?? "null"}]");
			if (!Valid)
			{
				sb.Append(" !Invalid!");
			}

			return sb.ToString();
		}
	}
}
