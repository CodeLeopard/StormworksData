﻿// Copyright 2022-2024 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;

namespace DataModel.DataTypes
{
	static class Strings
	{
		internal const string depreciatedMessage =
			"Depreciated: The game no longer uses this."
		  + " It may stop working in fututre updates without warning.";

		internal const string depreciatedButStillInUseMessage =
			"Depreciated: The game has marked Components with this as Depreciated,"
		  + " but those parts still exist."
		  + " It may stop working in fututre updates without warning.";

		internal const string notCurrentlyUsedMessage =
			"Note: The game currently does not have Components that use this value."
		  + " It is unknown if it works properly or will continue to do so in the future.";


		internal const string notCurrentlyUsedWorkingMessage =
			"Note: The game currently does not have Components that use this value."
		  + " It has been tested to work as expected but"
		  + " it is unknown if it will continue to do so in the future.";
	}

	/// <summary>
	/// Definition category is used for sorting in the inventory screen.
	/// </summary>
	public enum DefinitionCategory : UInt16
	{
		/// <summary>
		/// Structural blocks and some miscelanious.
		/// </summary>
		Structural = 0,
		/// <summary>
		/// Control and movement related components.
		/// Radio, Seat, Wheel, Wing, KeepActive
		/// </summary>
		VehicleControl = 1,
		/// <summary>
		/// Pivot, Hinge, Piston, Linear track, Button Keypad, Throttle, Sliding door, Hinged door, Custom door
		/// </summary>
		Mechanics = 2,
		/// <summary>
		/// Propeller, Rotor, Engine, Motor, Torque meter, Train bogie, Rocket
		/// </summary>
		Propulsion = 3,
		/// <summary>
		/// Seat, Bed, Light, Fluid nozzle, Equipment inventory, Heater, Cable anchor, winch, flare launcher, parachute
		/// </summary>
		SpecialistEquipment = 4,
		/// <summary>
		/// Logic gates, MicroController
		/// </summary>
		Logic = 5,
		/// <summary>
		/// Speaker, Display, Laser beacon, screen
		/// </summary>
		Displays = 6,
		/// <summary>
		/// Camera
		/// </summary>
		Sensors = 7,
		/// <summary>
		/// Rubber Tyre, railings.
		/// </summary>
		Decorative = 8,
		/// <summary>
		/// Fluid devices such as valves, tanks, pumps, radiators, etc.
		/// </summary>
		Fluid = 9,
		/// <summary>
		/// Electrical devices: Battery, charger, breaker, relay, generator, solar
		/// </summary>
		Electric = 10,
		/// <summary>
		/// Jet engine related components.
		/// </summary>
		JetEngine = 11,
		/// <summary>
		/// Weapon related components, guns, warheads, ammo handling.
		/// </summary>
		Weapon = 12,
		/// <summary>
		/// Modular engine components.
		/// </summary>
		ModularEngine = 13,
		/// <summary>
		/// Steam power related components, including nuclear and coal based heat generation.
		/// </summary>
		SteamPower = 14,
		/// <summary>
		/// Window components in various shapes and sizes.
		/// </summary>
		Window = 15,
	}

	/// <summary>
	/// The <see cref="DefinitionType"/> determines functionality the component has.
	/// </summary>
	public enum DefinitionType : UInt16
	{
		/// <summary>
		/// See <see cref="BlockType"/> for subtypes within this category.
		/// Cannot have mesh_data
		/// </summary>
		Structural = 0,
		/// <summary>
		/// See <see cref="SeatPose"/> for subtypes within this category.
		/// </summary>
		Seat = 1,
		/// <summary>
		/// Propellers etc.
		/// </summary>
		ForceEmitter = 2,
		/// <summary>
		/// Landing float
		/// </summary>
		Buoyancy = 3,
		/// <summary>
		/// Official name is "Wheel", but it's the old implementation.
		/// </summary>
		WheelOld = 4,
		Engine   = 5,
		/// <summary>
		/// Pipes, either for fluid or for mechanical (shaft) power.
		/// </summary>
		Pipe = 6,
		/// <summary>
		/// Part that connects to another <see cref="Vehicles.Body"/> using a permanent physics joint.
		/// </summary>
		MultiBody = 7,
		/// <summary>
		/// General User input devices: buttons, throttle, etc.
		/// See <see cref="ButtonType"/> for subtypes within this category.
		/// DevName: 'Button'.
		/// </summary>
		Button = 8,
		/// <summary>
		/// Items with Surfaces and a Mesh but no functionality.
		/// </summary>
		Basic = 9,
		/// <summary>
		/// See <see cref="LightType"/> for subtypes within this category.
		/// </summary>
		Light = 10,
		/// <summary>
		/// Spotlight on a integrated single axis pivot.
		/// Official name: "Dynamic light"
		/// </summary>
		LightDynamic = 11,
		/// <summary>
		/// Unknown purpose.
		/// </summary>
		[Obsolete(Strings.notCurrentlyUsedMessage)]
		Basic_Dynamic       = 12,
		DoorSlidingElectric = 13,
		/// <summary>
		/// The "Gyro" control system.
		/// </summary>
		Gyro = 14,
		[Obsolete(Strings.depreciatedMessage)]
		MultiBodyTorqueJoint = 15,
		/// <summary>
		/// Logic boxes, sensors, 'old' radar
		/// See <see cref="LogicGateType"/> for subtypes within this category.
		/// </summary>
		LogicGate = 16,
		/// <summary>
		/// Connects two separate vehicles with a physics joint of considerable strength.
		/// Able to transfer Composite and power or fluid depending on variant.
		/// See <see cref="ConnectorType"/> for subtypes within this category.
		/// </summary>
		Connector   = 17,
		/// <summary>
		/// Movable Aerodynamic and Hydrodynamic control surfaces.
		/// </summary>
		AeroHydroDynamic = 18,
		Ladder           = 19,
		/// <summary>
		/// The old style winch with a wire permanently connected to a included <see cref="Vehicles.Body"/>.
		/// Allows logicNode connections between the other Body.
		/// </summary>
		[Obsolete(Strings.depreciatedButStillInUseMessage)]
		WinchDepreciated = 20,
		/// <summary>
		/// See <see cref="IndicatorType"/> for subtypes within this category.
		/// </summary>
		Indicator = 21,
		/// <summary>
		/// Handhold that the player can grab onto.
		/// </summary>
		Handle = 22,
		[Obsolete(Strings.depreciatedMessage)]
		Spawner = 23,
		/// <summary>
		/// Tanks, radiators, intakes, valves, but also speaker, transponder, etc.
		/// See <see cref="FluidDeviceType"/> for subTypes in this category.
		/// </summary>
		FluidDevice = 24,
		FluidPump   = 25,
		[Obsolete(Strings.notCurrentlyUsedMessage)]
		Unknown_26,
		/// <summary>
		/// Magical magnet that connects to anything but fluid and gas.
		/// </summary>
		MagAll = 27,
		/// <summary>
		/// PaintAble Sign and Indicator.
		/// </summary>
		Sign = 28,
		/// <summary>
		/// Door with docking capability.
		/// </summary>
		DoorDock = 29,
		/// <summary>
		/// Door parts to create custom shape door.
		/// See <see cref="CustomDoorType"/> for subtypes within this category.
		/// </summary>
		DoorCustom = 30,
		/// <summary>
		/// See <see cref="ElectricComponentType"/> for subTypes within this category.
		/// </summary>
		Electrical = 31,
		/// <summary>
		/// See <see cref="InventoryClass"/> for sizes and shapes.
		/// See <see cref="InventoryItem"/> for items.
		/// </summary>
		EquipmentInventory = 32,
		/// <summary>
		/// DevName: 'Torque'.
		/// See <see cref="TransmissionComponentType"/> for subTypes within this category.
		/// </summary>
		Transmission = 33,
		/// <summary>
		/// See <see cref="JetComponentType"/> for subTypes within this category.
		/// </summary>
		JetEngineComponent = 34,
		Parachute          = 35,
		TrainBogie         = 36,
		MicroProcessor     = 37,
		/// <summary>
		/// RGB Light, RGB indicator.
		/// See <see cref="CompositeDeviceType"/> for subtypes within this category.
		/// </summary>
		CompositeIO = 38,
		Heater      = 39,
		Ski         = 40,
		/// <summary>
		/// See <see cref="WheelType"/> for subTypes within this category.
		/// DevName: 'WheelAdvanced'
		/// </summary>
		Wheel   = 41,
		/// <summary>
		/// Video cameras
		/// </summary>
		Camera  = 42,
		/// <summary>
		/// Video monitors / screens and transparent huds.
		/// </summary>
		Monitor = 43,
		/// <summary>
		/// See <see cref="WeaponType"/> for subtypes within this category.
		/// See also: <seealso cref="WeaponClass"/> and <seealso cref="WeaponBeltType"/>.
		/// </summary>
		Weapon = 44,
		Radio  = 45,
		/// <summary>
		/// DevName: 'NoSleep'.
		/// Keeps vehicles loaded in a 2km radius.
		/// </summary>
		KeepActive = 46,
		/// <summary>
		/// See <see cref="RocketType"/> for subtypes within this category.
		/// DevName: 'Solid Rocket'
		/// </summary>
		RocketPart = 47,
		/// <summary>
		/// Seems to use <see cref="LogicGateSubType"/> to discriminate between rope types.
		/// </summary>
		RopeHook = 48,
		/// <summary>
		/// Flare launcher.
		/// </summary>
		FlareLauncher = 49,
		/// <summary>
		/// Manually operated hinged door.
		/// </summary>
		DoorHingedManual = 50,
		/// <summary>
		/// Manually operated sliding door.
		/// </summary>
		DoorSlidingManual = 51,
		/// <summary>
		/// Modular engine parts.
		/// </summary>
		ModularEngine     = 52,
		/// <summary>
		/// Steam related parts: boiler, condenser, etc.
		/// </summary>
		Steam = 53,
		/// <summary>
		/// Nuclear reactor parts: fuel assembly, control rod, etc.
		/// </summary>
		Nuclear = 54,
		/// <summary>
		/// Friction pad.
		/// </summary>
		Friction = 55,
		/// <summary>
		/// The 'new' Composite-only radar blocks with noise and less control.
		/// </summary>
		Radar = 56,
		/// <summary>
		/// The 'new' Composite-only sonar blocks with noise and less control.
		/// </summary>
		Sonar = 57,
		/// <summary>
		/// Vehicle mounted welder and End-Effector.
		/// </summary>
		Mounted_Effector = 58,
		/// <summary>
		/// Oil Rig parts.
		/// </summary>
		Oil_Rig = 59,
	}


	/// <summary>
	/// Flags may add additional functionality to blocks,
	/// determine how they can be used or
	/// are used to differentiate subtypes within a <see cref="DefinitionType"/>.
	/// </summary>
	[Flags]
	public enum DefinitionFlags : UInt32
	{
		/// <summary>
		/// No flags applied.
		/// </summary>
		None = 0u,
		SeparatePhysics = 1u                << 0,
		/// <summary>
		/// Only for <see cref="DefinitionType.ForceEmitter"/>.
		/// </summary>
		ForceEmitterUnderwaterOnly = 1u     << 1,
		/// <summary>
		/// Only for <see cref="DefinitionType.ForceEmitter"/>.
		/// </summary>
		ForceEmitterBladePitchMode = 1u     << 2,
		/// <summary>
		/// Allow drag placement along the X axis (in/relative to the blocks own voxel coordinates).
		/// </summary>
		DragPlace_X = 1u                    << 3,
		/// <summary>
		/// Allow drag placement along the Y axis (in/relative to the blocks own voxel coordinates).
		/// </summary>
		DragPlace_Y = 1u                    << 4,
		/// <summary>
		/// Allow drag placement along the Z axis (in/relative to the blocks own voxel coordinates).
		/// </summary>
		DragPlace_Z = 1u                    << 5,
		MultiBodyParent = 1u                << 6,
		MultiBodyChild = 1u                 << 7,
		/// <summary>
		/// Used both for <see cref="DefinitionType.MultiBody"/> and <see cref="DefinitionType.Camera"/>.
		/// </summary>
		RoboticJoint_Gimbal = 1u            << 8,
		/// <summary>
		/// Only for <see cref="DefinitionType.MultiBody"/>
		/// </summary>
		RoboticVelocity = 1u                << 9,
		BoundingBoxPhysics = 1u             << 10,
		IsStatic = 1u                       << 11,
		/// <summary>
		/// Only for <see cref="DefinitionType.MultiBody"/>
		/// </summary>
		RoboticUpDown = 1u                  << 12,
		/// <summary>
		/// Player can enter a name for the block.
		/// </summary>
		Nameable = 1u                       << 13,
		HardcoreOnly = 1u                   << 14,
		/// <summary>
		/// Only for <see cref="DefinitionType.AeroHydroDynamic"/>
		/// </summary>
		Aerofoil = 1u                       << 15,
		/// <summary>
		/// Only for <see cref="DefinitionType.MultiBody"/>
		/// </summary>
		Spring = 1u                         << 16,
		/// <summary>
		/// Only for <see cref="DefinitionType.MultiBody"/>
		/// </summary>
		PositiveLimit = 1u                  << 17,
		SoftcoreOnly = 1u                   << 18,
		/// <summary>
		/// For <see cref="DefinitionType.ForceEmitter"/>: disables the blade physics.
		/// </summary>
		ForceEmitterDisableBadePhysics = 1u << 19,
		/// <summary>
		/// For <see cref="DefinitionType.Connector"/>: disables the magnetic attraction force.
		/// </summary>
		NoMagnet = 1u                       << 20,
		RotatingExhaust = 1u                << 21,
		/// <summary>
		/// <see cref="DefinitionType.Indicator"/> has no additive (emissive).
		/// </summary>
		NoAdditive = 1u                     << 22,
		NoSuspension = 1u                   << 23,
		RadioRx = 1u                        << 24,
		RadioTX = 1u                        << 25,
		RadioVideo = 1u                     << 26,
		Flag_27 = 1u                        << 27,
		RadioAudio = 1u                     << 28,
		/// <summary>
		/// Component does not show in the Inventory.
		/// </summary>
		HiddenFromInventory = 1u            << 29,
		AdvancedRotor = 1u                  << 30,
		Flag_31 = 1u                        << 31,
	}


	public enum LogicNodeType : byte
	{
		OnOff     = 0,
		Number    = 1,
		Power     = 2,
		Fluid     = 3,
		Electric  = 4,
		Composite = 5,
		Video     = 6,
		Audio     = 7,
		Rope      = 8,
	}

	public enum LogicNodeMode : byte
	{
		Output = 0,
		Input  = 1,
	}

	[Flags]
	public enum LogicNodeFlags : byte
	{
		Invisible = 1,
	}

	/// <summary>
	/// SubTypes within <see cref="DefinitionType.Structural"/>.
	/// </summary>
	public enum BlockType : UInt16
	{
		Square = 0,
		Wedge = 1,
		Pyramid = 2,
		InvPyramid = 3,
		Wedge_2 = 4,
		Pyramid_2 = 5,
		InvPyramid_2 = 6,
		Wedge_4 = 7,
		Pyramid_4 = 8,
		InvPyramid_4 = 9,
		Pyramid_2_2 = 10,
		Pyramid_2_4 = 11,
		Pyramid_4_4 = 12,
		InvPyramid_2_2 = 13,
		InvPyramid_2_4 = 14,
		InvPyramid_4_4 = 15,
	}

	/// <summary>
	/// Subtypes within <see cref="DefinitionType.Seat"/>.
	/// </summary>
	public enum SeatPose : byte
	{
		/// <summary>
		/// Standard seat.
		/// </summary>
		Seat = 0,
		/// <summary>
		/// Seat with control stick.
		/// </summary>
		Seat_Stick = 1,
		/// <summary>
		/// Standing with wheel.
		/// </summary>
		Helm = 2,
		/// <summary>
		/// Laying down.
		/// </summary>
		Bed = 3,
		/// <summary>
		/// Strapped into harness.
		/// </summary>
		Harness = 4,
		/// <summary>
		/// Seat with steering wheel.
		/// </summary>
		Seat_Wheel = 5,
		/// <summary>
		/// Standing with handle.
		/// </summary>
		Handle = 6,
	}

	/// <summary>
	/// Subtypes within <see cref="DefinitionType.Wheel"/>.
	/// </summary>
	public enum WheelType : byte
	{
		Wheel           = 0,
		TankTreadSmall  = 1,
		TankTreadMedium = 2,
		TankTreadLarge  = 3,
	}

	/// <summary>
	/// Subtypes within <see cref="DefinitionType.Button"/>.
	/// </summary>
	public enum ButtonType : byte
	{
		/// <summary>
		/// Push button.
		/// </summary>
		Push = 0,
		/// <summary>
		/// Toggle button.
		/// Also used by some not button related blocks. This could be a mistake on the game's part.
		/// </summary>
		Toggle = 1,
		/// <summary>
		/// The 'Key button' where you press and hold to activate and press to deactivate.
		/// </summary>
		KeyTurn = 2,
		/// <summary>
		/// Locked button
		/// </summary>
		Locked = 3,
		Throttle = 4,
		/// <summary>
		/// Keypad for a single number.
		/// </summary>
		Keypad_Small = 5,
		/// <summary>
		/// Keypad for two numbers, typically used for GPS coordinates.
		/// </summary>
		Keypad_Large = 6,
	}

	/// <summary>
	/// Subtypes within <see cref="DefinitionType.Light"/>.
	/// </summary>
	public enum LightType : byte
	{
		/// <summary>
		/// PointLight, does not cast shadow.
		/// </summary>
		Point = 0,
		/// <summary>
		/// SpotLight, casts a shadow.
		/// </summary>
		Spot = 1,
	}


	/// <summary>
	/// Subtypes within <see cref="DefinitionType.DoorCustom"/>.
	/// </summary>
	public enum CustomDoorType : byte
	{
		FrameStraight = 0,
		FrameCorner   = 1,
		PanelStraight = 2,
		PanelCorner   = 3,
		/// <summary>
		/// Used to lock the panel to the frame.
		/// </summary>
		Controller = 4,
	}
	/// <summary>
	/// Subtypes within <see cref="DefinitionType.LogicGate"/>.
	/// See <see cref="LogicGateSubType"/> for subtypes within
	/// </summary>
	public enum LogicGateType : byte
	{
		Bool_OR = 1,
		Bool_XOR = 2,
		Bool_Not = 3,
		/// <summary>
		/// Logic gate that holds a number that can be increased or decreased using two boolean inputs.
		/// </summary>
		Bool_UpDown = 4,
		Float_Junction = 5,
		Float_Add = 6,
		Float_Subtract = 7,
		Float_Invert = 8,
		Bool_PushToToggle = 9,
		Float_SwitchInput = 10,
		Bool_Blink = 11,
		Bool_Delay = 12,
		Float_Threshold = 13,
		Float_Constant = 14,
		Float_Multiply = 15,
		Float_Clamp = 16,
		Bool_Constant = 17,
		Sens_Altimeter = 18,
		Sens_LinearSpeed = 19,
		Sens_Tilt = 20,
		[Obsolete(Strings.notCurrentlyUsedMessage)]
		Unknown_21 = 21,
		/// <summary>
		/// Less than
		/// </summary>
		Float_LT = 22,
		Sens_Raycast = 23,
		/// <summary>
		/// This was used in the no longer existing 'simple mode'.
		/// </summary>
		[Obsolete(Strings.depreciatedMessage)]
		TorqueAdd = 24,
		/// <summary>
		/// This was used in the no longer existing 'simple mode'.
		/// </summary>
		[Obsolete(Strings.depreciatedMessage)]
		TorqueMeter = 25,
		Float_Modulo = 26,
		Float_Counter = 27,
		Float_Counter_PingPong = 28,
		PID = 29,
		Sens_GPS = 30,
		Sens_Compass = 31,
		Float_Register = 32,
		/// <summary>
		/// Greater than
		/// </summary>
		Float_GT = 33,
		Bool_SR = 34,
		Bool_JK = 35,
		Bool_Capacitor = 36,
		Float_Divide = 37,
		Float_Abs = 38,
		Sens_Humidity = 39,
		Sens_Rain = 40,
		Sens_Wind = 41,
		Sens_Time = 42,
		Float_Sin = 43,
		Float_Exp = 44,
		Float_Expression = 45,
		/// <summary>
		/// Interacts with a nearby train line junction in the world.
		/// Behaves like a push to toggle.
		/// The game does not save the state of junctions.
		/// </summary>
		TrainJunction = 46,
		Sens_Player = 47,
		Sens_LaserPoint = 48,
		Sens_Angular = 49,
		/// <summary>
		/// See <see cref="LogicGateSubType"/> for subtypes within this category.
		/// </summary>
		[Obsolete(Strings.depreciatedButStillInUseMessage)]
		Radar = 50,
		Microphone = 51,
		Speaker = 52,
		Sens_Temperature = 53,
		Transponder = 54,
		Transponder_Locator = 55,
		/// <summary>
		/// Radar Detector
		/// </summary>
		Sens_Radar = 56,
		/// <summary>
		/// Responds to instantaneous change in velocity.
		/// </summary>
		Sens_Impact = 57,
		Sens_Pressure = 58,
		Sens_Radiation = 59,
	}

	public enum LogicGateSubType : byte
	{
		Default = 0,
		Sonar = 1,
		Radar = 2,
		RadarMultiTarget = 3,
	}

	// Behind the scenes the game re-uses the attribute for <see cref="LogicGateSubType"/>.
	public enum Meta_RopeType : byte
	{
		Rope = 0,
		Fluid = 1,
		Composite = 2,
		Any = 3,
	}

	/// <summary>
	/// Subtypes within <see cref="DefinitionType.Indicator"/>.
	/// </summary>
	public enum IndicatorType : byte
	{
		/// <summary>
		/// Dial with needle.
		/// </summary>
		Dial = 0,
		/// <summary>
		/// Simple indicator light.
		/// </summary>
		Light = 1,
		/// <summary>
		/// Artificial horizon.
		/// </summary>
		Horizon = 2,
		/// <summary>
		/// Digital number display.
		/// (Behind the scenes this uses several rollers that move in discrete increments.)
		/// </summary>
		Number = 3,
		Compass = 4,
		/// <summary>
		/// Vertical gauge with two indicators.
		/// </summary>
		Gauge = 5,
		/// <summary>
		/// Speaker that can be configured to play a single sound.
		/// Configuration options include the sound, volume and pitch/speed.
		/// </summary>
		Buzzer = 6,
		/// <summary>
		/// Speaker with no configuration options.
		/// </summary>
		Horn = 7,
		[Obsolete(Strings.notCurrentlyUsedMessage)]
		Unknown_8 = 8,
		[Obsolete(Strings.notCurrentlyUsedMessage)]
		Unknown_9 = 9,
		/// <summary>
		/// Beacon that is visible to the laser point sensor.
		/// </summary>
		LaserBeacon = 10,
		/// <summary>
		/// Speaker that plays a single hardcoded sound with varying pitch/speed.
		/// The pitch depends on the internal state of the siren, which can be regulated using a boolean signal.
		/// </summary>
		Siren = 11,
	}


	/// <summary>
	/// Subtypes within <see cref="DefinitionType.Connector"/>.
	/// </summary>
	public enum ConnectorType : byte
	{
		/// <summary>
		/// Large connector with both fluid, electric and mechanical power connections.
		/// Aligns with the plane but can rotate freely on the connection axis.
		/// Must be activated with a boolean signal to connect.
		/// </summary>
		Large = 0,
		/// <summary>
		/// Small connector with a ball joint.
		/// Must be activated with a boolean signal to connect.
		/// </summary>
		Small = 1,
		/// <summary>
		/// Connector that can hinge along it's long axis while connected.
		/// Must be activated with a boolean signal to connect.
		/// </summary>
		Hinge = 2,
		/// <summary>
		/// Small connector with ball physics joint.
		/// Activated by default, requires boolean signal to disconnect.
		/// </summary>
		Fluid = 3,
		/// <summary>
		/// Small connector with ball physics joint.
		/// Activated by default, requires boolean signal to disconnect.
		/// </summary>
		Electric = 4,
		/// <summary>
		/// Connector that can slide freely along a track of <see cref="SliderTrack"/>.
		/// Has a brake to lock it in place and a signal to disconnect.
		/// </summary>
		SliderGripper = 5,
		/// <summary>
		/// Track along which <see cref="SliderGripper"/> can slide.
		/// </summary>
		SliderTrack = 6,
		/// <summary>
		/// Hardpoint connector. Has a very strong fixed physics connection.
		/// Two orientations are allowed for the <see cref="HardpointChild"/>.
		/// Has signals to release or to launch the child and
		/// also a number output to identify the kind of child.
		/// </summary>
		HardpointParent = 7,
		/// <summary>
		/// Hardpoint connector. Has a boolean that enables only when the <see cref="HardpointParent"/> was
		/// signaled to launch.
		/// </summary>
		HardpointChild = 8,
	}

	public enum GyroType : byte
	{
		Default = 0,
		// there really are no other values.
	}


	/// <summary>
	/// Subtypes within <see cref="DefinitionType.FluidDevice"/>.
	/// </summary>
	public enum FluidDeviceType
	{
		/// <summary>
		/// Small fluid port.
		/// </summary>
		Port_Small = 0,
		/// <summary>
		/// Measures the volume of fluid in a enclosed compartment.
		/// </summary>
		MeterVolume = 1,
		/// <summary>
		/// Spray nozzle
		/// </summary>
		Nozzle = 2,
		/// <summary>
		/// One way valve.
		/// </summary>
		ValveCheck = 3,
		/// <summary>
		/// Two state valve.
		/// </summary>
		ValveBool = 4,
		/// <summary>
		/// Valve with continuous control.
		/// </summary>
		ValveVariable = 5,
		/// <summary>
		/// Measures the pressure inside a pipe system.
		/// </summary>
		MeterPressure = 6,
		/// <summary>
		/// Fluid spawner.
		/// </summary>
		Spawner = 7,
		Tank = 8,
		/// <summary>
		/// Radiator
		/// </summary>
		Exchanger_Fluid_Ambient = 9,
		Filter = 10,
		/// <summary>
		/// Propulsion device that takes mechanical power and water to create a jet of water that produces thrust.
		/// </summary>
		PumpJet = 11,
		/// <summary>
		/// Large Fluid port.
		/// </summary>
		Port_Large = 12,
		Converter_Catalytic = 13,
		/// <summary>
		/// Pump driven by Torque connection.
		/// </summary>
		Pump_Torque = 14,
		/// <summary>
		/// Fluid port which produces higher pressure at speed (in the correct direction).
		/// </summary>
		Port_Scoop = 15,
		/// <summary>
		/// Heat exchanger.
		/// </summary>
		Exchanger_Fluid_Fluid = 16,
		/// <summary>
		/// Heat exchanger.
		/// </summary>
		Exchanger_Gas_Gas = 17,
		/// <summary>
		/// Heat exchanger.
		/// </summary>
		Exchanger_Fluid_Gas = 18,
		/// <summary>
		/// Physics optimizer: When placed in an enclosed volume that volume will be considered not-hollow for physics purposes.
		/// This means that the Physics engine can use less and larger physics primitives to generate the shape
		/// however it cannot be entered.
		/// </summary>
		PhysicsFlooder = 19,
		/// <summary>
		/// Tank with 100x capacity but also 100x weight.
		/// </summary>
		[Obsolete(Strings.notCurrentlyUsedWorkingMessage)]
		CompressedTank = 20,
		/// <summary>
		/// Boolean Valve that can be manually operated by a Player.
		/// </summary>
		[Obsolete(Strings.notCurrentlyUsedWorkingMessage)]
		ValveBoolManual = 21,
		/// <summary>
		/// Pump that can be manually operated by a Player.
		/// </summary>
		[Obsolete(Strings.notCurrentlyUsedWorkingMessage)]
		Pump_Manual = 22,
	}

	/// <summary>
	/// Subtypes within <see cref="DefinitionType.Transmission"/>.
	/// </summary>
	public enum TransmissionComponentType : byte
	{
		Clutch = 0,
		Gearbox = 1,
		/// <summary>
		/// Torque meter.
		/// </summary>
		Meter = 2,
	}

	/// <summary>
	/// Subtypes within <see cref="DefinitionType.JetEngineComponent"/>.
	/// </summary>
	public enum JetComponentType : byte
	{
		Intake = 0,
		Compressor = 1,
		CombustionChamber = 2,
		Turbine = 3,
		Exhaust = 4,
		Duct = 5,
	}

	/// <summary>
	/// Sizes within <see cref="DefinitionType.EquipmentInventory"/>
	/// </summary>
	public enum InventoryClass : byte
	{
		SingleHanded = 1,
		TwoHanded    = 2,
		Outfit       = 3
	}

	/// <summary>
	/// Items for an <see cref="DefinitionType.EquipmentInventory"/>
	/// </summary>
	public enum InventoryItem : byte
	{
		Outfit_Diving = 1,
		Outfit_FireFighter = 2,
		Outfit_Scuba = 3,
		Outfit_Parachute = 4,
		Outfit_Arctic = 5,
		Binoculars = 6,
		Cable = 7,
		Compass = 8,
		Defibrilator = 9,
		FireExtinguisher = 10,
		FirstAid = 11,
		Flare = 12,
		FlareGun = 13,
		FlareGunAmmo = 14,
		FlashLight = 15,
		Hose = 16,
		NightVisionBino = 17,
		OxygenMask = 18,
		Radio = 19,
		RadioSignalLocator = 20,
		RemoteControl = 21,
		Rope = 22,
		StrobeLight = 23,
		StrobeLightInfrared = 24,
		Transponder = 25,
		WelderUnderwater = 26,
		Welder = 27,
		[Obsolete(Strings.notCurrentlyUsedMessage)]
		Unknown_28 = 28,
		Outfit_Hazmat = 29,
		GeigerCounter = 30,
		C4_Explosive = 31,
		C4_detonator = 32,
		SpearGun = 33,
		SpearGunAmmo = 34,
		Pistol = 35,
		PistolAmmo = 36,
		SMG = 37,
		SMGAmmo = 38,
		Rifle = 39,
		RifleAmmo = 40,
		Grenade = 41,
	}

	/// <summary>
	/// Subtypes within <see cref="DefinitionType.Electrical"/>.
	/// </summary>
	public enum ElectricComponentType : byte
	{
		Motor = 0,
		Generator = 1,
		Battery = 2,
		CircuitBreaker = 3,
		Relay = 4,
		Solar = 5,
		Diode = 6,
	}

	/// <summary>
	/// Subtypes within <see cref="DefinitionType.CompositeIO"/>.
	/// </summary>
	public enum CompositeDeviceType : byte
	{
		LightRGB = 0,
		IndicatorRGB = 1,
		InstrumentPanel = 2,
	}

	/// <summary>
	/// Subtypes within <see cref="DefinitionType.RocketPart"/>.
	/// </summary>
	public enum RocketType : byte
	{
		/// <summary>
		/// Thruster of the rocket.
		/// </summary>
		Nozzle = 0,
		/// <summary>
		/// Body containing fuel.
		/// </summary>
		Body = 1,
		/// <summary>
		/// Body with controlable fins.
		/// </summary>
		Fins = 2,
	}


	/// <summary>
	/// Subtypes within <see cref="DefinitionType.Weapon"/>.
	/// </summary>
	public enum WeaponType : byte
	{
		WarheadEMP = 0,
		Warhead = 1,
		Gun = 2,
		Belt = 3,
		Barrel = 4,
		Drum = 5,
		RocketLauncher = 6,
		Muzzle = 7,
	}

	/// <summary>
	/// Size classes within <see cref="DefinitionType.Weapon"/>.
	/// </summary>
	public enum WeaponClass : byte
	{
		/// <summary>
		/// Manual reload only
		/// </summary>
		MachineGun = 0,
		/// <summary>
		/// Light AutoCannon, 1x1x3 ammo belt.
		/// </summary>
		Light = 1,
		/// <summary>
		/// Rotary AutoCannon, 1x1x3 ammo belt.
		/// </summary>
		Rotary = 2,
		/// <summary>
		/// Heavy AutoCannon, 1x1x3 ammo belt.
		/// </summary>
		Heavy = 3,
		/// <summary>
		/// Tank Cannon, 1x1x3 shells.
		/// </summary>
		Battle = 4,
		/// <summary>
		/// Artillery Cannon, 1x1x4 shells.
		/// </summary>
		Artillery = 5,
		/// <summary>
		/// Larger <see cref="Artillery"/>, 3x3x7 shells.
		/// </summary>
		Bertha = 6,
	}

	/// <summary>
	/// Subtypes within <see cref="WeaponType.Belt"/>.
	/// </summary>
	public enum WeaponBeltType : byte
	{
		Belt = 0,
		Motorized = 1,
		/// <summary>
		/// Connector that transfers ammo between vehicle(body)s.
		/// </summary>
		Receiver = 2,
		Junction = 3,
	}

}
