﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UTests
{
	public static class AssertExtensions
	{
		public static void Contains<T>(this Assert instance, ICollection<T> collection, T element, string message = null)
		{
			if (!collection.Contains(element))
			{
				Assert.Fail(message ?? $"{collection} should contain {element}.");
			}
		}

		public static void NotContains<T>(this Assert instance, ICollection<T> collection, T element, string message = null)
		{
			if (collection.Contains(element))
			{
				Assert.Fail(message ?? $"{collection} should not contain {element}.");
			}
		}

		public static void EqualsOneOf<T>
			(this Assert instance, ICollection<T> expected, T actual, string message = null)
		{
			if (!expected.Contains(actual))
			{
				Assert.Fail(message ?? $"{actual} is not one of {{{string.Join(", ", expected)}}}.");
			}
		}

		public static void SequenceEqualsWithAlternatives<T>
			(this Assert instance, IReadOnlyList<ICollection<T>> expected, IReadOnlyList<T> actual, string message = null)
		{
			if (expected.Count != actual.Count)
			{
				Assert.Fail("Collections don't have the same length.");
			}

			for (int i = 0; i < expected.Count; i++)
			{
				var a = actual[i];
				var e = expected[i];

				if (! e.Contains(a))
				{
					Assert.Fail(message ?? $"{a} at position {i} is not one of {{{string.Join(", ", e)}}}.");
				}
			}
		}

		public static void ContainsExactlyOnce<T>
			(this Assert instance, ICollection<T> collection, T element, string message_multiple = null, string message_never = null)
		{
			bool found = false;
			foreach (T e in collection)
			{
				if (element.Equals(e))
				{
					if (found) Assert.Fail(message_multiple ?? $"The collection contains {element} more than once.");

					found = true;
				}
			}

			if(! found) Assert.Fail(message_never ?? $"The collection does not contain {element}.");
		}

		public static void HasNoDuplicates<T>
			(this Assert instance, ICollection<T> collection, string message = null)
		{
			var seen = new HashSet<T>();

			foreach (T element in collection)
			{
				if (! seen.Add(element))
				{
					Assert.Fail(message ?? $"{collection} contains duplicate element {element}.");
				}
			}
		}
	}
}
