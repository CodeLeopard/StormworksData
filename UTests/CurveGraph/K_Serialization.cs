﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

using CurveGraph;
using CurveGraph.Interface;

using CustomSerialization;
using CustomSerialization.Specification;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using OpenToolkit.Mathematics;

using Shared.Serialization;

namespace UTests.CurveGraph
{
	[TestClass]
	public class K_Serialization
	{
		private StreamingContext context = new StreamingContext
			(StreamingContextStates.File | StreamingContextStates.Persistence);


		#region TestHelpers
		[Serializable]
		public class Mock_NodeCustomData : ICurveNodeCustomData, IEquatable<Mock_NodeCustomData>
		{
			private int _testNumber;

			public int TestNumber
			{
				get => _testNumber;
				set
				{
					if (value == _testNumber) return;
					_testNumber = value;
					Changed?.Invoke(false);
				}
			}

			public string TestString;


			internal Mock_NodeCustomData(int number, string str)
			{
				TestNumber = number;
				TestString = str;
			}

			/// <inheritdoc />
			public ICurveNodeCustomData CloneForMultiThreadedReading()
			{
				return new Mock_NodeCustomData(TestNumber, TestString);
			}

			/// <inheritdoc />
			[field: NonSerialized]
			public event Action<bool> Changed;

			/// <inheritdoc />
			public bool Equals(Mock_NodeCustomData other)
			{
				if (ReferenceEquals(null, other)) return false;
				if (ReferenceEquals(this, other)) return true;
				return _testNumber == other._testNumber;
			}

			/// <inheritdoc />
			public override bool Equals(object obj)
			{
				if (ReferenceEquals(null, obj)) return false;
				if (ReferenceEquals(this, obj)) return true;
				if (obj.GetType() != this.GetType()) return false;
				return Equals((Mock_NodeCustomData)obj);
			}

			/// <inheritdoc />
			public override int GetHashCode()
			{
				return HashCode.Combine(_testNumber, TestString);
			}
		}

		[Serializable]
		public class Mock_ConnectionCustomData : IConnectionCustomData, IEquatable<Mock_ConnectionCustomData>
		{
			private int _testNumber;

			public int TestNumber
			{
				get => _testNumber;
				set
				{
					if (value == _testNumber) return;
					_testNumber = value;
					Changed?.Invoke(this);
				}
			}

			public string TestString;

			internal Mock_ConnectionCustomData(int number, string str)
			{
				TestNumber = number;
				TestString = str;
			}

			/// <inheritdoc />
			public IConnectionCustomData CloneForMultiThreadedReading()
			{
				return new Mock_ConnectionCustomData(TestNumber, TestString);
			}

			/// <inheritdoc />
			[field: NonSerialized]
			public event Action<IConnectionCustomData> Changed;

			/// <inheritdoc />
			public bool Equals(Mock_ConnectionCustomData other)
			{
				if (ReferenceEquals(null, other)) return false;
				if (ReferenceEquals(this, other)) return true;
				return _testNumber == other._testNumber;
			}

			/// <inheritdoc />
			public override bool Equals(object obj)
			{
				if (ReferenceEquals(null, obj)) return false;
				if (ReferenceEquals(this, obj)) return true;
				if (obj.GetType() != this.GetType()) return false;
				return Equals((Mock_ConnectionCustomData)obj);
			}

			/// <inheritdoc />
			public override int GetHashCode()
			{
				return HashCode.Combine(_testNumber, TestString);
			}
		}


		public List<CurveNode> TrivialSetup()
		{
			var n0 = new CurveNode(new Vector3(0, 0,  0), new Vector3(1,  0, 0));
			var n1 = new CurveNode(new Vector3(1, 0,  0), new Vector3(-1, 0, 0));
			var n2 = new CurveNode(new Vector3(0, -1, 1));

			n0.AutoSmooth = false;
			n1.AutoSmooth = false;
			n2.AutoSmooth = true;

			n0.Curvature = 0.1f;
			n1.Curvature = 0.5f;
			n2.Curvature = 1.22345f;

			n0.Roll = 0.123456f;
			n1.Roll = 1.987654f;

			n0.Data = new Mock_NodeCustomData(0, "Foo");
			n1.Data = new Mock_NodeCustomData(1, "Bar");
			n2.Data = new Mock_NodeCustomData(2, "Baz");

			var c0_1 = n0.CreateConnection(n1);
			var c1_2 = n1.CreateConnection(n2);

			c0_1.data = new Mock_ConnectionCustomData(0, "");
			c1_2.data = new Mock_ConnectionCustomData(1, "With Special, Xml unapproved characters: </{([.,?'`\"])}\\> ~!@#$%^&*-_+=");

			c0_1.curve.EndMagnitudeOverride = 42.69f;
			c1_2.curve.StartMagnitudeOverride = 1234.56789f;

			return new List<CurveNode>() { n0, n1, n2 };
		}


		private int expectedEventCount;
		private int actualEventCount;
		private void AssertSerializationCorrect(List<CurveNode> expected, List<CurveNode> actual, bool fakeCustomData = true)
		{
			Assert.That.AreEqual(expected, actual, 0.00001f);

			if(! fakeCustomData) return;

			foreach (CurveNode node in expected)
			{
				node.NodeValuesChanged += Expected_NodeValuesChanged;
			}
			foreach (CurveNode node in actual)
			{
				node.NodeValuesChanged += Actual_NodeValuesChanged;
			}

			expectedEventCount = 0;
			actualEventCount = 0;

			var updateVec = new Vector3(42, 69, 96);

			for (int i = 0; i < expected.Count; i++)
			{
				expected[i].Position += updateVec;
				actual[i].Position   += updateVec;

				Assert.AreEqual(expectedEventCount, actualEventCount, "Did not receive the expected amount of events.");
			}

			for (int i = 0; i < expected.Count; i++)
			{
				var expectedData = expected[i].Data as Mock_NodeCustomData;
				var actualData = actual[i].Data as Mock_NodeCustomData;

				expectedData.TestNumber++;
				actualData.TestNumber++;

				Assert.AreEqual(expectedEventCount, actualEventCount, "Did not receive the expected amount of events.");
			}

			// Connection events //////////////////////////////////////////////
			var expectedConnections = expected[0].NetworkConnections(expected).ToArray();
			var actualConnections = actual[0].NetworkConnections(actual).ToArray();

			for (int i = 0; i < expectedConnections.Length; i++)
			{
				expectedConnections[i].Changed += Expected_ConnectionDataChanged;
				actualConnections[i].Changed += Actual_ConnectionDataChanged;
			}

			for (int i = 0; i < expectedConnections.Length; i++)
			{
				var expectedData = expectedConnections[i].data as Mock_ConnectionCustomData;
				var actualData = actualConnections[i].data as Mock_ConnectionCustomData;

				expectedData.TestNumber++;
				actualData.TestNumber++;

				Assert.AreEqual(expectedEventCount, actualEventCount, "Did not receive the expected amount of events.");
			}
		}

		private void Expected_NodeValuesChanged(CurveNode obj)
		{
			expectedEventCount++;
		}

		private void Actual_NodeValuesChanged(CurveNode obj)
		{
			actualEventCount++;
		}

		private void Expected_ConnectionDataChanged(Connection obj)
		{
			expectedEventCount++;
		}

		private void Actual_ConnectionDataChanged(Connection obj)
		{
			actualEventCount++;
		}


		#endregion TestHelpers



		[TestMethod]
		public void X_DataContract_RoundTrip()
		{
			var expected = TrivialSetup();

			using var stream = new MemoryStream();

			SerializationHelper.DContract.AddKnownTypes
				(typeof(List<CurveNode>), new[] { typeof(Mock_NodeCustomData), typeof(Mock_ConnectionCustomData) });

			SerializationHelper.DContract.SaveToStream(stream, expected);

			stream.Position = 0;

			var actual = SerializationHelper.DContract.LoadFromStream<List<CurveNode>>(stream);

			try
			{
				AssertSerializationCorrect(expected, actual);
			}
			catch (AssertFailedException e)
			{
				Assert.Inconclusive("This is broken due to DataContractSerializer: https://github.com/dotnet/runtime/issues/66788", e);
			}
		}

		[TestMethod]
		public void X_DataContract_RoundTrip_Mitigated()
		{
			var expected = TrivialSetup();

			using var stream = new MemoryStream();

			SerializationHelper.DContract.AddKnownTypes
				(typeof(List<CurveNode>), new[] { typeof(Mock_NodeCustomData), typeof(Mock_ConnectionCustomData) });

			SerializationHelper.DContract.SaveToStream(stream, expected);

			stream.Position = 0;

			var actual = SerializationHelper.DContract.LoadFromStream<List<CurveNode>>(stream);

			if (actual.Count != 0)
			{
				foreach (Connection connection in actual[0].NetworkConnections(actual))
				{
					connection.FixupAfterDeserialization(null);
				}
			}

			AssertSerializationCorrect(expected, actual);
		}

		[TestMethod]
		public void A_CustomSerializer_RoundTrip()
		{
			using var stream = new MemoryStream();
			try
			{
				var expected = TrivialSetup();

				var spec = SpecGenerator.CreateGraphSerializationSpecification(expected.GetType());

				spec.AddType(typeof(Mock_NodeCustomData));
				spec.AddType(typeof(Mock_ConnectionCustomData));

				spec.TypeSpecifications[typeof(CurveNode)].ByRef = true;
				spec.TypeSpecifications[typeof(Connection)].ByRef = true;

				

				Serializer.Serialize
					(
					 stream
				   , expected
				   , spec
				   , context
					);

				stream.Position = 0;

				var actual = Serializer.Deserialize<List<CurveNode>>
					(
					 stream
				   , spec
				   , context
					);

				AssertSerializationCorrect(expected, actual);
			}
			catch
			{
				stream.Position = 0;
				var reader = new StreamReader(stream);
				Console.WriteLine(reader.ReadToEnd());
				throw;
			}
		}

		[TestMethod]
		public void A_CustomSerializer_ExampleFile_RoundTrip()
		{
			Assert.Inconclusive("Test not implemented after breaking changes to CurveSpec.");
			using var original = File.OpenRead("Data/Curves.cs.xml");
			using var stream = new MemoryStream();
			try
			{
				var spec = SpecGenerator.CreateGraphSerializationSpecification(typeof(List<CurveNode>));

				spec.AddType(typeof(LuaIntegration.ConnectionCustomData));

				spec.TypeSpecifications[typeof(LuaIntegration.ConnectionCustomData)].SerializedName =
					"CurveGraph.ConnectionCustomData";

				spec.TypeSpecifications[typeof(CurveNode)].ByRef = true;
				spec.TypeSpecifications[typeof(Connection)].ByRef = true;


				var expected = Serializer.Deserialize<List<CurveNode>>(original, spec, context);

				Serializer.Serialize(stream, expected, spec, context);

				stream.Position = 0;

				var actual = Serializer.Deserialize<List<CurveNode>>(stream, spec, context);

				AssertSerializationCorrect(expected, actual, false);
			}
			catch
			{
				stream.Position = 0;
				var reader = new StreamReader(stream);
				Console.WriteLine(reader.ReadToEnd());
				throw;
			}
		}
	}
}
