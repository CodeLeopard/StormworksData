﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Text;

using CurveGraph;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using OpenToolkit.Mathematics;

namespace UTests.CurveGraph
{
	[TestClass]
	public class B_Replace_X_InConnection
	{

		[TestMethod]
		public void A0_ReplaceDestinationInConnection()
		{

			/* PreCondition
			 *    n2
			 *   /
			 * n1    n3
			 * PostCondition
			 *    n2
			 *      \
			 * n1    n3
			 */

			// Setup

			var n1 = new CurveNode(new Vector3(1));
			var n2 = new CurveNode(new Vector3(2));
			var n3 = new CurveNode(new Vector3(3));

			var c1 = n2.CreateConnection(n1);


			SharedPreCondition(c1, n2, n1, n3);

			n2.ReplaceDestinationInConnection(n1, n3);

			SharedPostCondition(c1, n2, n3, n1);
		}

		[TestMethod]
		public void B0_ReplaceThisInConnection()
		{

			/* PreCondition
			 *    n2
			 *   /
			 * n1    n3
			 * PostCondition
			 *    n2
			 *      \
			 * n1    n3
			 */

			// Setup

			var n1 = new CurveNode(new Vector3(1));
			var n2 = new CurveNode(new Vector3(2));
			var n3 = new CurveNode(new Vector3(3));

			var c1 = n2.CreateConnection(n1);



			SharedPreCondition(c1, n2, n1, n3);

			// Method under test ##############################################
			n1.ReplaceThisInConnection(n2, n3);

			SharedPostCondition(c1, n2, n3, n1);
		}

		private static void SharedPostCondition(Connection c1, CurveNode n2, CurveNode n3, CurveNode n1)
		{
			Assert.AreEqual(c1.StartNode, n2);
			Assert.AreEqual(c1.EndNode,   n3);


			Assert.AreEqual(n1.Connections.Count, 0);
			Assert.AreEqual(n2.Connections.Count, 1);
			Assert.AreEqual(n3.Connections.Count, 1);


			Assert.That.HasConnectionSettingsFor(c1, n2);
			Assert.That.HasConnectionSettingsFor(c1, n3);


			Assert.That.AreNotConnected(n2, n1);
			Assert.That.AreConnected(n2, n3);
		}

		private static void SharedPreCondition(Connection c1, CurveNode n2, CurveNode n1, CurveNode n3)
		{
			Assert.AreEqual(c1.StartNode, n2);
			Assert.AreEqual(c1.EndNode,   n1);

			Assert.AreEqual(n1.Connections.Count, 1);
			Assert.AreEqual(n2.Connections.Count, 1);
			Assert.AreEqual(n3.Connections.Count, 0);

			Assert.That.HasConnectionSettingsFor(c1, n1);
			Assert.That.HasConnectionSettingsFor(c1, n2);

			Assert.That.AreConnected(n2, n1);
			Assert.That.AreNotConnected(n2, n3);
		}
	}
}
