﻿using System;
using System.Collections.Generic;
using System.Net.WebSockets;
using System.Text;

using CurveGraph;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using OpenToolkit.Mathematics;

namespace UTests.CurveGraph
{
	[TestClass]
	public class M_Curvature
	{
		private void Test(Connection c, float dotThreshold)
		{
			var n1 = c.StartNode;

			Vector3 prev = n1.direction.Normalized();

			var samples = c.curve.GetSamples();
			int i = 0;
			foreach (CurveSample curveSample in samples)
			{
				Vector3 cur = curveSample.tangent;

				var dot = Vector3.Dot(prev, cur);

				if (dot < dotThreshold)
				{
					Assert.Fail($"tangent changed too abruptly. #{i}/{samples.Length} prev: {prev} cur: {cur} dot: {dot}");
				}

				prev = cur;
				i++;
			}
		}

		[TestMethod]
		public void A_Typical()
		{
			var n1 = new CurveNode(new Vector3(-100, 0, 0), new Vector3(50, -20, -20));
			var n2 = new CurveNode(new Vector3(100,  0, 0), new Vector3(50, 20, 20));
			var c = n1.CreateConnection(n2);

			const float dotThreshold = 0.999f;

			Test(c, dotThreshold);
		}

		[TestMethod]
		public void B_Long()
		{
			var n1 = new CurveNode(new Vector3(-1000, 0, 0), new Vector3(500, -200, -200));
			var n2 = new CurveNode(new Vector3(1000,  0, 0), new Vector3(500, 200,  200));
			var c = n1.CreateConnection(n2);

			const float dotThreshold = 0.999f;

			Test(c, dotThreshold);
		}

		[TestMethod]
		public void C_High_Velocity()
		{
			var n1 = new CurveNode(new Vector3(-10, 0, 0), new Vector3(5000, -2000, -2000));
			var n2 = new CurveNode(new Vector3(10,  0, 0), new Vector3(5000, 2000,  2000));
			var c = n1.CreateConnection(n2);

			const float dotThreshold = 0.99f;

			Test(c, dotThreshold);
		}
	}
}
