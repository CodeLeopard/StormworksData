﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Text;

using CurveGraph;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using OpenToolkit.Mathematics;

namespace UTests.CurveGraph
{
	[TestClass]
	public class G_Split
	{
		[TestMethod]
		public void A0_Split()
		{
			// Pre: n1 - n2 - n3
			// Pos: n1 - n4   n2 - n3

			var d = new Vector3(1, 0, 0);

			var n1 = new CurveNode(new Vector3(1, 0, 0), d);
			var n2 = new CurveNode(new Vector3(2, 0, 0), d);
			var n3 = new CurveNode(new Vector3(3, 0, 0), d);

			var c12 = n1.CreateConnection(n2);
			var c23 = n2.CreateConnection(n3);

			var n4 = n2.Split();

			n4.position = new Vector3(4f, 0, 0);

			var d14 = c12;
			var d23 = c23;

			Assert.AreEqual(1, n1.Connections.Count);
			Assert.AreEqual(1, n2.Connections.Count);
			Assert.AreEqual(1, n3.Connections.Count);
			Assert.AreEqual(1, n4.Connections.Count);

			Assert.That.AreConnected(n1, n4);
			Assert.That.AreConnected(n2, n3);

			Assert.That.AreNotConnected(n1, n2);
			Assert.That.AreNotConnected(n3, n4);

			Assert.That.HasConnectionSettingsFor(d14, n1);
			Assert.That.HasConnectionSettingsFor(d14, n4);
			Assert.That.HasConnectionSettingsFor(d23, n2);
			Assert.That.HasConnectionSettingsFor(d23, n3);
		}
	}
}
