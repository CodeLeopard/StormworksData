﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Text;

using CurveGraph;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using OpenToolkit.Mathematics;

namespace UTests.CurveGraph
{
	[TestClass]
	public class D_Absorb
	{
		[TestMethod]
		public void A0_Absorb()
		{
			/* Precondition
			 * n1 - n2     
			 *             
			 *      n3 - n4
			 * PostCondition
			 * n1 - n2 - n4
			 *             
			 *      n3     
			 */

			var n1 = new CurveNode(new Vector3(1));
			var n2 = new CurveNode(new Vector3(2));

			//n1.AutoSmooth = true;
			//n2.AutoSmooth = true;

			var c1 = n1.CreateConnection(n2);

			var n3 = new CurveNode(new Vector3(3f));
			var n4 = new CurveNode(new Vector3(4));

			//n3.AutoSmooth = true;
			//n4.AutoSmooth = true;

			var c2 = n3.CreateConnection(n4);

			n2.Absorb(n3);

			Assert.IsTrue(n3.Connections.Count == 0, "After Absorb(), the absorbed node should have no connections.");
			Assert.IsTrue(n2.Connections.Count == 2, "After Absorb(), the absorbing node should have 2 connections.");

			Assert.That.HasConnectionSettingsFor(c1, n1);
			Assert.That.HasConnectionSettingsFor(c1, n2);

			Assert.That.HasConnectionSettingsFor(c2, n2);
			Assert.That.HasConnectionSettingsFor(c2, n4);

			Assert.That.AreConnected(n1, n2);
			Assert.That.AreConnected(n2, n4);

			Assert.That.AreNotConnected(n3, n4);

			//n3.Position = new Vector3(2.2f);
			//n2.Position = new Vector3(2.4f);
		}
	}
}
