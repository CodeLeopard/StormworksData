﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using CurveGraph;

using Vector2 = OpenToolkit.Mathematics.Vector2;
using Vector3 = OpenToolkit.Mathematics.Vector3;
using Vector4 = System.Numerics.Vector4;

namespace UTests.CurveGraph
{
	internal static class Assertions
	{
		public static void AreConnected(this Assert instance, CurveNode a, CurveNode b)
		{
			bool ab = a.IsConnectedTo(b);
			bool ba = b.IsConnectedTo(a);

			if (! ab || !ba)
			{
				var message = $"Expected a connection CurveNodes: {a} and {b}.";

				if (! ab && ! ba)
				{
					message += $"\nBut both claim to not be connected to each other.";
				}
				else if (! ab)
				{
					message += $"\nBut {nameof(a)} claims to not be connected to {nameof(b)}.";
				}
				else if (! ba)
				{
					message += $"\nBut {nameof(b)} claims to not be connected to {nameof(a)}.";
				}

				Assert.Fail(message);
			}
		}

		public static void AreNotConnected(this Assert instance, CurveNode a, CurveNode b)
		{
			bool ab = a.IsConnectedTo(b);
			bool ba = b.IsConnectedTo(a);

			if (ab || ba)
			{
				var message = $"Expected no connection between CurveNodes: {a} and {b}.";
				if (ab && ba)
				{
					message += $"\nBut both claim to be connected to each other.";
				}
				else if (ab)
				{
					message += $"\nBut {nameof(a)} claims to be connected to {nameof(b)}.";
				}
				else if (ba)
				{
					message += $"\nBut {nameof(b)} claims to be connected to {nameof(a)}.";
				}

				if (a.TryGetConnection(b, out var cab))
				{
					message += $"\nAlso found: {cab}.";
				}

				if (b.TryGetConnection(a, out var cba))
				{
					if (cab == null)
					{
						message += $"\nAlso found: {cba}.";
					}
					else if (cab != cba)
					{
						message += $"\nAnd also found: {cba}.";
					}
				}

				Assert.Fail(message);
			}
		}

		public static void HasConnectionSettingsFor
			(this Assert instance, Connection connection, CurveNode node)
		{
			if (! connection.TryGetNodeSettings(node, out _))
			{
				var message = $"Expected connection {connection} to have settings for {node}.";
				Assert.Fail(message);
			}
		}

		private static string FormatVectorFailMessage
			(object expected, object actual, object delta, float margin, string message = null)
		{
			return message == null
				? $"Assert.AreEqual failed. Expected a difference no greater than <{margin}> between the components of the expected <{expected}> and actual <{actual}>. Delta <{delta}>."
				: $"Assert.AreEqual failed for {message}. Expected a difference no greater than <{margin}> between the components of the expected <{expected}> and actual <{actual}>. Delta <{delta}>.";
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="instance"></param>
		/// <param name="expected">Expected value</param>
		/// <param name="actual">Actual value</param>
		/// <param name="margin">margin</param>
		public static void AreEqual(this Assert instance, Vector2 expected, Vector2 actual, float margin = 0f, string message = null)
		{
			var delta = expected - actual;

			if (Abs(delta.X) < margin && Abs(delta.Y) < margin)
			{
				return;
			}

			throw new AssertFailedException(FormatVectorFailMessage(expected, actual, delta, margin, message));
		}
		private static float Abs(float f) => Math.Abs(f);


		/// <summary>
		/// 
		/// </summary>
		/// <param name="instance"></param>
		/// <param name="expected">Expected value</param>
		/// <param name="actual">Actual value</param>
		/// <param name="margin">margin</param>
		public static void AreEqual
			(this Assert instance, Vector3 expected, Vector3 actual, float margin = 0f, string message = null)
		{
			var delta = expected - actual;

			if (Abs(delta.X) < margin && Abs(delta.Y) < margin && Abs(delta.Z) < margin)
			{
				return;
			}

			throw new AssertFailedException(FormatVectorFailMessage(expected, actual, delta, margin, message));
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="instance"></param>
		/// <param name="expected">Expected value</param>
		/// <param name="actual">Actual value</param>
		/// <param name="margin">margin</param>
		public static void AreEqual
			(this Assert instance, Vector4 expected, Vector4 actual, float margin = 0f, string message = null)
		{
			var delta = expected - actual;

			if (Abs(delta.X) < margin && Abs(delta.Y) < margin && Abs(delta.Z) < margin && Abs(delta.W) < margin)
			{
				return;
			}

			throw new AssertFailedException(FormatVectorFailMessage(expected, actual, delta, margin, message));
		}

		public static void AreEqual(this Assert instance, CurveNode expected, CurveNode actual, float margin = 0f)
		{
			Assert.That.AreEqual(expected.Position,   actual.Position, margin);
			Assert.That.AreEqual(expected.Direction,  actual.Direction, margin);
			Assert.That.AreEqual(expected.Up,         actual.Up, margin);
			Assert.That.AreEqual(expected.Scale,      actual.Scale, margin);
			Assert.AreEqual(expected.Roll,       actual.Roll, margin);
			Assert.AreEqual(expected.AutoSmooth, actual.AutoSmooth);
			Assert.AreEqual(expected.Curvature,  actual.Curvature, margin);

			Assert.AreEqual(expected.Data, actual.Data);

			Assert.AreEqual(expected.Connections.Count, actual.Connections.Count);
		}

		public static void AreEqual
			(this Assert instance, float? expected, float? actual, float margin)
		{
			Assert.AreEqual(expected.HasValue, actual.HasValue);

			if (expected.HasValue)
			{
				Assert.AreEqual(expected.Value, actual.Value, margin);
			}
		}

		public static void AreEqual(this Assert instance, List<CurveNode> expected, List<CurveNode> actual, float margin)
		{
			if (expected.Count != actual.Count) Assert.Fail("Resulting list is not the same length.");

			// Check the fields and properties.
			for (int i = 0; i < expected.Count; i++)
			{
				CurveNode na = expected[i];
				CurveNode nb = actual[i];

				Assert.AreNotSame(na, nb, "Expected to get a different instance after serialization round trip.");

				Assert.That.AreEqual(na, nb, margin);

				foreach (Connection connection in na.Connections.Values)
				{
					int startIndex = expected.IndexOf(connection.StartNode);
					int endIndex = expected.IndexOf(connection.EndNode);

					Assert.That.AreConnected(actual[startIndex], actual[endIndex]);

					Assert.IsTrue(actual[startIndex].TryGetConnection(actual[endIndex], out Connection actualConnection), "A connection should exist between these nodes.");

					Assert.That.AreEqual(connection, actualConnection, margin);
				}
			}

			// Check connections for duplicates:
			int expectedCounter = 0;
			int actualCounter = 0;
			foreach (var conn in expected[0].NetworkConnections(expected))
			{
				expectedCounter++;
			}

			foreach (var conn in actual[0].NetworkConnections(actual))
			{
				actualCounter++;
			}

			Assert.AreEqual(expectedCounter, actualCounter);
		}

		public static void AreEqual(this Assert instance, Connection expected, Connection actual, float margin = 0f)
		{
			Assert.That.AreEqual(expected.StartNode,  actual.StartNode,  margin);
			Assert.That.AreEqual(expected.EndNode, actual.EndNode, margin);
			Assert.That.AreEqual(expected.startNodeSettings,        actual.startNodeSettings,        margin);
			Assert.That.AreEqual(expected.endNodeSettings,     actual.endNodeSettings,     margin);
			Assert.AreEqual(expected.data,       actual.data);

			// Curve
			Assert.That.AreEqual(expected.curve.StartMagnitudeOverride, actual.curve.StartMagnitudeOverride, margin);
			Assert.That.AreEqual(expected.curve.EndMagnitudeOverride,   actual.curve.EndMagnitudeOverride,   margin);

			Assert.AreEqual(expected.curve.Length, actual.curve.Length, margin);
		}

		public static void AreEqual(this Assert instance, ConnectionNodeSettings expected, ConnectionNodeSettings actual, float margin = 0f)
		{
			Assert.AreEqual(expected.SmoothingWeight,   actual.SmoothingWeight,   margin);
		}

		/// <summary>
		/// Checks that the curve <paramref name="truth"/> is the same as <paramref name="part1"/> and <paramref name="part2"/> concatenated together.
		/// </summary>
		/// <param name="instance">Assert instance, so you can pretend this method lives on <see cref="Assert.That"/>.</param>
		/// <param name="truth">Truth Curve.</param>
		/// <param name="part1">Curve part 1.</param>
		/// <param name="part2">Curve part 2.</param>
		/// <param name="locationMargin">Margin for floating point precision.</param>
		public static void IsSplitCorrectly
		(
			this Assert instance
		  , Connection  truth
		  , Connection  part1
		  , Connection  part2
		,  float lengthMargin
		  , float locationMargin
		  , float tangentMargin
		  , int   steps = 100
		) =>
			IsSplitCorrectly(instance, truth.curve, part1.curve, part2.curve, lengthMargin, locationMargin, tangentMargin, steps);

		/// <summary>
		/// Checks that the curve <paramref name="truth"/> is the same as <paramref name="part1"/> and <paramref name="part2"/> concatenated together.
		/// </summary>
		/// <param name="instance">Assert instance, so you can pretend this method lives on <see cref="Assert.That"/>.</param>
		/// <param name="truth">Truth Curve.</param>
		/// <param name="part1">Curve part 1.</param>
		/// <param name="part2">Curve part 2.</param>
		/// <param name="locationMargin">Margin for floating point precision.</param>
		/// <param name="steps">The number of samples checked.</param>
		public static void IsSplitCorrectly(
			this Assert      instance,
			CubicBezierCurve truth,
			CubicBezierCurve part1,
			CubicBezierCurve part2,
			float            lengthMargin,
			float            locationMargin,
			float            tangentMargin,
			int steps = 100)
		{
			float trueLength = truth.Length;
			float splitLength = part1.Length + part2.Length;

			bool lengthCorrect = Math.Abs(trueLength - splitLength) < locationMargin;
			int locationWrongCount = 0;
			int tangentWrongCount = 0;

			float biggestLocationDiff = 0;
			float biggestTangentDiff = 0;

			float step = trueLength / (float)steps;

			CurveSample GetPartSampleAtDistance(float d)
			{
				float half = part1.Length;
				if (d <= half)
				{
					return part1.GetSampleAtDistance(d);
				}

				float adjustedDistance = Math.Clamp(d - half, 0, part2.Length);
				return part2.GetSampleAtDistance(adjustedDistance);
			}

			CurveSample GetPartSampleAtTime(float t)
			{
				const float half = 0.5f;
				if (t <= half)
				{
					return part1.GetSample(t);
				}

				float adjustedTime = Math.Clamp(t - half, 0, 1);
				return part2.GetSample(adjustedTime);
			}

			for (int i = 0; i < steps; i++)
			{
				float t;
				float d;

				CurveSample trueSample;
				CurveSample testSample;
				if (false)
				{
					t = (float)i / steps;
					d = (float)i * step;

					trueSample = truth.GetSample(t);
					testSample = GetPartSampleAtTime(d);
				}
				else
				{
					t = (float)i / steps;
					d = (float)i * step;

					trueSample = truth.GetSampleAtDistance(d);
					testSample = GetPartSampleAtDistance(d);
				}

				bool headerWritten = false;

				void MaybeWriteHeader()
				{
					if (headerWritten) return;
					headerWritten = true;
					Console.WriteLine($"#{i} Time: {trueSample.timeInCurve:0.0000}, Distance: {d:0.0000}");
				}

				string V(Vector3 v)
				{
					const int alignment = 7;
					return $"({v.X,alignment:0.0000}, {v.Y,alignment:0.0000}, {v.Z,alignment:0.0000})";
				}

				void CheckVectors(string name, Vector3 expected, Vector3 actual, float margin, ref int counter, ref float biggest, string extraMessage = null)
				{
					Vector3 delta = expected - actual;
					float length = delta.Length;
					if (length <= margin) return;

					biggest = Math.Max(biggest, length);

					counter++;

					MaybeWriteHeader();
					Console.WriteLine($"{name}: {V(expected)} - {V(actual)} = {V(delta)} [{length:0.0000} > {margin:0.0000}].{extraMessage}");
				}

				string extraMessage;
				{
					var ld = trueSample.location - testSample.location;
					var ldn = ld.Normalized();
					var dot = Vector3.Dot(ldn, trueSample.tangent);
					var angle = Math.Acos(dot) * 180/Math.PI;
					extraMessage = $" Angle: {angle:0.0000}°";
				}

				CheckVectors("Location", trueSample.location, testSample.location, locationMargin, ref locationWrongCount, ref biggestLocationDiff, extraMessage);
				CheckVectors("Tangent ",  trueSample.tangent,  testSample.tangent, tangentMargin, ref tangentWrongCount, ref biggestTangentDiff);
			}

			if (! lengthCorrect || locationWrongCount > 0 || tangentWrongCount > 0)
			{
				string message =
					$"Validation failed: split sections do not match original curve. Checked {steps} samples. Tolerances: Length: {lengthMargin:0.0000}, Location: {locationMargin:0.0000}, Tangent: {tangentMargin:0.0000}.";

				if (! lengthCorrect)
				{
					message +=
						$"\nLength: ({trueLength:0.0000} - {splitLength:0.0000}) = {trueLength - splitLength:0.0000} > {locationMargin:0.0000}";
				}

				if (locationWrongCount > 0)
				{
					message += $"\nLargest position offset: {biggestLocationDiff:0.0000}";
				}

				if (tangentWrongCount > 0)
				{
					message += $"\nLargest tangent offset: {biggestTangentDiff:0.0000}";
				}

				Assert.Fail(message);
			}
		}
	}
}
