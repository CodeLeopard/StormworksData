﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Text;

using CurveGraph;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using OpenToolkit.Mathematics;

using Shared.Exceptions;

namespace UTests.CurveGraph
{
	[TestClass]
	public class J_RejectInvalid
	{
		private (CurveNode, CurveNode, Connection) TrivialSetup()
		{
			var n0 = new CurveNode(new Vector3(0, 0, 0), new Vector3(1,  0, 0));
			var n1 = new CurveNode(new Vector3(1, 0, 0), new Vector3(-1, 0, 0));

			n0.AutoSmooth = false;
			n1.AutoSmooth = false;

			var c0_1 = n0.CreateConnection(n1);

			return (n0, n1, c0_1);
		}



		[TestMethod]
		[ExpectedException(typeof(InvalidValueException), "Should throw for invalid value.")]
		public void J0_Position()
		{
			var (n0, n1, c0_1) = TrivialSetup();

			n0.position = Vector3.PositiveInfinity;
		}

		[TestMethod]
		public void J0_PositionZero()
		{
			var (n0, n1, c0_1) = TrivialSetup();

			n0.position = Vector3.Zero;
		}

		[TestMethod]
		[ExpectedException(typeof(InvalidValueException), "Should throw for invalid value.")]
		public void J1_Direction()
		{
			var (n0, n1, c0_1) = TrivialSetup();

			n0.direction = Vector3.PositiveInfinity;
		}

		[TestMethod]
		[ExpectedException(typeof(InvalidValueException), "Should throw for invalid value.")]
		public void J1_DirectionZero()
		{
			var (n0, n1, c0_1) = TrivialSetup();

			n0.direction = Vector3.Zero;
		}

		[TestMethod]
		[ExpectedException(typeof(InvalidValueException), "Should throw for invalid value.")]
		public void J3_Scale()
		{
			var (n0, n1, c0_1) = TrivialSetup();

			n0.scale = Vector2.PositiveInfinity;
		}

		[TestMethod]
		[ExpectedException(typeof(InvalidValueException), "Should throw for invalid value.")]
		public void J3_ScaleZero()
		{
			var (n0, n1, c0_1) = TrivialSetup();

			n0.scale = Vector2.Zero;
		}


		[TestMethod]
		[ExpectedException(typeof(InvalidValueException), "Should throw for invalid value.")]
		public void J4_Roll()
		{
			var (n0, n1, c0_1) = TrivialSetup();

			n0.roll = float.PositiveInfinity;
		}


		[TestMethod]
		public void J4_RollZero()
		{
			var (n0, n1, c0_1) = TrivialSetup();

			n0.roll = 0;
		}

		[TestMethod]
		[ExpectedException(typeof(InvalidValueException), "Should throw for invalid value.")]
		public void J5_Curvature()
		{
			var (n0, n1, c0_1) = TrivialSetup();

			n0.curvature = float.PositiveInfinity;
		}


		[TestMethod]
		[ExpectedException(typeof(InvalidValueException), "Should throw for invalid value.")]
		public void J5_CurvatureZero()
		{
			var (n0, n1, c0_1) = TrivialSetup();

			n0.curvature = 0;
		}

		[TestMethod]
		[ExpectedException(typeof(InvalidValueException), "Should throw for invalid value.")]
		public void J5_CurvatureMinimum()
		{
			var (n0, n1, c0_1) = TrivialSetup();

			n0.curvature = CurveNode.curvatureMinimum - 1;
		}

		[TestMethod]
		[ExpectedException(typeof(InvalidValueException), "Should throw for invalid value.")]
		public void J5_CurvatureMaximum()
		{
			var (n0, n1, c0_1) = TrivialSetup();

			n0.curvature = CurveNode.curvatureMaximum + 1;
		}

		[TestMethod]
		public void J5_CurvatureValid()
		{
			var (n0, n1, c0_1) = TrivialSetup();

			n0.curvature = (CurveNode.curvatureMaximum + CurveNode.curvatureMinimum) / 2f;
		}


	}
}
