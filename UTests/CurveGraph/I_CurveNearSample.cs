﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Text;

using CurveGraph;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using OpenToolkit.Mathematics;

namespace UTests.CurveGraph
{
	[TestClass]
	public class I_CurveNearSample
	{
		private (CurveNode, CurveNode, Connection) TrivialSetup()
		{
			var n0 = new CurveNode(new Vector3(0, 0, 0), new Vector3(1,  0, 0));
			var n1 = new CurveNode(new Vector3(1, 0, 0), new Vector3(-1, 0, 0));

			n0.AutoSmooth = false;
			n1.AutoSmooth = false;

			var c0_1 = n0.CreateConnection(n1);

			return (n0, n1, c0_1);
		}



		[TestMethod]
		public void I0_Trivial()
		{
			var (n0, n1, c0_1) = TrivialSetup();

			Vector3 expected = new Vector3(0.5f, 0, 0);
			CurveSample result = c0_1.curve.GetNearSample(expected);

			Assert.That.AreEqual(expected, result.location, 0.01f);
		}

		[TestMethod]
		public void I1_AtEnd()
		{
			var (n0, n1, c0_1) = TrivialSetup();

			Vector3 target = new Vector3(-0.5f, 0, 0);
			Vector3 expected = new Vector3(0,   0, 0);

			CurveSample result = c0_1.curve.GetNearSample(target);

			Assert.That.AreEqual(expected, result.location, 0.01f);
		}

		[TestMethod]
		[ExpectedException(typeof(ArgumentException), "Should throw for infinite target.")]
		public void I2_InvalidInput_Infinity()
		{
			var (n0, n1, c0_1) = TrivialSetup();

			c0_1.curve.GetNearSample(new Vector3(float.PositiveInfinity));
		}

		[TestMethod]
		[ExpectedException(typeof(ArgumentException), "Should throw for infinite target.")]
		public void I2_InvalidInput_InfinityNegative()
		{
			var (n0, n1, c0_1) = TrivialSetup();

			c0_1.curve.GetNearSample(new Vector3(float.NegativeInfinity));
		}

		[TestMethod]
		[ExpectedException(typeof(ArgumentException), "Should throw for NaN target.")]
		public void I2_InvalidInput_NaN()
		{
			var (n0, n1, c0_1) = TrivialSetup();

			c0_1.curve.GetNearSample(new Vector3(float.NaN));
		}
	}
}
