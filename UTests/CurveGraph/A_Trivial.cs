// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.ComponentModel.Design.Serialization;
using System.Security.Cryptography.X509Certificates;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using OpenToolkit.Mathematics;

using CurveGraph;

namespace UTests.CurveGraph
{
	[TestClass]
	public class A_Trivial
	{
		[TestMethod]
		public void A0_Trivial()
		{
			var n1 = new CurveNode();
			var n2 = new CurveNode(new Vector3(0,0,1));

			var c1 = n1.CreateConnection(n2);

			Assert.AreEqual(c1.StartNode, n1);
			Assert.AreEqual(c1.EndNode, n2);

			Assert.That.HasConnectionSettingsFor(c1, n1);
			Assert.That.HasConnectionSettingsFor(c1, n2);

			Assert.That.AreConnected(n1, n2);
		}

		[TestMethod]
		public void A1_Trivial()
		{
			var n1 = new CurveNode();
			var n2 = new CurveNode();
			var n3 = new CurveNode();

			var c1 = n1.CreateConnection(n2);
			var c2 = n2.CreateConnection(n3);

			Assert.AreEqual(c1.StartNode, n1);
			Assert.AreEqual(c1.EndNode, n2);

			Assert.AreEqual(c2.StartNode, n2);
			Assert.AreEqual(c2.EndNode, n3);

			Assert.That.HasConnectionSettingsFor(c1, n1);
			Assert.That.HasConnectionSettingsFor(c1, n2);

			Assert.That.HasConnectionSettingsFor(c2, n2);
			Assert.That.HasConnectionSettingsFor(c2, n3);

			Assert.That.AreConnected(n1, n2);
			Assert.That.AreConnected(n2, n3);
		}


		[TestMethod]
		public void B0_IsConnectedTo()
		{
			var n1 = new CurveNode();
			var n2 = new CurveNode();
			var n3 = new CurveNode();

			Assert.That.AreNotConnected(n1, n2);
			Assert.That.AreNotConnected(n2, n3);
			Assert.That.AreNotConnected(n3, n1);

			var c12 = n1.CreateConnection(n2);
			var c23 = n2.CreateConnection(n3);
			var c31 = n3.CreateConnection(n1);

			Assert.That.AreConnected(n1, n2);
			Assert.That.AreConnected(n2, n3);
			Assert.That.AreConnected(n3, n1);
		}


		[TestMethod]
		public void C0_GetConnectionSettings()
		{
			var n1 = new CurveNode();
			var n2 = new CurveNode();
			var n3 = new CurveNode();

			var c12 = n1.CreateConnection(n2);
			var c23 = n2.CreateConnection(n3);
			var c31 = n3.CreateConnection(n1);

			Assert.That.HasConnectionSettingsFor(c12, n1);
			Assert.That.HasConnectionSettingsFor(c12, n2);

			Assert.That.HasConnectionSettingsFor(c23, n2);
			Assert.That.HasConnectionSettingsFor(c23, n3);

			Assert.That.HasConnectionSettingsFor(c31, n3);
			Assert.That.HasConnectionSettingsFor(c31, n1);
		}


		[TestMethod]
		public void D0_CurveSampleSanity()
		{
			var n1 = new CurveNode();
			var n2 = new CurveNode(new Vector3(0, 0, 1));

			var c1 = n1.CreateConnection(n2);

			var samples = c1.curve.GetSamples();
			CurveSample previous = samples[0];
			for (int i = 1; i < samples.Length; i++)
			{
				var sample = samples[i];
				Assert.AreNotEqual(previous, sample, $"Index {i}");
				previous = sample;
			}
		}

		[TestMethod]
		public void E0_CurveGraphEnumeration_Nodes()
		{
			var n1 = new CurveNode();
			var n2 = new CurveNode();
			var n3 = new CurveNode();

			var c1_2 = n1.CreateConnection(n2);
			var c2_3 = n2.CreateConnection(n3);
			var c3_1 = n3.CreateConnection(n1);

			const int expectedCount = 3;
			int actualCount = 0;

			foreach (var node in n1.NetworkNodes())
			{
				actualCount++;
				// todo: check something
			}

			Assert.AreEqual(expectedCount, actualCount, "Did not receive the expected number of nodes.");
		}

		[TestMethod]
		public void E1_CurveGraphEnumeration_Connections()
		{
			var n1 = new CurveNode();
			var n2 = new CurveNode();
			var n3 = new CurveNode();

			var c1_2 = n1.CreateConnection(n2);
			var c2_3 = n2.CreateConnection(n3);
			var c3_1 = n3.CreateConnection(n1);

			const int expectedCount = 3;
			int actualCount = 0;

			foreach (var connection in n1.NetworkConnections())
			{
				actualCount++;

				// todo: check something
			}
			Assert.AreEqual(expectedCount, actualCount, "Did not receive the expected number of connections.");
		}

		[TestMethod]
		public void E2_CurveGraphEnumeration_Both()
		{
			var n1 = new CurveNode();
			var n2 = new CurveNode();
			var n3 = new CurveNode();

			var c1_2 = n1.CreateConnection(n2);
			var c2_3 = n2.CreateConnection(n3);
			var c3_1 = n3.CreateConnection(n1);

			const int expectedCount = 3 + 3;
			int actualCount = 0;

			foreach (var netObject in n1.NetworkNodesAndConnections())
			{
				actualCount++;
				// todo: check something
			}
			Assert.AreEqual(expectedCount, actualCount, "Did not receive the expected number of NetworkObjects.");
		}
	}
}
