﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Text;

using CurveGraph;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UTests.CurveGraph
{
	[TestClass]
	public class E_RemoveConnection
	{
		[TestMethod]
		public void A0_RemoveConnectionFromNode()
		{
			// Pre  n2 - n1 - n3
			// Post n2   n1 - n3
			var n1 = new CurveNode();
			var n2 = new CurveNode();
			var n3 = new CurveNode();

			var c12 = n1.CreateConnection(n2);
			var c13 = n1.CreateConnection(n3);

			var cr = n1.RemoveConnection(n2);

			Assert.AreEqual(c12, cr);

			Assert.IsTrue(c12.IsRemoved);
			Assert.IsFalse(c13.IsRemoved);

			Assert.AreEqual(n1.Connections.Count, 1);
			Assert.AreEqual(n2.Connections.Count, 0);
			Assert.AreEqual(n3.Connections.Count, 1);
		}

		[TestMethod]
		public void B0_RemoveConnectionInstance()
		{
			// Pre  n2 - n1 - n3
			// Post n2   n1 - n3
			var n1 = new CurveNode();
			var n2 = new CurveNode();
			var n3 = new CurveNode();

			var c12 = n1.CreateConnection(n2);
			var c13 = n1.CreateConnection(n3);

			c12.Remove();

			Assert.IsTrue(c12.IsRemoved);
			Assert.IsFalse(c13.IsRemoved);

			Assert.AreEqual(n1.Connections.Count, 1);
			Assert.AreEqual(n2.Connections.Count, 0);
			Assert.AreEqual(n3.Connections.Count, 1);
		}
	}
}
