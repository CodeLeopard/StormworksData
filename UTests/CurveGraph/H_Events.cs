﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

using CurveGraph;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UTests.CurveGraph
{
	[TestClass]
	public class H_Events
	{
		private CurveNode n1;
		private CurveNode n2;
		private CurveNode n3;
		private CurveNode n4;
		private CurveNode n5;

		[TestCleanup]
		public void Cleanup()
		{
			var fieldInfo = typeof(CurveNode).GetField
				(nameof(CurveNode.ConnectionsChanged), BindingFlags.NonPublic | BindingFlags.Instance);

			if(fieldInfo == null) Assert.Inconclusive("Unable to perform proper testCleanup");


			foreach (var node in new[] { n1, n2, n3, n4, n5 })
			{
				if(node == null) continue;

				fieldInfo.SetValue(node, null);
			}
		}

		private int A0_EventCount;
		[TestMethod]
		public void A0_RemoveSingle()
		{
			// Pre  n2 - n1 - n3
			// Post n2   n1 - n3
			n1 = new CurveNode();
			n2 = new CurveNode();
			n3 = new CurveNode();


			var c12 = n1.CreateConnection(n2);
			var c13 = n1.CreateConnection(n3);

			const int expectedCount = 2;
			A0_EventCount = 0;

			n1.ConnectionsChanged += A0_Event_NodeConnectionsChanged;
			n2.ConnectionsChanged += A0_Event_NodeConnectionsChanged;
			n3.ConnectionsChanged += A0_Event_NodeConnectionsChanged;

			var cr = n1.RemoveConnection(n2);

			Assert.AreEqual(c12, cr);

			Assert.AreEqual(expectedCount, A0_EventCount, "Did not receive the expected number of events.");
		}

		private void A0_Event_NodeConnectionsChanged(CurveNode.ConnectionsChangedEventArgs args)
		{
			var node = args.CurveNode;
			var flags = args.EventFlags;
			if (node == n3)
			{
				Assert.Fail($"Received event for unaffected node {node}.");
			}
			else if (node == n1 || node == n2)
			{
				Assert.IsNull(args.AddedConnections);
				Assert.IsNull(args.MovedConnections);
				Assert.IsNotNull(args.RemovedConnections);

				Assert.IsTrue(flags == CurveNode.EventFlags.Remove);

				(CurveNode destination, Connection connection) = args.RemovedConnections[0];

				Assert.IsTrue(connection.IsRemoved);

				if (node == n1)
				{
					Assert.AreEqual(n2, destination);
				}
				else if (node == n2)
				{
					Assert.AreEqual(n1, destination);
				}

				A0_EventCount++;
			}
			else
			{
				Assert.Fail($"Got event for unexpected node {args}.");
			}
		}

		private int B0_EventCount;
		[TestMethod]
		public void B0_Clear()
		{

			// Pre  n1 - n2 - n3 - n4 - n5
			// Post n1 - n2   n3   n4 - n5
			n1 = new CurveNode();
			n2 = new CurveNode();
			n3 = new CurveNode();
			n4 = new CurveNode();
			n5 = new CurveNode();



			var c12 = n1.CreateConnection(n2);
			var c23 = n2.CreateConnection(n3);
			var c34 = n3.CreateConnection(n4);
			var c45 = n4.CreateConnection(n5);

			const int ExpectedCount = 3;
			B0_EventCount = 0;

			n1.ConnectionsChanged += B0_Event_NodeConnectionsChanged;
			n2.ConnectionsChanged += B0_Event_NodeConnectionsChanged;
			n3.ConnectionsChanged += B0_Event_NodeConnectionsChanged;
			n4.ConnectionsChanged += B0_Event_NodeConnectionsChanged;
			n5.ConnectionsChanged += B0_Event_NodeConnectionsChanged;

			n3.ClearConnections();

			Assert.AreEqual(n3.Connections.Count, 0);

			Assert.AreEqual(ExpectedCount, B0_EventCount, "Did not receive the expected number of events.");
		}

		private void B0_Event_NodeConnectionsChanged(CurveNode.ConnectionsChangedEventArgs args)
		{
			var node = args.CurveNode;
			var flags = args.EventFlags;

			var unaffectedNodes = new[] { n1, n5 };
			var affectedNodes = new[] { n2, n3, n4 };


			if (unaffectedNodes.Contains(node))
			{
				Assert.Fail($"Received event for unaffected node {node}.");
				return;
			}
			if (! affectedNodes.Contains(node))
			{
				Assert.Fail($"Got event for unexpected node {args}.");
				return;
			}


			Assert.IsNull(args.AddedConnections);
			Assert.IsNull(args.MovedConnections);
			Assert.IsNotNull(args.RemovedConnections);

			if (node == n3)
			{
				var removed = args.RemovedConnections;

				Assert.IsTrue(flags == CurveNode.EventFlags.Clear);

				Assert.AreEqual(removed.Count, 2);

				foreach((CurveNode destination, Connection connection) in removed)
				{
					Assert.IsTrue(affectedNodes.Contains(destination));

					Assert.IsTrue(connection.IsRemoved);
				}
			}
			else if (node == n2 || node == n4)
			{
				Assert.IsTrue(flags == CurveNode.EventFlags.Remove);

				(CurveNode destination, Connection connection) = args.RemovedConnections[0];

				Assert.IsTrue(connection.IsRemoved);

				if (node == n2)
				{
					Assert.AreEqual(n3, destination);
				}
				else if (node == n4)
				{
					Assert.AreEqual(n3, destination);
				}
			}

			B0_EventCount++;
		}
	}
}
