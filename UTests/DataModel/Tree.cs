﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Shared;
using Shared.Exceptions;
using Shared.Serialization;

using Tiles_Tree = DataModel.Tiles.Tree;

namespace UTests.DataModel
{
	[TestClass]
	public class Tree
	{
		[TestMethod]
		public void LoadAll()
		{
			Helpers.InconclusiveWithoutStormworksInstall();
			var filePathList = Directory.EnumerateFiles(StormworksPaths.Data.tiles, "*.xml");
			foreach (string filePath in filePathList)
			{
				try
				{
					var fileName = Path.GetFileNameWithoutExtension(filePath);
					if (! fileName.EndsWith("_instances")) continue;

					Console.Write($"Reading trees: {fileName.PadLeft(50)}");

					var originalDocument = XMLHelper.LoadFromFile(filePath);

					var trees = Tiles_Tree.ParseTrees(originalDocument);

					Console.Write($" -> {trees.Count,6} trees.");

					// Todo: somehow do a writeBack test that properly ignores double -> float transition.

					Console.WriteLine();
				}
				catch (Exception e)
				{
					throw new FileInteractionException(filePath, e);
				}
			}
		}
	}
}
