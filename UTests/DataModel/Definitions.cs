﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Text;

using DataModel.Definitions;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Shared;

namespace UTests.DataModel
{
	[TestClass]
	public class Definitions
	{
		[TestMethod]
		public void LoadAll()
		{
			Helpers.InconclusiveWithoutStormworksInstall();
			// todo: The definitions being private static is bad.
			// Ideally it's stored in an instance that can be deleted/reset and inspected.
			// Also would be nice to have predictable is loaded or not
			Definition.ParseDefinitions(StormworksPaths.Data.definitions);
		}
	}
}
