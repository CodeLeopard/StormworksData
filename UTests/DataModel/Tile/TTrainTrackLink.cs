﻿using DataModel.Tiles;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenToolkit.Mathematics;

namespace UTests.DataModel.Tile
{
	[TestClass]
	public class TTrainTrackLink
	{
		[TestMethod]
		public void ExtraPropertiesCopy()
		{
			var n1 = new TrainTrackNode("foo", new Vector3(1, 2, 3));
			var n2 = new TrainTrackNode("bar", new Vector3(3, 2, 1));
			var l1 = n1.AddLink(n2).ForwardLink;
			l1.TrackSpeed_ms = 20;
			l1.SuperElevation_mm = 20;
			l1.TrackName = "TrackName";
			l1.CatenarySpec = "CatenarySpec";
			var l1a = l1.ReverseLink;

			Assert.AreNotEqual(l1.TrackSpeed_ms,     l1a.TrackSpeed_ms);
			Assert.AreNotEqual(l1.SuperElevation_mm, l1a.SuperElevation_mm);
			Assert.AreNotEqual(l1.TrackName,         l1a.TrackName);
			Assert.AreNotEqual(l1.CatenarySpec,      l1a.CatenarySpec);

			l1a.CopyExtraProperties(l1);

			Assert.AreEqual(l1.TrackSpeed_ms,     l1a.TrackSpeed_ms);
			Assert.AreEqual(l1.SuperElevation_mm, l1a.SuperElevation_mm);
			Assert.AreEqual(l1.TrackName,         l1a.TrackName);
			Assert.AreEqual(l1.CatenarySpec,      l1a.CatenarySpec);
		}

		[TestMethod]
		public void PreserveExtraProperties_Absorb()
		{
			var A = new TrainTrackNode("A", new Vector3(1, 0, 0));

			var B = new TrainTrackNode("B", new Vector3(2, 0, 0));
			var C = new TrainTrackNode("C", new Vector3(2, 0, 0));
			
			var D = new TrainTrackNode("D", new Vector3(3, 0, 0));

			var AB = A.AddLink(B);
			var CD = C.AddLink(D);


			AB.ForwardLink.TrackName = "AB";
			AB.ForwardLink.ReverseLink.TrackName = "BA";

			CD.ForwardLink.TrackName = "CD";
			CD.ForwardLink.ReverseLink.TrackName = "DC";

			Assert.IsTrue(B.Absorb(C));

			foreach (var node in new[] { A, B, D })
			{
				foreach(var link in node.TrackLinks.Values)
				{
					Assert.IsNotNull(link.TrackName);
					Assert.IsNotNull(link.ReverseLink.TrackName);
				}
			}
		}

		[TestMethod]
		public void PreserveExtraProperties_RemoveButPreserveLinkPast()
		{
			var A = new TrainTrackNode("A", new Vector3(1,    0, 0));
			var B = new TrainTrackNode("B", new Vector3(2,    0, 0));
			var C = new TrainTrackNode("C", new Vector3(2.1f, 0, 0));
			var D = new TrainTrackNode("D", new Vector3(3,    0, 0));

			var AB = A.AddLink(B);
			var BC = B.AddLink(C);
			var CD = C.AddLink(D);


			AB.ForwardLink.TrackName = "AB";
			AB.ForwardLink.ReverseLink.TrackName = "BA";

			BC.ForwardLink.TrackName = "BC";
			BC.ForwardLink.ReverseLink.TrackName = "CB";

			CD.ForwardLink.TrackName = "CD";
			CD.ForwardLink.ReverseLink.TrackName = "DC";

			Assert.IsTrue(B.RemoveButPreserveLinkPast(C));

			foreach (var node in A.AllReachableTrackNodes())
			{
				foreach (var link in node.TrackLinks.Values)
				{
					Assert.IsNotNull(link.TrackName);
					Assert.IsNotNull(link.ReverseLink.TrackName);
				}
			}
		}

		[TestMethod]
		public void PreserveExtraProperties_ReplaceLink()
		{
			var A = new TrainTrackNode("A", new Vector3(1, 0, 0));
			var B1 = new TrainTrackNode("B1", new Vector3(2, 0, 0));
			var B2 = new TrainTrackNode("B2", new Vector3(2, 1, 0));
			var C1 = new TrainTrackNode("C1", new Vector3(3, 0, 0));
			var C2 = new TrainTrackNode("C2", new Vector3(3, 1, 0));

			/*      A     
			 *    / > \   
			 *   B1   B2  
			 *   |     |  
			 *   C2   C2  
			 *            
			 */


			var AB1  = A.AddLink(B1);
			var B1C1 = B1.AddLink(C1);
			var B2C2 = B2.AddLink(C2);

			AB1.ForwardLink.TrackName = "AB1";
			AB1.ForwardLink.ReverseLink.TrackName = "B1A";

			B1C1.ForwardLink.TrackName = "B1C1";
			B1C1.ForwardLink.ReverseLink.TrackName = "C1B1";

			B2C2.ForwardLink.TrackName = "B2C2";
			B2C2.ForwardLink.ReverseLink.TrackName = "2CB2";

			Assert.IsTrue(A.ReplaceLink(B1, B2));

			foreach (var node in A.AllReachableTrackNodes())
			{
				foreach (var link in node.TrackLinks.Values)
				{
					Assert.IsNotNull(link.TrackName);
					Assert.IsNotNull(link.ReverseLink.TrackName);
				}
			}
		}
	}
}
