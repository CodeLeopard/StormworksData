﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;
using System.Xml.Linq;

using DataModel.Tiles;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Pastel;

using Shared;
using Shared.Serialization;

using Tiles_Tile = DataModel.Tiles.Tile;

namespace UTests.DataModel.Tile
{
	[TestClass]
	public class Tile
	{
		[TestMethod]
		public void LoadAll()
		{
			Helpers.InconclusiveWithoutStormworksInstall();

			Console.WriteLine("Starting tests for Tiles...");
			var stream = new MemoryStream();


			var filePathList = Directory.GetFiles(StormworksPaths.Data.tiles, "*.xml", SearchOption.AllDirectories);
			foreach (string filePath in filePathList)
			{
				var fileName = Path.GetFileNameWithoutExtension(filePath);
				if (fileName.EndsWith("_instances")) continue;

				Console.WriteLine($"Reading tile: {fileName.PadLeft(50)}");
				var document = XMLHelper.LoadFromFile(filePath);

				var romPath = PathHelper.GetRelativePath(StormworksPaths.rom, filePath);
				var tile = new Tiles_Tile(document, romPath);

				Assert.IsFalse(tile.ChangedSinceLastSaved);

				foreach (var item in tile.Elements.Values)
				{
					Assert.IsFalse(item.ChangedSinceLastSaved);
				}
				continue;
				// todo: Write-Back test: re-serialize and re-read and check for equality.
				// Writing tile is not implemented, TileExporter in Unity operates on XDocument direcly.



				SerializationHelper.Dialect.SaveToStream(stream, tile);

				stream.Position = 0;

				var reReadDocument = XMLHelper.LoadFromStream(stream);


				bool deepEquals = XNode.DeepEquals(document, reReadDocument);
				Assert.IsTrue(deepEquals);

				// Reset the stream, but re-use the allocated buffer.
				stream.SetLength(0);
				stream.Seek(0, SeekOrigin.Begin);
			}
		}

		[TestMethod]
		public void ValidateAll()
		{
			Helpers.InconclusiveWithoutStormworksInstall();

			Console.WriteLine("Starting tests for Tiles...");
			StringBuilder sb = new StringBuilder();
			int countFailed = 0;
			int countPass = 0;

			var filePathList = Directory.GetFiles(StormworksPaths.Data.tiles, "*.xml", SearchOption.AllDirectories);
			foreach (string filePath in filePathList)
			{
				var fileName = Path.GetFileNameWithoutExtension(filePath);

				if (Tiles_Tile.InstancesRegex.IsMatch(fileName))
				{
					// *_instances.xml files contain trees, these are loaded separately.
					continue;
				}

				if (Tiles_Tile.InstancesRegex.IsMatch(fileName))
				{
					// rotations of tiles. They don't show up in the in-game editor, so we won't show them either.
					//return;
				}


				XDocument doc;
				try
				{
					doc = XMLHelper.LoadFromFile(filePath);
				}
				catch (Exception e)
				{
					Console.WriteLine
						(
						 $"Error loading tile '{fileName}' from file '{filePath}' during XML loading."
					   + $" The tile will be skipped. Full Exception details:\n{e.ToString().Pastel(Color.Red)}"
						);

					countFailed++;
					continue;
				}

				var tileRomPath = PathHelper.GetRelativePath(StormworksPaths.rom, filePath);


				Tiles_Tile tileInfo;
				try
				{
					tileInfo = new Tiles_Tile(doc.Element("definition"), tileRomPath);
				}
				catch (Exception e)
				{
					Console.WriteLine
						(
						 $"Error loading tile '{fileName}' from file '{filePath}' during Tile Representation loading."
					   + $" The tile will be skipped. Full Exception details:\n{e.ToString().Pastel(Color.Red)}"
						);

					countFailed++;
					continue;
				}


				try
				{
					if (Validator.Validate(sb, tileInfo)
								 .HasFlag(Validator.ValidationResult.Fail | Validator.ValidationResult.Warn))
					{
						Console.WriteLine(tileRomPath.Pastel(Color.Red));
						Console.WriteLine(sb);
						countFailed++;
					}
					else
					{
						Console.WriteLine(tileRomPath.Pastel(Color.DarkGreen));
						countPass++;
					}
				}
				catch (Exception e)
				{
					Console.WriteLine
						(
						 $"Error validating tile '{fileName}' from file '{filePath}'."
					   + $" The tile will be skipped. Full Exception details:\n{e.ToString().Pastel(Color.Red)}"
						);

					Console.WriteLine($"Validation log (could be empty due to error):");
					Console.WriteLine(sb);


					countFailed++;
					continue;
				}
				finally
				{
					sb.Clear();
				}
			}

			int countTotal = countPass + countFailed;

			if (countFailed == 0)
			{
				Console.WriteLine($"\nValidation completed, all {countTotal} tiles passed validation.".Pastel(Color.LimeGreen));
			}
			else
			{
				Console.WriteLine($"\n{"Validation completed".Pastel(Color.Orange)}, "
								+ $"{countTotal} tiles checked, {$"{countPass} passed".Pastel(Color.LimeGreen)}, "
								+ $"{$"{countFailed} failed.".Pastel(Color.Red)}");
			}
		}
	}
}
