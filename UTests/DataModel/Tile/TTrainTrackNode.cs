﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DataModel.Tiles;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using OpenToolkit.Mathematics;

namespace UTests.DataModel.Tile
{
	[TestClass]
	public class TTrainTrackNode
	{
		[TestMethod]
		[ExpectedException(typeof(ArgumentException))]
		public void CannotLinkToSelf()
		{
			var node = new TrainTrackNode();
			node.id = "foo";

			node.AddLink(node);
		}

		[TestMethod]
		public void CannotRemoveLinkToSelf()
		{
			var node = new TrainTrackNode();
			node.id = "foo";

			Assert.IsFalse(node.RemoveLink(node));
		}

		[TestMethod]
		public void CanAddLink()
		{
			var a = new TrainTrackNode(); a.id = "foo";
			var b = new TrainTrackNode(); b.id = "bar";

			Assert.IsTrue(a.AddLink(b).WasAdded);
		}

		[TestMethod]
		public void CanRemoveLink_0()
		{
			var a = new TrainTrackNode(); a.id = "foo";
			var b = new TrainTrackNode(); b.id = "bar";

			if (! a.AddLink(b).WasAdded) Assert.Inconclusive();

			Assert.IsTrue(a.RemoveLink(b));
		}

		[TestMethod]
		public void CanRemoveLink_1()
		{
			var a = new TrainTrackNode(); a.id = "foo";
			var b = new TrainTrackNode(); b.id = "bar";

			if (!a.AddLink(b).WasAdded) Assert.Inconclusive();

			Assert.IsTrue(b.RemoveLink(a));
		}

		[TestMethod]
		public void ReachableNodes_0_TJunction()
		{
			var v = new Vector3();
			var a = new TrainTrackNode("a", v);
			var b = new TrainTrackNode("b", v);

			var t = new TrainTrackNode("t", v);

			var i = new TrainTrackNode("i", v);
			var j = new TrainTrackNode("j", v);

			a.AddLink(b);
			i.AddLink(j);


			t.AddLink(a);
			t.AddLink(i);

			var nodes = t.AllReachableTrackNodes().ToArray();

			Assert.That.NotContains(nodes, t, "The StartNode should not appear.");
			Assert.That.HasNoDuplicates(nodes);

			var first = new[] { a, i };
			Assert.That.EqualsOneOf(first, nodes[0]);
			Assert.That.EqualsOneOf(first, nodes[1]);

			var second = new[] { b, j };
			Assert.That.EqualsOneOf(second, nodes[2]);
			Assert.That.EqualsOneOf(second, nodes[3]);

			Assert.AreEqual(nodes.Length, 4);
		}

		[TestMethod]
		public void ReachableNodes_1_Cycle()
		{
			var v = new Vector3();
			var a = new TrainTrackNode("a", v);
			var b = new TrainTrackNode("b", v);
			var c = new TrainTrackNode("c", v);
			var d = new TrainTrackNode("d", v);
			var e = new TrainTrackNode("e", v);

			a.AddLink(b);
			b.AddLink(c);
			c.AddLink(d);
			d.AddLink(e);
			e.AddLink(a);

			var nodes = a.AllReachableTrackNodes().ToArray();

			Assert.That.NotContains(nodes, a, "The StartNode should not appear.");

			var expectedOrder = new List<ICollection<TrainTrackNode>>(4);

			expectedOrder.Add(new[] { b, e });
			expectedOrder.Add(new[] { e, b });
			expectedOrder.Add(new[] { c, d });
			expectedOrder.Add(new[] { d, c });

			Assert.That.SequenceEqualsWithAlternatives(expectedOrder, nodes);

			Assert.That.ContainsExactlyOnce(nodes, b);
			Assert.That.ContainsExactlyOnce(nodes, c);
			Assert.That.ContainsExactlyOnce(nodes, d);
			Assert.That.ContainsExactlyOnce(nodes, e);

			Assert.That.HasNoDuplicates(nodes);

			Assert.AreEqual(nodes.Length, 4);
		}
	}
}
