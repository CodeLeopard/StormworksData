﻿// Copyright 2022-2024 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;

using CustomSerialization;
using CustomSerialization.BuiltIn;
using CustomSerialization.Specification;

using DataModel.Missions;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using OpenToolkit.Mathematics;

using Shared;
using Shared.Serialization;

using Exception = System.Exception;

namespace UTests.DataModel
{
	[TestClass]
	public class Missions
	{
		private SpecGenerator.SpecGeneratorSettings settings;
		private GraphSerializationSpecification graphSpec;
		private StreamingContext context = new StreamingContext();

		[TestInitialize]
		public void Setup()
		{
			settings = new SpecGenerator.SpecGeneratorSettings();
			settings.AllowUnguided = true;

			graphSpec = SpecGenerator.CreateGraphSerializationSpecification(typeof(Playlist), settings);
			graphSpec.NoHeader = true;

			OpenTKMathTypes.SetStormworksMathTypes(graphSpec);

			/*
			 // todo: this should work automagically.
			var spawn_transform = graphSpec.TypeSpecifications[typeof(Component)]
			                               .Members.Find(ms => ms.RealName == nameof(Component.spawn_transform));
			spawn_transform.Optional = true;
			spawn_transform.DefaultValue = Matrix4.Identity;
			*/
		}


		/// <summary>
		/// Check that the Playlist does not have missing or nonsense values.
		/// </summary>
		/// <param name="playlist"></param>
		public void ValidatePlaylist(Playlist playlist)
		{
			foreach (Location location in playlist.Locations)
			{
				ValidateLocation(location);
			}
		}

		public void ValidateLocation(Location location)
		{
			foreach (Component component in location.components)
			{
				ValidateComponent(component);
			}
		}

		public void ValidateComponent(Component component)
		{
			if (component.component_type == ComponentType.Vehicle
			 || component.component_type == ComponentType.MissionZone
			 || component.component_type == ComponentType.EnvironmentZone)
			{
				var bounds = component.spawn_bounds;
				Assert.AreNotEqual(Vector3.Zero, bounds.Size);
			}

			Assert.AreNotEqual(0, component.id);

			Assert.AreNotEqual(Matrix4.Zero, component.spawn_transform);
		}

		public Playlist Deserialize(Stream stream)
		{
			var doc = XMLHelper.LoadFromStream(stream);
			return Serializer.Deserialize<Playlist>(doc, graphSpec, context);
		}

		public void Serialize(Stream stream, Playlist value)
		{
			var doc = Serializer.Serialize(value, graphSpec, context);
			XMLHelper.SaveToStream(stream, doc);
		}

		[TestMethod]
		public void LocationTileHandling()
		{
			var playlist = new Playlist();
			var location = new Location(playlist, "foo\\bar", true);

			Assert.AreEqual("foo/bar", location.tile);

			location.tile = null;

			Assert.AreEqual(null, location.tile);

			location.tile = "foo/bar";

			Assert.AreEqual("foo/bar", location.tile);
		}


		private Playlist CreateTestPlaylist()
		{
			var playlist = new Playlist();

			playlist.folder_path = "data/missions/playlist_folder";
			playlist.file_store = 4;
			playlist.name = "PlayListName, such creativety, much wow";

			{
				var l = new Location(playlist, "data/tiles/mega_island_3_3.xml", true);
				l.name = "MEGA_ISLAND_3_3";

				var c = new Component(ComponentType.EnvironmentZone);
				c.vehicle_parent_transform = Matrix4.Identity;
				c.spawn_transform = Matrix4.CreateTranslation(new Vector3(1, 2, 3));
				c.spawn_bounds = new Bounds3(true);
				c.spawn_bounds.Inflate(new Vector3(1, 2, 3));
				c.spawn_bounds.Inflate(new Vector3(-3, -2, -1));

				l.Add(c);
			}

			{
				var l = new Location(playlist, "data/tiles/mega_island_6_9.xml", false);
				l.name = "MEGA_ISLAND_6_9";

				var c = new Component(ComponentType.MissionZone);
				c.vehicle_parent_transform = Matrix4.CreateTranslation(new Vector3(3, 2, 1));
				c.spawn_transform = Matrix4.Identity;
				c.spawn_bounds.Inflate(new Vector3(1, 2, 3));
				c.spawn_bounds.Inflate(new Vector3(-3, -2, -1));
				l.Add(c);
			}

			return playlist;
		}


		[TestMethod]
		public void ModelConsistency()
		{
			var playlist = CreateTestPlaylist();
			ValidatePlaylist(playlist);
		}


		[TestMethod]
		public void TrivialRoundTrip()
		{
			var playlist = CreateTestPlaylist();

			using (var stream = new MemoryStream())
			{
				Serialize(stream, playlist);

				stream.Position = 0;


				Console.WriteLine("--- Generated Playlist Xml ---");
				var reader = new StreamReader(stream);
				Console.WriteLine(reader.ReadToEnd());
				Console.WriteLine("--- End Generated Playlist Xml ---");


				stream.Position = 0;
				var readBack = Deserialize(stream);
				stream.SetLength(0);

				Serialize(stream, playlist);
				stream.Position = 0;

				Console.WriteLine("--- WriteBack Playlist Xml ---");
				reader = new StreamReader(stream);
				Console.WriteLine(reader.ReadToEnd());

				Console.WriteLine("--- End WriteBack Playlist Xml ---");
			}
		}


		[TestMethod]
		public void LoadAll()
		{
			Helpers.InconclusiveWithoutStormworksInstall();
			string searchRoot = Path.Combine(StormworksPaths.data, "missions");
			Console.WriteLine($"Searching for playlists in '{searchRoot}'");

			IEnumerable<string> missionPaths = Directory.EnumerateFiles(searchRoot, "playlist.xml", SearchOption.AllDirectories);

			using var OriginalStream = new MemoryStream();
			using var WriteBackStream = new MemoryStream();

			foreach (var path in missionPaths)
			{
				try
				{
					Console.Write($"Loading playlist '{PathHelper.GetRelativePath(searchRoot, path)}'");

					using var file = File.OpenRead(path);

					OriginalStream.SetLength(0);
					file.CopyTo(OriginalStream);
					OriginalStream.Position = 0;

					Playlist originalPlaylist = Deserialize(OriginalStream);


					ValidatePlaylist(originalPlaylist);

					foreach (var component in originalPlaylist.Components)
					{
						var wrap = ComponentWrapper.Create(component);

						Assert.That.IsComponentWrapper(wrap.GetType());
					}

					WriteBackStream.SetLength(0);
					Serialize(WriteBackStream, originalPlaylist);
					WriteBackStream.Position = 0;

					Playlist readBack = Deserialize(WriteBackStream);

					ValidatePlaylist(readBack);

					OriginalStream.Position = 0;
					WriteBackStream.Position = 0;

					using (var compareTruth = File.Open("temp_truth.xml", FileMode.Create))
					{
						OriginalStream.WriteTo(compareTruth);
						OriginalStream.Position = 0;
					}

					using (var compareFile = File.Open("temp.xml", FileMode.Create))
					{

						WriteBackStream.WriteTo(compareFile);
						WriteBackStream.Position = 0;
					}

					// todo: the game uses the old format in palces so this just doesn't work.
					//Assert.That.SequenceEquals(OriginalStream.ToArray(), WriteBackStream.ToArray());


					/*
					SerializerHelper.SaveToStream(WriteBackStream, playlist);
	
					// todo: Compare results
	
					OriginalStream.Position = 0;
					WriteBackStream.SetLength(0);
					WriteBackStream.Position = 0;
	
					var compare = new StreamCompare();
					var streamEquals = compare.AreEqualAsync(OriginalStream, WriteBackStream).Result;
	
					Console.Write($" writeBack StreamEquals {(streamEquals ? strTrue : strFalse)}");
	
					*/
					Console.WriteLine();
				}
				catch (Exception e)
				{
					throw new Exception($"Error while testing with '{path}'.", e);
				}
			}
		}
	}
}
