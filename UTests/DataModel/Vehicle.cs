﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BinaryDataModel.Converters;
using BinaryDataModel.DataTypes;

using DataModel.Definitions;
using DataModel.Vehicles;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Pastel;

using Shared;
using Shared.Serialization;

using PathHelper = Shared.PathHelper;

using DataModel_Vehicle = DataModel.Vehicles.Vehicle;

namespace UTests.DataModel
{
	[TestClass]
	public class Vehicle
	{
		private const bool UseMultiThreading = true;


		private static readonly Dictionary<string, Mesh> meshes = new Dictionary<string, Mesh>();
		private static Mesh GetOrLoadMesh(string pathRelativeToRom)
		{
			// Unlocked fast path
			if (meshes.TryGetValue(pathRelativeToRom, out Mesh mesh)) return mesh;

			lock (meshes)
			{
				// Check again with the lock
				if (!meshes.TryGetValue(pathRelativeToRom, out mesh))
				{
					var fullPath = Path.Combine(StormworksPaths.rom, pathRelativeToRom);

					if (File.Exists(fullPath))
						mesh = Binary.LoadMesh(fullPath);

					// Saves null also, in case file not found.
					meshes[pathRelativeToRom] = mesh;
				}
			}

			return mesh;
		}


		[TestInitialize]
		public void Setup()
		{
			if (!Definition.Ready)
				Definition.UseOnTheFlyDefinitions(StormworksPaths.Data.definitions);

			MeshGenerator.GetOrLoadMesh = GetOrLoadMesh;
		}

		[TestCleanup]

		public void Cleanup()
		{
			// todo: reset definitions
		}


		private void Validate(DataModel_Vehicle v)
		{

		}

		private void Validate(Body b)
		{
			var mesh = b.mesh;
			Validate(mesh);
		}

		private void Validate(Mesh m)
		{
			var initialBounds = m.Bounds();
			Assert.That.IsNormal(initialBounds, "Initial mesh bounds has invalid value.");

			m.RecomputeBounds();
			var computedBounds = m.Bounds();

			Assert.AreEqual(initialBounds, computedBounds, "Expected bounds to stay the same after reComputation.");
		}

		private void LoadVehicles(IEnumerable<string> filePaths, string searchRoot = null)
		{
			bool completedAny = false;
			var parallelOptions = new ParallelOptions();
			parallelOptions.MaxDegreeOfParallelism = UseMultiThreading ? Environment.ProcessorCount : 1;
			var partitioner = Partitioner.Create(filePaths, EnumerablePartitionerOptions.NoBuffering);
			Parallel.ForEach(partitioner, parallelOptions, Each);
			void Each(string filePath, ParallelLoopState loopState, long i)
			{
				StringBuilder sb = new StringBuilder();
				try
				{
					string vehicleName = searchRoot != null ? PathHelper.GetRelativePath(searchRoot, filePath) : filePath;

					UInt64 fileSize = (UInt64)new FileInfo(filePath).Length;


					sb.Append($"{vehicleName,40} | {StringExt.PrintBytesReadable(fileSize),10}");
					var document = XMLHelper.LoadFromFile(filePath);
					var vehicle = new DataModel_Vehicle(document.Element("vehicle"), vehicleName);
					document = null;

					Validate(vehicle);

					UInt64 componentCount = 0;

					{ // Component counts
						foreach (Body vehicleBody in vehicle.bodies)
						{
							var count = vehicleBody.components.LongCount();
							componentCount += (UInt64)count;

							Validate(vehicleBody);
						}

						sb.Append($" | Components: {componentCount,5}");
						sb.Append($" | Voxels {vehicle.Voxels.Count, 5}");
					}

					completedAny = true;
				}
				finally
				{
					Console.WriteLine(sb);
				}
			}

			if (! completedAny)
			{
				Assert.Inconclusive("Did not receive any vehicles to test.");
			}
		}


		[TestMethod]
		public void LoadDebris()
		{
			Assert.Inconclusive("Debris vehicles don't follow the spec of the other vehicles. Currently can't deal with that.");
			Helpers.InconclusiveWithoutStormworksInstall();

			var searchRoot = StormworksPaths.Data.debris;
			Console.WriteLine($"Testing vehicles in '{searchRoot}'");
			IEnumerable<string> filePaths = Directory.EnumerateFiles
				(searchRoot, "*.xml", SearchOption.AllDirectories);

			LoadVehicles(filePaths, searchRoot);
		}

		[TestMethod]
		public void LoadPresetVehiclesAdvanced()
		{
			Helpers.InconclusiveWithoutStormworksInstall();

			var searchRoot = Path.Combine(StormworksPaths.data, "preset_vehicles_advanced");
			Console.WriteLine($"Testing vehicles in '{searchRoot}'");
			IEnumerable<string> filePaths = Directory.EnumerateFiles
				(searchRoot, "*.xml", SearchOption.AllDirectories);

			LoadVehicles(filePaths, searchRoot);
		}

		[TestMethod]
		public void LoadMissionVehicles()
		{
			Helpers.InconclusiveWithoutStormworksInstall();

			var searchRoot = StormworksPaths.Data.missions;
			Console.WriteLine($"Testing vehicles in '{searchRoot}'");
			IEnumerable<string> filePaths = Directory.EnumerateFiles
				(searchRoot, "vehicle*.xml", SearchOption.AllDirectories);

			LoadVehicles(filePaths, searchRoot);
		}

		[TestMethod]
		public void LoadRecentlySavedVehicles()
		{
			Helpers.InconclusiveWithoutStormworksSaveData();

			const int recentAmount = 10;
			//const int recentAmount = 20;

			var searchRoot = StormworksPaths.UserData.vehicles;
			try
			{
				Console.WriteLine($"Testing {recentAmount} of the most recently saved vehicles in '{searchRoot}'");

				var dinfo = new DirectoryInfo(searchRoot);
				var files = dinfo.GetFiles("*.xml").OrderByDescending(f => f.CreationTimeUtc);

				LoadVehicles(files.Take(recentAmount).Select(f => f.FullName), searchRoot);
			}
			catch (UnauthorizedAccessException)
			{
				// This will happen on CI
				Assert.Inconclusive("User saves not available");
			}
			catch (DirectoryNotFoundException)
			{
				// This will happen on CI
				Assert.Inconclusive("User saves not available");
			}
			catch (FileNotFoundException)
			{
				// This will happen on CI
				Assert.Inconclusive("User saves not available");
			}
		}
	}
}
