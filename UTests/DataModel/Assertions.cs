﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Text;

using DataModel.Missions;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using OpenToolkit.Mathematics;

using Shared;

namespace UTests.DataModel
{
	internal static class Assertions
	{
		public static void IsComponentWrapper(this Assert instance, Type t)
		{
			Assert.IsTrue(t.IsSubclassOf(typeof(ComponentWrapper)), $"Type {t.Name} is not a subclass of {nameof(ComponentWrapper)}.");
		}

		public static void IsNormal(this Assert instance, float value, string message = null)
		{
			Assert.IsFalse(float.IsNaN(value), message ?? "float.IsNaN(value)");
			Assert.IsFalse(float.IsInfinity(value), message ?? "float.IsInfinity(value)");
		}

		public static void IsNormal(this Assert instance, Vector3 value, string message = null)
		{
			Assert.That.IsNormal(value.X, message);
			Assert.That.IsNormal(value.Y, message);
			Assert.That.IsNormal(value.Z, message);
		}

		public static void IsNormal(this Assert instance, Bounds3 value, string message = null)
		{
			Assert.IsTrue(value.IsFinite(),  message ?? "value has NaN or Infinity");
		}
	}
}
