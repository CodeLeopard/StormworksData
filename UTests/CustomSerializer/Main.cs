﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Text;

using CurveGraph;

using CustomSerialization;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using CustomSerialization.Specification;

using UTests.CurveGraph;

namespace UTests.CustomSerializer
{
	/// <summary>
	/// Note that there are also tests in <see cref="K_Serialization"/>, though these are specifically aimed at <see cref="CurveGraph"/>.
	/// </summary>
	[TestClass]
	public class Main
	{
		[TestMethod]
		public void Test()
		{
			var helper = new K_Serialization();
			var data = helper.TrivialSetup();

			var spec = SpecGenerator.CreateGraphSerializationSpecification(data.GetType());

			spec.AddType(typeof(K_Serialization.Mock_NodeCustomData));
			spec.AddType(typeof(K_Serialization.Mock_ConnectionCustomData));

			spec.TypeSpecifications[typeof(CurveNode)].ByRef = true;
			spec.TypeSpecifications[typeof(Connection)].ByRef = true;

			using var stream = new MemoryStream();

			Serializer.Serialize(stream, data, spec, new StreamingContext(StreamingContextStates.File | StreamingContextStates.Persistence));

			stream.Position = 0;

			var reader = new StreamReader(stream);
			Console.WriteLine(reader.ReadToEnd());
			stream.Position = 0;

			var read = Serializer.Deserialize<List<CurveNode>>(stream, spec, new StreamingContext(StreamingContextStates.File | StreamingContextStates.Persistence));
		}
	}
}
