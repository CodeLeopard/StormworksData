﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.IO;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Shared;

using Environment = System.Environment;

namespace UTests
{
	internal static class Helpers
	{
		/// <summary>
		/// Will be true if tests are running on CI server. The CI server is missing some resources, so some tests cannot be run there.
		/// </summary>
		public static readonly bool CI_Preset = false;
		private static readonly bool _install_found = false;
		private static readonly bool _userData_found = false;

		static Helpers()
		{
			{
				var value = Environment.GetEnvironmentVariable("TEST_CI_PRESET");
				if (value == "1")
				{
					CI_Preset = true;
				}
			}
			{
				string path = StormworksPaths.Install;
				if (! string.IsNullOrWhiteSpace(path)
				 && Directory.Exists(path))
				{
					_install_found = true;
				}
			}
			{
				string path = StormworksPaths.userData;
				if (! string.IsNullOrWhiteSpace(path)
				 && Directory.Exists(path))
				{
					_userData_found = true;
				}
			}
		}


		internal static void InconclusiveWithoutStormworksInstall()
		{
			if (CI_Preset)
			{
				Assert.Inconclusive("CI does not have a Stormworks Instal to test with.");
			}
			else if (! _install_found)
			{
				Assert.Inconclusive("Did not find a Stormworks Install to test with.");
			}
		}

		internal static void InconclusiveWithoutStormworksSaveData()
		{
			if (CI_Preset)
			{
				Assert.Inconclusive("CI does not have Stormworks UserData to test with.");
			}
			else if (! _userData_found)
			{
				Assert.Inconclusive("Did not find Stormworks UserData to test with.");
			}
		}



		/// <summary>
		/// Compare streams a byte at a time.
		/// </summary>
		/// <param name="instance"></param>
		/// <param name="expected"></param>
		/// <param name="actual"></param>
		/// <param name="reportDiffs">Keep looking for diffirences until this many have been found and report them all.</param>
		public static bool StreamEquals(Stream expected, Stream actual, in StringBuilder sb, int reportDiffs = 0)
		{
			if (expected.Length != actual.Length)
			{
				sb.AppendLine($"Streams are not the same length, expected: ${expected.Length}, received: ${actual.Length}.");

				if (reportDiffs == 0)
				{
					return false;
				}
			}

			long i = 0;
			long max = Math.Min(expected.Length, actual.Length);

			while (i++ < max)
			{
				int bexpected = expected.ReadByte();
				int bactual = actual.ReadByte();


				if (bexpected != bactual)
				{
					sb.AppendLine($"[{i,4}] {bexpected} != {bactual}");

					reportDiffs--;

					if (reportDiffs <= 0)
					{
						sb.AppendLine("Reached limit of failed assertions, ending comparison.");
						return false;
					}
				}
			}


			if (sb.Length > 0)
			{
				return false;
			}
			return true;
		}



		/// <summary>
		/// Compare text streams a line at a time.
		/// </summary>
		/// <param name="instance"></param>
		/// <param name="expected"></param>
		/// <param name="actual"></param>
		/// <param name="reportDiffs">Keep looking for diffirences until this many have been found and report them all.</param>
		public static bool TextStreamEquals(Stream expected, Stream actual, in StringBuilder sb, int reportDiffs = 0, bool ignoreLineEndings = false)
		{
			bool foundUnequalLines = TextStreamEquals(new StreamReader(expected, leaveOpen: true), new StreamReader(actual, leaveOpen: true), sb, reportDiffs);

			if (!foundUnequalLines && sb.Length > 0 && !ignoreLineEndings)
			{
				sb.AppendLine($"Did not find line diffirences, falling back to byte comparison...");
				expected.Position = 0;
				actual.Position = 0;
				StreamEquals(expected, actual, sb, reportDiffs);
				return false;
			}
			return true;
		}

		/// <summary>
		/// Compare text streams a line at a time.
		/// </summary>
		/// <param name="instance"></param>
		/// <param name="expected"></param>
		/// <param name="actual"></param>
		/// <param name="reportDiffs">Keep looking for diffirences until this many have been found and report them all.</param>
		public static bool TextStreamEquals(TextReader expected, TextReader actual, in StringBuilder sb, int reportDiffs = 0)
		{
			long i = 0;

			while (true)
			{
				var lexpected = expected.ReadLine();
				var lactual = actual.ReadLine();

				if (lexpected != lactual)
				{
					sb.AppendLine($"[{i,4}] {lexpected}\n" +
								   $"       {lactual}");

					reportDiffs--;

					if (reportDiffs <= 0)
					{
						sb.AppendLine("Reached limit of failed assertions, ending comparison.");
						return false;
					}
				}

				if (lexpected == null && lactual == null) { break; }
				if (lexpected == null || lactual == null)
				{
					sb.AppendLine("Did not have the same number of lines.");
					return false;
				}
			}


			if (sb.Length > 0)
			{
				return false;
			}
			return true;
		}
	}
}
