﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

using BinaryDataModel.Converters;
using BinaryDataModel.DataTypes;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using NeoSmart.StreamCompare;

using Shared;

using static UTests.Helpers;

using UTests.CurveGraph;
using Microsoft.VisualStudio.TestPlatform.PlatformAbstractions.Interfaces;

namespace UTests.BinaryDataModel
{
	[TestClass]
	public class PlyConverter
	{
		public const string testMeshPath = BinaryConverter.testMeshPath;
		public const string testPhysPath = BinaryConverter.testPhysPath;


		public const string testSimpleMeshPath = "meshes/arctic_ice_floating_01.mesh";
		public const string testSimplePhysPath = "meshes/arctic_ice_floating_01_phys.phys";

		public const string testTempOutFolder = "TestTemp";


		public const string InconclusiveMessage = BinaryConverter.InconclusiveMessage;

		private StreamCompare comparer = new StreamCompare();

		public static void IgnoreFileNotFoundException
			(Action test) =>
			BinaryConverter.IgnoreFileNotFoundException(test);


		[TestInitialize]
		public void TestPrepare()
		{
			var path = Path.GetFullPath(testTempOutFolder);
			Directory.CreateDirectory(path);
		}

		private void MeshRoundTrip(string testPath)
		{
			IgnoreFileNotFoundException(Test);

			void Test()
			{
				// Fetch Test data
				var path = Path.Combine(StormworksPaths.rom, testPath);
				using var loadStream = File.OpenRead(path);
				var mesh = Binary.LoadMesh(loadStream);

				// Generate truth
				using var truthStream = new MemoryStream();
				Ply.Save(mesh, truthStream);


				// Read from truth
				truthStream.Position = 0;
				var readBack = Ply.LoadMesh(truthStream, "");

				// Generate test
				using var testStream = new MemoryStream();
				Ply.Save(readBack, testStream);

				truthStream.Position = 0;
				testStream.Position = 0;
#if DEBUG
				{
					long truthLength = truthStream.Length;
					long testLength = testStream.Length;
					long diff = truthLength - testLength;

					using var truthOut = File.Open($"{testTempOutFolder}/mesh_ply_Truth.ply", FileMode.Create);
					truthStream.WriteTo(truthOut);

					using var testOut = File.Open($"{testTempOutFolder}/mesh_ply_Test.ply", FileMode.Create);
					testStream.WriteTo(testOut);
				}
#endif
				truthStream.Position = 0;
				testStream.Position = 0;

				var truthReader = new StreamReader(truthStream);
				var testReader  = new StreamReader(testStream);

				int line = -1;
				while (! truthReader.EndOfStream && !testReader.EndOfStream)
				{
					line++;
					var tl = truthReader.ReadLine();
					var dl = testReader.ReadLine();

					if (tl != dl)
					{
						Assert.Fail($"Mismatch at line {line}:\nExpected: {tl}\nReceived: {dl}");
					}
				}

				if (truthReader.EndOfStream != testReader.EndOfStream)
				{
					if (truthReader.EndOfStream)
					{
						Assert.Fail($"Mismatch in length, Expected file reached end, but test data still has data remaining after line {line}.");
					}
					else if (testReader.EndOfStream)
					{
						Assert.Fail($"Mismatch in length, Test file reached end, but Truth data still has data remaining after line {line}.");
					}
				}

				if (truthStream.Length != testStream.Length)
				{
					Assert.Fail($"Both streams reached the end, but their length is not the same.");
				}

				// 2nd opinion just in case.
				Assert.IsTrue(comparer.AreEqualAsync(truthStream, testStream).Result, ".ply files did not match after round trip.");


				// Test saving back binary for even stricter check.
				using var loadCompare = new MemoryStream();
				Binary.Save(loadCompare, readBack);
#if DEBUG
				{
					long truthLength = loadStream.Length;
					long testLength = loadCompare.Length;
					long diff = truthLength - testLength;

					using var testOut = File.Open($"{testTempOutFolder}/mesh_ply_bin_Test.mesh", FileMode.Create);
					testStream.WriteTo(testOut);
				}
#endif
				Assert.IsTrue(comparer.AreEqualAsync(truthStream, testStream).Result, ".mesh files did not match after round trip.");
			}
		}

		private void PhysRoundTrip(string testPath)
		{
			IgnoreFileNotFoundException(Test);

			void Test()
			{
				var settings = new Ply.ConverterSettings();
				// Fetch Test data
				var path = Path.Combine(StormworksPaths.rom, testPath);
				using var loadStream = File.OpenRead(path);
				var phys = Binary.LoadPhys(loadStream);

				// Generate truth
				using var truthStream = new MemoryStream();
				Ply.Save(phys, truthStream, settings);

#if DEBUG
				{
					long truthLength = truthStream.Length;

					using var truthOut = File.Open($"{testTempOutFolder}/phys_ply_Truth.ply", FileMode.Create);
					truthStream.Position = 0;
					truthStream.WriteTo(truthOut);
				}
#endif


				// Read from truth
				truthStream.Position = 0;
				var readBack = Ply.LoadPhys(truthStream, settings);

				// Generate test
				using var testStream = new MemoryStream();
				Ply.Save(readBack, testStream, settings);

				truthStream.Position = 0;
				testStream.Position = 0;
#if DEBUG
				{
					long truthLength = truthStream.Length;
					long testLength = testStream.Length;
					long diff = truthLength - testLength;

					using var testOut = File.Open($"{testTempOutFolder}/phys_ply_Test.ply", FileMode.Create);
					testStream.WriteTo(testOut);
				}
#endif
				Assert.IsTrue(comparer.AreEqualAsync(truthStream, testStream).Result, ".ply files did not match after round trip.");


				// Test saving back binary for even stricter check.
				using var loadCompare = new MemoryStream();
				Binary.Save(loadCompare, readBack);
#if DEBUG
				{
					long truthLength = loadStream.Length;
					long testLength = loadCompare.Length;
					long diff = truthLength - testLength;

					using var testOut = File.Open($"{testTempOutFolder}/phys_ply_bin_Test.mesh", FileMode.Create);
					testStream.WriteTo(testOut);
				}
#endif
				Assert.IsTrue(comparer.AreEqualAsync(truthStream, testStream).Result, ".phys files did not match after round trip.");
			}
		}


		[TestMethod]
		public void SimpleMeshRoundTrip()
		{
			MeshRoundTrip(testSimpleMeshPath);
		}


		//[TestMethod]
		public void MeshRoundTrip()
		{
			// Needs multiple streams.
			MeshRoundTrip(testMeshPath);
		}

		[TestMethod]
		public void SimplePhysRoundTrip()
		{
			PhysRoundTrip(testSimplePhysPath);
		}

		[TestMethod]
		public void PhysRoundTrip()
		{
			PhysRoundTrip(testPhysPath);
		}

		// todo: add tests for the PlyConverterSettings
		// todo: check reverse triangles
		// todo: check reverse normals

		#region FileTests


		const string unit_cube_mesh = "BinaryDataModel/TestData/unit_cube.mesh";
		const string unit_cube_ply_raw = "BinaryDataModel/TestData/unit_cube_raw.ply";
		const string unit_cube_ply_reverseTriangles = "BinaryDataModel/TestData/unit_cube_tri_reverse.ply";

		[TestMethod]
		public void Settings_Raw()
		{
			Mesh truth = Binary.LoadMesh(unit_cube_mesh);

			var settings = new Ply.ConverterSettings();
			Mesh imported = Ply.LoadMesh(unit_cube_ply_raw, settings: settings);

			Assert.That.AreEqual(truth, imported);
		}

		private static void ReverseNormals(Mesh m)
		{
			var vertices = m.vertices;
			for (int i = 0; i < vertices.Count; i++)
			{
				var v = vertices[i];
				v.normal *= -1;
				vertices[i] = v;
			}
		}

		private static void ReverseTriangles(Mesh m)
		{
			var indices = m.indices;
			for (int i = 0; i < indices.Count;)
			{
				var a = indices[i];
				var b = indices[i + 1];
				var c = indices[i + 2];

				indices[i] = c;
				indices[i + 1] = b;
				indices[i + 2] = a;

				i += 3;
			}
		}

		[TestMethod]
		public void Settings_ReverseNormals()
		{
			Mesh truth = Binary.LoadMesh(unit_cube_mesh);
			ReverseNormals(truth);

			var settings = new Ply.ConverterSettings();
			settings.ReverseNormals = true;
			Mesh imported = Ply.LoadMesh(unit_cube_ply_raw, settings: settings);

			Assert.That.AreEqual(truth, imported);
		}

		[TestMethod]
		public void Settings_ReverseTriangles()
		{
			Mesh truth = Binary.LoadMesh(unit_cube_mesh);
			ReverseTriangles(truth);

			var settings = new Ply.ConverterSettings();
			settings.ReverseTriangles = true;
			Mesh imported = Ply.LoadMesh(unit_cube_ply_raw, settings: settings);

			Assert.That.AreEqual(truth, imported);
		}

		[TestMethod]
		public void Settings_ReverseNormalsAndTriangles()
		{
			Mesh truth = Binary.LoadMesh(unit_cube_mesh);
			ReverseNormals(truth);
			ReverseTriangles(truth);

			var settings = new Ply.ConverterSettings();
			settings.ReverseNormals = true;
			settings.ReverseTriangles = true;
			Mesh imported = Ply.LoadMesh(unit_cube_ply_raw, settings: settings);

			Assert.That.AreEqual(truth, imported);
		}


		[TestMethod]
		public void Settings_SavesCorrectly_NoTransform()
		{
			Mesh truth = Binary.LoadMesh(unit_cube_mesh);
			truth.fileName = "unit_cube.mesh";
			var settings = new Ply.ConverterSettings();

			using var stream = new MemoryStream();

			Ply.Save(truth, stream, settings);
			stream.Position = 0;

			using var truthStream = File.OpenRead(unit_cube_ply_raw);


			var sb = new StringBuilder();
			bool equal = TextStreamEquals(truthStream, stream, in sb, reportDiffs: 100, ignoreLineEndings: true);

#if DEBUG
			if (! equal)
			{
				using var testOut = File.Open($"{testTempOutFolder}/Settings_SavesCorrectly_result.ply", FileMode.Create);
				stream.WriteTo(testOut);
			}
#endif

			Assert.IsTrue(equal, "Saved .ply file did not match expected truth file.\n" + sb.ToString());
		}

		[TestMethod]
		public void Settings_SavesCorrectly_ReverseTriangles()
		{
			Mesh truth = Binary.LoadMesh(unit_cube_mesh);
			truth.fileName = "unit_cube.mesh";
			var settings = new Ply.ConverterSettings();
			settings.ReverseTriangles = true;

			using var stream = new MemoryStream();

			Ply.Save(truth, stream, settings);
			stream.Position = 0;

			using var truthStream = File.OpenRead(unit_cube_ply_reverseTriangles);

			var sb = new StringBuilder();
			bool equal = TextStreamEquals(truthStream, stream, in sb, reportDiffs: 100, ignoreLineEndings: true);

#if DEBUG
			if (!equal)
			{
				using var testOut = File.Open($"{testTempOutFolder}/Settings_SavesCorrectly_ReverseTriangles_result.ply", FileMode.Create);
				stream.WriteTo(testOut);
			}
#endif

			Assert.IsTrue(equal, "Saved .ply file did not match expected truth file.\n" + sb.ToString());
		}

		#region MapBin

		private const string mapBinTestply = "BinaryDataModel/TestData/mega_island_15_2_map_geometry_fixed.ply";
		[TestMethod]
		public void MapBin_Load()
		{
			var result = Ply.LoadMapBin(mapBinTestply);
		}

		[TestMethod]
		public void MapBin_RoundTrip()
		{
			using var truth = File.OpenRead(mapBinTestply);
			using var actual = new MemoryStream((int) truth.Length);

			var result = Ply.LoadMapBin(truth, mapBinTestply);

			truth.Position = 0;

			Ply.Save(result, actual);
			actual.Position = 0;

			var sb = new StringBuilder();
			bool equal = TextStreamEquals(truth, actual, in sb, reportDiffs: 100, ignoreLineEndings: true);


			#if DEBUG
			if (!equal)
			{
				using var testOut = File.Open($"{testTempOutFolder}/mega_island_15_2_map_geometry_fixed.ply", FileMode.Create);
				actual.WriteTo(testOut);
			}
			#endif

			Assert.IsTrue(equal, "Saved .ply file did not match expected truth file.\n" + sb.ToString());
		}


		#endregion MapBin

		#endregion FileTests

		private static Stream StringToStream(string s)
		{
			var stream = new MemoryStream();
			using var writer = new StreamWriter(stream, leaveOpen: true);
			writer.Write(s);
			writer.Flush();
			stream.Position = 0;
			return stream;
		}
	}
}