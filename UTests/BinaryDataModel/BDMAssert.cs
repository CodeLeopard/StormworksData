﻿// Copyright 2024 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Text;

using BinaryDataModel.DataTypes;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using UTests.TestExtrusion;

namespace UTests.BinaryDataModel
{
	/// <summary>
	/// Assertions for BinaryDataModel testing.
	/// </summary>
	internal static class BDMAssert
	{
		internal static void AreEqual(this Assert _, Mesh expected, Mesh actual)
		{
			var e = expected ?? throw new ArgumentNullException(nameof(expected));
			var a = actual ?? throw new ArgumentNullException(nameof(actual));

			var sb = new StringBuilder();

			// We ignore header fields because the .ply converter ignores them.

			_.SequenceEquals(e.vertices, a.vertices);
			_.SequenceEquals(e.indices, a.indices);
			_.SequenceEquals(e.subMeshes, a.subMeshes);

		}
	}
}
