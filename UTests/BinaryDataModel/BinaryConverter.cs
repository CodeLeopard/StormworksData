﻿// Copyright 2022-2024 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using BinaryDataModel.Converters;
using BinaryDataModel.DataTypes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NeoSmart.StreamCompare;
using Shared;
using Shared.Exceptions;
using System;
using System.IO;


namespace UTests.BinaryDataModel
{
	[TestClass]
	public class BinaryConverter
	{
		public const string testMeshPath = "meshes/ultimate_base_hangar.mesh";
		public const string testPhysPath = "meshes/mainland_assets/mainland_hanger_phys.phys";

		public const string InconclusiveMessage = "Did not find the file to test with.";

		private StreamCompare comparer = new StreamCompare();

		/// <summary>
		/// Ignore any File not found related exception as inconclusive.
		/// This could happen on CI server where the game is not installed and as such the referenced files are not present.
		/// </summary>
		/// <param name="test"></param>
		public static void IgnoreFileNotFoundException(Action test)
		{
			try
			{
				test.Invoke();
			}
			catch (UnauthorizedAccessException)
			{
				// The path does exist but is not visible to the current user, could happen on self-hosted CI.
				Assert.Inconclusive(InconclusiveMessage);
			}
			catch (DirectoryNotFoundException)
			{
				Assert.Inconclusive(InconclusiveMessage);
			}
			catch (FileNotFoundException)
			{
				Assert.Inconclusive(InconclusiveMessage);
			}
			catch (FileInteractionException e)
			{
				var innerType = e.NonFileException.GetType();
				if (innerType == typeof(FileNotFoundException)
				 || innerType == typeof(DirectoryNotFoundException)
				 || innerType == typeof(UnauthorizedAccessException))
				{
					Assert.Inconclusive(InconclusiveMessage);
				}

				throw;
			}
		}

		/// <summary>
		/// Expect an <see cref="FileInteractionException"/> with InnerException: <see cref="BinaryDataFormatException"/>.
		/// Calls <see cref="IgnoreFileNotFoundException"/>.
		/// </summary>
		/// <param name="test"></param>
		private static void ExpectFileTypeMismatch(Action test)
		{
			try
			{
				IgnoreFileNotFoundException(test);
			}
			catch (FileInteractionException fileException)
			{
				if (fileException.InnerException == null)
				{
					Assert.Fail($"Expected a {nameof(FileInteractionException)} to be thrown with InnerException: {nameof(BinaryDataFormatException)} but InnerException was null.");
				}

				var innerType = fileException.InnerException.GetType();

				Assert.AreEqual(innerType, typeof(BinaryDataFormatException)
				              , $"Expected a {nameof(FileInteractionException)} to be thrown with InnerException: {nameof(BinaryDataFormatException)} but instead got {innerType.Name}");
				return;
			}

			Assert.Fail($"Expected a {nameof(FileInteractionException)} to be thrown with InnerException: {nameof(BinaryDataFormatException)} but nothing was thrown at all.");
		}

		/// <summary>
		/// Test that <see cref="IgnoreFileNotFoundException"/> actually works.
		/// </summary>
		[TestMethod]
		public void TestExceptionIgnore()
		{
			try
			{
				IgnoreFileNotFoundException(Test);

				void Test()
				{
					File.OpenRead("/definitely/Does/Not.Exist.really.for.sure");
				}
			}
			catch (AssertInconclusiveException e)
			{
				if (e.Message == ("Assert.Inconclusive failed. " + InconclusiveMessage))
				{
					return;
				}
				throw;
			}
		}

		[TestMethod]
		public void LoadMesh()
		{
			IgnoreFileNotFoundException(Test);
			void Test()
			{
				var path = Path.Combine(StormworksPaths.rom, testMeshPath);
				Binary.LoadMesh(path);
			}
		}

		[TestMethod]
		public void LoadPhys()
		{
			IgnoreFileNotFoundException(Test);
			void Test()
			{
				var path = Path.Combine(StormworksPaths.rom, testPhysPath);
				Binary.LoadPhys(path);
			}
		}


		[TestMethod]
		public void MeshRejectPhys()
		{
			ExpectFileTypeMismatch(Test);
			void Test()
			{
				var path = Path.Combine(StormworksPaths.rom, testPhysPath);
				Binary.LoadMesh(path);
			}
		}

		[TestMethod]
		public void PhysRejectMesh()
		{
			ExpectFileTypeMismatch(Test);
			void Test()
			{
				var path = Path.Combine(StormworksPaths.rom, testMeshPath);
				Binary.LoadPhys(path);
			}
		}

		[TestMethod]
		public void RoundTripMesh()
		{
			IgnoreFileNotFoundException(Test);
			void Test()
			{
				var path = Path.Combine(StormworksPaths.rom, testMeshPath);

				using var truthStream = File.OpenRead(path);
				using var writeStream = new MemoryStream();

				Mesh mesh = Binary.LoadMesh(truthStream);
				truthStream.Position = 0;

				Binary.Save(writeStream, mesh);

				writeStream.Position = 0;

#if DEBUG
				long truthLength = truthStream.Length;
				long writeLength = writeStream.Length;
#endif

				Assert.IsTrue(comparer.AreEqualAsync(truthStream, writeStream).Result, "Truth and written back streams are not equal.");
			}
		}

		[TestMethod]
		public void RoundTripPhys()
		{
			IgnoreFileNotFoundException(Test);
			void Test()
			{
				var path = Path.Combine(StormworksPaths.rom, testPhysPath);

				using var truthStream = File.OpenRead(path);
				using var writeStream = new MemoryStream();

				Phys phys = Binary.LoadPhys(truthStream);
				truthStream.Position = 0;

				Binary.Save(writeStream, phys);

				writeStream.Position = 0;

#if DEBUG
				long truthLength = truthStream.Length;
				long writeLength = writeStream.Length;
#endif

				Assert.IsTrue(comparer.AreEqualAsync(truthStream, writeStream).Result, "Truth and written back streams are not equal.");
			}
		}

		[TestMethod]
		public void ConvertMeshToPhysWithIndices()
		{
			IgnoreFileNotFoundException(Test);
			void Test()
			{
				var path = Path.Combine(StormworksPaths.rom, testMeshPath);
				Mesh mesh = Binary.LoadMesh(path);
				Phys phys = Binary.ConvertToPhys(mesh);
			}
		}

		[TestMethod]
		public void ConvertMeshToPhysWithIndices_Then_OrderedVertices()
		{
			IgnoreFileNotFoundException(Test);
			void Test()
			{
				var path = Path.Combine(StormworksPaths.rom, testMeshPath);
				Mesh mesh = Binary.LoadMesh(path);
				Phys phys = Binary.ConvertToPhys(mesh);
				phys.ConvertToOrderedVertices();
			}
		}

		[TestMethod]
		public void ConvertMeshToPhysWithOrderedVertices()
		{
			IgnoreFileNotFoundException(Test);
			void Test()
			{
				var path = Path.Combine(StormworksPaths.rom, testMeshPath);
				Mesh mesh = Binary.LoadMesh(path);
				Phys phys = Binary.ConvertToVertexOnly(mesh);
			}
		}

		#region MapBin

		const string testFile_mapBin = "BinaryDataModel/TestData/mega_island_12_6_map_geometry.bin";
		[TestMethod]
		public void LoadMapBin()
		{
			var path = Path.Combine(Environment.CurrentDirectory, testFile_mapBin);
			var mapBin = Binary.LoadMapBin(testFile_mapBin);
		}

		[TestMethod]
		public void RoundTripMapBin()
		{
			using var truthStream = File.OpenRead(testFile_mapBin);
			using var writeStream = new MemoryStream();

			var mapBin = Binary.LoadMapBin(truthStream);
			truthStream.Position = 0;

			Binary.Save(writeStream, mapBin);

			writeStream.Position = 0;

#if DEBUG
			long truthLength = truthStream.Length;
			long writeLength = writeStream.Length;
#endif

			Assert.IsTrue(comparer.AreEqualAsync(truthStream, writeStream).Result, "Truth and written back streams are not equal.");
		}

		#endregion MapBin
	}
}
