﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/


using System;

using LuaIntegration;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UTests.Lua
{
	[TestClass]
	public class A_Initialization
	{
		[TestMethod]
		public void A0_SandboxVersion()
		{
			try
			{
				using var lua = CurveSpecLoader.GetLuaState();

				string version = (string)lua.DoString("return SandBoxVersion", "VersionCheck_Expr")[0];
				Console.WriteLine($"SandBoxVersion: {version}");

				if (version != CurveSpecLoader.SandBoxVersion)
				{
					Assert.Fail
						(
						 "Rebuild is needed on LuaIntegration: sandbox.lua wasn't updated inside the resources of the .dll."
						);
				}
			}
			catch (DllNotFoundException)
			{
				if (Helpers.CI_Preset)
				{
					Assert.Inconclusive("CI seems to have a bug where the Lua dl is not found.");
				}
				else
				{
					throw;
				}
			}
		}
	}
}
