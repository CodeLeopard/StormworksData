﻿// Copyright 2023 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using BinaryDataModel.DataTypes;
using BinaryDataModel.DataTypes.Enums;
using Extrusion.Support;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace UTests.Extrusion
{
	[TestClass]
	public class MeshOrPhysSharingTest
	{
		private void Assertions<TA, TB>(TA a, TB b)
			where TA : MeshOrPhysSharing
			where TB : MeshOrPhysSharing
		{
			//Assert.AreEqual(a, b);
			//Assert.AreEqual(b, a);

			Assert.IsTrue(a.Equals(b));
			Assert.IsTrue(b.Equals(a));


			Assert.IsTrue(((object) a).Equals(b));
			Assert.IsTrue(((object) b).Equals(a));

			Assert.IsTrue(a == b);
			Assert.IsFalse(a != b);

			Assert.AreEqual(a.GetHashCode(), b.GetHashCode());
		}

		[TestMethod]
		public void MeshSharing_EqualityTest_0()
		{
			var a = new MeshSharing();
			var b = new MeshSharing();

			Assertions<MeshSharing, MeshSharing>(a, b);
			Assertions<MeshOrPhysSharing, MeshOrPhysSharing>(a, b);
		}

		[TestMethod]
		public void MeshSharing_EqualityTest_1()
		{
			var a = new MeshSharing();
			a.SharedMesh = MeshOrPhysSharing.ParentSharedID;
			a.SharedSubMesh = MeshOrPhysSharing.ParentSharedID;
			a.Shader = Shader.Opaque;

			var b = new MeshSharing();
			b.SharedMesh = MeshOrPhysSharing.ParentSharedID;
			b.SharedSubMesh = MeshOrPhysSharing.ParentSharedID;
			b.Shader = Shader.Opaque;

			Assertions<MeshSharing, MeshSharing>(a, b);
			Assertions<MeshOrPhysSharing, MeshOrPhysSharing>(a, b);
		}

		[TestMethod]
		public void MeshSharing_EqualityTest_2()
		{
			var a = new MeshSharing();
			a.SharedMesh = MeshOrPhysSharing.ParentSharedID;
			a.SharedSubMesh = null;
			a.Shader = Shader.Opaque;

			var b = new MeshSharing();
			b.SharedMesh = MeshOrPhysSharing.ParentSharedID;
			b.SharedSubMesh = null;
			b.Shader = Shader.Opaque;

			Assertions<MeshSharing, MeshSharing>(a, b);
			Assertions<MeshOrPhysSharing, MeshOrPhysSharing>(a, b);
		}

		[TestMethod]
		public void MeshSharing_EqualityTest_3()
		{
			var a = new MeshSharing();
			a.SharedMesh = null;
			a.SharedSubMesh = MeshOrPhysSharing.ParentSharedID;
			a.Shader = Shader.Opaque;

			var b = new MeshSharing();
			b.SharedMesh = null;
			b.SharedSubMesh = MeshOrPhysSharing.ParentSharedID;
			b.Shader = Shader.Opaque;

			Assertions<MeshSharing, MeshSharing>(a, b);
			Assertions<MeshOrPhysSharing, MeshOrPhysSharing>(a, b);
		}
	}
}
