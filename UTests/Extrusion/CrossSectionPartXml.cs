﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

using Extrusion;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using OpenToolkit.Mathematics;

namespace UTests.TestExtrusion
{
	[TestClass]
	public class CrossSectionPartXml
	{
		[TestMethod]
		public void Trivial_Write_Read()
		{
			Assert.Inconclusive("Test not implemented after breaking changes.");
			throw new NotImplementedException();
			/*
			using var stream = new MemoryStream();
			try
			{
				var part = new CurveSpec();
				part.Vertices = new Vector2[] { new Vector2(1, 2), new Vector2(3, 4), new Vector2(5, 6) };
				part.LineIndices = new UInt16[] { 0, 1, 2 };
				part.AutoGenerateNormals = true;


				part.SaveToXmlStream(stream);

				stream.Position = 0;

				var part2 = CurveSpec.LoadFromXmlStream(stream);

				Assert.That.SequenceEquals(part.Vertices,    part2.Vertices);
				Assert.That.SequenceEquals(part.LineIndices, part2.LineIndices);
				Assert.AreEqual(part.AutoGenerateNormals, part2.AutoGenerateNormals);
				Assert.AreEqual(part2.Vertices.Length, part2.Normals.Length);
				Assert.That.NotZero(part2.Normals);
			}
			catch
			{
				stream.Position = 0;
				using var reader = new StreamReader(stream);

				Console.WriteLine(reader.ReadToEnd());

				throw;
			}
			*/
		}
	}
}
