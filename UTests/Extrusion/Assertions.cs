﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using OpenToolkit.Mathematics;

namespace UTests.TestExtrusion
{
	public static class Assertions
	{

		public static void SequenceEquals<TValue>(this Assert instance, IEnumerable<TValue> a, IEnumerable<TValue> b)
		{
			bool result = a.SequenceEqual(b);

			Assert.IsTrue(result, "Sequences ware not equal.");
		}

		public static void NotZero(this Assert instance, IEnumerable<Vector2> enumerable)
		{
			foreach (Vector2 v in enumerable)
			{
				Assert.AreNotEqual(Vector2.Zero, v);
			}
		}
	}
}
