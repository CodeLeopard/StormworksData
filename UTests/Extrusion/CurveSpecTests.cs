﻿// Copyright 2023 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/


using BinaryDataModel.DataTypes;
using CurveGraph;
using Extrusion;
using Extrusion.Extrusion;
using Extrusion.Generator;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenToolkit.Mathematics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UTests.Extrusion
{
	[TestClass]
	public class CurveSpecTests
	{
		[TestMethod]
		public void Clone()
		{
			/*
				This test checks that CurveSpec.Clone() (and by extension also the types inside the CurveSpec's Clone)
				works properly -> generates an identical mesh.
			*/

			var spec = new CurveSpec();
			spec.MaxSegmentLength = 1_000;

			var e0 = new ExtrusionSpec();

			spec.Extrusions.Add(e0);
			{
				var loopSet = new LoopSet();
				var loop = new Loop();

				var r = new VertexRecord();
				r.color = Color4.White;
				loop.Vertices.Add(r);

				r.position = new Vector3(1, 0, 0);
				loop.Vertices.Add(r);

				loopSet.Loops.Add(loop);

				loopSet.LineIndices.Add(0);
				loopSet.LineIndices.Add(1);

				e0.LoopSets.Add(loopSet);
			}

			// todo: periodics

			var spec2 = spec.Clone();
			spec2.Transform.LocalPosition = new Vector3(0, -1, 0);

			spec.SubSpecs.Add(spec2);

			var n0 = new CurveNode();
			var n1 = new CurveNode(new Vector3(100, 0, 0));

			var connection = n0.CreateConnection(n1);
			var d = new CurveDecorator();

			var m = d.Generate(connection.curve, spec, "original");

			var clone = spec.Clone();

			var cd = new CurveDecorator();
			var cm = cd.Generate(connection.curve, clone, "clone");

			{
				var mesh = m.Meshes.First().Mesh;
				var cmesh = cm.Meshes.First().Mesh;

				Assert.AreEqual(mesh.indices.Count, cmesh.indices.Count);
				Assert.AreEqual(mesh.vertices.Count, cmesh.vertices.Count);

				for(int i = 0; i < mesh.indices.Count; i++)
				{
					var a = mesh.indices[i];
					var b = cmesh.indices[i];

					Assert.AreEqual(a, b);
				}

				for (int i = 0; i < mesh.vertices.Count; i++)
				{
					var a = mesh.vertices[i];
					var b = cmesh.vertices[i];

					Assert.AreEqual(a, b);
				}
			}


			var ce0 = clone.Extrusions[0];
			Assert.AreEqual(e0.Enabled, ce0.Enabled);
			//Assert.AreEqual(e0.Transform.Parent, ce0.Transform.Parent);
			Assert.AreEqual(e0.Transform.Transform, ce0.Transform.Transform);
			Assert.AreEqual(e0.Transform.AnchorTransform, ce0.Transform.AnchorTransform);

			Assert.AreEqual(e0.MeshSharing, ce0.MeshSharing);
			Assert.AreEqual(e0.PhysSharing, ce0.PhysSharing);
		}
	}
}
