﻿// Copyright 2023 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using BinaryDataModel.DataTypes;
using CurveGraph;
using Extrusion;
using Extrusion.Extrusion;
using Extrusion.Generator;
using Extrusion.Support;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenToolkit.Mathematics;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace UTests.Extrusion
{
	[TestClass]
	public class Regression_ShareIdChangedWhileUsedAskey
	{
		[TestMethod]
		public void Test_1()
		{
			var n0 = new CurveNode();
			var n1 = new CurveNode(new Vector3(1_000, 0, 0));

			var connection = n0.CreateConnection(n1);

			var spec = new CurveSpec();
			spec.MaxSegmentLength = 10;

			var e = new ExtrusionSpec();

			var iSet = new InterpolationSettings();
			iSet.MaxInterpolationDistance = 5;
			iSet.MinInterpolationDistance = 1;

			spec.Extrusions.Add(e);

			var loopSet = new LoopSet();
			var loop = new Loop();

			var r = new VertexRecord();
			r.color = Color4.White;
			loop.Vertices.Add(r);

			r.position = new Vector3(1, 0, 0);
			loop.Vertices.Add(r);

			loopSet.Loops.Add(loop);

			loopSet.LineIndices.Add(0);
			loopSet.LineIndices.Add(1);

			e.LoopSets.Add(loopSet);


			var options = new ParallelOptions();
			options.MaxDegreeOfParallelism = 2;
			Parallel.For(0, 1_000, options, ParallelAction);
			void ParallelAction(int i)
			{
				// There was in the CurveDecorator where it uses a MeshOrPhysSharing as a dictionary key, and also updates fields on that key.
				// When multithreading and sharing that MeshOrPhysSharing between multiplem instances that can go wrong.
				// This regression test should trigger if that bug returns.

				var decorator = new CurveDecorator();
				var results = decorator.Generate(connection.curve, spec, "test");

				Assert.AreEqual(101, results.Meshes.Count());
				Assert.AreEqual(101, results.Physes.Count());
			}
		}
	}
}
