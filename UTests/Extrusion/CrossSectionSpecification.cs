﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

using Extrusion;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using OpenToolkit.Mathematics;

using Shared;

namespace UTests.TestExtrusion
{
	[TestClass]
	public class CrossSectionSpecification
	{

		private void AssertHasDataForTests()
		{
			if(! Directory.Exists(StormworksPaths.creatorToolkitData))
				Assert.Inconclusive("Folder with data not found.");
		}


		[TestMethod]
		public void Trivial_Write_Read()
		{
			Assert.Inconclusive("Test not implemented after breaking changes.");
			throw new NotImplementedException();
			/*
			var part = new CurveSpec();
			part.Transform.LocalPosition = new Vector3(42, 69, 0);

			using var stream = new MemoryStream();

			//part.SaveToXmlStream(stream);

			stream.Position = 0;

			var part2 = CurveSpec.LoadFromXmlStream(stream);

			Assert.AreEqual(part.Transform.LocalPosition, part2.Transform.LocalPosition);
			*/
		}


		[TestMethod]
		public void LoadAll()
		{
			Assert.Inconclusive("Test not implemented after breaking changes.");
			throw new NotImplementedException();
			/*
			AssertHasDataForTests();

			var result = CurveSpec.LoadAllXmlInFolder
				("CSSpec", StormworksPaths.creatorToolkitData, searchOption: SearchOption.AllDirectories);

			if(! result.GetEnumerator().MoveNext()) Assert.Inconclusive("Didn't find any files to test.");
			*/
		}

	}
}
