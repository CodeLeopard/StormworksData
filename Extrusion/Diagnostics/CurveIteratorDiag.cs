﻿// Copyright 2022-2023 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Text;

using CurveGraph;
using Extrusion.Generator;
using Extrusion.Support;

namespace Extrusion.Diagnostics
{
	public class CurveIteratorDiag : DiagBase
	{
		public CurveIterator MyIterator { get; private set; }

		internal void Init(CurveIterator iterator)
		{
			MyIterator = iterator;
			Initialize?.Invoke();
		}

		/// <summary>
		/// Called when the <see cref="CurveIteratorDiag"/> is attached to a <see cref="CurveIterator"/>.
		/// </summary>
		public Action Initialize;

		/// <summary>
		/// Called when Iteration starts.
		/// </summary>
		public Action Start;

		/// <summary>
		/// Called for each iteration,
		/// when the candidate sample has been computed, but not yet checked.
		/// </summary>
		public Action MainIteration;

		/// <summary>
		/// Called when a sample is checked.
		/// Note: enabling this diagnostic will cause all checks to run (no short-circuit).
		/// </summary>
		public Action<CurveIterator.Checks> Check_Result;

		/// <summary>
		/// The angle computed by Check_Angle (in degrees). The value must be less than <see cref="InterpolationSettings.MaxAngleBetweenInterpolations"/> (as provided by: <see cref="CurveIterator.CurrentSettings"/>).
		/// </summary>
		public Action<float> Check_Angle;

		/// <summary>
		/// The distance computed by Check_MidpointOffset. The value must be less than <see cref="InterpolationSettings.MaxSegmentOffset"/> (as provided by: <see cref="CurveIterator.CurrentSettings"/>).
		/// </summary>
		public Action<float> Check_SegmentOffset;

		/// <summary>
		/// The angle computed by Check_MidpointOffset (in degrees). The value must be less than <see cref="InterpolationSettings.MaxSegmentCenterAngle"/> (as provided by: <see cref="CurveIterator.CurrentSettings"/>).
		/// </summary>
		public Action<float> Check_SegmentCenterAgnle;


		/// <summary>
		/// Called when the candidate sample did not pass the test
		/// and a Binary Search will be performed to find a passing sample.
		/// </summary>
		public Action Search_Start;

		/// <summary>
		/// Called for each iteration in the search,
		/// when the candidate sample has been computed but not yet checked.
		/// </summary>
		public Action Search_Iteration;

		/// <summary>
		/// Called when the search minimum is set to the pivot.
		/// This means that the CandidateSample passed the test.
		/// </summary>
		public Action Search_Min;
		/// <summary>
		/// Called when the search maximum is set to the pivot.
		/// This means that the CandidateSample failed the test.
		/// </summary>
		public Action Search_Max;

		/// <summary>
		/// Called when the search is done and the result sample is adjusted to MinDistance.
		/// </summary>
		public Action Search_FinalAdjust;

		/// <summary>
		/// Called when the search is done.
		/// </summary>
		public Action Search_End;

		/// <summary>
		/// Called just before a <see cref="CurveSample"/> is <see langword="yield"/>ed.
		/// Is called regardless of what case caused the yield.
		/// </summary>
		public Action<CurveSample> Yield;

		/// <summary>
		/// Called when AdvanceCandidate resulted in a valid candidate at the end of the curve.
		/// Called after <see cref="Yield"/> but before the actual <see langword="yield"/>.
		/// </summary>
		public Action Yield_Done;

		/// <summary>
		/// Called when AdvanceCandidate resulted in a sample that CheckNestSampleFits stretched to the end.
		/// Note that due to implementation reasons this is called after that yield.
		/// </summary>
		public Action Yield_Done_Stretched;

		/// <summary>
		/// Called when the sample after search is the final sample somehow.
		/// This could happen when CheckNextSampleFits needed to stretch it.
		/// </summary>
		public Action Yield_Done_Searched_Stretched;

		public delegate void FinalSegmentConstraintDelegate
		(
			float stretchDistance,
			float shrinkDistance,
			float stretchedTooLong,
			float shrinkedTooShort
		);

		/// <summary>
		/// FinalSegmentConstrain check shrunk the current segment in order to
		/// </summary>
		public FinalSegmentConstraintDelegate FinalSegmentConstraint;

		/// <summary>
		/// FinalSegmentConstrain check shrunk the current segment in order to
		/// </summary>
		public Action FinalSegmentConstraint_Stretched;

		/// <summary>
		/// FinalSegmentConstrain check shrunk the current segment in order to
		/// </summary>
		public Action FinalSegmentConstraint_Shrunk;

		/// <summary>
		/// Called when the Extrusion is done.
		/// </summary>
		public Action Done;

		/// <summary>
		/// Called when the Iteration is disposed of (done or crashed).
		/// </summary>
		public Action Finally;


		public CurveIteratorDiag()
		{

		}

		public CurveIteratorDiag(StringBuilder sb)
			: base(sb)
		{

		}

		public static CurveIteratorDiag LogAll(StringBuilder sb)
		{
			var r = sb == null ? new CurveIteratorDiag() : new CurveIteratorDiag(sb);
			r.Start = r._Start;
			r.MainIteration = r._MainIteration;

			r.Check_Result = r._Check_Result;
			r.Check_Angle = r._Check_Angle;
			r.Check_SegmentOffset = r._Check_SegmentOffset;
			r.Check_SegmentCenterAgnle = r._Check_SegmentCenterAngle;

			r.Search_Start = r._Search_Start;
			r.Search_Iteration = r._Search_Iteration;
			r.Search_Min = r._Search_Min;
			r.Search_Max = r._Search_Max;
			r.Search_FinalAdjust = r._Search_FinalAdjust;
			r.Search_End = r._Search_End;

			r.Yield = r._Yield;
			r.Yield_Done = r._Yield_Done;
			r.Yield_Done_Stretched = r._Yield_Done_Stretched;
			r.Yield_Done_Searched_Stretched = r._Yield_Done_Searched_Stretched;

			r.FinalSegmentConstraint = r._FinalSegmentConstraint;
			r.FinalSegmentConstraint_Stretched = r._FinalSegmentConstraint_Stretched;
			r.FinalSegmentConstraint_Shrunk = r._FinalSegmentConstraint_Shrunk;

			r.Done = r._Done;

			r.Finally = r._Finally;

			return r;
		}

		#region Default Implementations

		public void _Start()
		{
			WriteLine($"Starting Iteration, yielding initial sample #0.");
		}

		public void _MainIteration()
		{
			WriteLine($"Iteration #{MyIterator.CurrentStep}");
		}

		public void _Check_Result(CurveIterator.Checks result)
		{
			WriteLine($"Check: Rejected tests: {result}");
		}

		public void _Check_Angle(float angle_deg)
		{
			float limit = MyIterator.CurrentSettings.MaxAngleBetweenInterpolations;
			bool valid = angle_deg < limit;

			WriteLine($"Check_Angle: ({angle_deg} < {limit}) = {valid}");
		}

		public void _Check_SegmentOffset(float distance)
		{
			float limit = MyIterator.CurrentSettings.MaxSegmentOffset;
			bool valid = distance < limit;
			WriteLine($"Check_SegmentOffset: ({distance} < {limit}) = {valid}");
		}

		public void _Check_SegmentCenterAngle(float angle_deg)
		{
			float limit = MyIterator.CurrentSettings.MaxSegmentCenterAngle;
			bool valid = angle_deg < limit;
			WriteLine($"Check_SegmentCenterAngle: ({angle_deg} < {limit}) = {valid}");
		}


		public void _Search_Start()
		{
			WriteLine($"Search: Maximum distance sample at {MyIterator.SearchFromSample.distanceInCurve} not accepted, searching for best distance sample.");
		}

		public void _Search_Iteration()
		{
			WriteLine($"Search: iteration #{MyIterator.SearchIteration} at {MyIterator.CandidateSample.distanceInCurve}.");
		}

		public void _Search_Min()
		{
			WriteLine($"Search: Candidate Accepted, adjusting search region to"
					+ $" [{MyIterator.SearchMinDistance} <> {MyIterator.SearchMaxDistance}].");
		}

		public void _Search_Max()
		{
			WriteLine($"Search: Candidate Rejected, adjusting search region to"
					+ $" [{MyIterator.SearchMinDistance} <> {MyIterator.SearchMaxDistance}].");
		}
		public void _Search_FinalAdjust()
		{
			WriteLine($"Search: Adjusting result sample to {MyIterator.CandidateSample.distanceInCurve} because candidate did not pass.");
		}
		public void _Search_End()
		{
			//WriteLine($"Search: ");
		}
		public void _Yield(CurveSample sample)
		{
			WriteLine($"Yield: length: {MyIterator.CurrentSegmentLength} {sample}");
		}
		public void _Yield_Done()
		{
			WriteLine($"Break: Reached end of the curve.");
		}
		public void _Yield_Done_Stretched()
		{
			_Yield_Done();
			//WriteLine($"Break: Reached the end of the curve after Advance, stretching the final segment to cover a gap where the minimum length segment would not fit.");
		}
		public void _Yield_Done_Searched_Stretched()
		{
			_Yield_Done();
			//WriteLine($"Break: Reached the end of the curve after Search, stretching the final segment to cover a gap where the minimum length segment would not fit.");
		}

		public void _FinalSegmentConstraint(float stretchDistance, float shrinkDistance, float stretchedTooLong, float shrinkedTooShort)
		{
			WriteLine($"FinalSegmentConstraint: Required stretch: {stretchDistance}, shrink: {shrinkDistance}"
					+ $" Constrain violation: stretch: {stretchedTooLong}, shrink: {shrinkedTooShort} +bias: {shrinkedTooShort * MyIterator.CurrentSettings.ShrinkReluctance}.");
		}

		public void _FinalSegmentConstraint_Stretched()
		{
			WriteLine($"FinalSegmentConstraint: Stretching to the end to prevent too short final segment.");
		}

		public void _FinalSegmentConstraint_Shrunk()
		{
			WriteLine($"FinalSegmentConstraint: Shrinking to allow another segment between this sample and the final one.");
		}

		public void _Done()
		{
			WriteLine($"Iteration completed after {MyIterator.CurrentStep} steps.");
		}

		private void _Finally()
		{
			if (!TeeToConsoleImmediately)
			{
				Console.WriteLine(sb);
			}

			Clear();
		}

		#endregion Default Implementations
	}
}
