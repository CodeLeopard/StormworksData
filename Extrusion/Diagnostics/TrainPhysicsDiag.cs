﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/


using System;
using System.Collections.Generic;
using System.Text;

using Extrusion.Generator;

namespace Extrusion.Diagnostics
{
	internal class TrainPhysicsDiag : DiagBase
	{
		public TrainPhysicsGenerator MyGenerator { get; private set; }

		/// <summary>
		/// Diagnostics to pass to the <see cref="TrainPhysicsGenerator.CurveIterator"/>.
		/// </summary>
		public CurveIteratorDiag IteratorDiag;

		internal void Init(TrainPhysicsGenerator instance)
		{
			MyGenerator = instance;
			Initialize?.Invoke();
		}


		/// <summary>
		/// Called when the <see cref="ExtruderDiag"/> is attached to a <see cref="Extrusion.Generator.Extruder"/>.
		/// </summary>
		public Action Initialize;


		/// <summary>
		/// Called when the Extrusion is done.
		/// </summary>
		public Action Done;

		/// <summary>
		/// Called when the Extrusion is disposed of (done or crashed).
		/// </summary>
		public Action Finally;

		public TrainPhysicsDiag()
		{

		}

		public TrainPhysicsDiag(StringBuilder sb)
			: base(sb)
		{

		}

		public static TrainPhysicsDiag LogAll(StringBuilder sb = null)
		{
			var r = sb == null ? new TrainPhysicsDiag() : new TrainPhysicsDiag(sb);


			return r;
		}
	}
}
