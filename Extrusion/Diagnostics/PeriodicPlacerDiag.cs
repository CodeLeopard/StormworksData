﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/


using System;
using System.Collections.Generic;
using System.Text;

using CurveGraph;
using Extrusion.Periodic;
using OpenToolkit.Mathematics;

namespace Extrusion.Diagnostics
{
	public class PeriodicPlacerDiag : DiagBase
	{
		public Generator.PeriodicPlacer MyPlacer { get; private set; }
		internal void Init(Generator.PeriodicPlacer instance)
		{
			MyPlacer = instance;
			Initialize?.Invoke();
		}

		/// <summary>
		/// Called when the <see cref="ExtruderDiag"/> is attached to a <see cref="Generator.Extruder"/>.
		/// </summary>
		public Action Initialize;

		/// <summary>
		/// Called before entering the loop.
		/// </summary>
		public Action Start;


		public Action Step;

		public Action<CurveSample> Sample;

		public Action<Matrix4> Transform;
		public Action<Matrix4> FinalTransform;

		public Action<PeriodicItem> SingleItem;

		public Action<PeriodicGroup> Group;

		public Action<MeshSpecification> Mesh;

		public Action Yield;

		/// <summary>
		/// Called when the curve segment is not long enough to fit any placements.
		/// </summary>
		public Action NotLongEnough;

		/// <summary>
		/// Called when all Placements are done.
		/// </summary>
		public Action Done;

		/// <summary>
		/// Called when the Iterator is disposed of (done or crashed).
		/// </summary>
		public Action Finally;


		public PeriodicPlacerDiag()
		{

		}

		public PeriodicPlacerDiag(StringBuilder sb)
			: base(sb)
		{

		}


		/// <summary>
		/// Returns a new <see cref="ExtruderDiag"/> with a default set of loggers attached.
		/// </summary>
		/// <returns></returns>
		public static PeriodicPlacerDiag LogAll(StringBuilder sb = null)
		{
			var r = sb == null ? new PeriodicPlacerDiag() : new PeriodicPlacerDiag(sb);
			r.Start = r._Start;

			r.Step = r._Step;

			r.Sample = r._Sample;
			r.Transform = r._Transform;
			r.FinalTransform = r._FinalTransform;
			r.SingleItem = r._SingleItem;
			r.Group = r._Group;
			r.Mesh = r._Mesh;
			r.Yield = r._Yield;
			r.NotLongEnough = r._NotLongEnough;
			r.Done = r._Done;

			r.Done = r._Done;

			r.Finally = r._Finally;

			return r;
		}


		#region Default Implementations

		public void _Start()
		{
			WriteLine("Starting Periodic Item Placement.");
		}

		public void _Step()
		{
			WriteLine($"Step #{MyPlacer.Step} Item#{MyPlacer.ItemIndex} LocalPos: {MyPlacer.CurrentItem.Transform.LocalPosition}");
		}

		public void _Sample(CurveSample sample)
		{
			WriteLine($"Retrieved Sample at {sample.distanceInCurve}: {sample}");
		}

		public void _Transform(Matrix4 transform)
		{
			var pos = transform.Row3.Xyz;
			WriteLine($"ItemTransform: @{pos}\n{transform}");
		}
		public void _FinalTransform(Matrix4 transform)
		{
			var pos = transform.Row3.Xyz;
			WriteLine($"MeshTransform: @{pos}\n{transform}");
		}
		public void _SingleItem(PeriodicItem item)
		{
			//WriteLine("Starting Generation.");
		}
		public void _Group(PeriodicGroup group)
		{
			//WriteLine("Starting Generation.");
		}
		public void _Mesh(MeshSpecification spec)
		{
			//WriteLine("Starting Generation.");
		}

		public void _Yield()
		{
			WriteLine();
		}

		public void _NotLongEnough()
		{
			WriteLine("Curve not long enough to fit this specification.");
		}
		public void _Done()
		{
			WriteLine("Done placing.");
		}

		public void _Finally()
		{
			if (!TeeToConsoleImmediately)
			{
				Console.WriteLine(sb);
			}

			Clear();
		}

		#endregion Default Implementations

	}
}
