﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Extrusion.Diagnostics
{
	public abstract class DiagBase
	{
		protected readonly StringBuilder sb;

		public bool TeeToConsoleImmediately = false;

		public string LinePrefix = string.Empty;

		protected DiagBase()
		{
			sb = new StringBuilder();
		}

		protected DiagBase(StringBuilder sb)
		{
			this.sb = sb ?? throw new ArgumentNullException(nameof(sb));
		}

		public void Write<TObject>(TObject o)
		{
			Write(o.ToString());
		}

		public void WriteLine<TObject>(TObject o)
		{
			WriteLine(o.ToString());
		}

		public void Write(string o)
		{
			sb.Append(o);

			if (TeeToConsoleImmediately)
				Console.Write(o);
		}

		public void WriteLine(string o)
		{
			sb.Append(LinePrefix);
			sb.AppendLine(o);
			if (TeeToConsoleImmediately)
			{
				Console.Write(LinePrefix);
				Console.WriteLine(o);
			}
		}

		public void WriteLine()
		{
			if (TeeToConsoleImmediately)
				Console.WriteLine();
		}

		public int Length => sb.Length;

		public int Capacity
		{
			get => sb.Capacity;
			set => sb.Capacity = value;
		}

		public void Clear()
		{
			sb.Clear();
		}
	}
}
