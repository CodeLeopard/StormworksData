﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/


using System;
using System.Collections.Generic;
using System.Text;

using BinaryDataModel.DataTypes;

using CurveGraph;
using OpenToolkit.Mathematics;


namespace Extrusion.Diagnostics
{
	/// <summary>
	/// Diagnostics handler for <see cref="Generator.Extruder"/>.
	/// Not suitable for MultiThreaded access, but can be re-used.
	/// </summary>
	public class ExtruderDiag : DiagBase
	{
		public Generator.Extruder MyExtruder { get; private set; }

		/// <summary>
		/// Diagnostics to pass to the <see cref="Extrusion.Generator.CurveIterator"/>.
		/// </summary>
		public CurveIteratorDiag IteratorDiag;

		internal void Init(Generator.Extruder instance)
		{
			MyExtruder = instance;
			Initialize?.Invoke();
		}

		/// <summary>
		/// Called when the <see cref="ExtruderDiag"/> is attached to a <see cref="Extrusion.Generator.Extruder"/>.
		/// </summary>
		public Action Initialize;


		/// <summary>
		/// Called just before entering the loop that will generate the extrusion.
		/// </summary>
		public Action Generate_Begin;


		/// <summary>
		/// Called before a set of vertices is generated to continue a <see cref="LoopSet"/>.
		/// </summary>
		public Action Generate_Vertices;

		/// <summary>
		/// Called before an extra set of vertices is generated to start the next <see cref="LoopSet"/>.
		/// </summary>
		public Action Generate_ExtraVertices;

		/// <summary>
		/// Called for each <see cref="Vertex"/> added.
		/// </summary>
		public Action<VertexRecord, VertexRecord> Generate_Vertex;

		/// <summary>
		/// Called before indices are generated.
		/// </summary>
		public Action Generate_Indices;

		/// <summary>
		/// Called for each index added.
		/// </summary>
		/// <param name="start">The index of the vertex for the start of the line.</param>
		/// <param name="end">The index of the vertex for the end of the line.</param>
		/// <param name="triangle_u">The indices of first generated triangle.</param>
		/// <param name="triangle_v">The indices of second generated triangle.</param>
		public delegate void TrianglePairAction
		(
			uint start
		  , uint end
		  , (uint v0, uint v1, uint v2) triangle_u
		  , (uint v0, uint v1, uint v2) triangle_v
		);
		/// <summary>
		/// Called for each index added.
		/// </summary>
		public TrianglePairAction Generate_TrianglePair;

		/// <summary>
		/// Called just before the <see cref="Extrusion.Generator.Extruder"/> <see langword="yield" />s.
		/// </summary>
		public Action Generate_Yield;

		/// <summary>
		/// Called just before the next <see cref="CurveSample"/> is requested from the <see cref="Extrusion.Generator.CurveIterator"/>.
		/// </summary>
		public Action Generate_EndOfStep;


		/// <summary>
		/// Called just before the next LoopSetIndex and LoopIndex are computed.
		/// </summary>
		public Action Advance_Before;

		#region Advance_Cases
		/// <summary>
		/// Called when the advance step moves to another <see cref="LoopSet"/>.
		/// </summary>
		public Action Advance_NextLoop;

		/// <summary>
		/// Called when the advance step moves to another <see cref="LoopSet"/>.
		/// </summary>
		public Action Advance_NextLoopSet;
		/// <summary>
		/// Called when the advance step restarts the current <see cref="LoopSet"/> (because there is only one).
		/// </summary>
		public Action Advance_OneLoopSet;
		/// <summary>
		/// Called when the advance step wrapped around to the first <see cref="LoopSet"/>.
		/// </summary>
		public Action Advance_ResetLoopSet;
		#endregion Advance_Cases

		/// <summary>
		/// Called just after the next LoopSetIndex and LoopIndex are computed.
		/// Parameter <see langword="true" /> when extra vertices will be generated because switching between LoopSets.
		/// </summary>
		/// <remarks>
		/// This is called for all cases. For individual cases use <see cref="Advance_NextLoopSet"/>, <see cref="Advance_OneLoopSet"/> and <see cref="Advance_ResetLoopSet"/>.
		/// </remarks>
		public Action<bool> Advance_After;

		/// <summary>
		/// Called when the Extrusion is done.
		/// </summary>
		public Action Done;

		/// <summary>
		/// Called when the Extrusion is disposed of (done or crashed).
		/// </summary>
		public Action Finally;


		public ExtruderDiag()
		{

		}

		public ExtruderDiag(StringBuilder sb)
			: base(sb)
		{

		}

		/// <summary>
		/// Returns a new <see cref="ExtruderDiag"/> with a default set of loggers attached.
		/// </summary>
		/// <returns></returns>
		public static ExtruderDiag LogAll(StringBuilder sb = null)
		{
			var r = sb == null ? new ExtruderDiag() : new ExtruderDiag(sb);
			r.Generate_Begin = r._Generate_Begin;

			r.Generate_Vertices = r._Generate_Vertices;
			r.Generate_ExtraVertices = r._Generate_ExtraVertices;
			r.Generate_Vertex = r._Generate_Vertex;

			r.Generate_Indices = r._Generate_Indices;
			r.Generate_TrianglePair = r._Generate_TrianglePair;
			r.Generate_EndOfStep = r._Generate_EndOfStep;

			r.Generate_Yield = r._Generate_Yield;

			r.Advance_NextLoop = r._Advance_NextLoop;
			r.Advance_NextLoopSet = r._Advance_NextLoopSet;
			r.Advance_OneLoopSet = r._Advance_OneLoopSet;
			r.Advance_ResetLoopSet = r._Advance_ResetLoopSet;

			r.Done = r._Done;

			r.Finally = r._Finally;

			return r;
		}


		#region Default Implementations

		public void _Generate_Begin()
		{
			WriteLine("Starting Generation.");
		}

		public void _Generate_Vertices()
		{
			WriteLine();
			WriteLine($"Step #{MyExtruder.CurrentStepIndex}, LoopSet#{MyExtruder.LoopSetIndex}, Loop#{MyExtruder.LoopIndex}");
			WriteLine($"Position: {MyExtruder.CurrentCurveSample.location}, Distance: {MyExtruder.DistanceInCurve}");
			WriteLine("Generating vertices.");
		}
		public void _Generate_ExtraVertices()
		{
			WriteLine("Generating extra vertices.");
		}
		public void _Generate_Vertex(VertexRecord raw, VertexRecord transformed)
		{

		}

		public void _Generate_Indices()
		{
			WriteLine("Generating indices.");
		}

		public void _Generate_TrianglePair(uint start
									  , uint end
									  , (uint v0, uint v1, uint v2) triangle_u
									  , (uint v0, uint v1, uint v2) triangle_v)
		{

		}

		public void _Generate_Yield()
		{
			WriteLine($"Yielding at interpolator step #{MyExtruder.CurrentStepIndex}.");
		}

		public void _Generate_EndOfStep()
		{
			WriteLine($"===============================\n");
		}

		public void _Advance_NextLoop()
		{
			WriteLine($"Advancing to next Loop: {MyExtruder.LoopIndex}.");
		}
		public void _Advance_NextLoopSet()
		{
			WriteLine($"Advancing to next LoopSet: {MyExtruder.LoopSetIndex}.");
		}

		public void _Advance_OneLoopSet()
		{
			WriteLine($"Restarting the only LoopSet.");
		}

		public void _Advance_ResetLoopSet()
		{
			WriteLine("Wrapping round to first LoopSet.");
		}

		public void _Done()
		{
			WriteLine($"Extrusion completed after {MyExtruder.CurrentStepIndex}, Vertices: {MyExtruder.meshData.Vertices.Count}, Indices: {MyExtruder.meshData.Indices.Count}.");
		}

		public void _Finally()
		{
			if (!TeeToConsoleImmediately)
			{
				Console.WriteLine(sb);
			}

			Clear();
		}

		#endregion Default Implementations

	}
}
