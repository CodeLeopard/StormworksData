﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/


using System;
using System.Collections.Generic;
using System.Text;

namespace Extrusion.Periodic
{
	/// <summary>
	/// A defined group of <see cref="PeriodicItem"/> that are placed together at the same spot along the curve.
	/// </summary>
	public class PeriodicGroup : PeriodicItemBase
	{
		/// <summary>
		/// The <see cref="MeshSpecification"/>s to be placed.
		/// </summary>
		public readonly List<MeshSpecification> Entries = new List<MeshSpecification>();


		public void UpdateParentHierarchy()
		{
			foreach (MeshSpecification periodicItem in Entries)
			{
				periodicItem.Transform.Parent = Transform;
			}
		}

		public override PeriodicItemBase Clone()
		{
			var r = new PeriodicGroup();
			Clone_Into(r);
			foreach (MeshSpecification item in Entries)
			{
				r.Entries.Add(item.Clone());
			}

			return r;
		}
	}
}
