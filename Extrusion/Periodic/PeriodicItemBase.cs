﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/


using Extrusion.Support;
using System;
using System.Collections.Generic;
using System.Text;

using Shared.Validation;

namespace Extrusion.Periodic
{
	/// <summary>
	/// Defines a mesh is to be placed periodically along a <see cref="CurveGraph.CubicBezierCurve"/>.
	/// </summary>
	public abstract class PeriodicItemBase : IValidateable
	{
		// todo: should this have it's own MeshSharing?


		public Transformation Transform = Transformation.Identity;

		/// <summary>
		/// If the distance (in meters) between the previous and this element is less than this value,
		/// prevent it from being placed.
		/// </summary>
		public float MinDistance = 0f;

		/// <summary>
		/// The distance in meters between the previous and this element.
		/// </summary>
		public float IdealDistance = 1f;

		/// <inheritdoc />
		public virtual void Validate(Validator v)
		{
			v.Validate(Transform, nameof(Transform));

			v.ValidateFloatFinite(MinDistance, nameof(MinDistance));
			v.Error(MinDistance <= 0, $"{nameof(MinDistance)} should be > 0.");

			v.ValidateFloatFinite(IdealDistance, nameof(IdealDistance));
			v.Error(IdealDistance <= 0, $"{nameof(IdealDistance)} should be > 0.");
			v.Warning(IdealDistance < MinDistance, $"{nameof(IdealDistance)} should be > {nameof(MinDistance)}.");
		}

		protected void Clone_Into(PeriodicItemBase realTypedInstance)
		{
			var r = realTypedInstance;
			r.Transform = Transform.Clone();
			r.MinDistance = MinDistance;
			r.IdealDistance = IdealDistance;
		}

		public abstract PeriodicItemBase Clone();
	}
}
