﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/


using System;
using System.Collections.Generic;
using System.Text;

using BinaryDataModel.DataTypes;

using Extrusion.Support;
using OpenToolkit.Mathematics;

using Shared.Validation;

namespace Extrusion.Periodic
{
	/// <summary>
	/// Specifies how to place meshes along a <see cref="CurveGraph.CubicBezierCurve"/>.
	/// </summary>
	public class PeriodicItemSpec : IValidateable
	{
		/// <summary>
		/// Allow disabling without having to destroy the configuration itself.
		/// </summary>
		public bool Enabled = true;

		public Transformation Transform = Transformation.Identity;


		/// <summary>
		/// Generate a visual mesh data for this Item.
		/// </summary>
		public bool Visual
		{
			get => MeshSharing != null;
			set
			{
				if (value)
				{
					MeshSharing = MeshSharing ?? MeshSharing.Default;
				}
				else
				{
					MeshSharing = null;
				}
			}
		}

		/// <summary>
		/// Generate physics mesh data for this Item.
		/// </summary>
		public bool Physics
		{
			get => PhysSharing != null;
			set {
				if (value)
				{
					PhysSharing = PhysSharing ?? PhysSharing.Default;
				}
				else
				{
					PhysSharing = null;
				}
			}
		}

		/// <summary>
		/// How the <see cref="Mesh"/> data should be shared between parts of the <see cref="CurveSpec"/>.
		/// When <see langword="null"/> no <see cref="Mesh"/> data will be generated.
		/// </summary>
		public MeshSharing MeshSharing = MeshSharing.Default;


		/// <summary>
		/// How the <see cref="Phys"/> data should be shared between parts of the <see cref="CurveSpec"/>.
		/// When <see langword="null"/> no <see cref="Phys"/> data will be generated.
		/// </summary>
		public PhysSharing PhysSharing = PhysSharing.Default;


		/// <summary>
		/// The initial offset, in terms of <see cref="PeriodicItem.IdealDistance"/> (total over all items)
		/// from the start of the curve that the first element will be placed.
		/// This ensures that consecutive curves have sensible distance between placements between curves.
		/// </summary>
		public float InitialOffset = 0.5f;

		/// <summary>
		/// Allow changing <see cref="PeriodicItem.IdealDistance"/> (on all items) to fit an exact integer amount inside the length of the curve.
		/// </summary>
		public bool Smudge = true;

		/// <summary>
		/// Require a full sequence to always be placed.
		/// If it cannot be placed then place nothing at all.
		/// <seealso cref="PeriodicItem.MinDistance"/>.
		/// </summary>
		public bool AlwaysFullSequence = true;

		/// <summary>
		/// During Extrusion periodicItems will be placed in the order they appear.
		/// </summary>
		public readonly List<PeriodicItemBase> Items = new List<PeriodicItemBase>();

		public void UpdateParentHierarchy()
		{
			foreach (PeriodicItemBase periodicItem in Items)
			{
				periodicItem.Transform.Parent = Transform;
			}
		}

		/// <inheritdoc />
		public void Validate(Validator v)
		{
			v.Validate(Transform,   nameof(Transform));
			v.Validate(MeshSharing, nameof(MeshSharing), allowNull: true);
			v.Validate(PhysSharing, nameof(PhysSharing), allowNull: true);
			v.ValidateFloatFinite(InitialOffset, nameof(InitialOffset));
			v.Error(InitialOffset < 0 || InitialOffset > 1.0f
			      , $"{nameof(InitialOffset)} should be >= 0 and <= 1");

			v.WarnIfEmpty(Items, nameof(Items));
		}

		public PeriodicItemSpec Clone()
		{
			var r = new PeriodicItemSpec();
			r.Enabled = Enabled;
			r.Transform = Transform.Clone();
			r.MeshSharing = MeshSharing?.Clone();
			r.PhysSharing = PhysSharing?.Clone();
			r.InitialOffset = InitialOffset;
			r.Smudge = Smudge;
			r.AlwaysFullSequence = AlwaysFullSequence;

			foreach (PeriodicItemBase item in Items)
			{
				r.Items.Add(item.Clone());
			}

			return r;
		}
	}
}
