﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/


using System;
using System.Collections.Generic;
using System.Text;

using BinaryDataModel;
using BinaryDataModel.DataTypes;

using Extrusion.Support;

using OpenToolkit.Mathematics;

using Shared;
using Shared.Validation;

namespace Extrusion.Periodic
{
	public class MeshSpecification : IValidateable
	{
		// todo: should this have it's own MeshSharing?

		/// <summary>
		/// The Transformation, relative to the position along the curve that has interpolated.
		/// This is only meaningful to use when using <see cref="PeriodicGroup"/>.
		/// </summary>
		public Transformation Transform = Transformation.Identity;


		private Mesh _mesh;

		/// <summary>
		/// The Mesh to be placed.
		/// </summary>
		public Mesh Mesh
		{
			get => _mesh;
			set
			{
				_mesh = value;
				_meshPath = null;
			}
		}

		private string _meshPath;
		/// <summary>
		/// The path, relative to rom from where the <see cref="BinaryDataModel.DataTypes.Mesh"/> should be fetched.
		/// </summary>
		public string MeshPath
		{
			get => _meshPath;
			set
			{
				if (value != _meshPath)
				{
					_meshPath = value;
					Mesh = value == null ? null : MeshCache.GetOrLoadMesh(_meshPath);
				}
			}

		}

		/// <summary>
		/// When defined will override specific colors (see: <see cref="MeshCombiner.paintZoneColors"/>) with the provided ones.
		/// </summary>
		public List<Color4> PaintZones = new List<Color4>();

		/// <summary>
		/// Apply <see cref="PaintZones"/>[0] to all vertices.
		/// </summary>
		public bool ForcePaint = false;

		/// <summary>
		/// Generate <see cref="Phys"/> from <see cref="Mesh"/>.
		/// </summary>
		public bool GeneratePhys = false;

		private Phys _phys;

		/// <summary>
		/// The PhysMesh to place or <see langword="null"/>.
		/// </summary>
		public Phys Phys
		{
			get => _phys;
			set
			{
				_phys = value;
				_physPath = null;
			}
		}

		private string _physPath;
		/// <summary>
		/// The path, relative to rom, from where the <see cref="BinaryDataModel.DataTypes.Phys"/> should be fetched.
		/// </summary>
		public string PhysPath
		{
			get => _physPath;
			set
			{
				if (value != _physPath)
				{
					_physPath = value;
					Phys = value == null ? null : MeshCache.GetOrLoadPhys(_physPath);
				}
			}
		}

		/// <inheritdoc />
		public void Validate(Validator v)
		{
			v.Validate(Transform, nameof(Transform));

			if (null == Mesh && null == Phys)
			{
				v.Warning($"Both {nameof(Mesh)} and {nameof(Phys)} have no value.");
			}

			v.Error(ForcePaint && PaintZones.Count == 0, $"{nameof(ForcePaint)} is specified but {nameof(PaintZones)} is empty.");

			for (int i = 0; i < PaintZones.Count; i++)
			{
				using (_ = v.PushContext(nameof(PaintZones), i))
				{
					var c = PaintZones[i];
					v.Error(! c.ToVector4().IsFinite(), $"Color contains non-finite component(s): {c}");
				}
			}
		}

		/// <summary>
		/// Performs a member wise clone, <see cref="Mesh"/> and <see cref="Phys"/> are copied by reference because they are assumed to be used in read-only manner.
		/// </summary>
		/// <returns></returns>
		public MeshSpecification Clone()
		{
			var result = (MeshSpecification)MemberwiseClone();
			result.Transform = Transform.Clone();
			result.PaintZones = new List<Color4>(PaintZones.Count);
			foreach (var color in PaintZones)
			{
				result.PaintZones.Add(color);
			}

			return result;
		}
	}
}
