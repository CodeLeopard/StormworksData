﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/


using System;
using System.Collections.Generic;
using System.Text;

using BinaryDataModel.DataTypes;


using OpenToolkit.Mathematics;

using Shared.Validation;

namespace Extrusion.Periodic
{
	public class PeriodicItem : PeriodicItemBase
	{
		/// <summary>
		/// Holds the specification for the <see cref="Mesh"/> and/or <see cref="Phys"/>.
		/// </summary>
		public MeshSpecification MeshSpec;

		/// <inheritdoc />
		public override void Validate(Validator v)
		{
			base.Validate(v);

			v.Validate(MeshSpec, nameof(MeshSpec));
		}

		/// <summary>
		/// Clones the instance into a new one.
		/// Members are cloned as appropriate,
		/// except the actual mesh inside <see cref="MeshSpec"/>
		/// which are copied by reference, because their instances are used for reading only.
		/// </summary>
		/// <returns></returns>
		public override PeriodicItemBase Clone()
		{
			var r = new PeriodicItem();
			Clone_Into(r);
			r.MeshSpec = MeshSpec.Clone();
			return r;
		}
	}
}
