﻿// Copyright 2022-2023 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/


using System;
using System.Collections.Generic;
using System.Text;

using Extrusion.Extrusion;
using Extrusion.Periodic;
using Extrusion.Support;
using Extrusion.Train;
using OpenToolkit.Mathematics;

using Shared.Validation;

namespace Extrusion
{
	/// <summary>
	/// The specification for the visual representation of a <see cref="CurveGraph.CubicBezierCurve"/>.
	/// </summary>
	public class CurveSpec : IValidateable
	{
		/// <summary>
		/// A user-defined name for the spec. If not set the fileName will be used (note: that only for the root spec).
		/// </summary>
		public string name;

		public bool Enabled = true;

		public Transformation Transform = Transformation.Identity;

		public MeshSharing MeshSharing = MeshSharing.Default;
		public PhysSharing PhysSharing = PhysSharing.Default;

		/// <summary>
		/// <see langword="true"/> when the <see cref="CurveSpec"/> will generate a (visual) mesh.
		/// </summary>
		public bool WillGenerateMesh
		{
			get
			{
				if (null == MeshSharing) return false;

				bool any = false;


				foreach (var extrusion in Extrusions)
				{
					any |= extrusion.Visual;
				}

				foreach (var periodic in Periodics)
				{
					any |= periodic.Visual;
				}

				foreach (var curveSpec in SubSpecs)
				{
					any |= curveSpec.WillGenerateMesh;
				}

				return any;
			}
		}

		/// <summary>
		/// <see langword="true"/> when the <see cref="CurveSpec"/> will generate a (physics) mesh.
		/// </summary>
		public bool WillGeneratePhys
		{
			get
			{
				if (null == PhysSharing) return false;

				bool any = false;


				foreach (var extrusion in Extrusions)
				{
					any |= extrusion.Physics;
				}

				foreach (var periodic in Periodics)
				{
					any |= periodic.Physics;
				}

				foreach (var curveSpec in SubSpecs)
				{
					any |= curveSpec.WillGeneratePhys;
				}

				return any;
			}
		}


		public const string SnapType_Train_StormworksGauge = "Train/Gauge_SW";

		/// <summary>
		/// Only Specs with the same <see cref="SnapType"/> can snap to this.
		/// If <see langword="null"/> no snapPoint will be created at all.
		/// </summary>
		public string SnapType = null;


		/// <summary>
		/// Arbitrarily decided minimum length for a curve with this <see cref="CurveSpec"/> assigned to it.
		/// </summary>
		public float MinLength = 0;

		/// <summary>
		/// The default value for <see cref="MaxSegmentLength"/> that will be used if none is explicitly assigned.
		/// </summary>
		public const float DefaultMaxSegmentLength = 250f;

		/// <summary>
		/// The Maximum length of resulting extrusion mesh. If the Curve to be extruded is longer it will be split into segments such that each segment is below the length limit.
		/// </summary>
		public float MaxSegmentLength = DefaultMaxSegmentLength;

		/// <summary>
		/// Actual minimum length for a curve with this <see cref="CurveSpec"/> assigned to it.
		/// This value takes into account any minimums specified by members.
		/// </summary>
		public float RecursiveMinLength
		{
			get
			{
				// todo: compute based on members.
				// todo: perhaps cache the value because expensive.
				// todo: but then ensure it is kep up to date.
				throw new NotImplementedException();
			}
		}

		/// <summary>
		/// Extrusions of a crossSection.
		/// </summary>
		public readonly List<ExtrusionSpec> Extrusions = new List<ExtrusionSpec>();

		/// <summary>
		/// Items to be placed along the curve periodically.
		/// </summary>
		public readonly List<PeriodicItemSpec> Periodics = new List<PeriodicItemSpec>();

		/// <summary>
		/// Physics for train wheels.
		/// </summary>
		public readonly List<TrainPhysicsSpec> TrainPhysics = new List<TrainPhysicsSpec>();


		/// <summary>
		/// References to other <see cref="CurveSpec"/>s.
		/// </summary>
		public List<CurveSpec> SubSpecs = new List<CurveSpec>(0);

		/// <summary>
		/// Validate that the <see cref="CurveSpec"/> is valid and will not produce errors when using it to generate decorations.
		/// </summary>
		public void Validate()
		{
			var diag = new Validator();
			using (_ = diag.PushContext($"{nameof(CurveSpec)}"))
			{
				Validate(diag);
			}

			diag.ThrowErrors();
		}

		/// <inheritdoc />
		public void Validate(Validator v)
		{
			v.Validate(Transform,   nameof(Transform));
			v.Validate(MeshSharing, nameof(MeshSharing), allowNull: true);
			v.Validate(PhysSharing, nameof(PhysSharing), allowNull: true);

			v.Warning(MinLength <= 0, $"{nameof(MinLength)} should be > 0 but was {MinLength}");
			v.Warning(MaxSegmentLength <= 0, $"{nameof(MaxSegmentLength)} should be > 0 but was {MaxSegmentLength}");

			v.Validate
				(Extrusions, nameof(Extrusions), emptySeverity: DiagnosticSeverity.None, allowNullElements: false);
			v.Validate(Periodics, nameof(Periodics), emptySeverity: DiagnosticSeverity.None, allowNullElements: false);
			v.Validate
				(TrainPhysics, nameof(TrainPhysics), emptySeverity: DiagnosticSeverity.None, allowNullElements: false);
			v.Validate(SubSpecs, nameof(SubSpecs), emptySeverity: DiagnosticSeverity.None, allowNullElements: false);

			v.Warning
				(
				 Extrusions.Count   == 0
			  && Periodics.Count    == 0
			  && TrainPhysics.Count == 0
			  && SubSpecs.Count     == 0
			   , $"{nameof(CurveSpec)} contains no elements."
				);
		}

		public void UpdateParentHierarchy()
		{
			foreach (ExtrusionSpec extrusionSpec in Extrusions)
			{
				extrusionSpec.Transform.Parent = Transform;
			}

			foreach (PeriodicItemSpec periodicItemSpec in Periodics)
			{
				periodicItemSpec.Transform.Parent = Transform;
				periodicItemSpec.UpdateParentHierarchy();
			}

			foreach (TrainPhysicsSpec trainWheelSpec in TrainPhysics)
			{
				trainWheelSpec.Transform.Parent = Transform;
			}

			foreach (CurveSpec curveSpec in SubSpecs)
			{
				curveSpec.Transform.Parent = Transform;
				curveSpec.UpdateParentHierarchy();
			}
		}

		public CurveSpec Clone()
		{
			var r = new CurveSpec();
			r.Enabled = Enabled;
			r.Transform = Transform.Clone();
			r.MeshSharing = MeshSharing?.Clone();
			r.PhysSharing = PhysSharing?.Clone();
			r.SnapType = SnapType;
			r.MinLength = MinLength;
			r.MaxSegmentLength = MaxSegmentLength;
			foreach (ExtrusionSpec spec in Extrusions)
			{
				r.Extrusions.Add(spec.Clone());
			}
			foreach (PeriodicItemSpec spec in Periodics)
			{
				r.Periodics.Add(spec.Clone());
			}
			foreach (TrainPhysicsSpec spec in TrainPhysics)
			{
				r.TrainPhysics.Add(spec.Clone());
			}
			foreach (CurveSpec spec in SubSpecs)
			{
				r.SubSpecs.Add(spec.Clone());
			}

			return r;
		}

		/// <inheritdoc/>
		public override string ToString()
		{
			return $"{{{nameof(CurveSpec)} '{name ?? "unnamed"}' {(Enabled ? "" : "(disabled) ")}{Extrusions.Count} extrusions, {Periodics.Count} periodics, {TrainPhysics.Count} train-physics, {SubSpecs.Count} subSpecs}}";
		}
	}
}
