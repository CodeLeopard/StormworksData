﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/


using System;
using System.Collections.Generic;
using System.Text;
using Extrusion.Support;
using OpenToolkit.Mathematics;

using Shared.Validation;

namespace Extrusion.Train
{
	/// <summary>
	/// Specification for train wheel physics.
	/// </summary>
	public class TrainPhysicsSpec : IValidateable
	{
		/// <summary>
		/// Allow disabling without having to destroy the configuration itself.
		/// </summary>
		public bool Enabled = true;

		public Transformation Transform = Transformation.Identity;

		public InterpolationSettings InterpolationSettings = InterpolationSettings.Default;

		/// <inheritdoc />
		public void Validate(Validator v)
		{
			v.Validate(InterpolationSettings, nameof(InterpolationSettings));
			v.Validate(Transform, nameof(Transform));
		}

		/// <summary>
		/// Create a clone of the <see cref="TrainPhysicsSpec"/> that can be modified without affecting the original.
		/// </summary>
		/// <returns>A copy of this <see cref="TrainPhysicsSpec"/>.</returns>
		public TrainPhysicsSpec Clone()
		{
			var r = new TrainPhysicsSpec();
			r.Transform = Transform.Clone();
			r.InterpolationSettings = InterpolationSettings.Clone();
			return r;
		}
	}
}
