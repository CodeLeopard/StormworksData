﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/


using System;
using System.Collections.Generic;
using System.Text;

using BinaryDataModel.DataTypes;
using Extrusion.Support;

using OpenToolkit.Mathematics;

using Shared;
using Shared.Validation;

namespace Extrusion.Extrusion
{
	/// <summary>
	/// A loop specifies a single point along the extrusion.
	/// </summary>
	public class Loop : IValidateable
	{
		/// <summary>
		/// Defines the length of the segment between this loop and the next loop.
		/// If there is no next loop it is ignored.
		/// </summary>
		public InterpolationSettings InterpolationSettings = InterpolationSettings.Default;

		/// <summary>
		/// Vertices shared for both Visual and Physics mesh.
		/// </summary>
		public readonly List<VertexRecord> Vertices = new List<VertexRecord>();

		/// <inheritdoc />
		public void Validate(Validator v)
		{
			v.Validate(InterpolationSettings, nameof(InterpolationSettings));

			v.Error(Vertices.Count < 2, $"Loop has not enough vertices, 2 are required but it only has {Vertices.Count}.");

			for(int i = 0; i < Vertices.Count; i++)
			{
				var vertexRecord = Vertices[i];
				v.Warning(vertexRecord.position.HasNan(),
				             $"Encountered non-finite {nameof(VertexRecord.position)} in {nameof(Vertices)} #{i}: {vertexRecord.position}.");

				v.Warning(vertexRecord.normal.HasNan(),
				             $"Encountered non-finite {nameof(VertexRecord.normal)} in {nameof(Vertices)} #{i}: {vertexRecord.normal}.");

				v.Warning(vertexRecord.normal == Vector3.Zero,
				             $"Encountered zero-length {nameof(VertexRecord.normal)} in {nameof(Vertices)} #{i}: {vertexRecord.normal}."
				                                              + $"\nYou may be able to generate normals automatically using {nameof(LoopSet.ComputeNormals)}.");

				v.Warning(vertexRecord.color.ToVector4().HasNan(),
				             $"Encountered non-finite {nameof(VertexRecord.color)} in {nameof(Vertices)} #{i}: {vertexRecord.color}.");
			}
		}

		public Loop Clone()
		{
			var result = new Loop();
			result.InterpolationSettings = InterpolationSettings.Clone();

			result.Vertices.Capacity = Vertices.Count;
			result.Vertices.AddRange(Vertices);
			return result;
		}
	}
}
