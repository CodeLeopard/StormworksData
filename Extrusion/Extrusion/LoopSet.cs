﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/


using System;
using System.Collections.Generic;

using BinaryDataModel.DataTypes;

using OpenToolkit.Mathematics;

using Shared.Validation;

namespace Extrusion.Extrusion
{
	/// <summary>
	/// Loops inside a <see cref="LoopSet"/> are attached to each other according to <see cref="LineIndices"/>.
	/// When switching between <see cref="LoopSet"/> the vertices of both are placed at the same <see cref="CurveGraph.CurveSample"/>.
	/// </summary>
	public class LoopSet : IValidateable
	{
		public readonly List<Loop> Loops = new List<Loop>();

		/// <summary>
		/// LineIndices only for visual mesh.
		/// </summary>
		public readonly List<UInt32> LineIndices = new List<UInt32>();

		/// <summary>
		/// Reverse the order of triangle indices.
		/// </summary>
		public bool ReverseTriangles = false;

		// The default is true because the parent ExtrusionSpec defines if there should be physics
		private bool physFromVisual = true;

		/// <summary>
		/// Use exactly <see cref="LineIndices"/> for physics.
		/// When <see langword="true"/> <see cref="PhysLineIndices"/> will set to <see langword="null"/>.
		/// </summary>
		public bool PhysFromVisual
		{
			get => physFromVisual;
			set
			{
				physFromVisual = value;
				if (value)
				{
					_physLineIndices = null;
				}
			}
		}


		private List<UInt32> _physLineIndices;

		/// <summary>
		/// LineIndices only for physics.
		/// When set to null <see cref="PhysFromVisual"/> will be set to <see langword="true"/>.
		/// Will never return <see langword="null"/>: it will return either <see cref="LineIndices"/>,
		/// <see cref="PhysLineIndices"/> (if it is not <see langword="null"/> or a new <see cref="List{T}"/>.
		/// </summary>
		public List<UInt32> PhysLineIndices
		{
			get
			{
				if (PhysFromVisual)
				{
					return LineIndices;
				}

				if (null == _physLineIndices)
				{
					_physLineIndices = new List<UInt32>();
				}
				return _physLineIndices;
			}
			set
			{
				PhysFromVisual = value == null;
				_physLineIndices = value;
			}
		}

		/// <summary>
		/// Compute normals for all loops.
		/// </summary>
		/// <param name="overrideExisting">When <see langword="false"/> (default) only normals with length zero will be recalculated.</param>
		public void ComputeNormals(bool overrideExisting = false)
		{
			var buffer = new List<Vector3>();
			foreach (var loop in Loops)
			{
				ComputeNormals(LineIndices, loop.Vertices, buffer: buffer, overrideExisting: overrideExisting);

				if (! PhysFromVisual)
				{
					// Only compute
					ComputeNormals(PhysLineIndices, loop.Vertices, buffer: buffer, overrideExisting: false);
				}
			}
		}

		private void ComputeNormals(
			List<UInt32> indices,
			List<VertexRecord> vertices,
			List<Vector3> buffer = null,

		bool overrideExisting = false)
		{
			_ = indices  ?? throw new ArgumentNullException(nameof(indices));
			_ = vertices ?? throw new ArgumentNullException(nameof(vertices));

			if (indices.Count % 2 == 1)
			{
				throw new ArgumentException
					($"Invalid number of indices: {indices.Count}. The amount of indices should be even.");
			}

			var lineNormals = buffer ?? new List<Vector3>();
			// When using a shared buffer: ensure it is empty.
			lineNormals.Clear();

			// Compute normals for lines.
			for (int i = 0; i < indices.Count - 1;)
			{
				var start = vertices[(int) indices[i++]];
				var end   = vertices[(int) indices[i++]];

				var direction = (end.position - start.position).Normalized();
				var normal = new Vector3(-direction.Y, direction.X, (start.normal.Z + end.normal.Z) / 2);

				if (normal.Z != 0)
				{
					normal.Normalize();
				}

				lineNormals.Add(normal);
			}

			// Compute vertex normals by averaging the normals from all lines it is part of.
			for (int i = 0; i < vertices.Count; i++)
			{
				var v = vertices[i];

				if (! overrideExisting&& v.normal.LengthSquared != 0)
				{
					continue;
				}

				int contributions = 0;
				var n = Vector3.Zero;

				// Loop through all indices to find any references to this vertex.
				for (int j = 0; j < indices.Count; j++)
				{
					if (i == indices[j])
					{ // If the lineIndex at j points the the current Vector (with index i)
						n += GetNormalForLine(j);
						contributions++;
					}
				}

				v.normal = n / contributions; // Normalize cheaper since we know the length (which is contributions) already.
				vertices[i] = v;
			}

			// When using a shared buffer: clear the buffer.
			lineNormals.Clear();

			Vector3 GetNormalForLine(int lineIndex)
			{
				// Fetches the normal for the line which either starts or ends at lineIndex.

				var normalIndex = lineIndex / 2;

				if (normalIndex >= lineNormals.Count)
				{
					// todo: really fix
					normalIndex = lineNormals.Count - 1;
				}
				return lineNormals[normalIndex];
			}
		}

		/// <inheritdoc />
		public void Validate(Validator v)
		{
			v.WarnIfEmpty(Loops, nameof(Loops));
			v.WarnIfEmpty(LineIndices, nameof(LineIndices));

			if (! PhysFromVisual)
			{
				v.WarnIfEmpty(PhysLineIndices, nameof(PhysLineIndices));
			}

			int count = -1;
			for(int i = 0; i < Loops.Count; i++)
			{
				using (_ = v.PushContext(nameof(Loops), i))
				{
					var loop = Loops[i];
					v.Validate(loop, nameof(Loop));

					if (count == -1)
					{
						count = loop.Vertices.Count;
						continue;
					}

					v.Error
						(
						 count != loop.Vertices.Count
					   , $"Encountered {nameof(Loop)} #{i} with {loop.Vertices.Count} vertices instead of {count}."
					   + $"\nAll {nameof(Loop)}s in a {nameof(LoopSet)} should have the same number of {nameof(Loop.Vertices)}."
						);
				}
			}

			for(int i = 0; i < LineIndices.Count; i++)
			{
				using (_ = v.PushContext(nameof(LineIndices), i))
				{
					var index = LineIndices[i];
					if (index >= count)
					{
						v.Error($"{nameof(LineIndices)} #{i} '{index}' references a Vertex that does not exist. (#vertices = {count}).");
					}
				}
			}

			if (! PhysFromVisual)
			{
				for (int i = 0; i < PhysLineIndices.Count; i++)
				{
					using (_ = v.PushContext(nameof(PhysLineIndices), i))
					{
						var index = PhysLineIndices[i];
						if (index >= count)
						{
							v.Error($"{nameof(PhysLineIndices)} #{i} '{index}' references a Vertex that does not exist. (#vertices = {count}).");
						}
					}
				}
			}
		}

		public LoopSet Clone()
		{
			var result = new LoopSet();

			result.ReverseTriangles = ReverseTriangles;

			result.Loops.Capacity = Loops.Capacity;
			foreach (Loop loop in Loops)
			{
				result.Loops.Add(loop.Clone());
			}

			result.LineIndices.Capacity = LineIndices.Count;
			result.LineIndices.AddRange(LineIndices);

			result.PhysFromVisual = PhysFromVisual;
			if (null != PhysLineIndices)
			{
				result.PhysLineIndices = new List<UInt32>(PhysLineIndices.Count);
				result.PhysLineIndices.AddRange(PhysLineIndices);
			}

			return result;
		}
	}
}
