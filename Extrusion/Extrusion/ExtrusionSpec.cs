﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/


using System;
using System.Collections.Generic;
using System.Text;

using BinaryDataModel.DataTypes;
using Extrusion.Support;
using OpenToolkit.Mathematics;

using Shared.Validation;

namespace Extrusion.Extrusion
{
	/// <summary>
	/// Specifies how to generate a single extrusion for a <see cref="CurveGraph.CubicBezierCurve"/>.
	/// </summary>
	public class ExtrusionSpec : IValidateable
	{
		/// <summary>
		/// Allow disabling without having to destroy the configuration itself.
		/// </summary>
		public bool Enabled = true;

		public Transformation Transform = Transformation.Identity;


		/// <summary>
		/// Generate a visual mesh data for this extrusion.
		/// </summary>
		public bool Visual => MeshSharing != null;

		/// <summary>
		/// Generate physics mesh data for this Extrusion.
		/// </summary>
		public bool Physics => PhysSharing != null;

		/// <summary>
		/// How the <see cref="Mesh"/> data should be shared between parts of the <see cref="CurveSpec"/>.
		/// When <see langword="null"/> no <see cref="Mesh"/> data will be generated.
		/// </summary>
		public MeshSharing MeshSharing = MeshSharing.Default;


		/// <summary>
		/// How the <see cref="Phys"/> data should be shared between parts of the <see cref="CurveSpec"/>.
		/// When <see langword="null"/> no <see cref="Phys"/> data will be generated.
		/// </summary>
		public PhysSharing PhysSharing = PhysSharing.Default;

		/// <summary>
		/// During Extrusion loopSets will be placed in the order they appear.
		/// </summary>
		public readonly List<LoopSet> LoopSets = new List<LoopSet>();

		/// <summary>
		/// When not <see langword="null"/> will override any physics defined in <see cref="LoopSets"/>.
		/// </summary>
		/// <remarks>
		/// Example use case: if you need different <see cref="InterpolationSettings"/> for the physics mesh.
		/// </remarks>
		public List<LoopSet> PhysicsOverrideSets; //todo: lua can't make List<T> instances, use a bool property to make a new instance.

		/// <inheritdoc />
		public void Validate(Validator v)
		{
			v.Validate(Transform, nameof(Transform));
			v.Validate(MeshSharing, nameof(MeshSharing), allowNull: true);
			v.Validate(PhysSharing, nameof(PhysSharing), allowNull: true);

			v.Validate(LoopSets, nameof(LoopSets), DiagnosticSeverity.Warning);

			if (null != PhysicsOverrideSets)
			{
				v.Validate(PhysicsOverrideSets, nameof(PhysicsOverrideSets), DiagnosticSeverity.Warning);
			}
		}

		public ExtrusionSpec Clone()
		{
			var r = new ExtrusionSpec();
			r.Enabled = Enabled;
			r.Transform = Transform.Clone();
			r.MeshSharing = MeshSharing?.Clone();
			r.PhysSharing = PhysSharing?.Clone();
			r.LoopSets.Capacity = LoopSets.Count;
			foreach (LoopSet set in LoopSets)
			{
				r.LoopSets.Add(set.Clone());
			}

			if (null != PhysicsOverrideSets)
			{
				r.PhysicsOverrideSets = new List<LoopSet>(PhysicsOverrideSets.Count);
				foreach (LoopSet set in PhysicsOverrideSets)
				{
					r.PhysicsOverrideSets.Add(set.Clone());
				}
			}

			return r;
		}
	}
}
