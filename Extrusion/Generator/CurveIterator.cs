﻿// Copyright 2022-2023 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/


using System;
using System.Collections.Generic;

using CurveGraph;
using Extrusion.Diagnostics;
using Extrusion.Support;
using OpenToolkit.Mathematics;

using Shared;

namespace Extrusion.Generator
{
	/// <summary>
	/// Iterates over a <see cref="CubicBezierCurve"/> according to an array of <see cref="InterpolationSettings"/>.
	/// Each instance can run a single iteration at a time, but it can be re-used multiple times.
	/// The iterator tries to find the maximum length segments within the constraints provided.
	/// </summary>
	public class CurveIterator
	{
		private readonly CurveIteratorDiag Diag;

		private const float Deg2Rad = 0.0174532924f;
		private const float Rad2Deg = 57.29578f;


		public readonly CubicBezierCurve Curve;

		public readonly IteratorSettings IteratorSettings;


		public bool Iterating { get; private set; }


		private int _currentStep = 0;
		public int CurrentStep
		{
			get => _currentStep;
			set
			{
				_currentStep = value;

				_currentLoop = _currentLoop++;
				if (CurrentLoop >= FlatSettings.Length)
				{
					_currentLoop = 0;
				}
			}
		}

		/// <summary>
		/// The current iteration in the searching phase, or -1 if not searching right now.
		/// </summary>
		public int SearchIteration { get; private set; } = -1;

		public float SearchMinDistance { get; private set; }
		public float SearchMaxDistance { get; private set; }
		public float PivotDistance { get; private set; }


		private int _currentLoop = 0;

		/// <summary>
		/// Index to the current loop in the flattened array of loops.
		/// </summary>
		public int CurrentLoop => _currentLoop;


		private readonly InterpolationSettings[] FlatSettings;
		private readonly int[] AcceptableEndIndices; // todo: actually implement this.


		private float CurveStartDistance => IteratorSettings.startDistance;
		private float CurveEndDistance => Math.Min(Curve.Length, IteratorSettings.endDistance);


		public InterpolationSettings CurrentSettings => FlatSettings[CurrentLoop];

		/// <summary>
		/// The distance along the curve for <see cref="CandidateSample"/>.
		/// </summary>
		public float CurrentDistance => CandidateSample.distanceInCurve;

		/// <summary>
		/// The length of the segment between <see cref="PreviousSample"/> and <see cref="CandidateSample"/>.
		/// </summary>
		public float CurrentSegmentLength => CurrentDistance - PreviousSample.distanceInCurve;

		public CurveSample PreviousSample { get; private set; }
		public CurveSample CandidateSample { get; private set; }
		/// <summary>
		/// The sample that did not pass the test causing search to start.
		/// </summary>
		public CurveSample SearchFromSample { get; private set; }


		private int _maxSearchIterations = 3;

		public int MaxSearchIterations
		{
			get => _maxSearchIterations;
			set
			{
				if (Iterating) throw new InvalidOperationException("Cannot change the parameters of the iteration while it's running.");

				_maxSearchIterations = value;
			}
		}

		/// <summary>
		/// Create a new <see cref="CurveIterator"/>.
		/// </summary>
		/// <param name="curve">The curve to iterate over.</param>
		/// <param name="settingsArray">The settings to use.</param>
		/// <param name="endIndices">
		/// Acceptable indices in <paramref name="settingsArray"/> where the iteration may stop, or <see langword="null"/> to allow always.
		/// The Length must match <paramref name="settingsArray"/>.
		/// </param>
		/// <exception cref="ArgumentException"></exception>
		internal CurveIterator(CubicBezierCurve curve, IteratorSettings iteratorSettings, InterpolationSettings[] settingsArray, int[] endIndices = null, CurveIteratorDiag diag = null)
		{
			Curve = curve ?? throw new ArgumentNullException(nameof(curve));
			IteratorSettings = iteratorSettings;
			FlatSettings = settingsArray ?? throw new ArgumentNullException(nameof(settingsArray));


			AcceptableEndIndices = endIndices;

			if (endIndices != null)
			{
				if (endIndices.Length != settingsArray.Length)
				{
					throw new ArgumentException
						(
						 $"When {nameof(endIndices)} is not null it's Length must be the same as that of {nameof(settingsArray)}"
					   , nameof(endIndices)
						);
				}
			}

			Diag = diag;
			Diag?.Init(this);
		}

		/// <summary>
		/// Enum to signal which tests failed.
		/// </summary>
		[Flags]
		public enum Checks : byte
		{
			None = 0,
			Tangent        = 1 << 0,
			MidpointOffset = 1 << 1,
			MidpointAngle  = 1 << 2,
		}

		/// <summary>
		/// Inspect <paramref name="sample"/> and look at the internal state to see if <paramref name="sample"/> is valid.
		/// Tests include: angle between tangents, distance from true curve and the midpoint between <see cref="PreviousSample"/> and <paramref name="sample"/>.
		/// </summary>
		/// <param name="sample"></param>
		/// <returns><see langword="true"/> if valid, <see langword="false"/> otherwise.</returns>
		internal bool CheckCandidate(CurveSample sample)
		{
			CurveSample halfway;
			if (Diag?.Check_Result == null)
			{
				// Order is deliberate:
				// - Tangent needs only Dot and Acos.
				// - MidPointOffset needs to create another sample.
				return CheckTangent(sample)
					&& CheckMidpointOffset(sample, out halfway)
					&& CheckMidpointAngle(sample, halfway);
			}

			// If there is a result diagnostic then don't short circuit the check.
			Checks result = Checks.None;

			result |= !CheckTangent(sample) ? Checks.Tangent : Checks.None;
			result |= !CheckMidpointOffset(sample, out halfway) ? Checks.MidpointOffset : Checks.None;
			result |= !CheckMidpointAngle(sample, halfway) ? Checks.MidpointAngle : Checks.None;

			Diag?.Check_Result?.Invoke(result);

			return result == Checks.None;
		}

		#region Check Implementations

		private float DotToAngle(float dot)
		{
			float angle;

			if (dot >= 1)
			{
				angle = 0;
			}
			else if (dot <= -1)
			{
				angle = 180 * Deg2Rad;
			}
			else if (dot == 0)
			{
				angle = 90 * Deg2Rad;
			}
			else
			{
				angle = (float)Math.Acos(dot);

				if (!FloatExt.IsFinite(angle))
				{
					throw new InvalidOperationException("Should be mathematically impossible.");
				}
			}
			return angle;
		}

		/// <summary>
		/// Check that the angle between the tangent of <paramref name="sample"/> and <see cref="PreviousSample"/>
		/// is less than <see cref="InterpolationSettings.MaxAngleBetweenInterpolations"/> (from <seealso cref="CurrentSettings"/>).
		/// </summary>
		/// <param name="sample"></param>
		/// <returns><see langword="true"/> if less, <see langword="false"/> otherwise.</returns>
		internal bool CheckTangent(in CurveSample sample)
		{
			Vector3 previousTangent = PreviousSample.tangent;
			Vector3 sampleTangent = sample.tangent;

			float dot = Vector3.Dot(previousTangent, sampleTangent);

			float angle = DotToAngle(dot);

			Diag?.Check_Angle?.Invoke(angle * Rad2Deg);

			return angle < CurrentSettings.MaxAngleBetweenInterpolations * Deg2Rad;
		}

		/// <summary>
		/// Check that the midpoint between <paramref name="sample"/> and <see cref="PreviousSample"/>
		/// is no further than <see cref="InterpolationSettings.MaxSegmentOffset"/> (from <seealso cref="CurrentSettings"/>)
		/// away from the 'true' <see cref="CubicBezierCurve"/>.
		/// </summary>
		/// <remarks>
		/// This check is expensive because it needs to compute a new <see cref="CurveSample"/> at the midpoint to compare to.
		/// </remarks>
		/// <param name="sample"></param>
		/// <param name="halfWaySample"></param>
		/// <returns><see langword="true"/> if less, <see langword="false"/> otherwise.</returns>
		internal bool CheckMidpointOffset(in CurveSample sample, out CurveSample halfWaySample)
		{
			Vector3 ps = sample.location;
			Vector3 pp = PreviousSample.location;

			Vector3 halfway = (pp + ps) / 2;

			float halfDistance = (sample.distanceInCurve + PreviousSample.distanceInCurve) / 2;
			halfWaySample = Curve.GetSampleAtDistance(halfDistance);
			Vector3 trueHalfWay = halfWaySample.location;

			Vector3 delta = halfway - trueHalfWay;

			float deltaMagnitudeSq = delta.LengthSquared;

			Diag?.Check_SegmentOffset?.Invoke((float)Math.Sqrt(deltaMagnitudeSq));

			return deltaMagnitudeSq < CurrentSettings.MaxSegmentOffset * CurrentSettings.MaxSegmentOffset;
		}

		/// <summary>
		/// Check that direction and tangent align, to catch the case where we have an S bend and the middle of the straight line happens to align properly, eveh though the curve does not.
		/// </summary>
		/// <param name="sample"></param>
		/// <param name="halfWaySample"></param>
		/// <returns></returns>
		internal bool CheckMidpointAngle(in CurveSample sample, in CurveSample halfWaySample)
		{
			Vector3 ps = sample.location;
			Vector3 pp = PreviousSample.location;
			Vector3 direction = (pp - ps).Normalized();

			var dot = Math.Abs(Vector3.Dot(direction, halfWaySample.tangent));
			float angle = DotToAngle(dot);

			Diag?.Check_SegmentCenterAgnle?.Invoke(angle * Rad2Deg);

			return angle < CurrentSettings.MaxSegmentCenterAngle * Deg2Rad;
		}

		#endregion Check Implementations

		/// <summary>
		/// Update the internal state to move on to the next Candidate Sample.
		/// </summary>
		private void AdvanceCandidate()
		{
			CurrentStep++;

			PreviousSample = CandidateSample;

			float distance = Math.Min(PreviousSample.distanceInCurve + CurrentSettings.MaxInterpolationDistance, CurveEndDistance);
			CandidateSample = Curve.GetSampleAtDistance(distance);
		}

		/// <summary>
		/// Check that the current sample will not cause constraints to break down the line.
		/// For example that the distance between the current sample and the end is less than the minimum distance.
		/// </summary>
		/// <returns><see langword="true" /> if iteration needs to stop, <see langword="false"/> otherwise.</returns>
		private bool CheckFinalSegmentConstraint()
		{
			if (CurrentDistance >= CurveEndDistance)
			{
				return true;
			}

			if (CurrentDistance + CurrentSettings.MinInterpolationDistance < CurveEndDistance)
			{
				// todo: we actually need to ensure we end on an allowed end segment, according to EndIndices.
				// todo: this may mean checking multiple segments ahead.
				return false;
			}


			// Using the current candidate would result in needing to place the final sample less than minimum distance away from the current one.
			// That would violate the constraint, we should try to avoid this.
			// There are two cases: we can move this sample all the way to the end, or move back so the minimum fits between this and the end.
			// To decide we compute which one is the least bad.


			// The distance to move the current sample to reach the end.
			float stretchDistance = CurveEndDistance - CurrentDistance;

			// The distance to move the current sample back to allow a minimum distance sample between the current and final sample.
			float shrinkDistance = CurrentDistance - (CurveEndDistance - CurrentSettings.MinInterpolationDistance);

			// The segment lengths for the two cases.
			float stretchedLength = CurrentSegmentLength + stretchDistance;
			float shrinkedLength = CurrentSegmentLength - shrinkDistance;


			float stretchedTooLong = Math.Min(0, stretchedLength - CurrentSettings.MaxInterpolationDistance);
			float shrinkTooShort = Math.Min(0, CurrentSettings.MinInterpolationDistance - shrinkedLength);

			Diag?.FinalSegmentConstraint?.Invoke(stretchDistance, shrinkDistance, stretchedTooLong, shrinkTooShort);

			if (stretchedTooLong < shrinkTooShort * CurrentSettings.ShrinkReluctance)
			{
				// Stretching would be less bad than shrinking.

				CandidateSample = Curve.GetSampleAtDistance(CurveEndDistance);

				Diag?.FinalSegmentConstraint_Stretched?.Invoke();
				return true;
			}
			// In case they are equal we opt for adding a sample between.
			// This should also be the sensible option in case the math resolves both to 0 for some weird reason.

			// todo: in case constraints break: distribute the breakage across all remaining segments somehow.
			CandidateSample = Curve.GetSampleAtDistance(CurveEndDistance - CurrentSettings.MinInterpolationDistance);

			Diag?.FinalSegmentConstraint_Shrunk?.Invoke();
			return false;
		}

		/// <summary>
		/// Begin iteration over <see cref="Curve"/>.
		/// </summary>
		/// <returns></returns>
		public IEnumerable<CurveSample> IterateCurve()
		{
			try
			{
				if (Iterating)
				{
					throw new InvalidOperationException
						("Cannot start iteration because another is already in progress.");
				}

				Iterating = true;

				PreviousSample = Curve.GetSampleAtDistance(CurveStartDistance);
				CandidateSample = Curve.GetSampleAtDistance
					(Math.Min(CurveStartDistance + CurrentSettings.MaxInterpolationDistance, CurveEndDistance));

				Diag?.Start?.Invoke();

				Diag?.Yield?.Invoke(PreviousSample);
				yield return PreviousSample;

				CurrentStep++;

				while (true)
				{
					Diag?.MainIteration?.Invoke();

					if (CheckCandidate(CandidateSample))
					{
						Diag?.Yield?.Invoke(CandidateSample);

						if (CurrentDistance >= CurveEndDistance)
						{
							Diag?.Yield_Done?.Invoke();
							yield return CandidateSample;
							break;
						}

						yield return CandidateSample;


						AdvanceCandidate();

						if (CheckFinalSegmentConstraint())
						{
							Diag?.Yield?.Invoke(CandidateSample);
							Diag?.Yield_Done_Stretched?.Invoke();
							yield return CandidateSample;
							break;
						}

						continue;
					}

					// else:
					// The candidate did not pass,
					// binary search for a sample that does pass.

					SearchFromSample = CandidateSample;

					SearchMinDistance = Math.Min
						(CurveEndDistance, PreviousSample.distanceInCurve + CurrentSettings.MinInterpolationDistance);
					SearchMaxDistance = Math.Min
						(CurveEndDistance, PreviousSample.distanceInCurve + CurrentSettings.MaxInterpolationDistance);

					Diag?.Search_Start?.Invoke();

					// Just so we don't need to re-generate in one of the cases it later.
					bool hasSampleAtMinDistance = false;
					CurveSample sampleAtMinDistance = new CurveSample();

					SearchIteration = 0;
					do
					{
						SearchIteration++;

						PivotDistance = (SearchMinDistance + SearchMaxDistance) / 2;

						CandidateSample = Curve.GetSampleAtDistance(PivotDistance);

						Diag?.Search_Iteration?.Invoke();

						if (CheckCandidate(CandidateSample))
						{
							// The candidate at the pivot passed the test.
							// This means that the next search should look at the region
							// from the pivot to the search max.
							SearchMinDistance = PivotDistance;

							// The new min distance is the old pivot distance.
							// The candidate sample sits at the pivot.
							hasSampleAtMinDistance = true;
							sampleAtMinDistance = CandidateSample;

							Diag?.Search_Min?.Invoke();
						}
						else
						{
							SearchMaxDistance = PivotDistance;

							Diag?.Search_Max?.Invoke();
						}
					}
					while (SearchIteration < MaxSearchIterations);

					if (SearchMaxDistance == PivotDistance)
					{
						// The final iteration did not pass the test.
						// In that case we need to select the sample at searchMinDistance instead, because that has passed the test before.

						if (hasSampleAtMinDistance)
						{
							// If we can re-use the saved sample rather than re-computing it, because that's expensive.
							CandidateSample = sampleAtMinDistance;
						}
						else
						{
							CandidateSample = Curve.GetSampleAtDistance(SearchMinDistance);
						}

						Diag?.Search_FinalAdjust?.Invoke();
					}

					///////////////////////////////////////////////////////////////
					// Search is done
					Diag?.Search_End?.Invoke();

					SearchIteration = -1;

					bool forcedFinalSample = CheckFinalSegmentConstraint();
					Diag?.Yield?.Invoke(CandidateSample);
					if (forcedFinalSample)
					{
						Diag?.Yield_Done_Searched_Stretched?.Invoke();
					}

					yield return CandidateSample;
					if (forcedFinalSample) break;

					AdvanceCandidate();
				}

				Iterating = false;

				Diag?.Done?.Invoke();
			}
			finally
			{
				Diag?.Finally?.Invoke();
			}
		}
	}
}
