﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/


using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

using BinaryDataModel;
using BinaryDataModel.DataTypes;

using CurveGraph;
using Extrusion.Diagnostics;
using Extrusion.Periodic;
using OpenToolkit.Mathematics;

using Shared;



namespace Extrusion.Generator
{
	public class PeriodicPlacer : IPartGenerator
	{
		private readonly CurveDecorator generator;
		private readonly PeriodicPlacerDiag Diag;
		private readonly PeriodicItemSpec Spec;

		private readonly MeshData meshData;
		private readonly MeshData physData;

		private CubicBezierCurve Curve => generator.ActiveCurve;


		public PeriodicPlacer(
			CurveDecorator generator,
			PeriodicItemSpec spec,
			PeriodicPlacerDiag diag = null)
		{
			this.generator = generator ?? throw new ArgumentNullException(nameof(generator));
			Spec = spec ?? throw new ArgumentNullException(nameof(spec));
			Diag = diag;

			if (null != spec.MeshSharing)
			{
				meshData = generator.GetMeshData(spec.MeshSharing);
			}

			if (null != spec.PhysSharing)
			{
				physData = generator.GetMeshData(spec.PhysSharing);
			}
		}


		public bool YieldForGroupItems = false;

		public int ItemIndex { [DebuggerStepThrough] get; [DebuggerStepThrough] private set; } = 0;

		public int GroupIndex { [DebuggerStepThrough] get; [DebuggerStepThrough] private set; } = 0;

		public int Step { [DebuggerStepThrough] get;  [DebuggerStepThrough] private set; }

		public PeriodicItemBase CurrentItem => Spec.Items[ItemIndex];


		/// <inheritdoc />
		public float DistanceInCurve { [DebuggerStepThrough] get; [DebuggerStepThrough] private set; }

		/// <inheritdoc />
		public IEnumerable Generate(IteratorSettings iteratorSettings, int sectionNumber)
		{
			try
			{
				Diag?.Init(this);
				// todo: case !Smudge
				// todo: case !AlwaysFullSequence

				int itemCount = Spec.Items.Count;

				float length = Math.Min(Curve.Length, iteratorSettings.endDistance) - iteratorSettings.startDistance;

				float sumIdealDistance = Spec.Items.Sum(item => item.IdealDistance);
				float sumMinDistance = Spec.Items.Sum(item => item.MinDistance);

				float idealPlacements = length / (sumIdealDistance / itemCount);
				float minPlacements = length / (sumMinDistance / itemCount);

				if (Spec.AlwaysFullSequence && minPlacements < 1)
				{
					// Does not fit, do nothing.

					Diag?.NotLongEnough?.Invoke();
					yield break;
				}

				// The number of items that will be placed.
				int placements = (int)Math.Round(idealPlacements);

				// The average real distance between items.
				float actualDistance = length / placements;
				// todo: what if this rounds to below minFit?


				// Now compute the multiplier to the idealDistance required to fit without a gap at the end.
				float smudgeMultiplier = actualDistance / sumIdealDistance * itemCount;

				// Set the starting position based on InitialOffset.
				// todo: should this instead use the average InitialOffset?
				DistanceInCurve = CurrentItem.IdealDistance * Spec.InitialOffset + iteratorSettings.startDistance;

				Diag?.Start?.Invoke();
				Diag?.WriteLine($"Items {itemCount}, length {length}, sumIdealDistance {sumIdealDistance}, idealFit {idealPlacements}, Placements {placements}, SmudgeMultiplier {smudgeMultiplier}");

				uint firstMeshVertex;
				uint firstPhysVertex;

				for (Step = 0; Step < placements; Step++)
				{
					Diag?.Step?.Invoke();

					CurveSample sample = Curve.GetSampleAtDistance(DistanceInCurve);

					Diag?.Sample?.Invoke(sample);

					Matrix4 SampleTransform = sample.Transformation();
					Matrix4 CurveTransform = CurrentItem.Transform.ComputeFinalTransform(SampleTransform);

					Diag?.Transform?.Invoke(CurveTransform);

					void AddMesh(MeshSpecification spec)
					{
						Diag?.Mesh?.Invoke(spec);

						Matrix4 FinalTransform = spec.Transform.Transform * CurveTransform;

						Diag?.FinalTransform?.Invoke(FinalTransform);

						firstMeshVertex = (uint)(meshData?.VertexCount ?? 0);
						firstPhysVertex = (uint)(physData?.VertexCount ?? 0);

						// todo: ideally we have this outside all loops.
						int numZones = Math.Min(spec.PaintZones.Count, MeshCombiner.paintZoneColors.Length);

						foreach (var vertex in spec.Mesh.vertices)
						{
							Vector3 transformedV = (vertex.position.WithW(1) * FinalTransform).Xyz;
							Vector3 transformedN = (vertex.normal.WithW(0) * FinalTransform).Xyz;

							var entry = new VertexRecord();
							entry.position = transformedV;
							entry.normal = transformedN;
							entry.color = vertex.color;

							if (spec.PaintZones != null)
							{
								if (spec.ForcePaint)
								{
									if (numZones < 1) throw new InvalidOperationException("No paint defined");

									entry.color = spec.PaintZones[0];
								}
								else
								{
									for (int i = 0; i < numZones; i++)
									{
										if (entry.color == MeshCombiner.paintZoneColors[i])
										{
											entry.color = spec.PaintZones[i];
										}
									}
								}
							}

							meshData?.Add(entry);

							// todo: read from spec.Phys instead (pain).
							physData?.Add(entry);
						}

						if (!generator.ReverseTriangles)
						{
							foreach (uint meshIndex in spec.Mesh.indices)
							{
								meshData?.Add(firstMeshVertex + meshIndex);

								// todo: read from spec.Phys instead (pain).
								physData?.Add(firstPhysVertex + meshIndex);
							}

							return;
						}

						for (int i = 0; i < spec.Mesh.indices.Count; i += 3)
						{
							meshData?.Add(firstMeshVertex + spec.Mesh.indices[i + 2]);
							meshData?.Add(firstMeshVertex + spec.Mesh.indices[i + 1]);
							meshData?.Add(firstMeshVertex + spec.Mesh.indices[i + 0]);

							// todo: read from spec.Phys instead (pain).
							physData?.Add(firstPhysVertex + spec.Mesh.indices[i + 2]);
							physData?.Add(firstPhysVertex + spec.Mesh.indices[i + 1]);
							physData?.Add(firstPhysVertex + spec.Mesh.indices[i + 0]);
						}
					}

					if (CurrentItem is Periodic.PeriodicItem item)
					{
						Diag?.SingleItem?.Invoke(item);

						var meshSpec = item.MeshSpec;
						AddMesh(meshSpec);
					}
					else if (CurrentItem is PeriodicGroup group)
					{
						Diag?.Group?.Invoke(group);
						bool first = true;
						foreach (var meshSpec in group.Entries)
						{
							if (!first && YieldForGroupItems)
							{
								yield return null;
							}
							first = false;

							AddMesh(meshSpec);
						}
					}
					else
					{
						throw new ArgumentException($"Unexpected type {CurrentItem.GetType()}.");
					}

					Diag?.Yield?.Invoke();
					yield return null;

					DistanceInCurve += CurrentItem.IdealDistance * smudgeMultiplier;
					ItemIndex++;

					if (ItemIndex >= Spec.Items.Count)
					{
						ItemIndex = 0;
					}
				}

				Diag?.Done?.Invoke();
			}
			finally
			{
				Diag?.Finally?.Invoke();
			}
		}
	}
}
