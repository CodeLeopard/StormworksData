﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/


using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using CurveGraph;

using DataModel.Tiles;

using Extrusion.Diagnostics;
using Extrusion.Support;
using Extrusion.Train;

using OpenToolkit.Mathematics;

namespace Extrusion.Generator
{
	internal class TrainPhysicsGenerator : IPartGenerator
	{
		private CurveDecorator generator;

		private readonly TrainPhysicsDiag Diag;

		public CubicBezierCurve Curve => generator.ActiveCurve;

		public readonly TrainPhysicsSpec Spec;

		/// <summary>
		/// Placed between curve name and step number. When defined should contain a leading separator.
		/// </summary>
		public string TrackName = String.Empty;


		public TrainPhysicsGenerator(CurveDecorator generator, TrainPhysicsSpec spec, TrainPhysicsDiag diag = null)
		{
			this.generator = generator ?? throw new ArgumentNullException(nameof(generator));
			Diag = diag;
			Spec = spec ?? throw new ArgumentNullException(nameof(spec));
		}



		/// <inheritdoc />
		public float DistanceInCurve { get; private set; }

		/// <inheritdoc />
		public IEnumerable Generate(IteratorSettings iteratorSettings, int sectionNumber)
		{
			try
			{
				int counter = 0;
				DistanceInCurve = 0;

				var iterator = new CurveIterator
					(Curve, iteratorSettings, new InterpolationSettings[] { Spec.InterpolationSettings }, null, Diag?.IteratorDiag);

				TrainTrackNode previous = null;

				foreach (CurveSample sample in iterator.IterateCurve())
				{
					DistanceInCurve = sample.distanceInCurve;

					Matrix4 sampleTransform = sample.Transformation();
					Matrix4 finalTransform = Spec.Transform.ComputeFinalTransform(sampleTransform);

					// Rotation and scale do not apply to TrainTracks and it would needlessly clutter the xml.
					Vector3 transformed = finalTransform.Row3.Xyz;

					// todo: ideally we could check here for conflicting tracks and handle it
					// but that turns out to be really messy.
					// For now I'll leave it up to the export process to sort that out.

					string id = $"{generator.CurveName}{TrackName}_S#{sectionNumber}_#{counter++}";

					TrainTrackNode current
						= previous == null
						? new TrainTrackNode(id, transformed)
						: new TrainTrackNode(id, transformed, previous);


					generator.AddTrack(current);

					yield return null;

					previous = current;
				}

				Diag?.Done?.Invoke();
			}
			finally
			{
				Diag?.Finally?.Invoke();
			}
		}
	}
}
