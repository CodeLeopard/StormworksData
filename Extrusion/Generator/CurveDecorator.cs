﻿// Copyright 2022-2025 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/


using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BinaryDataModel.DataTypes;

using CurveGraph;

using DataModel.Tiles;

using Extrusion.Diagnostics;
using Extrusion.Extrusion;
using Extrusion.Periodic;
using Extrusion.Support;
using Extrusion.Train;

using OpenToolkit.Mathematics;


namespace Extrusion.Generator
{
	/// <summary>
	/// Generates Decorations, such as <see cref="Mesh"/>, <see cref="Phys"/> and TrainTrack nodes.
	/// Can be re-used to minimize allocations, but cannot be used from multiple Threads at once.
	/// </summary>
	public class CurveDecorator
	{
		/// <summary>
		/// Reverse triangles to match Godot convention.
		/// </summary>
		public bool ReverseTriangles = false;

		public bool GenerateMesh = true;
		public bool GeneratePhys = true;
		public bool GenerateTrack = true;

		/// <summary>
		/// The name of the curve, used in names for elements, meshes, etc.
		/// </summary>
		public string CurveName { get; private set; }

		public CubicBezierCurve ActiveCurve { get; private set; }
		public CurveSpec ActiveSpec { get; private set; }

		/// <summary>
		/// Add to world space vertices to transform them
		/// from curve space to mesh space (so the center of curve is at 0,0,0).
		/// </summary>
		public Vector3 CurveSpace2MeshSpace { get; private set; }


		private readonly List<TrainTrackNode> _tracks = new List<TrainTrackNode>();
		public readonly IReadOnlyList<TrainTrackNode> Tracks;

		private readonly Dictionary<IPartGenerator, IEnumerator> enumerators = new Dictionary<IPartGenerator, IEnumerator>();
		private readonly Dictionary<MeshOrPhysSharing, MeshData> meshData = new Dictionary<MeshOrPhysSharing, MeshData>();


		private StringBuilder sb = new StringBuilder();

		private int splitSectionIndex = 0;

		private int NextUniqueMeshID = 0;
		private int NextUniqueSubmeshID = 0;

		private int extrusionCounter;
		private int periodicCounter;
		private int trackCounter;

		public bool EnableExtruderDiag = false;
		public bool EnablePeriodicPlacerDiag = false;
		public bool EnableTrainPhysicsDiag = false;


		public CurveDecorator()
		{
			Tracks = _tracks.AsReadOnly();
		}

		/// <summary>
		/// Clears all data in the Decorater, making it ready for re-use.
		/// </summary>
		public void Clear()
		{
			foreach (MeshData data in meshData.Values)
			{
				data.Clear();
			}
			meshData.Clear();

			_tracks.Clear();

			enumerators.Clear();

			sb.Clear();

			splitSectionIndex = 0;
		}


		internal MeshData GetMeshData(MeshOrPhysSharing shareID)
		{
			if (null == shareID) throw new ArgumentNullException(nameof(shareID));

			if (string.IsNullOrEmpty(shareID.SharedMesh))
			{
				shareID.SharedMesh = $"Unique_{NextUniqueMeshID++}";
			}

			if (string.IsNullOrEmpty(shareID.SharedSubMesh))
			{
				shareID.SharedSubMesh = $"Unique_{NextUniqueSubmeshID++}";
			}

			// cursed_SectionID
			// todo: you should never change the fields of a key in a dictionary, but it works in this very specific case, so don't touch it.
			shareID.SectionID = splitSectionIndex;

			if (!meshData.TryGetValue(shareID, out MeshData data))
			{
				data = new MeshData(this);
				meshData.Add(shareID, data);
			}

			return data;
		}

		internal void Add(MeshOrPhysSharing shareID, VertexRecord vertex)
		{
			var data = GetMeshData(shareID);
			data.Add(vertex);
		}

		internal void AddIndex(MeshOrPhysSharing shareID, UInt32 index)
		{
			var data = GetMeshData(shareID);
			data.Add(index);
		}

		internal void AddTrack(TrainTrackNode v)
		{
			_tracks.Add(v);
		}



		public Func<StringBuilder, ExtruderDiag> GetExtruderDiag = DefaultExtruderDiag;

		private static ExtruderDiag DefaultExtruderDiag(StringBuilder sb)
		{
			var d = ExtruderDiag.LogAll(sb);

			return d;
		}

		private ExtruderDiag MakeExtruderDiag(int number)
		{
			if (! EnableExtruderDiag) { return null; }

			var d = GetExtruderDiag(sb);
			d.LinePrefix = $"Extruder[{number,2}] ";

			var i = CurveIteratorDiag.LogAll(sb);
			i.LinePrefix = "    --------- ";
			i.Finally = null;
			i.TeeToConsoleImmediately = d.TeeToConsoleImmediately;

			d.IteratorDiag = i;

			return d;
		}

		private PeriodicPlacerDiag MakePlacerDiag(int number)
		{
			if (!EnablePeriodicPlacerDiag) { return null; }

			var d = PeriodicPlacerDiag.LogAll(sb);
			d.LinePrefix = $"Periodic[{number,2}] ";
			d.TeeToConsoleImmediately = true;

			return d;
		}

		private TrainPhysicsDiag MakeTrackDiag(int number)
		{
			if (!EnablePeriodicPlacerDiag) { return null; }

			var d = TrainPhysicsDiag.LogAll(sb);
			d.LinePrefix = $"Track   [{number,2}] ";
			d.TeeToConsoleImmediately = true;

			return d;
		}

		private IEnumerable<IPartGenerator> GetPartGenerators(CurveSpec spec, int subSpecDepth = 0)
		{
			if (subSpecDepth == 0)
			{
				extrusionCounter = 0;
				periodicCounter = 0;
				trackCounter = 0;
			}

			spec.UpdateParentHierarchy();

			foreach (ExtrusionSpec extrusion in spec.Extrusions)
			{
				if (! extrusion.Enabled) continue;


				if (GenerateMesh && extrusion.Visual)
				{
					var e = new Extruder(this, extrusion, Extruder.MeshKind.Mesh, MakeExtruderDiag(extrusionCounter++));
					yield return e;
				}

				if (GeneratePhys && extrusion.Physics)
				{
					var e = new Extruder(this, extrusion, Extruder.MeshKind.Phys, MakeExtruderDiag(extrusionCounter++));
					yield return e;
				}
			}

			foreach (PeriodicItemSpec periodic in spec.Periodics)
			{
				if (!periodic.Enabled) continue;

				var p = new PeriodicPlacer(this, periodic, MakePlacerDiag(periodicCounter++));
				yield return p;
			}
			if (GenerateTrack)
			{
				foreach (TrainPhysicsSpec track in spec.TrainPhysics)
				{
					if (!track.Enabled) continue;

					var t = new TrainPhysicsGenerator(this, track, MakeTrackDiag(trackCounter));
					t.TrackName = $"_Track#{trackCounter++}";
					yield return t;
				}
			}

			foreach (CurveSpec subSpec in spec.SubSpecs)
			{
				foreach (IPartGenerator generator in GetPartGenerators(subSpec, subSpecDepth + 1))
				{
					yield return generator;
				}
			}
		}

		/// <summary>
		/// Returns an <see cref="IEnumerable"/> that steps through the curve and all elements of <paramref name="spec"/>.
		/// The <see cref="ResultData"/> is in the last element of the <see cref="IEnumerable{T}"/>, all intermediate steps return <see langword="null"/>.
		/// If you wish to construct the partial result after steps call <see cref="CreateResultData()"/>.
		/// </summary>
		/// <param name="curve"></param>
		/// <param name="spec"></param>
		/// <param name="curveName"></param>
		/// <returns></returns>
		public IEnumerable<ResultData?> GetSteppingEnumerable(CubicBezierCurve curve, CurveSpec spec, string curveName)
		{
			Clear();

			curve = curve ?? throw new ArgumentNullException(nameof(curve));
			spec = spec ?? throw new ArgumentNullException(nameof(spec));
			curveName = curveName ?? throw new ArgumentNullException(nameof(curveName));

			// We copy the spec because the cursed implementation of SectionID, search in this file for: cursed_SectionID
			spec = spec.Clone();

			if(curve.Length <= spec.MaxSegmentLength)
			{
				foreach(var _ in SteppingEnumerable_Implementation(curve, spec, curveName, IteratorSettings.FullCurve, 0))
				{
					yield return null;
				}
				yield return CreateResultData();
				yield break;
			}

			float ratio = curve.Length / spec.MaxSegmentLength;
			var numSections = Math.Ceiling(ratio);

			float fractionalStep = (float)(1 / numSections);

			for(int i = 0; i < numSections; i++)
			{
				float startDistance = i * fractionalStep * curve.Length;
				float endDistance = (i + 1) * fractionalStep * curve.Length;
				endDistance = Math.Min(endDistance, curve.Length); // Floating point inaccuracy safety measure.

				var iteratorSettings = new IteratorSettings(startDistance, endDistance);

				foreach (var _ in SteppingEnumerable_Implementation(curve, spec, curveName, iteratorSettings, i))
				{
					yield return null;
				}

				splitSectionIndex++;
			}

			yield return CreateResultData();
			yield break;
		}

		/// <summary>
		/// Returns an <see cref="IEnumerable"/> that steps through the curve and all elements of <paramref name="spec"/>. All steps return null.
		/// </summary>
		/// <param name="curve"></param>
		/// <param name="spec"></param>
		/// <param name="curveName"></param>
		/// <returns></returns>
		private IEnumerable SteppingEnumerable_Implementation(CubicBezierCurve curve, CurveSpec spec, string curveName, IteratorSettings iteratorSettings, int sectionNo)
		{
			curve     = curve     ?? throw new ArgumentNullException(nameof(curve));
			spec      = spec      ?? throw new ArgumentNullException(nameof(spec));
			curveName = curveName ?? throw new ArgumentNullException(nameof(curveName));

			ActiveCurve = curve;
			ActiveSpec  = spec;
			CurveName   = curveName;

			CurveSpace2MeshSpace = -ActiveCurve.Position;

			foreach (IPartGenerator partGenerator in GetPartGenerators(spec))
			{
				var enumerator = partGenerator.Generate(iteratorSettings, sectionNo).GetEnumerator();

				bool any = enumerator.MoveNext();

				if (any)
				{
					enumerators.Add(partGenerator, enumerator);

					yield return null;
				}
			}

			while (true)
			{

				IPartGenerator best = null;
				float bestDist = float.PositiveInfinity;

				foreach (IPartGenerator partGenerator in enumerators.Keys)
				{
					float dist = partGenerator.DistanceInCurve;
					if (dist < bestDist)
					{
						best = partGenerator;
						bestDist = dist;
					}
				}

				if (null == best)
				{
					break;
				}

				bool hasMore = enumerators[best].MoveNext();

				if (!hasMore)
				{
					enumerators.Remove(best);
				}

				yield return null;
			}

			yield break;
		}

		/// <summary>
		/// Generates all elements in the <paramref name="spec"/> in a single invocation.
		/// </summary>
		/// <param name="curve"></param>
		/// <param name="spec"></param>
		/// <param name="curveName"></param>
		public ResultData Generate(CubicBezierCurve curve, CurveSpec spec, string curveName)
		{
			return GetSteppingEnumerable(curve, spec, curveName).Last().Value;
		}

		/// <summary>
		/// Generates all elements in the <paramref name="spec"/> in a single invocation.
		/// </summary>
		/// <param name="connection"></param>
		/// <param name="spec"></param>
		/// <param name="curveName"></param>
		public ResultData Generate(Connection connection, CurveSpec spec, string curveName)
		{
			return GetSteppingEnumerable(connection.curve, spec, curveName).Last().Value;
		}


		/// <summary>
		/// Generates a <see cref="ResultData"/> based on the current state of the <see cref="CurveDecorator"/>.
		/// The result is completely detached from this <see cref="CurveDecorator"/> instance, no state changes of this will be visible in the <see cref="ResultData"/>.
		/// Note: This is only valid after at least one step from <see cref="GetSteppingEnumerable"/> has yielded.
		/// </summary>
		/// <returns></returns>
		/// <exception cref="InvalidOperationException"></exception>
		public ResultData CreateResultData()
		{
			var meshes = new List<ResultMesh>();
			var physes = new List<ResultPhys>();

			foreach (var kvp in meshData)
			{
				var key = kvp.Key;
				var data = kvp.Value;

				// todo: implement subMeshes properly

				if (key is MeshSharing)
				{
					meshes.Add(new ResultMesh(CurveSpace2MeshSpace, data.ToMesh()));
				}
				else if (key is PhysSharing)
				{
					physes.Add(new ResultPhys(CurveSpace2MeshSpace, data.ToPhys()));
				}
				else
				{
					throw new InvalidOperationException();
				}
			}

			// Ensure all data does not rely on this instance anymore.
			// So that Clear() does not destroy the results.
			var result = new ResultData(meshes, physes, Tracks.ToArray());

			return result;
		}

		/// <summary>
		/// Contains all the data generated by the <see cref="CurveDecorator"/>.
		/// </summary>
		public readonly struct ResultData
		{
			public readonly IEnumerable<ResultMesh> Meshes;
			public readonly IEnumerable<ResultPhys> Physes;
			public readonly IEnumerable<TrainTrackNode> Tracks;

			internal ResultData
				(IEnumerable<ResultMesh> meshes, IEnumerable<ResultPhys> physes, IEnumerable<TrainTrackNode> tracks)
			{
				Meshes = meshes ?? throw new ArgumentNullException(nameof(meshes));
				Physes = physes ?? throw new ArgumentNullException(nameof(physes));
				Tracks = tracks ?? throw new ArgumentNullException(nameof(tracks));
			}
		}

		public readonly struct ResultMesh
		{
			public readonly Vector3 Position;
			public readonly Mesh Mesh;

			internal ResultMesh(Vector3 position, Mesh mesh)
			{
				Position = position;
				Mesh     = mesh ?? throw new ArgumentNullException(nameof(mesh));
			}
		}

		public readonly struct ResultPhys
		{
			public readonly Vector3 Position;
			public readonly Phys Phys;

			internal ResultPhys(Vector3 position, Phys phys)
			{
				Position = position;
				Phys     = phys ?? throw new ArgumentNullException(nameof(phys));
			}
		}
	}
}
