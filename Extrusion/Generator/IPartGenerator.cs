﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/


using System;
using System.Collections;
using System.Text;

namespace Extrusion.Generator
{
	/// <summary>
	/// Common interface for all the geometry generating components in a <see cref="CurveDecorator"/>.
	/// </summary>
	internal interface IPartGenerator
	{
		/// <summary>
		/// The distance along the curve that the <see cref="IPartGenerator"/>'s last activity occurred.
		/// </summary>
		float DistanceInCurve { get; }

		/// <summary>
		/// The IEnumerable that will generate the content.
		/// </summary>
		/// <returns></returns>
		IEnumerable Generate(IteratorSettings iteratorSettings, int sectionNumber);
	}
}
