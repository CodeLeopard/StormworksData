﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/


using System;
using System.Collections.Generic;
using System.Text;

using BinaryDataModel.DataTypes;

using DataModel.Tiles;

using OpenToolkit.Mathematics;

using Shared;

namespace Extrusion.Generator
{
	internal class MeshData
	{
		private readonly CurveDecorator parent;

		private Bounds3 _bounds = new Bounds3(true);
		private readonly List<VertexRecord> _vertices = new List<VertexRecord>();
		private readonly List<uint> _indices = new List<uint>();

		public Bounds3 Bounds => _bounds;
		public readonly IReadOnlyList<VertexRecord> Vertices;
		public readonly IReadOnlyList<uint> Indices;

		public Int32 VertexCount => _vertices.Count;
		public Int32 IndexCount => _indices.Count;

		/// <summary>
		/// Add to world space vertices to transform them
		/// from curve space to mesh space (so the center of curve is at 0,0,0).
		/// </summary>
		public Vector3 CurveSpace2meshSpace => parent.CurveSpace2MeshSpace;


		internal MeshData(CurveDecorator parent)
		{
			this.parent = parent;
			Vertices = _vertices.AsReadOnly();
			Indices = _indices.AsReadOnly();
		}



		private void InflateBounds(Vector3 v)
		{
			_bounds.Inflate(v);
		}

		internal void Add(VertexRecord vertex)
		{
			vertex.position += CurveSpace2meshSpace;
			InflateBounds(vertex.position);
			_vertices.Add(vertex);
		}

		internal void Add(UInt32 index)
		{
			_indices.Add(index);
		}

		internal void Clear()
		{
			_bounds = new Bounds3(true);
			_vertices.Clear();
			_indices.Clear();
		}

		internal Mesh ToMesh()
		{
			var m = new Mesh(_vertices.ToArray(), _indices.ToArray(), _bounds);

			// todo: name etc.
			return m;
		}

		internal Phys ToPhys()
		{
			var m = ToMesh();
			var p = BinaryDataModel.Converters.Binary.ConvertToVertexOnly(m);

			// todo: name etc.
			return p;
		}
	}
}
