﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/


using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

using BinaryDataModel.DataTypes;

using CurveGraph;

using Extrusion.Extrusion;
using Extrusion.Support;
using OpenToolkit.Mathematics;

using Shared;

using Diag = Extrusion.Diagnostics.ExtruderDiag;

namespace Extrusion.Generator
{
	public class Extruder : IPartGenerator
	{
		#region DataTypes

		public enum MeshKind
		{
			Mesh,
			Phys,
		}

		#endregion DataTypes


		private CurveDecorator generator;

		private readonly Diag Diag;

		public CubicBezierCurve Curve => generator.ActiveCurve;

		public readonly ExtrusionSpec Spec;


		public readonly MeshKind Kind;

		internal readonly MeshData meshData;


		/// <summary>
		/// The index of the current <see cref="LoopSet"/>.
		/// </summary>
		public int LoopSetIndex { [DebuggerStepThrough] get; [DebuggerStepThrough] private set; }

		/// <summary>
		/// The index of the current <see cref="Loop"/> in the current <see cref="LoopSet"/>.
		/// </summary>
		public int LoopIndex { [DebuggerStepThrough] get; [DebuggerStepThrough] private set; }

		/// <summary>
		/// The index of the first new <see cref="Vertex"/> that was generated this iteration.
		/// </summary>
		public int FirstNewVertex { [DebuggerStepThrough] get; [DebuggerStepThrough] private set; }

		/// <summary>
		/// The index of the first new triangle index that was generated this iteration.
		/// </summary>
		public int FirstNewIndex { [DebuggerStepThrough] get; [DebuggerStepThrough] private set; }

		/// <summary>
		/// The current <see cref="LoopSet"/>.
		/// </summary>
		public LoopSet CurrentLoopSet
		{
			get
			{
				switch (Kind)
				{
					case MeshKind.Mesh: return Spec.LoopSets[LoopSetIndex];
					case MeshKind.Phys when null != Spec.PhysicsOverrideSets:
						return Spec.PhysicsOverrideSets[LoopSetIndex];
					case MeshKind.Phys: return Spec.LoopSets[LoopSetIndex];
					default: throw new InvalidOperationException($"Unknown enum value: '{Kind}'");
				}
			}
		}

		public Loop CurrentLoop => CurrentLoopSet.Loops[LoopIndex];

		public List<UInt32> CurrentLineIndices
		{
			get
			{
				switch (Kind)
				{
					case MeshKind.Mesh: return CurrentLoopSet.LineIndices;
					case MeshKind.Phys: return CurrentLoopSet.PhysLineIndices;
					default: throw new InvalidOperationException($"Unknown enum value: '{Kind}'");
				}
			}
		}


		/// <summary>
		/// Simple counter for the amount of steps that have been made.
		/// </summary>
		public int CurrentStepIndex { [DebuggerStepThrough] get; [DebuggerStepThrough] private set; }
		public CurveSample CurrentCurveSample { [DebuggerStepThrough] get; [DebuggerStepThrough] private set; }
		public float TimeInCurve => CurrentCurveSample.timeInCurve;
		public float DistanceInCurve => CurrentCurveSample.distanceInCurve;

		public Matrix4 FinalTransform { [DebuggerStepThrough] get; [DebuggerStepThrough] private set; }


		public Extruder(CurveDecorator generator, ExtrusionSpec spec, MeshKind kind, Diag diag = null)
		{
			this.generator = generator ?? throw new ArgumentNullException(nameof(generator));
			Spec = spec ?? throw new ArgumentNullException(nameof(spec));
			Kind = kind;
			Diag = diag;

			if (kind == MeshKind.Mesh)
			{
				meshData = generator.GetMeshData(spec.MeshSharing);
			}
			else if (kind == MeshKind.Phys)
			{
				meshData = generator.GetMeshData(spec.PhysSharing);
			}
			else
			{
				throw new ArgumentException($"Unexpected enum value: '{kind}'.");
			}

			Diag?.Init(this);
		}

		/// <summary>
		/// Advance to the next loop.
		/// </summary>
		/// <returns><see langword="true"/> if the <see cref="CurrentLoopSet"/> changed and we need to place the starting vertices for that.</returns>
		private bool Advance()
		{
			Diag?.Advance_Before?.Invoke();

			// Advance to the nest Loop.
			LoopIndex++;

			if (LoopIndex < CurrentLoopSet.Loops.Count)
			{
				// Jut go to next Loop in the current LoopSet
				Diag?.Advance_NextLoop?.Invoke();
				Diag?.Advance_After?.Invoke(false);
				return false;
			}

			// Go to the first Loop of the next LoopSet
			LoopIndex = 0;
			LoopSetIndex++;

			if (LoopSetIndex < Spec.LoopSets.Count)
			{
				// Jut go to the next LoopSet
				Diag?.Advance_NextLoopSet?.Invoke();
				Diag?.Advance_After?.Invoke(true);
				return true; // Need to place the first vertices for the new LoopSet.
			}

			// Wrap round to the first LoopSet
			LoopSetIndex = 0;

			if (Spec.LoopSets.Count == 1)
			{
				Diag?.Advance_OneLoopSet?.Invoke();
				Diag?.Advance_After?.Invoke(true);
				return false; // When there is only one LoopSet we don't need to reset ever.
			}

			Diag?.Advance_ResetLoopSet?.Invoke();
			Diag?.Advance_After?.Invoke(true);
			return true; // Need to place the first vertices for the new LoopSet.
		}

		/// <summary>
		/// Each yield return generates one segment.
		/// The first iteration will make two loops so that there is always valid mesh data.
		/// </summary>
		/// <returns></returns>
		public IEnumerable Generate(IteratorSettings iteratorSettings, int sectionNumber)
		{
			try
			{
				uint prevFirstVertex = 0;
				bool hasFirstLoop = false;

				InterpolationSettings[] flattenedSettings =
					Spec.LoopSets.SelectMany(set => set.Loops)
						.Select(loop => loop.InterpolationSettings)
						.ToArray();


				var iterator = new CurveIterator(Curve, iteratorSettings, flattenedSettings, null, Diag?.IteratorDiag);

				Diag?.Generate_Begin?.Invoke();

				foreach (CurveSample sample in iterator.IterateCurve())
				{
					uint firstVertex;

					void DoVertices()
					{
						firstVertex = (uint)meshData.VertexCount;

						var loop = CurrentLoop;

						foreach (VertexRecord vertex in loop.Vertices)
						{
							Vector3 transformedV = (vertex.position.WithW(1) * FinalTransform).Xyz;
							Vector3 transformedN = (vertex.normal.WithW(0) * FinalTransform).Xyz;

							var transformed = vertex;
							transformed.position = transformedV;
							transformed.normal = transformedN;

							meshData.Add(transformed);

							Diag?.Generate_Vertex?.Invoke(vertex, transformed);
						}
					}

					// Remember the indices of the first Vertex and TriangleIndex to be placed this iteration.
					FirstNewVertex = meshData.VertexCount;
					FirstNewIndex = meshData.IndexCount;

					CurrentStepIndex = iterator.CurrentStep;

					CurrentCurveSample = sample;


					Matrix4 SampleTransform = sample.Transformation();
					FinalTransform = Spec.Transform.ComputeFinalTransform(SampleTransform);

					Diag?.Generate_Vertices?.Invoke();

					DoVertices();

					// In the first iteration there is only one loop of vertices, so we cannot create triangles yet.
					if (hasFirstLoop)
					{
						Diag?.Generate_Indices?.Invoke();
						var indices = CurrentLineIndices;

						for (int i = 0; i < indices.Count;)
						{
							var lineStart = indices[i++];
							var lineEnd = indices[i++];

							// Triangle A
							var a0 = firstVertex + lineStart;
							var a1 = firstVertex + lineEnd;
							var a2 = prevFirstVertex + lineStart;

							// Triangle B
							var b0 = prevFirstVertex + lineEnd;
							var b1 = prevFirstVertex + lineStart;
							var b2 = firstVertex + lineEnd;

							if (generator.ReverseTriangles ^ CurrentLoopSet.ReverseTriangles)
							{
								meshData.Add(a2);
								meshData.Add(a1);
								meshData.Add(a0);

								meshData.Add(b2);
								meshData.Add(b1);
								meshData.Add(b0);

								Diag?.Generate_TrianglePair?.Invoke(lineStart, lineEnd, (a2, a1, a0), (b2, b1, b0));
							}
							else
							{
								meshData.Add(a0);
								meshData.Add(a1);
								meshData.Add(a2);

								meshData.Add(b0);
								meshData.Add(b1);
								meshData.Add(b2);

								Diag?.Generate_TrianglePair?.Invoke(lineStart, lineEnd, (a0, a1, a2), (b0, b1, b2));
							}
						}
					}

					prevFirstVertex = firstVertex;

					// In the first iteration there are no triangles yet. Only yield when there are valid triangles.
					if (hasFirstLoop)
					{
						Diag?.Generate_Yield?.Invoke();
						yield return null;
					}

					hasFirstLoop = true;

					if (Advance())
					{
						Diag?.Generate_ExtraVertices?.Invoke();
						// When changing between loopSets we need to start the new LoopSet.
						DoVertices();

						// Only when the loopSet has more than 1 loop we also advance to the next loop.
						// When there is only one loop the expectation is that one segment is generated using that loop
						// which means it must be placed twice so triangles can be created between those loops.
						if (CurrentLoopSet.Loops.Count > 1)
						{
							Advance();
						}

						prevFirstVertex = firstVertex;
					}

					Diag?.Generate_EndOfStep?.Invoke();
				}
				Diag?.Done?.Invoke();
			}
			finally
			{
				Diag?.Finally?.Invoke();
			}
		}
	}
}
