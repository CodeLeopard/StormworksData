﻿// Copyright 2022-2023 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/


using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

using BinaryDataModel.DataTypes;
using Shared;
using Shared.Validation;

namespace Extrusion.Support
{
	/// <summary>
	/// Specifies how elements should be merged (or not) into (sub) meshes.
	/// </summary>
	public abstract class MeshOrPhysSharing : IEquatable<MeshOrPhysSharing>, IValidateable
	{
		// IEquatable required to have proper equality behaviour in a HashSet or Dictionary

		public const string ParentSharedID = "Parent";
		public const string MainSharedID = "Main";

		/// <summary>
		/// Specifies how the element should share mesh.
		/// When set to <see cref="ParentSharedID"/> it will merge into whatever mesh is defined for the parent.
		/// When set to <see cref="MainSharedID"/> it will always merge into the main mesh for a given curve.
		/// When set to <see langword="null"/> or empty string it will always generate a unique mesh.
		/// When set to any other value it will share with any other uses of that same value.
		/// </summary>
		public string SharedMesh = ParentSharedID;

		/// <summary>
		/// Specifies how the element should share subMesh within a mesh.
		/// In the case of <see cref="MeshSharing"/>: All identifiers are unique to the selected <see cref="MeshSharing.Shader"/>.
		/// When set to <see cref="ParentSharedID"/> it will merge into whatever mesh is defined for the parent.
		/// When set to <see cref="MainSharedID"/> it will always merge into the main mesh for a given curve.
		/// When set to <see langword="null"/> or empty string it will always generate a unique mesh.
		/// When set to any other value it will share with any other uses of that same value.
		/// </summary>
		public string SharedSubMesh = ParentSharedID;

		/// <summary>
		/// The ID of the section, this is used by the CurveDecorator to split a mesh into multiple parts if the curve is very long.
		/// It is not exposed publicly.
		/// </summary>
		internal int SectionID = 0;

		protected void CloneTo(MeshOrPhysSharing destination)
		{
			destination.SharedMesh = SharedMesh;
			destination.SharedSubMesh = SharedSubMesh;
			destination.SectionID = SectionID;
		}

		public virtual bool Equals(MeshOrPhysSharing other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return SharedMesh    == other.SharedMesh
				&& SharedSubMesh == other.SharedSubMesh
				&& SectionID     == other.SectionID;
		}

		/// <inheritdoc />
		public static bool operator == (MeshOrPhysSharing left, MeshOrPhysSharing right)
		{
			if (ReferenceEquals(left, right) ) return true;
			if (ReferenceEquals(left, null)) return false;
			return left.Equals(right);
		}

		/// <inheritdoc />
		public static bool operator !=(MeshOrPhysSharing left, MeshOrPhysSharing right)
		{
			return ! (left == right);
		}

		/// <inheritdoc />
		public override int GetHashCode()
		{
			var hb = new HashCodeBuilder();
			hb.Add(SharedMesh);
			hb.Add(SharedSubMesh);
			hb.Add(SectionID);
			return hb.GetHashCode();
		}

		/// <inheritdoc />
		public virtual void Validate(Validator v) { }

		public virtual object UntypedClone()
		{
			return MemberwiseClone();
		}
	}
}
