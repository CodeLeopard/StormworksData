﻿// Copyright 2022-2023 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Runtime.Serialization;

using Shared.Validation;

namespace Extrusion.Support
{
	/// <summary>
	/// Settings to adjust the interpolation process.
	/// </summary>
	[Serializable]
	[DataContract]
	public class InterpolationSettings : IValidateable
	{
		/// <summary>
		/// The minimum distance between interpolations (length of segments).
		/// </summary>
		[DataMember]
		public float MinInterpolationDistance = 1f;
		/// <summary>
		/// The maximum distance between interpolations (length of segments).
		/// </summary>
		[DataMember]
		public float MaxInterpolationDistance = 100;

		/// <summary>
		/// The maximum angle, in degrees, between the 'forward direction' of interpolated segments.
		/// </summary>
		[DataMember]
		public float MaxAngleBetweenInterpolations = 1;

		/// <summary>
		/// The maximum offset between the real curve and the middle of the segment.
		/// </summary>
		[DataMember]
		public float MaxSegmentOffset = 1;

		/// <summary>
		/// The maximum angle, in degrees, between the true curve and the segment, checked at the middle of the segment.
		/// </summary>
		[DataMember]
		public float MaxSegmentCenterAngle = 1;

		/// <summary>
		/// Bias towards making longer segments rather than shorter when segment lengths need to be adjusted to prevent ending with a constraint breaking segment.
		/// This is a multiplier that applies to the 'shrinked past constraint' value when it is compared against the 'stretched past constraint' value.
		/// </summary>
		[DataMember]
		public float ShrinkReluctance = 2f;


		/// <inheritdoc />
		public void Validate(Validator v)
		{
			v.ValidateFloatFinite(MinInterpolationDistance, nameof(MinInterpolationDistance));
			v.Error(MinInterpolationDistance <= 0,
			           $"{nameof(MinInterpolationDistance)} should be > 0.");

			v.ValidateFloatFinite(MaxInterpolationDistance, nameof(MaxInterpolationDistance));
			v.Error(MinInterpolationDistance > MaxInterpolationDistance,
			           $"{nameof(MinInterpolationDistance)} should be < {nameof(MaxInterpolationDistance)}.");

			v.ValidateFloatFinite(MaxAngleBetweenInterpolations, nameof(MaxAngleBetweenInterpolations));
			v.Error(MaxAngleBetweenInterpolations <= 0 || MaxAngleBetweenInterpolations > 360,
			           $"{nameof(MaxAngleBetweenInterpolations)} should be 0 < x < 360 but was {MaxAngleBetweenInterpolations}");

			v.ValidateFloatFinite(MaxSegmentOffset, nameof(MaxSegmentOffset));
			v.Error(MaxSegmentOffset <= 0,
			           $"{nameof(MaxSegmentOffset)} should be > 0.");

			v.ValidateFloatFinite(ShrinkReluctance, nameof(ShrinkReluctance));
			v.Error(ShrinkReluctance <= 0,
			           $"{nameof(ShrinkReluctance)} should be > 0.");
		}

		public InterpolationSettings Clone()
		{
			return (InterpolationSettings)MemberwiseClone();
		}

		public static InterpolationSettings Default => new InterpolationSettings();
	}
}
