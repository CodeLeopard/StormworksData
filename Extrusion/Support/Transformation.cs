﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/


using System;
using System.Collections.Generic;
using System.Text;

using OpenToolkit.Mathematics;

using Shared;
using Shared.Validation;

namespace Extrusion.Support
{
	public class Transformation : IValidateable
	{
		public Transformation Parent = null;


		/// <summary>
		/// Ignore all rotation around the X axis and use only the rotation specified in <see cref="Transform"/>.
		/// </summary>
		public bool LockRotationX = false;
		/// <summary>
		/// Ignore all rotation around the Y axis and use only the rotation specified in <see cref="Transform"/>.
		/// </summary>
		public bool LockRotationY = false;
		/// <summary>
		/// Ignore all rotation around the Z axis and use only the rotation specified in <see cref="Transform"/>.
		/// </summary>
		public bool LockRotationZ = false;

		/// <summary>
		/// Transform from the <see cref="CurveGraph.CurveSample"/> to where <see cref="Transform"/> will be applied.
		/// This only matters when there are locked rotation axis, because those locked rotations are computed after this.
		/// Using <see cref="AnchorTransform"/> you can transform inside the coordinate system of the sample and only after that lock the rotations to world space.
		/// </summary>
		public Matrix4 AnchorTransform = Matrix4.Identity;

		/// <summary>
		/// Transformation combined with <see cref="CurveGraph.CurveSample"/>
		/// to create the final transformation for the Mesh or VertexRecord.
		/// </summary>
		public Matrix4 Transform = Matrix4.Identity;

		/// <summary>
		/// Gets or sets the Local Position encoded in the <see cref="Transform"/>.
		/// </summary>
		public Vector3 LocalPosition
		{
			get => Transform.Row3.Xyz;
			set => Transform.Row3.Xyz = value;
		}

		/// <summary>
		/// Gets or sets the Local Scale encoded by the <see cref="Transform"/>.
		/// </summary>
		public Vector3 LocalScale
		{
			get => Transform.ExtractScale();
			set
			{
				Transform.Row0.Xyz = Transform.Row0.Xyz.Normalized() * value.X;
				Transform.Row1.Xyz = Transform.Row1.Xyz.Normalized() * value.Y;
				Transform.Row2.Xyz = Transform.Row2.Xyz.Normalized() * value.Z;
			}
		}

		/// <summary>
		/// Gets or sets the Local Rotation encoded by the <see cref="Transform"/> as Pitch (X), Yaw (Y), Roll (Z) angles in radians.
		/// </summary>
		public Vector3 LocalRotationRadians
		{
			get => Transform.ExtractRotation(true).ToEulerAngles();
			set
			{
				var quaternion = new Quaternion(value);
				LocalQuaternion = quaternion;
			}
		}

		/// <summary>
		/// Adds to the Local rotation encoded by the <see cref="Transform"/> as Pitch (X), Yaw (Y), Roll (Z) angles in radians.
		/// This adds to the rotation already there. This allows you to specify the rotation in steps, which may be easier to deal with.
		/// </summary>
		public Vector3 AddLocalRotationRadians
		{
			set
			{
				var quaternion = new Quaternion(value);
				AddLocalQuaternion = quaternion;
			}
		}

		/// <summary>
		/// Gets or sets the Local Rotation encoded by the <see cref="Transform"/> as Pitch (X), Yaw (Y), Roll (Z) angles in degrees.
		/// </summary>
		public Vector3 LocalRotationDegrees
		{
			get => LocalRotationRadians * (float)(180 / Math.PI);
			set => LocalRotationRadians = value * (float)(Math.PI / 180);
		}

		/// <summary>
		/// Adds to the Local rotation encoded by the <see cref="Transform"/> as Pitch (X), Yaw (Y), Roll (Z) angles in degrees.
		/// This adds to the rotation already there. This allows you to specify the rotation in steps, which may be easier to deal with.
		/// </summary>
		public Vector3 AddLocalRotationDegrees
		{
			set => AddLocalRotationRadians = value * (float)(Math.PI / 180);
		}

		/// <summary>
		/// Gets or sets the Local Rotation encoded by the <see cref="Transform"/> as a <see cref="Quaternion"/>.
		/// </summary>
		public Quaternion LocalQuaternion
		{
			get => Transform.ExtractRotation(true);
			set
			{
				var position = LocalPosition;
				var scale = LocalScale;

				var sca = Matrix4.CreateScale(scale);
				var tra = Matrix4.CreateTranslation(position);
				var rot = Matrix4.CreateFromQuaternion(value);
				// 1: scale around the origin.
				// 2: rotate the scaled stuff around the origin.
				// 3: translate the result of the previous operations.

				Transform = sca * rot * tra;
			}
		}

		/// <summary>
		/// Adds to the Local Rotation encoded by the <see cref="Transform"/> as a <see cref="Quaternion"/>.
		/// </summary>
		public Quaternion AddLocalQuaternion
		{
			set
			{
				var trans = Transform.ExtractTranslation();

				var trto = Matrix4.CreateTranslation(-trans); // Move to origin.
				var trfo = Matrix4.CreateTranslation(trans);  // Move back to position.

				// 1: translate to origin.
				// 2: rotate (around origin).
				// 3: translate back to position.
				var atOrigin = Transform * trto;
				var rotated = atOrigin   * Matrix4.CreateFromQuaternion(value);
				var result = rotated     * trfo;

				Transform = result;
			}
		}

		/// <summary>
		/// Compute the Final Transformation.
		/// </summary>
		/// <param name="sampleTransform"></param>
		/// <returns></returns>
		public Matrix4 ComputeFinalTransform(Matrix4 sampleTransform)
		{
			Matrix4 parentTransform = Parent?.ComputeFinalTransform(sampleTransform) ?? sampleTransform;


			if (!LockRotationX && !LockRotationY && !LockRotationZ)
			{
				return Transform * parentTransform;
			}

			if (LockRotationX && LockRotationY && LockRotationZ)
			{
				// Easy case.
				parentTransform = parentTransform.ClearRotation();
				return Transform * parentTransform;
			}

			// For each axis: compute the rotation on that axis and remove it.

			// First extract the components of the transform so it can be re-assembled later.
			var rotation = parentTransform.ExtractRotation(true);
			var scale = parentTransform.ExtractScale();
			var translation = parentTransform.ExtractTranslation();

#if DEBUG
			var eulerAnglesDegrees = rotation.ToEulerAngles() * (float) (180 / Math.PI);
#endif

			if (LockRotationX)
			{
				rotation = RemoveRotationAxis(rotation, Vector3.UnitX);
			}
			if (LockRotationY)
			{
				rotation = RemoveRotationAxis(rotation, Vector3.UnitY);
			}
			if (LockRotationZ)
			{
				rotation = RemoveRotationAxis(rotation, Vector3.UnitZ);
			}


#if DEBUG
			var afterEulerAnglesDegrees = rotation.ToEulerAngles() * (float)(180 / Math.PI);
#endif


			parentTransform = MatrixExt.CreateTransScaleRot(translation, scale, rotation);

			return Transform * parentTransform;
		}

		private static Quaternion RemoveRotationAxis(Quaternion q, Vector3 r)
		{
			// https://math.stackexchange.com/a/357991


			Vector3 p = new Vector3(r.Y, -r.X, r.Z); // Any vector perpendicular to r.

			// Find out how p is rotated by q
			Vector3 p1 = q * p;

			// Project p1 onto the plane of r
			var p11 = ProjectOnPlane(p1, r);

			// Calculate angle between p and p11
			float o = Vector3.CalculateAngle(p, p11);

#if DEBUG
			// Degrees
			float d = o * (float)(180 / Math.PI);
#endif

			// Create a Quaternion that rotates by o around r
			Quaternion z = Quaternion.FromAxisAngle(r, -o);

			// Create a Quaternion that has the rotation removed.
			Quaternion w = z * q;

			return w;
		}

		private static Vector3 ProjectOnPlane(Vector3 v, Vector3 plane)
		{
			float squareLen = plane.LengthSquared;

			if (squareLen == 0)
			{
				// Invalid plane.
				return v;
			}

			float dot = Vector3.Dot(v, plane);

			var w = new Vector3(
				v.X - plane.X * dot / squareLen,
				v.Y - plane.Y * dot / squareLen,
				v.Z - plane.Z * dot / squareLen);

			return w;
		}

		/// <inheritdoc />
		public void Validate(Validator v)
		{
			v.Error(!Transform.IsFinite(), $"{nameof(Transform)} was not finite: {Transform}.");
			v.Error(!AnchorTransform.IsFinite(), $"{nameof(AnchorTransform)} was not finite: {AnchorTransform}.");
		}

		public Transformation Clone()
		{
			var r = Identity;
			r.Parent = Parent;
			r.LockRotationX = LockRotationX;
			r.LockRotationY = LockRotationY;
			r.LockRotationZ = LockRotationZ;
			r.AnchorTransform = AnchorTransform;
			r.Transform = Transform;
			return r;
		}

		public Transformation Clone(Transformation parentOverride)
		{
			var r = Clone();
			r.Parent = parentOverride;
			return r;
		}


		#region Static
		public static Transformation Identity => new Transformation();


		#endregion Static

	}
}
