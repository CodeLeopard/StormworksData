﻿// Copyright 2022-2023 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/


using System;
using System.Collections.Generic;
using System.Text;

namespace Extrusion.Support
{
	public class PhysSharing : MeshOrPhysSharing, IEquatable<MeshOrPhysSharing>, IEquatable<PhysSharing>
	{
		// IEquatable required to have proper equality behaviour in a HashSet or Dictionary

		/// <summary>
		/// Returns a new instance with the default settings.
		/// </summary>
		public static PhysSharing Default => new PhysSharing();

		/// <inheritdoc />
		public bool Equals(PhysSharing other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return base.Equals(other);
		}

		/// <inheritdoc />
		public override bool Equals(MeshOrPhysSharing other)
		{
			return Equals((object)other);
		}

		/// <inheritdoc />
		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj is PhysSharing typedObj)
			{
				return Equals(typedObj);
			}
			else
			{
				return false;
			}
		}

		/// <inheritdoc />
		public override int GetHashCode()
		{
			// ReSharper disable once BaseObjectGetHashCodeCallInGetHashCode
			// The base hashCode operates on the fields of the base class.
			// This class is only a type: it does not add any fields.
			return base.GetHashCode();
		}

		public override string ToString()
		{
			return $"{{PhysSharing Mesh:{SharedMesh} SubMesh:{SharedSubMesh} Section:{SectionID}}}";
		}

		public override object UntypedClone()
		{
			return MemberwiseClone();
		}


		public PhysSharing Clone()
		{
			return (PhysSharing) UntypedClone();
		}
	}
}
