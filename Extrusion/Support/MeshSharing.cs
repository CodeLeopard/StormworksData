﻿// Copyright 2022-2023 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/


using System;
using System.Collections.Generic;
using System.Text;

using BinaryDataModel.DataTypes;
using BinaryDataModel.DataTypes.Enums;
using Shared;
using Shared.Validation;

namespace Extrusion.Support
{
	public class MeshSharing : MeshOrPhysSharing, IEquatable<MeshOrPhysSharing>, IEquatable<MeshSharing>
	{
		// IEquatable required to have proper equality behaviour in a HashSet or Dictionary

		/// <summary>
		/// The shader to be used.
		/// </summary>
		public Shader Shader = Shader.Opaque;

		/// <summary>
		/// Returns a new instance with the default settings.
		/// </summary>
		public static MeshSharing Default => new MeshSharing();

		/// <inheritdoc />
		public bool Equals(MeshSharing other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return base.Equals(other)
			    && Shader == other.Shader;
		}

		/// <inheritdoc />
		public override bool Equals(MeshOrPhysSharing other)
		{
			return Equals((object) other);
		}

		/// <inheritdoc />
		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj is MeshSharing typedObj)
			{
				return Equals(typedObj);
			}
			else
			{
				return false;
			}
		}

		/// <inheritdoc />
		public override int GetHashCode()
		{
			var hb = new HashCodeBuilder();
			hb.Add(base.GetHashCode());
			hb.Add(Shader);
			return hb.GetHashCode();
		}


		/// <inheritdoc />
		public override void Validate(Validator v)
		{
			// Base has nothing to validate: strings may be null or empty.
			v.Validate(Shader, nameof(Shader));
		}

		public override string ToString()
		{
			return $"{{MeshSharing Mesh:{SharedMesh} SubMesh:{SharedSubMesh} Section:{SectionID} Sshader:{Shader}}}";
		}

		public override object UntypedClone()
		{
			return MemberwiseClone();
		}

		public MeshSharing Clone()
		{
			return (MeshSharing)UntypedClone();
		}
	}
}
