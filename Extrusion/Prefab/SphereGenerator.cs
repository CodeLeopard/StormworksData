﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/


using System;

using BinaryDataModel.DataTypes;

using OpenToolkit.Mathematics;

using Shared;

namespace Extrusion.Prefab
{
	public class SphereGenerator
	{
		public int resolution = 20;
		public float radius = 1f;
		public Color4 color = Color4.White;

		private const int faces = 6;
		private const int trianglesPerQuad = 2;
		private const int indicesPerTriangle = 3;

		private static readonly Vector3[] directions = new Vector3[]
		{
			Vector3.UnitX, Vector3.UnitY, Vector3.UnitZ, -Vector3.UnitX, -Vector3.UnitY, -Vector3.UnitZ,
		};

		private SphereFaceSection[] sections = new SphereFaceSection[6];


		private void Generate()
		{
			for (int i = 0; i < sections.Length; i++)
			{
				sections[i] = new SphereFaceSection(this, directions[i]);
				sections[i].Generate();
			}
		}


		public Mesh GetMergedMesh()
		{
			Generate();

			Bounds3 bounds = new Bounds3(true);
			VertexRecord[] vertices = new VertexRecord[resolution * resolution * faces];
			uint[] indices = new uint[(resolution - 1) * (resolution - 1) * trianglesPerQuad * indicesPerTriangle * faces];

			int vertexIndex = 0;
			int indiceIndex = 0;

			foreach (SphereFaceSection sphereFaceSection in sections)
			{
				int firstVertex = vertexIndex;
				foreach (VertexRecord v in sphereFaceSection.vertices)
				{
					bounds.Inflate(v.position);
					vertices[vertexIndex++] = v;
				}

				foreach (uint index in sphereFaceSection.indices)
				{
					indices[indiceIndex++] = (uint)(index + firstVertex);
				}
			}

			return new Mesh(vertices, indices, bounds);
		}



		internal class SphereFaceSection
		{
			private SphereGenerator parent;
			private int resolution => parent.resolution;

			private float radius => parent.radius;

			private Color4 color => parent.color;

			private readonly Vector3 localUp;
			private readonly Vector3 axisA;
			private readonly Vector3 axisB;

			internal readonly VertexRecord[] vertices;
			internal readonly uint[] indices;

			public SphereFaceSection(SphereGenerator parent, Vector3 localUp)
			{
				this.parent = parent;
				this.localUp = localUp;
				axisA = new Vector3(localUp.Y, localUp.Z, localUp.X);
				axisB = Vector3.Cross(localUp, axisA);

				vertices = new VertexRecord[resolution * resolution];
				indices = new uint[(resolution - 1) * (resolution - 1) * trianglesPerQuad * indicesPerTriangle];
			}

			/// <summary>
			/// Corrects for the stretching that occurs when inflating the cube into a sphere,
			/// where the triangles close to the seams would be smaller.
			/// </summary>
			/// <param name="v"></param>
			/// <returns></returns>
			private Vector3 StretchCorrection(Vector3 v)
			{
				return v;

				// todo: fix math, this is super wrong.
				float x2 = v.X * v.X;
				float y2 = v.Y * v.Y;
				float z2 = v.Z * v.Z;

				Vector3 s = new Vector3
					(
					 v.X * (float)Math.Sqrt(1f - y2 / 2f - z2 / 2f + y2 * z2 / 3f)
				   , v.Y * (float)Math.Sqrt(1f - x2 / 2f - z2 / 2f + x2 * z2 / 3f)
				   , v.Z * (float)Math.Sqrt(1f - x2 / 2f - y2 / 2f + x2 * y2 / 3f)
					);

				return s;
			}

			public void Generate()
			{
				int ii = 0;

				void AddIndex(int index)
				{
					indices[ii++] = (uint)index;
				}

				for (int y = 0; y < resolution; y++)
				{
					for (int x = 0; x < resolution; x++)
					{
						int i = x + y * resolution;

						Vector2 percent = new Vector2(x, y) / (resolution - 1);

						Vector3 unitCubePt = localUp + (percent.X - 0.5f) * 2 * axisA + (percent.Y - 0.5f) * 2 * axisB;
						Vector3 unitSpherePt = unitCubePt.Normalized();

						unitSpherePt = StretchCorrection(unitSpherePt);

						VertexRecord v = new VertexRecord();
						v.position = unitSpherePt * radius;
						v.normal = unitSpherePt;
						v.color = color;
						vertices[i] = v;

						if (y < resolution - 1
						 && x < resolution - 1)
						{
							AddIndex(i);
							AddIndex(i + resolution + 1);
							AddIndex(i + resolution);

							AddIndex(i);
							AddIndex(i + 1);
							AddIndex(i + resolution + 1);
						}
					}
				}
			}
		}
	}


}
