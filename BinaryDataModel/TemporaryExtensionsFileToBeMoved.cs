﻿// Copyright 2024 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System.Collections.Generic;
using System.Linq;

namespace BinaryDataModel
{
	// todo: On some other branch there was an extensions class in Shared. But at the time of writing it isn't there yet.
	// This stuff should go in there.
	internal static class TemporaryExtensionsFileToBeMoved
	{
		/// <summary>
		/// If <paramref name="instance"/> is already a <see cref="List{T}"/> returns it, otherwise creates a new <see cref="List{T}"/>.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="instance"></param>
		/// <returns></returns>
		internal static List<T> AsOrToList<T>(this IEnumerable<T> instance)
		{
			return instance as List<T> ?? instance.ToList();
		}

		/// <summary>
		/// If <paramref name="instance"/> is already a <see cref="List{T}"/> returns it, otherwise creates a new <see cref="List{T}"/>.
		/// If <paramref name="instance"/> was <see langword="null"/> then returns <see langword="null"/>.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="instance"></param>
		/// <returns></returns>
		internal static List<T>? AsOrToListNullable<T>(this IEnumerable<T>? instance)
		{
			return instance as List<T> ?? instance?.ToList();
		}
	}
}
