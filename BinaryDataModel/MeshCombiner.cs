﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

using BinaryDataModel.DataTypes;
using BinaryDataModel.DataTypes.Enums;
using OpenToolkit.Mathematics;

using Shared;

namespace BinaryDataModel
{
	/// <summary>
	/// Combines multiple <see cref="Mesh"/> into a single.
	/// </summary>
	public static class MeshCombiner
	{
		/// <summary>
		/// Information on a <see cref="Mesh"/> to be combined.
		/// </summary>
		public readonly struct DataItem
		{
			/// <summary>
			/// The mesh.
			/// </summary>
			public readonly Mesh mesh;

			/// <summary>
			/// The transformation to apply.
			/// </summary>
			public readonly Matrix4 transform;

			/// <summary>
			/// The paint color that will be applied to paintable vertices.
			/// </summary>
			public readonly Color4[] paintColors;

			/// <summary>
			/// Should paint be applied.
			/// </summary>
			public readonly bool doPaint;

			/// <summary>
			/// Force-Apply paint to every vertex, not just paintable ones.
			/// </summary>
			public readonly bool forcePaintColor;

			/// <summary>
			/// Reverse the triangles, this may be needed for some transformations.
			/// </summary>
			public readonly bool reverseTriangles;

			/// <summary>
			/// When <see langword="true"/> the last value in <see cref="paintColors"/> will be used to override all color on emissive mesh data.
			/// </summary>
			public readonly bool forceEmissiveColor;

			//[RequireNamedOptionalArgs]
			/// <summary>
			/// 
			/// </summary>
			/// <param name="mesh"></param>
			/// <param name="transform"></param>
			/// <param name="paintColor"></param>
			/// <param name="reverseTriangles"></param>
			/// <param name="forcePaintColor"></param>
			/// <param name="forceEmissiveColor"></param>
			public DataItem
			(
				Mesh mesh
			  , Matrix4 transform
			  , Color4 paintColor
			  , bool reverseTriangles = false
			  , bool forcePaintColor = false
			  , bool forceEmissiveColor = false
			)
				: this
					(
					 mesh
				   , transform
				   , new Color4[1] { paintColor }
				   , reverseTriangles
				   , forcePaintColor
				   , forceEmissiveColor
					)
			{ }

			//[RequireNamedOptionalArgs]
			/// <summary>
			/// 
			/// </summary>
			/// <param name="mesh"></param>
			/// <param name="transform"></param>
			/// <param name="paintColors"></param>
			/// <param name="reverseTriangles"></param>
			/// <param name="forcePaintColor"></param>
			/// <param name="forceEmissiveColor"></param>
			/// <exception cref="ArgumentNullException"></exception>
			public DataItem(
				Mesh mesh,
				Matrix4 transform,
				Color4[] paintColors,
				bool reverseTriangles = false,
				bool forcePaintColor = false,
				bool forceEmissiveColor = false)
			{
				this.mesh = mesh ?? throw new ArgumentNullException(nameof(mesh));
				this.transform = transform;

				this.paintColors = paintColors;
				this.doPaint = true;
				this.forcePaintColor = forcePaintColor;
				this.reverseTriangles = reverseTriangles;
				this.forceEmissiveColor = forceEmissiveColor;
			}

			//[RequireNamedOptionalArgs]
			/// <summary>
			/// 
			/// </summary>
			/// <param name="mesh"></param>
			/// <param name="transform"></param>
			/// <param name="reverseTriangles"></param>
			/// <exception cref="ArgumentNullException"></exception>
			public DataItem(Mesh mesh, Matrix4 transform, bool reverseTriangles = false)
			{
				this.mesh = mesh ?? throw new ArgumentNullException(nameof(mesh));
				this.transform = transform;
				this.paintColors = Array.Empty<Color4>();
				this.doPaint = false;
				this.forcePaintColor = false;
				this.reverseTriangles = reverseTriangles;
				this.forceEmissiveColor = false;
			}
		}

		private static Vector4 ColorFromBytes(byte r, byte g, byte b, byte a)
		{
			unchecked
			{
				var result = new Vector4
					(
					 r / (float)byte.MaxValue
				   , g / (float)byte.MaxValue
				   , b / (float)byte.MaxValue
				   , a / (float)byte.MaxValue
					);
				return result;
			}
		}

		private static Color4 ColorFromInteger(uint rgba)
		{
			unchecked
			{
				var r = (byte)(rgba >> (8 * 0));
				var g = (byte)(rgba >> (8 * 1));
				var b = (byte)(rgba >> (8 * 2));
				var a = (byte)(rgba >> (8 * 3));
				return new Color4(r, g, b, a);
			}
		}

		/// <summary>
		/// The colors that are used for paintable zones. Each color is represents a zone that the user can paint.
		/// </summary>
		public static readonly Color4[] paintZoneColors;

		static MeshCombiner()
		{
			paintZoneColors = new Color4[]
			{
				new Color4(255, 125, 0, 255),
				new Color4(154, 125, 0, 255),
				new Color4(55,  125, 0, 255),
			};
		}

		/// <summary>
		/// Apply the <paramref name="transformation"/> to <paramref name="mesh"/>, creating a new <see cref="Mesh"/>.
		/// </summary>
		/// <param name="mesh">The <see cref="Mesh"/> to transform</param>
		/// <param name="transformation">The transformation to apply.</param>
		/// <param name="reverseTriangles">
		/// Should triangles be reversed.
		/// This may be needed when the transformation contains negative scale component.
		/// </param>
		/// <returns>A new <see cref="Mesh"/> with <paramref name="transformation"/> applied to it.</returns>
		public static Mesh Transform(Mesh mesh, Matrix4 transformation, bool reverseTriangles = false)
		{
			return Combine(new [] { new DataItem(mesh, transformation, reverseTriangles: reverseTriangles) });
		}

		/// <summary>
		/// Apply the <paramref name="transformation"/> to <paramref name="mesh"/>, creating a new <see cref="Phys"/>.
		/// </summary>
		/// <param name="mesh">The <see cref="Phys"/> to transform</param>
		/// <param name="transformation">The transformation to apply.</param>
		/// <param name="reverseTriangles">
		/// Should triangles be reversed.
		/// This may be needed when the transformation contains negative scale component.
		/// </param>
		/// <returns>A new <see cref="Phys"/> with <paramref name="transformation"/> applied to it.</returns>
		public static Phys Transform(Phys mesh, Matrix4 transformation, bool reverseTriangles = false)
		{
			mesh = mesh.Clone();

			foreach (var subMesh in mesh.subMeshes)
			{
				var vertices = subMesh.vertices;
				for (int i = 0; i < vertices.Count; i++)
				{
					Vector4 vertex = vertices[i].WithW(1);
					Vector4 transformed = transformation * vertex;
					vertices[i] = transformed.Xyz;
				}
			}

			if (reverseTriangles)
			{
				foreach (var subMesh in mesh.subMeshes)
				{
					if (subMesh.hasIndices)
					{
						var indices = subMesh.indices;
						for (int i = 0; i < indices.Count; i += 3)
						{
							UInt32 a = indices[i + 0];
							UInt32 b = indices[i + 1];
							UInt32 c = indices[i + 2];

							indices[i + 0] = c;
							indices[i + 1] = b;
							indices[i + 2] = a;
						}
					}
					else
					{
						var vertices = subMesh.vertices;
						for (int i = 0; i < vertices.Count; i += 3)
						{
							Vector3 a = vertices[i + 0];
							Vector3 b = vertices[i + 1];
							Vector3 c = vertices[i + 2];

							vertices[i + 0] = c;
							vertices[i + 1] = b;
							vertices[i + 2] = a;
						}
					}
				}
			}

			return mesh;
		}

		/// <summary>
		/// Combine the list of meshes into a new <see cref="Mesh"/>.
		/// </summary>
		/// <param name="meshes">Collection of meshes and metaData.</param>
		/// <param name="mergeSubMeshes">Merge SubMeshes by <see cref="SubMesh.shaderId"/></param>.
		/// <param name="deduplicateVertexRecords">Use <see cref="Mesh.DeduplicateVertices"/> to reduce the size of the combined mesh.</param>
		/// <returns></returns>
		public static Mesh Combine(IEnumerable<DataItem> meshes, bool mergeSubMeshes = false, bool deduplicateVertexRecords = false)
		{
			var allVertices  = new List<VertexRecord>();
			var allIndices   = new List<UInt32>();
			var allSubMeshes = new List<SubMesh>();
			var result = new Mesh(allVertices, allIndices, allSubMeshes, true);

			int meshIndex = 0;

			int vertexIndex = 0;
			int indiceIndex = 0;
			int subMeshIndex = 0;

			foreach (var item in meshes)
			{
				UInt32 meshFirstVertex = (UInt32) allVertices.Count;
				UInt32 meshFirstIndex = (UInt32) allIndices.Count;


				var mesh = item.mesh;
				var transform = item.transform;

				// Normals should only be rotated.
				// Note: if scale is always 1 we can just apply the normal transform to the vector3 directly.
				var normalTransform = transform;

				var sourceVertices = mesh.vertices;

				for (int i = 0; i < sourceVertices.Count; i++)
				{
					// struct is copy by value.
					var record = sourceVertices[i];
					{
						// We need a vector4 with w = 1 to apply translation, that's just how matrices work.
						var vertex = new Vector4(record.position, 1);

						record.position = (transform * vertex).Xyz;
					}
					{
						var normal = new Vector4(record.normal, 0);

						record.normal = (normalTransform * normal).Xyz;
					}
					if (item.forcePaintColor)
					{
						record.color = item.paintColors[0];
					}
					else if(item.doPaint)
					{
						int index = Array.IndexOf(paintZoneColors, record.color);

						if (index >= 0)
						{
							index = Math.Min(index, item.paintColors.Length - 1);

							record.color = item.paintColors[index];
						}
					}

					allVertices.Add(record);
				}

				if (item.reverseTriangles)
				{
					for (int i = 0; i < mesh.indices.Count;)
					{
						var a = meshFirstVertex + mesh.indices[i + 0];
						var b = meshFirstVertex + mesh.indices[i + 1];
						var c = meshFirstVertex + mesh.indices[i + 2];

						allIndices.Add(c);
						allIndices.Add(b);
						allIndices.Add(a);

						indiceIndex += 3;
						i += 3;
					}
				}
				else
				{
					for (int i = 0; i < mesh.indices.Count; i++)
					{
						allIndices.Add(meshFirstVertex + mesh.indices[i]);
					}
				}

				int subMeshFrontier = allSubMeshes.Count;
				for (int i = 0; i < mesh.subMeshes.Count; i++)
				{
					var sm = mesh.subMeshes[i];

					var newSm = new SubMesh(result, $"M#{meshIndex}/{sm.name}", new Bounds3(true));
					newSm.shaderId = sm.shaderId;

					newSm.indexBufferStart = meshFirstIndex + sm.indexBufferStart;
					newSm.indexBufferLength = sm.indexBufferLength;

					allSubMeshes.Add(newSm);
				}

				if (item.forceEmissiveColor)
				{
					var emissiveColor = item.paintColors.Last();
					for (int smi = subMeshFrontier; smi < allSubMeshes.Count; smi++)
					{
						var sm = allSubMeshes[smi];
						if (sm.shaderId != Shader.Emissive)
							continue;

						uint indexBufferEnd = sm.indexBufferStart + sm.indexBufferLength;
						for (uint i = sm.indexBufferStart; i < indexBufferEnd; i++)
						{
							var vi = allIndices[(int) i];

							var v = allVertices[(int)vi];

							v.color = emissiveColor;

							allVertices[(int) vi] = v;
						}
					}
				}

				meshIndex++;
			}


			// Bounds ware invalidated by transformation.
			result.RecomputeBounds();

#if DEBUG
			foreach (SubMesh subMesh in allSubMeshes)
			{
				Debug.Assert(subMesh.bounds.IsFinite());
			}
#endif

			if (deduplicateVertexRecords)
			{
				result.DeduplicateVertices();
			}

			if (mergeSubMeshes)
			{
				return MergeSubMeshes(result);
			}

			return result;
		}

		/// <summary>
		/// Merge all <see cref="SubMesh"/> in <paramref name="mesh"/> to a single <see cref="Mesh"/>.
		/// </summary>
		/// <param name="mesh"></param>
		/// <returns></returns>
		public static Mesh MergeSubMeshes(Mesh mesh)
		{
			if (mesh.subMeshes.Count <= 1) return mesh;

#if DEBUG
			var inputBounds = mesh.Bounds();
#endif

			// Sort the submeshes so that they are ordered by shader
			// Then re-organize the index buffer to match.
			// Then merge the submeshes.

			var oldSubMeshes = new SubMeshMergeItem[mesh.subMeshes.Count];
			for (int i = 0; i < oldSubMeshes.Length; i++)
			{
				oldSubMeshes[i] = new SubMeshMergeItem(i, mesh.subMeshes[i]);
			}

			Array.Sort(oldSubMeshes, Comparison);

			for (int i = 0; i < oldSubMeshes.Length; i++)
			{
				oldSubMeshes[i].newIndex = i;
			}

			var oldIndexBuffer = mesh.indices.ToArray();
			var newIndexBuffer = new UInt32[mesh.indexCount];
			uint newIndexBufferFrontier = 0;

			var newSubMeshes = new List<SubMesh>();

			SubMesh previousNewSubMesh = null;

			foreach (var item in oldSubMeshes)
			{
				SubMesh oldSM = item.oldSubMesh;

				SubMesh newSM = getSubMesh(oldSM);

				// Update bounds
				newSM.bounds.Inflate(oldSM.bounds.Min);
				newSM.bounds.Inflate(oldSM.bounds.Max);

				Debug.Assert(newSM.bounds.IsFinite());

				Array.Copy(oldIndexBuffer,
				           oldSM.indexBufferStart,
				           newIndexBuffer,
				           newIndexBufferFrontier,
				           oldSM.indexBufferLength);

				newIndexBufferFrontier += oldSM.indexBufferLength;
			}

			var result = new Mesh(mesh.vertices.ToArray(), newIndexBuffer, newSubMeshes.ToArray());

#if DEBUG
			var mergedBounds = result.Bounds();

			bool a = inputBounds == mergedBounds;

			result.RecomputeBounds();
			var recomputedBounds = result.Bounds();

			bool b = mergedBounds     == recomputedBounds;

			Debug.Assert(a && b);
#endif



			return result;

			SubMesh getSubMesh(SubMesh old)
			{
				if (null != previousNewSubMesh && previousNewSubMesh.shaderId == old.shaderId)
				{
					previousNewSubMesh.indexBufferLength += old.indexBufferLength;
					return previousNewSubMesh;
				}

				previousNewSubMesh = new SubMesh
					(
					 $"id{(UInt16)old.shaderId}"
				   , newIndexBufferFrontier
				   , old.indexBufferLength
				   , old.bounds
				   , old.shaderId
					);

				newSubMeshes.Add(previousNewSubMesh);

				return previousNewSubMesh;
			}

			int Comparison(SubMeshMergeItem x, SubMeshMergeItem y)
			{
				return ((UInt16)x.oldSubMesh.shaderId).CompareTo((UInt16)y.oldSubMesh.shaderId);
			}
		}

		private class SubMeshMergeItem
		{
			internal int oldIndex;
			internal int newIndex;

			internal SubMesh oldSubMesh;

			internal SubMeshMergeItem(int oldIndex, SubMesh oldSubMesh)
			{
				this.oldIndex = oldIndex;
				this.oldSubMesh = oldSubMesh;
			}
		}
	}
}
