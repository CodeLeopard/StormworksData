﻿// Copyright 2022-2024 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/





using System;
using System.Diagnostics;
using System.IO;

using BinaryDataModel.DataTypes;

using OpenToolkit.Mathematics;

using Shared;
using static BinaryDataModel.DataTypes.MapBin;

namespace BinaryDataModel
{
	/// <summary>
	/// Helper for reading and writing binary data.
	/// </summary>
	public static class BinaryRW
	{
		#region BinaryReader
		/// <summary>
		/// Read <see cref="Vector2"/> as 2 <see cref="System.Single"/>.
		/// </summary>
		/// <param name="instance"></param>
		/// <returns></returns>
		public static Vector2 ReadVector2(this BinaryReader instance)
		{
			float x = instance.ReadSingle();
			float y = instance.ReadSingle();

			return new Vector2(x, y);
		}

		/// <summary>
		/// Read <see cref="Vector3"/> as 3 <see cref="System.Single"/>.
		/// </summary>
		/// <param name="instance"></param>
		/// <returns></returns>
		public static Vector3 ReadVector3(this BinaryReader instance)
		{
			float x = instance.ReadSingle();
			float y = instance.ReadSingle();
			float z = instance.ReadSingle();

			return new Vector3(x, y, z);
		}

		/// <summary>
		/// Read <see cref="VertexRecord"/>.
		/// </summary>
		/// <param name="instance"></param>
		/// <returns></returns>
		public static VertexRecord ReadVertexRecord(this BinaryReader instance)
		{
			return new VertexRecord(instance.ReadBytes(VertexRecord.bytesPerVertex));
		}

		public static Vector3UV ReadTextureVertex(this BinaryReader instance)
		{
			var v = instance.ReadVector3();
			var uv = instance.ReadVector2();
			return new Vector3UV(v, uv);
		}

		/// <summary>
		/// Read <see cref="Triangle16"/>
		/// </summary>
		/// <param name="instance"></param>
		/// <returns></returns>
		public static Triangle16 ReadTriangle16(this BinaryReader instance)
		{
			return new Triangle16(instance.ReadBytes(Triangle16.bytesPerTriangle));
		}

		/// <summary>
		/// Read <see cref="Triangle32"/>
		/// </summary>
		/// <param name="instance"></param>
		/// <returns></returns>
		public static Triangle32 ReadTriangle32(this BinaryReader instance)
		{
			return new Triangle32(instance.ReadBytes(Triangle32.bytesPerTriangle));
		}

		/// <summary>
		/// Read SubMesh
		/// </summary>
		/// <param name="instance"></param>
		/// <returns></returns>
		public static SubMesh ReadSubMesh(this BinaryReader instance)
		{
			uint indexBufferStart = instance.ReadUInt32();
			uint indexBufferLength = instance.ReadUInt32();
			UInt16 header2 = instance.ReadUInt16();
			UInt16 shaderID = instance.ReadUInt16();
			Vector3 boundsMin = instance.ReadVector3();
			Vector3 boundsMax = instance.ReadVector3();
			UInt16 header6 = instance.ReadUInt16();
			ushort strLen = instance.ReadUInt16();
			if (strLen > 1_000) Debugger.Break(); // Probably bad.
			string name = new string(instance.ReadChars(strLen));
			Vector3 header8 = instance.ReadVector3();

			return new SubMesh(indexBufferStart, indexBufferLength, header2, shaderID, new Bounds3(boundsMin, boundsMax), header6, name, header8);
		}
		#endregion BinaryReader


		#region BinaryWriter

		/// <summary>
		/// Write <see cref="Vector3"/> as 3x <see cref="System.Single"/>
		/// </summary>
		/// <param name="b"></param>
		/// <param name="v"></param>
		public static void Write(this BinaryWriter b, Vector3 v)
		{
			b.Write(v.X);
			b.Write(v.Y);
			b.Write(v.Z);
		}

		/// <summary>
		/// Write <see cref="Vector4"/> as 4x <see cref="System.Single"/>
		/// </summary>
		/// <param name="b"></param>
		/// <param name="v"></param>
		public static void Write(this BinaryWriter b, Vector4 v)
		{
			b.Write(v.X);
			b.Write(v.Y);
			b.Write(v.Z);
			b.Write(v.W);
		}

		/// <summary>
		/// Write <see cref="Vector3UV"/> as 5x <see cref="System.Single"/>
		/// </summary>
		/// <param name="b"></param>
		/// <param name="tv"></param>
		public static void Write(this BinaryWriter b, Vector3UV tv)
		{
			var v = tv.position;
			b.Write(v.X);
			b.Write(v.Y);
			b.Write(v.Z);
			var u = tv.uv;
			b.Write(u.X);
			b.Write(u.Y);
		}

		/// <summary>
		/// Write <see cref="Triangle16"/> as 3x <see cref="UInt16"/>
		/// </summary>
		/// <param name="b"></param>
		/// <param name="t"></param>
		public static void Write(this BinaryWriter b, Triangle16 t)
		{
			b.Write(t.a);
			b.Write(t.b);
			b.Write(t.c);
		}


		/// <summary>
		/// Write <see cref="Triangle16"/> as 3x <see cref="UInt32"/>
		/// </summary>
		/// <param name="b"></param>
		/// <param name="t"></param>
		public static void Write(this BinaryWriter b, Triangle32 t)
		{
			b.Write(t.a);
			b.Write(t.b);
			b.Write(t.c);
		}

		/// <summary>
		/// Write <see cref="VertexRecord"/> as <see cref="Vector3"/> position, 4x <see cref="byte"/> RGBA, <see cref="Vector3"/> normal
		/// </summary>
		/// <param name="b"></param>
		/// <param name="r"></param>
		public static void Write(this BinaryWriter b, VertexRecord r)
		{
			b.Write(r.position);
			b.Write((byte)(r.color.R * 255f));
			b.Write((byte)(r.color.G * 255f));
			b.Write((byte)(r.color.B * 255f));
			b.Write((byte)(r.color.A * 255f));
			b.Write(r.normal);
		}

		/// <summary>
		/// Write <see cref="SubMesh"/>
		/// </summary>
		/// <param name="b"></param>
		/// <param name="s"></param>
		public static void Write(this BinaryWriter b, SubMesh s)
		{
			b.Write(s.indexBufferStart);
			b.Write(s.indexBufferLength);
			b.Write(s.header2);
			b.Write((UInt16) s.shaderId);
			b.Write(s.bounds.Min);
			b.Write(s.bounds.Max);
			b.Write(s.header6);
			b.Write(s.nameByteLength);
			b.Write(s.name.ToCharArray());
			b.Write(s.header8);
		}

		/// <summary>
		/// Write the <em>only the headers</em> of a <see cref="Mesh"/>.
		/// </summary>
		/// <param name="b"></param>
		/// <param name="m"></param>
		public static void WriteHeader(this BinaryWriter b, Mesh m)
		{
			b.Write(m.header0);
			b.Write(m.header1);
			b.Write((UInt16)m.vertexCount); // We use bigger number internally.
			b.Write(m.header3);
			b.Write(m.header4);
		}


		/// <summary>
		/// Write the <em>only the headers</em> of a <see cref="Phys"/>.
		/// </summary>
		/// <param name="b"></param>
		/// <param name="p"></param>
		public static void WriteHeader(this BinaryWriter b, Phys p)
		{
			b.Write(p.header0);
			b.Write((UInt16)p.subMeshCount);
		}

		#endregion BinaryWriter
	}
}
