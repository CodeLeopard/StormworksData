﻿// Copyright 2022-2024 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using BinaryDataModel.DataTypes;
using BinaryDataModel.DataTypes.Enums;
using OpenToolkit.Mathematics;
using Shared;
using Shared.Exceptions;


namespace BinaryDataModel.Converters
{
	/// <summary>
	/// Loads and saves Stormworks .mesh and .phys file formats and converts between them.
	/// </summary>
	public static class Binary
	{
		public static readonly char[] meshMarker = new[] { 'm', 'e', 's', 'h' };
		public static readonly char[] physMarker = new[] { 'p', 'h', 'y', 's' };

		#region Mesh

		public class MeshDiagCallback
		{
			public Action<char[]> fileTypeMarker;
			public Action<VertexRecord> vertex;
			public Action<UInt32> indexCount;
			public Action<UInt32> index;
			public Action<UInt16> subMeshCount;
			public Action<SubMesh> subMesh;
			public Action<BinaryReader> eofRemain;
			public Action<Mesh> result;
			public Action<Exception> exception;
			public Action onFinally;
		}

		/// <summary>
		/// Load a <see cref="Mesh"/> from the given <paramref name="filePath"/>.
		/// </summary>
		/// <param name="filePath"></param>
		/// <returns></returns>
		/// <exception cref="FileInteractionException"></exception>
		public static Mesh LoadMesh(string filePath)
		{
			try
			{
				Mesh mesh;
				using (var stream = File.OpenRead(filePath))
				{
					mesh =  LoadMesh(stream);
				}
				mesh.fileName = filePath;

				return mesh;
			}
			catch (Exception e)
			{
				throw new FileInteractionException(filePath, e);
			}
		}

		/// <summary>
		/// Load a <see cref="Mesh"/> from the given <see cref="Stream"/>.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="diag"></param>
		/// <returns></returns>
		/// <exception cref="BinaryDataException"></exception>
		public static Mesh LoadMesh(Stream stream, MeshDiagCallback diag = null)
		{
			try
			{
				var b = new BinaryReader(stream);

				var fileTypeMarker = b.ReadChars(4);

				diag?.fileTypeMarker?.Invoke(fileTypeMarker);

				if (! fileTypeMarker.SequenceEqual(meshMarker))
					throw new BinaryDataFormatException(meshMarker, fileTypeMarker);

				// File Header.
				var header0 = b.ReadUInt16();
				var header1 = b.ReadUInt16();
				var vertexCount = b.ReadUInt16();
				var header3 = b.ReadUInt16();
				var header4 = b.ReadUInt16();


				// ################################################################
				var vertices = new List<VertexRecord>(vertexCount);

				for (int i = 0; i < vertexCount; i++)
				{
					vertices.Add(b.ReadVertexRecord());
					diag?.vertex?.Invoke(vertices[i]);
				}

				// ################################################################
				var indexCount = b.ReadUInt32();
				diag?.indexCount?.Invoke(indexCount);

				var indices = new UInt32[indexCount];

				for (int i = 0; i < indices.Length; i++)
				{
					var index = b.ReadUInt16();
					diag?.index?.Invoke(index);

					if (index >= vertexCount)
						throw new BinaryDataException($"Index #{i} is out of bounds {index} > {vertexCount}.");

					indices[i] = index;
				}

				// ################################################################
				var subMeshCount = b.ReadUInt16();
				diag?.subMeshCount?.Invoke(subMeshCount);

				var subMeshes = new SubMesh[subMeshCount];

				for (int i = 0; i < subMeshes.Length; i++)
				{
					var subMesh = b.ReadSubMesh();
					diag?.subMesh?.Invoke(subMesh);

					if (subMesh.indexBufferStart > indices.Length)
					{
						throw new BinaryDataException
							(
							 $"SubMesh #{i}'s IndexBuffer starts out of bounds {subMesh.indexBufferStart} > {indices.Length}"
							);
					}

					if (subMesh.indexBufferStart + subMesh.indexBufferLength > indices.Length)
					{
						throw new BinaryDataException
							(
							 $"SubMesh #{i}'s IndexBuffer runs out of bounds "
						   + $"({subMesh.indexBufferStart} + {subMesh.indexBufferLength} = "
						   + $"{subMesh.indexBufferStart + subMesh.indexBufferLength}) > {indices.Length}"
							);
					}

					subMeshes[i] = subMesh;
				}

				// ################################################################
				// End of data.

				var trailingZeroBytes = b.ReadUInt16();
				if (trailingZeroBytes != 0)
				{
					throw new BinaryDataException($"Unexpected file end: '{trailingZeroBytes}' expected 0.");
				}

				// Any additional data is ignored by the game.
				diag?.eofRemain?.Invoke(b);

				var mesh = new Mesh(vertices, indices, subMeshes);
				mesh.header0 = header0;
				mesh.header1 = header1;
				mesh.header3 = header3;
				mesh.header4 = header4;

				diag?.result?.Invoke(mesh);

				return mesh;
			}
			catch (Exception e)
			{
				diag?.exception?.Invoke(e);
				throw;
			}
			finally
			{
				diag?.onFinally?.Invoke();
			}
		}

		/// <summary>
		/// Save a <see cref="Mesh"/> to the given <see cref="Stream"/>.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="mesh"></param>
		/// <param name="diag"></param>
		/// <exception cref="InvalidOperationException"></exception>
		public static void Save(Stream stream, Mesh mesh, MeshDiagCallback diag = null)
		{
			if (mesh.vertexCount > UInt16.MaxValue)
				throw new InvalidOperationException
					(
					 $"Cannot save a Mesh with more than {UInt16.MaxValue} vertices, this mesh has {mesh.vertexCount}."
				   + $" The existence of non-compliant meshes is allowed to allow merging meshes and rendering the result outside of Stormworks."
					);


			var b = new BinaryWriter(stream);

			b.Write(meshMarker);

			b.WriteHeader(mesh);

			for (UInt16 i = 0; i < mesh.vertexCount; i++)
			{
				b.Write(mesh.vertices[i]);
			}

			b.Write((UInt32)mesh.indexCount);
			for(int i = 0; i < mesh.indexCount; i++)
			{
				b.Write((UInt16)mesh.indices[i]);
			}

			b.Write((UInt16) mesh.SubMeshCount);
			for (UInt16 i = 0; i < mesh.SubMeshCount; i++)
			{
				b.Write(mesh.subMeshes[i]);
			}

			// End of file marker.
			// todo: could this refer to extra data?
			b.Write((UInt16) 0);
			b.Flush();
		}

		/// <summary>
		/// Save a <see cref="Mesh"/> to a (new) file at <paramref name="path"/>.
		/// </summary>
		/// <param name="path"></param>
		/// <param name="mesh"></param>
		/// <param name="diag"></param>
		public static void Save(string path, Mesh mesh, MeshDiagCallback diag = null)
		{
			using (var stream = File.Open(path, FileMode.Create))
			{
				Save(stream, mesh, diag);
			}
		}

		#endregion Mesh

		#region Phys

		public class PhyDiagCallback
		{
			public Action<char[]> fileTypeMarker;
			public Action<UInt16> header0;
			public Action<UInt16> subMeshCount;

			// SubMesh
			public Action<UInt16> vertexCount;
			public Action<Vector3> vertex;
			public Action<UInt16> indexCount;
			public Action<UInt32> index;
			// end SubMesh

			public Action<BinaryReader> eofRemain;
			public Action<Phys> result;
			public Action<Exception> exception;
			public Action onFinally;
		}

		/// <summary>
		/// Load a <see cref="Phys"/> from <paramref name="filePath"/>.
		/// </summary>
		/// <param name="filePath"></param>
		/// <returns></returns>
		/// <exception cref="FileInteractionException"></exception>
		public static Phys LoadPhys(string filePath)
		{
			try
			{
				Phys mesh;
				using (var stream = File.OpenRead(filePath))
				{
					mesh = LoadPhys(stream);
				}

				mesh.fileName = filePath;
				return mesh;
			}
			catch (Exception e)
			{
				throw new FileInteractionException(filePath, e);
			}
		}

		/// <summary>
		/// Load a <see cref="Phys"/> from the given <paramref name="stream"/>.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="diag"></param>
		/// <returns></returns>
		/// <exception cref="BinaryDataException"></exception>
		public static Phys LoadPhys(Stream stream, PhyDiagCallback diag = null)
		{
			try
			{
				var b = new BinaryReader(stream);

				var fileTypeMarker = b.ReadChars(4);

				diag?.fileTypeMarker?.Invoke(fileTypeMarker);

				if (! fileTypeMarker.SequenceEqual(physMarker))
					throw new BinaryDataFormatException(physMarker, fileTypeMarker);


				var header0 = b.ReadUInt16();
				diag?.header0?.Invoke(header0);
				var subMeshCount = b.ReadUInt16();
				diag?.subMeshCount?.Invoke(subMeshCount);

				var subMeshes = new List<PhySubMesh>(subMeshCount);

				for (int m = 0; m < subMeshCount; m++)
				{
					var vCount = b.ReadUInt16();
					diag?.vertexCount?.Invoke(vCount);
					var vertices = new List<Vector3>(vCount);

					for (int v = 0; v < vCount; v++)
					{
						vertices.Add(b.ReadVector3());
						diag?.vertex?.Invoke(vertices[v]);
					}

					var iCount = b.ReadUInt16();
					diag?.indexCount?.Invoke(iCount);
					if (iCount > 0)
					{
						var indices = new List<UInt32>(iCount);

						for (int i = 0; i < iCount; i++)
						{
							indices.Add(b.ReadUInt32());
							diag?.index?.Invoke(indices[i]);
						}

						subMeshes.Add(new PhySubMesh(vertices, indices, true));
					}
					else
					{
						subMeshes.Add(new PhySubMesh(vertices, true));
					}
				}


				diag?.eofRemain?.Invoke(b);

				if (b.BaseStream.Position != b.BaseStream.Length)
					throw new BinaryDataException
						(
						 $"Finished reading, but not at end of file yet. {b.BaseStream.Length - b.BaseStream.Position} bytes remain"
						);

				var phyMesh = new Phys(subMeshes);

				diag?.result?.Invoke(phyMesh);

				return phyMesh;
			}
			catch (Exception e)
			{
				diag?.exception?.Invoke(e);

				throw;
			}
			finally
			{
				diag?.onFinally?.Invoke();
			}
		}

		/// <summary>
		/// Save a <see cref="Phys"/> to the <paramref name="stream"/>.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="mesh"></param>
		/// <param name="diag"></param>
		public static void Save(Stream stream, Phys mesh, PhyDiagCallback diag = null)
		{

			if (mesh.subMeshes.Any(sm => sm.hasIndices))
			{
				// It seems SW doesn't read the index buffer we provide.
				// Perhaps the header is wrong.
				// For now we just save as ordered vertices because that definitely works.
				// Todo: fix the above.

				mesh = mesh.Clone(); // Don't modify caller's object
				mesh.ConvertToOrderedVertices(strict: false);
			}


			var b = new BinaryWriter(stream);
			b.Write(physMarker);
			b.WriteHeader(mesh);

			for (UInt16 m = 0; m < mesh.subMeshCount; m++)
			{
				var subMesh = mesh.subMeshes[m];

				if (subMesh.vertices.Count > UInt16.MaxValue)
				{
					throw new InvalidOperationException($"A phys submesh cannot contain more that {UInt16.MaxValue} vertices or indices.");
				}

				b.Write((UInt16)subMesh.vertexCount);

				for (UInt16 v = 0; v < subMesh.vertexCount; v++)
				{
					b.Write(subMesh.vertices[v]);
				}

				if (subMesh.hasIndices)
				{
					if (subMesh.indices.Count > UInt16.MaxValue)
					{
						throw new InvalidOperationException($"A phys submesh cannot contain more that {UInt16.MaxValue} vertices or indices.");
					}

					b.Write((UInt16)subMesh.indexCount);
					for (UInt16 i = 0; i < subMesh.indexCount; i++)
					{
						b.Write(subMesh.indices[i]);
					}
				}
				else
				{
					b.Write((UInt16)0);
				}
			}
			b.Flush();
		}

		/// <summary>
		/// Save a <see cref="Phys"/> to a (new) file at <paramref name="path"/>.
		/// </summary>
		/// <param name="path"></param>
		/// <param name="mesh"></param>
		/// <param name="diag"></param>
		public static void Save(string path, Phys mesh, PhyDiagCallback diag = null)
		{
			using (var stream = File.Open(path, FileMode.Create))
			{
				Save(stream, mesh, diag);
			}
		}


		public static Color4 DefaultColorProvider(int subMeshIndex)
		{
			return Color4.Wheat;
		}

		public static Mesh ConvertToMesh(Phys phys, Func<int, Color4> colorProvider = null)
		{
			colorProvider = colorProvider ?? DefaultColorProvider;

			var mesh = new Mesh();

			for (int i = 0; i < phys.subMeshCount; i++)
			{
				var color = colorProvider(i);

				var bounds = new Bounds3(true);

				var vertexBufferStart = mesh.vertices.Count;
				uint indexBufferStart = (uint) mesh.indices.Count;

				foreach(var p in phys.subMeshes[i].vertices)
				{
					var v = new VertexRecord();
					v.position = p;
					v.color = color;

					bounds.Inflate(p);

					mesh.vertices.Add(v);
				}

				if (phys.subMeshes[i].indices.Count > 0)
				{
					foreach (var j in phys.subMeshes[i].indices)
					{
						mesh.indices.Add(j);
					}
				}
				else
				{
					int start = vertexBufferStart;
					int end = vertexBufferStart + phys.subMeshes[i].vertices.Count;
					for (uint j = (uint)vertexBufferStart; j < end; j++)
					{
						mesh.indices.Add(j);
					}
				}

				var sm = new SubMesh($"{i}", (uint) indexBufferStart, (uint)mesh.indices.Count - indexBufferStart, bounds);
				mesh.subMeshes.Add(sm);
			}

			return mesh;
		}

		[Obsolete("This is probably broken, use " + nameof(ConvertToVertexOnly) + " instead.")]
		public static Phys ConvertToPhys(Mesh mesh)
		{
			var subMeshes = new List<PhySubMesh>(mesh.SubMeshCount);

			for(int subMeshIndex = 0; subMeshIndex < mesh.SubMeshCount; subMeshIndex++)
			{
				var sm = mesh.subMeshes[subMeshIndex];

				// Vertices in Phys files are not shared between subMeshes, where Meshes do share.
				// As such we don't know in advance how many vertices we will get in each subMesh.
				// This also means that indices will need to be adjusted to match the new index into the subMesh's buffer.

				var vertices = new List<Vector3>(mesh.vertices.Count); // Best effort guess at capacity.

				var indices = new List<UInt32>((int) sm.indexBufferLength);

				var start  = (int) sm.indexBufferStart;
				var length = (int) sm.indexBufferLength;
				for (int i = 0; i < length; i++)
				{
					var index = mesh.indices[start + i];

					var vertex = mesh.vertices[(int) index].position;

					// todo: probably slow.
					int newIndex = vertices.IndexOf(vertex);

					if (newIndex < 0)
					{
						newIndex = vertices.Count;
						vertices.Add(vertex);
					}

					indices.Add((uint) newIndex);
				}

				subMeshes.Add(new PhySubMesh(vertices, indices, true));
			}

			var phys = new Phys(subMeshes, true);
			phys.fileName = $"Converted from '{mesh.fileName ?? ""}'";

			return phys;
		}

		/// <summary>
		/// Convert the <paramref name="mesh"/> to a <see cref="Phys"/> that only contains vertices, leaving the faces to be defined by the vertex order.
		/// </summary>
		/// <param name="mesh"></param>
		/// <returns></returns>
		public static Phys ConvertToVertexOnly(Mesh mesh)
		{
			var subMeshes = new List<PhySubMesh>(mesh.SubMeshCount);

			for (int subMeshIndex = 0; subMeshIndex < mesh.SubMeshCount; subMeshIndex++)
			{
				var sm = mesh.subMeshes[subMeshIndex];

				// Vertices in Phys files are not shared between subMeshes, where Meshes do share.
				// As such we don't know in advance how many vertices we will get in each subMesh.

				var vertices = new List<Vector3>();


				int start = (int) sm.indexBufferStart;
				int end   = (int) (start + sm.indexBufferLength);
				for (int i = start; i < end;)
				{
					var a = mesh.indices[i++];
					var b = mesh.indices[i++];
					var c = mesh.indices[i++];

					var va = mesh.vertices[(int) a];
					var vb = mesh.vertices[(int) b];
					var vc = mesh.vertices[(int) c];

					vertices.Add(va.position);
					vertices.Add(vb.position);
					vertices.Add(vc.position);
				}

				subMeshes.Add(new PhySubMesh(vertices, true));
			}
			var phys = new Phys(subMeshes, true);
			phys.fileName = $"Converted from '{mesh.fileName ?? ""}'";

			return phys;
		}

		#endregion Phys

		#region MapBin


		public class MapBinDiagCallback
		{
			//public Action<char[]> fileTypeMarker;
			public Action<VertexLayerKind> beginVexLayer;
			public Action<UInt16> vertexCount;
			public Action<Vector3> vertex;

			public Action<UInt16> indexCount;
			public Action<Triangle16> face;

			// Texture layer
			public Action<int, TextureLayerKind> beginTexLayer;
			public Action<UInt16> texVertexCount;
			public Action<Vector3UV> texVertex;

			public Action<BinaryReader> eofRemain;
			public Action<MapBin> result;
			public Action<Exception> exception;
			public Action onFinally;
		}

		public static MapBin LoadMapBin(string filePath, MapBinDiagCallback diag = null)
		{
			try
			{
				MapBin mesh;
				using (var stream = File.OpenRead(filePath))
				{
					mesh = LoadMapBin(stream, diag);
				}
				mesh.fileName = filePath;

				return mesh;
			}
			catch (Exception e)
			{
				throw new FileInteractionException(filePath, e);
			}
		}

		public static MapBin LoadMapBin(Stream stream, MapBinDiagCallback diag = null)
		{
			try
			{
				MapBin m = new MapBin();
				var b = new BinaryReader(stream);
				// there is no fileType marker

				for (int l = 0; l < MapBin.vertexLayerCount; l++)
				{
					var kind = (VertexLayerKind)l;
					diag?.beginVexLayer?.Invoke(kind);
					var ld = m.vertexLayers[l];

					UInt16 vertexCount = b.ReadUInt16();
					diag?.vertexCount?.Invoke(vertexCount);
					for(int i  = 0; i < vertexCount; i++)
					{
						var v = b.ReadVector3();
						diag?.vertex?.Invoke(v);

						ld.Vertices.Add(v);
					}

					UInt16 indexCount = b.ReadUInt16();
					int triangleCount = indexCount / 3;
					diag?.indexCount?.Invoke(indexCount);
					for (int i = 0;i < triangleCount; i++)
					{
						var t = b.ReadTriangle16();
						diag?.face?.Invoke(t);

						ld.Triangles.Add(t);
					}
				}

				for (int l = 0; l < MapBin.textureLayerCount; l++)
				{
					var kind = (TextureLayerKind) (l % 2);
					diag?.beginTexLayer?.Invoke(l, kind);
					var ld = m.textureLayers[l];

					UInt16 vertexCount = b.ReadUInt16();
					diag?.texVertexCount?.Invoke(vertexCount);
					for(int i = 0; i<vertexCount; i++)
					{
						var v = b.ReadTextureVertex();
						diag?.texVertex?.Invoke(v);

						ld.Vertices.Add(v);
					}
				}

				diag?.result?.Invoke(m);
				diag?.eofRemain?.Invoke(b);

				return m;
			}
			catch (Exception e)
			{
				diag?.exception?.Invoke(e);
				throw;
			}
			finally
			{
				diag?.onFinally?.Invoke();
			}
		}

		/// <summary>
		/// Save a <see cref="MapBin"/> to a (new) file at <paramref name="path"/>.
		/// </summary>
		/// <param name="path"></param>
		/// <param name="mesh"></param>
		public static void Save(string path, MapBin mesh)
		{
			using (var stream = File.Open(path, FileMode.Create))
			{
				Save(stream, mesh);
			}
		}

		/// <summary>
		///  Save a <see cref="MapBin"/> to <paramref name="stream"/>.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="map"></param>
		public static void Save(Stream stream, MapBin map)
		{
			var b = new BinaryWriter(stream);
			int i = 0;
			foreach (var vertLayer in map.vertexLayers)
			{
				Console.WriteLine($"Vertex Layer {(VertexLayerKind)i} ({i})\n  vertices: {vertLayer.Vertices.Count}");
				b.Write((Int16)vertLayer.Vertices.Count);
				foreach (var vertex in vertLayer.Vertices)
				{
					b.Write(vertex);
				}

				Console.WriteLine($"  indices: {vertLayer.Triangles.Count * 3}");
				b.Write((Int16)(vertLayer.Triangles.Count * 3));
				foreach (var face in vertLayer.Triangles)
				{
					b.Write(face);
				}

				i++;
			}

			i = 0;
			foreach (var texLayer in map.textureLayers)
			{
				Console.WriteLine($"Texture Layer {(TextureLayerKind)(i % 2)} ({i})\n  vertices: {texLayer.Vertices.Count}");
				b.Write((Int16)texLayer.Vertices.Count);
				foreach (var vertex in texLayer.Vertices)
				{
					b.Write(vertex);
				}

				i++;
			}
		}
		#endregion
	}
}
