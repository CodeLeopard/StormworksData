﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Text;

using OpenToolkit.Mathematics;

namespace BinaryDataModel.Converters
{
	/// <summary>
	/// Loads and saves .obj file format.
	/// This converter cannot convert color data.
	/// </summary>
	[Obsolete("This converter cannot convert color data.")]
	public class Obj
	{
		private const char commentChar = '#';
		private const string comment = "# ";

		private const char mySplitCharacter = ' ';
		private static char[] splitCharacters = new char[] { mySplitCharacter };

		private const string vertex = "v";
		private const string face = "f";

		private const string materialFileDefinition = "mtllib ";
		public const string materialFileName = "materials.mtl";
		private const string useMaterial = "usemtl ";

		static Dictionary<Color4, string> colors = new Dictionary<Color4, string>();
		private const string materialDefinition = "newmtl ";
		private const string materialAmbient = "Ka ";
		private const string materialDiffuse = "Kd ";
		private const string materialSpecular = "Ks ";
		private const string materialSpecularExp = "Ns ";
		private const string materialTranspar = "Tr ";


		private const float ambientMultiplier = 0.1f;
		private const float specularMultiplier = 0.1f;
		private const float specularExponent = 0.1f;


		public static string ColorFile()
		{
			var sb = new StringBuilder();
			foreach (var kvp in colors)
			{
				var key = kvp.Key;
				var color = new Vector4(key.R / 255f, key.G / 255f, key.B / 255f, key.A / 255f);
				var id = kvp.Value;

				sb.AppendLine(materialDefinition + id);
				sb.AppendLine(materialAmbient + ColorStr(color * ambientMultiplier));
				sb.AppendLine(materialDiffuse + ColorStr(color));
				sb.AppendLine(materialSpecular + ColorStr(color * specularMultiplier));
				sb.AppendLine(materialSpecularExp + specularExponent);
				sb.AppendLine(materialTranspar + (1 - (key.A / 255)));
				sb.AppendLine();
			}


			string ColorStr(Vector4 c)
			{
				return c.X + " " + c.Y + " " + c.Z;
			}

			return sb.ToString();
		}

		static string ColorID(Color4 c)
		{
			if (false == colors.TryGetValue(c, out string id))
			{
				id = "R" + c.R + "G" + c.G + "B" + c.B + "A" + c.A;
				colors.Add(c, id);
			}
			return id;

		}
		/*
		public static string Save(Mesh mc)
		{
			var sb = new StringBuilder();
			sb.AppendLine(comment + "Header.");
			sb.AppendLine(comment + "OriginalFIleName: " + mc.fileName);

			int count = 0;
			foreach (SubMesh m in mc.meshes)
			{
				var vertexRecords = m.vertices;
				var triangles = m.triangles;

				sb.AppendLine($"o{mySplitCharacter}Mesh#{count++}");

				sb.AppendLine(comment + "Vertices");

				foreach (var record in vertexRecords)
				{
					var v = record.position;
					sb.Append(vertex);
					sb.Append(mySplitCharacter);
					sb.Append(v.X);
					sb.Append(mySplitCharacter);
					sb.Append(v.Y);
					sb.Append(mySplitCharacter);
					sb.Append(v.Z);
					sb.AppendLine();
				}

				sb.AppendLine(comment + "Faces (triangles)");
				foreach (var tri in triangles)
				{
					sb.Append(face);
					sb.Append(mySplitCharacter);
					sb.Append(tri.a + 1); // indices start at 1 in .obj
					sb.Append(mySplitCharacter);
					sb.Append(tri.b + 1);
					sb.Append(mySplitCharacter);
					sb.Append(tri.c + 1);
					sb.AppendLine();
				}
			}
			return sb.ToString();
		}*/
	}
}
