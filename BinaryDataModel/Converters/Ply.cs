// Copyright 2022-2024 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using BinaryDataModel.DataTypes;
using BinaryDataModel.DataTypes.Enums;
using OpenToolkit.Mathematics;
using Shared;
using Shared.Exceptions;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using static BinaryDataModel.DataTypes.MapBin;

namespace BinaryDataModel.Converters
{
	/// <summary>
	/// Loads and saves .Ply file format
	/// </summary>
	public static class Ply
	{
		private const string newLine = "\n";

		private const string fileTypeIdentifier = "ply";
		private const string formatIdentifier = "ascii 1.0";
		private const string formatHeader = "format " + formatIdentifier;
		private const string comment = "comment";

		private const string elementDefinition = "element";

		private const string element_vertex = "element vertex";
		private const string element_face = "element face";

		private const string propertyDefinition = "property";

		private const string property_vertexIndex_BM = "vertex_ind";

		private const string @byte = "uchar";

		private static readonly string[] allowedTypes = new[] { "char", "short", "int", "long", "uchar", "ushort", "uint", "ulong" };



		private const byte indicesInTriangle = 3;
		private const byte indicesInQuad = 4;


		private const string endOfHeader = "end_header";

		private static readonly string authorNote =
			$"{comment}{Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes("converter by CodeLeopard"))}";

		private const string conversionToBlenderNote = comment + "transformed to Blender coordinate space (y up => z up)";

		private static readonly Regex CreatedByBlenderRegex   = new Regex("^comment Created by Blender",               RegexOptions.Compiled);
		private static readonly Regex Mesh_SubMeshIndexRegex  = new Regex("^comment [sS]ub[mM]esh index:? (\\d+)$",    RegexOptions.Compiled);
		private static readonly Regex Mesh_SubMeshNameRegex   = new Regex("^comment [sS]ub[mM]esh [nN]ame:? '(.*)'$",  RegexOptions.Compiled);
		private static readonly Regex Mesh_SubMeshShaderRegex = new Regex("^comment [sS]hader:? (\\w+)",               RegexOptions.Compiled);

		/// <summary>
		/// Regex to match the first/primary file of a multi file .ply mesh (due to subMeshes).
		/// </summary>
		                                                             //path/name.0.extension
		                                                             //path/name.0.S0.ext      S0 = Shader ID
		public static readonly Regex MultiFileFirstRegex = new Regex("^(.*)[.]0+([.]S\\d)?(\\.\\w+)$");
		/// <summary>
		/// Regex to match any non-first file of a multi file .ply mesh (due to subMeshes).
		/// </summary>
		public static readonly Regex MultiFileAfterRegex = new Regex("^(.*)[.][1-9][0-9]*([.]S\\d)?(\\.\\w+)$");
		/// <summary>
		/// Regex to match Shader in filename.
		/// </summary>
		public static readonly Regex ShaderFileNameRegex = new Regex("[.]S(\\d)\\.\\w+$");

		private static Regex TransformUpConventionRegex = new Regex("^"+Regex.Escape(conversionToBlenderNote), RegexOptions.Compiled);

		static Ply()
		{
			storedOrederReverseMap
				= nameToPropertyMap
				.Where(kvp => { var key = kvp.Key; return key == "float s" || key == "float t"; })
				.ToDictionary(kvp => kvp.Value, kvp => kvp.Key);
		}

		[Serializable]
		public class ConverterSettings
		{
			// todo: write these settings to the .ply file for reference.

			/// <summary>
			/// Reverse the triangles, making them face in the other direction.
			/// Typically combined with <see cref="ReverseNormals"/>.
			/// </summary>
			public bool ReverseTriangles = false;

			/// <summary>
			/// Reverse the quads, making them face in the other direction.
			/// Typically combined with <see cref="ReverseNormals"/>.
			/// </summary>
			public bool ReverseQuads = false;

			/// <summary>
			/// Reverse the normals. Normals are used for light calculation where they provide the 'up' direction of the surface.
			/// Typically combined with <see cref="ReverseTriangles"/>.
			/// </summary>
			public bool ReverseNormals = false;

			/// <summary>
			/// Transform Blender convention to Stormworks and back. .Ply files will be written for Blender and read from Blender.
			/// Internal format will have Stormworks convention.
			/// Stormworks is y-up, Blender is z-up
			/// </summary>
			public bool TransformUpConvention = false; // Todo: this should be a matrix or something that supports multiple conventions (not just Blender yes-or-no).

			/// <summary>
			/// Mirror across X axis.
			/// </summary>
			public bool MirrorX
			{
				get => MirrorTransformi.X == -1;
				set => MirrorTransformi.X = value ? -1 : 1;
			}

			/// <summary>
			/// Mirror across Y axis.
			/// </summary>
			public bool MirrorY
			{
				get => MirrorTransformi.Y == -1;
				set => MirrorTransformi.Y = value ? -1 : 1;
			}

			/// <summary>
			/// Mirror across Z axis.
			/// </summary>
			public bool MirrorZ
			{
				get => MirrorTransformi.Z == -1;
				set => MirrorTransformi.Z = value ? -1 : 1;
			}


			public Vector3i MirrorTransformi = new Vector3i(1,1,1);

			public Vector3 MirrorTransform => new Vector3(MirrorTransformi.X, MirrorTransformi.Y, MirrorTransformi.Z);

			public ConverterSettings()
			{

			}

			public static ConverterSettings BlenderWorkflow()
			{
				var s = new ConverterSettings();
				s.ReverseTriangles = false;
				s.ReverseNormals = true;
				s.TransformUpConvention = true;
				s.MirrorX = true;
				return s;
			}

			public static ConverterSettings LegacyWorkflow()
			{
				var s = new ConverterSettings();
				s.ReverseTriangles = true;
				s.ReverseNormals = false;
				s.TransformUpConvention = false;
				return s;
			}

			public static ConverterSettings NoConversion()
			{
				var s = new ConverterSettings();
				return s;
			}

			public static ConverterSettings Settings = BlenderWorkflow();

			/// <inheritdoc />
			public override string ToString()
			{
				return
					$"Triangles: {(ReverseTriangles ? "Reversed" : "As-Is")}, Normals: {(ReverseNormals ? "Flipped" : "As-Is")}, Convention: {(TransformUpConvention ? "Blender (Z-up & Y-forward)" : "As-Is")}, MirrorTransform: {MirrorTransformi}";
			}

			#region Transformations

			/// <summary>
			/// Transforms a vertex that was loaded from .ply file.
			/// </summary>
			/// <param name="v"></param>
			/// <returns></returns>
			public Vector3 LoadVertex(Vector3 v)
			{
				if (TransformUpConvention)
					v = new Vector3(v.X, v.Z, v.Y);
				v *= MirrorTransform;
				return v;
			}

			/// <summary>
			/// Transforms a normal that was loaded from .ply file.
			/// </summary>
			/// <param name="v"></param>
			/// <returns></returns>
			public Vector3 LoadNormal(Vector3 v)
			{
				if (TransformUpConvention)
					v = new Vector3(v.X, v.Z, v.Y);
				v *= MirrorTransform;
				if (ReverseNormals)
					v *= -1f;
				return v;
			}

			/// <summary>
			/// Transforms a vertex to be saved to .ply file.
			/// </summary>
			/// <param name="v"></param>
			/// <returns></returns>
			public Vector3 SaveVertex(Vector3 v)
			{
				v *= MirrorTransform;
				if (TransformUpConvention)
					v = new Vector3(v.X, v.Z, v.Y);
				return v;
			}

			/// <summary>
			/// Transforms a normal to be saved to .ply file.
			/// </summary>
			/// <param name="v"></param>
			/// <returns></returns>
			public Vector3 SaveNormal(Vector3 v)
			{
				if (ReverseNormals)
					v *= -1f;
				v *= MirrorTransform;
				if (TransformUpConvention)
					v = new Vector3(v.X, v.Z, v.Y);
				return v;
			}

			#endregion Transformations
		}


		private static bool MultiFileRegex(string filePath, out FileInfo[] files)
		{
			string fileName = Path.GetFileName(filePath);
			var match = MultiFileFirstRegex.Match(fileName);
			if (!match.Success)
			{
				files = Array.Empty<FileInfo>();
				return false;
			}

			var name = match.Groups[1].Value;
			var extension = match.Groups[3].Value;

			var directory = new DirectoryInfo(Path.GetDirectoryName(Path.GetFullPath(filePath)));
			string pattern = $"{name}*{extension}";

			files = directory.GetFiles(pattern);
			return true;
		}

		#region Ply

		
		private static IReadOnlyDictionary<string, PlyVertexProperty> nameToPropertyMap = new Dictionary<string, PlyVertexProperty>(StringComparer.InvariantCultureIgnoreCase)
		{
			{ "float x",       PlyVertexProperty.PositionX }
		  , { "float y",       PlyVertexProperty.PositionY }
		  , { "float z",       PlyVertexProperty.PositionZ }

		  , { "float nx",      PlyVertexProperty.NormalX }
		  , { "float ny",      PlyVertexProperty.NormalY }
		  , { "float nz",      PlyVertexProperty.NormalZ }
			// Alternative names
		  , { "float normalx", PlyVertexProperty.NormalX }
		  , { "float normaly", PlyVertexProperty.NormalY }
		  , { "float normalz", PlyVertexProperty.NormalZ }

		  , { "uchar red",     PlyVertexProperty.ColorR }
		  , { "uchar green",   PlyVertexProperty.ColorG }
		  , { "uchar blue",    PlyVertexProperty.ColorB }
		  , { "uchar alpha",   PlyVertexProperty.ColorA }

		  , { "float u",       PlyVertexProperty.TextureU }
		  , { "float v",       PlyVertexProperty.TextureV }
			// Alternative names
		  , { "float s",       PlyVertexProperty.TextureU }
		  , { "float t",       PlyVertexProperty.TextureV }
		};

		private static IReadOnlyDictionary<PlyVertexProperty, string> storedOrederReverseMap;

		private static Dictionary<PlyVertexProperty, int> ReadHeader(Action GetDataLine, ref string line, ref int lineNo, out bool needReadLine)
		{
			var propertyOrder = new Dictionary<PlyVertexProperty, int>();

			needReadLine = true;
			
			int filePropertyIndex = 0;
			while (true)
			{
				GetDataLine();

				if (line.StartsWith(element_face))
				{
					needReadLine = false;
					break;
				}

				if (line.StartsWith(endOfHeader))
				{
					throw new PlyDataException(lineNo, $"Reached end of header before finding any element definitions. Expected 'property ...' but found '{line}'");
				}

				var parts = line.Split(' ');
				if (parts[0] != propertyDefinition || parts.Length != 3)
				{
					throw new PlyDataException(lineNo, $"Expected '{propertyDefinition} [TYPE] [NAME]'... but found '{line}'.");
				}

				string propertyName = line.Substring(propertyDefinition.Length + 1); // +1 for space
				if (nameToPropertyMap.TryGetValue(propertyName, out PlyVertexProperty property))
				{
					if (! propertyOrder.TryAdd(property, filePropertyIndex))
					{
						throw new PlyDataException(lineNo, $"Duplicate Vertex property definition '{propertyName}'.");
					}
				}
				else
				{
					Console.WriteLine($"Ignoring unknown property at line {lineNo}: '{propertyName}'.");
				}

				filePropertyIndex++;
			}

			return propertyOrder;
		}

		private static void ValidateHeader(PlyLoadSettings loadSettings, Dictionary<PlyVertexProperty, int> propertyOrder, int lineNo)
		{
			// Elements will be removed as they are found.
			var requiredProperties = new HashSet<PlyVertexProperty>();
			var disallowedProperties = new HashSet<PlyVertexProperty>();
			var requiredColorComponents = new HashSet<PlyVertexProperty>();


			void HandlePresenceSpecifier(PrescenseSpecifier specifier, params PlyVertexProperty[] properties)
			{
				HashSet<PlyVertexProperty> addWhere = null;
				if (specifier == PrescenseSpecifier.Required)
				{
					addWhere = requiredProperties;
				}
				else if (specifier == PrescenseSpecifier.Disallowed)
				{
					addWhere = disallowedProperties;
				}

				if (addWhere == null) return;
				foreach (PlyVertexProperty property in properties)
				{
					addWhere.Add(property);
				}
			}

			HandlePresenceSpecifier(loadSettings.position, PlyVertexProperty.PositionX, PlyVertexProperty.PositionY, PlyVertexProperty.PositionZ);
			HandlePresenceSpecifier(loadSettings.normal, PlyVertexProperty.NormalX, PlyVertexProperty.NormalY, PlyVertexProperty.NormalZ);
			HandlePresenceSpecifier(loadSettings.color, PlyVertexProperty.ColorR, PlyVertexProperty.ColorG, PlyVertexProperty.ColorB);
			HandlePresenceSpecifier(loadSettings.colorAlpha, PlyVertexProperty.ColorA);
			HandlePresenceSpecifier(loadSettings.uv, PlyVertexProperty.TextureU, PlyVertexProperty.TextureV);

			var remainingRequiredProperties = new HashSet<PlyVertexProperty>(requiredProperties);
			var remainingRequiredColorProperties = new HashSet<PlyVertexProperty>(requiredColorComponents);

			foreach (var component in propertyOrder.Keys)
			{
				remainingRequiredProperties.Remove(component);
				remainingRequiredColorProperties.Remove(component);
			}

			if (remainingRequiredProperties.Count > 0)
			{
				string required = string.Join(", ", remainingRequiredProperties.Select(i => storedOrederReverseMap[i]));
				string missing = string.Join(", ", remainingRequiredProperties.Select(i => storedOrederReverseMap[i]));
				string message = $"Missing some required Vertex components.\nRequired: {required}\nMissing: {missing}";

				throw new PlyDataException(lineNo, message);
			}

			if (
				// If any color component is specified...
				(propertyOrder.ContainsKey(PlyVertexProperty.ColorR)
				 || propertyOrder.ContainsKey(PlyVertexProperty.ColorG)
				 || propertyOrder.ContainsKey(PlyVertexProperty.ColorB)
				 || propertyOrder.ContainsKey(PlyVertexProperty.ColorA)
				) && (
					// Then we need at least R, G, B components present.
					!propertyOrder.ContainsKey(PlyVertexProperty.ColorR)
				 || !propertyOrder.ContainsKey(PlyVertexProperty.ColorG)
				 || !propertyOrder.ContainsKey(PlyVertexProperty.ColorB)
				)
				)
			{
				// We can only get here when color was optinal, because the required check would have caught it if it was.
				throw new PlyDataException
					(lineNo, $"Missing some Vertex color components. Valid components are all uchar: r,g,b,a. "
						   + $"Color in general is optional, but when it is specified only property 'a' is optional.");
			}
		}

		/// <summary>
		/// Load <see cref="PlyData"/> from <paramref name="stream"/>.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="filename"></param>
		/// <param name="loadSettings"></param>
		/// <param name="convertSettings"></param>
		/// <returns></returns>
		/// <exception cref="PlyDataException"></exception>
		public static PlyData LoadPly(
			Stream stream,
			string filename,
			PlyLoadSettings loadSettings,
			ConverterSettings convertSettings = null
			)
		{
			convertSettings = convertSettings ?? ConverterSettings.Settings;

			bool allowTriangles = loadSettings.triangles != PrescenseSpecifier.Disallowed;
			bool allowQuads = loadSettings.quads != PrescenseSpecifier.Disallowed;


			var reader = new StreamReader(stream);
			int lineNo = 0;
			try
			{
#if DEBUG
				string prevLine;
#endif
				string line = null;

				void GetLine()
				{
#if DEBUG
					prevLine = line;
#endif
					line = reader.ReadLine();
					if (null == line)
					{
						throw new PlyDataException(lineNo, "Unexpected end of file.");
					}

					lineNo++;
				}

				void GetDataLine()
				{
					do
					{
						GetLine();
					}
					while (line.StartsWith(comment));
				}

				void ExpectLine(string expected)
				{
					GetDataLine();
					if (line != expected)
					{
						throw new PlyDataException(lineNo, $"Expected '{expected}' but found '{line}'");
					}
				}

				void ExpectStart(string expectedStart, bool readLine = true)
				{
					if (readLine)
					{
						GetDataLine();
					}

					if (!line.StartsWith(expectedStart))
					{
						throw new PlyDataException(lineNo, $"Expected '{expectedStart}'... but found '{line}'");
					}
				}

				void TryRegex<T>(Regex r, Func<string, T> parser, ref T result)
				{
					var match = r.Match(line);
					if (match.Success)
					{
						result = parser(match.Groups[1].Value);
					}
				}

				ExpectLine(fileTypeIdentifier);
				try
				{
					ExpectLine(formatHeader);
				}
				catch (PlyDataException e)
				{
					throw new PlyDataException($"Only format '{formatIdentifier}' is supported.", e);
				}

				int? subMeshIndex = null;
				string subMeshName = null;
				Shader? shaderID = null;

				bool wasCreatedByBlender = false;
				bool transformUpConvention = convertSettings.TransformUpConvention;
				bool reverseTriangles = convertSettings.ReverseTriangles;
				bool reverseQuads = convertSettings.ReverseQuads;

				Shader ParseShader(string s)
				{
					if (int.TryParse(s, out int parsedInt))
					{
						return (Shader)parsedInt;
					}
					else if (Enum.TryParse(s, ignoreCase: true, out Shader shader))
					{
						return shader;
					}
					else
					{
						throw new ArgumentException($"'{s}' could not be parsed to a number and was not recognized as a Shader name.");
					}
				}
				do
				{
					GetLine();
					lineNo++;
					wasCreatedByBlender |= CreatedByBlenderRegex.IsMatch(line);
					//transformUpConvention |= TransformUpConventionRegex.IsMatch(line);

					TryRegex(Mesh_SubMeshIndexRegex, s => int.Parse(s), ref subMeshIndex);
					TryRegex(Mesh_SubMeshNameRegex, s => s, ref subMeshName);
					TryRegex(Mesh_SubMeshShaderRegex, s => ParseShader(s), ref shaderID);
				}
				while (line.StartsWith(comment));

				if (filename != null)
				{
					var match = ShaderFileNameRegex.Match(filename);
					if (match.Success)
					{
						var newID = ParseShader(match.Groups[1].Value);
						if (shaderID.HasValue && newID != shaderID)
						{
							Console.WriteLine($"Warning: Shader ID specified in the comment {shaderID} does not match ID specified in the file name: {newID}. Using {newID}.");
						}
						shaderID = newID;
					}
				}

				ExpectStart(element_vertex, false);
				string sub = line.Substring(element_vertex.Length + 1); // +1 for space
				int vertexCount = int.Parse(sub);

				if (vertexCount <= 0)
				{
					throw new PlyDataException(lineNo, $"File contains no vertices.");
				}

				// Mapping from the vertexProperty to the index of that data in the file.
				Dictionary<PlyVertexProperty, int> propertyOrder = ReadHeader(GetLine, ref line, ref lineNo, out bool needReadLine);
				ValidateHeader(loadSettings, propertyOrder, lineNo);

				bool positionsSpecified =
					   propertyOrder.ContainsKey(PlyVertexProperty.PositionX)
					&& propertyOrder.ContainsKey(PlyVertexProperty.PositionY)
					&& propertyOrder.ContainsKey(PlyVertexProperty.PositionZ)
					;
				bool normalsSpecified =
					   propertyOrder.ContainsKey(PlyVertexProperty.NormalX)
					&& propertyOrder.ContainsKey(PlyVertexProperty.NormalY)
					&& propertyOrder.ContainsKey(PlyVertexProperty.NormalZ)
					;
				bool colorSpecified =
					   propertyOrder.ContainsKey(PlyVertexProperty.ColorR)
					&& propertyOrder.ContainsKey(PlyVertexProperty.ColorG)
					&& propertyOrder.ContainsKey(PlyVertexProperty.ColorB)
					;

				bool alphaSpecified =
					   propertyOrder.ContainsKey(PlyVertexProperty.ColorA);

				bool uvSpecified =
					   propertyOrder.ContainsKey(PlyVertexProperty.TextureU)
					&& propertyOrder.ContainsKey(PlyVertexProperty.TextureV);

				ExpectStart(element_face, needReadLine);
				sub = line.Substring(element_face.Length + 1 /* +1 for ' ' not in the const string */);
				int surfaceCount = int.Parse(sub);

				{
					const string expected = propertyDefinition + " list uchar ";
					ExpectStart(expected);
					// todo: check for "uint vertex_indices" or variations thereof
					string propertyDescription = line.Substring(expected.Length);

					string[] words = propertyDescription.Split(' ');

					if (words.Length != 2)
						throw new PlyDataException($"Unexpected length of property descriptor. Expected '{expected}' followed by one of [{string.Join(", ", allowedTypes)}] then followed by a word starting with '{property_vertexIndex_BM}'.", lineNo);

					if (!allowedTypes.Contains(words[0]))
					{
						throw new PlyDataException($"Unexpected type for the elements of vertex indices."
												 + $" Expected one of [{string.Join(", ", allowedTypes)}] but found '{words[0]}'.", lineNo);
					}

					if (!words[1].StartsWith(property_vertexIndex_BM))
					{
						throw new PlyDataException($"Unexpected property name: '{words[1]}', expected 'vertex_indices' although anything starting with '{property_vertexIndex_BM}' is acceptable.", lineNo);
					}
				}
				ExpectLine(endOfHeader);

				Bounds3 triangleBounds = new Bounds3(true);
				Bounds3 quadBounds = new Bounds3(true);
				Bounds3 allBounds = new Bounds3(true);

				var positions = positionsSpecified ? new List<Vector3>(vertexCount) : null;
				var normals = normalsSpecified ? new List<Vector3>(vertexCount) : null;
				var colors = colorSpecified ? new List<Color4>(vertexCount) : null;
				var uvs = uvSpecified ? new List<Vector2>(vertexCount) : null;
				for (int i = 0; i < vertexCount; i++)
				{
					GetDataLine();
					var parts = line.Split(' ');

					if (positionsSpecified)
					{
						var pos = new Vector3
							(
							 float.Parse(parts[propertyOrder[PlyVertexProperty.PositionX]], CultureInfo.InvariantCulture)
						   , float.Parse(parts[propertyOrder[PlyVertexProperty.PositionY]], CultureInfo.InvariantCulture)
						   , float.Parse(parts[propertyOrder[PlyVertexProperty.PositionZ]], CultureInfo.InvariantCulture)
						);

						pos = convertSettings.LoadVertex(pos);

						allBounds.Inflate(pos);

						positions.Add(pos);
					}

					if (normalsSpecified)
					{
						var norm = new Vector3
						(
							  float.Parse(parts[propertyOrder[PlyVertexProperty.NormalX]], CultureInfo.InvariantCulture)
							, float.Parse(parts[propertyOrder[PlyVertexProperty.NormalY]], CultureInfo.InvariantCulture)
							, float.Parse(parts[propertyOrder[PlyVertexProperty.NormalZ]], CultureInfo.InvariantCulture)
						);

						norm = convertSettings.LoadNormal(norm);
						normals.Add(norm);
					}

					if (colorSpecified)
					{
						Color4 col;
						byte r = byte.Parse(parts[propertyOrder[PlyVertexProperty.ColorR]], CultureInfo.InvariantCulture);
						byte g = byte.Parse(parts[propertyOrder[PlyVertexProperty.ColorG]], CultureInfo.InvariantCulture);
						byte b = byte.Parse(parts[propertyOrder[PlyVertexProperty.ColorB]], CultureInfo.InvariantCulture);
						byte a;
						if (alphaSpecified)
						{
							a = byte.Parse(parts[propertyOrder[PlyVertexProperty.ColorA]], CultureInfo.InvariantCulture);
						}
						else
						{
							a = 255;
						}

						col = new Color4(r, g, b, a);

						colors.Add(col);
					}

					if (uvSpecified)
					{
						var uv = new Vector2(
						 float.Parse(parts[propertyOrder[PlyVertexProperty.TextureU]], CultureInfo.InvariantCulture)
					   , float.Parse(parts[propertyOrder[PlyVertexProperty.TextureV]], CultureInfo.InvariantCulture));

						uvs.Add(uv);
					}
				}

				var triangles = allowTriangles ? new List<Triangle32>(surfaceCount) : null;
				var quads = allowQuads ? new List<Quad16>() : null;

				for (int i = 0; i < surfaceCount; i++)
				{
					GetDataLine();
					var parts = line.Split(' ');

					if (!int.TryParse(parts[0], out var cornerCount))
					{
						throw new PlyDataException
						(
							lineNo
						, $"Failed to parse corner count, received '{parts[0]}'."
						);
					}

					if (parts.Length != cornerCount + 1)
					{
						throw new PlyDataException(lineNo, $"Expected {cornerCount + 1} numbers but got {parts.Length}.");
					}

					UInt32 a = UInt32.Parse(parts[1]);
					UInt32 b = UInt32.Parse(parts[2]);
					UInt32 c = UInt32.Parse(parts[3]);

					Vector3 va;
					Vector3 vb;
					Vector3 vc;
					Vector3 vd;

					try
					{
						va = positions[(int)a];
						vb = positions[(int)b];
						vc = positions[(int)c];
					}
					catch (Exception e)
					{
						// todo: make specific for the index that was out of range.
						throw new PlyDataException(lineNo, $"Encountered out of range vertex index at face index {i}.", e);
					}

					if (allowTriangles && cornerCount == 3)
					{
						Triangle32 t;
						if (reverseTriangles)
						{
							t = new Triangle32(c, b, a);
						}
						else
						{
							t = new Triangle32(a, b, c);
						}

						triangles.Add(t);

						triangleBounds.Inflate(va);
						triangleBounds.Inflate(vb);
						triangleBounds.Inflate(vc);
					}
					else if (allowQuads && cornerCount == 4)
					{
						UInt16 d = UInt16.Parse(parts[4]);

						Quad16 q;

						try
						{
							vd = positions[d];
						}
						catch (Exception e)
						{
							// todo: make specific for the index that was out of range.
							throw new PlyDataException(lineNo, $"Encountered out of range vertex index at face index {i}.", e);
						}

						if (reverseQuads)
						{
							q = new Quad16(d, (UInt16)c, (UInt16)b, (UInt16)a);
						}
						else
						{
							q = new Quad16((UInt16)a, (UInt16)b, (UInt16)c, d);
						}

						quads.Add(q);

						quadBounds.Inflate(va);
						quadBounds.Inflate(vb);
						quadBounds.Inflate(vc);
						quadBounds.Inflate(vd);
					}
					else
					{
						throw new PlyDataException
						(
							lineNo
						, $"Failed to parse corner count, received '{parts[0]}'."
						);
					}
				}

				var m = new PlyData(
					PlyOriginalDataKind.Ply
					, positions
					, normals
					, colors
					, uvs
					, triangles
					, quads
					, allBounds
					, triangleBounds
					, quadBounds
					, shaderID
				);

				m.submesh_index = subMeshIndex;
				m.submesh_name = subMeshName;
				return m;
			}
			catch (PlyDataException)
			{
				throw;
			}
			catch (Exception e)
			{
				throw new PlyDataException(lineNo, e);
			}
		}

		/// <summary>
		/// Save <see cref="PlyData"/> to <paramref name="stream"/>.
		/// </summary>
		/// <param name="data"></param>
		/// <param name="stream"></param>
		/// <param name="settings"></param>
		/// <exception cref="ArgumentNullException"></exception>
		public static void Save(PlyData data, Stream stream, ConverterSettings settings = null)
		{
			settings = settings ?? ConverterSettings.Settings;
			if (null == data) throw new ArgumentNullException(nameof(data));
			if (null == stream) throw new ArgumentNullException(nameof(stream));

			var w = new StreamWriter(stream);
			w.NewLine = newLine;

			var positions = data.positions.AsOrToListNullable();
			var normals = data.normals.AsOrToListNullable();
			var colors = data.colors.AsOrToListNullable();
			var uvs = data.uvs.AsOrToListNullable();

			int numVertices = positions?.Count() ?? normals?.Count() ?? colors?.Count() ?? uvs?.Count() ?? throw new ArgumentNullException("All of the vertex property collections ware null");

			var triangles = data.triangles;
			var quads = data.quads;

			void EmptyLine() => w.WriteLine();
			void AppendLine(string s) => w.WriteLine(s);

			void AppendLineElements(params object[] array)
			{
				string MyToString(object o)
				{
					if (o is float f)
					{
						return f.ToString(CultureInfo.InvariantCulture);
					}
					else
					{
						return o.ToString();
					}
				}

				w.WriteLine(string.Join(" ", array.Where(e => e != null).Select(MyToString)));
			}

			AppendLine(fileTypeIdentifier);
			AppendLine(formatHeader);

			AppendLine(authorNote);
			if (!string.IsNullOrEmpty(data.original_mesh_name))
			{
				if (data.submesh_index.HasValue)
				{
					string submeshNamePart = data.submesh_name != null ? $" '{data.submesh_name}'" : string.Empty;
					AppendLine($"{comment} original {data.originalDataKind} name: '{data.original_mesh_name}' subMesh #{data.submesh_index + 1}/{data.submesh_count}{submeshNamePart}");
				}
				else
				{
					AppendLine($"{comment} Original {data.originalDataKind} name: '{data.original_mesh_name}'");
				}
			}
			if (data.submesh_index != null)
			{
				AppendLine($"{comment} SubMesh index: {data.submesh_index}");
			}
			if (data.submesh_index != null)
			{
				AppendLine($"{comment} SubMesh name: '{data.submesh_name ?? "null"}'");
			}
			if (data.shader != null)
			{
				var shader = data.shader.Value;
				AppendLine($"{comment} Shader: {shader} ({(int)shader})");
			}

			AppendLine($"{elementDefinition} vertex {numVertices}");

			if (positions != null)
			{
				AppendLine($"{propertyDefinition} float x");
				AppendLine($"{propertyDefinition} float y");
				AppendLine($"{propertyDefinition} float z");
			}

			if (normals != null)
			{
				AppendLine($"{propertyDefinition} float nx");
				AppendLine($"{propertyDefinition} float ny");
				AppendLine($"{propertyDefinition} float nz");
			}

			if (colors != null)
			{
				AppendLine($"{propertyDefinition} {@byte} red");
				AppendLine($"{propertyDefinition} {@byte} green");
				AppendLine($"{propertyDefinition} {@byte} blue");
				AppendLine($"{propertyDefinition} {@byte} alpha");
			}

			if (uvs != null)
			{
				AppendLine($"{propertyDefinition} float u");
				AppendLine($"{propertyDefinition} float v");
			}


			int numFaces = triangles?.Count() ?? 0 + quads?.Count() ?? 0;
			AppendLine($"{elementDefinition} face {numFaces}");

			//                                 list, length defined by a single byte
			//                                      |||||
			//                                             /--- ushort index
			AppendLine($"{propertyDefinition} list uchar ushort vertex_index");
			AppendLine(endOfHeader);

			for (int i = 0; i < numVertices; i++)
			{
				var p = positions?[i];
				var n = normals?[i];
				var c = colors?[i];
				var u = uvs?[i];

				p = p != null ? (Vector3?)settings.SaveVertex(p.Value) : null;
				n = n != null ? (Vector3?)settings.SaveNormal(n.Value) : null;

				AppendLineElements(p?.X, p?.Y, p?.Z, n?.X, n?.Y, n?.Z, (byte?)(c?.R * 255f), (byte?)(c?.G * 255f), (byte?)(c?.B * 255f), (byte?)(c?.A * 255f), u?.X, u?.Y);
			}

			foreach (var t in triangles ?? Array.Empty<Triangle32>())
			{
				var a = t.a;
				var b = t.b;
				var c = t.c;

				if (settings.ReverseTriangles)
				{
					AppendLineElements(indicesInTriangle, c, b, a);
				}
				else
				{
					AppendLineElements(indicesInTriangle, a, b, c);
				}
			}

			foreach (var q in quads ?? Array.Empty<Quad16>())
			{
				var a = q.a;
				var b = q.b;
				var c = q.c;
				var d = q.d;

				if (settings.ReverseQuads)
				{
					AppendLineElements(indicesInQuad, d, c, b, a);
				}
				else
				{
					AppendLineElements(indicesInQuad, a, b, c, d);
				}
			}

			EmptyLine();
			w.Flush();
		}

		#endregion Ply

		#region Mesh

		/// <summary>
		/// Load a <see cref="Mesh"/> from the specified path.
		/// If <paramref name="multiFile"/> is true and <paramref name="filePath"/> is in the format 'path/name.0.extension'
		/// then the loader will search for all files in the folder with the format 'name.*.extension'
		/// and generate a single result with all the files becoming subMeshes in the result.
		/// </summary>
		/// <param name="filePath">The filePath to load from.</param>
		/// <param name="multiFile">If true will attempt to load subMeshes from separate files.</param>
		/// <param name="writeFileNames">When multiple files will be loaded (submeshes) write the names to the console.</param>
		/// <returns></returns>
		/// <exception cref="FileInteractionException"></exception>
		public static Mesh LoadMesh(string filePath, bool multiFile = true, ConverterSettings settings = null, bool writeFileNames = true)
		{
			settings = settings ?? ConverterSettings.Settings;

			try
			{
				if (multiFile && MultiFileRegex(filePath, out var files))
				{
					var parts = new List<MeshCombiner.DataItem>();
					foreach (FileInfo fileInfo in files)
					{
						try
						{
							if (writeFileNames)
							{
								Console.WriteLine($"Reading SubMesh: {fileInfo.Name}");
							}
							using (var stream = File.OpenRead(fileInfo.FullName))
							{
								var partMesh = LoadMesh(stream, fileInfo.Name, settings);
								parts.Add(new MeshCombiner.DataItem(partMesh, Matrix4.Identity));
							}
						}
						catch (Exception e)
						{
							throw new FileInteractionException(fileInfo.FullName, e);
						}
					}

					var result = MeshCombiner.Combine(parts, deduplicateVertexRecords: false);

					return result;
				}

				using (var stream = File.OpenRead(filePath))
				{
					return LoadMesh(stream, Path.GetFileName(filePath), settings);
				}
			}
			catch (FileInteractionException)
			{
				throw;
			}
			catch (Exception e)
			{
				throw new FileInteractionException(filePath, e);
			}
		}

		public static Mesh LoadMesh(Stream stream, string filename, ConverterSettings settings = null)
		{
			var loadSettings = new PlyLoadSettings();
			loadSettings.position = PrescenseSpecifier.Required;
			loadSettings.normal = PrescenseSpecifier.Required;
			loadSettings.color = PrescenseSpecifier.Optional;
			loadSettings.uv = PrescenseSpecifier.Disallowed;

			loadSettings.triangles = PrescenseSpecifier.Required;
			loadSettings.quads = PrescenseSpecifier.Disallowed;

			var data = LoadPly(stream, filename, loadSettings, settings);

			
			var positions = data.positions.AsOrToList();
			var normals = data.normals.AsOrToList();
			var colors = data.colors.AsOrToListNullable();
			
			// The color used when colors are not defined by the file.
			var defaultColor = Color4.White;

			var vertices = new List<VertexRecord>(positions.Count);

			for (int i = 0; i < positions.Count(); i++)
			{
				var pos = positions[i];
				var nor = normals[i];
				var col = colors?[i] ?? defaultColor;

				var vertex = new VertexRecord(pos, col, nor);
				vertices.Add(vertex);
			}

			var indices = new List<uint>();
			foreach (var triangle in data.triangles)
			{
				indices.Add(triangle.a);
				indices.Add(triangle.b);
				indices.Add(triangle.c);
			}

			var mesh = new Mesh(vertices, indices, data.triangleBounds, true);
			if (data.shader.HasValue)
			{
				mesh.subMeshes[0].shaderId = data.shader.Value;
			}
			if (data.submesh_name != null)
			{
				mesh.subMeshes[0].name = data.submesh_name;
			}
			return mesh;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="m"></param>
		/// <param name="filePath"></param>
		/// <param name="settings"></param>
		/// <param name="writeFileNames">When multiple files will be written (submeshes) write the names to the console.</param>
		/// <exception cref="FileInteractionException"></exception>
		public static void Save(Mesh m, string filePath, ConverterSettings settings = null, bool writeFileNames = true)
		{
			try
			{
				if (m.SingleSubMesh)
				{
					using (var stream = File.Open(filePath, FileMode.Create))
					{
						Save(m, stream, settings);
					}
				}
				else
				{
					var fileDirectory = Path.GetDirectoryName(filePath);
					var fileNameNX = Path.GetFileNameWithoutExtension(filePath);
					var extension = Path.GetExtension(filePath);

					for (int i = 0; i < m.SubMeshCount; i++)
					{
						SubMesh sm = m.subMeshes[i];
						string subMeshName = sm.name;
						if (string.IsNullOrWhiteSpace(subMeshName))
						{
							subMeshName = i.ToString();
						}

						var thisFilePath = Path.Combine(fileDirectory, $"{fileNameNX}.{i:000}{extension}");
						using (var stream = File.Open(thisFilePath, FileMode.Create))
						{
							Save(m, stream, i, settings);

							if (writeFileNames)
							{
								Console.WriteLine($"Wrote SubMesh #{i,-2} {Path.GetFileName(thisFilePath)}");
							}
						}
					}
				}
			}
			catch (Exception e)
			{
				throw new FileInteractionException(filePath, e);
			}
		}

		public static void Save(Mesh mesh, Stream stream, ConverterSettings settings = null)
		{
			if (null == mesh) throw new ArgumentNullException(nameof(mesh));
			if (! mesh.SingleSubMesh)
				throw new ArgumentException($"Unable to save a mesh with multiple submeshes to a single stream.");

			Save(mesh, stream, 0, settings);
		}


		public static void Save(Mesh mesh, Stream stream, int subMeshIndex, ConverterSettings settings = null)
		{
			settings = settings ?? ConverterSettings.Settings;
			if (null == mesh) throw new ArgumentNullException(nameof(mesh));
			if (null == stream) throw new ArgumentNullException(nameof(stream));
			if (subMeshIndex < 0 || subMeshIndex >= mesh.SubMeshCount)
				throw new ArgumentOutOfRangeException(nameof(subMeshIndex), subMeshIndex, $"Submesh index out of range (max index: {mesh.SubMeshCount - 1})");

			var w = new StreamWriter(stream);
			w.NewLine = newLine;

			var subMesh = mesh.subMeshes[subMeshIndex];

			var vertexRecords = mesh.vertices;
			var indices = mesh.indices;

			var positions = new List<Vector3>(vertexRecords.Count);
			var normals   = new List<Vector3>(vertexRecords.Count);
			var colors    = new List<Color4>(vertexRecords.Count);

			var triangles = new List<Triangle32>();

			foreach (var vertex in vertexRecords)
			{
				positions.Add(vertex.position);
				normals.Add(vertex.normal);
				colors.Add(vertex.color);
			}

			for (int i = 0; i < indices.Count; i += 3)
			{
				var a = (UInt32) indices[i + 0];
				var b = (UInt32) indices[i + 1];
				var c = (UInt32) indices[i + 2];

				var t = new Triangle32(a, b, c);

				triangles.Add(t);
			}

			var plyData = new PlyData(
				  PlyOriginalDataKind.Mesh
				, positions
				, normals
				, colors
				, null // uv
				, triangles
				, null // quads
				, subMesh.shaderId
				);

			plyData.original_mesh_name = mesh.fileName;
			plyData.submesh_index = subMeshIndex;
			plyData.submesh_count = mesh.SubMeshCount;
			plyData.submesh_name = subMesh.name;

			Save(plyData, stream);
		}

		#endregion Mesh

		#region Phys

		/// <summary>
		/// Load a <see cref="Phys"/> from the specified path.
		/// If <paramref name="multiFile"/> is true and <paramref name="filePath"/> is in the format 'path/name.0.extension'
		/// then the loader will search for all files in the folder with the format 'name.*.extension'
		/// and generate a single result with all the files becoming subMeshes in the result.
		/// </summary>
		/// <param name="filePath">The filePath to load from.</param>
		/// <param name="multiFile">If true will attempt to load subMeshes from separate files.</param>
		/// <param name="writeFileNames">When multiple files will be loaded (submeshes) write the names to the console.</param>
		/// <returns></returns>
		/// <exception cref="FileInteractionException"></exception>
		public static Phys LoadPhys(string filePath, bool multiFile = true, ConverterSettings settings = null, bool writeFileNames = true)
		{
			settings = settings ?? ConverterSettings.Settings;

			try
			{
				if (multiFile && MultiFileRegex(filePath, out var files))
				{
					var subMeshes = new List<PhySubMesh>();

					foreach (FileInfo fileInfo in files)
					{
						try
						{
							if (writeFileNames)
							{
								Console.WriteLine($"Reading SubMesh: {fileInfo.Name}");
							}
							using (var stream = File.OpenRead(fileInfo.FullName))
							{
								var fullPhys = LoadPhys(stream, settings);
								subMeshes.Add(fullPhys.subMeshes[0]);
							}
						}
						catch (Exception e)
						{
							throw new FileInteractionException(fileInfo.FullName, e);
						}
					}

					return new Phys(subMeshes, true);
				}

				using (var stream = File.OpenRead(filePath))
				{
					return LoadPhys(stream, settings);
				}
			}
			catch (Exception e)
			{
				throw new FileInteractionException(filePath, e);
			}
		}

		/// <summary>
		/// Loads a <see cref="Phys"/> from a .ply ASCII (1.0) stream.
		/// </summary>
		/// <param name="stream"></param>
		/// <returns></returns>
		/// <exception cref="PlyDataException"></exception>
		public static Phys LoadPhys(Stream stream, ConverterSettings settings = null)
		{
			var loadSettings = new PlyLoadSettings();
			loadSettings.position = PrescenseSpecifier.Required;
			loadSettings.normal = PrescenseSpecifier.Disallowed;
			loadSettings.color = PrescenseSpecifier.Disallowed;
			loadSettings.uv = PrescenseSpecifier.Disallowed;

			loadSettings.triangles = PrescenseSpecifier.Required;
			loadSettings.quads = PrescenseSpecifier.Disallowed;

			var data = LoadPly(stream, null, loadSettings, settings);

			var vertices = data.positions as List<Vector3> ?? data.positions.ToList();
			var indices = new List<uint>(data.triangles.Count() * 3);
			foreach (var triangle in data.triangles)
			{
				indices.Add(triangle.a);
				indices.Add(triangle.b);
				indices.Add(triangle.c);
			}

			bool hadIndices = indices.Any();
			int indexCount = indices.Count;
			int vertexCount = vertices.Count;

			// If the indices are degenerate remove them from the mesh.
			// By degenerate I mean that it just goes 1,2,3,4,5...
			// This indicates that the vertex order already defines the triangles.
			// The indices are then irrelevant.
			bool degenerate = true;
			for (int i = 0; i < indexCount; i++)
			{
				if (indices[i] != i)
				{
					degenerate = false;
					break;
				}
			}

			if (degenerate)
			{
				indices.Clear();
				indices.Capacity = 0;
			}

			if ((! hadIndices || degenerate) && settings.ReverseTriangles)
			{
				// If we need to reverse triangles, and there are no indices then we need to re-order the vertices themselves.
				for (int i = 0; i < vertexCount; i += 3)
				{
					var a = vertices[i + 0];
					//var b = vertices[i + 1];
					var c = vertices[i + 2];

					vertices[i + 0] = c;
					//vertices[i + 1] = b;
					vertices[i + 2] = a;
				}
			}

			var m = new Phys(new PhySubMesh(vertices, indices, true));

			return m;
			
		}

		public static void Save(Phys phys, Stream stream, ConverterSettings settings = null)
		{
			if (phys.subMeshes.Count > 1)
				throw new ArgumentException("Can only save a a single SubMesh to a single stream.");

			SavePhyMesh(phys, stream, phys.fileName, 0, settings);
		}

		/// <summary>
		/// Save the specified <see cref="PhySubMesh"/> to the provided stream.
		/// </summary>
		/// <param name="phys"></param>
		/// <param name="stream"></param>
		/// <param name="subMeshIndex"></param>
		public static void Save(Phys phys, Stream stream, int subMeshIndex, ConverterSettings settings = null)
		{
			SavePhyMesh(phys, stream, phys.fileName, subMeshIndex, settings);
		}


		public static void Save(Phys m, string mainFilePath, ConverterSettings settings = null)
		{
			try
			{
				if (m.subMeshes.Any(sm => sm.hasIndices))
				{
					// It seems SW doesn't read the index buffer we provide.
					// Perhaps the header is wrong.
					// For now we just save as ordered vertices because that definitely works.
					// Todo: fix the above.

					m = m.Clone(); // Don't modify caller's object
					m.ConvertToOrderedVertices(strict: false);
				}

				if (m.subMeshCount == 1)
				{
					using (var stream = File.Open(mainFilePath, FileMode.Create))
					{
						SavePhyMesh(m, stream, m.fileName, 0, settings);
					}
				}
				else
				{
					for(int meshNo = 0; meshNo < m.subMeshCount; meshNo++)
					{
						var directoryName = Path.GetDirectoryName(mainFilePath);
						if (string.IsNullOrWhiteSpace(directoryName))
							directoryName = "";
						else
							directoryName += Path.DirectorySeparatorChar;
						var nameWithoutExtension = Path.GetFileNameWithoutExtension(mainFilePath);
						var extension = Path.GetExtension(mainFilePath);

						var newFilePath = $"{directoryName}{nameWithoutExtension}.{meshNo:000}{extension}";
						try
						{
							using (var stream = File.Open(newFilePath, FileMode.Create))
							{
								SavePhyMesh(m, stream, $"{m.fileName}", meshNo, settings);
							}
						}
						catch (Exception e)
						{
							throw new FileInteractionException(newFilePath, e);
						}
					}
				}
			}
			catch (Exception e)
			{
				throw new FileInteractionException(mainFilePath, e);
			}
		}

		internal static void SavePhyMesh(Phys phys, Stream stream, string name, int subMeshNo, ConverterSettings settings = null)
		{
			settings = settings ?? ConverterSettings.Settings;
			var w = new StreamWriter(stream);
			w.NewLine = newLine;

			var m = phys.subMeshes[subMeshNo];

			var vertices = m.vertices;
			var indices  = m.indices;

			int vertexCount = vertices.Count;
			int indexCount = indices?.Count ?? 0;
			bool hasIndices = indices != null && indices.Count >= 3;

			var triangles = new List<Triangle32>(hasIndices ? indexCount / 3 : vertexCount / 3);

			if (hasIndices)
			{
				for (int i = 0; i < vertexCount; i += 3)
				{
					var a = (UInt32) indices[i + 0];
					var b = (UInt32) indices[i + 1];
					var c = (UInt32) indices[i + 2];

					var t = new Triangle32(a, b, c);

					triangles.Add(t);
				}
			}
			else
			{
				for (UInt32 i = 0; i < vertexCount; i += 3)
				{
					var a = i + 0;
					var b = i + 1;
					var c = i + 2;

					var t = new Triangle32(a, b, c);

					triangles.Add(t);
				}
			}

			var plyData = new PlyData(
				  PlyOriginalDataKind.Phys
				, vertices
				, null // normals
				, null // colors
				, null // uv
				, triangles
				, null // quads
				, null // shader, not applicable
				);

			Save(plyData, stream, settings);
		}

		#endregion Phys

		#region MapBin

		private static ConverterSettings MapBinConvertrSettings = new ConverterSettings()
		{
			TransformUpConvention = true,
			ReverseTriangles = false,
			ReverseQuads = true,
			//settings.MirrorY = true,
			//settings.MirrorZ = true,
		};

		private const float offset = 10f;
		private static readonly Dictionary<VertexLayerKind, float> v_y_offsets = new Dictionary<VertexLayerKind, float>()
		{
			{ VertexLayerKind.Gray,       offset * 0 },
			{ VertexLayerKind.Grass,      offset * 1 },
			{ VertexLayerKind.Sand,       offset * 2 },
			{ VertexLayerKind.Water,      offset * 3 },
			{ VertexLayerKind.White,      offset * 4 },
			{ VertexLayerKind.Rock,       offset * 5 },
			{ VertexLayerKind.Dirt,       offset * 6 },

			{ VertexLayerKind.Sea_Shade0, offset * -1 },
			{ VertexLayerKind.Sea_Shade1, offset * -2 },
			{ VertexLayerKind.Sea_Shade2, offset * -3 },
			{ VertexLayerKind.Sea_shade3, offset * -4 },
		};

		private const float texLayer_start_offset = offset * 7;

		/// <summary>
		/// Get the <see cref="Color4"/> to encode the <see cref="TextureLayer"/> data for ply.
		/// </summary>
		/// <param name="layerIndex"></param>
		/// <returns></returns>
		/// <exception cref="ArgumentOutOfRangeException"></exception>
		public static Color4 TextureLayerColor(int layerIndex)
		{
			ThrowIfTextureLayerOutOfRange(layerIndex);

			float r = 0f;
			float g = 0f;
			float b = layerIndex / 255f;
			float a = 1f;

			return new Color4(r, g, b, a);
		}

		public static MapBin LoadMapBin(string filePath, ConverterSettings settings = null)
		{
			try
			{
				using (var stream = File.OpenRead(filePath))
				{
					return LoadMapBin(stream, Path.GetFileName(filePath), settings);
				}
			}
			catch (Exception e)
			{
				throw new FileInteractionException(filePath, e);
			}
		}

		public static MapBin LoadMapBin(Stream stream, string filename, ConverterSettings settings = null)
		{
			settings = settings ?? MapBinConvertrSettings;

			var loadSettings = new PlyLoadSettings();
			loadSettings.position = PrescenseSpecifier.Required;
			loadSettings.normal   = PrescenseSpecifier.Optional;
			loadSettings.color    = PrescenseSpecifier.Required;
			loadSettings.uv       = PrescenseSpecifier.Optional;

			loadSettings.triangles = PrescenseSpecifier.Required;
			loadSettings.quads     = PrescenseSpecifier.Required;

			var data = LoadPly(stream, filename, loadSettings, settings);

			var positions = data.positions.AsOrToList();
			var colors    = data.colors.AsOrToList();
			var uvs       = data.uvs.AsOrToListNullable();

			var triangles = data.triangles.AsOrToList();
			var quads     = data.quads.AsOrToList();

			var mb = new MapBin();

			int GetVertexLayerIndex(Color4 color, int triangleIndex, int vertexIndex)
			{
				for (int i = 0; i < MapBin.colorLayerMap.Length; i++)
				{
					if (color == MapBin.colorLayerMap[i])
					{
						return i;
					}
				}

				throw new DataException($"Invalid color: {color} in vertex #{vertexIndex} referenced by triangle {triangleIndex}.");
			}

			int GetTextureLayerIndex(Color4 color, int quadIndex, int vertexIndex)
			{
				int blue = (int)(color.B * 255f);

				if (   blue >= 0
					&& blue <= textureLayerCount - 1
					&& color.R == 0
					&& color.G == 0
				)
				{
					return blue;
				}

				throw new DataException($"Invalid color: {color} in vertex #{vertexIndex} referenced by quad {quadIndex}.");
			}

			int triangleIndex = 0;
			foreach (var t in data.triangles)
			{
				var va = positions[(int)t.a];
				var vb = positions[(int)t.b];
				var vc = positions[(int)t.c];

				var cca = colors[(int)t.a];
				var ccb = colors[(int)t.b];
				var ccc = colors[(int)t.c];


				// Undo blender convenience separation.
				va.Y = 0;
				vb.Y = 0;
				vc.Y = 0;

				var li_a = GetVertexLayerIndex(cca, triangleIndex, (int)t.a);
				var li_b = GetVertexLayerIndex(ccb, triangleIndex, (int)t.b);
				var li_c = GetVertexLayerIndex(ccc, triangleIndex, (int)t.c);

				if (li_a != li_b || li_b != li_c)
				{
					throw new DataException($"Colors on a triangle #{triangleIndex} don't match! (Vertices {t.a}, {t.b}, {t.c})");
				}

				int layer = li_a;
				var layerData = mb.vertexLayers[layer];

				var firstVertexIndex = layerData.Vertices.Count;

				layerData.Vertices.Add(va);
				layerData.Vertices.Add(vb);
				layerData.Vertices.Add(vc);

				var adjt = new Triangle16(
					  (UInt16) (firstVertexIndex + 0)
					, (UInt16) (firstVertexIndex + 1)
					, (UInt16) (firstVertexIndex + 2));

				layerData.Triangles.Add(adjt);

				triangleIndex++;
			}

			int quadIndex = 0;
			foreach (var q in data.quads)
			{
				var va = positions[q.a];
				var vb = positions[q.b];
				var vc = positions[q.c];
				var vd = positions[q.d];

				var cca = colors[q.a];
				var ccb = colors[q.b];
				var ccc = colors[q.c];
				var ccd = colors[q.d];

				// Undo blender convenience separation.
				va.Y = 0;
				vb.Y = 0;
				vc.Y = 0;
				vd.Y = 0;

				var ca = GetTextureLayerIndex(cca, quadIndex, q.a);
				var cb = GetTextureLayerIndex(ccb, quadIndex, q.b);
				var cc = GetTextureLayerIndex(ccc, quadIndex, q.c);
				var cd = GetTextureLayerIndex(ccd, quadIndex, q.d);

				if (ca != cb || cb != cc || cc != cd)
				{
					throw new DataException($"Colors on a quad #{quadIndex} don't match! (Vertices {q.a}, {q.b}, {q.c}, {q.d})");
				}

				int layer = ca;
				var layerData = mb.textureLayers[layer];

				var uva = uvs?[q.a] ?? new Vector2(1, 0);
				var uvb = uvs?[q.b] ?? new Vector2(1, 1);
				var uvc = uvs?[q.c] ?? new Vector2(1, 1);
				var uvd = uvs?[q.d] ?? new Vector2(1, 0);

				layerData.Vertices.Add(new Vector3UV(va, uva));
				layerData.Vertices.Add(new Vector3UV(vb, uvb));
				layerData.Vertices.Add(new Vector3UV(vc, uvc));
				layerData.Vertices.Add(new Vector3UV(vd, uvd));

				quadIndex++;
			}

			return mb;
		}

		public static void Save(MapBin data, Stream stream, ConverterSettings settings = null)
		{
			settings = settings ?? MapBinConvertrSettings;

			Vector3 normal = Vector3.UnitY;

			var vertexCount
				= data.vertexLayers.Sum(l => l.Vertices.Count)
				+ data.textureLayers.Sum(l => l.Vertices.Count)
				;

			var triangleCount = data.vertexLayers.Sum(l => l.Triangles.Count);
			var quadCount = data.textureLayers.Sum(l => l.Vertices.Count) / 4;

			var positions = new List<Vector3>(vertexCount);
			var normals   = new List<Vector3>(vertexCount);
			var colors    = new List<Color4>(vertexCount);
			var uvs       = new List<Vector2>(vertexCount);

			var triangles = new List<Triangle32>(triangleCount);
			var quads     = new List<Quad16>(quadCount);

			var allBounds      = new Bounds3(true);
			var triangleBounds = new Bounds3(true);
			var quadBounds     = new Bounds3(true);

			foreach (var ld in data.vertexLayers)
			{
				float y_pos = v_y_offsets[ld.Layer];
				Color4 color = VertexLayerColor(ld.LayerIndex);
				foreach (var t in ld.Triangles)
				{
					var va = ld.Vertices[t.a];
					var vb = ld.Vertices[t.b];
					var vc = ld.Vertices[t.c];

					// Apply Blender convenience separation between the layers.
					va.Y = y_pos;
					vb.Y = y_pos;
					vc.Y = y_pos;

					var nextIndex = positions.Count;

					positions.Add(va);
					positions.Add(vb);
					positions.Add(vc);

					normals.Add(normal);
					normals.Add(normal);
					normals.Add(normal);

					colors.Add(color);
					colors.Add(color);
					colors.Add(color);

					// We need to supply uv data for all vertices, even though they don't apply to the vertices of the triangle layers.
					// Because we must export the vertices with the same properties.
					uvs.Add(Vector2.Zero);
					uvs.Add(Vector2.Zero);
					uvs.Add(Vector2.Zero);
					uvs.Add(Vector2.Zero);

					triangles.Add(new Triangle32(
						  (UInt32) nextIndex + 0u
						, (UInt32) nextIndex + 1u
						, (UInt32) nextIndex + 2u
						));

					triangleBounds.Inflate(va);
					triangleBounds.Inflate(vb);
					triangleBounds.Inflate(vc);
				}
			}

			foreach(var ld in data.textureLayers)
			{
				float y_pos = texLayer_start_offset + (offset * ld.LayerIndex);
				Color4 color = TextureLayerColor(ld.LayerIndex);
				for(int i = 0; i < ld.Vertices.Count; i += 4)
				{
					var va = ld.Vertices[i + 0];
					var vb = ld.Vertices[i + 1];
					var vc = ld.Vertices[i + 2];
					var vd = ld.Vertices[i + 3];

					var pa = va.position;
					var pb = vb.position;
					var pc = vc.position;
					var pd = vd.position;

					// Apply Blender convenience separation between the layers.
					pa.Y = y_pos;
					pb.Y = y_pos;
					pc.Y = y_pos;
					pd.Y = y_pos;

					var uva = va.uv;
					var uvb = vb.uv;
					var uvc = vc.uv;
					var uvd = vd.uv;

					var nextIndex = positions.Count;

					positions.Add(pa);
					positions.Add(pb);
					positions.Add(pc);
					positions.Add(pd);

					normals.Add(normal);
					normals.Add(normal);
					normals.Add(normal);
					normals.Add(normal);

					colors.Add(color);
					colors.Add(color);
					colors.Add(color);
					colors.Add(color);

					uvs.Add(uva);
					uvs.Add(uvb);
					uvs.Add(uvc);
					uvs.Add(uvd);

					quads.Add(new Quad16(
						  nextIndex + 0
						, nextIndex + 1
						, nextIndex + 2
						, nextIndex + 3
						));

					quadBounds.Inflate(pa);
					quadBounds.Inflate(pb);
					quadBounds.Inflate(pc);
					quadBounds.Inflate(pd);
				}
			}

			allBounds.Inflate(triangleBounds.Min);
			allBounds.Inflate(triangleBounds.Max);

			allBounds.Inflate(quadBounds.Min);
			allBounds.Inflate(quadBounds.Max);

			var ply = new PlyData(
				  PlyOriginalDataKind.MapGeometryBin
				, positions
				, normals
				, colors
				, uvs
				, triangles
				, quads
				, allBounds
				, triangleBounds
				, quadBounds
				);

			Save(ply, stream, settings);
		}

		public static void Save(MapBin data, string filePath, ConverterSettings settings = null)
		{
			try
			{
				using (var stream = File.Open(filePath, FileMode.Create))
				{
					Save(data, stream, settings);
				}
			}
			catch (Exception e)
			{
				throw new FileInteractionException(filePath, e);
			}
		}

		#endregion MapBin
	}
}
