﻿// Copyright 2022-2024 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using OpenToolkit.Mathematics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;

namespace BinaryDataModel.DataTypes
{
	/// <summary>
	/// A single record of vertex data for Stormworks Mesh
	/// </summary>
	[StructLayout(LayoutKind.Sequential)]
	public struct VertexRecord : IEquatable<VertexRecord>
	{
		// todo: this is only valid for SWMesh -> move it to parsers
		private const byte bytesPerComponent = sizeof(float);
		private const byte entriesPerVertex = 7;

		/// <summary>
		/// When saved into a <see cref="Mesh"/> the amount of <see cref="byte"/>s each <see cref="VertexRecord"/> takes to save.
		/// </summary>
		public const byte bytesPerVertex = entriesPerVertex * bytesPerComponent;

		/// <summary>
		/// When saved into a <see cref="Mesh"/> the amount of <see cref="byte"/>s each <see cref="VertexRecord"/> takes to save.
		/// </summary>
		public const uint SizeEstimate = bytesPerVertex;

		/// <summary>
		/// The vertex position.
		/// </summary>
		public Vector3 position;
		/// <summary>
		/// The vertex color
		/// </summary>
		public Color4 color;

		/// <summary>
		/// The vertex normal
		/// </summary>
		public Vector3 normal;


		public VertexRecord(IEnumerable<byte> enumerable) : this(enumerable.ToArray()) { }

		public VertexRecord(byte[] bytes)
		{
			if(bytes.Length != bytesPerVertex) throw new ArgumentOutOfRangeException(nameof(bytes), "Must be of length " + bytesPerVertex);
			{
				var x = BitConverter.ToSingle(bytes, 0 * sizeof(float));
				var y = BitConverter.ToSingle(bytes, 1 * sizeof(float));
				var z = BitConverter.ToSingle(bytes, 2 * sizeof(float));
				position = new Vector3(x,y,z);
			}
			{
				var i = 3 * sizeof(float);
				byte r = bytes[i++];
				byte g = bytes[i++];
				byte b = bytes[i++];
				byte a = bytes[i++];

				color = new Color4(r, g, b, a);
			}
			{
				var x = BitConverter.ToSingle(bytes, 4 * sizeof(float));
				var y = BitConverter.ToSingle(bytes, 5 * sizeof(float));
				var z = BitConverter.ToSingle(bytes, 6 * sizeof(float));
				normal = new Vector3(x, y, z);
			}
		}

		public VertexRecord(Vector3 position, Color4 color, Vector3 normal)
		{
			this.position = position;
			this.color = color;
			this.normal = normal;
		}

		public static bool operator == (VertexRecord left, VertexRecord right)
		{
			return left.Equals(right);
		}

		public static bool operator != (VertexRecord left, VertexRecord right)
		{
			return ! (left  == right);
		}

		/// <inheritdoc />
		public bool Equals(VertexRecord other)
		{
			return position.Equals(other.position)
			    && color.Equals(other.color)
			    && normal.Equals(other.normal);
		}

		/// <inheritdoc />
		public override bool Equals(object obj)
		{
			return obj is VertexRecord other && Equals(other);
		}


		/// <inheritdoc />
		public override int GetHashCode()
		{
			unchecked
			{
				int hashCode = position.GetHashCode();
				hashCode = (hashCode * 397) ^ color.GetHashCode();
				hashCode = (hashCode * 397) ^ normal.GetHashCode();
				return hashCode;
			}
		}
	}
}