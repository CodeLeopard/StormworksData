﻿// Copyright 2024 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;

namespace BinaryDataModel.DataTypes
{
	/// <summary>
	/// Quad from 16 bit indices.
	/// </summary>
	public struct Quad16
	{
		/// <summary>
		/// Corner A.
		/// </summary>
		public UInt16 a;

		/// <summary>
		/// Corner B.
		/// </summary>
		public UInt16 b;

		/// <summary>
		/// Corner C.
		/// </summary>
		public UInt16 c;

		/// <summary>
		/// Corner D.
		/// </summary>
		public UInt16 d;


		/// <summary>
		/// Create from components.
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <param name="c"></param>
		/// <param name="d"></param>
		public Quad16(UInt16 a, UInt16 b, UInt16 c, UInt16 d)
		{
			this.a = a;
			this.b = b;
			this.c = c;
			this.d = d;
		}


		/// <summary>
		/// Create from components.
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <param name="c"></param>
		/// <param name="d"></param>
		public Quad16(int a, int b, int c, int d)
			: this((UInt16)a, (UInt16)b, (UInt16)c, (UInt16)d)
		{ }

		/// <inheritdoc/>
		public override string ToString()
		{
			return $"{nameof(Quad16)}: {a} {b} {c} {d}";
		}
	}
}
