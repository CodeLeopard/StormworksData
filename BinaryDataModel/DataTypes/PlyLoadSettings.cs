﻿// Copyright 2024 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using BinaryDataModel.DataTypes.Enums;

namespace BinaryDataModel.DataTypes
{
	/// <summary>
	/// Specifies what data is to be found in a ply file.
	/// </summary>
	public class PlyLoadSettings
	{
		/// <summary>
		/// Position data
		/// </summary>
		public PrescenseSpecifier position = PrescenseSpecifier.Required;
		/// <summary>
		/// Normal vector data.
		/// </summary>
		public PrescenseSpecifier normal;
		/// <summary>
		/// Color data
		/// </summary>
		public PrescenseSpecifier color;

		/// <summary>
		/// Color alpha channel.
		/// </summary>
		public PrescenseSpecifier colorAlpha = PrescenseSpecifier.Optional;

		/// <summary>
		/// Texture coordinate data.
		/// </summary>
		public PrescenseSpecifier uv;

		/// <summary>
		/// Triangles (faces with exactly 3 corners).
		/// </summary>
		public PrescenseSpecifier triangles;
		/// <summary>
		/// Quads (faces with exactly 4 corners).
		/// </summary>
		public PrescenseSpecifier quads;
	}
}
