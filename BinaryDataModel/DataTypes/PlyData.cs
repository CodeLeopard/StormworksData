﻿// Copyright 2024 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using BinaryDataModel.DataTypes.Enums;
using OpenToolkit.Mathematics;
using Shared;
using System.Collections.Generic;

namespace BinaryDataModel.DataTypes
{
	/// <summary>
	/// Represents the data stored in a Ply file
	/// (Or the subset that we can read anyway).
	/// </summary>
	public class PlyData
	{
		public IEnumerable<Vector3>? positions;
		public IEnumerable<Vector3>? normals;
		public IEnumerable<Color4>? colors;
		public IEnumerable<Vector2>? uvs;

		public IEnumerable<Triangle32>? triangles;
		public IEnumerable<Quad16>? quads;

		public Bounds3? triangleBounds;
		public Bounds3? quadBounds;
		public Bounds3? allBounds;

		public Shader? shader;

		public string? original_mesh_name;

		public int? submesh_index;
		/// <summary>
		/// The total count of submeshes in the source data (loaded from binary format).
		/// </summary>
		public int? submesh_count;

		public string? submesh_name;

		public PlyOriginalDataKind? originalDataKind;

		public PlyData(
			  PlyOriginalDataKind originalDataKind
			, IEnumerable<Vector3>? vertices
			, IEnumerable<Vector3>? normals
			, IEnumerable<Color4>? colors
			, IEnumerable<Vector2>? uvs
			, IEnumerable<Triangle32>? triangleIndices
			, IEnumerable<Quad16>? quadIndices
			, Bounds3 allBounds
			, Bounds3 triangleBounds
			, Bounds3 quadBounds
			, Shader? shader = null
			)
		{
			this.positions = vertices;
			this.normals = normals;
			this.colors = colors;
			this.uvs = uvs;

			this.triangles = triangleIndices;
			this.quads = quadIndices;
			
			this.triangleBounds = triangleBounds;
			this.quadBounds = quadBounds;
			this.allBounds = allBounds;
			this.shader = shader;
		}

		/// <summary>
		/// When saving to Ply the <see cref="Bounds3"/> properties are not used so can be omitted.
		/// </summary>
		/// <param name="vertices"></param>
		/// <param name="normals"></param>
		/// <param name="colors"></param>
		/// <param name="uvs"></param>
		/// <param name="triangleIndices"></param>
		/// <param name="quadIndices"></param>
		public PlyData(
			  PlyOriginalDataKind originalDataKind
			, IEnumerable<Vector3>? vertices
			, IEnumerable<Vector3>? normals
			, IEnumerable<Color4>? colors
			, IEnumerable<Vector2>? uvs
			, IEnumerable<Triangle32>? triangleIndices
			, IEnumerable<Quad16>? quadIndices
			, Shader? shader = null
			)
		{
			this.positions = vertices;
			this.normals = normals;
			this.colors = colors;
			this.uvs = uvs;

			this.triangles = triangleIndices;
			this.quads = quadIndices;
		}
	}
}
