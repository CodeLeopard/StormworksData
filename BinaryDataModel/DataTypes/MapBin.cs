﻿// Copyright 2024 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using BinaryDataModel.DataTypes.Enums;
using OpenToolkit.Mathematics;
using Shared.Serialization.DataTypes;
using System;
using System.Collections.Generic;

namespace BinaryDataModel.DataTypes
{
	/// <summary>
	/// Represents the *_map_geometry.bin file.
	/// It contains the data shown the map, both in the UI and on in-game monitors through Lua.
	/// </summary>
	public class MapBin
	{
		/// <summary>
		/// Metadata: file this data was loaded from. May not be set if the data was generated instead.
		/// </summary>
		public string fileName;

		/// <summary>
		/// The number of <see cref="VertexLayer"/> in <see cref="vertexLayers"/>.
		/// </summary>
		public const int vertexLayerCount = 11;
		
		/// <summary>
		/// The vertexLayers contain meshes of specific colors (as defined by the layer index see <see cref="VertexLayerKind"/>).
		/// Layers are stacked, with the lowest index appearing on top.
		/// See <see cref="colorLayerMap"/> for what layer index corresponds to what color..
		/// </summary>
		public VertexLayer[] vertexLayers = new VertexLayer[vertexLayerCount];

		/// <summary>
		/// The number of <see cref="TextureLayer"/> in <see cref="textureLayers"/>.
		/// </summary>
		public const int textureLayerCount = 10;
		/// <summary>
		/// The <see cref="textureLayers"/> contain meshes of quads with uv data and represent lines.
		/// There are two textures, alternating between <see cref="TextureLayerKind.SolidLine"/> (even index) and <see cref="TextureLayerKind.DashedLine"/> (odd index).
		/// </summary>
		public TextureLayer[] textureLayers = new TextureLayer[textureLayerCount];

		/*
		Layout:

		VertexLayer
			Int16 vertex_count
				float x,y,z
			Int16 face_count
				Int16 ->vertex, ->vertex, ->vertex

		TextureLayer
			Int16 vertex_count
				float x,y,z
				float u,v

		Workflow
			Layers are identified by color

		*/

		/// <summary>
		/// Create a new <see cref="MapBin"/> and fill <see cref="vertexLayers"/> and <see cref="textureLayers"/> filled and ready to be used.
		/// </summary>
		public MapBin()
		{
			for(int i = 0; i < vertexLayers.Length; i++)
			{
				vertexLayers[i] = new VertexLayer(i);
			}
			for (int i = 0; i < textureLayers.Length; i++)
			{
				textureLayers[i] = new TextureLayer(i);
			}
		}

		/// <summary>
		/// Mapping from <see cref="VertexLayer"/> index to <see cref="Color4"/>.
		/// </summary>
		public static Color4[] colorLayerMap = new Color4[vertexLayerCount]
		{
			VectorSer.FromRGB(0xd0d0c5), // Light gray
			VectorSer.FromRGB(0xa4b874), // Grass
			VectorSer.FromRGB(0xe3d08c), // sand
			VectorSer.FromRGB(0x51b7d2), // blue
			VectorSer.FromRGB(0xffffff), // pure white
			VectorSer.FromRGB(0x8b6d5b), // red rock
			VectorSer.FromRGB(0x583e2c), // brown (dirt)
			VectorSer.FromRGB(0x52b8d1), // blue (again)
			VectorSer.FromRGB(0x47a3b8), // blue 1st darker shade
			VectorSer.FromRGB(0x3d8d9f), // blue 2nd darker shade
			VectorSer.FromRGB(0x327985), // blue 3rd darker shade
		};

		/// <summary>
		/// Get the <see cref="Color4"/> for the given <paramref name="layerIndex"/>.
		/// Valid values range from 0 to <see cref="vertexLayerCount"/> (exclusive).
		/// </summary>
		/// <param name="layerIndex"></param>
		/// <returns></returns>
		public static Color4 VertexLayerColor(int layerIndex)
		{
			ThrowIfVertexLayerOutOfRange(layerIndex);
			return colorLayerMap[layerIndex];
		}

		/// <summary>
		/// Get the <see cref="Color4"/> for the given <paramref name="kind"/>.
		/// </summary>
		/// <param name="kind"></param>
		/// <returns></returns>
		public static Color4 VertexLayerColor(VertexLayerKind kind)
		{
			return VertexLayerColor((int)kind);
		}


		/// <summary>
		/// Throw an <see cref="ArgumentOutOfRangeException"/> when the <paramref name="layerIndex"/> is out of range.
		/// </summary>
		/// <param name="layerIndex"></param>
		/// <exception cref="ArgumentOutOfRangeException"></exception>
		public static void ThrowIfVertexLayerOutOfRange(int layerIndex)
		{
			if (layerIndex < 0 || layerIndex >= vertexLayerCount)
				throw new ArgumentOutOfRangeException(nameof(layerIndex), $"Out of range, accepted values: 0 >= {nameof(layerIndex)} < {vertexLayerCount}.");
		}

		/// <summary>
		/// Throw an <see cref="ArgumentOutOfRangeException"/> when the <paramref name="layerIndex"/> is out of range.
		/// </summary>
		/// <param name="layerIndex"></param>
		/// <exception cref="ArgumentOutOfRangeException"></exception>
		public static void ThrowIfTextureLayerOutOfRange(int layerIndex)
		{
			if (layerIndex < 0 || layerIndex >= textureLayerCount)
				throw new ArgumentOutOfRangeException(nameof(layerIndex), $"Out of range, accepted values: 0 >= {nameof(layerIndex)} < {textureLayerCount}.");
		}

		/// <summary>
		/// Get the <see cref="TextureLayerKind"/> for the given <paramref name="layerIndex"/>.
		/// Valid values range from 0 to <see cref="textureLayerCount"/> (exclusive).
		/// </summary>
		/// <param name="layerIndex"></param>
		/// <returns></returns>
		public static TextureLayerKind LayerKind(int layerIndex)
		{
			ThrowIfTextureLayerOutOfRange(layerIndex);
			return (TextureLayerKind) (layerIndex % 2);
		}

		/// <summary>
		/// Data for a vertex layer.
		/// See: <see cref="Layer"/> for the meaning of the <see cref="VertexLayer"/>.
		/// </summary>
		public class VertexLayer
		{
			/// <summary>
			/// Index of the layer.
			/// </summary>
			public readonly int LayerIndex;
			/// <summary>
			/// Kind of the layer.
			/// </summary>
			public readonly VertexLayerKind Layer;
			/// <summary>
			/// Vertices.
			/// </summary>
			public readonly List<Vector3> Vertices = new List<Vector3>();
			/// <summary>
			/// Triangles.
			/// </summary>
			public readonly List<Triangle16> Triangles = new List<Triangle16>();


			internal VertexLayer(int layer_index)
			{
				LayerIndex = layer_index;
				Layer = (VertexLayerKind)layer_index;
			}
		}

		/// <summary>
		/// Data for a texture layer.
		/// See: <see cref="Layer"/> for the meaning of the <see cref="TextureLayer"/>.
		/// </summary>
		public class TextureLayer
		{
			/// <summary>
			/// Index of the layer.
			/// </summary>
			public readonly int LayerIndex;
			/// <summary>
			/// Kind of the layer.
			/// </summary>
			public readonly TextureLayerKind Layer;
			/// <summary>
			/// Vertices. Each consecutive set of 4 vertices makes a quad.
			/// </summary>
			public readonly List<Vector3UV> Vertices = new List<Vector3UV>();

			internal TextureLayer(int layer_index)
			{
				LayerIndex = layer_index;
				Layer = (TextureLayerKind)(layer_index % 2);
			}
		}
	}
}

