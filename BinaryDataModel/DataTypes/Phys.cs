﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Linq;

using OpenToolkit.Mathematics;

namespace BinaryDataModel.DataTypes
{
	/// <summary>
	/// Holds the data in the Stormworks .phys file format.
	/// </summary>
	public class Phys
	{
		/// <summary>
		/// The filePath from where this <see cref="Phys"/> was loaded.
		/// </summary>
		public string fileName = "Not defined";

		/// <summary>
		/// unknown. values: 2
		/// </summary>
		/// <remarks>Position: header 0</remarks>
		public UInt16 header0 = 2;

		/// <summary>
		/// The amount of <see cref="PhySubMesh"/>.
		/// </summary>
		/// <remarks>Position: header 1, type <see cref="UInt16"/>.</remarks>
		public int subMeshCount => subMeshes.Count;

		/// <summary>
		/// The <see cref="PhySubMesh"/>es contained in this <see cref="Phys"/>ics mesh.
		/// </summary>
		public readonly List<PhySubMesh> subMeshes;


		public int TotalVertices => subMeshes.Sum(s => s.vertices.Count);

		public int TotalIndices => subMeshes.Sum(s => s.indices.Count);

		public uint SizeEstimate => ComputeSizeEstimate();


		public Phys()
		{
			subMeshes = new List<PhySubMesh>();
		}

		public Phys(PhySubMesh subMesh)
		{
			subMeshes = new List<PhySubMesh>(1);
			subMeshes.Add(subMesh);
		}

		public Phys(IEnumerable<PhySubMesh> meshes)
		{
			subMeshes = meshes.ToList();
		}

		public Phys(List<PhySubMesh> meshes, bool useListInstance = false)
		{
			if (useListInstance)
			{
				subMeshes = meshes;
			}
			else
			{
				subMeshes = meshes.ToList();
			}
		}

		private uint ComputeSizeEstimate()
		{
			uint size = 0;
			size += sizeof(UInt16); // header0, subMeshCount
			foreach (var phySubMesh in subMeshes)
			{
				size += phySubMesh.SizeEstimate;
			}

			return size;
		}

		/// <summary>
		/// Convert all <see cref="PhySubMesh"/> in this <see cref="Phys"/> to IndexedTriangles format.
		/// </summary>
		public void ConvertToIndexedTriangles()
		{
			foreach (var subMesh in subMeshes)
			{
				if (subMesh.hasIndices)
					continue;

				subMesh.ConvertToIndexedTriangles();
			}
		}


		/// <summary>
		/// Convert all <see cref="PhySubMesh"/> in this <see cref="Phys"/> to Ordered Vertices format.
		/// </summary>
		/// <param name="strict">if false, skip any <see cref="PhySubMesh"/> that already has no indices. otherwise those would throw.</param>
		public void ConvertToOrderedVertices(bool strict = true)
		{
			foreach (var subMesh in subMeshes)
			{
				if (!strict && !subMesh.hasIndices) continue;

				subMesh.ConvertToOrderedVertices();
			}
		}



		public Phys Clone()
		{
			var result = new Phys();
			result.header0 = header0;

			foreach (PhySubMesh phySubMesh in subMeshes)
			{
				result.subMeshes.Add(phySubMesh.Clone());
			}
			return result;
		}


		public override string ToString()
		{
			return $"{{{nameof(Phys)} file:'{fileName}', S:{subMeshCount}}}";
		}
	}


	public class PhySubMesh
	{
		/// <summary>
		/// The amount of vertices in this <see cref="PhySubMesh"/>.
		/// </summary>
		/// <remarks>Position: header 0, type <see cref="UInt16"/>.</remarks>
		public UInt16 vertexCount => (UInt16) vertices.Count;

		/// <summary>
		/// The amount of indices in this <see cref="PhySubMesh"/>.
		/// </summary>
		/// <remarks>Position: after vertexBuffer, type <see cref="UInt16"/>.</remarks>
		public UInt16 indexCount => (UInt16)indices.Count;


		/// <summary>
		/// Are triangles defined explicitly with indices.
		/// </summary>
		public bool hasIndices => indices.Count >= 3;


		public readonly List<Vector3> vertices;
		public readonly List<UInt32> indices;
		// todo: this doesn't make sense, the dataType is UInt32, but the count of vertices can only hold up to UInt16.MaxValue items.
		// todo: could the high bytes have special meaning?
		// todo: perhaps header0 specifies the format?

		public uint SizeEstimate
		{
			get
			{
				uint size = 0;
				size += sizeof(UInt16) * 2; // vertexCount, indexCount

				size += vertexCount * VertexRecord.SizeEstimate;
				size += (uint) indexCount  * sizeof(UInt32);
				return size;
			}
		}

		public PhySubMesh()
		{
			vertices = new List<Vector3>();
			indices = new List<UInt32>(0);
		}

		public PhySubMesh(IEnumerable<Vector3> vertices)
		{
			this.vertices = vertices?.ToList() ?? throw new ArgumentNullException(nameof(vertices));
			this.indices = new List<UInt32>(0);
		}

		public PhySubMesh(IEnumerable<Vector3> vertices, IEnumerable<UInt32> indices)
		{
			this.vertices = vertices?.ToList() ?? throw new ArgumentNullException(nameof(vertices));
			this.indices  = indices?.ToList()  ?? throw new ArgumentNullException(nameof(indices));
		}

		public PhySubMesh(List<Vector3> vertices, bool useListInstance = false)
		{
			if (useListInstance)
			{
				this.vertices = vertices;
			}
			else
			{
				this.vertices = vertices.ToList();
			}

			this.indices = new List<UInt32>(0);
		}

		public PhySubMesh(List<Vector3> vertices, List<UInt32> indices, bool useListInstance = false)
		{
			if (useListInstance)
			{
				this.vertices = vertices;
				this.indices = indices;
			}
			else
			{
				this.vertices = vertices.ToList();
				this.indices = indices.ToList();
			}
		}

		/// <summary>
		/// Convert the mesh to use indices to define the triangles. This typically means a reduction in memory usage.
		/// Note that this process could use up to 4x the current memory temporarily.
		/// </summary>
		public void ConvertToIndexedTriangles()
		{
			if (hasIndices)
			{
				throw new InvalidOperationException($"{nameof(PhySubMesh)} already has indices.");
			}

			if (vertexCount % 3 != 0)
			{
				throw new InvalidOperationException($"Number of vertices must be divisible by 3.");
			}

			var newVertexToIndexMap = new Dictionary<Vector3, UInt32>();

			var newVertices = new List<Vector3>(vertices.Count * 2 / 3); // Guesstimate of the needed space.
			var newIndices = new List<UInt32>(vertices.Count); // Guesstimate of the needed space.

			foreach (Vector3 vertex in vertices)
			{
				if (newVertexToIndexMap.TryGetValue(vertex, out UInt32 index))
				{
					// Vertex already present in buffer, just put index to it.
					newIndices.Add(index);
				}
				else
				{
					// Vertex not in new buffer, get the new index, which is the current count.
					index = (UInt32) newVertices.Count;

					newVertexToIndexMap.Add(vertex, index);
					newVertices.Add(vertex);
					newIndices.Add(index);
				}
			}

			vertices.Clear();
			vertices.Capacity = newVertices.Count;
			vertices.AddRange(newVertices);

			indices.Clear();
			indices.Capacity = newIndices.Count;
			indices.AddRange(newIndices);
		}

		/// <summary>
		/// Convert the mesh to not use indices at all. This will typically increase memory usage.
		/// </summary>
		public void ConvertToOrderedVertices()
		{
			if (! hasIndices) throw new InvalidOperationException($"{nameof(PhySubMesh)} does not have indices.");
			var newVertices = new List<Vector3>(indices.Count);
			for(int i = 0; i < indices.Count; i++)
			{
				int vIndex = (int)indices[i];
				newVertices.Add(vertices[vIndex]);
			}

			vertices.Clear();
			vertices.Capacity = newVertices.Count;
			vertices.AddRange(newVertices);

			indices.Clear();
			indices.Capacity = 0;
		}

		public PhySubMesh Clone()
		{
			if (hasIndices)
			{
				return new PhySubMesh(vertices, indices, false);
			}
			else
			{
				return new PhySubMesh(vertices, false);
			}
		}

		public override string ToString()
		{
			return $"{{{nameof(PhySubMesh)} V:{vertexCount}, I:{indexCount}}}";
		}
	}
}
