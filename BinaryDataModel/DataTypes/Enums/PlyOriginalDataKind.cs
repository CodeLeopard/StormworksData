﻿// Copyright 2024 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



namespace BinaryDataModel.DataTypes.Enums
{
	public enum PlyOriginalDataKind
	{
		/// <summary>
		/// The data came from a .ply file, or is otherwise not specific to a Stormworks data format.
		/// </summary>
		Ply,
		/// <summary>
		/// The data came from a .mesh
		/// </summary>
		Mesh,
		/// <summary>
		/// The data came from a .ply
		/// </summary>
		Phys,
		/// <summary>
		/// The data came from map_geometry.bin
		/// </summary>
		MapGeometryBin,
	}
}
