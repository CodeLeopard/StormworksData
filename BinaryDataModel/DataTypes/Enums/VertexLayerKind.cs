﻿// Copyright 2024 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



namespace BinaryDataModel.DataTypes.Enums
{
	/// <summary>
	/// Kind of VertexLayer
	/// </summary>
	public enum VertexLayerKind
	{
		/// <summary>
		/// Light gray
		/// </summary>
		Gray,
		/// <summary>
		/// Green (light/pale)
		/// </summary>
		Grass,
		/// <summary>
		/// yellow
		/// </summary>
		Sand,
		/// <summary>
		/// Blue, same as <see cref="Sea_Shade0"/>.
		/// </summary>
		Water,
		/// <summary>
		/// Pure white
		/// </summary>
		White,
		/// <summary>
		/// Red-ish rock.
		/// </summary>
		Rock,
		/// <summary>
		/// Brown
		/// </summary>
		Dirt,
		/// <summary>
		/// Blue, same as <see cref="Water"/>.
		/// </summary>
		Sea_Shade0,
		/// <summary>
		/// 1st Darker shade of blue.
		/// </summary>
		Sea_Shade1,
		/// <summary>
		/// n2d Darker shade of blue.
		/// </summary>
		Sea_Shade2,
		/// <summary>
		/// 3rd Darker shade of blue.
		/// </summary>
		Sea_shade3,
	}
}
