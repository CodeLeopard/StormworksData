﻿// Copyright 2024 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;

namespace BinaryDataModel.DataTypes.Enums
{
	/// <summary>
	/// The valid values in the field <see cref="SubMesh.shaderId"/>.
	/// </summary>
	public enum Shader : UInt16
	{
		/// <summary>
		/// Standard Opaque shader.
		/// </summary>
		Opaque = 0,
		/// <summary>
		/// Transparency shader.
		/// </summary>
		Transparent = 1,
		/// <summary>
		/// Emissive/unlit shader.
		/// </summary>
		Emissive = 2,
		/// <summary>
		/// Used for lava flows.
		/// </summary>
		Lava = 3,
	}
}
