﻿// Copyright 2024 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using OpenToolkit.Mathematics;

namespace BinaryDataModel.DataTypes.Enums
{
	/// <summary>
	/// Specifies the possible vertex properties in a ply file.
	/// </summary>
	public enum PlyVertexProperty : sbyte
	{
		/// <summary>
		/// Meta: Value not present.
		/// </summary>
		NotPresent = -1,

		/// <summary>
		/// Position <see cref="Vector3"/> compopnent X.
		/// </summary>
		PositionX,
		/// <summary>
		/// Position <see cref="Vector3"/> component Y.
		/// </summary>
		PositionY,
		/// <summary>
		/// Position <see cref="Vector3"/> component Z.
		/// </summary>
		PositionZ,

		/// <summary>
		/// Normal <see cref="Vector3"/> component X.
		/// </summary>
		NormalX,
		/// <summary>
		/// Normal <see cref="Vector3"/> component Y.
		/// </summary>
		NormalY,
		/// <summary>
		/// Normal <see cref="Vector3"/> component Z.
		/// </summary>
		NormalZ,

		/// <summary>
		/// Color <see cref="Color4"/> component Red.
		/// </summary>
		ColorR,
		/// <summary>
		/// Color <see cref="Color4"/> component Green.
		/// </summary>
		ColorG,
		/// <summary>
		/// Color <see cref="Color4"/> component Blue.
		/// </summary>
		ColorB,
		/// <summary>
		/// Color <see cref="Color4"/> component Alpha.
		/// </summary>
		ColorA,

		/// <summary>
		/// Texture/UV coordinate <see cref="Vector2"/> component X/U.
		/// </summary>
		TextureU,
		/// <summary>
		/// Texture/UV coordinate <see cref="Vector2"/> component Y/V.
		/// </summary>
		TextureV,

		/// <summary>
		/// Meta: the last item.
		/// </summary>
		LAST = TextureV,
		/// <summary>
		/// Meta: the number of real items (excluding meta items) in the enum.
		/// </summary>
		NUMBER_OF_ITEMS = LAST + 1,
	}
}
