﻿// Copyright 2022-2024 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Linq;

namespace BinaryDataModel.DataTypes
{
	/// <summary>
	/// Index data for a triangle using <see cref="UInt16"/>.
	/// </summary>
	public struct Triangle16
	{
		private const byte bytesPerIndex = sizeof(Int16);
		private const byte indicesPerTriangle = 3;

		/// <summary>
		/// Number of bytes for each triangle.
		/// </summary>
		public const byte bytesPerTriangle = indicesPerTriangle * bytesPerIndex;

		/// <summary>
		/// Corner A.
		/// </summary>
		public UInt16 a;

		/// <summary>
		/// Corner B.
		/// </summary>
		public UInt16 b;

		/// <summary>
		/// Corner C.
		/// </summary>
		public UInt16 c;

		/// <summary>
		/// Create from bytes.
		/// </summary>
		/// <param name="enumerable"></param>
		public Triangle16(IEnumerable<byte> enumerable) : this(enumerable.ToArray()) { }

		/// <summary>
		/// Create from bytes.
		/// </summary>
		/// <param name="bytes"></param>
		/// <exception cref="ArgumentOutOfRangeException"></exception>
		public Triangle16(byte[] bytes)
		{
			if (bytes.Length != bytesPerTriangle) throw new ArgumentOutOfRangeException(nameof(bytes), "Must be of length " + bytesPerTriangle);

			a = BitConverter.ToUInt16(bytes, 0 * bytesPerIndex);
			b = BitConverter.ToUInt16(bytes, 1 * bytesPerIndex);
			c = BitConverter.ToUInt16(bytes, 2 * bytesPerIndex);
		}

		/// <summary>
		/// Create from components.
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <param name="c"></param>
		public Triangle16(UInt16 a, UInt16 b, UInt16 c)
		{
			this.a = a;
			this.b = b;
			this.c = c;
		}

		/// <summary>
		/// Create from components.
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <param name="c"></param>
		public Triangle16(int a, int b, int c)
			: this((UInt16)a, (UInt16)b, (UInt16)c)
		{ }

		/// <inheritdoc/>
		public override string ToString()
		{
			return $"{nameof(Triangle16)}: {a} {b} {c}";
		}
	}
}
