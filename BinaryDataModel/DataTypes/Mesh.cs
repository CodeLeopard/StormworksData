﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using BinaryDataModel.DataTypes.Enums;
using OpenToolkit.Mathematics;

using Shared;

namespace BinaryDataModel.DataTypes
{
	/// <summary>
	/// Holds the data found in the Stormworks .mesh file format.
	/// </summary>
	public class Mesh
	{
		/// <summary>
		/// The filePath from where this <see cref="Mesh"/> was loaded.
		/// </summary>
		public string fileName = "Not defined";

		#region Header Fields

		/// <summary>
		/// Unknown, values: 7
		/// </summary>
		/// <remarks>Position: header 0</remarks>
		public UInt16 header0 = 7;

		/// <summary>
		/// Unknown, values: 1
		/// </summary>
		/// <remarks>Position: header 1</remarks>
		public UInt16 header1 = 1;

		/// <summary>
		/// Number of <see cref="VertexRecord"/>s in the mesh.
		/// </summary>
		/// <remarks>
		/// The real value is actually <see cref="UInt16"/> and the
		/// Maximum vertexCount for saving is <see cref="UInt16.MaxValue"/>.
		/// Larger meshes are allowed to exist in memory for rendering purposes.
		///
		/// Position: header 2, type: <see cref="UInt32"/>.
		/// </remarks>
		public UInt32 vertexCount => (UInt32) vertices.Count;

		/// <summary>
		/// Unknown, values: 19
		/// </summary>
		/// <remarks>Position: header 3</remarks>
		public UInt16 header3 = 19;

		/// <summary>
		/// Unknown, values: 0
		/// </summary>
		/// <remarks>Position: header 4</remarks>
		public UInt16 header4 = 0;

		/// <summary>
		/// Number of indices in the Mesh. 3 indices make a triangle.
		/// </summary>
		/// <remarks>Position: After vertex buffer, type: <see cref="UInt32"/>.</remarks>
		public UInt32 indexCount => (UInt32)indices.Count;

		#endregion Header Fields



		public readonly List<VertexRecord> vertices;

		// We use UInt32 instead of the actual UInt16,
		// because modern CPUs don't implement sub 32bit math anyway,
		// and when merging meshes we can end up with more than UInt16.MaxValue vertices causing the numbers to overflow.
		// Note that such merged meshes can't be saved.
		/// <summary>
		/// The triangle indices of the mesh.
		/// Note that the real data structure uses <see cref="UInt16"/> instead, but for our purposes we use <see cref="UInt32"/>.
		/// </summary>
		public readonly List<UInt32> indices;

		/// <summary>
		/// The <see cref="SubMesh"/>es in this <see cref="Mesh"/>.
		/// SubMeshes define (amongst other things) what shader to use.
		/// </summary>
		public readonly List<SubMesh> subMeshes;

		/// <summary>
		/// The number of SubMeshes in the Mesh.
		/// </summary>
		/// <remarks>Position: after indexBuffer, type: <see cref="UInt16"/>.</remarks>
		public int SubMeshCount => subMeshes.Count;

		/// <summary>
		/// Is there a single SubMesh.
		/// </summary>
		public bool SingleSubMesh => subMeshes.Count == 1;

		/// <summary>
		/// Can the <see cref="Mesh"/> be saved to the Stormworks .mesh format.
		/// </summary>
		public bool CanBeSaved => vertices.Count <= UInt16.MaxValue;

		/// <summary>
		/// Create a new Empty <see cref="Mesh"/> instance.
		/// Note that you have to add a <see cref="SubMesh"/> in order to use it.
		/// </summary>
		public Mesh()
		{
			vertices  = new List<VertexRecord>();
			indices   = new List<UInt32>();
			subMeshes = new List<SubMesh>();
		}

		public Mesh(IEnumerable<VertexRecord> vertices, IEnumerable<UInt32> indices, IEnumerable<SubMesh> meshes)
		{
			this.vertices  = vertices?.ToList() ?? throw new ArgumentNullException(nameof(vertices));
			this.indices   = indices?.ToList()  ?? throw new ArgumentNullException(nameof(indices));
			this.subMeshes = meshes?.ToList()   ?? throw new ArgumentNullException(nameof(meshes));
		}

		public Mesh(List<VertexRecord> vertices, List<UInt32> indices, List<SubMesh> meshes, bool useListInstances = false)
		{
			if (useListInstances)
			{
				this.vertices  = vertices ?? throw new ArgumentNullException(nameof(vertices));
				this.indices   = indices  ?? throw new ArgumentNullException(nameof(indices));
				this.subMeshes = meshes   ?? throw new ArgumentNullException(nameof(meshes));
			}
			else
			{
				this.vertices  = vertices?.ToList() ?? throw new ArgumentNullException(nameof(vertices));
				this.indices   = indices?.ToList()  ?? throw new ArgumentNullException(nameof(indices));
				this.subMeshes = meshes?.ToList()   ?? throw new ArgumentNullException(nameof(meshes));
			}
		}

		private void Constructor_MakeSubMesh(Bounds3? maybeBounds)
		{
			Bounds3 bounds;
			if (maybeBounds == null)
			{
				bounds = new Bounds3(true);
				foreach (VertexRecord vertex in vertices)
				{
					bounds.Inflate(vertex.position);
				}
			}
			else
			{
				bounds = maybeBounds.Value;
			}

			var sm = new SubMesh(this, "id0", bounds);
			subMeshes.Add(sm);
		}

		public Mesh(IEnumerable<VertexRecord> vertices, IEnumerable<UInt32> indices, Bounds3? bounds = null)
		{
			this.vertices = vertices?.ToList() ?? throw new ArgumentNullException(nameof(vertices));
			this.indices = indices?.ToList()   ?? throw new ArgumentNullException(nameof(indices));

			subMeshes = new List<SubMesh>(1);
			Constructor_MakeSubMesh(bounds);
		}

		public Mesh(List<VertexRecord> vertices, List<UInt32> indices, Bounds3? bounds = null, bool useListInstances = false)
		{
			if (useListInstances)
			{
				this.vertices = vertices ?? throw new ArgumentNullException(nameof(vertices));
				this.indices = indices   ?? throw new ArgumentNullException(nameof(indices));
			}
			else
			{
				this.vertices = vertices?.ToList() ?? throw new ArgumentNullException(nameof(vertices));
				this.indices = indices?.ToList()   ?? throw new ArgumentNullException(nameof(indices));
			}

			subMeshes = new List<SubMesh>(1);
			Constructor_MakeSubMesh(bounds);
		}

		/// <summary>
		/// The approximate size in bytes, assuming no padding.
		/// </summary>
		public uint SizeEstimate => ComputeSizeEstimate();

		private uint ComputeSizeEstimate()
		{
			uint size = 5 * sizeof(UInt16); // header fields

			size += VertexRecord.SizeEstimate * vertexCount;
			size += sizeof(UInt32); // indexCount field
			size += sizeof(UInt32)            * indexCount;

			foreach (SubMesh subMesh in subMeshes)
			{
				size += subMesh.SizeEstimate;
			}

			return size;
		}

		/// <summary>
		/// Get the bounds of the mesh by adding the bounds of the submeshes.
		/// </summary>
		/// <returns></returns>
		public Bounds3 Bounds()
		{
			if (subMeshes.Count == 0) return new Bounds3();

			var result = new Bounds3(true);
			foreach (SubMesh subMesh in subMeshes)
			{
				result.Inflate(subMesh.bounds.Min);
				result.Inflate(subMesh.bounds.Max);
			}

			return result;
		}

		/// <summary>
		/// Recompute the bounds of all submeshes.
		/// </summary>
		/// <returns></returns>
		public void RecomputeBounds()
		{
			foreach (SubMesh subMesh in subMeshes)
			{
				var bounds = new Bounds3(true);
				for (int i = (int) subMesh.indexBufferStart; i < subMesh.indexBufferStart + subMesh.indexBufferLength; i++)
				{
					var vertexIndex = (int) indices[i];
					bounds.Inflate(vertices[vertexIndex].position);
				}

				subMesh.bounds = bounds;
			}
		}

		/// <summary>
		/// Deduplicate identical VertexRecords.
		/// </summary>
		/// <remarks>
		/// This process is rather slow.
		/// </remarks>
		public void DeduplicateVertices()
		{
			// Maps from vertex instance to new index.
			var vertexMap = new Dictionary<VertexRecord, UInt32>();

			// Maps old index to new index.
			var indexMap = new Dictionary<UInt32, UInt32>();

			var oldVertices = vertices.ToArray();
			vertices.Clear();
			var newVertices = vertices;

			UInt32 uniqueCounter = 0;
			for (UInt32 i = 0; i < oldVertices.Length; i++)
			{
				var record = oldVertices[i];
				if (vertexMap.TryGetValue(record, out UInt32 newIndex))
				{
					indexMap.Add(i, newIndex);
				}
				else
				{
					vertexMap.Add(record, uniqueCounter++);
					newVertices.Add(record);
				}
			}

			for (int i = 0; i < indices.Count; i++)
			{
				var index = indices[i];
				if (indexMap.TryGetValue(index, out UInt32 newIndex))
				{
					indices[i] = newIndex;
				}
			}

#if DEBUG
			RecomputeBounds();
#endif
		}

		/// <inheritdoc />
		public override string ToString()
		{
			return $"{{{nameof(Mesh)} file:'{fileName}', V:{vertexCount}, I:{indexCount}, S:{SubMeshCount}}}";
		}

		public Mesh Clone()
		{
			var cloneSubMeshes = new List<SubMesh>(subMeshes.Count);
			cloneSubMeshes.AddRange(subMeshes.Select(s => s.Clone()));

			var c = new Mesh(vertices, indices, cloneSubMeshes);
			c.header0 = header0;
			c.header1 = header1;
			c.header3 = header3;
			c.header4 = header4;

			return c;
		}
	}

	public class SubMesh : IEquatable<SubMesh>
	{
		/// <summary>
		/// The starting index into the shared index buffer for this <see cref="SubMesh"/>.
		/// </summary>
		/// <remarks>Position: field 0.</remarks>
		public UInt32 indexBufferStart;

		/// <summary>
		/// The amount of indices in the shared index buffer to uses for this <see cref="SubMesh"/>.
		/// </summary>
		/// <remarks>Position: field 1.</remarks>
		public UInt32 indexBufferLength;

		/// <summary>
		/// Unknown, values: 0
		/// </summary>
		/// <remarks>Position:field 2.</remarks>
		public UInt16 header2 = 0;

		/// <summary>
		/// Shader to be used for this SubMesh.
		/// </summary>
		/// <remarks>Position: field 3</remarks>
		public Shader shaderId = Shader.Opaque;

		/// <summary>
		/// The bounds, this is used for thrustrum culling.
		/// </summary>
		/// <remarks>Position: fields 4 and 5</remarks>
		public Bounds3 bounds;

		/// <summary>
		/// Unknown, values: 0
		/// </summary>
		/// <remarks>Position: field 6</remarks>
		public UInt16 header6 = 0;

		/// <summary>
		/// Length, in bytes, of the string <see cref="name"/>.
		/// </summary>
		/// <remarks>Position: field 7.</remarks>
		public UInt16 nameByteLength
		{
			get
			{
				if (null == name)
					return 0;
				else
					return (UInt16) name.Length;
			}
		}

		/// <summary>
		/// Name of the SubMesh
		/// </summary>
		/// <remarks>Position: field 7.</remarks>
		public string name;

		/// <summary>
		/// Unknown, seems to be unused. Speculated purpose is SubMesh origin, or scale.
		/// values: (1,1,1)
		/// </summary>
		/// <remarks>Position: field 8.</remarks>
		public Vector3 header8 = Vector3.One;


		/// <summary>
		/// Estimate of the fixed size part of this structure, assuming no padding.
		/// The <see cref="string"/> <see cref="name"/> is not included.
		/// </summary>
		public const uint FixedSizeEstimate = sizeof(UInt16) * 4 + sizeof(UInt32) * 2 + sizeof(float) * 3 * 3;

		public uint SizeEstimate => FixedSizeEstimate + (uint) name.Length;


		protected SubMesh(UInt32 indexBufferStart, UInt32 indexBufferLength)
		{
			if (indexBufferLength == 0)
				throw new ArgumentOutOfRangeException(nameof(indexBufferLength), "Length cannot be 0");


			this.indexBufferStart = indexBufferStart;
			this.indexBufferLength = indexBufferLength;
		}

		public SubMesh(UInt32 indexBufferStart, UInt32 indexBufferLength, UInt16 header2, UInt16 shaderID,
		               Bounds3 bounds,
		               UInt16 header6,
		               string name,
			Vector3 header8) : this(indexBufferStart, indexBufferLength)
		{
			this.header2 = header2;
			this.shaderId = (Shader) shaderID;
			this.bounds = bounds;
			this.header6 = header6;
			this.name = name ?? throw new ArgumentNullException(nameof(name));
			this.header8 = header8;
		}

		/// <summary>
		/// Create a <see cref="SubMesh"/> to encompass all the data in <see cref="Mesh"/> <paramref name="parent"/>.
		/// </summary>
		/// <param name="parent"></param>
		/// <param name="name"></param>
		/// <param name="bounds"></param>
		public SubMesh(Mesh parent, string name, Bounds3 bounds) : this(0, parent.indexCount)
		{
			this.bounds = bounds;
			this.name = name;
		}

		public SubMesh(string name, UInt32 indexBufferStart, UInt32 indexBufferLength, Bounds3 bounds, Shader shaderId = Shader.Opaque)
			: this(indexBufferStart, indexBufferLength)
		{
			this.shaderId = shaderId;
			this.bounds = bounds;
			this.name = name;
		}


		/// <inheritdoc />
		public override string ToString()
		{
			return $"{{{nameof(SubMesh)} BS:{indexBufferStart}, BL:{indexBufferLength}, S:{shaderId}}}";
		}

		public SubMesh Clone()
		{
			return (SubMesh)MemberwiseClone();
		}

		/// <inheritdoc />
		public bool Equals(SubMesh other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return indexBufferStart  == other.indexBufferStart
			    && indexBufferLength == other.indexBufferLength
			    && header2           == other.header2
			    && shaderId          == other.shaderId
			    && bounds.Equals(other.bounds)
			    && header6 == other.header6
			    && name    == other.name
			    && header8.Equals(other.header8);
		}

		/// <inheritdoc />
		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != this.GetType()) return false;
			return Equals((SubMesh)obj);
		}

		/// <inheritdoc />
		public override int GetHashCode()
		{
			unchecked
			{
				int hashCode = (int)indexBufferStart;
				hashCode = (hashCode * 397) ^ (int)indexBufferLength;
				hashCode = (hashCode * 397) ^ header2.GetHashCode();
				hashCode = (hashCode * 397) ^ (int)shaderId;
				hashCode = (hashCode * 397) ^ bounds.GetHashCode();
				hashCode = (hashCode * 397) ^ header6.GetHashCode();
				hashCode = (hashCode * 397) ^ (name != null ? name.GetHashCode() : 0);
				hashCode = (hashCode * 397) ^ header8.GetHashCode();
				return hashCode;
			}
		}
	}
}
