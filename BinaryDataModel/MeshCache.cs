﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

using BinaryDataModel.Converters;
using BinaryDataModel.DataTypes;

using Shared;

namespace BinaryDataModel
{
	public static class MeshCache
	{
		private static readonly LazyCache<string, Mesh> mesh = new LazyCache<string, Mesh>(CreateMesh);
		private static readonly LazyCache<string, Phys> phys = new LazyCache<string, Phys>(CreatePhys);



		private static Mesh CreateMesh(string romPath)
		{
			var fullPath = Path.Combine(StormworksPaths.rom, romPath);

			Stopwatch timer = Stopwatch.StartNew();
			Mesh result = null;
			try
			{
				result = Binary.LoadMesh(fullPath);
			}
			catch (Exception e)
			{
				Console.WriteLine($"[{nameof(MeshCache)}] Error loading '{romPath}' after {timer.Elapsed.TotalSeconds}s on thread: #{Thread.CurrentThread.ManagedThreadId}'{Thread.CurrentThread.Name}'. Will return null for this path.\n{e}");
			}

			Console.WriteLine($"[{nameof(MeshCache)}] Loaded '{romPath}' after {timer.Elapsed.TotalSeconds}s on thread: #{Thread.CurrentThread.ManagedThreadId}'{Thread.CurrentThread.Name}' ...");

			// returning null is allowed.
			// the null is stored so it will not be tried again.
			return result;
		}

		private static Phys CreatePhys(string romPath)
		{
			var fullPath = Path.Combine(StormworksPaths.rom, romPath);

			Stopwatch timer = Stopwatch.StartNew();
			Phys result = null;
			try
			{
				result = Binary.LoadPhys(fullPath);
			}
			catch (Exception e)
			{
				Console.WriteLine($"[{nameof(MeshCache)}] Error loading '{romPath}' after {timer.Elapsed.TotalSeconds}s on thread: #{Thread.CurrentThread.ManagedThreadId}'{Thread.CurrentThread.Name}'. Will return null for this path.\n{e}");
			}
			Console.WriteLine($"[{nameof(MeshCache)}] Loaded '{romPath}' after {timer.Elapsed.TotalSeconds}s on thread: #{Thread.CurrentThread.ManagedThreadId}'{Thread.CurrentThread.Name}' ...");

			// returning null is allowed.
			// the null is stored so it will not be tried again.
			return result;
		}


		#region Public Interface

		/// <summary>
		/// Returns the <see cref="Mesh"/> at this path, or null if such mesh does not exist or loading failed.
		/// </summary>
		/// <param name="romPath"></param>
		/// <returns></returns>
		public static Mesh GetOrLoadMesh(string romPath)
		{
			return mesh.GetOrAdd(romPath);
		}

		/// <summary>
		/// Returns the <see cref="Phys"/> at this path, or null if such phys does not exist or loading failed.
		/// </summary>
		/// <param name="romPath"></param>
		/// <returns></returns>
		public static Phys GetOrLoadPhys(string romPath)
		{
			return phys.GetOrAdd(romPath);
		}

		/// <summary>
		/// Try to retrieve the <see cref="Lazy{T}"/> for this path (which could point to a null mesh).
		/// returns true if the <paramref name="result"/> has a value (which could be a <see cref="Lazy{T}"/> pointing to null)
		/// </summary>
		/// <param name="romPath"></param>
		/// <returns></returns>
		public static bool TryGetLazyMesh(string romPath, out Lazy<Mesh> result)
		{
			return mesh.TryGetLazy(romPath, out result);
		}

		/// <summary>
		/// Try to retrieve the <see cref="Lazy{T}"/> for this path (which could point to a null phys).
		/// returns true if the <paramref name="result"/> has a value (which could be a <see cref="Lazy{T}"/> pointing to null)
		/// </summary>
		/// <param name="romPath"></param>
		/// <returns></returns>
		public static bool TryGetLazyPhys(string romPath, out Lazy<Phys> result)
		{
			return phys.TryGetLazy(romPath, out result);
		}

		/// <summary>
		/// Add or update the mesh for the given path. Mesh may be null.
		/// </summary>
		/// <param name="romPath"></param>
		/// <param name="value">The value to insert, or null</param>
		public static void AddOrUpdate(string romPath, Mesh value)
		{
			mesh.AddOrUpdate(romPath, value);
		}

		/// <summary>
		/// Add or update the mesh for the given path. Mesh may be null.
		/// </summary>
		/// <param name="romPath"></param>
		/// <param name="value">The value to insert, or null</param>
		public static void AddOrUpdate(string romPath, Phys value)
		{
			phys.AddOrUpdate(romPath, value);
		}

		/// <inheritdoc cref="LazyCache{TKey,TValue}.TryAdd"/>
		public static bool TryAdd(string romPath, Mesh value)
		{
			return mesh.TryAdd(romPath, value);
		}

		/// <inheritdoc cref="LazyCache{TKey,TValue}.TryAdd"/>
		public static bool TryAdd(string romPath, Phys value)
		{
			return phys.TryAdd(romPath, value);
		}

		/// <inheritdoc cref="LazyCache{TKey,TValue}.Clear"/>
		public static void Clear()
		{
			mesh.Clear();
			phys.Clear();
		}

		#region Async

		public static async Task<Mesh> GetOrLoadMeshAsync(string key)
		{
			return await mesh.GetOrAddAsync(key);
		}

		public static async Task<Phys> GetOrLoadPhysAsync(string key)
		{
			return await phys.GetOrAddAsync(key);
		}

		#endregion Async

		#endregion Public Interface
	}
}
