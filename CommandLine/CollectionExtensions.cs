﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommandLine
{
	public static class CollectionExtensions
	{
		/// <summary>
		/// Attempts to remove and return the object at the beginning of the <see cref="System.Collections.Generic.Queue{T}"/>.
		/// </summary>
		/// <param name="queue">The instance of a Queue on which to operate.</param>
		/// <param name="result">
		/// When this method returns, if the operation was successful, <paramref name="result"/> contains the
		/// object removed. If no object was available to be removed, the value is unspecified.
		/// </param>
		/// <returns>true if an element was removed and returned from the beginning of the <see cref="System.Collections.Generic.Queue{T}"/>
		/// successfully; otherwise, false.</returns>
		public static bool TryDequeue<T>(this Queue<T> queue, out T result)
		{
			try
			{
				if (queue.Count != 0)
				{
					result = queue.Dequeue();
					return true;
				}
				else
				{
					result = default(T);
					return false;
				}
			}
			catch (InvalidOperationException)
			{
				// Catch InvalidOperationException when the queue has no items. In case of race conditions etc.
				result = default(T);
				return false;
			}
		}

		/// <summary>
		/// Attempts to add the specified key and value to the dictionary, but only if the key was not already present.
		/// </summary>
		/// <typeparam name="TKey">Type of the Key</typeparam>
		/// <typeparam name="TValue">Type of the Value</typeparam>
		/// <param name="instance">Dictionary instance to work with</param>
		/// <param name="key">Key to add</param>
		/// <param name="value">Value to add</param>
		/// <returns>true if added, false if existed already</returns>
		public static bool TryAdd<TKey, TValue>(this Dictionary<TKey, TValue> instance, TKey key, TValue value)
		{
			if (instance.ContainsKey(key)) return false;
			// todo: race condition
			instance.Add(key, value);
			return true;
		}

		/// <summary>
		/// Increment the value at <paramref name="key"/> by <see cref="1"/> if it exists, and initialize it to <see cref="1"/> if it does not.
		/// </summary>
		/// <typeparam name="TKey">the key type of the dictionary</typeparam>
		/// <param name="instance">the dictionary instance to operate on</param>
		/// <param name="key">the key to index the dictionary with</param>
		public static void IncrementOrCreateKey<TKey>(this Dictionary<TKey, Int32> instance, TKey key)
		{
			if (instance.TryGetValue(key, out Int32 value))
			{
				instance[key] = value + 1;
			}
			else
			{
				instance[key] = 1;
			}
		}

		/// <summary>
		/// Increment the value at <paramref name="key"/> by <see cref="1"/> if it exists, and initialize it to <see cref="1"/> if it does not.
		/// </summary>
		/// <typeparam name="TKey">the key type of the dictionary</typeparam>
		/// <param name="instance">the dictionary instance to operate on</param>
		/// <param name="key">the key to index the dictionary with</param>
		public static void IncrementOrCreateKey<TKey>(this Dictionary<TKey, UInt32> instance, TKey key)
		{
			if (instance.TryGetValue(key, out UInt32 value))
			{
				instance[key] = value + 1;
			}
			else
			{
				instance[key] = 1;
			}
		}

		/// <summary>
		/// Increment the value at <paramref name="key"/> by <see cref="1"/> if it exists, and initialize it to <see cref="1"/> if it does not.
		/// </summary>
		/// <typeparam name="TKey">the key type of the dictionary</typeparam>
		/// <param name="instance">the dictionary instance to operate on</param>
		/// <param name="key">the key to index the dictionary with</param>
		public static void IncrementOrCreateKey<TKey>(this Dictionary<TKey, Int64> instance, TKey key)
		{
			if (instance.TryGetValue(key, out Int64 value))
			{
				instance[key] = value + 1;
			}
			else
			{
				instance[key] = 1;
			}
		}

		/// <summary>
		/// Increment the value at <paramref name="key"/> by <see cref="1"/> if it exists, and initialize it to <see cref="1"/> if it does not.
		/// </summary>
		/// <typeparam name="TKey">the key type of the dictionary</typeparam>
		/// <param name="instance">the dictionary instance to operate on</param>
		/// <param name="key">the key to index the dictionary with</param>
		public static void IncrementOrCreateKey<TKey>(this Dictionary<TKey, UInt64> instance, TKey key)
		{
			if (instance.TryGetValue(key, out UInt64 value))
			{
				instance[key] = value + 1;
			}
			else
			{
				instance[key] = 1;
			}
		}

		/// <summary>
		/// Prints out the contents of a Count dictionary as created using <see cref="IncrementOrCreateKey"/>
		/// </summary>
		/// <typeparam name="TKey"></typeparam>
		/// <typeparam name="TCount"></typeparam>
		/// <param name="instance"></param>
		/// <param name="sb"></param>
		/// <param name="keyPad"></param>
		public static void PrintCountDictionary<TKey>(
			this Dictionary<TKey, UInt64> instance, StringBuilder sb,
			string format = "{0} : {1} / {2}",
			int maxResults = int.MaxValue,
			Func<TKey, string> extraFormatter = null
		)
		{
			if (instance.Count == 0)
			{
				sb.AppendLine("No elements");
				return;
			}
			UInt64 total = instance.Values.Aggregate<ulong>((a, n) => a + n);
			UInt64 remainCount = total;
			int values = 0;
			foreach (var kvp in instance.OrderByDescending<KeyValuePair<TKey, ulong>, ulong>(kvp => kvp.Value))
			{
				string key = typeof(TKey) == typeof(string) ? $"'{kvp.Key}'" : kvp.Key.ToString();

				if(extraFormatter != null)
				{
					string extraValue = extraFormatter(kvp.Key);
					sb.AppendLine(string.Format(format, key, kvp.Value, total, extraValue));
				}
				else
				{
					sb.AppendLine(string.Format(format, key, kvp.Value, total));
				}

				
				remainCount -= kvp.Value;
				values++;
				if (--maxResults <= 0) break;
			}

			// fiddle with the format to obtain the amount of indentation used.
			var index = format.IndexOf('{');
			string format2 = format.Substring(0, index) + "{0}";

			int remainValues = instance.Count - values;

			sb.AppendLine(string.Format(format2, ""));
			if (remainCount > 0)
			{
				sb.AppendLine(string.Format(format2, $"Skipped {remainValues} more values, for a combined {remainCount} occurrences."));
			}
			sb.AppendLine(string.Format(format2, $"Total {instance.Count} values, together {total} occurrences counted."));
		}

		public static void MergeCountDictionary<TKey>
			(this Dictionary<TKey, Int32> instance, Dictionary<TKey, Int32> other)
		{
			foreach (KeyValuePair<TKey, Int32> kvp in other)
			{
				var key = kvp.Key;
				var val = kvp.Value;
				if (instance.ContainsKey(key))
				{
					instance[key] += val;
				}
				else
				{
					instance[key] = val;
				}
			}
		}

		public static void MergeCountDictionary<TKey>
			(this Dictionary<TKey, UInt32> instance, Dictionary<TKey, UInt32> other)
		{
			foreach (KeyValuePair<TKey, UInt32> kvp in other)
			{
				var key = kvp.Key;
				var val = kvp.Value;
				if (instance.ContainsKey(key))
				{
					instance[key] += val;
				}
				else
				{
					instance[key] = val;
				}
			}
		}

		public static void MergeCountDictionary<TKey>
			(this Dictionary<TKey, Int64> instance, Dictionary<TKey, Int64> other)
		{
			foreach (KeyValuePair<TKey, Int64> kvp in other)
			{
				var key = kvp.Key;
				var val = kvp.Value;
				if (instance.ContainsKey(key))
				{
					instance[key] += val;
				}
				else
				{
					instance[key] = val;
				}
			}
		}

		public static void MergeCountDictionary<TKey>
			(this Dictionary<TKey, UInt64> instance, Dictionary<TKey, UInt64> other)
		{
			foreach (KeyValuePair<TKey, UInt64> kvp in other)
			{
				var key = kvp.Key;
				var val = kvp.Value;
				if (instance.ContainsKey(key))
				{
					instance[key] += val;
				}
				else
				{
					instance[key] = val;
				}
			}
		}

		/// <summary>
		/// Create pairs of elements, substituting null in the last pair if the enumerable contained an odd number of elements.
		/// </summary>
		/// <typeparam name="TData"></typeparam>
		/// <param name="source"></param>
		/// <returns></returns>
		public static IEnumerable<(TData, TData)> Pairs<TData>(this IEnumerable<TData> source)
			where TData : class
		{
			var enumerator = source.GetEnumerator();
			while (enumerator.MoveNext())
			{
				var a = enumerator.Current;

				if (false == enumerator.MoveNext())
				{
					yield return (a, null);
					yield break;
				}

				var b = enumerator.Current;
				yield return (a, b);
			}
		}

		/// <summary>
		/// Create pairs of elements, dropping the final element if the source contained a odd number of elements.
		/// </summary>
		/// <typeparam name="TData"></typeparam>
		/// <param name="source"></param>
		/// <returns></returns>
		/// <exception cref="System.ArgumentException">if <paramref name="throwIfOdd"/> is specified and the source contained an odd number of elements.</exception>
		public static IEnumerable<(TData, TData)> Pairs<TData>(this IEnumerable<TData> source, bool throwIfOdd = false)
		{
			var enumerator = source.GetEnumerator();
			while (enumerator.MoveNext())
			{
				var a = enumerator.Current;

				if (false == enumerator.MoveNext())
				{
					if (throwIfOdd)
						throw new ArgumentException("Enumerator must have even number of elements to create pairs.");
					yield break;
				}

				var b = enumerator.Current;
				yield return (a, b);
			}
		}
	}
}
