// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;

using BinaryDataModel.Converters;

using Pastel;

using Shared;

namespace CommandLine
{
	public class Program
	{
		public static bool Do_CLCC = Debugger.IsAttached;
		public static bool GL_Debug = true;
		public static bool savesAvailable = true;

		private static CultureInfo consistentNumberFormatCulture = CultureInfo.GetCultureInfo("en-GB");

		static void Main(string[] args)
		{
			if (! args.Contains("-NoHeader"))
			{
				var assembly = Assembly.GetExecutingAssembly();
				var fvi = System.Diagnostics.FileVersionInfo.GetVersionInfo(assembly.Location);
				var version = fvi.ProductVersion;

				Console.WriteLine
					(
					 "Copyright 2022-2023 CodeLeopard"
				   + "\nThis program comes with ABSOLUTELY NO WARRANTY"
				   + "\nLicense: LGPL-3.0-or-later See: https://www.gnu.org/licenses/"
				   + "\nSource code: https://gitlab.com/CodeLeopard/StormworksData/"
				   + $"\nVersion: {version}"
					);
			}

			if (!Thread.CurrentThread.CurrentCulture.Equals(consistentNumberFormatCulture))
			{
				var oldCulture = Thread.CurrentThread.CurrentCulture.Name;
				Thread.CurrentThread.CurrentCulture = consistentNumberFormatCulture;
				Console.WriteLine(($"Waring: current culture '{oldCulture}' may not provide proper number formatting."
				                 + $"\nThe culture was changed to '{Thread.CurrentThread.CurrentCulture.Name}' to prevent issues.").Pastel(Color.Orange));
			}

			if (args.Contains("-NoUserData"))
			{
				savesAvailable = false;
			}

			if (! args.Contains("-Continuous_Integration"))
			{
				new CommandLineInteraction().Run();

				return;
			}

			if (Debugger.IsAttached)
			{
				CustomTests();
				//AllTests();
			}
			else
			{
				AllTests();
			}
		}

		internal static void CustomTests()
		{
			AnalyticsParser.Run();
			//DefinitionsTest.Run();
		}

		internal static void AllTests()
		{
			Console.WriteLine($"StormworksPaths: '{StormworksPaths.Install}'");
			bool anyFailed = false;

			try
			{
				AnalyticsParser.Run();
			}
			catch (Exception e)
			{
				anyFailed = true;
				Console.WriteLine(e.ToString().Pastel(Color.Red));
			}



			VehicleTest.ListComponentCounts = false;
			VehicleTest.DoDefinitionUsage = false;
			VehicleTest.GenerateMeshes = true;
			try
			{
				VehicleTest.Run();
			}
			catch (Exception e)
			{
				anyFailed = true;
				Console.WriteLine(e.ToString().Pastel(Color.Red));
			}

			// Lives in Unity for now.
			//ExtrudeTest.Run();

			Console.WriteLine("Tests completed.");
			if (anyFailed)
			{
				Console.WriteLine("Some tests failed.");
				Environment.ExitCode = 1; // signal error to CI
			}
		}


		/// <summary>
		/// Creates a relative path from one file or folder to another.
		/// </summary>
		/// <param name="fromPath">Contains the directory that defines the start of the relative path.</param>
		/// <param name="toPath">Contains the path that defines the endpoint of the relative path.</param>
		/// <returns>The relative path from the start directory to the end path.</returns>
		/// <exception cref="ArgumentNullException"><paramref name="fromPath"/> or <paramref name="toPath"/> is <c>null</c>.</exception>
		/// <exception cref="UriFormatException"></exception>
		/// <exception cref="InvalidOperationException"></exception>
		public static string GetRelativePath(string fromPath, string toPath)
		{
			if (string.IsNullOrEmpty(fromPath)) throw new ArgumentNullException(nameof(fromPath));
			if (string.IsNullOrEmpty(toPath)) throw new ArgumentNullException(nameof(toPath));

			Uri fromUri = new Uri(AppendDirectorySeparatorChar(fromPath));
			Uri toUri = new Uri(AppendDirectorySeparatorChar(toPath));

			if (fromUri.Scheme != toUri.Scheme)
			{
				return toPath;
			}

			Uri relativeUri = fromUri.MakeRelativeUri(toUri);
			string relativePath = Uri.UnescapeDataString(relativeUri.ToString());

			if (string.Equals(toUri.Scheme, Uri.UriSchemeFile, StringComparison.OrdinalIgnoreCase))
			{
				relativePath = relativePath.Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar);
			}

			return relativePath;
		}

		private static string AppendDirectorySeparatorChar(string path)
		{
			// Append a slash only if the path is a directory and does not have a slash.
			if (!Path.HasExtension(path) &&
			    !path.EndsWith(Path.DirectorySeparatorChar.ToString()))
			{
				return path + Path.DirectorySeparatorChar;
			}

			return path;
		}
	}
}
