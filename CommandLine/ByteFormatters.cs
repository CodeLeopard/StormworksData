﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;

using BinaryDataModel;
using BinaryDataModel.DataTypes;

using OpenToolkit.Mathematics;

using Pastel;

using ArrayRange = Shared.ArrayRangedWrapper;
using ByteRange = Shared.ArrayRangedWrapper<byte>;


namespace CommandLine
{
	internal static class ByteFormatters
	{
		internal const int ByteRowHeaderLength = 3;
		internal static string ByteRowHeaderDefault = "  ";

		internal static readonly byte PadLengthUInt16 = (byte) UInt16.MaxValue.ToString().Length;
		internal static readonly byte PadLengthUInt32 = (byte) UInt32.MaxValue.ToString().Length;
		internal static readonly byte PadLengthFloat32 = 15;


		public static string ToHexDumpString_Header(this Mesh r)
		{
			using var s = new MemoryStream();
			using var b = new BinaryWriter(s);
			b.WriteHeader(r);

			var a = s.ToArray();

			int i = 0;

			int A(int length)
			{
				i += length;
				return length;
			}

			var sb = new StringBuilder();
			FormatBytesRowDataRecord(ArrayRange.New(a, i, A(2)), sb, false, "");
			sb.Append($">>> header0       : {r.header0}\n");

			FormatBytesRowDataRecord(ArrayRange.New(a, i, A(2)), sb, false, "");
			sb.Append($">>> header1       : {r.header1}\n");

			FormatBytesRowDataRecord(ArrayRange.New(a, i, A(2)), sb, false, "");
			sb.Append($">>> VertexCount   : {r.vertexCount}\n");

			FormatBytesRowDataRecord(ArrayRange.New(a, i, A(2)), sb, false, "");
			sb.Append($">>> header3       : {r.header3}\n");

			FormatBytesRowDataRecord(ArrayRange.New(a, i, A(2)), sb, false, "");
			sb.Append($">>> header4       : {r.header4}\n");

			return sb.ToString();
		}

		public static string ToHexDumpString_Header(this Phys r)
		{
			using var s = new MemoryStream();
			using var b = new BinaryWriter(s);
			b.WriteHeader(r);

			var a = s.ToArray();

			int i = 0;

			int A(int length)
			{
				i += length;
				return length;
			}

			var sb = new StringBuilder();
			FormatBytesRowDataRecord(ArrayRange.New(a, i, A(2)), sb, false, "");
			sb.Append($">>> header0       : {r.header0}\n");

			FormatBytesRowDataRecord(ArrayRange.New(a, i, A(2)), sb, false, "");
			sb.Append($">>> SubMeshCount  : {r.subMeshCount}\n");

			return sb.ToString();
		}


		public static string ToHexDumpString(this VertexRecord r, bool finalNewLine = true)
		{
			using var s = new MemoryStream();
			using var b = new BinaryWriter(s);
			b.Write(r);
			var a = s.ToArray();

			int i = 0;

			int A(int length)
			{
				i += length;
				return length;
			}

			var sb = new StringBuilder();


			FormatBytesRowDataRecord(new ByteRange(a, i, A(4)), sb, true, "X");
			FormatBytesRowDataRecord(new ByteRange(a, i, A(4)), sb, true, "Y");
			FormatBytesRowDataRecord(new ByteRange(a, i, A(4)), sb, true, "Z");

			FormatBytesRowDataRecord(new ByteRange(a, i, A(4)), sb, true, "C");

			FormatBytesRowDataRecord(new ByteRange(a, i, A(4)), sb, true, "Nx");
			FormatBytesRowDataRecord(new ByteRange(a, i, A(4)), sb, true, "Ny");
			FormatBytesRowDataRecord(new ByteRange(a, i, A(4)), sb, finalNewLine, "Nz");


			return sb.ToString();
		}

		public static string ToHexDumpString(this Vector3 r, bool finalNewLine = true)
		{
			using var s = new MemoryStream();
			using var b = new BinaryWriter(s);
			b.Write(r);
			var a = s.ToArray();

			int i = 0;

			int A(int length)
			{
				i += length;
				return length;
			}

			var sb = new StringBuilder();

			FormatBytesRowDataRecord(new ByteRange(a, i, A(4)), sb, true, "X");
			FormatBytesRowDataRecord(new ByteRange(a, i, A(4)), sb, true, "Y");
			FormatBytesRowDataRecord(new ByteRange(a, i, A(4)), sb, finalNewLine, "Z");

			return sb.ToString();
		}

		public static string ToHexDumpString(this Triangle16 r)
		{
			using var s = new MemoryStream();
			using var b = new BinaryWriter(s);
			b.Write(r);

			return DoArrayHexDump(s.ToArray(), 6, true, ByteRowFormat.DataRecord);
		}

		public static string ToHexDumpString(this Triangle32 r)
		{
			using var s = new MemoryStream();
			using var b = new BinaryWriter(s);
			b.Write(r);

			return DoArrayHexDump(s.ToArray(), 4, false, ByteRowFormat.DataRecord);
		}

		public static string ToHexDumpString(this UInt16 r, string valueHeader = "")
		{
			using var s = new MemoryStream();
			using var b = new BinaryWriter(s);
			b.Write(r);
			var a = s.ToArray();

			var sb = new StringBuilder();

			FormatBytesRowDataRecord(new ByteRange(a), sb);
			sb.Append($">>> {valueHeader, -14}: {r}\n");

			return sb.ToString();
		}

		public static string ToHexDumpString(this UInt32 r, string valueHeader = "")
		{
			using var s = new MemoryStream();
			using var b = new BinaryWriter(s);
			b.Write(r);
			var a = s.ToArray();

			var sb = new StringBuilder();

			FormatBytesRowDataRecord(new ByteRange(a), sb);
			sb.Append($">>> {valueHeader,-14}: {r}\n");

			return sb.ToString();
		}


		public static string ToHexDumpString(this SubMesh r)
		{
			using var s = new MemoryStream();
			using var b = new BinaryWriter(s);
			b.Write(r);
			var a = s.ToArray();

			int i = 0;

			int A(int length)
			{
				i += length;
				return length;
			}

			var sb = new StringBuilder();
			FormatBytesRowDataRecord(ArrayRange.New(a, i, A(4)), sb, false, "");
			sb.AppendLine($">>> IBufferStart  : {r.indexBufferStart}");
			FormatBytesRowDataRecord(ArrayRange.New(a, i, A(4)), sb, false, "");
			sb.AppendLine($">>> IBufferLength : {r.indexBufferLength}");

			FormatBytesRowDataRecord(ArrayRange.New(a, i, A(2)), sb, false, "");
			sb.AppendLine($">>> header2       : {r.header2}");

			FormatBytesRowDataRecord(ArrayRange.New(a, i, A(2)), sb, false, "");
			sb.AppendLine($">>> shaderID      : {r.shaderId}");

			FormatBytesRowDataRecord(ArrayRange.New(a, i, A(4)), sb, false, "");
			sb.AppendLine($">>> boundsMinX    : {r.bounds.Min.X}");
			FormatBytesRowDataRecord(ArrayRange.New(a, i, A(4)), sb, false, "");
			sb.AppendLine($">>> boundsMinY    : {r.bounds.Min.Y}");
			FormatBytesRowDataRecord(ArrayRange.New(a, i, A(4)), sb, false, "");
			sb.AppendLine($">>> boundsMinZ    : {r.bounds.Min.Z}");

			FormatBytesRowDataRecord(ArrayRange.New(a, i, A(4)), sb, false, "");
			sb.AppendLine($">>> boundsMaxX    : {r.bounds.Max.X}");
			FormatBytesRowDataRecord(ArrayRange.New(a, i, A(4)), sb, false, "");
			sb.AppendLine($">>> boundsMaxY    : {r.bounds.Max.Y}");
			FormatBytesRowDataRecord(ArrayRange.New(a, i, A(4)), sb, false, "");
			sb.AppendLine($">>> boundsMaxZ    : {r.bounds.Max.Z}");

			FormatBytesRowDataRecord(ArrayRange.New(a, i, A(2)), sb, false, "");
			sb.AppendLine($">>> header6       : {r.header6}");

			FormatBytesRowDataRecord(ArrayRange.New(a, i, A(2)), sb, false, "");
			sb.AppendLine($">>> nameByteLength: {r.name.Length}");

			sb.Append(DoArrayHexDump(ArrayRange.New(a, i, A(r.name.Length)), 6));

			FormatBytesRowDataRecord(ArrayRange.New(a, i, A(4)), sb, false, "");
			sb.AppendLine($">>> header8X      : {r.header8.X}");
			FormatBytesRowDataRecord(ArrayRange.New(a, i, A(4)), sb, false, "");
			sb.AppendLine($">>> header8Y      : {r.header8.Y}");
			FormatBytesRowDataRecord(ArrayRange.New(a, i, A(4)), sb, false, "");
			sb.AppendLine($">>> header8Z      : {r.header8.Z}");


			return sb.ToString();
		}





		internal static string DoArrayHexDump(IEnumerable<byte> bytes, byte rowByteCount, bool noFinalNewLine = false, ByteRowFormat format = ByteRowFormat.DataRecord)
		{
			var sb = new StringBuilder();
			DoArrayHexDump((byte[]) bytes.ToArray(), rowByteCount, sb, noFinalNewLine: noFinalNewLine, format: format);
			return sb.ToString();
		}

		internal static void DoArrayHexDump(IEnumerable<byte> bytes, byte rowByteCount, StringBuilder sb, bool noFinalNewLine = false, ByteRowFormat format = ByteRowFormat.DataRecord)
		{
			DoArrayHexDump((byte[]) bytes.ToArray(), rowByteCount, sb, noFinalNewLine: noFinalNewLine, format: format);
		}


		internal static void DoArrayHexDump(byte[] bytes, byte rowByteCount, StringBuilder sb, bool noFinalNewLine = false, ByteRowFormat format = ByteRowFormat.DataRecord)
		{
			int i;
			for (i = 0; i < bytes.Length - (rowByteCount - 1);)
			{
				FormatBytesRow(new ByteRange(bytes, i, rowByteCount), sb, newLine: true, format: format);

				i += rowByteCount;
			}


			if (i < bytes.Length) // some bytes remain
			{
				FormatBytesRow(new ByteRange(bytes, i, bytes.Length - i), sb, newLine: true, format: format);
			}

			if (noFinalNewLine)
			{
				var nll = Environment.NewLine.Length;
				sb.Remove(sb.Length - nll, nll);
			}
		}

		#region Overloads

		internal static string FormatBytesRow(IEnumerable<byte> bytes, bool newLine = false, string header = null, ByteRowFormat format = ByteRowFormat.DataRecord)
		{
			var sb = new StringBuilder();
			FormatBytesRow((byte[]) bytes.ToArray(), sb, newLine: newLine, header: header, format: format);
			return sb.ToString();
		}

		internal static string FormatBytesRow(ByteRange bytes, bool newLine = false, string header = null, ByteRowFormat format = ByteRowFormat.DataRecord)
		{
			var sb = new StringBuilder();
			FormatBytesRow(bytes, sb, newLine: newLine, header: header, format: format);
			return sb.ToString();
		}

		internal static string FormatBytesRow(byte[] bytes, bool newLine = false, string header = null, ByteRowFormat format = ByteRowFormat.DataRecord)
		{
			var sb = new StringBuilder();
			FormatBytesRow(new ByteRange(bytes), sb, newLine: newLine, header: header, format: format);
			return sb.ToString();
		}

		internal static void FormatBytesRow(IEnumerable<byte> bytes, StringBuilder sb, bool newLine = false, string header = null, ByteRowFormat format = ByteRowFormat.DataRecord)
		{
			FormatBytesRow(new ByteRange(bytes.ToArray()), sb, newLine: newLine, header: header, format: format);
		}

		internal static void FormatBytesRow(byte[] bytes, StringBuilder sb, bool newLine = false, string header = null, ByteRowFormat format = ByteRowFormat.DataRecord)
		{
			FormatBytesRow(new ByteRange(bytes), sb, newLine: newLine, header: header, format: format);
		}

		#endregion Overloads

		public enum ByteRowFormat { Raw, Legacy, DataRecord, Commonality }

		internal static void FormatBytesRow(ByteRange bytes, StringBuilder sb, bool newLine = false, string header = null, ByteRowFormat format = ByteRowFormat.DataRecord)
		{
			switch (format)
			{
				case ByteRowFormat.Raw:
					FormatBytesRowRaw(bytes, sb, newLine: newLine, header: header);
					break;
				case ByteRowFormat.Legacy:
					FormatBytesRowLegacy(bytes, sb, newLine: newLine, header: header);
					break;
				case ByteRowFormat.DataRecord:
					FormatBytesRowDataRecord(bytes, sb, newLine: newLine, header: header);
					break;
				case ByteRowFormat.Commonality:
					FormatBytesRowDataCommonality(bytes, sb, newLine: newLine, header: header);
					break;
			}
		}

		internal static void FormatBytesRowDataRecord(ByteRange bytes, StringBuilder sb, bool newLine = false, string header = null)
		{
			void UI16(UInt16 o)
			{
				sb.Append(o.ToString().PadLeft(PadLengthUInt16));
				sb.Append(" ");
			}
			void UI32(UInt32 o)
			{
				//var Normal = GetIntColor(o);
				var Revers = GetIntColor(o, true);

				var numStr = o.ToString().PadLeft(PadLengthUInt32 /*, '_'*/);

				//var part1 = numStr.Substring(0, PadLengthUInt32 / 2);
				//var part2 = numStr.Substring(PadLengthUInt32 / 2);

				//sb.Append(part1.Pastel(Normal.secondary).PastelBg(Normal.primary));
				//sb.Append(part2.Pastel(Revers.secondary).PastelBg(Revers.primary));

				if (header.StartsWith("C") || true)
				{
					sb.Append(numStr.Pastel(Revers.secondary).PastelBg(Revers.primary));
				}
				else
				{
					sb.Append(numStr);
				}

				sb.Append(" ");
			}

			const int EmptyShade = 60;
			var EmptyColor = Color.FromArgb(EmptyShade, EmptyShade, EmptyShade);
			void Empty(int length)
			{
				sb.Append(new string('_', length).Pastel(EmptyColor) + " ");
			}

			UInt32 num = 0;

			// ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ###
			if (null == header) header = ByteRowHeaderDefault;
			header = header.PadRight(ByteRowHeaderLength);
			sb.Append(header);

			byte Length = (byte)bytes.Length;

			// Short integer representations

			if (Length >= 2)
				UI16(BitConverter.ToUInt16(bytes, 0));
			else Empty(PadLengthUInt16);

			if (Length >= 4)
				UI16(BitConverter.ToUInt16(bytes, 2));
			else Empty(PadLengthUInt16);

			if (Length >= 6)
				UI16(BitConverter.ToUInt16(bytes, 4));
			else Empty(PadLengthUInt16);

			// integer and float representation
			if (Length >= 4)
			{
				num = BitConverter.ToUInt32(bytes, 0);
				UI32(num);
				sb.Append(BitConverter.ToSingle(bytes, 0).ToString().PadLeft(PadLengthFloat32));
				sb.Append(" ");
			}
			else
			{
				Empty(PadLengthUInt32);
				Empty(PadLengthFloat32);
			}

			// byte Color representation


			const byte bgShade = 30;
			//const string format = "{0:x2} {1:x2}"; // todo: replace bytes format with this, so changes are made to all.

			// Color
			{
				const int count = 11;
				if (Length >= 4)
				{
					/*
					{
						// Normal color (ARGB)
						var c = GetIntColor(num);

						var alpha = c.primary.A;
						var alphaShade = Color.FromArgb(alpha, alpha, alpha);
						sb.Append(" ".PastelBg(alphaShade).Pastel(MaximumContrast(alphaShade)));

						var R = Color.FromArgb(c.primary.R, 0, 0);
						sb.Append(" ".PastelBg(R).Pastel(MaximumContrast(R)));

						var G = Color.FromArgb(0, c.primary.G, 0);
						sb.Append(" ".PastelBg(G).Pastel(MaximumContrast(G)));

						var B = Color.FromArgb(0, 0, c.primary.B);
						sb.Append(" ".PastelBg(B).Pastel(MaximumContrast(B)));
					}*/

					{
						// Reverse color (RGBA)
						var c = GetIntColor(num, true);

						sb.Append(FormatColorPastel(c.primary));
					}
					sb.Append(" ");
				}
				else
					Empty(count);
			}

			// individual bytes

			for (int i = 0; i < Length; i++)
			{
				if(i == 2)
					sb.Append($"{bytes[i]:x2} ".PastelBg(Color.FromArgb(bgShade, bgShade, bgShade)));
				else if(i == 3)
					sb.Append($"{bytes[i]:x2}".PastelBg(Color.FromArgb(bgShade, bgShade, bgShade)) + " ");
				else
					sb.Append($"{bytes[i]:x2} ");
			}

			for (int i = Length; i < 6; i++)
			{
				if (i == 2) sb.Append("__ ".Pastel(EmptyColor).PastelBg(Color.FromArgb(bgShade, bgShade, bgShade)));
				else if(i == 3)
					sb.Append($"__".Pastel(EmptyColor).PastelBg(Color.FromArgb(bgShade, bgShade, bgShade)) + " ");
				else
					Empty(2);
			}

			HexDumpString(bytes, sb);
			if (newLine) sb.AppendLine();
		}

		public static string FormatColorPastel(Color primary)
		{
			StringBuilder sb = new StringBuilder();
			var R = Color.FromArgb(primary.R, 0, 0);
			sb.Append("R ".PastelBg(R).Pastel(MaximumContrast(R)));

			var G = Color.FromArgb(0, primary.G, 0);
			sb.Append("G ".PastelBg(G).Pastel(MaximumContrast(G)));

			var B = Color.FromArgb(0, 0, primary.B);
			sb.Append("B ".PastelBg(B).Pastel(MaximumContrast(B)));

			var alpha = primary.A;
			var alphaShade = Color.FromArgb(alpha, alpha, alpha);
			sb.Append("A ".PastelBg(alphaShade).Pastel(MaximumContrast(alphaShade)));

			sb.Append(":");

			var l = primary.GetBrightness();
			var L = Color.FromArgb((int) (l * 255f), (int) (l * 255), (int) (l * 255));
			sb.Append("L ".PastelBg(L).Pastel(MaximumContrast(L)));

			return sb.ToString();
		}

		internal static void FormatBytesRowLegacy(ByteRange bytes, StringBuilder sb, bool newLine = false, string header = null)
		{
			if (null == header) header = ByteRowHeaderDefault;
			sb.Append(header);

			const byte firstPad = 11;
			const byte secondPad = 24;


			switch (bytes.Length)
			{
				case 1:
				{ // single byte
					sb.Append(bytes[0].ToString().PadLeft(firstPad));
					sb.Append(new string(' ', secondPad + 1 + 1)); // Empty space for float.
					sb.Append($"{bytes[0]:x2}");
					sb.Append(" ");
					break;
				}
				case 2:
				{
					UInt16 i = BitConverter.ToUInt16(bytes, 0);
					sb.Append(i.ToString().PadLeft(firstPad));
					const byte betweenPaddedFields = 2;
					const byte untilAligned = 6;
					sb.Append(new string(' ', secondPad + betweenPaddedFields + untilAligned)); // Empty space for float.

					foreach (byte b in bytes)
					{
						sb.Append($"{b:x2}");
						sb.Append(" ");
					}
					break;
				}
				case 4:
				{
					UInt32 i = BitConverter.ToUInt32(bytes, 0);
					(Color nfg, Color nbg) = GetIntColor(i);
					sb.Append(i.ToString().PadLeft(firstPad).Pastel(nfg).PastelBg(nbg));

					float f = BitConverter.ToSingle(bytes, 0);
					sb.Append(" ");
					sb.Append(f.ToString().PadLeft(secondPad));
					sb.Append(" ");

					(Color rfg, Color rbg) = GetIntColor(i, true);
					foreach (byte b in bytes)
					{
						sb.Append($"{b:x2}".Pastel(rfg).PastelBg(rbg));
						sb.Append(" ");
					}
					break;
				}
				case 6:
				{
					UInt16 i1 = BitConverter.ToUInt16(bytes, 0);
					UInt16 i2 = BitConverter.ToUInt16(bytes, 2);
					UInt16 i3 = BitConverter.ToUInt16(bytes, 4);

					sb.Append(i1.ToString().PadLeft(PadLengthUInt16) + "  ");
					sb.Append(i2.ToString().PadLeft(PadLengthUInt16) + "  ");
					sb.Append(i3.ToString().PadLeft(PadLengthUInt16) + "  │         ");

					sb.Append($"{bytes[0]:x2} {bytes[1]:x2} ");
					const byte shade = 60;
					sb.Append($"{bytes[2]:x2} {bytes[3]:x2}".PastelBg(Color.FromArgb(shade, shade, shade)) + " ");
					sb.Append($"{bytes[4]:x2} {bytes[5]:x2} ");

					break;
				}
				default:
				{
					foreach (byte b in bytes)
					{
						sb.Append($"{b:x2}");
						sb.Append(" ");
					}
					break;
				}
			}
			HexDumpString(bytes, sb);
			if (newLine) sb.AppendLine();
		}

		internal static void FormatBytesRowRaw(ByteRange bytes, StringBuilder sb, bool newLine = false, string header = null)
		{
			if (null == header) header = ByteRowHeaderDefault;
			sb.Append(header);

			foreach (byte b in bytes)
			{
				sb.Append($"{b:x2}");
				sb.Append(" ");
			}
			
			HexDumpString(bytes, sb);
			if (newLine) sb.AppendLine();
		}

		internal static void FormatBytesRowDataCommonality(ByteRange bytes, StringBuilder sb, bool newLine = false, string header = null)
		{
			if (null == header) header = ByteRowHeaderDefault;
			sb.Append(header);

			Color color = Color.DarkGray;
			Color colorbg = Color.Black;

			for (int i = 0; i < bytes.Length; i++)
			{
				var noDistinctValues =AnalyticsParser.distinctValues[i].Count;
				if (noDistinctValues > 1)
				{
					var color1 = Color.DarkKhaki;
					var color2 = Color.Red;

					color = color1;

					if (noDistinctValues > 32)
					{
						color = color2;
					}

					// todo: make fancy
				}
				sb.Append($"{bytes[i]:x2}".Pastel(color).PastelBg(colorbg));
				sb.Append(" ");
			}

			//HexDumpString(bytes, sb);
				if (newLine) sb.AppendLine();
		}

		/// <summary>
		/// Create a pair of colors (primary, contrast) from a integer.
		/// </summary>
		/// <param name="i"></param>
		/// <param name="reverse">format bgra instead of argb</param>
		/// <returns></returns>
		internal static (Color primary, Color secondary) GetIntColor(UInt32 i, bool reverse = false)
		{
			Color primary;
			var bytes = BitConverter.GetBytes(i);
			if (reverse)
			{
				primary = Color.FromArgb(bytes[3], bytes[0], bytes[1], bytes[2]);
			}
			else
			{
				primary = Color.FromArgb(bytes[0], bytes[1], bytes[2], bytes[3]);
			}

			Color secondary = MaximumContrast(primary);

			return (primary, secondary);
		}

		internal static Color MaximumContrast(Color c)
		{
			if (c.GetBrightness() > 0.4f)
				return Color.Black;
			else
				return Color.White;

			/*
			var r = 255 - c.R;
			var g = 255 - c.G;
			var b = 255 - c.B;

			return Color.FromArgb(r, g, b);*/
		}


		static void HexDumpString(byte[] bytes, StringBuilder sb)
		{
			var str = Encoding.Default.GetString(bytes, 0, bytes.Length);
			var startlength = sb.Length;
			foreach (char c in str)
			{
				if (Char.IsControl(c))
				{
					sb.Append(".".Pastel(Color.Orange));
				}
				else if (Char.IsLetterOrDigit(c))
				{
					sb.Append(c);
				}
				else if (Char.IsWhiteSpace(c))
				{
					sb.Append(c.ToString().PastelBg(Color.DarkSlateGray));
				}
				else
				{
					sb.Append(c.ToString().Pastel(Color.Red));
				}
			}
			const int defaultWidth = 6;
			var toDefaultWidth = defaultWidth - bytes.Length;

			var addedChars = str.Length; // deal with invisible characters
			var missingChars = bytes.Length - addedChars;

			var toAppend = toDefaultWidth + missingChars;
			sb.Append(new string(' ', Math.Max(0, toAppend))); // Normalize length with other lines
		}
	}
}
