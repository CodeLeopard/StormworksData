// Copyright 2022-2024 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;

using BinaryDataModel.Converters;
using BinaryDataModel.DataTypes;
using BinaryDataModel.DataTypes.Enums;
using NeoSmart.StreamCompare;

using OpenToolkit.Mathematics;

using Pastel;

using Shared;

using static CommandLine.ByteFormatters;

namespace CommandLine
{
	public static class AnalyticsParser
	{

		internal const int testLength = 64;
		internal static Dictionary<byte, int>[] distinctValues = null;

		public static string meshFolderPath => StormworksPaths.meshes;


		public static string[] files = new[]
		{
			"arrow_camera.mesh",                //  0
			"arrow_infrared_detector.mesh",     //  1
			"island_11.mesh",                   //  2 (big)
			"island_06.mesh",                   //  3
			"hangar_door_01.phys",              //  4
			"wind_turbine_02_blades.mesh",      //  5
			"wind_turbine_02_blades_phys.phys", //  6 phys (2)
			"hangar_door.mesh",                 //  7
			"hangar_door_01.phys",              //  8 phys (2)
			"workbench.mesh",                   //  9
			"workbench_phys.phys",              // 10 phys (2)
			"island_01_rocks.phys",             // 11 phys (2)
			"lighthouse_door_phys.phys",        // 12 phys (2)
			"arctic_ice_floating_01_phys.phys", // 13 phys (2)
			"grid_square_voxel.mesh",           // 14
			"ultimate_base_hangar.mesh",        // 15
			"unit_cube.mesh",                   // 16
			"turbine_blades_phys.phys",         // 17
			"extruded_track_pieces\\mega_island_12_6_ExtrudedTrackMesh_Curve_10.phys",// 18
			"component_fluid_exhaust.mesh",      // 19
			"mainland\\mega_island_0_0.phys", // 20
			"mainland\\mega_island_0_0.mesh", //21
			"arctic_ice_floating_01.mesh", // 22
			"arctic_ice_floating_01_phys.phys", // 23
			"lava_phys_underneath_rock.phys", // 24
			"door_arctic_module_interior_phys.phys", // 25
		};

		public static void Run()
		{
			ReadAll();

			AnalyzeMeshes(false, true);
		}

		public static void UserTest()
		{
			SetupDiag();


			//using var file = File.OpenRead(meshFolderPath + Path.DirectorySeparatorChar + files[26]);
			//var phys = Binary.LoadPhys(file, _phyDiag);
			//phys.ConvertToIndexedTriangles();

			//ReadAll();

			//PhysIndexTest(Path.Combine(StormworksPaths.meshes, files[26]));

			Do(files[4]);
			//Do(files[11]);
			Do(files[12]);
			//Do(files[19]);
			//Do(files[20]);
			//Do(files[22]);
			//Do(files[23]);
			/*
			foreach (string file in files)
			{
				if (file.EndsWith(".phys"))
				{
					Do(file);
				}
			}*/

			//Do(files[8]);
			return;

			//Do(files[23]);



			//DoFile($"{localOutputFolder}\\mesh2phys.phys");

			//FindEndCommonality(SeekOrigin.End);
			//ReadHeadersMesh();
			//ReadHeadersPhy();
			//Read(files[0]);
			//Do(files[15]);
			//Do(files[4]);
			//_meshDiag.vertex = null;
			//_meshDiag.index = null;
			//Do("extruded_track_pieces/mega_island_12_7_ExtrudedTrackMesh_Spline_0_Curve_0.mesh");
			//Do(files[18]);

			//ReadFile(File.OpenRead(meshFolderPath + Path.DirectorySeparatorChar + files[17]));

			//ReadAll();

			return;
			void Do(string file) => DoFile(meshFolderPath + Path.DirectorySeparatorChar + file, true);
		}

		static void PhysIndexTest(string path)
		{
			string separator = new string('=', 160).Pastel(Color.Lime);


			var name = Path.GetFileName(path);
			var romPath = PathHelper.GetRelativePath(StormworksPaths.rom, path);

			var nameWithoutExtension = Path.GetFileNameWithoutExtension(path);
			var ext = Path.GetExtension(path);

			Mesh mesh;
			using (var file = File.OpenRead(path))
			{
				Console.WriteLine(separator);
				Console.WriteLine
					($"Source Mesh: {romPath,-70} : {StringExt.PrintBytesReadable((long) file.Length),10}");


				mesh = Binary.LoadMesh(file, _meshDiag);
			}

			var phys1 = Binary.ConvertToPhys(mesh);


			var phys2 = Binary.ConvertToVertexOnly(mesh);

			using (var stream = new MemoryStream())
			{
				Console.WriteLine(separator);
				Console.WriteLine("Converted with indices");
				Binary.Save(stream, phys1);
				stream.Position = 0;
				Binary.LoadPhys(stream, _phyDiag);

				stream.SetLength(0);

				Console.WriteLine(separator);
				phys1.ConvertToOrderedVertices();
				Console.WriteLine("Converted with indices -> Converted to VertexOrder");
				Binary.Save(stream, phys1);
				stream.Position = 0;
				Binary.LoadPhys(stream, _phyDiag);

				stream.SetLength(0);

				Console.WriteLine(separator);
				Console.WriteLine("Converted directly to VertexOrder");
				Binary.Save(stream, phys2);
				stream.Position = 0;
				Binary.LoadPhys(stream, _phyDiag);
			}
		}

		

		#region Diag
		static Binary.MeshDiagCallback _meshDiag = null;
		static Binary.PhyDiagCallback _phyDiag = null;

		static void SetupDiag()
		{
			_meshDiag = new Binary.MeshDiagCallback();
			//_meshDiag.header = DiagHeader;
			//_meshDiag.vertex = DiagVertex;
			//_meshDiag.indexCount = DiagIndexCount;
			//_meshDiag.index = DiagIndex;
			//_meshDiag.subMeshCount = v => Console.WriteLine(v.ToHexDumpString("SubMeshCount"));
			//_meshDiag.subMesh = v => Console.WriteLine(v.ToHexDumpString());
			_meshDiag.eofRemain = DiagEof;

			_phyDiag = new Binary.PhyDiagCallback();
			_phyDiag.header0 = v => Console.Write(v.ToHexDumpString("header0"));
			_phyDiag.subMeshCount = PSubMeshCount;
			_phyDiag.vertexCount = DiagPVertexCount;
			_phyDiag.vertex = v => Console.Write($"{PvertexNo++,3} / {PvertexCount,3}{Environment.NewLine}{v.ToHexDumpString()}");
			_phyDiag.indexCount = DiagPIndexCount;
			_phyDiag.index = DiagIndex;
			_phyDiag.eofRemain = DiagEof;
		}

		static void PSubMeshCount(UInt16 count)
		{
			PsubMeshCount = count;
			PsubMeshNo = 0;
			Console.Write(count.ToHexDumpString("subMeshCount"));
		}

		static void DiagPVertexCount(UInt16 v)
		{
			PvertexCount = v;
			PvertexNo = 0;
			Console.WriteLine($"{Environment.NewLine}Submesh {(PsubMeshNo++) + 1,3} / {PsubMeshCount,3}");
			Console.Write(v.ToHexDumpString("VertexCount"));
		}

		static void DiagPIndexCount(UInt16 v)
		{
			PindexCount = v;
			PindexNo = 0;
			Console.Write(v.ToHexDumpString("IndexCount"));
		}

		static void DiagVertex(VertexRecord v)
		{
			Console.WriteLine($"{vertexNo++,3}/{vertexCount}");
			Console.WriteLine(v.ToHexDumpString(false));
		}

		static void DiagIndexCount(UInt32 v)
		{
			indexCount = v;
			Console.WriteLine(v.ToHexDumpString("IndexCount"));
		}

		static void DiagEof(BinaryReader b)
		{
			Console.WriteLine(DoArrayHexDump(b.ReadBytes((int) (b.BaseStream.Length - b.BaseStream.Position)), 4));
		}

		private static UInt32 vertexCount;
		private static UInt32 vertexNo;

		private static UInt32 indexCount;
		private static UInt32 indexNo;

		private static UInt16 PsubMeshCount;
		private static UInt16 PsubMeshNo;

		private static UInt16 PvertexCount;
		private static UInt16 PvertexNo;

		private static UInt32 PindexCount;
		private static UInt32 PindexNo;


		private static UInt16[] indices = new UInt16[3];
		private static byte triIndex = 0;

		private static byte PtriIndex = 0;
		static void DiagIndex(UInt16 partial)
		{
			indices[triIndex++] = partial;

			if (triIndex > 2)
			{
				var t = new Triangle16(indices[0], indices[1], indices[2]);
				triIndex = 0;

				Console.Title = $"{indexCount}\\{indexNo += 3}";
				Console.WriteLine(t.ToHexDumpString());
			}
		}

		static void DiagIndex(UInt32 partial)
		{
			switch (PtriIndex)
			{
				case 0:
					Console.WriteLine($"{PindexNo,3} .. {PindexNo + 2,3} / {PindexCount - 1,3}");
					Console.Write(partial.ToHexDumpString("A"));
					PtriIndex++;
					break;


				case 1:
					Console.Write(partial.ToHexDumpString("B"));
					PtriIndex++;
					break;


				case 2:
					Console.Write(partial.ToHexDumpString("C"));
					PtriIndex = 0;
					break;

				default: throw new Exception();
			}

			PindexNo++;
		}


		#endregion Diag


		public static void MeshReadWriteTest()
		{
			var meshes = Directory.GetFiles(meshFolderPath, "*.mesh", SearchOption.AllDirectories);
			var phys = Directory.GetFiles(meshFolderPath,   "*.phys", SearchOption.AllDirectories);
			var both = meshes.Union(phys);

			//var paths = meshes;
			//var paths = phys;
			var paths = both;


			var timer = new Stopwatch();
			timer.Start();

			foreach (string path in paths)
			{
				MeshReadWrite(path);
			}

			timer.Stop();
			Console.WriteLine($"Done. {timer.Elapsed}");
		}

		public static void MeshToPhysTest(string meshFullPath, string physWritePath, string plyWritePath = null)
		{
			Console.WriteLine($"File '{meshFullPath}' -> Phys");
			var mesh = Binary.LoadMesh(meshFullPath);

			var phys = Binary.ConvertToVertexOnly(mesh);

			if (null != physWritePath)
			{
				Console.WriteLine($"    -> '{physWritePath}'");
				using (var saveFile = File.Open(physWritePath, FileMode.Create))
				{
					Binary.Save(saveFile, phys);
				}
			}

			if (null != plyWritePath)
			{
				Console.WriteLine($"    -> '{plyWritePath}'");
				Ply.Save(phys, plyWritePath);
			}
		}



		public static void MeshReadWrite(string path)
		{
			var name = Path.GetFileName(path);
			var romPath = PathHelper.GetRelativePath(StormworksPaths.rom, path);

			var nameWithoutExtension = Path.GetFileNameWithoutExtension(path);
			var ext = Path.GetExtension(path);

			var original = new MemoryStream();

			long length;
			using (var file = File.OpenRead(path))
			{
				length = file.Length;


				file.CopyTo(original);
			}

			original.Seek(0, SeekOrigin.Begin);

			var result = new MemoryStream();

			if (ext == ".mesh")
			{
				var mesh = Binary.LoadMesh(original, _meshDiag);

				Binary.Save(result, mesh);
			}
			else if (ext == ".phys")
			{
				var mesh = Binary.LoadPhys(original, _phyDiag);

				Binary.Save(result, mesh);
			}
			else
			{
				throw new NotImplementedException();
			}

			original.Seek(0, SeekOrigin.Begin);
			result.Seek(0, SeekOrigin.Begin);

			var compare = new StreamCompare();
			var match = compare.AreEqualAsync(original, result).Result;

			Console.WriteLine
				($"File: {romPath,-70} : {StringExt.PrintBytesReadable(length),10} -> {(match ? "Ok" : "Fail")}");
		}

		public static ulong DoFile(string path, bool export = false)
		{
			var name = Path.GetFileName(path);
			var romPath = PathHelper.GetRelativePath(StormworksPaths.rom, path);

			var nameWithoutExtension = PathHelper.GetFilePathWithoutExtension(path);
			var ext = Path.GetExtension(path);

			using (var file = File.OpenRead(path))
			{
				Console.WriteLine($"File: {romPath,-77} ___ { ("size"),-13} : {StringExt.PrintBytesReadable((long) file.Length)}");

				if (ext == ".mesh")
				{
					var mesh = Binary.LoadMesh(file, _meshDiag);
					mesh.fileName = romPath;
					__meshes.Add(mesh);
					foreach (var sm in mesh.subMeshes)
					{
						//shaders.IncrementOrCreateKey((UInt16)sm.shaderID);
						//Console.WriteLine(sm.ToHexDumpString());
					}

					if (export) Ply.Save(mesh, $"{nameWithoutExtension}.ply");
				}
				else if (ext == ".phys")
				{
					var mesh = Binary.LoadPhys(file, _phyDiag);
					mesh.fileName = romPath;

					__physes.Add(mesh);

					if(export) Ply.Save(mesh, $"{nameWithoutExtension}.ply");
				}
				else
				{
					throw new NotImplementedException();
				}

				return (ulong) file.Length;
			}
		}

		static Dictionary<UInt32, UInt32> shaders = new Dictionary<uint, uint>();
		static List<Mesh> __meshes = new List<Mesh>();
		static List<Phys> __physes = new List<Phys>();

		static void ReadAll()
		{
			var meshes = Directory.GetFiles(StormworksPaths.rom, "*.mesh", SearchOption.AllDirectories);
			var phys = Directory.GetFiles(StormworksPaths.rom,   "*.phys", SearchOption.AllDirectories);
			var both = meshes.Union(phys);

			//var paths = meshes;
			//var paths = phys;
			var paths = both;

			int fileCount = 0;
			ulong totalData = 0;

			var timer = new Stopwatch();
			timer.Start();

			foreach (string path in paths)
			{
				var fileName = Path.GetFileNameWithoutExtension(path);
				/*
				if(! (false 
				//   || Regex.IsMatch(fileName, "^arctic_tile_\\d\\d$")
				   || Regex.IsMatch(fileName, "^mega_island_\\d+_\\d+$")
				//   || path.Contains("arid_island_") 
				//   || path.Contains("arctic")
				//   || path.Contains("cloud")
				//   || path.Contains("tree")
				))
					continue;
				*/

				fileCount++;
				try
				{
					totalData += DoFile(path);
				}
				catch (Exception e)
				{
					Console.WriteLine(e.ToString().Pastel(Color.Red));
				}
				//Console.WriteLine();
			}

			timer.Stop();
			Console.WriteLine
				(
				 $"Processed {fileCount} files, together {StringExt.PrintBytesReadable(totalData)} in {timer.Elapsed}"
				);
		}

		static void AnalyzeMeshes(bool a_mesh, bool a_phys)
		{
			void PrintDIct<TKey>(Dictionary<TKey, UInt32> d, int max = 50)
			{
				if (d.Count == 0)
				{
					Console.WriteLine("\tEmpty");
					return;
				}
				int count = 0;
				UInt32 total = d.Values.Aggregate((accum, newVal) => accum + newVal);
				foreach (var kvp in d.AsEnumerable().OrderByDescending(kvp => kvp.Value))
				{
					if (count++ > max) break;

					if (typeof(TKey) == typeof(string))
					{
						Console.WriteLine($"\tVal: { $"'{kvp.Key}'",40} Count: {kvp.Value,6} / {total,6}");
					}
					else if (typeof(TKey) == typeof(Color))
					{
						var col = (Color) (object) kvp.Key; // Ew

						var rgbaValues = $"R{col.R,4} G{col.G,4} B{col.B,4} A{col.A,4}";

						var line = $"{$"{rgbaValues, 41}".PastelBg(col)} {FormatColorPastel(col)}";


						Console.WriteLine($"{line} Count: {kvp.Value,8} / {total,8}");
					}
					else Console.WriteLine($"\tVal: {kvp.Key,40} Count: {kvp.Value,6} / {total, 6}");
				}

				if (d.Count > max)
				{
					Console.WriteLine($"And {d.Count - max} more.".Pastel(Color.Yellow));
				}
			}

			if(a_mesh)
			{
				var allColors = new Dictionary<Color4, UInt32>();

				foreach (var mesh in __meshes)
				{
					var meshColors = new Dictionary<Color4, UInt32>();
					foreach (var record in mesh.vertices)
					{
						meshColors.IncrementOrCreateKey(record.color);
					}

					Console.WriteLine($"Unique Colors in mesh '{mesh.fileName}': ");
					PrintDIct(meshColors);

					allColors.MergeCountDictionary(meshColors);
				}

				Console.WriteLine($"Unique Colors over all {__meshes.Count} meshes: {allColors.Count}");
				PrintDIct(allColors, int.MaxValue);
			}

			if(a_mesh)
			{
				var mh0 = new Dictionary<UInt16, UInt32>();
				var mh1 = new Dictionary<UInt16, UInt32>();
				var mh3 = new Dictionary<UInt16, UInt32>();
				var mh4 = new Dictionary<UInt16, UInt32>();

				var sh2 = new Dictionary<UInt16, UInt32>();
				var sh3 = new Dictionary<Shader, UInt32>();
				var sh4 = new Dictionary<Vector3, UInt32>();
				var sh5 = new Dictionary<Vector3, UInt32>();
				var sh6 = new Dictionary<UInt16, UInt32>();
				var sh7 = new Dictionary<string, UInt32>();
				var sh8 = new Dictionary<Vector3, UInt32>();


				foreach (var mesh in __meshes)
				{
					mh0.IncrementOrCreateKey(mesh.header0);
					mh1.IncrementOrCreateKey(mesh.header1);
					mh3.IncrementOrCreateKey(mesh.header3);
					mh4.IncrementOrCreateKey(mesh.header4);
					foreach (var sm in mesh.subMeshes)
					{
						sh2.IncrementOrCreateKey(sm.header2);
						sh3.IncrementOrCreateKey(sm.shaderId);
						sh4.IncrementOrCreateKey(sm.bounds.Min);
						sh5.IncrementOrCreateKey(sm.bounds.Max);
						sh6.IncrementOrCreateKey(sm.header6);
						sh7.IncrementOrCreateKey(sm.name);
						sh8.IncrementOrCreateKey(sm.header8);
					}
				}

				Console.WriteLine($"mh0 (unknown):");
				PrintDIct(mh0);
				Console.WriteLine($"mh1 (unknown):");
				PrintDIct(mh1);
				Console.WriteLine($"mh2 (VertexCount) - Skipped");
				Console.WriteLine($"mh3 (unknown):");
				PrintDIct(mh3);
				Console.WriteLine($"mh4 (unknown):");
				PrintDIct(mh4);

				Console.WriteLine($"sh0 (indexBufferStart) - Skipped");
				Console.WriteLine($"sh1 (indexBufferLength) - Skipped");
				Console.WriteLine($"sh2 (unknown):");
				PrintDIct(sh2);
				Console.WriteLine($"sh3 (shaderID):");
				PrintDIct(sh3);
				Console.WriteLine($"sh2 (unknown):");
				PrintDIct(sh4);
				Console.WriteLine($"sh4 (boundsMin) - Skipped");
				//PrintDIct(sh5);
				Console.WriteLine($"sh5 (boundsMax) - Skipped");
				Console.WriteLine($"sh6 (unknown):");
				PrintDIct(sh6);
				Console.WriteLine($"sh7 (name):");
				PrintDIct(sh7);
				Console.WriteLine($"sh8 (unknown):");
				PrintDIct(sh8);
			}

			if(a_phys)
			{
				var ph0 = new Dictionary<UInt16, UInt32>();
				var ph1 = new Dictionary<int, UInt32>();
				var psh2 = new Dictionary<int, UInt32>();

				var withIndices = new HashSet<string>();

				foreach (Phys phy in __physes)
				{
					ph0.IncrementOrCreateKey(phy.header0);
					ph1.IncrementOrCreateKey(phy.subMeshCount);

					foreach (PhySubMesh subMesh in phy.subMeshes)
					{
						psh2.IncrementOrCreateKey(subMesh.indexCount);

						if (subMesh.indexCount > 0)
						{
							withIndices.Add(phy.fileName);
						}
					}
				}

				Console.WriteLine($"ph0 (unknown):");
				PrintDIct(ph0);
				Console.WriteLine($"ph1 (subMeshCount):");
				PrintDIct(ph1);
				Console.WriteLine($"psh1 (indexCount):");
				PrintDIct(psh2);
				Console.WriteLine($"phys with indices: ");
				foreach (string withIndex in withIndices)
				{
					Console.WriteLine($"    {withIndex}");
				}
			}
		}


		static void InitDistinctValues()
		{
			if (null != distinctValues)
			{
				foreach (var dictionary in distinctValues)
				{
					dictionary.Clear();
				}
			}
			else
			{
				distinctValues = new Dictionary<byte, int>[testLength];
				for (int i = 0; i < distinctValues.Length; i++)
				{
					distinctValues[i] = new Dictionary<byte, int>();
				}
			}
		}


		

		static void FindCommonSequences(Stream data, int resultCount = 30, int[] sequenceLengths = null)
		{
			if (null == sequenceLengths)
				sequenceLengths = new int[] { 2, 3, 4, 5, 6 };

			var occurrences = new Dictionary<byte[], uint>(new ByteArrayComparer());

			foreach (int sequenceLength in sequenceLengths)
			{
				var current = new byte[sequenceLength];

				data.Read(current, 0, sequenceLength);
				data.Seek(1, SeekOrigin.Begin);

				occurrences.IncrementOrCreateKey(current);

				for (int i = 0; i < data.Length - sequenceLength; i++)
				{
					// We have to create a new one, because else all the dict keys are referenceEquals.
					var newArr = new byte[sequenceLength];

					for (int j = 0; j < sequenceLength - 1; j++)
					{
						// Shift content one over
						newArr[j] = current[j + 1];
					}

					// Read the empty spot from file
					newArr[sequenceLength - 1] = (byte) data.ReadByte();

					current = newArr;
					occurrences.IncrementOrCreateKey(current);
				}
			}

			var sorted = occurrences.ToList();
			sorted.Sort((kvp1, kvp2) => kvp1.Value.CompareTo(kvp2.Value));
			sorted.Reverse();
			resultCount = Math.Min(sorted.Count, resultCount);

			var duplicationCount = new Dictionary<uint, uint>();

			for (int i = 0; i < resultCount; i++)
			{
				var kvp = sorted[i];
				var arr = kvp.Key;
				var count = kvp.Value;
				if (count <= 1) continue;

				Console.Write(FormatBytesRow(arr));
				Console.Write(" >>> Occurs: " + count.ToString().PadLeft(8));

				duplicationCount.IncrementOrCreateKey(count);

				Console.WriteLine();
			}

			var totalRecords = data.Length / sequenceLengths[0]; // todo: depends on actual sequenceLength of the record
			Console.WriteLine(totalRecords);
			uint totalDuplicatedRecords = 0;
			foreach (var kvp in duplicationCount)
			{
				var amountOfDuplicates = kvp.Key;
				var amountOfThatNumberOfDuplicates = kvp.Value;

				Console.WriteLine($"{amountOfDuplicates,4} duplicates: {amountOfThatNumberOfDuplicates,5} ==> {(float)amountOfThatNumberOfDuplicates / (float)totalRecords * 100f}%");

				totalDuplicatedRecords += amountOfThatNumberOfDuplicates;
			}

			Console.WriteLine($"Total duplicated records: {totalDuplicatedRecords,5} ==> {(float)totalDuplicatedRecords / (float)totalRecords * 100f}%");

			data.Seek(0, SeekOrigin.Begin);
		}

		static void FindEndCommonality(SeekOrigin origin)
		{
			if (origin == SeekOrigin.Current) throw new ArgumentOutOfRangeException(nameof(origin), "Only " + SeekOrigin.Begin + " or " + SeekOrigin.End);

			var offset = 0;

			if (origin == SeekOrigin.End) offset = -testLength;

			InitDistinctValues();

			//var files = Directory.GetFiles(meshFolderPath, "*.mesh");
			var files = Directory.GetFiles(meshFolderPath, "*.mesh", SearchOption.AllDirectories);

			foreach (string path in files)
			{
				var name = Path.GetFileNameWithoutExtension(path);
				var file = File.OpenRead(path);

				var thisHeader = new byte[testLength];

				file.Seek(offset, origin);
				var readBytesCount = file.Read(thisHeader, 0, testLength);
				file.Close();

				if (readBytesCount != testLength)
					continue;

				int i;
				for (i = 0; i < testLength; i++)
				{
					distinctValues[i].IncrementOrCreateKey(thisHeader[i]);
				}
				Console.WriteLine(FormatBytesRow(thisHeader, header: "", format: ByteRowFormat.Commonality) + name.PadRight(0));
			}

			{
				var sorted = new List<byte>[testLength];
				for (int i = 0; i < sorted.Length; i++)
				{
					sorted[i] = distinctValues[i].Keys.ToList();

					sorted[i].Sort(
						(b1, b2) =>
						{
							var v1 = distinctValues[i][b1];
							var v2 = distinctValues[i][b2];
							var r1 = v1.CompareTo(v2);
							// if r1 is equal, compare the bytes themselves.
							//if (r1 != 0) return r1; else
							return b1.CompareTo(b2);
						});
				}

				sorted.Reverse();
				// we'll evaluate in reverse order.
				// Let's hope the don't try to be smart and secretly just set a flag
				// Killing the performance because of all the shifting around of elements.


				var sb = new StringBuilder();
				bool any = true;
				while (any)
				{
					any = false;
					for (int i = 0; i < sorted.Length; i++)
					{
						var list = sorted[i];
						if (list.Any())
						{
							any = true;
							var theByte = list.Last();
							list.RemoveAt(list.Count - 1);

							var color = Color.DarkGray;

							var noDistinctValues = distinctValues[i].Count;
							if (noDistinctValues > 1)
							{
								color = Color.DarkKhaki;

								if (noDistinctValues > 8)
								{
									color = Color.Orange;
									if (noDistinctValues > 32)
									{
										color = Color.Red;
									}
								}
							}

							sb.Append(string.Format((string) "{0:x2} ", (object) theByte).Pastel(color));
						}
						else
						{
							sb.Append("   ");
						}
					}
					sb.AppendLine();
				}
				Console.Write(sb.ToString());
			}
		}


	}
}
