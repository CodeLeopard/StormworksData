﻿// Copyright 2023-2024 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using BinaryDataModel;
using BinaryDataModel.Converters;
using BinaryDataModel.DataTypes;
using InteractivePromptLib;
using OpenToolkit.Mathematics;
using Pastel;
using Shared;
using Shared.Serialization;
using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace CommandLine.CommandImplementations
{
	internal static class MeshConversion
	{
		static MeshConversion()
		{
			StormworksPaths.InstallChanged += StormworksPaths_InstallChanged;
		}


		internal static void StormworksPaths_InstallChanged()
		{
			LoadSettings();
		}

		internal static void LoadSettings()
		{
			string path = $"{StormworksPaths.CreatorToolKitData.setttings}/plyConverter.xml";

			if (!File.Exists(path))
			{
				return;
			}

			try
			{
				var settings = SerializationHelper.Pure.LoadFromFile<Ply.ConverterSettings>(path);
				Ply.ConverterSettings.Settings = settings;
				Console.WriteLine($"Loaded .Ply converter settings from: {path}.".Pastel(Color.DarkGray));
				Console.WriteLine(Ply.ConverterSettings.Settings.ToString().Pastel(Color.DarkGray));
			}
			catch (Exception e)
			{
				Console.WriteLine($"Failed to load .ply Converter settings from '{path}'.".Pastel(Color.Red));
				Console.WriteLine(e.ToString().Pastel(Color.Red));
			}
		}


		[InteractiveCommand(oneLineHelp: "Shows the currently applied settings for .ply file conversions.")]
		internal static void PlyConverterSettings()
		{
			Console.WriteLine(Ply.ConverterSettings.Settings);
		}

		[InteractiveCommand(oneLineHelp: "Change settings for .ply file conversions.")]
		internal static void SetPlyConverterSettings(
			[InteractiveCommandParameter(oneLineHelp: "Reverse Triangles, typically combined with reverseNormals.")]
			bool reverseTriangles,

			[InteractiveCommandParameter(oneLineHelp: "Reverse normals, typically combined with reverseTriangles.")]
			bool reverseNormals,

			[InteractiveCommandParameter(
				oneLineHelp: "Apply transformation for Blender workflow (Z-up, Y-forward).",
				longHelp: "Transformation is applied on load and save to .ply so that .mesh and .phys have Stormworks convention.")]
			bool blenderConvention,
			bool mirrorX,
			bool mirrorY,
			bool mirrorZ,
			[InteractiveCommandParameter(oneLineHelp: "Save the settings to a file in the current StormworksInstall, in the CreatorToolkitData folder (default: yes).")]
			bool save = true)
		{
			var settings = Ply.ConverterSettings.Settings;
			settings.ReverseTriangles = reverseTriangles;
			settings.ReverseNormals = reverseNormals;
			settings.TransformUpConvention = blenderConvention;
			settings.MirrorX = mirrorX;
			settings.MirrorY = mirrorY;
			settings.MirrorZ = mirrorZ;

			if (save)
			{
				string path = $"{StormworksPaths.CreatorToolKitData.setttings}/plyConverter.xml";

				var file = new FileInfo(path);
				if (file.Exists)
				{
					file.MoveTo($"{path}.old", true); 
				}

				var dir = file.Directory;
				dir.Create();
				SerializationHelper.Pure.SaveToFile(settings, path);

				Console.WriteLine($"Saved Ply Converter Settings to '{path}'.");
			}
			else
			{
				Console.WriteLine("Applied temporary Ply Converter Settings:");
			}

			Console.WriteLine(Ply.ConverterSettings.Settings);
		}

		/// <summary>
		/// Abstraction that implements wildcard handling (operating on multiple files from one command).
		/// </summary>
		/// <typeparam name="TData"></typeparam>
		/// <param name="loader"></param>
		/// <param name="meta">Returns a string with metadata about the object.</param>
		/// <param name="saver"></param>
		/// <param name="sourceExtension"></param>
		/// <param name="destExtension"></param>
		/// <param name="sourcePath"></param>
		/// <param name="destPath"></param>
		/// <param name="stopOnFirstError"></param>
		/// <param name="isRecursing"></param>
		/// <param name="excludeFileNameRegex"></param>
		/// <returns></returns>
		/// <exception cref="FileNotFoundException"></exception>
		/// <exception cref="InvalidOperationException"></exception>
		/// <exception cref="ArgumentException"></exception>
		private static bool Convert_A_to_B_Abstraction<TData>
		(
			Func<string, TData> loader,
			Func<TData, string> meta,
			Action<string, TData> saver,
			string sourceExtension,
			string destExtension,

			string sourcePath,
			string destPath = null,
			bool stopOnFirstError = true,
			bool isRecursing = false,
			Regex excludeFileNameRegex = null
		) where TData : class
		{
			string[] sourceFiles;
			if (Path.IsPathRooted(sourcePath))
			{
				sourceFiles = Directory.GetFiles(Path.GetDirectoryName(sourcePath), Path.GetFileName(sourcePath));
			}
			else
			{
				sourceFiles = Directory.GetFiles(Directory.GetCurrentDirectory(), sourcePath);
			}

			if (sourceFiles.Length == 0)
			{
				throw new FileNotFoundException("No files matched the specified pattern.", sourcePath);
			}
			if (sourceFiles.Length > 1)
			{
				if (isRecursing)
					throw new InvalidOperationException("Received a source file pattern that matched multiple files but expected an unambiguous path.");

				return Convert_A_to_B_Abstraction_Recursing(loader, meta, saver, sourceExtension, destExtension, sourceFiles, destPath, stopOnFirstError: stopOnFirstError, excludeFileNameRegex);
			}

			return Convert_A_to_B_Abstraction_Single(loader, meta, saver, sourceExtension, destExtension, sourceFiles[0], destPath, stopOnFirstError: stopOnFirstError, isRecursing: isRecursing);
		}

		private static bool Convert_A_to_B_Abstraction_Recursing<TData>
		(
			Func<string, TData> loader,
			Func<TData, string> meta,
			Action<string, TData> saver,
			string sourceExtension,
			string destExtension,

			string[] sourceFiles,
			string destPath = null,
			bool stopOnFirstError = true,
			Regex excludeFileNameRegex = null
		) where TData : class
		{
			if (!string.IsNullOrWhiteSpace(destPath) && !(destPath.EndsWith('/') || destPath.EndsWith('\\')))
			{
				// DestPath must be a folder or all matches would write to the same file.
				throw new ArgumentException("When using a wildcard path the destination must be a folder (path ending with `/`).", nameof(destPath));
			}

			foreach (string sourcePath1 in sourceFiles)
			{
				if (excludeFileNameRegex != null && excludeFileNameRegex.IsMatch(sourcePath1))
				{
					// Multiple .ply files are used to represent the subMeshes of a single .mesh file.
					// Those subFiles need to be excluded: only the primary file should be included.
					// The regex (if provided), should match files to exclude.
					continue;
				}
				string relSourcePath = Path.GetRelativePath(Directory.GetCurrentDirectory(), sourcePath1);

				bool mayContinue = Convert_A_to_B_Abstraction<TData>(loader, meta, saver, sourceExtension, destExtension, relSourcePath, destPath, isRecursing: true, stopOnFirstError: stopOnFirstError);

				if (mayContinue) continue;

				Console.WriteLine("Stopping conversion due to error.");
				return false;
			}

			return true;
		}

		private static bool Convert_A_to_B_Abstraction_Single<TData>
		(
			Func<string, TData> loader,
			Func<TData, string> meta,
			Action<string, TData> saver,
			string sourceExtension,
			string destExtension,

			string sourcePath,
			string destPath = null,
			bool stopOnFirstError = true,
			bool isRecursing = false
		) where TData : class
		{
			TData source;
			try
			{
				source = loader(sourcePath);

				string name = isRecursing ? sourcePath : sourceExtension;
				Console.WriteLine($"Loaded {name}{meta?.Invoke(source) ?? ""}");
			}
			catch (Exception e)
			{
				Console.WriteLine(e.ToString().Pastel(Color.Orange));
				Console.WriteLine();
				Console.WriteLine("No changes ware made.".Pastel(Color.Orange));

				return !stopOnFirstError;
			}

			if (string.IsNullOrWhiteSpace(destPath))
			{
				destPath = PathHelper.GetFilePathWithoutExtension(sourcePath) + destExtension;

				Console.WriteLine($"Inferring {nameof(destPath)}: '{destPath}'");
			}

			// if destPath is a folder then add the name of the file to it.
			if (destPath.EndsWith('/') || destPath.EndsWith('\\'))
			{
				destPath += Path.GetFileNameWithoutExtension(sourcePath) + destExtension;
			}

			try
			{
				saver(destPath, source);
				Console.WriteLine("Ok".Pastel(Color.Green));
			}
			catch (Exception e)
			{
				Console.WriteLine(e.ToString().Pastel(Color.Red));
				Console.WriteLine();
				Console.WriteLine("The operation failed".Pastel(Color.Red));

				return !stopOnFirstError;
			}

			return true;
		}


		[InteractiveCommand(oneLineHelp: "Convert a .mesh to .ply", aliases: new string[] { "Mesh2Ply" },
			longHelp: "Convert a .mesh to .ply format.")]
		internal static void ConvertMeshToPly(
			[InteractiveCommandStringParameter(
				oneLineHelp:"Absolute or relative path of the source .mesh",
				encapsulationCharacters:"\"'")]
			string sourcePath,

			[InteractiveCommandStringParameter(
				oneLineHelp:"Optional, Absolute or relative path of the destination .ply",
				encapsulationCharacters: "\"'")]
			string destPath = null,

			[InteractiveCommandParameter(
				oneLineHelp: "Stop conversion on first file that causes an error (default: true).")]
			bool stopOnFirstError = true
			)
		{
			if (sourcePath == "*") sourcePath = "*.mesh";
			Convert_A_to_B_Abstraction(Loader, Meta, Saver, ".mesh", ".ply", sourcePath, destPath: destPath, stopOnFirstError: stopOnFirstError);

			Mesh Loader(string mySourcePath)
			{
				return Binary.LoadMesh(mySourcePath);
			}

			string Meta(Mesh mesh)
			{
				return
					$" with {mesh.vertexCount} vertices, {mesh.indexCount} indices, {mesh.SubMeshCount} subMeshes.";
			}

			void Saver(string myDestPath, Mesh mesh)
			{
				Ply.Save(mesh, myDestPath);
			}
		}

		[InteractiveCommand(
			oneLineHelp: "Convert a .phys to .ply",
			aliases: new string[] { "Phys2Ply" },
			longHelp: "Convert a .phys to .ply format.\nAlso creates triangle indices if the .phys uses vertex order to define triangles.")]
		internal static void ConvertPhysToPly(
			[InteractiveCommandStringParameter(
				oneLineHelp: "Absolute or relative path of the source .phys",
				encapsulationCharacters: "\"'")]
			string sourcePath,

			[InteractiveCommandStringParameter(
				oneLineHelp: "Optional, Absolute or relative path of the destination .ply",
				encapsulationCharacters: "\"'")]
			string destPath = null,

			[InteractiveCommandParameter(oneLineHelp: "Stop conversion on first file that causes an error (default: true).")]
			bool stopOnFirstError = true)
		{
			if (sourcePath == "*") sourcePath = "*.phys";
			Convert_A_to_B_Abstraction(Loader, Meta, Saver, ".phys", ".ply", sourcePath, destPath: destPath, stopOnFirstError: stopOnFirstError);

			Phys Loader(string mySourcePath)
			{
				return Binary.LoadPhys(mySourcePath);
			}

			string Meta(Phys phys)
			{
				return $" with {phys.subMeshCount} subMeshes, with {phys.subMeshes.Select(sm => sm.vertices.Count).Sum()} vertices total.";
			}

			void Saver(string myDestPath, Phys phys)
			{
				Ply.Save(phys, myDestPath);
			}
		}

		const string submeshExplanation = @"
Supports SubMeshes by using multiple files and a naming convention:
Place one or multiple 0 just before the file extension, also separated by a dot: 'folder/my_file.ply' --> 'folder/my_file.0.ply'.
The converter will then search for additional files that have the same name but different numbers and use each as a subMesh in the final result ('folder/my_file.1.ply' ... 'folder/my_file.999.ply').
This can be combind with a wildcard search, but beware to not match the number in your wildcard search: 'folder/my_file*.0.ply' is okay, but 'folder/my_file*.ply' will also match 'folder/my_file.1.ply' and cause problems.
";
		const string shaderExplanation = @"
Supports Shaders by adding a comment at the start of the .ply file, or in the file name.
In a comment:
    comment Shader: SHADER_VALUE
    Where SHADER_VALUE is one of: Opaque, 0, Transparent, 1, Emissive, 2, Lava, 3
    Both the number or name is accepted. Note that the line with the shader comment should appear after the line 'format ascii 1.0' but before any line that does not start with 'comment'.
In the file name: 
    Put the shader id (number only, see above) in the file name behind '.S' so for example: 'my_file.ply' with shader Transparent (1) would become: 'my_file.S1.ply'.
If both ways are used then the file name method has priority, you will see a warning when this happens.

If you have both submeshes and shaders then you must put the submesh number last.
For example 'my_mesh_opaque_parts.S0.000.ply', 'my_mesh_transparent_parts.S1.001.ply', 'my_mesh_emissive_parts.S2.002.ply', etc.
";

		[InteractiveCommand(
			oneLineHelp: "Convert a .ply to .mesh",
			aliases: new string[] { "Ply2Mesh" },
			longHelp: "Convert one or multiple .ply (Ascii 1.0) to .mesh format.\n" + submeshExplanation + shaderExplanation)]
		internal static void ConvertPlyToMesh(
			[InteractiveCommandStringParameter(
				oneLineHelp: "Absolute path, relative path or search pattern (* or ? wildcards) to the source .ply",
				encapsulationCharacters:"\"'")]
			string sourcePath,

			[InteractiveCommandStringParameter(
				oneLineHelp: "Optional, Absolute or relative path of the destination .mesh",
				encapsulationCharacters: "\"'")]
			string destPath = null,

			[InteractiveCommandParameter(
				oneLineHelp: "When converting multiple files using wildcard: Stop conversion on first file that causes an error (default: true).")]
			bool stopOnFirstError = true
			)
		{
			if (sourcePath == "*") sourcePath = "*.ply";
			Convert_A_to_B_Abstraction(Loader, Meta, Saver, ".ply", ".mesh", sourcePath, destPath: destPath, stopOnFirstError: stopOnFirstError, excludeFileNameRegex: Ply.MultiFileAfterRegex);

			Mesh Loader(string mySourcePath)
			{
				return Ply.LoadMesh(mySourcePath, writeFileNames: true);
			}

			string Meta(Mesh mesh)
			{
				var sb = new StringBuilder();
				sb.Append($" with {mesh.vertexCount} vertices, {mesh.indexCount} indices, {mesh.SubMeshCount} subMeshes total.");

				sb.Append("\n    ### Shader        Name");
				foreach (var sm in mesh.subMeshes.EnumerateWithIndices())
				{
					sb.Append($"\n    {sm.index,3} {sm.item.shaderId,-13} {sm.item.name}");
				}
				return sb.ToString();
			}

			void Saver(string myDestPath, Mesh mesh)
			{
				Binary.Save(myDestPath, mesh);
			}
		}

		[InteractiveCommand(
			oneLineHelp: "Convert .ply to .phys",
			aliases: new string[] { "Ply2Phys" },
			longHelp: "Convert one or multiple .ply (Ascii 1.0) to a single .phys file.\n" + submeshExplanation)]
		internal static void ConvertPlyToPhys(
			[InteractiveCommandStringParameter(
				oneLineHelp: "Absolute path, relative path or search pattern (* or ? wildcards) to the source .ply",
				encapsulationCharacters:"\"'")]
			string sourcePath,

			[InteractiveCommandStringParameter(
				oneLineHelp: "Optional, Absolute or relative path of the destination .phys",
				encapsulationCharacters: "\"'")]
			string destPath = null,

			[InteractiveCommandParameter(
				oneLineHelp: "When converting multiple files using wildcard: Stop conversion on first file that causes an error (default: true).")]
			bool stopOnFirstError = true)
		{
			if (sourcePath == "*") sourcePath = "*.ply";
			Convert_A_to_B_Abstraction(Loader, Meta, Saver, ".ply", ".phys", sourcePath, destPath: destPath, stopOnFirstError: stopOnFirstError, excludeFileNameRegex: Ply.MultiFileAfterRegex);

			Phys Loader(string mySourcePath)
			{
				return Ply.LoadPhys(mySourcePath);
			}

			string Meta(Phys mesh)
			{
				return
					$" with {mesh.TotalVertices} vertices, {mesh.TotalIndices} indices, {mesh.subMeshCount} subMeshes.";
			}

			void Saver(string myDestPath, Phys mesh)
			{
				Binary.Save(myDestPath, mesh);
			}
		}

		[InteractiveCommand(
			oneLineHelp: "Scale a .phys",
			longHelp: "Scale a .phys into a new file. If only one scale component is supplied the scaling performed is uniform.")]
		internal static void ScalePhys(
			[InteractiveCommandStringParameter(
				oneLineHelp: "Absolute or relative path of the source .phys",
				encapsulationCharacters: "\"'")]
			string sourcePath,

			[InteractiveCommandStringParameter(
				oneLineHelp: "Optional, Absolute or relative path of the destination .phys",
				encapsulationCharacters: "\"'")]
			string destPath,

			[InteractiveCommandParameter(oneLineHelp: "The Uniform scale to apply (when no other components specified) or the X scale.")]
			float uniformOrXScale,

			[InteractiveCommandParameter(oneLineHelp: "The Y scale.")]
			float yScale = float.NaN,

			[InteractiveCommandParameter(oneLineHelp: "The Z scale.")]
			float zScale = float.NaN)
		{
			Phys source = null;
			Phys transformed = null;
			try
			{
				source = Binary.LoadPhys(sourcePath);

				Console.WriteLine($"Loaded .phys with {source.subMeshCount} subMeshes, with {source.subMeshes.Select(sm => sm.vertices.Count).Sum()} vertices total.");

				Vector3 scale = new Vector3();
				scale.X = uniformOrXScale;

				if (float.IsNaN(yScale))
				{
					scale.Y = uniformOrXScale;
				}
				else
				{
					scale.Y = yScale;
				}

				if (float.IsNaN(zScale))
				{
					scale.Z = uniformOrXScale;
				}
				else
				{
					scale.Z = zScale;
				}

				Console.WriteLine($"Applying scale transformation {scale}...");

				Matrix4 transformation = Matrix4.CreateScale(scale);

				transformed = MeshCombiner.Transform(source, transformation);
			}
			catch (Exception e)
			{
				Console.WriteLine(e.ToString().Pastel(Color.Orange));
				Console.WriteLine();
				Console.WriteLine("No changes ware made.".Pastel(Color.Orange));
				return;
			}


			try
			{
				using (var stream = File.Open(destPath, FileMode.Create))
				{
					Binary.Save(stream, transformed);
				}
				Console.WriteLine($"Result saved to {destPath}");
				Console.WriteLine("Ok".Pastel(Color.Green));
			}
			catch (Exception e)
			{
				Console.WriteLine(e.ToString().Pastel(Color.Red));
				Console.WriteLine();
				Console.WriteLine("The operation failed".Pastel(Color.Red));
			}
		}


		const string map_geom_bin_explanation = @"The map_geometry.bin files are used to show the map view (both in the UI and LUA monitors).
It consists several layers in two groups: the V layers and T layers.
The groups are separated by consisting of triangles or quads.
There are 11 V layers each representing a color.
  This layer must consist of triangles only!
  When converting from .ply the converter will find the layers based on the vertex color. You must use the exact colors or it will not work.
  The colors are:
    d0d0c5 light gray
    a4b874 grass
    e3d08c sand
    52b8d1 blue
    ffffff pure white
    8b6d5b red rock
    583e2c brown (dirt)
    52b8d1 blue (again)
    47a3b8 blue 1st darker shade
    3d8d9f blue 2nd darker shade
    327985 blue 3rd darker shade
  The layers will draw over each other in the game.

There are 10 T layers. These represent lines on the map.
  This layer must consist of quads only!
  While there are 10 layers there are only two textures used, they alternate.
  When converting from .ply the converter will find the layers based on the number in the blue channel (red and green must be 0):
    0 Solid line
    1 dashed line
    2 Solid line (again)
    ... This pattern repeats to make 10 layers (0 through 9).
  In the binary format the vertices have uv coordinates, specifying them is optional. If not specified the converter will generate them for you based on the order of the vertices.
  

For both groups: The height coordinate has no meaning. When converting to .ply the layers are separated to prevent z-fighting for convenience.
When converting to .bin the height is set to 0.
    (Y axis in the binary format, in the ply format the axis is subject to conversion settings.)

";


		[InteractiveCommand(
			oneLineHelp: "Convert .ply to (map_geometry) .bin",
			aliases: new string[] { "Ply2MapBin" },
			longHelp: "Convert .ply (Ascii 1.0) to (map_geometry) .bin format.\n" + map_geom_bin_explanation)]
		internal static void ConvertPlyToMapBin(
			[InteractiveCommandStringParameter(
				oneLineHelp: "Absolute path, relative path or search pattern (* or ? wildcards) to the source .ply",
				encapsulationCharacters:"\"'")]
			string sourcePath,

			[InteractiveCommandStringParameter(
				oneLineHelp: "Optional, Absolute or relative path of the destination .bin",
				encapsulationCharacters: "\"'")]
			string destPath = null,

			[InteractiveCommandParameter(
				oneLineHelp: "When converting multiple files using wildcard: Stop conversion on first file that causes an error (default: true).")]
			bool stopOnFirstError = true
			)
		{
			if (sourcePath == "*") sourcePath = "*.ply";
			Convert_A_to_B_Abstraction(Loader, Meta, Saver, ".ply", ".bin", sourcePath, destPath: destPath, stopOnFirstError: stopOnFirstError, excludeFileNameRegex: Ply.MultiFileAfterRegex);

			MapBin Loader(string mySourcePath)
			{
				return Ply.LoadMapBin(mySourcePath);
			}

			string Meta(MapBin mesh)
			{
				var sb = new StringBuilder();
				sb.Append($" with {mesh.vertexLayers.Select(l => l.Vertices.Count).Sum()} vertices, {mesh.textureLayers.Select(l => l.Vertices.Count).Sum()} texVertices.");
				return sb.ToString();
			}

			void Saver(string myDestPath, MapBin mesh)
			{
				Binary.Save(myDestPath, mesh);
			}
		}

		[InteractiveCommand(
			oneLineHelp: "Convert (map_geometry) .bin to .ply",
			aliases: new string[] { "MapBin2Ply" },
			longHelp: "Convert (map_geometry) .bin files to .ply.\n" + map_geom_bin_explanation)]
		internal static void ConvertMapBinToPly(
			[InteractiveCommandStringParameter(
				oneLineHelp: "Absolute path, relative path or search pattern (* or ? wildcards) to the source .bin",
				encapsulationCharacters:"\"'")]
			string sourcePath,

			[InteractiveCommandStringParameter(
				oneLineHelp: "Optional, Absolute or relative path of the destination .ply",
				encapsulationCharacters: "\"'")]
			string destPath = null,

			[InteractiveCommandParameter(
				oneLineHelp: "When converting multiple files using wildcard: Stop conversion on first file that causes an error (default: true).")]
			bool stopOnFirstError = true
			)
		{
			if (sourcePath == "*") sourcePath = "*.bin";
			Convert_A_to_B_Abstraction(Loader, Meta, Saver, ".bin", ".ply", sourcePath, destPath: destPath, stopOnFirstError: stopOnFirstError, excludeFileNameRegex: Ply.MultiFileAfterRegex);

			MapBin Loader(string mySourcePath)
			{
				return Binary.LoadMapBin(mySourcePath);
			}

			string Meta(MapBin mesh)
			{
				var sb = new StringBuilder();
				sb.Append($" with {mesh.vertexLayers.Select(l => l.Vertices.Count).Sum()} vertices, {mesh.textureLayers.Select(l => l.Vertices.Count).Sum()} texVertices.");
				return sb.ToString();
			}

			void Saver(string myDestPath, MapBin mesh)
			{
				Ply.Save(mesh, myDestPath);
			}
		}
	}
}
