﻿// Copyright 2023 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using BinaryDataModel;
using BinaryDataModel.Converters;
using DataModel.DataTypes;
using DataModel.Definitions.NestedTypes;
using InteractivePromptLib;
using OpenToolkit.Mathematics;
using Pastel;
using Shared;
using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml;

/*
Workflow
[ ] Split mesh into bits of < 8196 vertices.
[*] Offset each part by 0.25m * part_number
[*] Export to SW Mesh
[*] Create block definitions for the meshes.
[*] Create a vehicle containing the meshes.
-------------------------------------------
Input is a folder containing split but not-offset ply files.
For each file:
	Inport from ply
	Transform
	Save as mesh
	Create block definition
	Add component to vehicle

*/

namespace CommandLine.CommandImplementations
{
	internal static class SpecialOperations
	{
		const float offset = -0.25f;
		const string no_physics_definition_name = "no_physics";
		const string with_physics_balancer_definition = "01_block";

		[InteractiveCommand(
			oneLineHelp: "Generate a vehicle to be used as a LOD.",
			longHelp: "Generate a vehicle from a folder containing .ply files. Each file is converted and a block definition is generated for it. Those blocks are added to a vehicle (with appropriate offset baked into the mesh).",
			aliases: new[] { "ply2LODV", "ply2LODVehicle" }
			
			)]
		internal static void GenerateLODVehicle(
			[InteractiveCommandStringParameter(
			oneLineHelp: "The path containing the .ply files to process. The output mesh(es) will also end up in this folder. "
					   + "Quote the value if it contains spaces.",
			encapsulationCharacters: "\"'")]
			string inputFolder,

			[InteractiveCommandStringParameter(
			oneLineHelp: "The regex pattern to be used to extract the offset. "
					   + "Quote the value if it contains spaces.",
			longHelp: "The regex pattern to extract the offset. The pattern should contain one capture group that matches only a number: (\\d+)\nIf the pattern contains spaces it should be quoted.",
			encapsulationCharacters: "\"'")]
			string idPattern,

			[InteractiveCommandStringParameter(
			oneLineHelp: "The path where the output vehicle should be placed. "
					   + "Quote the value if it contains spaces.",
			encapsulationCharacters: "\"'")]
			string outputVehicle = null,

			[InteractiveCommandParameter(
				oneLineHelp: "Give the block definitions physics and surfaces (default: no).",
				longHelp: "By default the generated definitions don't have physics or surfaces. This means that you cannot use them in a vehicle because there is nothing to attach to. By enabling this there will be physics and surfaces so you can use it in manually edited vehicle."
			)]
			bool withPhysicsAndSurfaces = false
			)
		{
			var dir = new DirectoryInfo(inputFolder);
			if(! dir.Exists)
			{
				throw new ArgumentException($"Directory not found '{dir.FullName}'");
			}

			if (string.IsNullOrEmpty(outputVehicle))
			{
				outputVehicle = $"{StormworksPaths.Data.debris}/{dir.Name}";
			}

			var regex = new Regex(idPattern, RegexOptions.Compiled);

			var vehicle_raw = @"<?xml version=""1.0"" encoding=""UTF-8""?>
<vehicle data_version=""3"" bodies_id=""1"">
	<authors/>
	<bodies>
		<body unique_id=""1"">
			<components></components>
		</body>
	</bodies>
	<logic_node_links/>
</vehicle>";

			var vdoc = new XmlDocument();
			vdoc.LoadXml(vehicle_raw);

			var components = vdoc.SelectSingleNode("vehicle/bodies/body/components") ?? throw new Exception($"Error in hardcoded xml snipplet '{nameof(vehicle_raw)}'.");

			int counter = 0;
			foreach(var file in dir.GetFiles("*.ply", SearchOption.TopDirectoryOnly))
			{
				var match = regex.Match(file.Name);
				int id;
				try
				{
					id = int.Parse(match.Groups[1].Value);
				}
				catch(FormatException ex)
				{
					throw new ArgumentException($"Regex '{idPattern}' failed, expected input '{file.Name}' to result in a number but received '{match.Groups[1].Value}' (full match: '{match.Value}').", ex);
				}

				string meshFilePath = TransformAndSaveMesh(file, counter);

				string definitionName = Path.GetFileNameWithoutExtension(file.FullName);

				SaveDefinition(meshFilePath, definitionName, withPhysicsAndSurfaces);

				AddVehicleComponent(vdoc, components, counter, definitionName, withPhysicsAndSurfaces);

				counter++;

				file.Delete();
			}

			var writerSettings = new XmlWriterSettings();
			writerSettings.Indent = true;

			using(var stream = File.Open($"{outputVehicle}.xml", FileMode.Create))
			{
				vdoc.Save(stream);
			};

			EnsureNoPhysicsDefinition();

			Console.WriteLine("Done");
		}

		private static void AddCOMBalancerBlock(XmlDocument vdoc, XmlNode components, int id, bool withPhysicsAndSurfaces)
		{
			if (id == 0) return;

			var definition = withPhysicsAndSurfaces ? with_physics_balancer_definition : no_physics_definition_name;

			var newNodeXml =
			$@"
				<c d=""{definition}"">
					<o r=""1,0,0,0,1,0,0,0,1"" sc=""6"">
						<vp x=""{-id}""/>
					</o>
				</c>";
			var frag = vdoc.CreateDocumentFragment();
			frag.InnerXml = newNodeXml;
			components.AppendChild(frag);
		}

		private static void AddVehicleComponent(XmlDocument vdoc, XmlNode components, int id, string name, bool withPhysicsAndSurfaces)
		{
			var newNodeXml =
			$@"
				<c d=""{name}"">
					<o r=""1,0,0,0,1,0,0,0,1"" sc=""6"">
						<vp x=""{id}""/>
					</o>
				</c>";
			var frag = vdoc.CreateDocumentFragment();
			frag.InnerXml = newNodeXml;
			components.AppendChild(frag);

			AddCOMBalancerBlock(vdoc, components, id, withPhysicsAndSurfaces);
		}

		private static string TransformAndSaveMesh(FileInfo file, int id)
		{
			var transform2 = Matrix4.CreateTranslation(id * offset, 0, 0);
			var transform = Matrix4.Transpose(transform2); // todo: wtf?

			var input = Ply.LoadMesh(file.FullName, false);

			const int limit = 16_384;
			if (input.vertexCount > limit)
			{
				Console.Write($"Warning: Mesh {file.Name} has {input.vertexCount} vertices, this is higher than the limit imposed on meshes in a definition ({limit}).".Pastel(System.Drawing.Color.Orange));
			}

			var output = MeshCombiner.Transform(input, transform);

			var meshFilePath = Path.ChangeExtension(file.FullName, ".mesh");
			using (var stream = File.Open(meshFilePath, FileMode.Create))
			{
				Binary.Save(stream, output);
			}

			Console.WriteLine($"Saved Transformed mesh {meshFilePath}");
			return meshFilePath;
		}

		private static void SaveDefinition(string meshFilePath, string name, bool withPhysicsAndSurfaces)
		{
			string meshPath_defintion = Path.GetRelativePath(StormworksPaths.rom, meshFilePath).Replace("\\", "/");
			if (meshPath_defintion.Contains("..")) throw new ArgumentException("Something is wrong with the file paths, the mesh files appear to be in a different Stormworks installation then the definition files.");

			var category = DefinitionCategory.Decorative;
			var flags = DefinitionFlags.HiddenFromInventory;
			var voxelFlags = withPhysicsAndSurfaces ? Voxel.Flags.Physics : Voxel.Flags.Empty;

			const string surfacesAndBuoyancy = @"	<surfaces>
		<surface orientation=""4"" rotation=""0"" shape=""1"" trans_type=""0"">
			<position x=""0"" y=""0"" z=""0""/>
		</surface>
		<surface orientation=""5"" rotation=""0"" shape=""1"" trans_type=""0"">
			<position x=""0"" y=""0"" z=""0""/>
		</surface>
		<surface orientation=""2"" rotation=""0"" shape=""1"" trans_type=""0"">
			<position x=""0"" y=""0"" z=""0""/>
		</surface>
		<surface orientation=""3"" rotation=""0"" shape=""1"" trans_type=""0"">
			<position x=""0"" y=""0"" z=""0""/>
		</surface>
		<surface orientation=""1"" rotation=""0"" shape=""1"" trans_type=""0"">
			<position x=""0"" y=""0"" z=""0""/>
		</surface>
		<surface orientation=""0"" rotation=""0"" shape=""1"" trans_type=""0"">
			<position x=""0"" y=""0"" z=""0""/>
		</surface>
	</surfaces>
	<buoyancy_surfaces>
		<surface orientation=""5"" rotation=""0"" shape=""1"" trans_type=""0"">
			<position x=""0"" y=""0"" z=""0""/>
		</surface>
		<surface orientation=""1"" rotation=""0"" shape=""1"" trans_type=""0"">
			<position x=""0"" y=""0"" z=""0""/>
		</surface>
		<surface orientation=""4"" rotation=""0"" shape=""1"" trans_type=""0"">
			<position x=""0"" y=""0"" z=""0""/>
		</surface>
		<surface orientation=""2"" rotation=""0"" shape=""1"" trans_type=""0"">
			<position x=""0"" y=""0"" z=""0""/>
		</surface>
		<surface orientation=""0"" rotation=""0"" shape=""1"" trans_type=""0"">
			<position x=""0"" y=""0"" z=""0""/>
		</surface>
		<surface orientation=""3"" rotation=""0"" shape=""1"" trans_type=""0"">
			<position x=""0"" y=""0"" z=""0""/>
		</surface>
	</buoyancy_surfaces>";

			const string noSurfacesOrBuoyancy = @"	<surfaces/>
	<buoyancy_surfaces/>";
			var chosenSurfacesAndBuoyancy = withPhysicsAndSurfaces ? surfacesAndBuoyancy : noSurfacesOrBuoyancy;


			var definition = $@"<?xml version=""1.0"" encoding=""UTF-8""?>
<definition name=""{name}""
category=""{(uint)category}""
type=""9""
mass=""1""
value=""1""
flags=""{(uint)flags}""
tags=""""
mesh_data_name=""{meshPath_defintion}""
mesh_editor_only_name=""meshes/unit_sphere.mesh""
block_type=""0""
light_ies_map=""graphics/ies/ies_default.txtr""
>
{chosenSurfacesAndBuoyancy}
	<logic_nodes/>
	<voxels>
		<voxel flags=""{(uint)voxelFlags}"" >
			<position />
			<physics_shape_rotation />
		</voxel>
	</voxels>
	<tooltip_properties short_description=""Mesh used for Scuffed LOD system."" description=""LOD Hack Mesh."" />
</definition>
";
			string definition_path = $"{StormworksPaths.Data.definitions}/{name}.xml";

			using (var stream = File.Open(definition_path, FileMode.Create))
			{
				using (var textWriter = new StreamWriter(stream))
				{
					textWriter.Write(definition);
				}
			}

			Console.WriteLine($"Saved definition {definition_path}");
		}

		private static void EnsureNoPhysicsDefinition()
		{
			string definition_path = $"{StormworksPaths.Data.definitions}/{no_physics_definition_name}.xml";
			if (File.Exists(definition_path))
			{
				return;
			}

			var category = DefinitionCategory.Decorative;
			var flags = DefinitionFlags.HiddenFromInventory;
			var voxelFlags = Voxel.Flags.Empty;

			var definition = $@"<?xml version=""1.0"" encoding=""UTF-8""?>
<definition name=""{no_physics_definition_name}""
category=""{(uint)category}""
type=""9""
mass=""1""
value=""1""
flags=""{(uint)flags}""
tags=""""
mesh_data_name=""""
mesh_editor_only_name=""meshes/unit_sphere.mesh""
block_type=""0""
light_ies_map=""graphics/ies/ies_default.txtr""
>
	<surfaces/>
	<buoyancy_surfaces/>
	<logic_nodes/>
	<voxels>
		<voxel flags=""{(uint)voxelFlags}"" >
			<position />
			<physics_shape_rotation />
		</voxel>
	</voxels>
	<tooltip_properties short_description=""Mesh used for Scuffed LOD system."" description=""LOD Hack Mesh."" />
</definition>
";

			using (var stream = File.Open(definition_path, FileMode.Create))
			{
				using (var textWriter = new StreamWriter(stream))
				{
					textWriter.Write(definition);
				}
			}
		}
	}
}
