﻿// Copyright 2023 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using BinaryDataModel.Converters;
using BinaryDataModel.DataTypes;
using InteractivePromptLib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using WinClipboard;

namespace CommandLine.CommandImplementations
{
    internal static class GrabPointLocations
    {

        [InteractiveCommand(oneLineHelp: "Grabs vertex locations from .ply to put things on those.", aliases: new string[] { "Ply2Loc" },
            longHelp: "Takes the positions from a source .ply vertex cloud and puts those in a separate file, adding the prefix and suffix to each transform." +
            "This can then be pasted into tile xml files to for example automatically place lights along a tunnel.")]
        static void GrabLocationsFromVertexCloud(
            [InteractiveCommandStringParameter(
                oneLineHelp:"Absolute or relative path of the source .ply",
                encapsulationCharacters:"\"'")]
        string vertexCloudFilepath,
            [InteractiveCommandStringParameter(
                oneLineHelp:"What will be put in front of each transform line. Quote as '<prefix>'",
                encapsulationCharacters:"'")]
        string prefix,
            [InteractiveCommandStringParameter(
                oneLineHelp:"What will be pasted after each transform line. Quote as '<suffix>'",
                encapsulationCharacters:"'")]
        string suffix)
        {
            List<VertexRecord> vertices;
            vertices = Ply.LoadMesh(vertexCloudFilepath).vertices;

            var stringBuilder = new StringBuilder();

            var counter = 0;

            foreach(var vertex in vertices)
            {
                stringBuilder.AppendLine(prefix);
                stringBuilder.AppendLine($"<transform 30=\"{vertex.position.X}\" 31=\"{vertex.position.Y}\" 32=\"{vertex.position.Z}\"");
                stringBuilder.AppendLine(suffix);
                counter++;
            }

            Clipboard.SetText(stringBuilder.ToString());

            Console.WriteLine($"Put section with {counter} instances in the clipboard.");

        }
    }
}
