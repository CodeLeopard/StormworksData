﻿// Copyright 2023-2024 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using DataModel.Tiles;
using InteractivePromptLib;
using Pastel;
using Shared;
using Shared.Serialization;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;

using OpenToolkit.Mathematics;
using System.Text.RegularExpressions;

namespace CommandLine.CommandImplementations
{
	internal static class TrackExport
	{
		[InteractiveCommand(oneLineHelp: "Exports the track nodes in each Tile to lua files.")]
		internal static void ExportTrackNodesToLua(
			[InteractiveCommandStringParameter(encapsulationCharacters: "\"'")]
			string destinationFolder,
			[InteractiveCommandStringParameter(encapsulationCharacters: "\"'", nameOverride: "fileIncludeRegex")]
			string regex_str = null,
			float threshold = 1.5f
		)
		{
			if (Directory.Exists(destinationFolder))
			{
				//Directory.Delete(destinationFolder, true);
			}
			Directory.CreateDirectory(destinationFolder);
			Console.WriteLine($"Writing data to {destinationFolder}");

			var regex = regex_str != null ? new Regex(regex_str) : null;


			var files = Directory
				.EnumerateFiles(StormworksPaths.Data.tiles, "*.xml", SearchOption.TopDirectoryOnly)
				.Where(s => !Tile.InstancesRegex.IsMatch(s))
				;

			if (regex != null)
			{
				files = files.Where(s => regex.IsMatch(s));
			}

			var indexRegex = new Regex("(.*?)_(\\d+)_(\\d+)");

			files = files.OrderBy(fp =>
			{
				try
				{
					var m = indexRegex.Match(fp);

					if (m.Success)
					{
						int a = int.Parse(m.Groups[2].Value);
						int b = int.Parse(m.Groups[3].Value);
						var adjusted = $"{m.Groups[1].Value}_{a:D2}_{b:D2}";
						return adjusted;
					}
				}
				catch
				{

				}
				return fp;
			});

			var sb = new StringBuilder();
			var initFileSB = new StringBuilder();
			initFileSB.Append(initFileHeader);
			void AddInitFileEntry(string name)
			{
				initFileSB.Append($"\n\t'{name}',");
			}

			uint counter_loaded = 0;
			uint counter_removed = 0;

			foreach (var filePath in files)
			{
				DoTile(filePath);
			}
			void DoTile(string filePath)
			{
				var fileName = Path.GetFileNameWithoutExtension(filePath);
				XDocument doc;
				try
				{
					doc = XMLHelper.LoadFromFile(filePath);
				}
				catch (Exception e)
				{
					Console.WriteLine
						(
						 $"Error loading tile '{fileName}' from file '{filePath}' during XML loading."
					   + $" The tile will be skipped. Full Exception details:\n{e.ToString().Pastel(Color.Red)}"
						);
					return;
				}

				var tileRomPath = PathHelper.GetRelativePath(StormworksPaths.rom, filePath);


				Tile tile;
				try
				{
					tile = new Tile(doc.Element("definition"), tileRomPath);
				}
				catch (Exception e)
				{
					Console.WriteLine
						(
						 $"Error loading tile '{fileName}' from file '{filePath}' during Tile Representation loading."
					   + $" The tile will be skipped. Full Exception details:\n{e.ToString().Pastel(Color.Red)}"
						);
					return;
				}

				if (!tile.trackNodes.Any()) return;

				try
				{
					Console.WriteLine($"Working on {fileName}");

					var debug_names = new string[] { "track_bridge_straight_horizontal" };
					if (debug_names.Contains(fileName))
					{
						// Put breakpoint here.
					}

					counter_loaded += (uint)tile.trackNodes.LongCount();
					counter_removed += OptimizedGraph(tile.trackNodes, threshold);

					Export(sb, tile);

					string luaFilePath = Path.Combine(destinationFolder, Path.ChangeExtension(fileName, ".lua"));
					using var luaFile = File.Open(luaFilePath, FileMode.Create);
					var writer = new StreamWriter(luaFile);
					writer.AutoFlush = false;
					writer.Write(sb.ToString());
					sb.Clear();
					writer.Flush();

					AddInitFileEntry(fileName);
				}
				catch (Exception e)
				{
					Console.WriteLine
						(
						 $"Error working on tile '{fileName}' from file '{filePath}'."
					   + $" The tile will be skipped. Full Exception details:\n{e.ToString().Pastel(Color.Red)}"
						);
					return;
				}
			}

			Console.WriteLine($"Loaded    {counter_loaded,6} nodes.");
			Console.WriteLine($"Optimized {counter_removed,6} nodes away.");
			Console.WriteLine($"Saved     {(counter_loaded - counter_removed),6} nodes.");

			{
				initFileSB.AppendLine(initFileTrailer);
				string filePath = Path.Combine(destinationFolder, "init.lua");
				using var luaFile = File.Open(filePath, FileMode.Create);
				var writer = new StreamWriter(luaFile);
				writer.AutoFlush = false;
				writer.Write(initFileSB.ToString());
				writer.Flush();
			}
		}


		const string initFileHeader =
			"local names = {";

		const string initFileTrailer =
			"\n}\n\n" +
			$"---@type table<string, {luaAnnotationTypeName}>\n" +
			"local tiles = {}\n\n" +
			"for _, name in ipairs(names) do\n" +
			"\tlocal tile = require(\"common.mod_objects.trainTrack.content.nso.tracks.\"..name)\n" +
			"\ttiles[tile.tile_name] = tile\n" +
			"end\n\n" +
			"return tiles";

		const string luaAnnotationTypeName = "TrainTrackData.Tile.WithLinkProperties";

		private static void Export(StringBuilder sb, Tile tile)
		{
			sb.Clear();
			sb.AppendLine($"---@type {luaAnnotationTypeName}");
			sb.AppendLine("local data = {");
			sb.AppendLine($"\ttile_name = '{tile.tileName}',");

			sb.AppendLine("\ttrain_tracks = {");
			foreach (var node in tile.trackNodes.OrderBy(n => n.id))
			{
				sb.AppendLine("\t\t{");

				sb.AppendLine($"\t\t\tid = '{node.id}',");
				var v = node.GTransform.Row3;
				sb.AppendLine($"\t\t\tposition = {{ x = {v.X:0.###}, y = {v.Y:0.###}, z = {v.Z:0.###} }},");

				sb.AppendLine("\t\t\tlinks = {");
				foreach (var link in node.TrackLinks.Values.OrderBy(l => l.toNodeId))
				{
					sb.AppendLine( "\t\t\t\t{");
					sb.AppendLine($"\t\t\t\t\tlink = '{link.toNodeId}',");
					if (link.TrackSpeed_ms.HasValue)
					{
						sb.AppendLine($"\t\t\t\t\ttrackSpeed_ms = {link.TrackSpeed_ms.Value},");
					}
					if (link.SuperElevation_mm.HasValue)
					{
						sb.AppendLine($"\t\t\t\t\tsuperElevation_mm = {link.SuperElevation_mm.Value},");
					}
					if (link.TrackName != null)
					{
						sb.AppendLine($"\t\t\t\t\ttrackName = '{link.TrackName}',");
					}
					if (link.CatenarySpec != null)
					{
						sb.AppendLine($"\t\t\t\t\tcatenarySpec = '{link.CatenarySpec}',");
					}
					sb.AppendLine("\t\t\t\t},");
				}
				sb.AppendLine("\t\t\t},");

				sb.AppendLine("\t\t},");

			}
			sb.AppendLine("\t},");

			sb.AppendLine("}");
			sb.AppendLine("return data");
		}


		private static uint OptimizedGraph(IEnumerable<TrainTrackNode> nodes, float threshold)
		{
			var beforeCount = nodes.Count();

			if (beforeCount > 500)
			{

			}

			// Find nodes that don't have exactly two links
			HashSet<TrainTrackNode> nonTwoLinkNodes = nodes.Where(node => node.TrackLinks.Count != 2).ToHashSet();

			var allPathStarts = new HashSet<PathEntry>(new PathEntryEqualityComparer());

			foreach (var node in nonTwoLinkNodes)
			{
				var paths = PathsFrom(node);

				foreach (var item in paths)
				{
					// The equality comparer will chuck out paths we already found.
					// Yes this is wasteful. No it will not matter.
					allPathStarts.Add(item);
				}
			}

			foreach (var pathStart in allPathStarts)
			{
				OptimizePath(pathStart, threshold);
			}

			var missing = new HashSet<string>();
			var noLinks = new HashSet<string>();
			// Sanity check: all referneced nodes should exist!
			foreach (var node in nodes)
			{
				foreach(var link in node.LinkIds)
				{
					bool ok = false;
					foreach(var checkNode in nodes)
					{
						if (checkNode.id == link)
						{
							ok = true;
							break;
						}
					}
					if (!ok)
					{
						missing.Add(link);
					}
				}

				if (node.TrackLinks.Count == 0)
				{
					noLinks.Add(node.id);
				}
			}
			foreach(var id in missing)
			{
				Console.WriteLine($"Missing: {id}".Pastel(Color.Orange));
			}
			foreach (var id in noLinks)
			{
				Console.WriteLine($"Node with no links: {id}".Pastel(Color.Orange));
			}

			var afterCount = nodes.Count();

			if (beforeCount > 0)
				Console.WriteLine($"Optimized {beforeCount,4} nodes down to {afterCount,4}: {beforeCount - afterCount,4} nodes removed.");

			return (uint) (beforeCount - afterCount);
		}

		private class PathEntry
		{
			[DisallowNull]
			internal TrainTrackNode node;
			[AllowNull]
			internal PathEntry previous;
			[AllowNull]
			internal PathEntry next;

			internal PathEntry start
			{
				get
				{
					var current = this;
					while (current.previous != null)
					{
						current = current.previous;
					}
					return current;
				}
			}
			internal PathEntry end
			{
				get
				{
					var current = this;
					while(current.next != null)
					{
						current = current.next;
					}
					return current;
				}
			}

			internal int countNext
			{
				get
				{
					int count = 0;
					var current = this;
					while (current.next != null)
					{
						count++;
						current = current.next;
					}
					return count;
				}
			}

			internal int countPrev
			{
				get
				{
					int count = 0;
					var current = this;
					while (current.previous != null)
					{
						count++;
						current = current.previous;
					}
					return count;
				}
			}

			internal int countTotal
			{
				get
				{
					return 1 + countPrev + countNext;
				}
			}

			internal IEnumerable<PathEntry> listView => ToList().ToList();

			internal IEnumerable<PathEntry> ToList()
			{
				var current = start;
				while (current.next != null)
				{
					yield return current;
					current = current.next;
				}
			}

			/// <summary>
			/// Returns the start of a new reversed path.
			/// </summary>
			/// <returns></returns>
			internal PathEntry Reversed()
			{
				// The node of the original path, traversing in reverse.
				var original = end;

				// The current node of the new reversed path.
				var current = new PathEntry();
				current.node = original.node;

				original = original.previous;

				var start = current;

				while (original != null)
				{
					var temp = new PathEntry();
					temp.node = original.node;
					temp.previous = current;
					current.next = temp;

					current = temp;

					original = original.previous;
				}

				return start;
			}

			public override string ToString()
			{
				return $"PathEntry: {node.id}";
				//return $"Path from '{start.node.id}' to '{end.node.id}'";
			}
		}

		private class PathEntryEqualityComparer : IEqualityComparer<PathEntry>
		{
			public bool Equals([AllowNull] PathEntry x, [AllowNull] PathEntry y)
			{
				if (ReferenceEquals(x, y)) return true;
				if (x is null || y is null) return false;

				return (x.start.node == y.start.node && x.end.node == y.end.node) || (x.start.node == y.end.node && x.end.node == y.start.node);
			}

			public int GetHashCode([DisallowNull] PathEntry obj)
			{
				unchecked
				{
					int hash = 17;
					hash = hash * 23 + (obj.start.node.GetHashCode() + obj.end.node.GetHashCode());
					return hash;
				}
			}
		}

		/// <summary>
		/// Return Paths starting from this node to any node that does not have exactly 2 links.
		/// </summary>
		/// <param name="startNode"></param>
		/// <returns></returns>
		private static ICollection<PathEntry> PathsFrom(TrainTrackNode startNode)
		{
			var paths = new HashSet<PathEntry>();

			foreach (var node in startNode.LinkedNodes)
			{
				var start = new PathEntry();
				start.node = startNode;
				
				var next = new PathEntry();
				next.node = node;
				next.previous = start;

				start.next = next;

				paths.Add(start);
				ContinuePath(next);
			}

			return paths;
		}

		private static PathEntry ContinuePath(PathEntry from)
		{
			if (from.next != null) throw new ArgumentException("PathEntry already has next. Path cannot be continued again.");

			var node = from.node;

			if (node.TrackLinks.Count != 2)
			{
				var end = new PathEntry();
				end.previous = from;
				end.node = node;

				return end;
			}

			var prev = from.previous.node;
			foreach (var neighbour in node.LinkedNodes)
			{
				if (neighbour == prev) continue;

				var entry = new PathEntry();
				entry.node = neighbour;
				entry.previous = from;
				from.next = entry;

				// Pray that the compiler does tail call optimization here.
				// If only there was a keyword that would force the compiler to either do it or throw an error if it can't.
				return ContinuePath(entry);
			}

			throw new InvalidOperationException("Invariant broken: Execution should never get here.");
		}

		private static void OptimizePath(PathEntry path, float margin)
		{
			// todo: we should check that, when the start or end node is a junction
			// we don't optimize the junction fork to the other side (left and right fork swapped).


			float margin2 = margin * margin;

			var start = path.start;
			var end = path.end;


			var head = start.next?.next;
			var tail = start;

			bool wasWithinTolerance = false;
			
			while (head != null)
			{
				// todo: edge case where whitin tolerance and head == end should optimize!

				if (WithinThresholdBetween(tail, head, margin2))
				{
					// Ok so far, try next node.

					wasWithinTolerance = true;

					head = head.next;
				}
				else if (! wasWithinTolerance)
				{
					// First/smallest advance already does not work.
					tail = head;
					head = head.next?.next;

					wasWithinTolerance = false;
				}
				else
				{
					// Previous was in tolerance but current is not.
					// optimize from prevhead to tail

					MergeBetween(tail, head);

					tail = head;
					//head = head.next?.next;
					head = head.next?.next;

					wasWithinTolerance = false;
				}
			}
		}

		/// <summary>
		/// Check that the nodes between (<em>not including</em>) <paramref name="start"/> and <paramref name="end"/>
		/// are withing <paramref name="threshold"/> away from the line between <paramref name="start"/> and <paramref name="end"/>.
		/// </summary>
		/// <param name="start"></param>
		/// <param name="end"></param>
		/// <param name="threshold"></param>
		/// <returns></returns>
		private static bool WithinThresholdBetween(PathEntry start, PathEntry end, float threshold)
		{
			var linStart = start.node.GPosition;
			var linEnd = end.node.GPosition;

			var current = start.next;
			while (current != null && current != end)
			{
				var point = current.node.GPosition;
				var closest = ClosestPointOnLine(point, linStart, linEnd);
				var diff = point - closest;
				var len2 = diff.LengthSquared;

				if (len2 > threshold)
				{
					return false;
				}

				current = current.next;
			}

			return true;
		}

		/// <summary>
		/// Merge <see cref="PathEntry"/>s between <paramref name="start"/> and <paramref name="end"/>
		/// so that after the operation <paramref name="start"/>'s <see cref="PathEntry.next"/> will refer to <paramref name="end"/> and all nodes between them are removed..
		/// </summary>
		/// <param name="start"></param>
		/// <param name="end"></param>
		private static void MergeBetween(PathEntry start, PathEntry end)
		{
			var current = start.next;

			while (current != null && current.next != null && current.next != end)
			{
				var prev = current.previous;
				var next = current.next;

				// Replace in the TranTrackNode first.
				prev.node.ReplaceLink(current.node, next.node);
				next.node.ReplaceLink(current.node, prev.node);
				current.node.RemoveSelf();

				// Adjust the pathEntry too.
				prev.next = next;
				next.previous = prev;

				current = next;
			}
		}

		/// <summary>
		/// Returns the point on the line that is the closest to the given <paramref name="point"/>.
		/// </summary>
		/// <param name="point"></param>
		/// <param name="lineStart"></param>
		/// <param name="lineEnd"></param>
		/// <returns></returns>
		private static Vector3 ClosestPointOnLine(Vector3 point, Vector3 lineStart, Vector3 lineEnd)
		{
			var line = lineEnd - lineStart;
			var pointVec = point - lineStart;
			var lineLength = line.Length;
			var lineDir = line.Normalized();
			float projection = Vector3.Dot(pointVec, lineDir);

			if (projection < 0)
			{
				return lineStart;
			}
			else if (projection > lineLength)
			{
				return lineEnd;
			}
			else
			{
				return lineStart + lineDir * projection;
			}
		}
	}
}
