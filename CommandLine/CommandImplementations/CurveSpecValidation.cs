﻿// Copyright 2024 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using CurveGraph;
using Extrusion;
using Extrusion.Generator;
using InteractivePromptLib;
using Pastel;
using System;
using System.Drawing;
using System.Linq;

namespace CommandLine.CommandImplementations
{
	internal static class CurveSpecValidation
	{
		[InteractiveCommand(oneLineHelp: "Validate a CurveSpec.lua file.")]
		internal static void ValidateCurveSpec([InteractiveCommandStringParameter(
			oneLineHelp: "The path, relative to Stormworks_Install/rom/CreatorToolkitData/CurveSpec/ of the file to validate.\n"
					   + "Quote the value if it contains spaces.",
			encapsulationCharacters: "\"'")]
							   string relativePath)
		{
			try
			{
				var spec = LuaIntegration.CurveSpecLoader.Load(relativePath);
				Console.WriteLine("Loading ok");

				TestExtrusion(spec);
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.ToString().Pastel(Color.Red));
			}
		}

		private static void TestExtrusion(CurveSpec spec)
		{
			// todo: extrude over straight section and curved section, and report vertex counts, vertices / meter etc.

			var nodeCenter   = new CurveNode(new OpenToolkit.Mathematics.Vector3(0, 0, 0));
			var nodeStraight = new CurveNode(new OpenToolkit.Mathematics.Vector3(1000, 0, 0));
			var nodeCurve    = new CurveNode(new OpenToolkit.Mathematics.Vector3(1000, 0, 1000));

			var conStraight = nodeCenter.CreateConnection(nodeStraight);
			var conCurved   = nodeCenter.CreateConnection(nodeCurve);

			nodeCenter.Direction = new OpenToolkit.Mathematics.Vector3(452, 0, 0);
			nodeStraight.Direction = new OpenToolkit.Mathematics.Vector3(374, 0 , 0);
			nodeCurve.Direction = new OpenToolkit.Mathematics.Vector3(0, 0, 500);

			var decorator = new CurveDecorator();

			Console.WriteLine($"Straight extrusion: ");
			var resultStraight = decorator.Generate(conStraight, spec, "test_straight");
			WriteResultStats(conStraight, resultStraight);

			Console.WriteLine($"Curved extrusion: ");
			var resultCurved = decorator.Generate(conCurved, spec, "test_curved");
			WriteResultStats(conCurved, resultCurved);

			void WriteResultStats(Connection connection, CurveDecorator.ResultData result)
			{
				var length = connection.curve.Length;

				Int64 meshVertices = result.Meshes.Select(m => (Int64) m.Mesh.vertexCount).Sum();
				Int64 physVertices = result.Physes.Select(m => (Int64)m.Phys.TotalVertices).Sum();

				Int64 totalVertices = meshVertices + physVertices;

				Int64 trackNodes = result.Tracks.LongCount();

				Console.WriteLine($"Total vertices over {length,6:0.00}m: {totalVertices,6}, {totalVertices / length * 100,8:0.00} vertices   / 100 meter.");
				Console.WriteLine($"Mesh  vertices over {length,6:0.00}m: {meshVertices ,6}, {meshVertices  / length * 100,8:0.00} vertices   / 100 meter.");
				Console.WriteLine($"Phys  vertices over {length,6:0.00}m: {physVertices ,6}, {physVertices  / length * 100,8:0.00} vertices   / 100 meter.");
				Console.WriteLine($"TrackNodes     over {length,6:0.00}m: {trackNodes   ,6}, {trackNodes    / length * 100,8:0.00} trackNodes / 100 meter.");
			}
		}
	}
}
