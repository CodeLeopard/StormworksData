﻿// Copyright 2023 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using DataModel.Tiles;
using InteractivePromptLib;
using Pastel;
using Shared;
using Shared.Serialization;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;

namespace CommandLine.CommandImplementations
{
	public class TileBulkOps
	{
		[InteractiveCommand(
			oneLineHelp: "Migration for the introduction of StormworksEditor's `_CT_loadIgnore` attribute.",
			longHelp: "Removes any objects/elements that ended up duplicated due to the migration. Note: This will re-serialize the affected tiles: the file may appear significantly different due tp formatting changes, number rounding and 'xml compression'."
			
			)]
		public static void CT_LoadIgnore_Migration(
			[InteractiveCommandParameter("Specify 'true' to apply the changes, otherwise the affected objects/elements are only listed but not removed.")]
			bool apply = false)
		{
			var regex = new Regex("^ExtrudedTrackMesh_Curve(_phys)?_\\d+$|^StaticObject_\\d+(_phys)?$", RegexOptions.Compiled);

			var sb = new StringBuilder();

			int countFailed = 0;

			bool Transformer(Tile t)
			{
				var pending_removes = new List<AbstractTileItem>();

				bool madeChanges = false;
				foreach(var item in t.AllTileItems())
				{
					if(item.LoadIgnore)
					{
						continue;
					}

					if(! regex.IsMatch(item.id))
					{
						continue;
					}

					madeChanges = true;
					pending_removes.Add(item);
				}

				if(madeChanges)
				{
					sb.AppendLine($"Summary for tile '{t.tileName}':");
				}

				foreach(var item in pending_removes)
				{
					if (apply)
					{
						t.Remove(item);

						sb.AppendLine($"\tRemoved {item.meta_elementName} '{item.id}'.");
					}
					else
					{
						sb.AppendLine($"\tFound {item.meta_elementName} '{item.id}'.");
					}
				}

				return apply && madeChanges;
			}

			foreach (string tileFilePath in Directory.EnumerateFiles
				(StormworksPaths.Data.tiles, "*.xml", SearchOption.AllDirectories))
			{
				CT_LoadIgnore_Migration_Tile(sb, tileFilePath, ref countFailed, Transformer);
			}

			if (! apply)
			{
				Console.WriteLine($"End of summary. Add parameter 'true' to actually apply the changes.");
			}
		}

		private static void CT_LoadIgnore_Migration_Tile(StringBuilder stringBuilder, string filePath, ref int _countFailed, Func<Tile, bool> transformer)
		{
			var fileName = Path.GetFileNameWithoutExtension(filePath);

			if (Tile.InstancesRegex.IsMatch(fileName))
			{
				// *_instances.xml files contain trees, these are loaded separately.
				return;
			}

			if (Tile.RotationsRegex.IsMatch(fileName))
			{
				// rotations of tiles. They don't show up in the in-game editor, so we won't show them either.
				//return;
			}

			XDocument doc;
			try
			{
				doc = XMLHelper.LoadFromFile(filePath);
			}
			catch (Exception e)
			{
				Console.WriteLine
					(
					 $"Error loading tile '{fileName}' from file '{filePath}' during XML loading."
				   + $" The tile will be skipped. Full Exception details:\n{e.ToString().Pastel(Color.Red)}"
					);

				_countFailed++;
				return;
			}

			var tileRomPath = PathHelper.GetRelativePath(StormworksPaths.rom, filePath);


			Tile tileInfo;
			try
			{
				tileInfo = new Tile(doc.Element("definition"), tileRomPath);
			}
			catch (Exception e)
			{
				Console.WriteLine
					(
					 $"Error loading tile '{fileName}' from file '{filePath}' during Tile Representation loading."
				   + $" The tile will be skipped. Full Exception details:\n{e.ToString().Pastel(Color.Red)}"
					);
				_countFailed++;
				return;
			}


			try
			{
				bool needs_save = transformer(tileInfo);

				if (needs_save)
				{
					tileInfo.SaveDataToDocument(doc);

					XMLHelper.SaveToFile(doc, filePath);
				}
				if (stringBuilder.Length > 0)
				{
					Console.WriteLine(stringBuilder.ToString());
				}
			}
			catch (Exception e)
			{
				Console.WriteLine
					(
					 $"Error transforming tile '{fileName}' from file '{filePath}'."
				   + $" The tile will be skipped. Full Exception details:\n{e.ToString().Pastel(Color.Red)}"
					);

				Console.WriteLine($"Validation log (could be empty due to error):");
				Console.WriteLine(stringBuilder);
			}
			finally
			{
				stringBuilder.Clear();
			}
		}

	}
}
