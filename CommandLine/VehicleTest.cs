﻿// Copyright 2022-2024 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using BinaryDataModel.Converters;
using BinaryDataModel.DataTypes;

using DataModel.Definitions;
using DataModel.Vehicles;

using Pastel;

using Shared;
using Shared.Serialization;

namespace CommandLine
{
	static class VehicleTest
	{
		public static bool ListComponentCounts = true;
		public static bool DoDefinitionUsage = true;
		public static bool GenerateMeshes = true;
		public static bool UseMultiThreading = true;

		public static void Run()
		{
			Console.WriteLine("Starting tests for Vehicles...");
			if(!Definition.Ready) Definition.UseOnTheFlyDefinitions(StormworksPaths.Data.definitions);

			if (GenerateMeshes)
			{
				Console.WriteLine("Vehicle meshes will be generated, Component Meshes will be loaded on the fly as required.");
				MeshGenerator.GetOrLoadMesh = GetOrLoadMesh;
			}

			Console.WriteLine();
			DefaultAnalysis();
		}

		private static readonly Dictionary<string, Mesh> meshes = new Dictionary<string, Mesh>();
		private static Mesh GetOrLoadMesh(string pathRelativeToRom)
		{
			// Unlocked fast path
			if (meshes.TryGetValue(pathRelativeToRom, out Mesh mesh)) return mesh;

			lock (meshes)
			{
				// Check again with the lock
				if (!meshes.TryGetValue(pathRelativeToRom, out mesh))
				{
					var fullPath = Path.Combine(StormworksPaths.rom, pathRelativeToRom);

					if (File.Exists(fullPath))
						mesh = Binary.LoadMesh(fullPath);

					// Saves null also, in case file not found.
					meshes[pathRelativeToRom] = mesh;
				}
			}

			return mesh;
		}


		public static void DefaultAnalysis()
		{
			IEnumerable<string> filePathList;

			IEnumerable<string> safePaths = Directory.EnumerateFiles(Path.Combine(StormworksPaths.data, "debris"), "*.xml")
			                                         //.Concat(Directory.EnumerateFiles(Path.Combine(StormworksPaths.data, "preset_vehicles"), "*.xml"))
			                                         .Concat(Directory.EnumerateFiles(Path.Combine(StormworksPaths.data, "preset_vehicles_advanced"), "*.xml"))
			                                         .Concat(Directory.EnumerateFiles(Path.Combine(StormworksPaths.data, "missions"), "vehicle_*.xml", SearchOption.AllDirectories));

			if (Program.savesAvailable)
			{
				filePathList = Directory.EnumerateFiles(StormworksPaths.UserData.vehicles, "*.xml", SearchOption.AllDirectories).Concat(safePaths);
			}
			else
			{
				filePathList = safePaths;
			}

			AnalyzeVehicles(filePathList);
		}

		public static void AnalyzeVehicles(IEnumerable<string> filePathList)
		{
			// Note: Test partially moved to UnitTests project.

			var timer = new Stopwatch();
			timer.Start();


			object lock_misc = new object();
			int vehicleCount = 0;
			UInt64 fileSizeTotal = 0;
			UInt64 componentsTotal = 0;
			UInt64 voxelsTotal = 0;
			UInt64 verticesTotal = 0;
			UInt64 indicesTotal = 0;

			var definitionUsage = new Dictionary<string, UInt32>();

			var mirrorValues = new Dictionary<byte, UInt32>();

			Regex fileNameRegex = new Regex($"{Regex.Escape(StormworksPaths.UserData.vehicles + "\\")}(.*){Regex.Escape(".xml")}", RegexOptions.Compiled);

			ConcurrentQueue<Exception> exceptions = new ConcurrentQueue<Exception>();

			var parallelOptions = new ParallelOptions();
			parallelOptions.MaxDegreeOfParallelism = UseMultiThreading ? 8 : 1;
			var partitioner = Partitioner.Create(filePathList, EnumerablePartitionerOptions.NoBuffering);
			Parallel.ForEach(partitioner, parallelOptions, Each);
			void Each(string filePath, ParallelLoopState loopState, long i)
			{
				StringBuilder sb = new StringBuilder();
				try
				{
					string vehicleName;
					var match = fileNameRegex.Match(filePath);

					if (match.Success)
					{
						vehicleName = match.Groups[1].Value;
					}
					else
					{
						vehicleName = Path.Combine(Path.GetDirectoryName(filePath), Path.GetFileName(filePath));
					}

					UInt64 fileSize = (UInt64)new FileInfo(filePath).Length;


					sb.AppendLine($"Reading vehicle: {vehicleName} {StringExt.PrintBytesReadable(fileSize)}");
					var document = XMLHelper.LoadFromFile(filePath);
					var vehicle = new Vehicle(document.Element("vehicle"), vehicleName);
					document = null;

					UInt64 componentCount = 0;
					UInt64 vertexCount = 0;
					UInt64 indexCount = 0;

					{ // Component counts
						int index = 0;

						var myMirrorValues = new Dictionary<byte, UInt32>();

						foreach (Body vehicleBody in vehicle.bodies)
						{
							var count = vehicleBody.components.LongCount();
							componentCount += (UInt64)count;

							if (ListComponentCounts)
								sb.AppendLine
									(
									 "Components in Body #"
								   + index++.ToString().PadLeft(3)
								   + " : "
								   + count.ToString().PadLeft(6)
									);

							foreach (var component in vehicleBody.components)
							{
								myMirrorValues.IncrementOrCreateKey(component.t);
							}

							if (GenerateMeshes)
							{
								var m = vehicleBody
								   .GenerateMesh(); // Don't store the mesh afterwards, will use too much memory.
								vertexCount += m.vertexCount;
								indexCount += m.indexCount;
							}
						}

						lock (mirrorValues)
						{
							mirrorValues.MergeCountDictionary(myMirrorValues);
						}
					}

					{ // Definition usage
						var vehicleTotal = vehicle.RawDefinitionUsage();

						lock (definitionUsage)
						{
							definitionUsage.MergeCountDictionary(vehicleTotal);
						}

						// Don't show definitions used only a few times, because that would spamm.
						var minimumCountToShow = 50;
						int definitionsTooFewToShow = 0;
						UInt64 componentsNotRepresented = 0;

						foreach (KeyValuePair<string, uint> kvp in vehicleTotal)
						{
							var key = kvp.Key;
							var val = kvp.Value;

							if (val < minimumCountToShow)
							{
								definitionsTooFewToShow++;
								componentsNotRepresented += val;
								continue;
							}

							if (DoDefinitionUsage) sb.AppendLine($"Definition: {key,25} used: {val.ToString(),6}");
							//                 Definition: 1234567890123456789012345 used: 123456
						}

						if (DoDefinitionUsage && definitionsTooFewToShow > 0)
						{
							sb.AppendLine
								(
								 $"Skipped {definitionsTooFewToShow,4} definitions, total occurrences:{componentsNotRepresented,6} because they occurred <{minimumCountToShow}."
								);
							//                 Skipped 123 definitions, total occurrences: 123456 because they occurred < 1234)
						}
					}

					if (!ListComponentCounts) sb.AppendLine($"Bodies in total        : {vehicle.bodies.Count,6}");
					sb.AppendLine($"Components in vehicle  : {componentCount,6}");
					sb.AppendLine($"Voxels     in vehicle  : {vehicle.Voxels.Count,6}");
					sb.AppendLine($"Mass of of vehicle     : {vehicle.mass,6}");
					if (GenerateMeshes)
					{
						sb.AppendLine($"Sum of Generated Mesh Vertices: {vertexCount,8}");
						sb.AppendLine($"Sum of Generated Mesh Indices : {indexCount,8}");
					}

					sb.AppendLine();

					lock (lock_misc)
					{
						vehicleCount++;
						fileSizeTotal += fileSize;
						componentsTotal += componentCount;
						voxelsTotal += (UInt64)vehicle.Voxels.Count;
						verticesTotal += vertexCount;
						indicesTotal += indexCount;
					}
				}
				catch (Exception e)
				{
					exceptions.Enqueue(e);
					sb.AppendLine(e.ToString().Pastel(Color.Red));
				}
				finally
				{
					Console.WriteLine(sb);
				}
			}

			timer.Stop();
			Console.WriteLine($"Loaded {vehicleCount} vehicles in {timer.Elapsed} -> {timer.Elapsed / vehicleCount} per vehicle on average.");
			Console.WriteLine($"{exceptions.Count} Exceptions occurred, which ware shown in the vehicle summary.");

			Console.WriteLine("Mirror values:\n   t__ : occurrences");
			foreach (KeyValuePair<byte, uint> kvp in mirrorValues.OrderBy(kvp => kvp.Key))
			{
				Console.WriteLine($"   {kvp.Key,3} : {kvp.Value,5}");
			}

			Console.WriteLine($"Loaded total xml data  : {StringExt.PrintBytesReadable(fileSizeTotal)}");
			Console.WriteLine($"Loaded total Components: {componentsTotal}");
			Console.WriteLine($"Loaded total Voxels    : {voxelsTotal}");
			if (GenerateMeshes)
			{
				int l = 0;
				int n = 0;

				UInt64 v = 0;
				UInt64 i = 0;
				foreach (Mesh m in meshes.Values)
				{
					if (m == null)
					{
						n++;
					}
					else
					{
						l++;
						v += m.vertexCount;
						i += m.indexCount;
					}
				}
				Console.WriteLine($"Loaded {l,4} meshes for the mesh generation of vehicle bodies.");
				Console.WriteLine($"Found  {n,4} null mesh records.");

				Console.WriteLine($"Sum of Loaded Mesh Vertices   : {v,10}");
				Console.WriteLine($"Sum of Loaded Mesh Indices    : {i,10}");
				Console.WriteLine($"Sum of Generated Mesh Vertices: {verticesTotal,10}");
				Console.WriteLine($"Sum of Generated Mesh Indices : {indicesTotal,10}");

				ulong MeshDataSize(ulong vertices, ulong indices)
				{
					return vertices * (3 + 3) * sizeof(float) // vertices + normals
					     + vertices * sizeof(UInt32)          // VertexColor
					     + indices  * sizeof(UInt16);
				}

				var loadedTotal = MeshDataSize(v,                i);
				var generatedTotal = MeshDataSize(verticesTotal, indicesTotal);
				Console.WriteLine($"Sum of Loaded    mesh data size: {StringExt.PrintBytesReadable(loadedTotal)}");
				Console.WriteLine($"Sum of Generated mesh data size: {StringExt.PrintBytesReadable(generatedTotal)}");
				Console.WriteLine($"Sum of All       mesh data size: {StringExt.PrintBytesReadable(loadedTotal + generatedTotal)}");
			}

			{
				Console.WriteLine($"Component usage across all vehicles:");
				foreach (KeyValuePair<string, uint> kvp in definitionUsage.OrderByDescending((kvp) => kvp.Value))
				{
					var definition = kvp.Key.PadLeft(50);
					var usage = kvp.Value;

					if (definition.EndsWith('?'))
					{
						definition = definition.Pastel(Color.Orange);
					}

					Console.WriteLine($"{definition} : {usage,7}");
				}
			}


			if (false && exceptions.Count > 0)
			{
				Console.WriteLine($"End of test summary, now showing exception summary.");
				throw new AggregateException($"{exceptions.Count} vehicles failed to load.", exceptions);
			}
			Console.WriteLine($"End of test summary.");
		}



		/// <summary>
		/// Get a dictionary that maps the definitions used in this body to their usage count.
		/// </summary>
		/// <param name="addToDictionary">optional: dictionary to add to.</param>
		/// <returns></returns>
		public static Dictionary<Definition, UInt32> DefinitionUsage(this Vehicle instance, Dictionary<Definition, UInt32> addToDictionary = null)
		{
			if (null == addToDictionary) addToDictionary = new Dictionary<Definition, UInt32>();

			foreach(var body in instance.bodies)
			foreach (var component in body.components)
			{
				var definition = component.Definition;

				if (null != definition) addToDictionary.IncrementOrCreateKey(definition);
			}

			return addToDictionary;
		}

		/// <summary>
		/// Get a dictionary that maps the definitions used in this body to their usage count.
		/// </summary>
		/// <param name="addToDictionary">optional: dictionary to add to.</param>
		/// <returns></returns>
		public static Dictionary<string, UInt32> RawDefinitionUsage
			(this Vehicle instance, Dictionary<string, UInt32> addToDictionary = null)
		{
			addToDictionary ??= new Dictionary<string, UInt32>();

			foreach (var body in instance.bodies)
			foreach (var component in body.components)
			{
				string name = null == component.Definition
					? $"{component.rawDefinitionName} ??"
					: component.rawDefinitionName;

				addToDictionary.IncrementOrCreateKey(name);
			}

			return addToDictionary;
		}
	}
}
