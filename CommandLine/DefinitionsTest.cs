﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DataModel.DataTypes;
using DataModel.Definitions;
using DataModel.Definitions.NestedTypes;

using Shared;

namespace CommandLine
{
	static class DefinitionsTest
	{
		private const int _DefNamLen = 50;
		private const int _TypeNamLen = 15;


		public static void Run()
		{
			//Console.WriteLine("Starting tests for Definitions...");
			ParseAll();
			//MeshFlags();
			//Categories();
			//Types();
			FlagsByType();
			//TransTypes();
		}

		static void MeshFlags()
		{
			var flagsWith = new Dictionary<uint, uint>();
			var flagsWithout = new Dictionary<uint, uint>();
			foreach (Definition definition in Definition.Definitions.Values)
			{
				if (string.IsNullOrWhiteSpace(definition.mesh_data_name))
				{
					flagsWithout.IncrementOrCreateKey((uint)definition.flags);
				}
				else
				{
					flagsWith.IncrementOrCreateKey((uint)definition.flags);
				}
			}

			var flagsOnlyWith = new Dictionary<uint, uint>();
			foreach (uint withKey in flagsWith.Keys)
			{
				if (! flagsWithout.ContainsKey(withKey))
				{
					flagsOnlyWith.Add(withKey, flagsWith[withKey]);
				}
			}

			var flagsOnlyWithout = new Dictionary<uint, uint>();
			foreach (uint withKey in flagsWithout.Keys)
			{
				if (! flagsWith.ContainsKey(withKey))
				{
					flagsOnlyWithout.Add(withKey, flagsWithout[withKey]);
				}
			}


			var bitIndicesWith = new Dictionary<byte, ulong>();
			foreach (var kvp in flagsOnlyWith)
			{
				foreach (var val in BitIndices(kvp.Key))
				{
					for (uint i = 0; i < kvp.Value; i++)
						bitIndicesWith.IncrementOrCreateKey(val); // bad, but works.
				}
			}

			var bitIndicesWithout = new Dictionary<byte, ulong>();
			foreach (var kvp in flagsOnlyWithout)
			{
				foreach (var val in BitIndices(kvp.Key))
				{
					for (uint i = 0; i < kvp.Value; i++)
						bitIndicesWithout.IncrementOrCreateKey(val); // bad, but works.
				}
			}

			var bitIndicesOnlyWith = new Dictionary<byte, ulong>();
			foreach (byte withKey in bitIndicesWith.Keys)
			{
				if (!bitIndicesWithout.ContainsKey(withKey))
				{
					bitIndicesOnlyWith.Add(withKey, bitIndicesWith[withKey]);
				}
			}

			var bitIndicesOnlyWithout = new Dictionary<byte, ulong>();
			foreach (byte withKey in bitIndicesWithout.Keys)
			{
				if (!bitIndicesWith.ContainsKey(withKey))
				{
					bitIndicesOnlyWithout.Add(withKey, bitIndicesWithout[withKey]);
				}
			}



			Console.WriteLine("Flags with 'mesh_data_name'");
			WriteFlagsDictionary(flagsWith);
			Console.WriteLine("Flags without 'mesh_data_name'");
			WriteFlagsDictionary(flagsWithout);
			//Console.WriteLine("Flags only with 'mesh_data_name'");
			//WriteFlagsDictionary(flagsOnlyWith);
			//Console.WriteLine("Flags only without 'mesh_data_name'");
			//WriteFlagsDictionary(flagsOnlyWithout);

			var sb = new StringBuilder();

			//sb.AppendLine($"\nBitIndices with mesh: {bitIndicesWith.Count} distinct values");
			//bitIndicesWith.PrintCountDictionary<byte>(sb, "  {0,3} : {1,3}/{2,3}");

			//sb.AppendLine($"\nBitIndices without mesh: {bitIndicesWithout.Count} distinct values");
			//bitIndicesWithout.PrintCountDictionary<byte>(sb, "  {0,3} : {1,3}/{2,3}");

			sb.AppendLine($"\nBitIndices only with mesh: {bitIndicesOnlyWith.Count} distinct values");
			bitIndicesOnlyWith.PrintCountDictionary<byte>(sb, "  {0,3} : {1,3}/{2,3}");

			sb.AppendLine($"\nBitIndices only without mesh: {bitIndicesOnlyWithout.Count} distinct values");
			bitIndicesOnlyWithout.PrintCountDictionary<byte>(sb, "  {0,3} : {1,3}/{2,3}");


			Console.WriteLine(sb);
		}

		static void ParseAll()
		{
			Definition.ParseDefinitions(StormworksPaths.Data.definitions);
		}

		static void Categories()
		{
			var categories = new Dictionary<DefinitionCategory, int>();
			var types = new Dictionary<DefinitionType, int>();

			foreach (Definition definition in Definition.Definitions.Values.OrderBy(d => d.category).ThenBy(d => d.type).ThenBy(d=>d.name))
			//foreach (Definition definition in Definition.Definitions.Values.OrderBy(d => d.type).ThenBy(d => d.category).ThenBy(d=>d.name))
			{
				var category = definition.category;
				var type = definition.type;

				categories.IncrementOrCreateKey(category);
				types.IncrementOrCreateKey(type);

				Console.WriteLine($"{definition.name,-_DefNamLen} : cat:{category,3} type:{type,3}");
				//Console.WriteLine(Path.GetFileNameWithoutExtension(path) + "," + category + "," + type + "," + block_type);
			}

			Console.WriteLine("\nCategories");
			foreach (KeyValuePair<DefinitionCategory, int> kvp in categories.OrderBy(p => p.Key))
			{
				Console.WriteLine($"{kvp.Key,3} : {kvp.Value,3} occurrences");
			}
			Console.WriteLine("Types");
			foreach (KeyValuePair<DefinitionType, int> kvp in types.OrderBy(p => p.Key))
			{
				Console.WriteLine($"{kvp.Key,3} : {kvp.Value,3} occurrences");
			}
			Console.WriteLine();
		}

		static void TransTypes()
		{
			var types = new Dictionary<Surface.TransType, UInt32>();

			foreach (Definition definition in Definition.Definitions.Values.OrderBy(d => d.category).ThenBy(d => d.type).ThenBy(d => d.name))
				//foreach (Definition definition in Definition.Definitions.Values.OrderBy(d => d.type).ThenBy(d => d.category).ThenBy(d=>d.name))
			{
				foreach (Surface s in definition.surfaces)
				{
					types.IncrementOrCreateKey(s.trans_type);
				}
			}

			Console.WriteLine("Trans_Types");
			foreach (KeyValuePair<Surface.TransType, UInt32> kvp in types.OrderBy(p => p.Key))
			{
				Console.WriteLine($"{kvp.Key,3} : {kvp.Value,3} occurrences");
			}
			Console.WriteLine();
		}

		static void Types()
		{
			var types = new Dictionary<DefinitionType, UInt32>();
			const int typeNameLen = 20;

			foreach (Definition definition in Definition.Definitions.Values.OrderBy(d => d.type).ThenBy(d => d.category).ThenBy(d => d.name))
			{
				types.IncrementOrCreateKey(definition.type);

				Console.WriteLine($"{definition.type,typeNameLen} ({(uint)definition.type,3}) {definition.name}");
			}

			Console.WriteLine("Occurrances");
			foreach (KeyValuePair<DefinitionType, UInt32> kvp in types.OrderBy(p => p.Key))
			{
				Console.WriteLine($"{kvp.Key,typeNameLen} ({(uint) kvp.Key,3}) : {kvp.Value,3} occurrences");
			}
			Console.WriteLine();
		}

		static void WriteFlagsDictionary(Dictionary<uint, uint> flags)
		{
			var sb = new StringBuilder();
			sb.AppendLine($"Flags: {flags.Count} distinct values");

			sb.AppendLine($"  |{" ",8}byte {new string("00001111222233334444555566667777".Reverse().ToArray())}");
			sb.AppendLine($"  |{" ",8} bit {new string("01230123012301230123012301230123".Reverse().ToArray())}");
			foreach (var kvp in flags.OrderByDescending(t => t.Value))
			{
				sb.AppendLine($"  |{kvp.Key,10} = {Convert.ToString((int)kvp.Key, 2).PadLeft(4 * 8, '0').Replace("0", "░").Replace('1', '█')} : {kvp.Value,5} occurrences");
			}

			var bitIndices = new Dictionary<byte, UInt64>();

			foreach (var kvp in flags)
			{
				foreach (var val in BitIndices(kvp.Key))
				{
					for (uint i = 0; i < kvp.Value; i++)
						bitIndices.IncrementOrCreateKey(val); // bad, but works.
				}
			}

			sb.AppendLine($"\nBitIndices: {bitIndices.Count} distinct values");
			bitIndices.PrintCountDictionary<byte>(sb, "  {0,3} : {1,3}/{2,3}");


			Console.WriteLine(sb);
		}


		static void Flags()
		{
			var flags = new Dictionary<uint, UInt64>();

			foreach (var definition in Definition.Definitions.Values)
			{
				flags.IncrementOrCreateKey((uint)definition.flags);
			}
			var sb = new StringBuilder();
			sb.AppendLine($"Flags: {flags.Count} distinct values");

			sb.AppendLine($"  |{" ",8}byte {new string("00001111222233334444555566667777".Reverse().ToArray())}");
			sb.AppendLine($"  |{" ",8} bit {new string("01230123012301230123012301230123".Reverse().ToArray())}" );
			foreach (var kvp in flags.OrderByDescending(t=>t.Value))
			{
				sb.AppendLine($"  |{kvp.Key,10} = {Convert.ToString((int) kvp.Key, 2).PadLeft(4*8, '0').Replace("0", ".")} : {kvp.Value, 5} occurrences");
			}

			var bitIndices = new Dictionary<byte, UInt64>();

			foreach (var kvp in flags)
			{
				foreach (var val in BitIndices((uint)kvp.Key))
				{
					for(uint i = 0; i < kvp.Value; i++)
						bitIndices.IncrementOrCreateKey(val); // bad, but works.
				}
			}

			sb.AppendLine($"\nBitIndices: {bitIndices.Count} distinct values");
			bitIndices.PrintCountDictionary<byte>(sb, "  {0,3} : {1,3}/{2,3}");


			Console.WriteLine(sb);
			sb.Clear();

			Console.WriteLine($"{"Flag value", 10} => {"Bit indices", -15}: {"Part name",-_DefNamLen} : File name");

			foreach (Definition definition in Definition.Definitions.Values.OrderBy(d => d.flags))
			{
				var flag = definition.flags;
				Console.WriteLine
				(
					$"{flag,10} => {BitIndicesString((uint)flag), -15}: {definition.name, -_DefNamLen} : {definition.fileName}"
				);
			}
			Console.WriteLine();
		}

		static void FlagsByType()
		{
			var flagCategorys = new Dictionary<DefinitionType, Dictionary<UInt32, UInt32>>();

			foreach (DefinitionType type in Enum.GetValues(typeof(DefinitionType)))
			{
				flagCategorys[type] = new Dictionary<uint, uint>();
			}

			foreach (var definition in Definition.Definitions.Values)
			{
				flagCategorys[definition.type].IncrementOrCreateKey((uint)definition.flags);
			}
			var sb = new StringBuilder();

			foreach (DefinitionType type in Enum.GetValues(typeof(DefinitionType)))
			{
				var flags = flagCategorys[type];

				sb.AppendLine($"Flags in type: {type} ({(long)type}): {flags.Count} distinct values.");

				sb.AppendLine($"  |{" ",8}byte {new string("00001111222233334444555566667777".Reverse().ToArray())}");
				sb.AppendLine($"  |{" ",8} bit {new string("01230123012301230123012301230123".Reverse().ToArray())}");
				foreach (var kvp in flags.OrderByDescending(t => t.Value))
				{
					var key = Convert.ToString(kvp.Key, 2).PadLeft(4 * 8, '0').Replace("0", ".");
					sb.AppendLine($"  |{kvp.Key,10} = {key} : {kvp.Value,5} occurrences.");
				}

				var bitIndices = new Dictionary<byte, UInt64>();

				foreach (var kvp in flags)
				{
					foreach (var val in BitIndices((uint)kvp.Key))
					{
						for (uint i = 0; i < kvp.Value; i++)
							bitIndices.IncrementOrCreateKey(val); // bad, but works.
					}
				}

				sb.AppendLine($"BitIndices: {bitIndices.Count} distinct values");

				string ExtraFormatter(byte indice)
				{
					uint rawValue = 1u << indice;
					if(indice == 0)
					{
						rawValue = 0;
					}

					var enumValue = (DefinitionFlags)rawValue;

					return $"  ({enumValue})";
				}

				bitIndices.PrintCountDictionary<byte>(sb, "  {0,3} : {1,3}/{2,3} {3}", extraFormatter: ExtraFormatter);
				sb.AppendLine();
			}

			Console.WriteLine(sb);
			sb.Clear();

			Console.WriteLine($"{"Flag value",10} => {"Bit indices",-15}: {"Part name",-_DefNamLen} : {"File name",-_DefNamLen} : type");

			foreach (Definition definition in Definition.Definitions.Values.OrderBy(d => d.type).ThenBy(d => d.flags))
			{
				var flag = definition.flags;
				Console.WriteLine
					(
					 $"{flag,10} => {BitIndicesString((uint)flag),-15}: {definition.name,-_DefNamLen} : {definition.fileName,-_DefNamLen} : {(long)definition.type,2} {definition.type, -15}"
					);
			}
			Console.WriteLine();
		}

		static List<byte> BitIndices(UInt32 value)
		{
			var result = new List<byte>();
			byte index = 0;

			while (value != 0)
			{
				if ((value & 1) == 1)
				{
					result.Add(index);
				}

				value >>= 1;
				index++;
			}

			return result;
		}

		static string BitIndicesString(UInt32 value)
		{
			var list = BitIndices(value);

			var sb = new StringBuilder();
			foreach (byte b in list)
			{
				sb.Append(b);
				sb.Append(", ");
			}

			if (sb.Length > 2) sb.Remove(sb.Length - 2, 2);

			return sb.ToString();
		}
	}
}
