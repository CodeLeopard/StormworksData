﻿// Copyright 2022-2024 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Xml.Linq;

using BinaryDataModel;
using BinaryDataModel.Converters;
using CommandLine.CommandImplementations;
using DataModel.DataTypes;
using DataModel.Definitions;
using DataModel.Tiles;
using DataModel.Vehicles;

using InteractivePromptLib;

using OpenToolkit.Mathematics;

using Pastel;

using Shared;
using Shared.Serialization;

using Mesh = BinaryDataModel.DataTypes.Mesh;


namespace CommandLine
{
	class CommandLineInteraction
	{
		private readonly InteractivePrompt myPrompt;

		private static CultureInfo consistentNumberFormatCulture = CultureInfo.GetCultureInfo("en-GB");


		private static readonly Regex UnQuoteString = new Regex("\\\"?(.*)\\\"?");

		internal CommandLineInteraction()
		{
			myPrompt = new InteractivePrompt(typeof(InteractiveCommandAttribute), this, "SWData>");
		}


		internal void Run()
		{
			try
			{
				if (! Thread.CurrentThread.CurrentCulture.Equals(consistentNumberFormatCulture))
				{
					var oldCulture = Thread.CurrentThread.CurrentCulture.Name;
					Thread.CurrentThread.CurrentCulture = consistentNumberFormatCulture;
					Console.WriteLine(($"Waring: current culture '{oldCulture}' may not provide proper number formatting."
					                + $"\nThe culture was changed to '{Thread.CurrentThread.CurrentCulture.Name}' to prevent issues.").Pastel(Color.Orange));
				}


				if (Directory.GetCurrentDirectory()
				 == Path.GetDirectoryName((Assembly.GetExecutingAssembly() ?? Assembly.GetEntryAssembly()).Location))
				{
					// We are in the folder with the .exe
					// There is nothing interesting to find here so let's go to the default Stormworks Installation instead.

					if (null != StormworksPaths.Install)
					{
						SetWorkingDirectory(StormworksPaths.Install);

						// The first event is missed because not subscribed yet.
						MeshConversion.StormworksPaths_InstallChanged();
					}
				}

				myPrompt.Run();


				// m2b "C:\Program Files (x86)\Steam\steamapps\common\Stormworks\rom\meshes\arctic_ice_floating_01.mesh"
				// m2b "C:\Program Files (x86)\Steam\steamapps\common\Stormworks\rom\meshes\mainland\mega_island_9_8.mesh"
				// p2b "C:\Program Files (x86)\Steam\steamapps\common\Stormworks\rom\meshes\mainland\mega_island_9_8.phys"
				// ScaleP "C:\Program Files (x86)\Steam\steamapps\common\Stormworks\rom\meshes\arctic_ice_floating_01_phys.phys" "result.phys" 2
			}
			catch (Exception e)
			{
				Console.WriteLine(e.ToString().Pastel(Color.Red));
			}
		}

		[InteractiveCommand(oneLineHelp: "Stop at breakPoint and Launch Debugger if not already attached.")]
		private void Break()
		{
			if (! Debugger.IsAttached)
			{
				Debugger.Launch();
			}
			Debugger.Break();
		}

		[InteractiveCommand(oneLineHelp: "Set the Main Thread's culture to the specified one.",
		                    longHelp: "This typically shouldn't be needed, but if you have number formatting issues changing the culture can fix it.")]
		private void SetCulture([InteractiveCommandParameter(oneLineHelp: "The culture specifier, like: en-GB")] string specifier)
		{
			var previousCulture = Thread.CurrentThread.CurrentCulture.Name;
			Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo(specifier);
			Console.WriteLine($"Changed the culture from '{previousCulture}' to '{Thread.CurrentThread.CurrentCulture.Name}'");
		}


		

		[InteractiveCommand(aliases: new []{"cd", "ChangeDirectory"}, oneLineHelp: "Change the directory from which relative paths are resolved, specify nothing to get the current value.",
		                    longHelp: "When you specify a relative path in any command it will be taken relative to the current directory as set by this command."
		                            + "\nIf you don't specify anything it will report the current value.")]
		void SetWorkingDirectory(
			[InteractiveCommandStringParameter(
				oneLineHelp:"The directory to go to. If not specified reports the current value.",
				longHelp: "Both absolute and relative paths are allowed."
				        + "\nUse the folder name '..' to go up one directory."
				        + "\nIf the path contains spaces you need to encapsulate it in quotes \" or '",
				encapsulationCharacters: "\"'"
				)]string directory = null)
		{
			if (directory == null)
			{
				Console.WriteLine($"Working Directory: {Directory.GetCurrentDirectory()}");
				return;
			}

			if (! Path.IsPathRooted(directory))
			{
				var matches = Directory.GetDirectories(Directory.GetCurrentDirectory(), directory);
				if (matches.Length == 0)
				{
					Console.WriteLine($"'{directory}' not found.".Pastel(Color.Red));
					return;
				}

				if (matches.Length > 1)
				{
					Console.WriteLine($"Pattern '{directory}' found multiple results, specify an unambiguous pattern.".Pastel(Color.Red));
					return;
				}

				directory = matches[0];
			}
			else
			{
				if(!Directory.Exists(directory))
				{
					Console.WriteLine($"'{directory}' not found.".Pastel(Color.Red));
					return;
				}
			}

		

			Directory.SetCurrentDirectory(directory);
			Console.Write($"Working Directory: '{Directory.GetCurrentDirectory()}'.");

			if (StormworksPaths.IsPathInStormworksInstall(Directory.GetCurrentDirectory(), out string installRoot))
			{
				if (StormworksPaths.Install != installRoot)
				{
					StormworksPaths.Install = installRoot;
					Console.Write($" Also set Stormworks Install to '{installRoot}'.");
				}

				myPrompt.prompt = $"SWCLI {Path.GetRelativePath($"{installRoot}/..", Directory.GetCurrentDirectory())}> ";
			}
			else
			{
				myPrompt.prompt = $"SWCLI {Directory.GetCurrentDirectory()}> ";
			}

			Console.WriteLine();
		}


		[InteractiveCommand(aliases: new [] {"ls", "dir"}, oneLineHelp: "Lists the content of the specified (or current) directory.")]
		void DirectoryListing(
			[InteractiveCommandStringParameter(
				oneLineHelp:"The directory to list. If not specified uses the Working Directory.",
				longHelp: "Both absolute and relative paths are allowed."
				        + "\nUse the folder name '..' to go up one directory."
				        + "\nIf the path contains spaces you need to encapsulate it in quotes \" or '",
				encapsulationCharacters: "\"'"
			)] string directory = null,
			[InteractiveCommandParameter(nameOverride: "hidden", oneLineHelp: "Include hidden entries.")]
			bool includeHidden = false)
		{
			directory ??= Directory.GetCurrentDirectory();

			var dir = new DirectoryInfo(directory);
			if (! dir.Exists)
			{
				Console.WriteLine($"Directory '{directory}' not found.".Pastel(Color.Red));
				return;
			}

			Console.WriteLine($"Contents of '{directory}'");

			foreach (FileSystemInfo info in dir.EnumerateFileSystemInfos())
			{
				bool hidden = (info.Attributes & FileAttributes.Hidden) != 0
					|| info.Name[0] == '.';

				if(!includeHidden && hidden)
					continue;

				string s;
				if (info is FileInfo file)
				{
					s = $"{file.Name,-45}";

					s += $" {StringExt.PrintBytesReadable(file.Length), 10}";

					// Attempt to display more information about the file.
					if (file.Name.EndsWith(".mesh"))
					{
						try
						{
							var m = Binary.LoadMesh(file.FullName);

							s += $" data: {m.vertexCount, 5} vertices, {m.indexCount, 6} indices, {m.SubMeshCount, 3} subMeshes.";
						}
						catch (Exception e)
						{
							s += $" could not load mesh metaData ({e.GetType().Name}).".Pastel(Color.Orange);
						}
					}
					else if (file.Name.EndsWith(".phys"))
					{
						try
						{
							var m = Binary.LoadPhys(file.FullName);

							s += $" data: {m.TotalVertices, 5} vertices, {m.TotalIndices, 6} indices, {m.subMeshCount, 3} subMeshes.";
						}
						catch (Exception e)
						{
							s += $" could not load phys metaData ({e.GetType().Name}).".Pastel(Color.Orange);
						}
					}


					var ext = Path.GetExtension(file.Name);
					if (ext == ".mesh")
					{
						s = s.Pastel(Color.Green);
					}
					else if (ext == ".phys")
					{
						s = s.Pastel(Color.Blue);
					}
					else if (ext == ".anim")
					{
						s = s.Pastel(Color.Magenta);
					}
				}
				else if (info is DirectoryInfo directoryInfo)
				{
					s = $"{directoryInfo.Name}{Path.DirectorySeparatorChar}";
				}
				else
				{
					s = $"{info.Name}";
				}

				if (hidden)
				{
					s = s.Pastel(Color.Gray);
				}

				Console.WriteLine(s);
			}
		}


		[InteractiveCommand(
			oneLineHelp: "Change the currently used Stormworks Installation, specify nothing to get the current value.",
			longHelp: "When you set this to a directory that contains stormworks.exe any command that operates on a Stormworks Installation will use the specified install.")]
		void SetStormworksInstallation(
			[InteractiveCommandStringParameter(
				oneLineHelp: "Change the currently used Stormworks Installation, specify nothing to get the current value.",
				longHelp: "For commands that operate on an entire stormworks installation this command sets where that installation is."
				        + "It is separate from the 'Current Directory' as set by " + nameof(SetWorkingDirectory) + "."
				        + "\nIf the path contains spaces you need to encapsulate it in quotes \" or '",
				encapsulationCharacters: "\"'")]
			string directory = null,
			[InteractiveCommandParameter(oneLineHelp: "Use the specified folder, even if it does not contain 'stormworks.exe'.")]
			bool force = false)
		{
			if (directory == null)
			{
				if (null == StormworksPaths.Install)
				{
					Console.WriteLine("No installation of Stormworks was automatically found.".Pastel(Color.Orange));
					return;
				}

				Console.WriteLine($"Current Stormworks installation: '{StormworksPaths.Install}'.");
				return;
			}

			if (! Directory.Exists(directory))
			{
				Console.WriteLine($"'{directory}' not found.".Pastel(Color.Red));
				return;
			}

			var results = Directory.EnumerateFiles(directory, "stormworks??.exe");

			if (! results.Any())
			{
				const string error = "Did not find 'stormworks.exe' or 'stormworks64.exe' in that directory.";

				if (force)
				{
					Console.WriteLine(error.Pastel(Color.Orange));
				}
				else
				{
					Console.WriteLine(error.Pastel(Color.Red));
					return;
				}
			}

			StormworksPaths.Install = directory;
			Console.WriteLine($"Current Stormworks installation: '{StormworksPaths.Install}'.");
		}

		[InteractiveCommand(
			oneLineHelp: "Check Tiles for problems, such as xml errors or missing referenced files (mesh, phys).",
			longHelp: "Currently checks:"
			        + "\nxml is valid"
			        + "\nmesh/file_name exists and is valid"
			        + "\nphysics_mesh/file_name exists and is valid"
			        + "\nedit_area/default_vehicle exists"
		)]
		void ValidateTileXml([InteractiveCommandStringParameter(
			oneLineHelp:"Optional: tile to validate. If not specified validates all tiles in Stormworks.",
			longHelp:"Does a reasonable guess to find the file, so you can omit the extension and path to the tiles folder for example."
			       + "\nAbsolute paths are also allowed."
			       + "\nIf the path contains spaces you need to encapsulate it in quotes \" or '",
			encapsulationCharacters: "\"'")] string tile = null)
		{
			StringBuilder sb = new StringBuilder();
			int countFailed = 0;
			int countWarn = 0;
			int countPass = 0;

			Validator.meshFileBytes = 0;
			Stopwatch timer = new Stopwatch();
			timer.Start();

			if (tile != null)
			{
				if (! Path.HasExtension(tile))
				{
					tile += ".xml";
				}

				string filePath;
				if (Path.IsPathRooted(tile))
				{
					filePath = tile;
				}
				else if(File.Exists(tile)) // relative to current location
				{
					filePath = tile;
				}
				else
				{
					filePath = Path.Combine(StormworksPaths.rom, tile);

					if (! File.Exists(filePath))
					{
						filePath = Path.Combine(StormworksPaths.Data.tiles, tile);
					}
				}

				if (! File.Exists(filePath))
				{
					throw new FileNotFoundException("The specified tile could not be found.", filePath);
				}

				Validate(sb, filePath, ref countFailed, ref countWarn, ref countPass);
			}
			else
			{

				foreach (string tileFilePath in Directory.EnumerateFiles
					(StormworksPaths.Data.tiles, "*.xml", SearchOption.AllDirectories))
				{
					Validate(sb, tileFilePath, ref countFailed, ref countWarn, ref countPass);
				}
			}

			void Validate(StringBuilder stringBuilder, string filePath, ref int _countFailed, ref int _countWarn, ref int _countPass)
			{
				var fileName = Path.GetFileNameWithoutExtension(filePath);

				if (Tile.InstancesRegex.IsMatch(fileName))
				{
					// *_instances.xml files contain trees, these are loaded separately.
					return;
				}

				if (Tile.RotationsRegex.IsMatch(fileName))
				{
					// rotations of tiles. They don't show up in the in-game editor, so we won't show them either.
					//return;
				}


				XDocument doc;
				try
				{
					doc = XMLHelper.LoadFromFile(filePath);
				}
				catch (Exception e)
				{
					Console.WriteLine
						(
						 $"Error loading tile '{fileName}' from file '{filePath}' during XML loading."
					   + $" The tile will be skipped. Full Exception details:\n{e.ToString().Pastel(Color.Red)}"
						);

					_countFailed++;
					return;
				}

				var tileRomPath = PathHelper.GetRelativePath(StormworksPaths.rom, filePath);


				Tile tileInfo;
				try
				{
					tileInfo = new Tile(doc.Element("definition"), tileRomPath);
				}
				catch (Exception e)
				{
					Console.WriteLine
						(
						 $"Error loading tile '{fileName}' from file '{filePath}' during Tile Representation loading."
					   + $" The tile will be skipped. Full Exception details:\n{e.ToString().Pastel(Color.Red)}"
						);
					_countFailed++;
					return;
				}


				try
				{
					var result = Validator.Validate(stringBuilder, tileInfo);
					if (result == Validator.ValidationResult.Pass)
					{
						Console.WriteLine(tileRomPath.Pastel(Color.DarkGreen));
						_countPass++;
					}
					else if (result == Validator.ValidationResult.Warn)
					{
						Console.WriteLine(tileRomPath.Pastel(Color.Orange));
						Console.WriteLine(stringBuilder);
						_countWarn++;
					}
					else
					{
						Console.WriteLine(tileRomPath.Pastel(Color.Red));
						Console.WriteLine(stringBuilder);
						_countFailed++;
					}
				}
				catch (Exception e)
				{
					Console.WriteLine
						(
						 $"Error validating tile '{fileName}' from file '{filePath}'."
					   + $" The tile will be skipped. Full Exception details:\n{e.ToString().Pastel(Color.Red)}"
						);

					Console.WriteLine($"Validation log (could be empty due to error):");
					Console.WriteLine(stringBuilder);
				}
				finally
				{
					stringBuilder.Clear();
				}
			}

			timer.Stop();
			int countTotal = countPass + countFailed;

			Console.WriteLine($"Done after {timer.Elapsed}, checked {StringExt.PrintBytesReadable(Validator.meshFileBytes)} worth of meshes ({StringExt.PrintBytesReadable(Validator.meshFileBytes / (uint) timer.Elapsed.TotalSeconds)} / s).");
			if (countFailed == 0 && countWarn == 0)
			{
				Console.WriteLine($"Validation completed, all {countTotal} tiles passed validation.".Pastel(Color.LimeGreen));
			}
			else if (countFailed == 0)
			{
				Console.WriteLine($"Validation completed, "
				                + $"{countTotal} tiles checked, "
				                + $"{countPass} passed, ".Pastel(Color.LimeGreen)
				                + $"{countWarn} passed with warnings, ".Pastel(Color.Orange)
				                + $"{countFailed} failed.".Pastel(Color.Lime));
			}
			else
			{
				Console.WriteLine($"{"Validation completed".Pastel(Color.Orange)}, "
				                + $"{countTotal} tiles checked, "
				                + $"{countPass} passed, ".Pastel(Color.LimeGreen)
				                + $"{countWarn} passed with warnings, ".Pastel(Color.Orange)
				                + $"{countFailed} failed.".Pastel(Color.Red));
			}
		}


		[InteractiveCommand(oneLineHelp: "Generate a summary of which meshes are used according to Definition and Tile xml.")]
		void MeshUsage()
		{
			if(! Definition.Ready) Definition.ParseDefinitions(StormworksPaths.Data.definitions);

			Dictionary<string, int> meshUsage = new Dictionary<string, int>();

			foreach (var entry in Directory.EnumerateFiles
				(StormworksPaths.meshes, "*.mesh", SearchOption.AllDirectories).Concat(Directory.EnumerateFiles
				         (StormworksPaths.meshes, "*.phys", SearchOption.AllDirectories)))
			{
				var romPath = PathHelper.GetRelativePath(StormworksPaths.rom, entry).Replace('\\', '/');

				meshUsage.Add(romPath, 0);
			}

			Console.WriteLine("Checking Definitions...");
			foreach (var definition in Definition.Definitions.Values)
			{
				if (null != definition.mesh_data_name)
				{
					meshUsage.IncrementOrCreateKey(definition.mesh_data_name);
				}

				if (null != definition.mesh_0_name)
				{
					meshUsage.IncrementOrCreateKey(definition.mesh_0_name);
				}

				if (null != definition.mesh_1_name)
				{
					meshUsage.IncrementOrCreateKey(definition.mesh_1_name);
				}

				if (null != definition.mesh_editor_only_name)
				{
					meshUsage.IncrementOrCreateKey(definition.mesh_editor_only_name);
				}
			}

			Console.WriteLine("Checking Tiles...");
			foreach (string tileFilePath in Directory.EnumerateFiles
				(StormworksPaths.Data.tiles, "*.xml", SearchOption.AllDirectories))
			{
				var fileName = Path.GetFileNameWithoutExtension(tileFilePath);

				if (Tile.InstancesRegex.IsMatch(fileName))
				{
					// *_instances.xml files contain trees, these are loaded separately.
					continue;
				}

				if (Tile.InstancesRegex.IsMatch(fileName))
				{
					// rotations of tiles. They don't show up in the in-game editor, so we won't show them either.
					continue;
				}


				XDocument doc;
				try
				{
					doc = XMLHelper.LoadFromFile(tileFilePath);
				}
				catch (Exception e)
				{
					Console.WriteLine($"Error loading tile '{fileName}' from file '{tileFilePath}' during XML loading."
					                + $" The tile will be skipped. Full Exception details:\n{e.ToString().Pastel(Color.Red)}");
					continue;
				}

				var tileRomPath = PathHelper.GetRelativePath(StormworksPaths.rom, tileFilePath);


				Tile tileInfo;
				try
				{
					tileInfo = new Tile(doc.Element("definition"), tileRomPath);
				}
				catch (Exception e)
				{
					Console.WriteLine
						(
						 $"Error loading tile '{fileName}' from file '{tileFilePath}' during Tile Representation loading."
					   + $" The tile will be skipped. Full Exception details:\n{e.ToString().Pastel(Color.Red)}"
						);
					continue;
				}

				foreach (MeshInfo meshInfo in tileInfo.meshes)
				{
					meshUsage.IncrementOrCreateKey(meshInfo.file_name);
				}

				foreach (PhysInfo physInfo in tileInfo.physicsMeshes)
				{
					meshUsage.IncrementOrCreateKey(physInfo.file_name);
				}
			}

			int numMissing = 0;
			Console.WriteLine("Results:");
			Console.WriteLine("Usages : mesh name");
			foreach (KeyValuePair<string, int> kvp in meshUsage.OrderBy(kvp => kvp.Key))
			{
				string name = kvp.Key;
				int count = kvp.Value;

				string text = $" {count,5} : {name}";

				if (! File.Exists(Path.Combine(StormworksPaths.rom, name)))
				{
					numMissing++;
					Console.WriteLine(text.Pastel(Color.Orange));
				}
				else if (count == 0)
				{
					const int shade = 50;
					Console.WriteLine(text.Pastel(Color.FromArgb(shade, shade, shade)));
				}
				else if (count > 1)
				{
					Console.WriteLine(text.Pastel(Color.LimeGreen));
				}
				else
				{
					Console.WriteLine(text);
				}
			}

			if (numMissing > 0)
			{
				Console.WriteLine($"{numMissing} meshes ware referenced but the file could not be found.".Pastel(Color.Orange));
			}
		}

		[InteractiveCommand(oneLineHelp: "Finds the nummerical value for the combination of flag names. Note that the names are case-sensitive.")]
		void DefinitionFlagNumber(string[] names)
		{
			DefinitionFlags finalValue = 0;

			foreach(var name in names)
			{
				var value = (DefinitionFlags)Enum.Parse(typeof(DefinitionFlags), name);

				finalValue |= value;
			}

			Console.WriteLine((int) finalValue);
		}

		[InteractiveCommand(oneLineHelp: "Finds the flag names for the numerical value of the 'flags' attribute of a block definition.")]
		void DefinitionFlagNames(uint? number = null)
		{
			if(number == null)
			{
				var names = string.Join(", ", Enum.GetNames(typeof(DefinitionFlags)));
				Console.WriteLine("All flag names: ");
				Console.WriteLine(names);
				return;
			}

			// Todo: when the enum has named multi flag values this will ensure correct printing.
			// https://stackoverflow.com/questions/5542816/printing-flags-enum-as-separate-flags

			Console.WriteLine((DefinitionFlags)number);

			// todo: perhaps do this for other enums too
			// todo: expose the xml documentation summary and remarks for the enum values too.
			// -> https://stackoverflow.com/a/238058
		}

		[InteractiveCommand(hidden: true, oneLineHelp: "[Debug] Computes some statistics about tiles.",
							longHelp: "This command is used for debugging and as such may disappear or change in the future."
		                    )]
		void TileStatistics()
		{
			Tiles.CustomStatistics();
		}

		[InteractiveCommand(hidden: true, oneLineHelp: "[Debug] Computes some statistics about definitions.",
		                    longHelp: "This command is used for debugging and as such may disappear or change in the future.")]
		void DefinitionStatistics()
		{
			DefinitionsTest.Run();
		}


		[InteractiveCommand(hidden: true, oneLineHelp: "[Debug] Run tests from a 'standard' list.",
		                    longHelp: "This command is used for debugging and as such may disappear or change in the future.")]
		void Continuous_Integration()
		{
			Program.AllTests();
		}

		[InteractiveCommand(hidden: true, oneLineHelp: "[Debug] Run tests from a 'custom' list.",
		                    longHelp: "This command is used for debugging and as such may disappear or change in the future.")]
		void Continuous_Integration_Custom()
		{
			Program.CustomTests();
		}

		[InteractiveCommand(hidden: true, oneLineHelp: "[Debug] Run tests from a 'user' list.",
							longHelp: "This command is used for debugging and as such may disappear or change in the future.")]
		void Continuous_Integration_User()
		{
			AnalyticsParser.UserTest();
		}

		[InteractiveCommand(hidden: true, oneLineHelp: "[Debug] Open an interactive Lua prompt in the CurveSpecLoader's sandbox.",
		                    longHelp: "This command is used for debugging and as such may disappear or change in the future.")]
		void TestLuaCurveSpec()
		{
			Console.WriteLine("Enter 'exit' to leave the lua environment.");
			Console.WriteLine("Enter 'return variable' to see the value of variable.");
			LuaIntegration.Program.Main(null);
		}



		private string GetVehiclePathFromUserInputString(string input)
		{
			// todo: can't use this because it would cause unexpected behaviour with outputPath.
			// todo: add 2nd parameter for destination that need not exist but is transformed in the same way as input.

			string result = input;
			if (Path.IsPathRooted(result)) return result;

			var f = new FileInfo(result);

			if (f.Exists) return result;

			result = Path.Combine(StormworksPaths.UserData.vehicles, result);
			f = new FileInfo(result);

			if(f.Exists) return result;

			throw new FileNotFoundException($"Could not find a vehicle file.", input);
		}


		[InteractiveCommand(hidden: true,
			oneLineHelp: "NOT-READY FOR GENERAL USE! Invisify blocks in a vehicle that match a specified color.",
		                    longHelp: "NOT-READY FOR GENERAL USE!\n"
		                            + "Load the vehicle and make the blocks that match the specified color invisible by making them one-dimensional."
		                            + "Note! Vehicles are not fully implemented so data could be lost!"
		                            + "MicroControllers and blocks with additional properties will be reset to default.")]
		void InvisifyVehicle(
			[InteractiveCommandStringParameter(encapsulationCharacters:"'\"")]  string vehiclePath,
			[InteractiveCommandNumberParameter(NumberStyles.AllowHexSpecifier)] uint   colorFilter = 0x7E2553,
			bool                                                                       undo        = false,
			[InteractiveCommandStringParameter(encapsulationCharacters: "'\"")] string outputPath = null)
		{
			if (! Definition.Ready)
			{
				Definition.UseOnTheFlyDefinitions(StormworksPaths.Data.definitions);
			}

			var document = XMLHelper.LoadFromFile(vehiclePath);
			var vehicle = new Vehicle(document);

			int processedCount = 0;
			int appliedCount = 0;
			int alreadyAppliedCount = 0;
			int skippedCount = 0;

			Matrix3 rot;

			if (undo)
			{
				rot = Matrix3.Identity;
			}
			else
			{
				rot = Matrix3.Zero;
				rot.M11 = 1;
			}

			foreach (Component component in vehicle.AllComponents)
			{
				processedCount++;
				if (component.bc != colorFilter)
				{
					continue;
				}
				if (! component.Definition.IsSingleVoxel)
				{
					// Doing this to multi-voxel blocks will break things even harder.
					skippedCount++;
					continue;
				}
				if (component.rotation == rot)
				{
					alreadyAppliedCount++;
					continue;
				}

				component.rotation = rot;

				appliedCount++;
			}

			Console.WriteLine($"Processed {processedCount} components, applied {appliedCount}, found {alreadyAppliedCount} already applied and skipped {skippedCount} incompatible blocks.");

			document = vehicle.ToXDocument();
			XMLHelper.SaveToFile(document, outputPath ?? vehiclePath);
		}

		[InteractiveCommand(oneLineHelp: "Invisify blocks in a vehicle that match a specified color. Saves only the changed blocks to a new file.",
		longHelp: "Load the vehicle and make the blocks that match the specified color invisible by making them one-dimensional.\n"
		        + "Note: this operation *replaces* the rotation information, so rotation cannot be preserved.\n"
		        + "Don't try to invisify blocks where rotation matters.\n"
		        + "When completed there will be a short summary with how many blocks ware affected.")]
		void InvisifyVehicleBlocks(
			[InteractiveCommandStringParameter(encapsulationCharacters: "'\"", oneLineHelp: "Path to the vehicle to be loaded.")]
			string vehiclePath,
			[InteractiveCommandNumberParameter(NumberStyles.AllowHexSpecifier, oneLineHelp: "The color of the blocks to be invisified as hexadecimal (you can google 'rgb to hex' if you don't know).")]
			uint   colorFilter = 0x7E2553,
			[InteractiveCommandParameter(oneLineHelp: "If undo is specified then all matching blocks will be made visible again.")]
			bool undo        = false,
			[InteractiveCommandStringParameter(encapsulationCharacters: "'\"", oneLineHelp: "Path to the output file, defaults to '{vehiclePath}_invis'.")]
			string outputPath  = null)
		{
			if (!Definition.Ready)
			{
				Definition.UseOnTheFlyDefinitions(StormworksPaths.Data.definitions);
			}


			var document = XMLHelper.LoadFromFile(vehiclePath);
			var vehicle = new Vehicle(document);

			int processedCount = 0;
			int appliedCount = 0;
			int alreadyAppliedCount = 0;
			int skippedCount = 0;

			Matrix3 rot;

			if (undo)
			{
				rot = Matrix3.Identity;
			}
			else
			{
				rot = Matrix3.Zero;
				rot.M11 = 1;
			}

			var outVehicle = new Vehicle();
			outVehicle.editor_placement_offset = vehicle.editor_placement_offset;


			var components = new List<Component>(0);
			foreach (Body vehicleBody in vehicle.bodies)
			{
				components.Clear();
				foreach (var component in vehicleBody.components)
				{
					processedCount++;
					if (component.bc != colorFilter)
					{
						continue;
					}
					if (!component.Definition.IsSingleVoxel)
					{
						// Doing this to multi-voxel blocks will break things even harder.
						skippedCount++;
						continue;
					}
					if (component.rotation == rot)
					{
						alreadyAppliedCount++;
						continue;
					}

					component.rotation = rot;
					components.Add(component);

					appliedCount++;
				}

				if (components.Count > 0)
				{
					var b = new Body(outVehicle);
					b.components.AddRange(components);

				}
			}

			Console.WriteLine($"Processed {processedCount} components, applied {appliedCount}, found {alreadyAppliedCount} already applied and skipped {skippedCount} incompatible blocks.");

			document = outVehicle.ToXDocument();

			if (null == outputPath)
			{
				string ext = Path.GetExtension(vehiclePath);
				string pathNoExt = Path.ChangeExtension(vehiclePath, null);
				outputPath = $"{pathNoExt}_invis{ext}";
			}

			XMLHelper.SaveToFile(document, outputPath);
			Console.WriteLine($"Saved result to '{outputPath}'");
		}


		[InteractiveCommand(oneLineHelp: "Validate a Vehicle file.")]
		void ValidateVehicle([InteractiveCommandStringParameter(
			oneLineHelp: "The path of the file to validate.\n"
				+ "Quote the value if it contains spaces.",
			encapsulationCharacters: "\"'")]
				string relativePath)
		{
			if(! Definition.Ready)
			{
				Definition.UseOnTheFlyDefinitions(StormworksPaths.Data.definitions);
			}

			MeshGenerator.GetOrLoadMesh ??= MeshCache.GetOrLoadMesh;

			var path = Path.GetFullPath(relativePath);
			try
			{
				var vehicle = new Vehicle(XMLHelper.LoadFromFile(path), path);

				foreach(var body in vehicle.bodies)
				{
					var mesh = body.mesh;
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.ToString().Pastel(Color.Red));
			}
		}
	}
}
