﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;

using DataModel.Tiles;

using Pastel;

using Shared;
using Shared.Serialization;

namespace CommandLine
{
	public static class Tiles
	{
		private static int SameObjectLinkCounter = 0;
		private static int SameObjectMaxLinks = 0;

		public static void CustomStatistics()
		{
			NotableThings.Clear();
			SameObjectLinkCounter = 0;
			SameObjectMaxLinks = 0;
			StringBuilder sb = new StringBuilder();
			int countFailed = 0;
			int countPass = 0;

			foreach (string tileFilePath in Directory.EnumerateFiles
				(StormworksPaths.Data.tiles, "*.xml", SearchOption.AllDirectories))
			{
				Validate(sb, tileFilePath, ref countFailed, ref countPass);
			}

			void Validate(StringBuilder stringBuilder, string filePath, ref int _countFailed, ref int _countPass)
			{
				var fileName = Path.GetFileNameWithoutExtension(filePath);

				if (Tile.InstancesRegex.IsMatch(fileName))
				{
					// *_instances.xml files contain trees, these are loaded separately.
					return;
				}

				if (Tile.RotationsRegex.IsMatch(fileName))
				{
					// rotations of tiles. They don't show up in the in-game editor, so we won't show them either.
					//return;
				}


				XDocument doc;
				try
				{
					doc = XMLHelper.LoadFromFile(filePath);
				}
				catch (Exception e)
				{
					Console.WriteLine
						(
						 $"Error loading tile '{fileName}' from file '{filePath}' during XML loading."
					   + $" The tile will be skipped. Full Exception details:\n{e.ToString().Pastel(Color.Red)}"
						);

					_countFailed++;
					return;
				}

				var tileRomPath = PathHelper.GetRelativePath(StormworksPaths.rom, filePath);


				Tile tileInfo;
				try
				{
					tileInfo = new Tile(doc.Element("definition"), tileRomPath);
				}
				catch (Exception e)
				{
					Console.WriteLine
						(
						 $"Error loading tile '{fileName}' from file '{filePath}' during Tile Representation loading."
					   + $" The tile will be skipped. Full Exception details:\n{e.ToString().Pastel(Color.Red)}"
						);
					_countFailed++;
					return;
				}


				try
				{
					CheckTile(sb, doc, tileInfo);
					countPass++;
				}
				catch (Exception e)
				{
					Console.WriteLine
						(
						 $"Error validating tile '{fileName}' from file '{filePath}'."
					   + $" The tile will be skipped. Full Exception details:\n{e.ToString().Pastel(Color.Red)}"
						);

					Console.WriteLine($"Validation log (could be empty due to error):");
					Console.WriteLine(stringBuilder);
				}
				finally
				{
					stringBuilder.Clear();
				}
			}

			int countTotal = countPass + countFailed;

			if (countFailed == 0)
			{
				Console.WriteLine($"\nValidation completed, all {countTotal} tiles passed validation.".Pastel(Color.LimeGreen));
			}
			else
			{
				Console.WriteLine($"\n{"Validation completed".Pastel(Color.Orange)}, "
								+ $"{countTotal} tiles checked, {$"{countPass} passed".Pastel(Color.LimeGreen)}, "
								+ $"{$"{countFailed} failed.".Pastel(Color.Red)}");
			}

			foreach (var a in attributeValues)
			{
				var attrName = a.Key;
				var values = a.Value;
				Console.WriteLine(attrName.Pastel(Color.Cyan));
				foreach (KeyValuePair<string, uint> kvp in values.OrderByDescending((kvp) => kvp.Value))
				{
					var definition = $"'{kvp.Key}'".PadLeft(50);
					var usage = kvp.Value;

					Console.WriteLine($"    {definition} : {usage,7}");
				}
			}

			Console.WriteLine(NotableThings.ToString());

			Console.WriteLine($" SameObjectLinks/Total   {SameObjectLinkCounter, 4}");
			Console.WriteLine($" SameObjectLinks/MaxEach {SameObjectMaxLinks, 4}");
		}

		private static StringBuilder NotableThings = new StringBuilder();


		private static Dictionary<string, Color> colors = new Dictionary<string, Color>()
		{
			{ "0", Color.Blue }
		  , { "1", Color.Cyan }
		  , { "2", Color.Green }
		  , { "3", Color.Yellow }
		  , { "4", Color.Magenta }
		};

		private static void CheckTile(StringBuilder sb, XDocument doc, Tile t)
		{
			//CheckTileAttributeValues(sb, doc, t);
			//CheckTileItemReferencesToOtherElements(sb, doc, t);
			CheckTileSameItemLinks(sb, doc, t);
		}

		public static void CheckTileItemReferencesToOtherElements(StringBuilder sb, XDocument doc, Tile t)
		{
			foreach (AbstractTileItem item in t.AllTileItems())
			{
				AddAttributeValue(attributeValues, "parent_id", item.parent_id);
				AddAttributeValue(attributeValues, "purchase_interactable_id", item.purchase_interactable_id);
			}
		}

		public static void CheckTileSameItemLinks(StringBuilder sb, XDocument doc, Tile t)
		{
			foreach (MeshInfo meshInfo in t.meshes)
			{
				int count = meshInfo.SameObjectLinks.Count();
				SameObjectLinkCounter += count;
				SameObjectMaxLinks = Math.Max(SameObjectMaxLinks, count);

				if (count == 0) continue;


				NotableThings.AppendLine($"Links of '{meshInfo.id}' in tile '{t.fileName}':");
				foreach (var item in meshInfo.SameObjectLinks)
				{
					float angle = meshInfo.LTransform.ExtractRotation().AngleBetween(item.LTransform.ExtractRotation()) * (float) (180 / Math.PI);
					float distance = (meshInfo.LTransform.Column3.Xyz - item.LTransform.Column3.Xyz).Length;

					NotableThings.AppendLine($"    {item.meta_elementName,20} '{item.id,-40}' Angle: {angle:0.####}°, Distance: {distance:0.####}");




				}
			}
		}


		private static Dictionary<string, Dictionary<string, UInt32>> attributeValues = new Dictionary<string, Dictionary<string, UInt32>>();
		private static void CheckTileAttributeValues(StringBuilder sb, XDocument doc, Tile t)
		{
			var tileElement = doc.Element("definition");
			var attributes = tileElement.Attributes().XAttributeToDictionary();

			foreach (var kvp in attributes)
			{
				var name = kvp.Key;
				var value = kvp.Value;

				AddAttributeValue(attributeValues, name, value);
			}

			foreach (AbstractTileItem itemBase in t.AllTileItems())
			{
				var elementName = itemBase.meta_elementName;

				foreach (var attribute in itemBase.meta_constructor_attributes)
				{
					var attributeName = attribute.Key;
					var attributeValue = attribute.Value;
					AddAttributeValue(attributeValues, $"{elementName}/{attributeName}", attributeValue);

					string elemString = null;
					if (attributeName == "use_case" && attributeValue != "0")
					{
						elemString ??= itemBase.ToXElement().ToString();

						elemString = elemString.Replace("use_case", "use_case".Pastel(Color.Red));
					}

					if (attributeName == "interact_type" && attributeValue == "0")
					{
						elemString ??= itemBase.ToXElement().ToString();

						elemString = elemString.Replace
							("interact_type", "interact_type".Pastel(colors[attributeValue]));
					}


					if (null != elemString)
					{
						NotableThings.AppendLine(elemString);
						NotableThings.AppendLine();
					}
				}
			}
		}

		private static void AddAttributeValue
		(
			Dictionary<string, Dictionary<string, UInt32>> collection
		  , string                                         attributeName
		  , string                                         attributeValue
		)
		{
			if (!collection.ContainsKey(attributeName))
			{
				collection[attributeName] = new Dictionary<string, uint>();
			}

			collection[attributeName].IncrementOrCreateKey(attributeValue);
		}
	}
}
