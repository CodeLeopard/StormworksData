﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Linq;

namespace CommandLine
{
	[Obsolete("Use SystemExtensions instead", false)]
	public class ByteArrayComparer : EqualityComparer<byte[]>
	{
		// https://stackoverflow.com/a/30353296

		public override bool Equals(byte[] first, byte[] second)
		{
			if (first == null || second == null)
			{
				// null == null returns true.
				// non-null == null returns false.
				return first == second;
			}
			if (ReferenceEquals(first, second))
			{
				return true;
			}
			if (first.Length != second.Length)
			{
				return false;
			}
			// Linq extension method is based on IEnumerable, must evaluate every item.
			return first.SequenceEqual(second);
		}


		// This implementation works great if you assume the byte[] arrays
		// are themselves cryptographic hashes. It probably works alright too,
		// for general-purpose byte arrays.
		public override int GetHashCode(byte[] obj)
		{
			if (obj == null)
			{
				throw new ArgumentNullException("obj");
			}
			if (obj.Length >= 4)
			{
				return BitConverter.ToInt32(obj, 0);
			}
			// Length occupies at most 2 bits. Might as well store them in the high order byte
			int value = obj.Length;
			foreach (var b in obj)
			{
				value <<= 8;
				value += b;
			}
			return value;
		}
	}
}
