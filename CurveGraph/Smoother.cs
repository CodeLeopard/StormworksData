﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/


// This file incorporates work covered by the following copyright:
// Copyright (c) 2017 Benoît
// The Original, licensed MIT can be found here: https://github.com/methusalah/SplineMesh



using System;
using System.Diagnostics.Contracts;

using OpenToolkit.Mathematics;

using Shared;

namespace CurveGraph
{
	public static class Smoother
	{
		[Pure]
		public static Vector3 GetSmoothDirection(CurveNode node)
		{
			if(null == node) throw new ArgumentNullException(nameof(node));
			if (node.Connections.Count == 0) return node.Direction;

			// New direction is weighted sum of vectors towards connected nodes.


			// First compute the total weight

			float connectionCount = 0; // Backup in case total is 0.
			float weightTotal = 0;
			foreach (var kvp in node.Connections)
			{
				var cNode = kvp.Key;
				var cConnection = kvp.Value;
				var settings = cConnection.GetNodeSettings(cNode);
				var weight = settings.SmoothingWeight;

				weightTotal += weight;
				connectionCount++;
			}

			float weightModifier = 1 / weightTotal;
			float weightAdjustment = 0;

			// If all connections contribute 0 it will break the math.
			if (float.IsNaN(weightModifier) || float.IsInfinity(weightModifier))
			{
				// Re-compute the total weight based on count.
				weightModifier = 1 / connectionCount;

				// So in that case they all contribute 1.
				weightAdjustment = 1;
			}


			// Compute the new direction.

			var Position = node.Position;
			var newDirection = Vector3.Zero;

			float averageMagnitude = 0;

			foreach (var kvp in node.Connections)
			{
				var cNode = kvp.Key;
				var cConnection = kvp.Value;
				var settings = cConnection.GetNodeSettings(cNode);
				var correctedWeight = (settings.SmoothingWeight + weightAdjustment) * weightModifier;

				var toOtherPos = Position - cNode.Position;
				averageMagnitude += toOtherPos.Length * correctedWeight;

				// The Direction may be reversed, this behaviour is defined in CubicBezierCurve.//
				if (Vector3.Dot(node.Direction, toOtherPos) < 0)
				{
					toOtherPos = -toOtherPos;
				}

				newDirection += toOtherPos * correctedWeight;
			}

			averageMagnitude *= 0.5f;
			// This constant should vary between 0 and 0.5, and allows to add more or less smoothness.
			newDirection = newDirection.Normalized() * averageMagnitude * node.Curvature;

			if (! VectorExt.IsFinite(newDirection))
				throw new InvalidOperationException($"Smoothing resulted in non-Finite result {newDirection}");
			if (newDirection == Vector3.Zero)
				throw new InvalidOperationException($"Smoothing resulted in Zero Direction.");

			return newDirection;
		}
	}
}
