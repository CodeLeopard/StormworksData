// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/


// This file incorporates work covered by the following copyright:
// Copyright (c) 2017 Benoît
// The Original, licensed MIT can be found here: https://github.com/methusalah/SplineMesh



using System;
using System.Linq;
using System.Runtime.Serialization;

using CurveGraph.Interface;

using OpenToolkit.Mathematics;

using Shared;

using Mathf = OpenToolkit.Mathematics.MathHelper;

namespace CurveGraph
{
	/// <summary>
	/// Mathematical object for cubic Bézier curve definition.
	/// It is made of two spline nodes which hold the four needed control points : two positions and two directions
	/// It provides methods to get positions and tangent along the curve, specifying a distance or a ratio, plus the curve length.
	/// 
	/// Note that a time of 0.5 and half the total distance won't necessarily define the same curve point as the curve curvature is not linear.
	/// </summary>
	[Serializable]
	[DataContract(Namespace = "")]
	public class CubicBezierCurve : ICloneForMTR<CubicBezierCurve>, IDeserializationCallback
	{
		private const int STEP_COUNT = 30 * 4; // * 4 to add more detail.
		private const int SAMPLE_ARRAY_LENGTH = STEP_COUNT;
		private const float T_STEP = 1.0f / (STEP_COUNT - 1);

		[NonSerialized]
		private CurveSample[] samples;

		[DataMember(Name = "StartNode")]
		protected CurveNode startNode;
		[DataMember(Name = "EndNode")]
		protected CurveNode endNode;

		[DataMember(Name = "StartMagnitudeOverride")]
		protected float? _startMagnitudeOverride = null;
		[DataMember(Name = "EndMagnitudeOverride")]
		protected float?   _endMagnitudeOverride = null;



		// todo: do we need to be able to set these?
		// todo: why not create a new one?
		public CurveNode StartNode
		{
			get => startNode;
			set => SetNode(ref startNode, value);
		}

		public CurveNode EndNode
		{
			get => endNode;
			set => SetNode(ref endNode, value);
		}

		private void SetNode(ref CurveNode current, CurveNode value)
		{
			if (value   == null) throw new ArgumentNullException(nameof(value));
			if (current == value) return;

			current.NodeValuesChanged -= ComputeSamples;
			current = value;
			current.NodeValuesChanged += ComputeSamples;

			ComputeSamples(current);
		}


		public float? StartMagnitudeOverride
		{
			get => _startMagnitudeOverride;
			set
			{
				if (value == _startMagnitudeOverride) return;
				//if (value <= 0) return;

				_startMagnitudeOverride = value;

				ComputeSamples(null);
			}
		}

		public float? EndMagnitudeOverride
		{
			get => _endMagnitudeOverride;
			set
			{
				if (value == _endMagnitudeOverride) return;
				//if (value <= 0) return;

				_endMagnitudeOverride = value;

				ComputeSamples(null);
			}
		}


		/// <summary>
		/// Length of the curve in world unit.
		/// </summary>
		public float Length { get; private set; }

		/// <summary>
		/// This event is raised when the <see cref="CubicBezierCurve"/> has changed, for example when one of of the control points has moved.
		/// </summary>
		[field: NonSerialized]
		public event Action<CubicBezierCurve> Changed;

		/// <summary>
		/// The Position of the <see cref="CubicBezierCurve"/>, specifically the midpoint between <see cref="StartNode"/> and <see cref="EndNode"/>.
		/// </summary>
		public Vector3 Position => (startNode.Position + endNode.Position) / 2;

		/// <summary>
		/// The Control point for the start node.
		/// </summary>
		public Vector3 StartControlPoint
		{
			get
			{
				if (! _startMagnitudeOverride.HasValue || _startMagnitudeOverride.Value <= 0)
				{
					return startNode.Position + startDirection;
				}
				else
				{
					return startNode.Position + (startDirection.Normalized() * _startMagnitudeOverride.Value);
				}
			}
		}

		/// <summary>
		/// The Control point for the end node.
		/// </summary>
		public Vector3 EndControlPoint
		{
			get
			{
				if (!_endMagnitudeOverride.HasValue || _endMagnitudeOverride.Value <= 0)
				{
					return endNode.Position + endDirection;
				}
				else
				{
					return endNode.Position + (endDirection.Normalized() * _endMagnitudeOverride.Value);
				}
			}
		}

		/// <summary>
		/// The EndDirection as it is actually used in the current Curve.
		/// It may differ from the direction of <see cref="StartNode"/> for various reasons.
		/// Value is assigned in <see cref="ComputeSamples"/>
		/// </summary>
		[NonSerialized]
		private Vector3 startDirection;

		/// <summary>
		/// The EndDirection as it is actually used in the current Curve.
		/// It may differ from the direction of <see cref="EndNode"/> for various reasons.
		/// Value is assigned in <see cref="ComputeSamples"/>
		/// </summary>
		[NonSerialized]
		private Vector3 endDirection;

		/// <summary>
		/// Build a new cubic Bézier curve between two given spline node.
		/// </summary>
		/// <param name="n1"></param>
		/// <param name="n2"></param>
		public CubicBezierCurve(CurveNode n1, CurveNode n2)
		{
			startNode = n1;
			endNode   = n2;

			Initialize(true);
		}

		public CubicBezierCurve(CubicBezierCurve toCopy, bool connectEvents = true)
		{
			startNode = toCopy.startNode;
			endNode   = toCopy.endNode;
			samples   = (CurveSample[]) toCopy.samples.Clone();
			Length    = toCopy.Length;

			if(connectEvents) ConnectEvents();
		}

		public CubicBezierCurve CloneForMultiThreadedReading()
		{
			return new CubicBezierCurve(this, false);
		}

		/// <summary>
		/// Raise the <see cref="Changed"/> <see langword="event"/>.
		/// </summary>
		public void RaiseChanged()
		{
			Changed?.Invoke(this);
		}

		private void ConnectEvents()
		{
			// Just in case
			startNode.NodeValuesChanged -= ComputeSamples;
			endNode.NodeValuesChanged -= ComputeSamples;

			startNode.NodeValuesChanged += ComputeSamples;
			endNode.NodeValuesChanged += ComputeSamples;
		}


		private void Initialize(bool connectEvents = true)
		{
			if (connectEvents) ConnectEvents();
			ComputeSamples(null);
		}


		// Bug: DataContractSerializer incorrectly calls this before the entire graph is deserialized.
		public void OnDeserialization(object sender)
		{
			Initialize(true);
		}


		/// <summary>
		/// Change both the start and end nodes of the curve.
		/// </summary>
		/// <param name="newN1"></param>
		/// <param name="newN2"></param>
		public void Connect(CurveNode newN1, CurveNode newN2)
		{
			bool changed = startNode != newN1;
			changed |= endNode != newN2;

			if (! changed) return;

			startNode.NodeValuesChanged -= ComputeSamples;
			endNode.NodeValuesChanged   -= ComputeSamples;

			startNode = newN1;
			endNode = newN2;

			startNode.NodeValuesChanged += ComputeSamples;
			endNode.NodeValuesChanged   += ComputeSamples;

			ComputeSamples(null);
		}

		#region GetSample

		/// <summary>
		/// Convenient method to get the third control point of the curve, as the direction of the end spline node indicates the starting tangent of the next curve.
		/// </summary>
		/// <returns></returns>
		public Vector3 GetInverseDirection()
		{
			return (2 * endNode.Position) - EndControlPoint;
		}

		/// <summary>
		/// Returns point on curve at given time. Time must be between 0 and 1.
		/// </summary>
		/// <param name="t"></param>
		/// <returns></returns>
		private Vector3 GetLocation(float t)
		{
			float omt = 1f - t;
			float omt2 = omt * omt;
			float omt3 = omt2 * omt;
			float t2 = t * t;
			return
				startNode.Position  * omt3 +
				StartControlPoint * (3f * omt2 * t) +
				GetInverseDirection() * (3f * omt * t2) +
				endNode.Position * (t2 * t);
		}

		/// <summary>
		/// Returns velocity (tangent with a magnitude)  of curve at given time. Time must be between 0 and 1.
		/// </summary>
		/// <param name="t"></param>
		/// <returns></returns>
		private Vector3 GetVelocity(float t)
		{
			float omt = 1f - t;
			float omt2 = omt * omt;
			float t2 = t * t;

			Vector3 velocity =
				startNode.Position * (-omt2) +
				StartControlPoint  * (3 * omt2 - 2 * omt) +
				GetInverseDirection() * (-3 * t2 + 2 * t) +
				endNode.Position * (t2);
			return velocity;
		}

		private Vector3 GetUp(float t) {
			return Vector3.Lerp(startNode.Up, endNode.Up, t);
		}

		private Vector2 GetScale(float t) {
			return Vector2.Lerp(startNode.Scale, endNode.Scale, t);
		}

		private float GetRoll(float t) {
			return Mathf.Lerp(startNode.Roll, endNode.Roll, t);
		}


		private void ComputeSamples(CurveNode sender)
		{
			samples = samples ?? new CurveSample[SAMPLE_ARRAY_LENGTH];
			Length = 0;

			// Hack to make junctions just work.
			// This makes it so that if the direction of the nodes is more than 180 degrees
			// away from the actual direction to each other their direction used as if
			// it's pointing toward each other.
			// todo: put this behaviour behind a toggle.
			var toNextNode = (startNode.Position - endNode.Position);
			bool swapStartDirection = Vector3.Dot(startNode.Direction, toNextNode) > 0;
			bool swapEndDirection   = Vector3.Dot(endNode.Direction, toNextNode) > 0;

			startDirection = startNode.Direction;
			endDirection   = endNode.Direction;

			if (swapStartDirection) startDirection = -startDirection;
			if (swapEndDirection) endDirection     = -endDirection;

			Vector3 previousPosition = GetLocation(0);
			if(true)
			{
				for (int i = 0; i < STEP_COUNT - 1; i++)
				{
					float t = i * T_STEP;
					Vector3 position = GetLocation(t);
					Length += Vector3.Distance(previousPosition, position);
					previousPosition = position;
					samples[i] = CreateSample(Length, t);
				}

				Length += Vector3.Distance(previousPosition, GetLocation(1));
				samples[SAMPLE_ARRAY_LENGTH - 1] = CreateSample(Length, 1);
			}
			else
			{
				int index = 0;
				for (float t = 0; t < 1; t += T_STEP)
				{
					Vector3 position = GetLocation(t);
					Length += Vector3.Distance(previousPosition, position);
					previousPosition = position;
					samples[index++] = CreateSample(Length, t);
				}

				Length += Vector3.Distance(previousPosition, GetLocation(1));
				samples[index] = CreateSample(Length, 1);
			}

			if (float.IsNaN(Length) || float.IsInfinity(Length))
			{
				// Bug: DataContractSerializer incorrectly causes this method to run while data is not fully deserialized.
				// todo: once DataContractSerializer is depreciated or fixed actually throw the exception.

				//throw new Exception($"Computing samples resulted in invalid {nameof(Length)}: {Length}");
			}

			Changed?.Invoke(this);
		}

		private CurveSample CreateSample(float distance, float time)
		{
			return new CurveSample(
				GetLocation(time),
				GetVelocity(time),
				GetUp(time),
				GetScale(time),
				GetRoll(time),
				distance,
				time,
				this);
		}

		/// <summary>
		/// Get the array of samples. Could be <see langword="null" />.
		/// </summary>
		/// <returns></returns>
		public CurveSample[] GetSamples()
		{
			return samples?.ToArray();
		}

		/// <summary>
		/// Copy the samples into the specified array, starting at <paramref name="index"/> in <paramref name="destination"/>.
		/// </summary>
		/// <param name="destination"></param>
		/// <param name="index"></param>
		public void CopySamplesTo(CurveSample[] destination, long index)
		{
			samples.CopyTo(destination, index);
		}

		/// <summary>
		/// Returns an interpolated sample of the curve, containing all curve data at this time.
		/// </summary>
		/// <param name="time"></param>
		/// <returns></returns>
		public CurveSample GetSample(float time)
		{
			AssertTimeInBounds(time);
			CurveSample previous = samples[0];
			CurveSample next = default(CurveSample);
			bool found = false;

			foreach (CurveSample cp in samples)
			{
				if (cp.timeInCurve >= time)
				{
					next = cp;
					found = true;
					break;
				}
				previous = cp;
			}
			if (!found) throw new Exception("Can't find curve samples.");
			float t = next == previous ? 0 : (time - previous.timeInCurve) / (next.timeInCurve - previous.timeInCurve);

			return CurveSample.Lerp(previous, next, t);
		}

		/// <summary>
		/// Returns an interpolated sample of the curve, containing all curve data at this time like <see cref="GetSample"/>.
		/// But operates in reverse, as if <see cref="StartNode"/> is <see cref="EndNode"/> and vice versa.
		/// This means that the <see cref="CurveSample.tangent"/> faces in reverse,
		/// and <see cref="CurveSample.timeInCurve"/> and <see cref="CurveSample.distanceInCurve"/>
		/// are measured from the 'normal' end of the curve instead.
		/// </summary>
		/// <param name="reverseTime">Time, from the end of the curve where the sample should be fetched. [0..1]</param>
		/// <returns></returns>
		public CurveSample GetReverseSample(float reverseTime)
		{
			AssertTimeInBounds(reverseTime);

			float forwardTime = 1 - reverseTime;
			var forwardSample = GetSample(forwardTime);

			CurveSample reverseSample = ReverseSample(forwardSample);
			return reverseSample;
		}

		/// <summary>
		/// Returns an interpolated sample of the curve, containing all curve data at this distance.
		/// </summary>
		/// <param name="distance"></param>
		/// <returns></returns>
		public CurveSample GetSampleAtDistance(float distance)
		{
			AssertDistanceInBounds(distance);

			CurveSample previous = samples[0];
			CurveSample next = default(CurveSample);
			bool found = false;
			foreach (CurveSample cp in samples)
			{
				if (cp.distanceInCurve >= distance)
				{
					next = cp;
					found = true;
					break;
				}
				previous = cp;
			}
			if (!found) throw new Exception("Can't find curve samples.");
			float t = next == previous ? 0 : (distance - previous.distanceInCurve) / (next.distanceInCurve - previous.distanceInCurve);

			return CurveSample.Lerp(previous, next, t);
		}

		/// <summary>
		/// Returns an interpolated sample of the curve, containing all curve data at this time like <see cref="GetSampleAtDistance"/>.
		/// But operates in reverse, as if <see cref="StartNode"/> is <see cref="EndNode"/> and vice versa.
		/// This means that the <see cref="CurveSample.tangent"/> faces in reverse,
		/// and <see cref="CurveSample.timeInCurve"/> and <see cref="CurveSample.distanceInCurve"/>
		/// are measured from the 'normal' end of the curve instead.
		/// </summary>
		/// <param name="reverseDistance">Distance, from the end of the curve where the sample should be fetched. [0..<see cref="Length"/>]</param>
		/// <returns></returns>
		public CurveSample GetReverseSampleAtDistance(float reverseDistance)
		{
			AssertDistanceInBounds(reverseDistance);

			float forwardDistance = Length - reverseDistance;
			var forwardSample = GetSampleAtDistance(forwardDistance);

			CurveSample reverseSample = ReverseSample(forwardSample);
			return reverseSample;
		}

		/// <summary>
		/// Reverse a <see cref="CurveSample"/> so that it faces in reverse and
		/// it's <see cref="CurveSample.timeInCurve"/> and <see cref="CurveSample.distanceInCurve"/>
		/// are measured from the end of the curve.
		/// </summary>
		/// <remarks>
		/// Note: Reversing an already reversed sample produces an INVALID sample.
		/// </remarks>
		/// <param name="forwardSample"></param>
		/// <returns>reverseSample</returns>
		public CurveSample ReverseSample(CurveSample forwardSample)
		{
			CurveSample reverseSample
				= new CurveSample(
					forwardSample.location,
					-forwardSample.tangent,
					forwardSample.up,
					forwardSample.scale,
					forwardSample.roll,
					Length - forwardSample.distanceInCurve,
					1      - forwardSample.timeInCurve,
					this);

			return reverseSample;
		}

		private static void AssertTimeInBounds(float time)
		{
			if (time < 0 || time > 1)
				throw new ArgumentOutOfRangeException(nameof(time), time,"[0..1].");
		}

		private void AssertDistanceInBounds(float distance)
		{
			if (distance < 0 || distance > Length)
				throw new ArgumentOutOfRangeException(nameof(distance), distance, $"[0..{Length}]");
		}

		public CurveSample GetProjectionSample(Vector3 pointToProject)
		{
			float minSqrDistance = float.PositiveInfinity;
			int closestIndex = -1;
			int i = 0;
			foreach (var sample in samples)
			{
				float sqrDistance = (sample.location - pointToProject).LengthSquared;
				if (sqrDistance < minSqrDistance) {
					minSqrDistance = sqrDistance;
					closestIndex = i;
				}
				i++;
			}
			CurveSample previous, next;
			if(closestIndex == 0)
			{
				previous = samples[closestIndex];
				next = samples[closestIndex + 1];
			}
			else if(closestIndex == SAMPLE_ARRAY_LENGTH - 1)
			{
				previous = samples[closestIndex - 1];
				next = samples[closestIndex];
			}
			else
			{
				var toPreviousSample = (pointToProject - samples[closestIndex - 1].location).LengthSquared;
				var toNextSample = (pointToProject - samples[closestIndex + 1].location).LengthSquared;
				if (toPreviousSample < toNextSample)
				{
					previous = samples[closestIndex - 1];
					next = samples[closestIndex];
				}
				else
				{
					previous = samples[closestIndex];
					next = samples[closestIndex + 1];
				}
			}

			var onCurve = VectorExt.Project(pointToProject - previous.location, next.location - previous.location) + previous.location;
			var rate = (onCurve - previous.location).LengthSquared / (next.location - previous.location).LengthSquared;
			rate = Mathf.Clamp(rate, 0, 1);
			var result = CurveSample.Lerp(previous, next, rate);
			return result;
		}

		/// <summary>
		/// Returns a CurveSample that is closest to the target position.
		/// </summary>
		/// <param name="target"></param>
		/// <returns></returns>
		public CurveSample GetNearSample(Vector3 target)
		{
			if (! VectorExt.IsFinite(target))
				throw new ArgumentException($"{nameof(target)}, may not be NaN or infinite.", nameof(target));

			int closestIndex = -1;
			int secondIndex = -1;
			float closestDistance = float.PositiveInfinity;
			float secondDistance = float.PositiveInfinity;

			for (int i = 0; i < samples.Length; i++)
			{
				var s = samples[i];

				float d = (target - s.location).LengthSquared;

				if (d < closestDistance)
				{
					secondIndex = closestIndex;
					closestIndex = i;

					secondDistance = closestIndex;
					closestDistance = d;
				}
			}

			if (closestIndex == 0 || closestIndex == samples.Length - 1)
			{
				return samples[closestIndex];
			}

			Vector2 ratios = new Vector2(closestDistance, secondDistance).Normalized();

			var sa = samples[closestIndex];
			var sb = samples[secondIndex];

			return CurveSample.Lerp(sa, sb, ratios.X);
		}

		#endregion GetSample
	}
}
