﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/


// This file incorporates work covered by the following copyright:
// Copyright (c) 2017 Benoît
// The Original, licensed MIT can be found here: https://github.com/methusalah/SplineMesh



using System;

using OpenToolkit.Mathematics;

using Shared;

using Mathf = OpenToolkit.Mathematics.MathHelper;

namespace CurveGraph
{
	/// <summary>
	/// Immutable <see langword="struct"/> containing all data about a point on a <see cref="CubicBezierCurve"/>.
	/// </summary>
	[Serializable]
	public struct CurveSample : IEquatable<CurveSample>
	{
		/// <summary>
		/// The position of the <see cref="CurveSample"/>.
		/// </summary>
		public readonly Vector3 location;
		/// <summary>
		/// The '<see cref="velocity"/>' of the <see cref="CurveSample"/> the <see cref="velocity"/> points in the same direction as <see cref="tangent"/> but the magnitude depends on the magnitude of the <see cref="CurveNode.Direction"/>.
		/// </summary>
		public readonly Vector3 velocity;
		/// <summary>
		/// The <see cref="tangent"/> of the <see cref="CurveSample"/>, the <see cref="tangent"/> points in the same direction as <see cref="velocity"/> but is always of unit length.
		/// </summary>
		public readonly Vector3 tangent;
		/// <summary>
		/// The <see cref="up"/> unit vector, which is interpolated between the <see cref="CurveNode.Up"/> of either end.
		/// </summary>
		public readonly Vector3 up;
		/// <summary>
		/// The <see cref="scale"/> vector, which is interpolated between the <see cref="CurveNode.Scale"/> of either end.
		/// </summary>
		public readonly Vector2 scale;
		/// <summary>
		/// The <see cref="roll"/>, which is interpolated between the <see cref="CurveNode.Roll"/> of either end.
		/// </summary>
		public readonly float roll; // todo: what unit?
		/// <summary>
		/// The distance along the curve in world units.
		/// </summary>
		public readonly float distanceInCurve;
		/// <summary>
		/// The distance along the curve as a fraction. Note that a <see cref="timeInCurve"/> of o.5f does not necessarily correspond to the true half-way point.
		/// </summary>
		public readonly float timeInCurve;
		/// <summary>
		/// The <see cref="CubicBezierCurve"/> that this is a sample of.
		/// </summary>
		public readonly CubicBezierCurve curve;

		private Quaternion rotation;
		private static readonly Quaternion ZeroQuaternion = new Quaternion();

		/// <summary>
		/// Rotation is a look-at quaternion calculated from the tangent, roll and up vector. Mixing non zero roll and custom up vector is not advised.
		/// </summary>
		public Quaternion Rotation
		{
			get
			{
				if (rotation == ZeroQuaternion)
				{
					var crossRight = Vector3.Cross(Quaternion.FromAxisAngle(Vector3.UnitZ, roll) * up, tangent)
					                        .Normalized();
					var upVector = Vector3.Cross(tangent, crossRight);
					rotation = QuaternionExt.FromLookAt(tangent, upVector);
				}

				return rotation;
			}
		}


		public CurveSample
		(
			Vector3          location
		  , Vector3          velocity
		  , Vector3          up
		  , Vector2          scale
		  , float            roll
		  , float            distanceInCurve
		  , float            timeInCurve
		  , CubicBezierCurve curve
		)
		{
			this.location = location;
			this.velocity = velocity;
			this.tangent = velocity.Normalized();
			this.up = up;
			this.roll = roll;
			this.scale = scale;
			this.distanceInCurve = distanceInCurve;
			this.timeInCurve = timeInCurve;
			this.curve = curve ?? throw new ArgumentNullException(nameof(curve));
			rotation = ZeroQuaternion; // Sentinel value, real value only generated if needed because expensive.
		}

		/// <inheritdoc />
		public override bool Equals(object obj)
		{
			return obj is CurveSample other && Equals(other);
		}

		/// <inheritdoc />
		public override int GetHashCode()
		{
			unchecked
			{
				int hashCode = location.GetHashCode();
				hashCode = (hashCode * 397) ^ velocity.GetHashCode();
				//hashCode = (hashCode * 397) ^ tangent.GetHashCode(); // depends on velocity so redundant to compute.
				hashCode = (hashCode * 397) ^ up.GetHashCode();
				hashCode = (hashCode * 397) ^ scale.GetHashCode();
				hashCode = (hashCode * 397) ^ roll.GetHashCode();
				hashCode = (hashCode * 397) ^ distanceInCurve.GetHashCode();
				hashCode = (hashCode * 397) ^ timeInCurve.GetHashCode();
				hashCode = (hashCode * 397) ^ curve.GetHashCode();
				return hashCode;
			}
		}


		public static bool operator ==(CurveSample cs1, CurveSample cs2)
		{
			return cs1.Equals(cs2);
		}

		public static bool operator !=(CurveSample cs1, CurveSample cs2)
		{
			return ! cs1.Equals(cs2);
		}

		/// <summary>
		/// Creates a transformation <see cref="Matrix4"/> that represents the <see cref="CurveSample"/>.
		/// <see cref="scale"/> is included, but the scale along the forward (Z axis) is always one.
		/// The resulting matrix is Row Major.
		/// Note: the result is not stored but computed each time it is needed.
		/// </summary>
		public Matrix4 Transformation()
		{
			Vector3 forwardUnit = tangent;
			Vector3 rightUnit = Vector3.Cross(Quaternion.FromAxisAngle(Vector3.UnitZ, roll) * up, tangent)
			                           .Normalized();
			Vector3 upUnit = up.Normalized();

			var result = new Matrix4(); // OpenTK convention is Row-Major.

			result.Row0 = rightUnit.WithW(0) * scale.X;
			result.Row1 = upUnit.WithW(0)    * scale.Y;
			result.Row2 = forwardUnit.WithW(0);
			result.Row3 = location.WithW(1);

			return result;
		}

		/// <summary>
		/// Linearly interpolates between two curve samples.
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <param name="t"></param>
		/// <returns></returns>
		public static CurveSample Lerp(CurveSample a, CurveSample b, float t)
		{
			return new CurveSample(
				Vector3.Lerp(a.location, b.location, t),
				Vector3.Lerp(a.velocity, b.velocity, t),
				Vector3.Lerp(a.up,       b.up,       t),
				Vector2.Lerp(a.scale, b.scale, t),
				Mathf.Lerp(a.roll, b.roll, t),
				Mathf.Lerp(a.distanceInCurve, b.distanceInCurve, t),
				Mathf.Lerp(a.timeInCurve, b.timeInCurve, t),
				a.curve);
		}

		/// <inheritdoc />
		public override string ToString()
		{
			return $"{{CurveSample P:{location}, V:{velocity}, T:{timeInCurve}}}";
		}

		/// <inheritdoc />
		public bool Equals(CurveSample other)
		{
			return location.Equals(other.location)
			    && velocity.Equals(other.velocity)
			    //&& tangent.Equals(other.tangent) // depends on velocity, so redundant.
				&& up.Equals(other.up)
			    && scale.Equals(other.scale)
			    && roll.Equals(other.roll)
			    && distanceInCurve.Equals(other.distanceInCurve)
			    && timeInCurve.Equals(other.timeInCurve)
			    && Equals(curve, other.curve);
			// rotation is generated based on the other members as needed, so shouldn't matter for compare.
		}
	}
}
