﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/


using System;
using System.Runtime.Serialization;

using CurveGraph.Interface;

namespace CurveGraph
{
	/// <summary>
	/// Contains information about one side of a connection between nodes.
	/// </summary>
	[Serializable]
	[DataContract(Namespace = "")]
	public class ConnectionNodeSettings : ICloneForMTR<ConnectionNodeSettings>
	{
		[DataMember(Name = "SmoothingWeight")]
		private float _smoothingWeight = 1f;

		/// <summary>
		/// Smoothing contribution factor. If set to 0 this node will have no contribution to the new Direction.
		/// </summary>
		public float SmoothingWeight
		{
			get => _smoothingWeight;
			set
			{
				if (value < 0) return;
				if (_smoothingWeight == value) return;

				_smoothingWeight = value;
				InvokeChanged();
			}
		}


		[field: NonSerialized]
		public event Action<ConnectionNodeSettings> Changed;

		public ConnectionNodeSettings() {}

		public ConnectionNodeSettings(ConnectionNodeSettings toCopy)
		{
			_smoothingWeight = toCopy._smoothingWeight;
		}

		public ConnectionNodeSettings CloneForMultiThreadedReading()
		{
			return new ConnectionNodeSettings(this);
		}

		private void InvokeChanged()
		{
			Changed?.Invoke(this);
		}
	}
}
