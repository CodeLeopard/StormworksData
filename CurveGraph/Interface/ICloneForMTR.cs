﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/




namespace CurveGraph.Interface
{
	/// <summary>
	/// For types that implement a method to create a Clone that is suitable for multiThreaded reading.
	/// That is, changes to the original don't show up and global bookKeeping events are disabled.
	/// It's allowed (but not required) that events don't fire.
	/// </summary>
	/// <typeparam name="TConcreteType"></typeparam>
	public interface ICloneForMTR<out TConcreteType>
	{
		/// <summary>
		/// Create a copy of the object that is suitable for reading from in a multiThreaded environment.
		/// Changes to the original don't show up and global bookKeeping events are disabled.
		/// It's allowed (but not required) that other events also don't fire.
		/// </summary>
		/// <returns></returns>
		TConcreteType CloneForMultiThreadedReading();
	}
}
