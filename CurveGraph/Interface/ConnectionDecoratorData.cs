﻿// Copyright 2025 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/




using CustomSerialization.Attributes;
using Shared;
using System;
using System.Runtime.Serialization;

namespace CurveGraph.Interface
{
	/// <summary>
	/// Interface for data to be added to the links of a connection.
	/// </summary>
	[Serializable]
	[SerializerExplicit]
	[DataContract]
	public class ConnectionDecoratorData
	{
		/// <summary>
		/// Track speed in M/s.
		/// </summary>
		[VisibleProperty]
		[SerializerInclude(optional: true, omitIfDefault: true, xmlAsAttribute: true)]
		[DataMember]
		public float? TrackSpeed_ms { get; set; }

		/// <summary>
		/// Superelevation in millimeters.
		/// </summary>
		[VisibleProperty]
		[SerializerInclude(optional: true, omitIfDefault: true, xmlAsAttribute: true)]
		[DataMember]
		public float? SuperElevation_mm { get; set; }


		/// <summary>
		/// Arbitrary track name.
		/// </summary>
		[VisibleProperty]
		[SerializerInclude(optional: true, omitIfDefault: true, xmlAsAttribute: true)]
		[DataMember]
		public string? TrackName { get; set; }

		/// <summary>
		/// Catenary Specification (format TBD).
		/// </summary>
		[VisibleProperty]
		[SerializerInclude(optional: true, omitIfDefault: true, xmlAsAttribute: true)]
		[DataMember]
		public string? CatenarySpec { get; set; }
	}
}
