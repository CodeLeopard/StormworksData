﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;

namespace CurveGraph.Interface
{
	/// <summary>
	/// Interface for the <see cref="Connection.data"/> object.
	/// This interface allows the object to cause the <see cref="Connection.Changed"/> <see langword="event"/> to be raised.
	/// </summary>
	public interface IConnectionCustomData : ICloneForMTR<IConnectionCustomData>
	{
		/// <summary>
		/// An event that fires when the CustomData has changed and the connection needs to be updated.
		/// </summary>
		event Action<IConnectionCustomData> Changed;
	}
}
