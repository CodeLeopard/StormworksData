// Copyright 2022-2025 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/


// This file incorporates work covered by the following copyright:
// Copyright (c) 2017 Benoît
// The Original, licensed MIT can be found here: https://github.com/methusalah/SplineMesh



using CurveGraph.Interface;
using CustomSerialization.Attributes;
using OpenToolkit.Mathematics;
using System;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace CurveGraph
{
	/// <summary>
	/// A <see cref="Connection"/> between <see cref="CurveNode"/>s.
	/// </summary>
	[Serializable]
	[DataContract(IsReference = true, Namespace = "")]
	public class Connection : ICloneForMTR<Connection>
	{
		[DataMember(Name = "Curve")]
		public readonly CubicBezierCurve curve;

		public CurveNode StartNode
		{
			get => curve.StartNode;
			set => ChangeNode(SetStartNode, curve.StartNode, curve.EndNode, value);
		}

		public CurveNode EndNode
		{
			get => curve.EndNode;
			set => ChangeNode(SetEndNode, curve.EndNode, curve.StartNode, value);
		}

		#region ChangeNode

		// todo: do we need to be able to set these?
		// todo: why not create a new one?
		private void SetStartNode(CurveNode newValue)
		{
			curve.StartNode = newValue;
		}

		private void SetEndNode(CurveNode newValue)
		{
			curve.EndNode = newValue;
		}

		private void ChangeNode(Action<CurveNode> setter, CurveNode current, CurveNode otherCurrent, CurveNode value)
		{
			// We use this method to prevent code duplication and all it's problems.
			// Because the setter is quite complicated, and has had bugs in the past.

			if (current == value) return;
			if(! value.CanConnectTo(otherCurrent))
				throw new InvalidOperationException("Cannot connect to that node.");

			if (!current.Remote_RemovedFromConnection(otherCurrent, value))
			{
				throw new Exception("Failed to update the internal BookKeeping on the removed node.");
			}

			if (!otherCurrent.Remote_ConnectionDestinationChanged(current, value))
			{
				throw new Exception("Failed to update the internal BookKeeping on the unchanged node.");
			}

			setter.Invoke(value);
		}
		#endregion ChangeNode


		public Vector3 Position => curve.Position;


		[DataMember(Name = "StartNodeSettings")]
		public readonly ConnectionNodeSettings startNodeSettings = new ConnectionNodeSettings();
		[DataMember(Name = "EndNodeSettings")]
		public readonly ConnectionNodeSettings endNodeSettings = new ConnectionNodeSettings();

		/// <summary>
		/// Has this connection been removed.
		/// </summary>
		public bool IsRemoved { get; protected set; }

		/// <summary>
		/// Triggered when a connection is created.
		/// </summary>
		[field: NonSerialized]
		public static event Action<Connection> Created;

		/// <summary>
		/// Triggered when the <see cref="Connection"/> is changed, for example because the <see cref="CubicBezierCurve"/> is changed.
		/// </summary>
		[field: NonSerialized]
		public event Action<Connection> Changed;

		/// <summary>
		/// Triggered when the connection is removed.
		/// </summary>
		[field: NonSerialized]
		public event Action<Connection> Removed;


		[DataMember(Name = "CustomData")]
		private IConnectionCustomData _data;

		/// <summary>
		/// Any custom data attached to this <see cref="Connection"/>.
		/// Should be serializable.
		/// </summary>
		public IConnectionCustomData data
		{
			get => _data;
			set
			{
				if(_data == value) return;

				if(null != _data)
					_data.Changed -= DataOnChanged;
				_data = value;
				if(null != _data)
					_data.Changed += DataOnChanged;
			}
		}

		/// <summary>
		/// Data used by the CurveDecorator to add to the XML representation.
		/// </summary>
		[DataMember(Name = "DecoratorData", IsRequired = false)]
		[SerializerInclude(optional: true, omitIfDefault: true)]
		public ConnectionDecoratorData? decoratorData { get; set; } = null;

		public Connection(CubicBezierCurve curve, bool raiseCreated = true, bool connectEvents = true)
		{
			this.curve = curve;

			if (connectEvents)
			{
				ConnectEvents();
			}

			if (raiseCreated) Created?.Invoke(this);
		}

		public Connection(Connection toCopy, bool deepCopy = true, bool raiseCreated = true, bool connectEvents = true)
		{
			curve = deepCopy ? toCopy.curve.CloneForMultiThreadedReading() : toCopy.curve;

			startNodeSettings = deepCopy ? toCopy.startNodeSettings.CloneForMultiThreadedReading() : toCopy.startNodeSettings;
			endNodeSettings = deepCopy ? toCopy.endNodeSettings.CloneForMultiThreadedReading() : toCopy.endNodeSettings;

			_data = deepCopy ? toCopy._data?.CloneForMultiThreadedReading() : toCopy._data;

			if (connectEvents)
			{
				ConnectEvents();
			}

			if(raiseCreated) Created?.Invoke(this);
		}


		private void ConnectEvents()
		{
			curve.Changed += Curve_Changed;
			startNodeSettings.Changed += NodeSettingsChanged;
			endNodeSettings.Changed += NodeSettingsChanged;

			if (null != _data)
			{
				_data.Changed += DataOnChanged;
			}
		}


		private void DisconnectEvents()
		{
			curve.Changed -= Curve_Changed;
			startNodeSettings.Changed -= NodeSettingsChanged;
			endNodeSettings.Changed -= NodeSettingsChanged;

			if (null != _data) _data.Changed -= DataOnChanged;
		}

		/// <summary>
		/// Create a copy of the Connection data (To get a unchanging copy for use with MultiThreading). Events won't fire on this copy.
		/// </summary>
		/// <returns></returns>
		public Connection CloneForMultiThreadedReading()
		{
			var c = new Connection(this, true, false, false);

			return c;
		}


		#region Events

		private void Curve_Changed(CubicBezierCurve obj)
		{
			RaiseChanged();
		}

		private void NodeSettingsChanged(ConnectionNodeSettings obj)
		{
			if(obj == startNodeSettings) StartNode.RaiseNodeValuesChanged(true);
			if(obj == endNodeSettings) EndNode.RaiseNodeValuesChanged(true);
			// Our Changed will be raised through Node -> Curve.
		}

		private void DataOnChanged(IConnectionCustomData obj)
		{
			RaiseChanged();
		}

		public void RaiseChanged()
		{
			Changed?.Invoke(this);
		}


		#endregion Events

		public ConnectionNodeSettings GetNodeSettings(CurveNode forNode)
		{
			if(null == forNode) throw new ArgumentNullException(nameof(forNode));

			if(TryGetNodeSettings(forNode, out var result))
				return result;
			else
				throw new ArgumentException($"{forNode} is not a node in this connection.");
		}

		public bool TryGetNodeSettings(CurveNode forNode, out ConnectionNodeSettings settings)
		{
			if (null == forNode) throw new ArgumentNullException(nameof(forNode));

			if (forNode == StartNode)
			{
				settings = startNodeSettings;
				return true;
			}

			if (forNode == EndNode)
			{
				settings = endNodeSettings;
				return true;
			}

			settings = null;

			return false;
		}

		/// <summary>
		/// Split this <see cref="Connection"/> in two, returning the created <see cref="CurveNode"/> and <see cref="Connection"/>.
		/// </summary>
		/// <returns></returns>
		[Obsolete("Use " + nameof(SplitAtTime) + "(0.5f) instead.")]
		public (CurveNode splitPointNode, Connection connection) Split()
		{
			return SplitAtTime(0.5f);
		}

		/// <summary>
		/// Split this <see cref="Connection"/> in two at the given <paramref name="time"/>, returning the created <see cref="CurveNode"/> and <see cref="Connection"/>.
		/// </summary>
		/// <returns></returns>
		public (CurveNode splitPointNode, Connection connection) SplitAtTime(float time)
		{
			var sample = curve.GetSample(time);
			return SplitAtSample(sample);
		}

		/// <summary>
		/// Split this <see cref="Connection"/> in two at the given <paramref name="distance"/>, returning the created <see cref="CurveNode"/> and <see cref="Connection"/>.
		/// </summary>
		/// <returns></returns>
		public (CurveNode splitPointNode, Connection connection) SplitAtDistance(float distance)
		{
			var sample = curve.GetSampleAtDistance(distance);
			return SplitAtSample(sample);
		}


		private (CurveNode splitPointNode, Connection connection) SplitAtSample(CurveSample sample)
		{
			float frac1 = sample.timeInCurve;
			float frac2 = 1 - sample.timeInCurve;

			float oldStartMagnitude = curve.StartMagnitudeOverride ?? StartNode.Direction.Length;
			float oldEndEndMagnitude = curve.EndMagnitudeOverride  ?? EndNode.Direction.Length;

#if TRACE
			float oldLength = curve.Length;
#endif

			CurveNode oldEnd = EndNode;

			// Create new midpoint node.
			var MidNode = new CurveNode(sample.location, sample.velocity);
			MidNode.scale = sample.scale;
			MidNode.up = sample.up;

			// Change our end node to be the new MidNode
			EndNode = MidNode;

			// Create a connection from the new MidNode to the old EndNode.
			Connection MidToEndConnection = MidNode.CreateConnection(oldEnd);


#if TRACE
			string traceMessage =
				$"Splitting curve at t={sample.timeInCurve}"
			  + $"\nPreconditions: f1={frac1:0.000}, f2={frac2:0.000}, len={oldLength:0.000}"
			  + $"\nStartMag    = {oldStartMagnitude:0.000}"
			  + $"\nSplitPntMag = {sample.velocity.Length:0.000}"
			  + $"\nEndMag      = {oldEndEndMagnitude:0.000}";
#endif

			// Start point
			curve.StartMagnitudeOverride = oldStartMagnitude    * frac1;
			// Mid point, Start side
			curve.EndMagnitudeOverride = sample.velocity.Length * frac1;
			// Mid point, End side
			MidToEndConnection.curve.StartMagnitudeOverride = sample.velocity.Length * frac2;
			// End point
			MidToEndConnection.curve.EndMagnitudeOverride = oldEndEndMagnitude       * frac2;

#if TRACE
			float combinedLength = curve.Length + MidToEndConnection.curve.Length;
			Debug.WriteLine($"{traceMessage}\n=========="
			              + $"\nPostconditions: lenA={curve.Length:0.000}, "
			                  + $"lenB={MidToEndConnection.curve.Length:0.000}, "
			                  + $"lenAB={combinedLength:0.000} "
			                  + $"(lenDiff={oldLength - combinedLength:0.0000})"
			              + $"\nStartMag  = {curve.StartMagnitudeOverride:0.000}"
			              + $"\nSplitMagA = {curve.EndMagnitudeOverride:0.000}"
			              + $"\nSplitMagB = {MidToEndConnection.curve.StartMagnitudeOverride:0.000}"
			              + $"\nEndMag    = {MidToEndConnection.curve.EndMagnitudeOverride:0.000}");
#endif

			return (MidNode, MidToEndConnection);
		}

		/// <summary>
		/// Remove this connection
		/// </summary>
		public void Remove()
		{
			StartNode.RemoveConnection(EndNode);
		}

		/// <summary>
		/// Remove the connection. Returns <see	langword="true"/> on success.
		/// </summary>
		/// <returns></returns>
		public bool TryRemove()
		{
			if (null == StartNode) return false;
			if (null == EndNode) return false;

			return StartNode.TryRemoveConnection(EndNode, out _);
		}

		internal void MarkRemoved()
		{
			if (IsRemoved) return;
			IsRemoved = true;

			DisconnectEvents();

			Removed?.Invoke(this);
		}


		[OnDeserialized]
		public void OnDeserialized(StreamingContext context)
		{
			ConnectEvents();
		}


		/// <summary>
		/// Fixup Deserialization because DataContract does not follow the spec.
		/// </summary>
		/// <param name="sender"></param>
		public void FixupAfterDeserialization(object sender)
		{
			curve.OnDeserialization(sender);
		}


		/// <inheritdoc />
		public override string ToString()
		{
			return $"{{{nameof(Connection)} {StartNode} <> {EndNode}}}";
		}

	}
}
