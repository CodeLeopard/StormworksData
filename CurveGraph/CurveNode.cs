// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/


// This file incorporates work covered by the following copyright:
// Copyright (c) 2017 Benoît
// The Original, licensed MIT can be found here: https://github.com/methusalah/SplineMesh




using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;

using CurveGraph.Interface;

using OpenToolkit.Mathematics;

using Shared;
using Shared.Exceptions;

namespace CurveGraph
{
	[Serializable]
	[DataContract(IsReference = true, Namespace = "")]
	public class CurveNode : ICloneForMTR<CurveNode>
	{
		private EventProperty<Vector3> _position;
		/// <summary>
		/// Node position.
		/// Does not trigger <see cref="NodeValuesChanged"/> event.
		/// </summary>
		[DataMember(Name = "Position")]
		public Vector3 position
		{
			get => _position.Value;
			set => _position.Silent = value;
		}

		/// <summary>
		/// Node position.
		/// Changes will trigger <see cref="NodeValuesChanged"/> event.
		/// </summary>
		public Vector3 Position
		{
			get => _position.Value;
			set => _position.Standard = value;
		}

		private EventProperty<Vector3> _direction;

		/// <summary>
		/// Node Direction (Tangent)
		/// Does not trigger <see cref="NodeValuesChanged"/> event.
		/// </summary>
		[DataMember(Name = "Direction")]
		public Vector3 direction
		{
			get => _direction.Value;
			set => _direction.Silent = value;
		}

		/// <summary>
		/// Node Direction (Tangent)
		/// Changes will trigger <see cref="NodeValuesChanged"/> event.
		/// If <see cref="AutoSmooth"/> is enabled then the <see cref="Direction"/> is computed automatically.
		/// </summary>
		public Vector3 Direction
		{
			get => _direction.Value;
			set => _direction.Standard = value;
		}


		private EventProperty<Vector3> _up;
		/// <summary>
		/// Up vector to apply at this node.
		/// Useful to specify the orientation when the tangent blend with the world UP (gimbal lock)
		/// This value is not used on the spline itself but is commonly used on bended content.
		/// Does not trigger <see cref="NodeValuesChanged"/> event.
		/// </summary>
		[DataMember(Name = "Up")]
		public Vector3 up
		{
			get => _up.Value;
			set => _up.Silent = value;
		}
		/// <summary>
		/// Up vector to apply at this node.
		/// Useful to specify the orientation when the tangent blend with the world UP (gimbal lock)
		/// This value is not used on the spline itself but is commonly used on bended content.
		/// Changes will trigger <see cref="NodeValuesChanged"/> event.
		/// </summary>
		public Vector3 Up
		{
			get => _up.Value;
			set => _up.Standard = value;
		}

		private EventProperty<Vector2> _scale;
		/// <summary>
		/// Scale to apply at this node.
		/// This value is not used on the spline itself but is commonly used on bended content.
		/// Does not trigger <see cref="NodeValuesChanged"/> event.
		/// </summary>
		[DataMember(Name = "Scale")]
		public Vector2 scale
		{
			get => _scale.Value;
			set => _scale.Silent = value;
		}
		/// <summary>
		/// Scale to apply at this node.
		/// This value is not used on the spline itself but is commonly used on bended content.
		/// Changes trigger <see cref="NodeValuesChanged"/> event.
		/// </summary>
		public Vector2 Scale
		{
			get => _scale.Value;
			set => _scale.Standard = value;
		}


		private EventProperty<float> _roll;
		/// <summary>
		/// Roll to apply at this node.
		/// This value is not used on the spline itself but is commonly used on bended content.
		/// Does not trigger <see cref="NodeValuesChanged"/> event.
		/// </summary>
		[DataMember(Name = "Roll")]
		public float roll
		{
			get => _roll.Value;
			set => _roll.Silent = value;
		}

		/// <summary>
		/// Roll to apply at this node.
		/// This value is not used on the spline itself but is commonly used on bended content.
		/// Changes will trigger <see cref="NodeValuesChanged"/>.
		/// </summary>
		public float Roll
		{
			get => _roll.Value;
			set => _roll.Standard = value;
		}

		/// <summary>
		/// Set Automatic smoothing for this Node.
		/// Does not trigger <see cref="NodeValuesChanged"/> event.
		/// </summary>
		[DataMember(Name = "AutoSmooth")]
		public bool _autoSmooth = false;

		public bool autoSmooth
		{
			get => _autoSmooth;
			set => _autoSmooth = value;
		}

		/// <summary>
		/// Set Automatic smoothing for this Node.
		/// Changes will trigger <see cref="NodeValuesChanged"/>.
		/// </summary>
		public bool AutoSmooth
		{
			get => _autoSmooth;
			set
			{
				if (_autoSmooth == value) return;
				_autoSmooth = value;
				if (_autoSmooth)
					RaiseNodeValuesChanged(true);
			}
		}


		public const float curvatureMinimum = 0.00001f;
		public const float curvatureMaximum = 5f;
		private float _curvature = 0.5f;

		/// <summary>
		/// Auto smoother curvature at this Node.
		/// Does not trigger <see cref="NodeValuesChanged"/> event.
		/// </summary>
		[DataMember(Name = "Curvature")]
		public float curvature
		{
			[DebuggerStepThrough]
			get => _curvature;
			set
			{
				if (! FloatExt.IsFinite(value)) throw new InvalidValueException(finiteMessage);
				if (value < curvatureMinimum)
				{
					throw new InvalidValueException
						($"{nameof(curvature)} must be greater than {curvatureMinimum}, value: {value}");
				}

				if (value > curvatureMaximum)
				{
					throw new InvalidValueException
						($"{nameof(curvature)} must be less than {curvatureMaximum}, value: {value}");
				}
				_curvature = value;
			}
		}

		/// <summary>
		/// Auto smoother curvature at this Node.
		/// Changes will trigger <see cref="NodeValuesChanged"/>.
		/// </summary>
		public float Curvature
		{
			[DebuggerStepThrough]
			get => curvature;
			set
			{
				if (curvature == value) return;
				curvature = value; // Deliberate invocation of property rather than field to get validation.
				RaiseNodeValuesChanged(true);
			}
		}

		[DataMember(Name = "Connections")]
		protected readonly Dictionary<CurveNode, Connection> _connections;

		/// <summary>
		/// Connections to other nodes. Use the dedicated methods to edit the collection.
		/// </summary>
		public IReadOnlyDictionary<CurveNode, Connection> Connections;

		/* todo: implement
		/// <summary>
		/// Raised when the values that affect the <see cref="CubicBezierCurve"/>s are changed.
		/// </summary>
		[field: NonSerialized]
		public event Action<CurveNode> CurveValuesChanged;
		*/

		/// <summary>
		/// Raised when any value of this <see cref="CurveNode"/> changes.
		/// </summary>
		[field: NonSerialized]
		public event Action<CurveNode> NodeValuesChanged;


		/// <summary>
		/// Event raised when <see cref="Connections"/> changed.
		/// </summary>
		[field: NonSerialized]
		public event Action<ConnectionsChangedEventArgs> ConnectionsChanged;



		/// <summary>
		/// Any custom data attached to this <see cref="CurveNode"/>.
		/// Should be serializable.
		/// </summary>
		[DataMember(Name = "CustomData")]
		protected ICurveNodeCustomData _data;

		public ICurveNodeCustomData Data
		{
			get => _data;
			set
			{
				if (_data == value) return;

				if(null != _data)
					_data.Changed -= DataOnChanged;
				_data = value;
				if (null != _data)
					_data.Changed += DataOnChanged;
			}
		}

		#region EventProperty

		#region Validation

		internal const string finiteMessage = "Value should be finite (neither Ininite nor NaN).";
		internal const string finiteNonZeroMessage = "Value should be nonZero and finite (neither infinite nor NaN).";

		internal static bool ValidateFinite(float value)
		{
			if (! FloatExt.IsFinite(value)) throw new InvalidValueException(finiteMessage);
			return true;
		}
		internal static bool ValidateFinite(Vector2 value)
		{
			if (! VectorExt.IsFinite(value)) throw new InvalidValueException(finiteMessage);
			return true;
		}
		internal static bool ValidateFinite(Vector3 value)
		{
			if (!VectorExt.IsFinite(value)) throw new InvalidValueException(finiteMessage);
			return true;
		}
		internal static bool ValidateFiniteNonZero(Vector2 value)
		{
			if (value == Vector2.Zero) throw new InvalidValueException(finiteNonZeroMessage);
			if (!VectorExt.IsFinite(value)) throw new InvalidValueException(finiteMessage);
			return true;
		}
		internal static bool ValidateFiniteNonZero(Vector3 value)
		{
			if (value == Vector3.Zero) throw new InvalidValueException(finiteNonZeroMessage);
			if (!VectorExt.IsFinite(value)) throw new InvalidValueException(finiteMessage);
			return true;
		}

		#endregion Validation


		private void RaiseChanged()
		{
			RaiseNodeValuesChanged(false);
		}

		private void RaiseChangedSmooth()
		{
			RaiseNodeValuesChanged(true);
		}



		#endregion EventProperty

		public CurveNode()
		{
			_connections = new Dictionary<CurveNode, Connection>();
			Connections = new ReadOnlyDictionary<CurveNode, Connection>(_connections);

			SetupEventProperty();
		}

		private void SetupEventProperty()
		{
			// This needs to work in 3 cases:
			// 1 the constructor
			// 2 DataContract serialization
			// 3 BinaryFormatter serialization

			// For BinaryFormatter we only need to set the Validator, but those are readonly.
			// So instead we re-Assign the entire EventProperty, and read the serialized value from
			// the partially serialized eventProperty.


			_position =
				new EventProperty<Vector3>(_position?.Value ?? Vector3.Zero, ValidateFinite);
			_position.Changed += RaiseChangedSmooth;

			_direction =
				new EventProperty<Vector3>(_direction?.Value ?? Vector3.UnitZ, ValidateFiniteNonZero);
			_direction.Changed += RaiseChangedSmooth;

			_up =
				new EventProperty<Vector3>(_up?.Value ?? Vector3.UnitY, ValidateFiniteNonZero);
			_up.Changed += RaiseChanged;

			_scale =
				new EventProperty<Vector2>(_scale?.Value ?? Vector2.One, ValidateFiniteNonZero);
			_scale.Changed += RaiseChanged;

			_roll =
				new EventProperty<float>(_roll?.Value ?? 0, ValidateFinite);
			_roll.Changed += RaiseChanged;
		}

		public CurveNode(Vector3 position) : this()
		{
			this.position = position;
		}

		public CurveNode(Vector3 position, Vector3 direction) : this(position)
		{
			this.direction = direction;
		}

		protected CurveNode(CurveNode toCopy, bool connectEvents = true) : this(toCopy.position, toCopy.direction)
		{
			if (null == toCopy) throw new ArgumentNullException(nameof(toCopy));
			if (connectEvents)
			{
				// The property will connect the event.
				Data = toCopy._data?.CloneForMultiThreadedReading();
			}
			else
			{
				_data = toCopy._data?.CloneForMultiThreadedReading();
			}
		}

		public CurveNode CloneForMultiThreadedReading()
		{
			return new CurveNode(this);
		}

		private void DataOnChanged(bool mayNeedSmoothing)
		{
			RaiseNodeValuesChanged(mayNeedSmoothing);
		}

		/// <summary>
		/// Raise the NodeValuesChanged event.
		/// </summary>
		/// <param name="needSmoothing">Should the Smoother be triggered (if it's enabled).</param>
		public void RaiseNodeValuesChanged(bool needSmoothing)
		{
			if (needSmoothing && _autoSmooth)
			{
				direction = Smoother.GetSmoothDirection(this);

				foreach (var kvp in _connections)
				{
					kvp.Key.Remote_Smooth(this);
				}
			}

			NodeValuesChanged?.Invoke(this);
		}

		protected void Remote_Smooth(CurveNode sender)
		{
			if (_autoSmooth)
			{
				direction = Smoother.GetSmoothDirection(this);

				RaiseNodeValuesChanged(false);
			}
		}

		/// <summary>
		/// Does this node have a direct <see cref="Connection"/> to <paramref name="destination"/>?
		/// </summary>
		/// <param name="destination"></param>
		/// <returns></returns>
		public bool IsConnectedTo(CurveNode destination) => _connections.ContainsKey(destination);

		/// <summary>
		/// Try to get the direct <see cref="Connection"/> between this and <paramref name="destination"/>.
		/// </summary>
		/// <param name="destination">The destination to get the <see cref="Connection"/> for.</param>
		/// <param name="connection">The <see cref="Connection"/> if found, or null.</param>
		/// <returns>A connection was returned.</returns>
		public bool TryGetConnection(CurveNode destination, out Connection connection)
		{
			if (this == destination) throw new ArgumentException($"Cannot be connected to self.");

			return _connections.TryGetValue(destination, out connection);
		}

		/// <summary>
		/// Get the <see cref="Connection"/> to the specified <paramref name="destination"/>.
		/// </summary>
		/// <param name="destination"></param>
		/// <returns></returns>
		public Connection GetConnection(CurveNode destination)
		{
			if (this == destination) throw new ArgumentException($"Cannot be connected to self.");

			return _connections[destination];
		}


		/// <summary>
		/// This is called on the 'other' <see cref="CurveNode"/> when a <see cref="Connection"/> is added.
		/// So that it's state can be updated to match.
		/// </summary>
		/// <param name="toConnectTo"></param>
		/// <param name="connection"></param>
		protected void _Remote_AddConnection(CurveNode toConnectTo, Connection connection)
		{
			if (null == toConnectTo) throw new ArgumentNullException(nameof(toConnectTo));
			if (this == toConnectTo) throw new ArgumentException($"Cannot connect to self.", nameof(toConnectTo));
			if (IsConnectedTo(toConnectTo)) throw new InvalidOperationException($"Already connected to {toConnectTo}.");

			_connections.Add(toConnectTo, connection);
			// todo: sanity check?

			var args = new ConnectionsChangedEventArgs(this, EventFlags.Add, added: new List<(CurveNode, Connection)>() { (toConnectTo, connection) });

			ConnectionsChanged?.Invoke(args);
		}

		/// <summary>
		/// Can a connection be made to <paramref name="destination"/>.
		/// </summary>
		/// <param name="destination"></param>
		/// <returns></returns>
		public bool CanConnectTo(CurveNode destination)
		{
			if (this == destination) return false;
			if (IsConnectedTo(destination)) return false;

			return true;
		}

		/// <summary>
		/// Create a new <see cref="Connection"/> to the specified <paramref name="destination"/>.
		/// </summary>
		/// <param name="destination">The destination for the <see cref="Connection"/>.</param>
		/// <returns>The created <see cref="Connection"/>.</returns>
		public Connection CreateConnection(CurveNode destination)
		{
			if(null == destination) throw new ArgumentNullException(nameof(destination));
			if(this == destination) throw new ArgumentException($"Cannot connect to self.", nameof(destination));
			if(IsConnectedTo(destination)) throw new InvalidOperationException($"Already connected to {destination}.");

			var curve = new CubicBezierCurve(this, destination);

			var connection = new Connection(curve);

			_connections.Add(destination, connection);

			destination._Remote_AddConnection(this, connection);

			var args = new ConnectionsChangedEventArgs(this, EventFlags.Add, added: new List<(CurveNode, Connection)>() { (destination, connection) });

			ConnectionsChanged?.Invoke(args);

			return connection;
		}

		/// <summary>
		/// This is called on the 'other' <see cref="CurveNode"/> when a <see cref="Connection"/> is removed.
		/// So that it's state can be updated to match.
		/// </summary>
		/// <param name="toRemove"></param>
		protected void _Remote_RemoveConnection(CurveNode toRemove)
		{
			if (_connections.TryGetValue(toRemove, out var removed))
			{
				var args = new ConnectionsChangedEventArgs
					(this, EventFlags.Remove, removed: new List<(CurveNode, Connection)>() { (toRemove, removed) });

				_connections.Remove(toRemove);

				ConnectionsChanged?.Invoke(args);
			}
		}

		/// <summary>
		/// Attempt to remove the <see cref="Connection"/> to the specified <paramref name="destination"/>.
		/// </summary>
		/// <param name="destination">The destination node to identify the <see cref="Connection"/> with.</param>
		/// <param name="removed">The removed <see cref="Connection"/>.</param>
		/// <returns>A <see cref="Connection"/> was removed and returned.</returns>
		public bool TryRemoveConnection(CurveNode destination, out Connection removed)
		{
			if (null == destination) throw new ArgumentNullException(nameof(destination));
			if (this == destination) throw new ArgumentException($"Cannot be connected to self.");

			if (_connections.TryGetValue(destination, out removed))
			{
				var args = new ConnectionsChangedEventArgs(this, EventFlags.Remove, removed: new List<(CurveNode, Connection)>() { (destination, removed) });

				_connections.Remove(destination);

				removed.MarkRemoved();

				destination._Remote_RemoveConnection(this);

				ConnectionsChanged?.Invoke(args);

				return true;
			}

			return false;
		}

		/// <summary>
		/// Remove the connection to the specified destination.
		/// </summary>
		/// <param name="destination">The destination node to identify the <see cref="Connection"/> with.</param>
		/// <returns>The removed connection.</returns>
		public Connection RemoveConnection(CurveNode destination)
		{
			if (TryRemoveConnection(destination, out var connection)) return connection;
			else throw new KeyNotFoundException($"There is no connection to {nameof(destination)}.");
		}

		/// <summary>
		/// Remove all connections.
		/// </summary>
		public void ClearConnections()
		{
			if (_connections.Count <= 0) return;

			var args = new ConnectionsChangedEventArgs(this, EventFlags.Clear, removed: _connections.Select(kvp => (kvp.Key, kvp.Value)).ToList());

			foreach (var kvp in _connections)
			{
				var otherNode = kvp.Key;
				var connection = kvp.Value;

				connection.MarkRemoved();
				otherNode._Remote_RemoveConnection(this);
			}

			_connections.Clear();

			ConnectionsChanged?.Invoke(args);
		}

		/// <summary>
		/// Absorb the given <see cref="CurveNode"/> into this: move all connection of <paramref name="other"/> to this instead.
		/// </summary>
		/// <param name="other"></param>
		public void Absorb(CurveNode other)
		{
			// todo: raise single event
			foreach (var kvp in other._connections.ToArray())
			{
				var destination = kvp.Key;

				other.ReplaceThisInConnection(destination, this);
			}
		}

		/// <summary>
		/// A <see cref="Connection"/> start or end was changed, we ware removed.
		/// </summary>
		/// <param name="destination">The destination by which the <see cref="Connection"/> will be identified.</param>
		/// <param name="newNode">The node that has replaced us in the connection.</param>
		/// <returns>True if the connection was found, false otherwise (failed).</returns>
		internal bool Remote_RemovedFromConnection(CurveNode destination, CurveNode newNode)
		{
			if (_connections.TryGetValue(destination, out var connection))
			{
				_connections.Remove(destination);

				var args = new ConnectionsChangedEventArgs(this, EventFlags.Remove, removed: new List<(CurveNode, Connection)>(){(destination, connection)});
				ConnectionsChanged?.Invoke(args);

				newNode._Remote_AddConnection(destination, connection);

				return true;
			}
			else
				return false;
		}

		/// <summary>
		/// A <see cref="Connection"/> start or end was changed, we need to update the destination.
		/// </summary>
		/// <param name="oldDestination">Old destination</param>
		/// <param name="newDestination">New destination</param>
		/// <returns>True if the connection was found, false otherwise (failed).</returns>
		internal bool Remote_ConnectionDestinationChanged(CurveNode oldDestination, CurveNode newDestination)
		{
			if (_connections.TryGetValue(oldDestination, out var connection))
			{
				_connections.Remove(oldDestination);
				_connections.Add(newDestination, connection);

				var args = new ConnectionsChangedEventArgs(this, EventFlags.Move, moved: new List<(CurveNode, CurveNode, Connection)>() { (oldDestination, newDestination, connection) });
				ConnectionsChanged?.Invoke(args);

				return true;
			}
			else
				return false;
		}

		/// <summary>
		/// Change the <see cref="Connection"/> to <paramref name="destination"/> by replacing this node with <paramref name="replacement"/>.
		/// </summary>
		/// <param name="destination">The destination <see cref="CurveNode"/> to lookup the <see cref="Connection"/> for.</param>
		/// <param name="replacement">The <see cref="CurveNode"/> that will replace this in the <see cref="Connection"/>.</param>
		/// <returns>True if success, false otherwise.</returns>
		public bool ReplaceThisInConnection(CurveNode destination, CurveNode replacement)
		{
			if (_connections.TryGetValue(destination, out var connection))
			{
				if (connection.StartNode == this)
				{
					connection.StartNode = replacement;
				}
				else
				{
					connection.EndNode = replacement;
				}

				// ConnectionsChanged will be raised through the property of connection via ConnectionMoved.

				return true;
			}

			return false;
		}

		/// <summary>
		/// Change the <see cref="Connection"/> to <paramref name="oldDestination"/> to instead go to <paramref name="newDestination"/>.
		/// </summary>
		/// <param name="oldDestination">The destination <see cref="CurveNode"/> to lookup the <see cref="Connection"/> for.</param>
		/// <param name="newDestination">The <see cref="CurveNode"/> that will replace the <paramref name="oldDestination"/> in the <see cref="Connection"/>.</param>
		/// <returns>True if success, false otherwise.</returns>
		public bool ReplaceDestinationInConnection(CurveNode oldDestination, CurveNode newDestination)
		{
			if (_connections.TryGetValue(oldDestination, out var connection))
			{
				if (connection.StartNode == oldDestination)
				{
					connection.StartNode = newDestination;
				}
				else
				{
					connection.EndNode = newDestination;
				}

				// ConnectionsChanged will be raised through the property of connection via ConnectionMoved.

				return true;
			}

			return false;
		}

		/// <summary>
		/// Split the <see cref="CurveNode"/> in two.
		/// A new node is created, and connections are moved to it based on their outgoing directions.
		/// Connections heading along our <see cref="Direction"/> will stay.
		/// Connections heading opposite are moved to the new Node.
		/// Note: it is not guaranteed that any connections will be moved.
		/// </summary>
		/// <returns>The new <see cref="CurveNode"/></returns>
		public CurveNode Split()
		{
			var newNode = new CurveNode(position, direction);

			foreach (var kvp in _connections.ToArray())
			{
				var destinationNode = kvp.Key;

				var toDestination = destinationNode.position - position;

				if (Vector3.Dot(toDestination, direction) < 0)
				{
					ReplaceThisInConnection(destinationNode, newNode);
				}
			}

			return newNode;
		}


		/// <summary>
		/// All nodes reachable from this node (including this). Ordered closest first (in terms of hops).
		/// </summary>
		/// <remarks>
		/// This <see cref="CurveNode"/> will always be returned first.
		/// </remarks>
		/// <param name="additionalStartNodes">
		/// Additional nodes to investigate. This will allow discovery of nodes from unconnected subnets.
		/// </param>
		/// <returns></returns>
		public IEnumerable<CurveNode> NetworkNodes(IEnumerable<CurveNode> additionalStartNodes = null)
		{
			HashSet<CurveNode> seenNodes = new HashSet<CurveNode>();
			Queue<CurveNode> nodeQueue;

			if (null != additionalStartNodes)
			{
				nodeQueue = new Queue<CurveNode>(additionalStartNodes.Prepend(this));
			}
			else
			{
				nodeQueue = new Queue<CurveNode>();
				nodeQueue.Enqueue(this);
			}


			while (nodeQueue.Any())
			{
				var current = nodeQueue.Dequeue();

				yield return current;

				seenNodes.Add(current);

				foreach (var kvp in current.Connections)
				{
					var otherNode = kvp.Key;

					if (seenNodes.Add(otherNode))
					{
						nodeQueue.Enqueue(otherNode);
					}
				}
			}
		}


		/// <summary>
		/// All connections reachable from this node. Ordered closes first (in terms of hops).
		/// </summary>
		/// <remarks>
		/// Connections from this node are returned first.
		/// </remarks>
		/// <param name="additionalStartNodes">
		/// Additional nodes to investigate. This will allow discovery of connections from unconnected subnets.
		/// </param>
		/// <returns></returns>
		public IEnumerable<Connection> NetworkConnections(IEnumerable<CurveNode> additionalStartNodes = null)
		{
			HashSet<CurveNode> seenNodes = new HashSet<CurveNode>();
			HashSet<Connection> seenConnections = new HashSet<Connection>();

			Queue<CurveNode> nodeQueue;
			if (null != additionalStartNodes)
			{
				nodeQueue = new Queue<CurveNode>(additionalStartNodes.Prepend(this));
			}
			else
			{
				nodeQueue = new Queue<CurveNode>();
				nodeQueue.Enqueue(this);
			}

			while (nodeQueue.Any())
			{
				var current = nodeQueue.Dequeue();

				seenNodes.Add(current);

				foreach (var kvp in current.Connections)
				{
					var otherNode = kvp.Key;
					var connection = kvp.Value;

					if (seenNodes.Add(otherNode))
					{
						nodeQueue.Enqueue(otherNode);
					}

					if (seenConnections.Add(connection))
					{
						yield return connection;
					}
				}
			}
		}

		/// <summary>
		/// All nodes and connections reachable from this node (including this node). Ordered closest first (in terms of hops).
		/// returns a <see cref="NetworkObject"/> containing *either* a <see cref="CurveNode"/> or a <see cref="Connection"/>.
		/// </summary>
		/// <remarks>
		/// This <see cref="CurveNode"/> will always be returned first.
		/// </remarks>
		/// <returns></returns>
		public IEnumerable<NetworkObject> NetworkNodesAndConnections(IEnumerable<CurveNode> additionalStartNodes = null)
		{
			HashSet<CurveNode> seenNodes = new HashSet<CurveNode>();
			HashSet<Connection> seenConnections = new HashSet<Connection>();

			Queue<CurveNode> nodeQueue;
			if (null != additionalStartNodes)
			{
				nodeQueue = new Queue<CurveNode>(additionalStartNodes.Prepend(this));
			}
			else
			{
				nodeQueue = new Queue<CurveNode>();
				nodeQueue.Enqueue(this);
			}

			while (nodeQueue.Any())
			{
				var current = nodeQueue.Dequeue();
				yield return new NetworkObject(current);

				seenNodes.Add(current);

				foreach (var kvp in current.Connections)
				{
					var otherNode = kvp.Key;
					var connection = kvp.Value;

					if (seenNodes.Add(otherNode))
					{
						nodeQueue.Enqueue(otherNode);
					}

					if (seenConnections.Add(connection))
					{
						yield return new NetworkObject(connection);
					}
				}
			}
		}

		[OnDeserializing]
		private void OnDeserializing(StreamingContext context)
		{
			// For DataContract.
			// In the case of dataContract the EventProperty is not serialized at all.
			// Instead the [DataMember]s are.
			// before they are set the EventProperty needs to exist.
			SetupEventProperty();
		}

		[OnDeserialized]
		public void OnDeserialized(StreamingContext context)
		{
			// For BinaryFormatter.
			// In the case of BinaryFormatter we need to assign any non-serializable information
			// of the EventProperty, like the Validators and Comparer because those can't be serialized.
			SetupEventProperty();

			Connections = Connections ?? new ReadOnlyDictionary<CurveNode, Connection>(_connections);

			if (null != _data)
			{
				_data.Changed -= DataOnChanged;
				_data.Changed += DataOnChanged;
			}
		}


		/// <inheritdoc />
		public override string ToString()
		{
			return $"{{{nameof(CurveNode)} @{_position} #{_connections.Count}}}";
		}

		/// <summary>
		/// An Entry in a CurveGraph network.
		/// Contains either a <see cref="CurveNode"/> or a <see cref="Connection"/>.
		/// </summary>
		public readonly struct NetworkObject
		{
			public readonly bool IsNode;
			public readonly CurveNode Node;
			public readonly Connection Connection;

			internal NetworkObject(CurveNode node)
			{
				IsNode = true;
				Node = node;
				Connection = null;
			}

			internal NetworkObject(Connection connection)
			{
				IsNode = false;
				Node = null;
				Connection = connection;
			}
		}




		[Flags]
		public enum EventFlags
		{
			Add    = 1 << 0,
			Remove = 1 << 1,
			Clear = (1 << 2) | Remove,
			Move   = 1 << 3,
		}

		/// <summary>
		/// Details for how the <see cref="CurveGraph.CurveNode.Connections"/> of a <see cref="CurveNode"/> have changed.
		/// </summary>
		public class ConnectionsChangedEventArgs : EventArgs
		{
			public readonly CurveNode CurveNode;
			public readonly EventFlags EventFlags;

			public readonly List<
				(CurveNode destination,
				Connection connection)
			> AddedConnections;

			public readonly List<
				(CurveNode oldDestination,
				CurveNode newDestination,
				Connection connection)
			> MovedConnections;

			public readonly List<
				(CurveNode destination,
				Connection connection)
			> RemovedConnections;

			public ConnectionsChangedEventArgs(CurveNode node, EventFlags flags, List<(CurveNode, Connection)> added = null, List<(CurveNode, CurveNode, Connection)> moved = null, List<(CurveNode, Connection)> removed = null)
			{
				CurveNode = node;
				EventFlags = flags;
				AddedConnections = added;
				MovedConnections = moved;
				RemovedConnections = removed;
			}


			/// <inheritdoc />
			public override string ToString()
			{
				var count = AddedConnections?.Count + MovedConnections?.Count + RemovedConnections?.Count;
				return $"{{{nameof(ConnectionsChangedEventArgs)} flags: {EventFlags}, node: {CurveNode}"
				     + $" #{count}}}";
			}
		}
	}
}
