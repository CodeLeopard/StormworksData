﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/


// This file incorporates work covered by the following copyright.
// The original can be found here: https://github.com/opentk/opentk

//
// Box3i.cs
//
// Copyright (C) 2019 OpenTK
//
// This software may be modified and distributed under the terms
// of the MIT license. See the MIT_LICENSE.txt file for details.
//

using System;
using System.Diagnostics.Contracts;
using System.Globalization;
using System.Runtime.InteropServices;

using OpenToolkit.Mathematics;

namespace Shared
{
	/// <summary>
	/// Defines an axis-aligned 2d box (rectangle).
	/// </summary>
	[StructLayout(LayoutKind.Sequential)]
	public struct Bounds3i : IEquatable<Bounds3i>
	{
		private Vector3i _min;

		/// <summary>
		/// Gets or sets the minimum boundary of the structure.
		/// </summary>
		public Vector3i Min
		{
			get => _min;
			set
			{
				if (value.X > _max.X)
				{
					_max.X = value.X;
				}
				if (value.Y > _max.Y)
				{
					_max.Y = value.Y;
				}
				if (value.Z > _max.Z)
				{
					_max.Z = value.Z;
				}

				_min = value;
			}
		}

		private Vector3i _max;

		/// <summary>
		/// Gets or sets the maximum boundary of the structure.
		/// </summary>
		public Vector3i Max
		{
			get => _max;
			set
			{
				if (value.X < _min.X)
				{
					_min.X = value.X;
				}
				if (value.Y < _min.Y)
				{
					_min.Y = value.Y;
				}
				if (value.Z < _min.Z)
				{
					_min.Z = value.Z;
				}

				_max = value;
			}
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="Bounds3"/> struct.
		/// When <paramref name="initEmpty"/> is specified <see cref="Min"/> and <see cref="Max"/> will be set to infinity so
		/// that the <see cref="Inflate"/> operation can be used to populate the bounds.
		/// </summary>
		/// <param name="initEmpty"></param>
		public Bounds3i(bool initEmpty)
		{
			if (initEmpty)
			{
				_min = new Vector3i(int.MaxValue);
				_max = new Vector3i(int.MinValue);
			}
			else
			{
				_min = new Vector3i(0);
				_max = new Vector3i(0);
			}
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="Bounds3i"/> struct.
		/// </summary>
		/// <param name="min">The minimum point on the XY plane this box encloses.</param>
		/// <param name="max">The maximum point on the XY plane this box encloses.</param>
		public Bounds3i(Vector3i min, Vector3i max)
		{
			_min = Vector3i.ComponentMin(min, max);
			_max = Vector3i.ComponentMax(min, max);
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="Bounds3i"/> struct.
		/// </summary>
		/// <param name="minX">The minimum X value to be enclosed.</param>
		/// <param name="minY">The minimum Y value to be enclosed.</param>
		/// <param name="minZ">The minimum Z value to be enclosed.</param>
		/// <param name="maxX">The maximum X value to be enclosed.</param>
		/// <param name="maxY">The maximum Y value to be enclosed.</param>
		/// <param name="maxZ">The maximum Z value to be enclosed.</param>
		public Bounds3i(int minX, int minY, int minZ, int maxX, int maxY, int maxZ)
			: this(new Vector3i(minX, minY, minZ), new Vector3i(maxX, maxY, maxZ))
		{
		}

		/// <summary>
		/// Gets a vector describing the size of the Bounds3i structure.
		/// </summary>
		public Vector3i Size
		{
			get => Max - Min;
		}

		/// <summary>
		/// Gets or sets a vector describing half the size of the box.
		/// </summary>
		public Vector3i HalfSize
		{
			get => Size / 2;
			set
			{
				Vector3i center = new Vector3i((int)Center.X, (int)Center.Y, (int)Center.Z);
				_min = center - value;
				_max = center + value;
			}
		}

		/// <summary>
		/// Gets a vector describing the center of the box.
		/// </summary>
		/// to avoid annoying off-by-one errors in box placement, no setter is provided for this property
		public Vector3 Center
		{
			get => ((_min + _max).ToVector3() * 0.5f) + _min.ToVector3();
		}

		/// <summary>
		/// Returns whether the box contains the specified point (borders inclusive).
		/// </summary>
		/// <param name="point">The point to query.</param>
		/// <returns>Whether this box contains the point.</returns>
		[Pure]
		public bool Contains(Vector3i point)
		{
			return _min.X < point.X && point.X < _max.X &&
				   _min.Y < point.Z && point.Y < _max.Y &&
				   _min.Z < point.Z && point.Z < _max.Z;
		}

		/// <summary>
		/// Returns whether the box contains the specified point (borders inclusive).
		/// </summary>
		/// <param name="point">The point to query.</param>
		/// <param name="boundaryInclusive">
		/// Whether points on the box boundary should be recognised as contained as well.
		/// </param>
		/// <returns>Whether this box contains the point.</returns>
		[Pure]
		public bool Contains(Vector3i point, bool boundaryInclusive)
		{
			if (boundaryInclusive)
			{
				return _min.X <= point.X && point.X <= _max.X &&
					   _min.Y <= point.Y && point.Y <= _max.Y &&
					   _min.Z <= point.Z && point.Z <= _max.Z;
			}
			return _min.X < point.X && point.X < _max.X &&
				   _min.Y < point.Y && point.Y < _max.Y &&
				   _min.Z < point.Z && point.Z < _max.Z;
		}

		/// <summary>
		/// Returns whether the box contains the specified box (borders inclusive).
		/// </summary>
		/// <param name="other">The box to query.</param>
		/// <returns>Whether this box contains the other box.</returns>
		[Pure]
		public bool Contains(Bounds3i other)
		{
			return _max.X >= other._min.X && _min.X <= other._max.X &&
				   _max.Y >= other._min.Y && _min.Y <= other._max.Y &&
				   _max.Z >= other._min.Z && _min.Z <= other._max.Z;
		}

		/// <summary>
		/// Returns the distance between the nearest edge and the specified point.
		/// </summary>
		/// <param name="point">The point to find distance for.</param>
		/// <returns>The distance between the specified point and the nearest edge.</returns>
		[Pure]
		public float DistanceToNearestEdge(Vector3i point)
		{
			var distX = new Vector3(
				Math.Max(0f, Math.Max(_min.X - point.X, point.X - _max.X)),
				Math.Max(0f, Math.Max(_min.Y - point.Y, point.Y - _max.Y)),
				Math.Max(0f, Math.Max(_min.Z - point.Z, point.Z - _max.Z)));
			return distX.Length;
		}

		/// <summary>
		/// Translates this Bounds3i by the given amount.
		/// </summary>
		/// <param name="distance">The distance to translate the box.</param>
		public void Translate(Vector3i distance)
		{
			_min += distance;
			_max += distance;
		}

		/// <summary>
		/// Returns a Bounds3i translated by the given amount.
		/// </summary>
		/// <param name="distance">The distance to translate the box.</param>
		/// <returns>The translated box.</returns>
		[Pure]
		public Bounds3i Translated(Vector3i distance)
		{
			// create a local copy of this box
			Bounds3i box = this;
			box.Translate(distance);
			return box;
		}

		/// <summary>
		/// Scales this Bounds3i by the given amount.
		/// </summary>
		/// <param name="scale">The scale to scale the box.</param>
		/// <param name="anchor">The anchor to scale the box from.</param>
		public void Scale(Vector3i scale, Vector3i anchor)
		{
			_min = anchor + ((_min - anchor) * scale);
			_max = anchor + ((_max - anchor) * scale);
		}

		/// <summary>
		/// Returns a Bounds3i scaled by a given amount from an anchor point.
		/// </summary>
		/// <param name="scale">The scale to scale the box.</param>
		/// <param name="anchor">The anchor to scale the box from.</param>
		/// <returns>The scaled box.</returns>
		[Pure]
		public Bounds3i Scaled(Vector3i scale, Vector3i anchor)
		{
			// create a local copy of this box
			Bounds3i box = this;
			box.Scale(scale, anchor);
			return box;
		}

		/// <summary>
		/// Inflate this Bounds3i to encapsulate a given point.
		/// </summary>
		/// <param name="point">The point to query.</param>
		public void Inflate(Vector3i point)
		{
			_min = Vector3i.ComponentMin(_min, point);
			_max = Vector3i.ComponentMax(_max, point);
		}

		/// <summary>
		/// Inflate this Bounds3i to encapsulate a given point.
		/// </summary>
		/// <param name="point">The point to query.</param>
		/// <returns>The inflated box.</returns>
		[Pure]
		public Bounds3i Inflated(Vector3i point)
		{
			// create a local copy of this box
			Bounds3i box = this;
			box.Inflate(point);
			return box;
		}

		/// <summary>
		/// Equality comparator.
		/// </summary>
		/// <param name="left">The left operand.</param>
		/// <param name="right">The right operand.</param>
		public static bool operator ==(Bounds3i left, Bounds3i right)
		{
			return left.Equals(right);
		}

		/// <summary>
		/// Inequality comparator.
		/// </summary>
		/// <param name="left">The left operand.</param>
		/// <param name="right">The right operand.</param>
		public static bool operator !=(Bounds3i left, Bounds3i right)
		{
			return !(left == right);
		}

		/// <inheritdoc/>
		public override bool Equals(object obj)
		{
			return obj is Bounds3i && Equals((Bounds3i)obj);
		}

		/// <inheritdoc/>
		public bool Equals(Bounds3i other)
		{
			return _min.Equals(other._min) &&
				   _max.Equals(other._max);
		}

		/// <inheritdoc/>
		public override int GetHashCode()
		{
			return HashCodeBuilder.Combine(_min, _max);
		}

		/// <inheritdoc/>
		public override string ToString()
		{
			return $"{Min} - {Max}";
		}
	}
}
