﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.Text;
using OpenToolkit.Mathematics;

namespace Shared
{
	public static class QuaternionExt
	{
		/// <summary>
		/// Create a quaternion that looks in <paramref name="direction"/> with <paramref name="up"/> as the up vector.
		/// Both should be normalized vectors.
		/// </summary>
		/// <param name="direction">Normalized direction vector.</param>
		/// <param name="up">Normalized up vector.</param>
		/// <returns>A Quaternion that represents the direction.</returns>
		[Pure]
		public static Quaternion FromLookAt(Vector3 direction, Vector3 up)
		{
			Vector3 sideAxis = Vector3.Cross(direction, up);

			Matrix3 matrix = new Matrix3(sideAxis, up, direction);
			matrix.Transpose();

			return Quaternion.FromMatrix(matrix);
		}

		/// <summary>
		/// Creates a <see cref="Quaternion"/> that represents the rotation from <paramref name="fromDirection"/> to <paramref name="toDirection"/>.
		/// All inputs must be normalized vectors.
		/// </summary>
		/// <param name="fromDirection">Normalized starting direction.</param>
		/// <param name="toDirection">Normalized target direction.</param>
		/// <param name="up">Normalized up vector for the result.</param>
		/// <returns>The equivalent <see cref="Quaternion"/>.</returns>
		[Pure]
		public static Quaternion FromDirectionVectors
			(Vector3 fromDirection, Vector3 toDirection, Vector3 up)
		{
			// todo?: this ignores the up in non-trivial case.


			// todo: Maybe use (but also ignores up) https://stackoverflow.com/a/11741520

			// Cases that break the math:
			if (fromDirection == toDirection)
			{
				return Quaternion.Identity;
			}
			if (fromDirection == -toDirection)
			{
				// todo: This produces unwanted rotation.
				var w = 0f;
				var s = (float)Math.Sin(MathHelper.PiOver2);

				var result = new Quaternion(up * s, w).Normalized();

				return result;


				return Quaternion.FromAxisAngle(up, MathHelper.Pi + 0.001f);
			}

			Vector3 axis = Vector3.Cross(fromDirection, toDirection);

			float angle = (float) Math.Acos(Vector3.Dot(fromDirection, toDirection));

			return Quaternion.FromAxisAngle(axis, angle);
		}

		public static Quaternion FromToRotation(Vector3 fromDirection, Vector3 toDirection)
		{
			//https://answers.unity.com/questions/1750909/unitymathematics-equivalent-to-quaternionfromtorot.html
			//https://answers.unity.com/questions/1668856/whats-the-source-code-of-quaternionfromtorotation.html

			Vector3 axis = Vector3.Cross(fromDirection, toDirection);
			float angle = VectorExt.Angle(fromDirection, toDirection);

			if(axis.LengthSquared <= 0) axis = Vector3.UnitY; // Cross product zero -> Set aribtrary axis.
			return Quaternion.FromAxisAngle(axis.Normalized(), angle);
		}

		[DebuggerStepThrough] // Just a rename to match Unity.
		public static Quaternion FromToRotation(Vector3 fromDirection, Vector3 toDirection, Vector3 up)
		{
			return QuaternionExt.FromDirectionVectors(fromDirection, toDirection, up);
		}


		/// <summary>
		/// Returns the angle in radians between <paramref name="a"/> and <paramref name="b"/>.
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <returns>Signed Angle in radians</returns>
		public static float AngleBetween(this Quaternion a, Quaternion b)
		{
			Quaternion difference = a.Inverted() * b;
			float angle = 2f * (float) Math.Atan2(difference.Xyz.Length, difference.W);

			return angle;
		}

		/// <summary>
		/// Returns the distance <paramref name="angle"/> is away from any integer multiple of <paramref name="alignment"/>.
		/// </summary>
		/// <param name="angle"></param>
		/// <param name="alignment"></param>
		/// <returns></returns>
		public static float AngleFromMultipleOf(float angle, float alignment)
		{
			float a = angle;

			const float Deg2Rad = (float)Math.PI / 180f;
			float b = alignment;

			if (a < 0)
			{
				a = a % (360 * Deg2Rad);
				a += (360 * Deg2Rad);
			}

			a = a % (360 * Deg2Rad);

			a += b / 2f;
			a = a  % b;
			a -= b / 2f;
			a = Math.Abs(a);

			return a;
		}
	}
}
