﻿// Copyright 2022-2024 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Runtime.CompilerServices;

namespace Shared
{
	/// <summary>
	/// Signals that the decorated property should be visible to the User.
	/// </summary>
	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
	public class VisiblePropertyAttribute : Attribute
	{
		public readonly bool isReadonly;
		public readonly string nameOverride;
		/// <summary>
		/// Use a large variant of whatever editor for this Property.
		/// </summary>
		public readonly bool large;
		public readonly int order;
		

		public VisiblePropertyAttribute(bool isReadonly = false, string nameOverride = null, bool large = false, [CallerLineNumber] int order = 0)
		{
			this.isReadonly = isReadonly;
			this.nameOverride = nameOverride;
			this.large = large;
			this.order = order;
		}
	}
}
