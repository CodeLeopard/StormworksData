﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Shared
{
	/// <summary>
	/// Cache that guarantees <see cref="CreateEntry"/> will be called exactly once for each new key.
	/// </summary>
	/// <typeparam name="TKey"></typeparam>
	/// <typeparam name="TValue"></typeparam>
	public class LazyCache<TKey, TValue>
	{
		// todo: automatic eviction.

		/// <summary>
		/// The underlying collection.
		/// </summary>
		protected readonly ConcurrentDictionary<TKey, Lazy<TValue>> cache =
			new ConcurrentDictionary<TKey, Lazy<TValue>>();

		/// <summary>
		/// The Func used to create the entry for a given Key.
		/// When this is used <see cref="DefaultCreateEntryLazy"/> will be used
		/// to transform the Value returned by this Func into a <see cref="Lazy{T}"/>.
		/// </summary>
		public readonly Func<TKey, TValue> CreateEntry;

		/// <summary>
		/// The Func used to create a <see cref="Lazy{T}"/> for a given key.
		/// </summary>
		public readonly Func<TKey, Lazy<TValue>> CreateEntryLazy;

		/// <summary>
		/// The Func used to update the <see cref="Lazy{T}"/> for a given key.
		/// </summary>
		public readonly Func<TKey, Lazy<TValue>, Lazy<TValue>, Lazy<TValue>> UpdateEntry;

		#region Construction

		/// <summary>
		/// Create a new <see cref="LazyCache{TKey,TValue}"/> using a valueFactory that creates non-lazy <typeparamref name="TValue"/>s.
		/// </summary>
		/// <remarks>
		/// <see cref="CreateEntryLazy"/> will be <see cref="DefaultCreateEntryLazy"/> and <see cref="UpdateEntry"/> will be <see cref="DefaultUpdateEntry"/>.
		/// </remarks>
		/// <param name="createEntry"></param>
		/// <exception cref="ArgumentNullException"></exception>
		public LazyCache(Func<TKey, TValue> createEntry)
		{
			CreateEntry     = createEntry ?? throw new ArgumentNullException(nameof(createEntry));
			CreateEntryLazy = DefaultCreateEntryLazy;
			UpdateEntry     = DefaultUpdateEntry;
		}

		/// <summary>
		/// Create a new <see cref="LazyCache{TKey,TValue}"/> using a valueFactory that creates non-lazy <typeparamref name="TValue"/>s,
		/// and a Func for updating entries.
		/// </summary>
		/// <remarks>
		/// <see cref="CreateEntryLazy"/> will be <see cref="DefaultCreateEntryLazy"/>.
		/// </remarks>
		/// <param name="createEntry"></param>
		/// <param name="updateEntry"></param>
		/// <exception cref="ArgumentNullException"></exception>
		public LazyCache
		(
			Func<TKey, TValue>                                   createEntry
		  , Func<TKey, Lazy<TValue>, Lazy<TValue>, Lazy<TValue>> updateEntry
		)
		{
			CreateEntry     = createEntry ?? throw new ArgumentNullException(nameof(createEntry));
			CreateEntryLazy = DefaultCreateEntryLazy;
			UpdateEntry     = updateEntry ?? throw new ArgumentNullException(nameof(updateEntry));
		}

		/// <summary>
		/// Create a new <see cref="LazyCache{TKey,TValue}"/> using a value factory that returns a <see cref="Lazy{TValue}"/> directly.
		/// </summary>
		/// <remarks>
		/// <see cref="UpdateEntry"/> will be <see cref="DefaultUpdateEntry"/>.
		/// </remarks>
		/// <param name="createEntryLazy"></param>
		/// <exception cref="ArgumentNullException"></exception>
		public LazyCache(Func<TKey, Lazy<TValue>> createEntryLazy)
		{
			CreateEntry     = null;
			CreateEntryLazy = createEntryLazy ?? throw new ArgumentNullException(nameof(createEntryLazy));
			UpdateEntry     = DefaultUpdateEntry;
		}

		/// <summary>
		/// Create a new <see cref="LazyCache{TKey,TValue}"/> using a value factory that returns a <see cref="Lazy{TValue}"/> directly,
		/// and a Func for updating entries.
		/// </summary>
		/// <param name="createEntryLazy"></param>
		/// <param name="updateEntry"></param>
		/// <exception cref="ArgumentNullException"></exception>
		public LazyCache
		(
			Func<TKey, Lazy<TValue>>                             createEntryLazy
		  , Func<TKey, Lazy<TValue>, Lazy<TValue>, Lazy<TValue>> updateEntry
		)
		{
			CreateEntry     = null;
			CreateEntryLazy = createEntryLazy ?? throw new ArgumentNullException(nameof(createEntryLazy));
			UpdateEntry     = updateEntry     ?? throw new ArgumentNullException(nameof(updateEntry));
		}

		#endregion Construction

		#region DefaultImplementations

		/// <summary>
		/// The default method to create a <see cref="Lazy{T}"/> from the value returned by <see cref="CreateEntry"/>.
		/// This method simply creates a new <see cref="Lazy{T}"/> with <see cref="LazyThreadSafetyMode.ExecutionAndPublication"/>.
		/// </summary>
		/// <param name="key"></param>
		/// <returns></returns>
		public Lazy<TValue> DefaultCreateEntryLazy(TKey key)
		{
			TValue ValueFactory()
			{
				return CreateEntry(key);
			}
			return new Lazy<TValue>(ValueFactory, LazyThreadSafetyMode.ExecutionAndPublication);
		}

		/// <summary>
		/// The default method to update a value.
		/// This method simply returns <paramref name="newValue"/>.
		/// </summary>
		/// <param name="key">The key for this entry.</param>
		/// <param name="oldValue">The old value that will be replaced.</param>
		/// <param name="newValue">The new value.</param>
		/// <returns>The new value.</returns>
		public Lazy<TValue> DefaultUpdateEntry(TKey key, Lazy<TValue> oldValue, Lazy<TValue> newValue)
		{
			return newValue;
		}

		#endregion DefaultImplementations

		#region Public interface

		/// <summary>
		/// Returns the <typeparamref name="TValue"/> for the <paramref name="key"/>,
		/// or <see langword="null"/> if that is what was stored.
		/// If <see cref="CreateEntry"/> throws and <see cref="Exception"/> it will appear here.
		/// </summary>
		/// <param name="key"></param>
		/// <returns></returns>
		public TValue GetOrAdd(TKey key)
		{
			return cache.GetOrAdd(key, CreateEntryLazy).Value;
		}

		/// <summary>
		/// Try to retrieve the <typeparamref name="TValue"/> for this <paramref name="key"/>.
		/// returns <see langword="true"/> if <paramref name="key"/> exists.
		/// Note that <paramref name="result"/> could still be <see langword="null"/> if that is what was stored.
		/// </summary>
		/// <param name="key"></param>
		/// <param name="result"></param>
		/// <returns></returns>
		public bool TryGet(TKey key, out TValue result)
		{
			if (cache.TryGetValue(key, out Lazy<TValue> lazyResult))
			{
				result = lazyResult.Value;
				return true;
			}

			result = default;
			return false;
		}

		/// <summary>
		/// Try to retrieve the <see cref="Lazy{TData}"/> for <paramref name="key"/>.
		/// returns <see langword="true"/> if <paramref name="key"/> exists.
		/// Note that <paramref name="result"/> could be a <see cref="Lazy{TData}"/> holding a value of <see langword="null"/>.
		/// </summary>
		/// <param name="key"></param>
		/// <param name="result"></param>
		/// <returns></returns>
		public bool TryGetLazy(TKey key, out Lazy<TValue> result)
		{
			return cache.TryGetValue(key, out result);
		}

		/// <summary>
		/// Add or update the <paramref name="value"/> for the given <paramref name="key"/>.
		/// <paramref name="value"/> may be <see langword="null"/>.
		/// </summary>
		/// <param name="key"></param>
		/// <param name="value">The value to insert, or <see langword="null"/></param>
		public void AddOrUpdate(TKey key, TValue value)
		{
			var newValue = new Lazy<TValue>(() => value, LazyThreadSafetyMode.None);
			cache.AddOrUpdate(key, newValue,
				(newKey, oldValue) => UpdateEntry(newKey, oldValue, newValue));
		}

		/// <summary>
		/// Try to add the <paramref name="key"/>, <paramref name="value"/> pair.
		/// Returns <see langword="true"/> if it didn't exist yet and was added,
		/// <see langword="false"/> otherwise.
		/// </summary>
		/// <param name="key"></param>
		/// <param name="value"></param>
		/// <returns></returns>
		public bool TryAdd(TKey key, TValue value)
		{
			return cache.TryAdd(key, new Lazy<TValue>(
				() => value, LazyThreadSafetyMode.None));
		}

		/// <summary>
		/// Try to remove the entry for <paramref name="key"/>.
		/// Returns <see langword="true"/> if <paramref name="key"/> existed and was removed.
		/// Note that <paramref name="result"/> could still be <see langword="null"/> if that 'value' was stored.
		/// </summary>
		/// <param name="key"></param>
		/// <param name="result"></param>
		/// <returns></returns>
		public bool TryRemove(TKey key, out TValue result)
		{
			if (cache.TryRemove(key, out Lazy<TValue> lazyResult))
			{
				result = lazyResult.Value;
				return true;
			}

			result = default;
			return false;
		}

		/// <summary>
		/// Try to remove the <see cref="Lazy{TData}"/> for <paramref name="key"/>.
		/// returns <see langword="true"/> if <paramref name="key"/> existed and was removed.
		/// Note that <paramref name="result"/> could be a <see cref="Lazy{TData}"/> holding a value of <see langword="null"/>.
		/// </summary>
		/// <param name="key"></param>
		/// <param name="result"></param>
		/// <returns></returns>
		public bool TryRemoveLazy(TKey key, out Lazy<TValue> result)
		{
			return cache.TryRemove(key, out result);
		}

		/// <summary>
		/// Remove all entries from the Cache.
		/// </summary>
		public void Clear()
		{
			cache.Clear();
		}

		/// <summary>
		/// Check if the cache contains an entry for <paramref name="key"/>.
		/// Note that the value that <paramref name="key"/> points to could still be <see langword="null"/> if that is what was stored.
		/// </summary>
		/// <param name="key"></param>
		/// <returns><see langword="true"/> if the <paramref name="key"/> has an associated value, <see langword="false"/> otherwise.</returns>
		public bool ContainsKey(TKey key)
		{
			return cache.ContainsKey(key);
		}

		#region Async

		/// <summary>
		/// Returns the <typeparamref name="TValue"/> for the <paramref name="key"/>,
		/// or <see langword="null"/> if that is what was stored.
		/// If <see cref="CreateEntry"/> throws and <see cref="Exception"/> it will appear here.
		/// </summary>
		/// <param name="key"></param>
		/// <returns></returns>
		public async Task<TValue> GetOrAddAsync(TKey key)
		{
			return await Task.Run(() => GetOrAdd(key));
		}

		/// <summary>
		/// Try to retrieve the <typeparamref name="TValue"/> for this <paramref name="key"/>.
		/// returns <see langword="true"/> if <paramref name="key"/> exists.
		/// Note that the resulting value could still be <see langword="null"/> if that is what was stored.
		/// </summary>
		/// <param name="key"></param>
		/// <returns></returns>
		public async Task<(bool found, TValue value)> TryGetAsync(TKey key)
		{
			return await Task.Run(Method);
			(bool, TValue) Method()
			{
				bool found = cache.TryGetValue(key, out Lazy<TValue> lazyValue);
				return found
					? (true, lazyValue.Value)
					: (false, default);
			}
		}

		/// <summary>
		/// Try to remove the entry for <paramref name="key"/>.
		/// Returns <see langword="true"/> if <paramref name="key"/> existed and was removed.
		/// Note that result could still be <see langword="null"/> if that is what was stored.
		/// </summary>
		/// <param name="key"></param>
		/// <returns></returns>
		public async Task<(bool found, TValue value)> TryRemoveAsync(TKey key)
		{
			return await Task.Run(Method);
			(bool found, TValue value) Method()
			{
				bool found = TryRemove(key, out var value);
				return (found, value);
			}
		}

		#endregion Async

		/// <inheritdoc cref="ConcurrentDictionary{TKey,TValue}.Count"/>
		public int Count => cache.Count;

		/// <inheritdoc cref="ConcurrentDictionary{TKey,TValue}.IsEmpty"/>
		public bool IsEmpty => cache.IsEmpty;

		/// <inheritdoc cref="ConcurrentDictionary{TKey,TValue}.Keys"/>
		public ICollection<TKey> Keys => cache.Keys;

		/// <summary>
		/// A collection that contains the values in the <see cref="LazyCache{TKey,TValue}"/>.
		/// </summary>
		public ICollection<TValue> Values => cache.Values.Select(lazy => lazy.Value).ToList();

		/// <summary>
		/// A collection that contains the <see cref="Lazy{TValue}"/> values in the <see cref="LazyCache{TKey,TValue}"/>.
		/// </summary>
		public ICollection<Lazy<TValue>> LazyValues => cache.Values;

		/// <summary>
		/// Returns an enumerator that iterates through the <see cref="LazyCache{TKey,TValue}"/>.
		/// </summary>
		/// <returns></returns>
		public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
		{
			foreach (var kvp in cache)
			{
				yield return new KeyValuePair<TKey, TValue>(kvp.Key, kvp.Value.Value);
			}
		}

		/// <inheritdoc cref="ConcurrentDictionary{TKey,TValue}.GetEnumerator"/>
		public IEnumerator<KeyValuePair<TKey, Lazy<TValue>>> GetLazyEnumerator()
		{
			return cache.GetEnumerator();
		}

		#endregion Public interface
	}

	/// <summary>
	/// Contains some Methods that due to language constraints cannot exist as a member of the <see cref="LazyCache{TKey,TValue}"/> class itself.
	/// </summary>
	public static class LazyCacheExtension
	{
		/// <summary>
		/// Check if the <paramref name="cache"/> contains a value for <paramref name="key"/> and that value is not <see langword="null"/>.
		/// </summary>
		/// <typeparam name="TKey">The type of the Key</typeparam>
		/// <typeparam name="TValue">The type of the Value</typeparam>
		/// <param name="cache">The <see cref="LazyCache{TKey,TValue}"/> instance to work with.</param>
		/// <param name="key">The key to check.</param>
		/// <returns><see langword="true"/> if the <paramref name="key"/> has an associated value and that value is not <see langword="null"/>.
		/// <see langword="false"/> otherwise.</returns>
		public static bool ContainsKeyWithNonNullValue<TKey, TValue>(this LazyCache<TKey, TValue> cache, TKey key) where TValue : class
		{
			_ = cache ?? throw new ArgumentNullException(nameof(cache));

			bool found = cache.TryGet(key, out var value);

			if (! found) return false;

			return value != null;
		}

		/// <summary>
		/// Check if the <paramref name="cache"/> contains a value for <paramref name="key"/> and that value is not <see langword="null"/>.
		/// </summary>
		/// <typeparam name="TKey">The type of the Key</typeparam>
		/// <typeparam name="TValue">The type of the Value</typeparam>
		/// <param name="cache">The <see cref="LazyCache{TKey,TValue}"/> instance to work with.</param>
		/// <param name="key">The key to check.</param>
		/// <returns><see langword="true"/> if the <paramref name="key"/> has an associated value and that value is not <see langword="null"/>.
		/// <see langword="false"/> otherwise.</returns>
		public static async Task<bool> ContainsKeyWithNonNullValueAsync<TKey, TValue>(this LazyCache<TKey, TValue> cache, TKey key) where TValue : class
		{
			_ = cache ?? throw new ArgumentNullException(nameof(cache));

			(bool found, TValue value) = await cache.TryGetAsync(key);

			if (! found) return false;

			return value != null;
		}
	}
}
