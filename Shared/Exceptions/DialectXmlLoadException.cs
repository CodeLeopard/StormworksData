﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Runtime.Serialization;
using System.Text;

namespace Shared.Exceptions
{
	/// <summary>
	/// Exception thrown when reading a Stormworks dialect XML file. It's data includes both dialect and clean xml text.
	/// </summary>
	[Serializable]
	public class DialectXmlLoadException : Exception
	{
		/// <summary>
		/// The stormworks-Dialect xml that was loaded from file.
		/// </summary>
		public readonly string DialectXml;
		/// <summary>
		/// The spec-compliant xml that will be read by the parser.
		/// </summary>
		public readonly string CompliantXml;

		public DialectXmlLoadException()
		{
		}

		public DialectXmlLoadException(string message, string dialectXml, string compliantXml) : base(message)
		{
			this.DialectXml = dialectXml;
			this.CompliantXml = compliantXml;
		}

		public DialectXmlLoadException(string dialectXml, string compliantXml, Exception innerException)
			: base($"{innerException.GetType().Name} while Loading Stormworks Dialect Xml.", innerException)
		{
			this.DialectXml = dialectXml;
			this.CompliantXml = compliantXml;
		}

		public DialectXmlLoadException(string message, string dialectXml, string compliantXml, Exception innerException) : base(message, innerException)
		{
			this.DialectXml = dialectXml;
			this.CompliantXml = compliantXml;
		}

		protected DialectXmlLoadException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		private void CustomToStringPart(StringBuilder sb)
		{
			sb.AppendLine();
			/*
			if (null == dialectXml)
			{
				sb.AppendLine($"Dialect/Original XML was null.");
			}
			else
			{
				sb.AppendLine($"Dialect/Original XML: ");
				sb.AppendLine(dialectXml.Replace("\t", "  "));
				sb.AppendLine(" --- End of Dialect/Original Xml ---");
			}
			*/

			if (null == CompliantXml)
			{
				sb.AppendLine($"Transformed/Compliant XML was null.");
			}
			else
			{
				sb.AppendLine($"Transformed/Compliant XML: ");
				// terminal has less space for tabs.
				sb.AppendLine(CompliantXml.Replace("\t", "  "));
				sb.AppendLine(" --- End of Transformed/Compliant Xml ---");
			}

			sb.AppendLine(Message);
		}

		/// <inheritdoc />
		public override string ToString()
		{
			return this.ExceptionToString(CustomToStringPart);
		}
	}
}
