﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Runtime.Serialization;

namespace Shared.Exceptions
{
	/// <summary>
	/// This exception is thrown when an attempt was made to set a invalid value.
	/// When this exception is thrown the object guarantees to remain in a valid state.
	/// </summary>
	[Serializable]
	public class InvalidValueException : Exception
	{
		public InvalidValueException()
		{
		}

		public InvalidValueException(string message) : base(message)
		{
		}

		public InvalidValueException(string message, Exception innerException) : base(message, innerException)
		{
		}

		protected InvalidValueException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
