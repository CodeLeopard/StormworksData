﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Shared.Exceptions
{
	/// <summary>
	/// Exception thrown when the file/stream format is wrong.
	/// </summary>
	[Serializable]
	public class BinaryDataFormatException : BinaryDataException
	{
		public BinaryDataFormatException()
		{
		}

		public BinaryDataFormatException(string message) : base(message)
		{
		}

		/// <summary>
		/// Automatically generate a message like $"Unsupported file type: '{new string(actual)}' expected: '{new string(expected)}'."
		/// </summary>
		/// <param name="expected"></param>
		/// <param name="actual"></param>
		public BinaryDataFormatException(char[] expected, char[] actual) : base($"Unsupported file type: '{new string(actual)}' expected: '{new string(expected)}'.")
		{
		}

		public BinaryDataFormatException(string message, Exception innerException) : base(message, innerException)
		{
		}

		protected BinaryDataFormatException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
