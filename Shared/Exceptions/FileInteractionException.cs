﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Shared.Exceptions
{
	/// <summary>
	/// A General Exception used for any operation that involves a file with a known path.
	/// The filePath will appear in the exception message.
	/// The actual exception is typically stored inside.
	/// </summary>
	[Serializable]
	public class FileInteractionException : Exception
	{
		/// <summary>
		/// Does the message already contain the file path.
		/// </summary>
		protected readonly bool messageHasFilePath = false;
		/// <summary>
		/// The path of the file that the interaction was attempted with.
		/// </summary>
		public readonly string FilePath;

		public FileInteractionException()
		{
		}

		public FileInteractionException(string filePath)
		{
			this.FilePath = filePath;
		}

		/// <summary>
		/// Create a new <see cref="FileInteractionException"/> with auto generated message:
		/// $"{innerException.GetType().Name} occurred reading '{filePath}'."
		/// </summary>
		/// <param name="filePath"></param>
		/// <param name="innerException"></param>
		public FileInteractionException(string filePath, Exception innerException) : base
			($"{innerException.GetType().Name} occurred interacting with '{filePath}'.", innerException)
		{
			this.FilePath = filePath;
			messageHasFilePath = true;
		}

		public FileInteractionException(string filePath, string message, Exception innerException) : base(message, innerException)
		{
			this.FilePath = filePath;
			messageHasFilePath = message.Contains(filePath);
		}

		protected FileInteractionException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		/// <summary>
		/// Get the first non <see cref="FileInteractionException"/> by traversing <see cref="Exception.InnerException"/>.
		/// </summary>
		public Exception NonFileException
		{
			get
			{
				var e = InnerException;
				while (e is FileInteractionException)
				{
					e = e.InnerException;
				}

				return e;
			}
		}

		private void CustomToStringPart(StringBuilder sb)
		{
			if (messageHasFilePath) return;

			sb.Append(" Filepath: ");
			if (null != FilePath)
				sb.Append($"'{FilePath}'");
			else
				sb.Append($"null");
		}

		/// <inheritdoc />
		public override string ToString()
		{
			return this.ExceptionToString(CustomToStringPart);
		}
	}
}
