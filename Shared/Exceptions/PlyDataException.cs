﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Shared.Exceptions
{
	/// <summary>
	/// Exception thrown when interacting (loading/saving) a .ply file.
	/// </summary>
	[Serializable]
	public class PlyDataException : Exception
	{
		/// <summary>
		/// The line in the .ply file where the error occurred.
		/// </summary>
		public readonly int LineNumber;

		private PlyDataException()
		{
		}

		public PlyDataException(string message, PlyDataException inner)
			: this(inner.LineNumber, message, inner)
		{ }

		public PlyDataException(string message, int lineNo)
			: base(message)
		{
			LineNumber = lineNo;
		}

		public PlyDataException(int lineNo, string message)
			: base($"Error at line  {lineNo}: {message}")
		{
			LineNumber = lineNo;
		}

		public PlyDataException(int lineNo, Exception innerException)
			: base($"Error at line  {lineNo}: {innerException.Message}", innerException)
		{
			LineNumber = lineNo;
		}

		public PlyDataException(int lineNo, string message, Exception innerException)
			: base($"Error at line  {lineNo}: {message}", innerException)
		{
			LineNumber = lineNo;
		}

		protected PlyDataException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}
	}
}
