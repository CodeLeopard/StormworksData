﻿// Copyright 2022-2025 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

using Microsoft.Win32;

namespace Shared
{
	public static class StormworksPaths
	{
		/// <summary>
		/// The Installation Directory of Steam
		/// </summary>
		public static string Steam;


		private static string _install;

		/// <summary>
		/// The installation directory of Stormworks
		/// </summary>
		public static string Install
		{
			get => _install;
			set
			{
				_install = value;
				InstallChanged?.Invoke();
			}
		}

		/// <summary>
		/// Event that is triggered after the <see cref="Install"/> is changed.
		/// </summary>
		public static event Action InstallChanged;



		public static string rom => $"{Install}/rom";

		public static string audio => $"{rom}/audio";
		public static string data => $"{rom}/data";
		public static string graphics => $"{rom}/graphics";
		public static string meshes => $"{rom}/meshes";
		public static string creatorToolkitData => $"{rom}/creatorToolkitData";


		public static string userData = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Stormworks");


		public static class Data
		{
			public static string benchmark => $"{data}/benchmark";
			public static string debris => $"{data}/debris";
			public static string definitions => $"{data}/definitions";
			public static string missions => $"{data}/missions";
			public static string narrative => $"{data}/narrative";
			public static string preset_vehicles => $"{data}/preset_vehicles";
			public static string preset_vehicles_advanced => $"{data}/preset_vehicles_advanced";
			public static string realtime_values => $"{data}/realtime_values";
			public static string tiles => $"{data}/tiles";
		}

		public static class Meshes
		{
			public static string clouds => $"{meshes}/clouds";
			public static string mainland => $"{meshes}/mainland";
			public static string mainland_assets => $"{meshes}/mainland_assets";
			public static string rock_library => $"{meshes}/rock_library";
			public static string track_pieces => $"{meshes}/track_pieces";

			/// <summary>
			/// Generated .mesh and .phys for extruded track pieces.
			/// </summary>
			public static string extruded_track_pieces => $"{meshes}/extruded_track_pieces";

			/// <summary>
			/// Generated .mesh and .phys for static objects.
			/// </summary>
			public static string generated_object_geom => $"{meshes}/generated_object_geom";

			public static string trees => $"{meshes}/trees";
			public static string undersea_assets => $"{meshes}/undersea_assets";
		}

		public static class UserData
		{
			public static string saves => $"{userData}/saves";
			public static string vehicles => $"{userData}/data/vehicles";
			public static string missions => $"{userData}/data/missions";
		}


		public static class CreatorToolKitData
		{
			public static class FolderNames
			{
				public static string crossSectionSpecifications = "CSSpec";
				public static string crossSectionParts = "CSSpecSub";
				public static string curveSpec = "CurveSpec";
				public static string settings = "Settings";
			}

			public static string crossSectionSpecifications => $"{creatorToolkitData}/{FolderNames.crossSectionSpecifications}";
			public static string crossSectionParts => $"{creatorToolkitData}/{FolderNames.crossSectionParts}";
			public static string curveSpec => $"{creatorToolkitData}/{FolderNames.curveSpec}";

			public static string setttings => $"{creatorToolkitData}/{FolderNames.settings}";
		}

		/// <summary>
		/// Get a path relative to rom from an absolute path (which must be inside the stormworks folder).
		/// </summary>
		/// <param name="absPath"></param>
		/// <returns></returns>
		/// <exception cref="ArgumentException"></exception>
		public static string GetRelativePath(string absPath)
		{
			if (!Path.IsPathRooted(absPath))
			{
				throw new ArgumentException($"Path is not rooted");
			}

			absPath = absPath.Replace('\\', '/');
			var sanitizedRom = $"{rom.Replace('\\', '/')}/"; // Extra slash at the end to remove that from the input too.

			if (!absPath.Contains(sanitizedRom))
			{
				throw new ArgumentException($"Path is not inside stormworks, cannot make it relative to rom");
			}

			string result = absPath.Replace(sanitizedRom, String.Empty);
			return result;
		}

		public static string LocateSteam()
		{
			string steamInstall = null;
			if (Environment.OSVersion.Platform == PlatformID.Win32NT)
			{
				Console.WriteLine($"[StormworksPaths] Using Windows Registry to find the Steam Installation.");

				// Get steam install from Registry
				steamInstall = Registry.GetValue
					("HKEY_LOCAL_MACHINE\\SOFTWARE\\Valve\\Steam", "InstallPath", null) as string;
				if (string.IsNullOrWhiteSpace(steamInstall))
				{
					steamInstall = Registry.GetValue
						("HKEY_LOCAL_MACHINE\\SOFTWARE\\WOW6432Node\\Valve\\Steam", "InstallPath", null) as string;
				}
			}
			else
			{
				Console.WriteLine($"[StormworksPaths] Not on windows, can only provide a best guess for the Steam Installation.");

				var guess = "~/.local/share/Steam";
				if (Directory.Exists(guess))
				{
					steamInstall = guess;
				}
			}

			if (string.IsNullOrWhiteSpace(steamInstall))
			{
				Console.WriteLine($"[StormworksPaths] Unable to find Steam installation.");
				return null;
			}
			else
			{
				Console.WriteLine($"[StormworksPaths] Steam is installed at '{steamInstall}'.");
				return steamInstall;
			}
		}

		public static string LocateStormworksFromSteam()
		{
			if (string.IsNullOrWhiteSpace(Steam)) return null;

			// Try the main SteamLibrary folder.
			var inMainSteamFolder = Path.Combine(Steam, "steamapps", "common", "Stormworks");
			if (File.Exists(Path.Combine(inMainSteamFolder, "stormworks.exe")))
			{
				return inMainSteamFolder;
			}

			// Try other SteamLibrary folders.
			var steamConfigPath = Path.Combine(Steam,          "config");
			var configFilePath = Path.Combine(steamConfigPath, "config.vdf");
			var file = File.ReadAllText(configFilePath);

			var regex = new Regex("^\\s*\"BaseInstallFolder_\\d+\"\\s+\"(.*)\"\\s*$", RegexOptions.Compiled | RegexOptions.Multiline);

			var matches = regex.Matches(file);
			foreach (Match match in matches)
			{
				var libraryPath = match.Groups[1].Value;

				var potentialPath = Path.Combine(libraryPath, "steamapps", "common", "Stormworks");

				if (File.Exists(Path.Combine(potentialPath, "stormworks.exe")))
				{
					return potentialPath;
				}
			}

			return null;
		}

		public static string LocateStormworksFallbackHardcoded()
		{
			// Fallback to hardcoded paths
			var paths = new string[]
			{
				"C:\\Program Files (x86)\\Steam\\steamapps\\common\\Stormworks"
			  , "D:\\Program Files (x86)\\Steam\\steamapps\\common\\Stormworks"
			  , "D:\\Steam\\steamapps\\common\\Stormworks"
			  , "I:\\SteamLibrary\\steamapps\\common\\Stormworks"
			};

			foreach (string path in paths)
			{
				if (File.Exists(path + "\\stormworks.exe") || File.Exists(path + "\\stormworks64.exe"))
				{
					return path;
				}
			}

			return null;
		}

		public static string LocateStormworks()
		{
			return LocateStormworksFromSteam() ?? LocateStormworksFallbackHardcoded();
		}

		private static void SetupPaths()
		{
			// https://stackoverflow.com/questions/34090258/find-steam-games-folder/34091380

			Steam = LocateSteam();
			if(null != Steam)
			{
				Install = LocateStormworksFromSteam();
			}

			if (null == Install)
			{
				Install = LocateStormworksFallbackHardcoded();
			}

			if (null == Install)
			{
				Console.WriteLine($"[StormworksPaths] [Warning] Stormworks folder not found.");
			}
			else
			{
				Console.WriteLine($"[StormworksPaths] Located Stormworks installation at '{Install}'.");
			}
		}


		private static readonly IReadOnlyCollection<string> stormworksInstallIndicationFiles = new string[] { "stormworks.exe", "stormworks64.exe" };

		/// <summary>
		/// Check if <paramref name="path"/> is inside a Stormworks Installation.
		/// If so <see langword="true"/> is returned and <paramref name="installRoot"/> has the full path to the Installation's root.
		/// </summary>
		/// <param name="path">A path (directory or file) to be checked.</param>
		/// <param name="installRoot">If returned <see langword="true"/> the root of the Stormworks Installation that was found.</param>
		/// <returns><see langword="true"/> if an install was found.</returns>
		public static bool IsPathInStormworksInstall(string path, out string installRoot)
		{
			installRoot = null;

			if (path == null) path = Directory.GetCurrentDirectory();

			var dir = new DirectoryInfo(path);

			if(! dir.Exists)
			{
				// The supplied path could point to a file.
				var file = new FileInfo(path);
				if (file.Exists)
				{
					dir = file.Directory;
				}
			}

			while (dir != null && dir.Exists)
			{
				var result = dir.GetFiles("stormworks*.exe", SearchOption.TopDirectoryOnly);
				if (result.Select(f => f.Name).Intersect(stormworksInstallIndicationFiles).Any())
				{
					installRoot = dir.FullName;
					return true;
				}

				dir = dir.Parent;
			}

			return false;
		}

		static StormworksPaths()
		{
			SetupPaths();
		}
	}
}
