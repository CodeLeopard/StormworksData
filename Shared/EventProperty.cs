﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Text;

namespace Shared
{
	/// <summary>
	/// Represents a Property with a <see cref="Changed"/> event and Validation.
	/// </summary>
	[Serializable]
	public class EventProperty<TData> where TData : IEquatable<TData>
	{
		[NonSerialized]
		private readonly IEqualityComparer<TData> comparer = EqualityComparer<TData>.Default;
		[NonSerialized]
		private readonly Func<TData, bool> silentValidator;
		[NonSerialized]
		private readonly Func<TData, bool> standardValidator;

		[field: NonSerialized]
		public event Action Changed;
		[field: NonSerialized]
		public event Action<TData> ChangedValue;

		/// <summary>
		/// The current Value.
		/// </summary>
		public TData Value { get; private set; }

		/// <summary>
		/// Set the value without triggering <see cref="Changed"/>.
		/// </summary>
		public TData Silent
		{
			get => Value;
			set
			{
				if (comparer.Equals(value, Value)) return;
				if (! silentValidator.Invoke(value)) return;

				Value = value;
			}
		}

		/// <summary>
		/// Set the value and trigger <see cref="Changed"/>.
		/// </summary>
		public TData Standard
		{
			get => Value;
			set
			{
				if (comparer.Equals(value, Value)) return;
				if (! standardValidator.Invoke(value)) return;

				Value = value;

				Changed?.Invoke();
				ChangedValue?.Invoke(value);
			}
		}


		public EventProperty
		(
			TData                    initialValue
		  , Func<TData, bool>        standardValidator
		  , Func<TData, bool>        silentValidator = null
		  , IEqualityComparer<TData> comparer = null
		)
		{
			this.standardValidator = standardValidator ?? throw new ArgumentNullException(nameof(standardValidator));
			this.silentValidator = silentValidator     ?? standardValidator;
			this.comparer = comparer                   ?? EqualityComparer<TData>.Default ?? throw new ArgumentNullException(nameof(comparer), "There is no comparer specified and the default comparer was null.");

			if (!standardValidator.Invoke(initialValue))
				throw new ArgumentOutOfRangeException(nameof(initialValue), initialValue, "Initial value failed validation");

			this.Value = initialValue;
		}

		public void RaiseChanged()
		{
			Changed?.Invoke();
			ChangedValue?.Invoke(Value);
		}
	}
}
