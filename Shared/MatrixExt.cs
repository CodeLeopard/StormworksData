﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using OpenToolkit.Mathematics;

namespace Shared
{
	public static class MatrixExt
	{
		public static bool ApproxEquals(this in Matrix4 instance, in Matrix4 other, float margin = 0)
		{
			Matrix4 dist = instance - other;
			float len0 = dist.Column0.LengthSquared;
			float len1 = dist.Column1.LengthSquared;
			float nen2 = dist.Column2.LengthSquared;
			float nen3 = dist.Column3.LengthSquared;

			float total = len0 + len1 + nen2 + nen3;

			return total <= margin;
		}

		public static bool ApproxEqualsExeptScale(this ref Matrix4 instance, ref Matrix4 other, float margin = 0)
		{
			// 'ref' instead of 'in', because the compiler can't tell that doing that is safe.

			float angle = instance.ExtractRotation().AngleBetween(other.ExtractRotation());

			Vector3 distance = instance.Row3.Xyz - other.Row3.Xyz;

			float total = distance.LengthSquared + angle * angle;

			return total <= margin;
		}

		public static Matrix4 CreateTransScaleRot(Vector3 translation, Vector3 scale, Quaternion rotation)
		{
			// From Editor's repo.
			var sca = Matrix4.CreateScale(scale);
			var tra = Matrix4.CreateTranslation(translation);
			var rot = Matrix4.CreateFromQuaternion(rotation);
			// 1: scale around the origin.
			// 2: rotate the scaled stuff around the origin.
			// 3: translate the result of the previous operations.
			return sca * rot * tra;
		}

		public static bool IsFinite(this Matrix4 instance)
		{
			return instance.Row0.IsFinite()
			    && instance.Row1.IsFinite()
			    && instance.Row2.IsFinite()
			    && instance.Row3.IsFinite();
		}
	}
}
