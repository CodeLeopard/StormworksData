﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shared
{
	public static class IEnumerableExt
	{
		/// <summary>
		/// Enumerate over <param name="self"></param> with an included index.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="self"></param>
		/// <returns></returns>
		public static IEnumerable<(T item, int index)> EnumerateWithIndices<T>(this IEnumerable<T> self)
			=> self?.Select((item, index) => (item, index)) ?? Enumerable.Empty<(T, int)>();

		/// <summary>
		/// Enumerates the common prefix of two <see cref="IEnumerable{T}"/>s.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="source1"></param>
		/// <param name="source2"></param>
		/// <returns></returns>
		public static IEnumerable<T> CommonPrefix<T>(this IEnumerable<T> source1, IEnumerable<T> source2)
		{
			_ = source1 ?? throw new ArgumentNullException(nameof(source1));
			_ = source2 ?? throw new ArgumentNullException(nameof(source2));

			IEnumerator<T> enumerator1 = null;
			IEnumerator<T> enumerator2 = null;
			try
			{
				enumerator1 = source1.GetEnumerator();
				enumerator2 = source2.GetEnumerator();

				while (enumerator1.MoveNext() && enumerator2.MoveNext())
				{
					if (enumerator1.Current.Equals(enumerator2.Current))
						yield return enumerator1.Current;
					else
						yield break;
				}

			}
			finally
			{
				enumerator1?.Dispose();
				enumerator2?.Dispose();
			}
		}

		/// <summary>
		/// Returns an IEnumerable that iterates the <paramref name="sequence"/> in chunks of <paramref name="size"/>
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="sequence"></param>
		/// <param name="size"></param>
		/// <returns></returns>
		public static IEnumerable<IList<T>> ChunksOf<T>(this IEnumerable<T> sequence, int size)
		{
			List<T> chunk = new List<T>(size);

			foreach (T element in sequence)
			{
				chunk.Add(element);
				if (chunk.Count == size)
				{
					yield return chunk;
					chunk = new List<T>(size);
				}
			}
		}
	}
}
