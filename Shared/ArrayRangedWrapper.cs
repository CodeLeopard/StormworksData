﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Shared
{

	/// <summary>
	/// A wrapper for an array that exposes only a subset of that array.
	/// </summary>
	public static class ArrayRangedWrapper
	{
		/// <summary>
		/// Create a new ArrayRange, but without having to specify the type parameter explicitly.
		/// </summary>
		/// <typeparam name="TData"></typeparam>
		/// <param name="data"></param>
		/// <returns></returns>
		public static ArrayRangedWrapper<TData> New<TData>(IEnumerable<TData> data)
		{
			return new ArrayRangedWrapper<TData>(data.ToArray());
		}

		/// <summary>
		/// Create a new ArrayRange, but without having to specify the type parameter explicitly.
		/// </summary>
		/// <typeparam name="TData"></typeparam>
		/// <param name="data"></param>
		/// <returns></returns>
		public static ArrayRangedWrapper<TData> New<TData>(TData[] data)
		{
			return new ArrayRangedWrapper<TData>(data);
		}

		/// <summary>
		/// Create a new ArrayRange, but without having to specify the type parameter explicitly.
		/// </summary>
		/// <typeparam name="TData"></typeparam>
		/// <param name="data"></param>
		/// <param name="start"></param>
		/// <param name="length"></param>
		/// <returns></returns>
		public static ArrayRangedWrapper<TData> New<TData>(TData[] data, int start, int length)
		{
			return new ArrayRangedWrapper<TData>(data, start, length);
		}
	}

	/// <summary>
	/// A wrapper for an array that exposes only a subset of that array.
	/// </summary>
	/// <typeparam name="TData">The type of the elements in the array.</typeparam>
	public class ArrayRangedWrapper<TData> : IReadOnlyList<TData>
	{
		/// <summary>
		/// The data to be wrapped.
		/// </summary>
		private readonly TData[] data;

		/// <summary>
		/// The index of the first element that the wrapper exposes.
		/// </summary>
		public readonly int Start;

		/// <summary>
		/// The Length of the exposed segment.
		/// </summary>
		public readonly int Length;

		/// <summary>
		/// Create a new wrapper that exposes the entire source array.
		/// </summary>
		/// <param name="data">The array to wrap.</param>
		public ArrayRangedWrapper(TData[] data)
		{
			this.data = data;
			this.Start = 0;
			this.Length = data.Length;
		}

		/// <summary>
		/// Create a new wrapper that exposes a segment of the source array.
		/// </summary>
		/// <param name="data">The array to wrap.</param>
		/// <param name="start">The index of the first available element.</param>
		/// <param name="length">The amount of elements to expose.</param>
		public ArrayRangedWrapper(TData[] data, int start, int length)
		{
			if (start > data.Length)
				throw new ArgumentOutOfRangeException(nameof(start), start, $"{nameof(start)} > {nameof(data)}.length :: {start} > {data.Length}");

			if (start + length > data.Length)
			{
				throw new ArgumentOutOfRangeException(
					nameof(length),
					length,
					$"{nameof(start)} + {nameof(length)} > {nameof(data)}.length :: ({start} + {length} = {start + length}) > {data.Length}");

			}

			this.data = data;
			this.Start = start;
			this.Length = length;
		}

		/// <summary>
		/// Index into the wrapped data [0..Length - 1].
		/// </summary>
		/// <param name="i">The 0 based index of the element in the wrapped segment.</param>
		/// <returns>The value at the specified index.</returns>
		public TData this[int i]
		{
			get
			{
				if (i < 0) throw new IndexOutOfRangeException($"{nameof(i)} < 0");
				if (i >= Length) throw new IndexOutOfRangeException($"{nameof(i)} >= {nameof(Length)}");
				return data[i + Start];
			}
			set
			{
				if (i < 0) throw new IndexOutOfRangeException($"{nameof(i)} < 0");
				if (i >= Length) throw new IndexOutOfRangeException($"{nameof(i)} >= {nameof(Length)}");
				data[i + Start] = value;
			}
		}

		/// <summary>
		/// Get a copy of the wrapped data as a new Array.
		/// </summary>
		public TData[] Data => this.AsEnumerable().ToArray();

		public static implicit operator TData[](ArrayRangedWrapper<TData> rangedWrapper) => rangedWrapper.Data;

		/// <inheritdoc />
		public IEnumerator<TData> GetEnumerator()
		{
			for (int i = Start; i < Start + Length; i++)
				yield return data[i];
		}

		/// <summary>
		/// The count of elements in the exposed segment. Same as <see cref="Length"/>.
		/// </summary>
		public int Count => Length;

		/// <inheritdoc />
		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}
	}
}
