﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/


using System;

namespace Shared.Validation
{
	/// <summary>
	/// Provides a diagnostic report with a <see cref="Severity"/>, <see cref="Message"/> and <see cref="Location"/>.
	/// <br/>
	/// <see cref="Diagnostic"/>s are created by a <see cref="Validator"/>.
	/// </summary>
	public class Diagnostic
	{
		/// <summary>
		/// The severity level of the <see cref="Diagnostic"/>.
		/// </summary>
		public readonly DiagnosticSeverity Severity;
		/// <summary>
		/// The message describing the diagnostic.
		/// </summary>
		public readonly string Message;
		/// <summary>
		/// The location of the diagnostic. The exact format depends on what is being validated.
		/// In general it's a stack of names (and indices when collections are involved)
		/// , starting with the current location and ending with the object where validation started.
		/// </summary>
		public readonly string Location;

		internal Diagnostic(DiagnosticSeverity severity, string message, string location)
		{
			if (severity == DiagnosticSeverity.None)
				throw new ArgumentException($"{nameof(DiagnosticSeverity.None)} should not be used as a severity.", nameof(severity));

			Severity = severity;
			Message = message ?? throw new ArgumentNullException(nameof(message));
			Location = location ?? throw new ArgumentNullException(nameof(location));
		}

		/// <inheritdoc />
		public override string ToString()
		{
			return $"{Severity}: {Message}\n{Location}";
		}

		/// <summary>
		/// Return <see cref="ToString"/> but without <see cref="Location"/>.
		/// </summary>
		/// <returns></returns>
		public string ToStringExceptLocation()
		{
			return $"{Severity}: {Message}";
		}
	}
}
