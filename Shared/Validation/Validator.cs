﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/


using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace Shared.Validation
{
	/// <summary>
	/// A <see cref="Validator"/> can validate an <see cref="IValidateable"/>.
	/// It will collect the <see cref="Diagnostic"/>s that are generated.
	/// <br/>
	/// A <see cref="Validator"/> instance can be used by a single thread at a time.
	/// Call <see cref="Clear"/> to prepare it for re-use.
	/// </summary>
	public class Validator
	{
		private readonly List<Diagnostic> diagnostics = new List<Diagnostic>();

		/// <summary>
		/// The <see cref="Diagnostic"/>s collected by this <see cref="Validator"/>.
		/// </summary>
		public IReadOnlyList<Diagnostic> Diagnostics;

		private DiagnosticSeverity worstSeverity = DiagnosticSeverity.None;

		/// <summary>
		/// The highest/worst <see cref="DiagnosticSeverity"/> the <see cref="Validator"/> has observed so far.
		/// </summary>
		public DiagnosticSeverity WorstSeverity => worstSeverity;


		private readonly Stack<ValidationContext> contextStack = new Stack<ValidationContext>();

		/// <summary>
		/// Retrieves the current location where validation is taking place,
		/// as a list of member names (and indices into collections if applicable)
		/// starting with the current/deepest member and ending with the object Validation was started with.
		/// </summary>
		public string Location => "   at " + string.Join("\n   at ", contextStack);


		public Validator()
		{
			Diagnostics = new ReadOnlyCollection<Diagnostic>(diagnostics);
		}

		/// <summary>
		/// Add a <see cref="Diagnostic"/> to <see cref="diagnostics"/> and update <see cref="WorstSeverity"/>.
		/// </summary>
		/// <param name="d"></param>
		private void Add(Diagnostic d)
		{
			_ = d ?? throw new ArgumentNullException(nameof(d));
			diagnostics.Add(d);

			if (d.Severity > worstSeverity)
			{
				worstSeverity = d.Severity;
			}
		}

		/// <summary>
		/// Clear and Reset the <see cref="Validator"/> so it can be re-used.
		/// </summary>
		public void Clear()
		{
			diagnostics.Clear();
			worstSeverity = DiagnosticSeverity.None;
			contextStack.Clear();
		}

		#region Create Diagnostics

		/// <summary>
		/// Generate a <see cref="Diagnostic"/> of <see cref="DiagnosticSeverity.Warning"/> with <paramref name="message"/>.
		/// </summary>
		/// <param name="message">The message of the <see cref="Diagnostic"/>.</param>
		public void Warning(string message)
		{
			_ = message ?? throw new ArgumentNullException(nameof(message));
			Add(new Diagnostic(DiagnosticSeverity.Warning, message, Location));
		}

		/// <summary>
		/// When <paramref name="condition"/> is <see langword="true"/>
		/// add a <see cref="Diagnostic"/> with <see cref="DiagnosticSeverity.Warning"/> with <paramref name="message"/>.
		/// </summary>
		/// <param name="condition"></param>
		/// <param name="message"></param>
		public void Warning(bool condition, string message)
		{
			_ = message ?? throw new ArgumentNullException(nameof(message));
			if (! condition) return;
			Add(new Diagnostic(DiagnosticSeverity.Warning, message, Location));
		}


		/// <summary>
		/// Generate a <see cref="Diagnostic"/> of <see cref="DiagnosticSeverity.Error"/> with <paramref name="message"/>.
		/// </summary>
		/// <param name="message">The message of the <see cref="Diagnostic"/>.</param>
		public void Error(string message)
		{
			_ = message ?? throw new ArgumentNullException(nameof(message));
			Add(new Diagnostic(DiagnosticSeverity.Error, message, Location));
		}

		/// <summary>
		/// When <paramref name="condition"/> is <see langword="true"/>
		/// add a <see cref="Diagnostic"/> with <see cref="DiagnosticSeverity.Error"/> with <paramref name="message"/>.
		/// </summary>
		/// <param name="condition"></param>
		/// <param name="message"></param>
		public void Error(bool condition, string message)
		{
			_ = message ?? throw new ArgumentNullException(nameof(message));
			if (!condition) return;
			Add(new Diagnostic(DiagnosticSeverity.Error, message, Location));
		}


		#region SpecificDiagnostics
		/// <summary>
		/// Add a <see cref="DiagnosticSeverity.Warning"/> <see cref="Diagnostic"/> when <paramref name="collection"/> is empty.
		/// </summary>
		/// <typeparam name="TElement"></typeparam>
		/// <param name="collection"></param>
		/// <param name="memberName"></param>
		public void WarnIfEmpty<TElement>(ICollection<TElement> collection, string memberName)
		{
			_ = memberName ?? throw new ArgumentNullException(nameof(memberName));

			Warning(collection.Count == 0, $"{memberName} contains no elements.");
		}

		/// <summary>
		/// Add a <see cref="DiagnosticSeverity.Error"/> <see cref="Diagnostic"/> when <paramref name="collection"/> is empty.
		/// </summary>
		/// <typeparam name="TElement"></typeparam>
		/// <param name="collection"></param>
		/// <param name="memberName"></param>
		public void ErrorIfEmpty<TElement>(ICollection<TElement> collection, string memberName)
		{
			_ = memberName ?? throw new ArgumentNullException(nameof(memberName));

			Error(collection.Count == 0, $"{memberName} contains no elements.");
		}


		#endregion SpecificDiagnostics

		#endregion Create Diagnostics

		#region Validate
		/// <summary>
		/// Validate <paramref name="toValidate"/>, generating a <see cref="Diagnostic"/>s
		/// using <paramref name="memberName"/> as the <see cref="Diagnostic.Location"/>.
		/// <br/>
		/// This method will use <see cref="PushContext(string)"/>.
		/// </summary>
		/// <param name="toValidate">The object to be validated.</param>
		/// <param name="memberName">The name of the member to display in the <see cref="Diagnostic.Location"/>.</param>
		/// <param name="allowNull">Allow <see langword="null"/> values.</param>
		public void Validate(IValidateable toValidate, string memberName, bool allowNull = false)
		{
			_ = memberName ?? throw new ArgumentNullException(nameof(memberName));

			if (null == toValidate)
			{
				if(allowNull) return;

				Add(new Diagnostic(DiagnosticSeverity.Error, $"Missing required {memberName}", Location));
				return;
			}

			using (_ = PushContext(memberName))
			{
				toValidate.Validate(this);
			}
		}

		/// <summary>
		/// Validate <paramref name="toValidate"/>, generating a
		/// <see cref="Diagnostic"/>s using the name 'element' as the <see cref="Diagnostic.Location"/>.
		/// <br/>
		/// This method is meant to be used in combination with <see cref="PushContext(string, int)"/>.
		/// </summary>
		/// <param name="toValidate">The object to be validated.</param>
		/// <param name="allowNull">Allow <see langword="null"/> values.</param>
		public void Validate(IValidateable toValidate, bool allowNull = false)
		{
			if (null == toValidate)
			{
				if (allowNull) return;

				Add(new Diagnostic(DiagnosticSeverity.Error, $"Encountered null element.", Location));
				return;
			}

			toValidate.Validate(this);
		}

		/// <summary>
		/// Validate that the provided enum value is a defined value for that enum:
		/// That the enum contains a name that maps to the provided value.
		/// </summary>
		/// <typeparam name="TEnum">The type of the enum.</typeparam>
		/// <param name="toValidate">The value to validate.</param>
		/// <param name="memberName">The name of the member being checked.</param>
		public void Validate<TEnum>(TEnum toValidate, string memberName) where TEnum : System.Enum
		{
			_ = memberName ?? throw new ArgumentNullException(nameof(memberName));

			if (! toValidate.IsDefined())
			{
				Error($"Invalid Enum value '{toValidate}' for: {memberName}.");
			}
		}

		/// <summary>
		/// Validate a <see cref="ICollection{T}"/> of <see cref="IValidateable"/>.
		/// </summary>
		/// <typeparam name="TElement">The type of the elements.</typeparam>
		/// <param name="collection">The collection to validate.</param>
		/// <param name="collectionName">The name of the Collection to display in any <see cref="Diagnostic"/>s.</param>
		/// <param name="emptySeverity">The severity of <see cref="Diagnostic"/> when the collection is empty.</param>
		/// <param name="allowNullCollection">Is the collection allowed to be <see langword="null"/>.</param>
		/// <param name="allowNullElements">Are elements in the collection allowed to be <see langword="null"/>.</param>
		public void Validate<TElement>(ICollection<TElement> collection,
		                               string collectionName,
		                               DiagnosticSeverity emptySeverity = DiagnosticSeverity.None,
		                               bool allowNullCollection = false,
		                               bool allowNullElements = false)
			where TElement : IValidateable
		{
			_ = collectionName ?? throw new ArgumentNullException(nameof(collectionName));

			if (null == collection)
			{
				Error(! allowNullCollection, $"{collectionName} was null.");
				return;
			}

			if (collection.Count == 0 && emptySeverity != DiagnosticSeverity.None)
			{
				Add(new Diagnostic(emptySeverity, $"{collectionName} contains no elements.", Location));
				return;
			}

			foreach ((TElement item, int index) in collection.EnumerateWithIndices())
			{

				using (_ = PushContext(collectionName, index))
				{
					Validate(item, allowNullElements);
				}
			}
		}

		#endregion Validate

		#region SpecialValidation
		/// <summary>
		/// Validate that the provided float is finite and generate an
		/// <see cref="DiagnosticSeverity.Error"/> level <see cref="Diagnostic"/> otherwise.
		/// </summary>
		/// <param name="value">The value to validate.</param>
		/// <param name="memberName">The name of the member being validated.</param>
		/// <exception cref="ArgumentNullException"></exception>
		public void ValidateFloatFinite(float value, string memberName)
		{
			_ = memberName ?? throw new ArgumentNullException(nameof(memberName));

			if (! value.IsFinite())
			{
				Error($"{memberName} was not finite: {value}");
			}
		}

		#endregion SpecialValidation



		/// <summary>
		/// Throw an <see cref="AggregateException"/> containing all <see cref="diagnostics"/> with severity <see cref="DiagnosticSeverity.Error"/> or above.
		/// </summary>
		public void ThrowErrors()
		{
			if (worstSeverity < DiagnosticSeverity.Error)
			{
				return;
			}

			throw new ValidationException(diagnostics.Where(d => d.Severity >= DiagnosticSeverity.Error).ToArray());
		}

		/// <summary>
		/// Generate a string for all <see cref="Diagnostic"/>s provided.
		/// </summary>
		/// <param name="diagnostics">The diagnostics to generate a string representation for.</param>
		/// <param name="entryHandler">Receives the severity and string for a entry, could be used to format the string.</param>
		/// <returns></returns>
		public static string ToString(IEnumerable<Diagnostic> diagnostics, Func<DiagnosticSeverity, string, string> entryHandler = null)
		{
			var sb = new StringBuilder();
			foreach (Diagnostic diagnostic in diagnostics)
			{
				string str = entryHandler != null
					? entryHandler.Invoke(diagnostic.Severity, diagnostic.ToString())
					: diagnostic.ToString();

				sb.AppendLine(str);
			}

			return sb.ToString();
		}

		/// <summary>
		/// Generate a string for all entries in the <see cref="Validator"/>.
		/// </summary>
		/// <returns></returns>
		public override string ToString()
		{
			return ToString(diagnostics);
		}

		#region ContextStack

		/// <summary>
		/// A token to be placed in a <see langword="using"/> construct to keep track of <see cref="Validation.Validator.Location"/>.
		/// <br/>
		/// Use <see cref="Validation.Validator.PushContext(string)"/> or <see cref="Validation.Validator.PushContext(string, int)"/>
		/// to receive one.
		/// </summary>
		public readonly struct ValidationContext : IDisposable, IEquatable<ValidationContext>
		{
			internal readonly Validator Validator;

			public readonly string MemberName;
			public readonly int? MemberIndex;

			internal ValidationContext(Validator validator, string memberName)
			{
				Validator = validator   ?? throw new ArgumentNullException(nameof(validator));
				MemberName = memberName ?? throw new ArgumentNullException(nameof(memberName));
				MemberIndex = null;
			}

			internal ValidationContext(Validator validator, string memberName, int memberIndex)
			{
				Validator = validator ?? throw new ArgumentNullException(nameof(validator));
				MemberName = memberName ?? throw new ArgumentNullException(nameof(memberName));
				MemberIndex = memberIndex;
			}

			/// <summary>
			/// Usage of this method is not recommended.
			/// <br/>
			/// Instead make use of the <see langword="using"/> construct to handle this automatically.
			/// <br/>
			/// This method simply calls <see cref="Validation.Validator.PopContext(ValidationContext)"/>, see that method for more details.
			/// </summary>
			public void Dispose()
			{
				Validator.PopContext(this);
			}

			/// <inheritdoc />
			public bool Equals(ValidationContext other)
			{
				return Validator == other.Validator
				    && MemberName  == other.MemberName
				    && MemberIndex == other.MemberIndex;
			}

			/// <inheritdoc />
			public override bool Equals(object obj)
			{
				return obj is ValidationContext other && Equals(other);
			}

			public static bool operator ==(ValidationContext lhs, ValidationContext rhs)
			{
				return lhs.Equals(rhs);
			}

			public static bool operator !=(ValidationContext lhs, ValidationContext rhs)
			{
				return ! lhs.Equals(rhs);
			}

			/// <inheritdoc />
			public override int GetHashCode()
			{
				unchecked
				{
					int hashCode = Validator.GetHashCode();
					hashCode = (hashCode * 397) ^ MemberName.GetHashCode();
					hashCode = (hashCode * 397) ^ MemberIndex.GetHashCode();
					return hashCode;
				}
			}

			/// <inheritdoc />
			public override string ToString()
			{
				return MemberIndex.HasValue
					? $"{MemberName}#{MemberIndex.Value}"
					: $"{MemberName}";
			}
		}


		/// <summary>
		/// When a member of an <see cref="IValidateable"/> is itself <see cref="IValidateable"/>
		/// put the return value in a <see langword="using"/> construct to keep the
		/// <see cref="Location"/> of the <see cref="Validator"/> up to date.
		/// </summary>
		/// <param name="memberName">The name of the Member.</param>
		/// <returns></returns>
		/// <exception cref="ArgumentNullException"></exception>
		public ValidationContext PushContext(string memberName)
		{
			_ = memberName ?? throw new ArgumentNullException(nameof(memberName));

			var context = new ValidationContext(this, memberName);

			contextStack.Push(context);
			return context;
		}

		/// <summary>
		/// When a member of an <see cref="IValidateable"/> is a collection of <see cref="IValidateable"/>
		/// and <see cref="Validate{T}(ICollection{T}, string, DiagnosticSeverity, bool, bool)"/>
		/// cannot be used:
		/// Put the return value in a <see langword="using"/> construct to keep the <see cref="Location"/> of the <see cref="Validator"/> up to date.
		/// </summary>
		/// <param name="memberName">The name of the collection.</param>
		/// <param name="index">The index of the current element.</param>
		/// <returns></returns>
		/// <exception cref="ArgumentNullException"></exception>
		public ValidationContext PushContext(string memberName, int index)
		{
			_ = memberName ?? throw new ArgumentNullException(nameof(memberName));

			var context = new ValidationContext(this, memberName, index);

			contextStack.Push(context);
			return context;
		}

		/// <summary>
		/// Usage of this method is not recommended.
		/// <br/>
		/// Instead put <see cref="PushContext(string)"/> or <see cref="PushContext(string, int)"/>
		/// inside the <see langword="using"/> construct to handle this automatically.
		/// <br/>
		/// Leaving contexts out of order will cause an <see cref="InvalidOperationException"/>.
		/// Use this method ONLY if you have special needs that <see langword="using"/> cannot provide.
		/// </summary>
		/// <param name="context"></param>
		/// <exception cref="InvalidOperationException"></exception>
		public void PopContext(ValidationContext context)
		{
			if (contextStack.Peek() == context)
			{
				contextStack.Pop();
			}
			else
			{
				throw new InvalidOperationException($"Leaving contexts out of order.");
			}
		}
		#endregion ContextStack
	}
}
