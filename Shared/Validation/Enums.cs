﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/


namespace Shared.Validation
{
	public enum DiagnosticSeverity : byte
	{
		/// <summary>
		/// Placeholder value: do <strong>not</strong> use as the SeverityLevel of a <see cref="Diagnostic"/>.
		/// </summary>
		None = 0,
		/// <summary>
		/// Use for general information.
		/// </summary>
		Information,
		/// <summary>
		/// Use for cases where a problem was automatically mitigated.
		/// </summary>
		Mitigation,
		/// <summary>
		/// Use for cases where there is something wrong, but the problem will not cause a critical error or crash.
		/// </summary>
		Warning,
		/// <summary>
		/// Use for cases where the problem could cause a critical error or crash.
		/// </summary>
		Error,
		/// <summary>
		/// Use for cases where the problem could cause collection of additional <see cref="Diagnostic"/>s to fail or misreport.
		/// </summary>
		Critical,
	}
}
