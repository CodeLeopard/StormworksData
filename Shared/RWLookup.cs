﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shared
{
	public class RWLookup<TKey, TValue> : IEnumerable<RWLookup<TKey, TValue>.Grouping<TKey, TValue>>
	{
		private readonly Func<TValue, TKey> selector;
		private readonly Dictionary<TKey, List<TValue>> lookup = new Dictionary<TKey, List<TValue>>();


		public RWLookup(IEnumerable<TValue> data, Func<TValue, TKey> selector)
		{
			this.selector = selector;
			foreach (TValue value in data)
			{
				var key = selector.Invoke(value);
				if (lookup.TryGetValue(key, out var list))
				{
					list.Add(value);
				}
				else
				{
					list = new List<TValue>();
					list.Add(value);
					lookup.Add(key, list);
				}
			}
		}

		public bool TryRemove(TValue data)
		{
			var key = selector.Invoke(data);
			if (! lookup.TryGetValue(key, out var list))
			{
				return false;
			}

			return list.Remove(data);
		}

		public Grouping<TKey, TValue> this[TKey key]
		{
			get
			{
				if (lookup.TryGetValue(key, out var list))
				{
					return new Grouping<TKey, TValue>(key, list);
				}
				else
				{
					return new Grouping<TKey, TValue>(key);
				}
			}
		}

		/// <inheritdoc />
		public IEnumerator<Grouping<TKey, TValue>> GetEnumerator()
		{
			foreach (KeyValuePair<TKey, List<TValue>> kvp in lookup)
			{
				yield return new Grouping<TKey, TValue>(kvp.Key, kvp.Value);
			}
		}

		/// <inheritdoc />
		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		public IEnumerable<TValue> GetValueEnumerator()
		{
			foreach (var kvp in lookup)
			{
				foreach (TValue value in kvp.Value)
				{
					yield return value;
				}
			}
		}


		public class Grouping<TKey, TElement> : IList<TElement>, IGrouping<TKey, TElement>
		{
			private readonly List<TElement> list;

			public TKey Key { get; }

			public Grouping(TKey key)
			{
				list = new List<TElement>();
			}

			public Grouping(TKey key, int capacity)
			{
				list = new List<TElement>(capacity);
			}

			public Grouping(TKey key, List<TElement> list)
			{
				this.Key = key;
				this.list = list;
			}


			/// <inheritdoc />
			public IEnumerator<TElement> GetEnumerator()
			{
				return list.GetEnumerator();
			}

			/// <inheritdoc />
			IEnumerator IEnumerable.GetEnumerator()
			{
				return ((IEnumerable)list).GetEnumerator();
			}

			/// <inheritdoc />
			public void Add(TElement item)
			{
				list.Add(item);
			}

			/// <inheritdoc />
			public void Clear()
			{
				list.Clear();
			}

			/// <inheritdoc />
			public bool Contains(TElement item)
			{
				return list.Contains(item);
			}

			/// <inheritdoc />
			public void CopyTo(TElement[] array, int arrayIndex)
			{
				list.CopyTo(array, arrayIndex);
			}

			/// <inheritdoc />
			public bool Remove(TElement   item)
			{
				return list.Remove(item);
			}

			/// <inheritdoc />
			public int Count => list.Count;

			/// <inheritdoc />
			public bool IsReadOnly => false;

			/// <inheritdoc />
			public int IndexOf(TElement item)
			{
				return list.IndexOf(item);
			}

			/// <inheritdoc />
			public void Insert(int   index, TElement item)
			{
				list.Insert(index, item);
			}

			/// <inheritdoc />
			public void RemoveAt(int index)
			{
				list.RemoveAt(index);
			}

			/// <inheritdoc />
			public TElement this[int index]
			{
				get => list[index];
				set => list[index] = value;
			}

			public override string ToString()
			{
				return $"{{Grouping at {Key} with {Count} elements}}";
			}
		}
	}
}
