﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace Shared
{
	public static class ExceptionExt
	{
		/// <summary>
		/// Generate a string for an <see cref="Exception"/>, with an optional
		/// formatter for extra fields.
		/// </summary>
		/// <param name="ex"></param>
		/// <param name="customFieldsFormatterAction"></param>
		/// <returns></returns>
		public static string ExceptionToString
		(
			this Exception        ex,
			Action<StringBuilder> customFieldsFormatterAction = null
		)
		{
			StringBuilder description = new StringBuilder();
			description.AppendFormat("{0}: {1}", ex.GetType().Name, ex.Message);

			if (customFieldsFormatterAction != null)
				customFieldsFormatterAction(description);

			if (ex.InnerException != null)
			{
				description.AppendFormat("{1} ---> {0}", ex.InnerException, Environment.NewLine);
				description.AppendFormat(
				                         "{0}   --- End of inner exception stack trace ---{0}",
				                         Environment.NewLine);
			}

			description.Append(ex.StackTrace);

			return description.ToString();
		}

		/// <summary>
		/// Override the normally not writable <see cref="Exception.StackTrace"/> property using optimized reflection caller.
		/// </summary>
		/// <param name="target">The <see cref="Exception"/> to operate on.</param>
		/// <param name="stack">The <see cref="StackTrace"/> to apply.</param>
		/// <returns><paramref name="target"/></returns>
		public static Exception SetStackTrace(this Exception target, StackTrace stack) => _SetStackTrace(target, stack);

		private static readonly Func<Exception, StackTrace, Exception> _SetStackTrace
			= new Func<Func<Exception, StackTrace, Exception>>(() =>
		{
			//https://stackoverflow.com/a/63685720
			ParameterExpression target = Expression.Parameter(typeof(Exception));
			ParameterExpression stack = Expression.Parameter(typeof(StackTrace));
			Type traceFormatType = typeof(StackTrace).GetNestedType("TraceFormat", BindingFlags.NonPublic);
			MethodInfo toString = typeof(StackTrace).GetMethod("ToString", BindingFlags.NonPublic | BindingFlags.Instance, null, new[] { traceFormatType }, null);
			object normalTraceFormat = Enum.GetValues(traceFormatType).GetValue(0);
			MethodCallExpression stackTraceString = Expression.Call(stack, toString, Expression.Constant(normalTraceFormat, traceFormatType));
			FieldInfo stackTraceStringField = typeof(Exception).GetField("_stackTraceString", BindingFlags.NonPublic | BindingFlags.Instance);
			BinaryExpression assign = Expression.Assign(Expression.Field(target, stackTraceStringField), stackTraceString);
			return Expression.Lambda<Func<Exception, StackTrace, Exception>>(Expression.Block(assign, target), target, stack).Compile();
		})();
	}
}

