﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Text;

using OpenToolkit.Mathematics;

namespace Shared
{
	public static class VectorExt
	{
		/// <summary>
		/// Project <paramref name="vector"/> onto <paramref name="direction"/>.
		/// </summary>
		/// <param name="vector">The vector to project.</param>
		/// <param name="direction">The vector to project onto.</param>
		/// <returns><paramref name="vector"/> projected onto <paramref name="direction"/>.
		/// IF the Length of <paramref name="direction"/> is 0 then <see cref="Vector3.Zero"/> is returned.</returns>
		[Pure]
		public static Vector3 Project(Vector3 vector, Vector3 direction)
		{
			float directionLengthSquared = direction.LengthSquared;
			if (directionLengthSquared < float.Epsilon)
			{
				return Vector3.Zero;
			}
			else
			{
				return Vector3.Dot(vector, direction) * direction * (1 / directionLengthSquared);
			}
		}

		public static float Angle(Vector3 a, Vector3 b)
		{
			return (float)Math.Acos(Vector3.Dot(a.Normalized(), b.Normalized()));
		}

		public static Vector3 WithZ(this Vector2 instance, float Z)
		{
			return new Vector3(instance.X, instance.Y, Z);
		}

		public static Vector4 WithW(this Vector3 instance, float W)
		{
			return new Vector4(instance.X, instance.Y, instance.Z, W);
		}

		public static int ManhattanLength(this Vector2i v)
		{
			return v.X + v.Y;
		}

		public static int ManhattanLength(this Vector3i v)
		{
			return v.X + v.Y + v.Z;
		}

		public static int ManhattanLength(this Vector4i v)
		{
			return v.X + v.Y + v.Z + v.W;
		}

		public static Color4 ToColor4(this Vector4 color)
		{
			return new Color4(color.X, color.Y, color.Z, color.W);
		}

		public static Vector4 ToVector4(this Color4 color)
		{
			return new Vector4(color.R, color.G, color.B, color.A);
		}

		public static IEnumerable<Vector3i> Neighbours(Vector3i v)
		{
			yield return v + new Vector3i( 1,  0,  0);
			yield return v + new Vector3i(-1,  0,  0);
			yield return v + new Vector3i( 0,  1,  0);
			yield return v + new Vector3i( 0, -1,  0);
			yield return v + new Vector3i( 0,  0,  1);
			yield return v + new Vector3i( 0,  0, -1);
		}

		/// <summary>
		/// Returns true when the difference between <paramref name="a"/> and <paramref name="b"/>
		/// is less than or equal to <paramref name="margin"/>.
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <param name="margin"></param>
		/// <returns></returns>
		public static bool ApproxEquals(this Vector2 a, Vector2 b, float margin = 0)
		{
			var dist = a - b;
			var len = dist.LengthSquared;
			return len <= (margin * margin);
		}

		/// <summary>
		/// Returns true when the difference between <paramref name="a"/> and <paramref name="b"/>
		/// is less than or equal to <paramref name="margin"/>.
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <param name="margin"></param>
		/// <returns></returns>
		public static bool ApproxEquals(this Vector3 a, Vector3 b, float margin = 0)
		{
			var dist = a - b;
			var len = dist.LengthSquared;
			return len <= (margin * margin);
		}

		/// <summary>
		/// Returns true when the difference between <paramref name="a"/> and <paramref name="b"/>
		/// is less than or equal to <paramref name="margin"/>.
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <param name="margin"></param>
		/// <returns></returns>
		public static bool ApproxEquals(this Vector4 a, Vector4 b, float margin = 0)
		{
			var dist = a - b;
			var len = dist.LengthSquared;
			return len <= (margin * margin);
		}


		#region Checks

		/// <summary>
		/// Returns true if any component of the vector is NaN
		/// </summary>
		/// <param name="v"></param>
		/// <returns></returns>
		public static bool HasNan(this Vector2 v)
		{
			return float.IsNaN(v.X) || float.IsNaN(v.Y);
		}


		/// <summary>
		/// Returns true if any component of the vector is NaN
		/// </summary>
		/// <param name="v"></param>
		/// <returns></returns>
		public static bool HasNan(this Vector3 v)
		{
			return float.IsNaN(v.X) || float.IsNaN(v.Y) || float.IsNaN(v.Z);
		}


		/// <summary>
		/// Returns true if any component of the vector is NaN
		/// </summary>
		/// <param name="v"></param>
		/// <returns></returns>
		public static bool HasNan(this Vector4 v)
		{
			return float.IsNaN(v.X) || float.IsNaN(v.Y) || float.IsNaN(v.Z) || float.IsNaN(v.W);
		}

		/// <summary>
		/// Returns true if all components of the vector are finite, that is: not infinite, NaN or otherwise not a valid number.
		/// </summary>
		/// <param name="v"></param>
		/// <returns></returns>
		public static bool IsFinite(this Vector2 v)
		{
			return FloatExt.IsFinite(v.X) && FloatExt.IsFinite(v.Y);
		}


		/// <summary>
		/// Returns true if all components of the vector are finite, that is: not infinite, NaN or otherwise not a valid number.
		/// </summary>
		/// <param name="v"></param>
		/// <returns></returns>
		public static bool IsFinite(this Vector3 v)
		{
			return FloatExt.IsFinite(v.X) && FloatExt.IsFinite(v.Y) && FloatExt.IsFinite(v.Z);
		}


		/// <summary>
		/// Returns true if all components of the vector are finite, that is: not infinite, NaN or otherwise not a valid number.
		/// </summary>
		/// <param name="v"></param>
		/// <returns></returns>
		public static bool IsFinite(this Vector4 v)
		{
			return FloatExt.IsFinite(v.X) && FloatExt.IsFinite(v.Y) && FloatExt.IsFinite(v.Z) && FloatExt.IsFinite(v.W);
		}


		#endregion Checks
	}
}
