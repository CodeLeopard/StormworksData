﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Globalization;
using System.Xml.Linq;

using OpenToolkit.Mathematics;

namespace Shared.Serialization.DataTypes
{
	public static class VectorSer
	{
		private static CultureInfo IC = CultureInfo.InvariantCulture;

		public static Color4 FromRGBA(uint rgba)
		{
			unchecked
			{
				byte r = (byte)(rgba >> (8 * 3));
				byte g = (byte)(rgba >> (8 * 2));
				byte b = (byte)(rgba >> (8 * 1));
				byte a = (byte)(rgba >> (8 * 0));
				var result = new Color4(r, g, b, a);
				return result;
			}
		}

		public static Color4 FromRGB(uint rgb)
		{
			unchecked
			{
				byte r = (byte)(rgb >> (8 * 2));
				byte g = (byte)(rgb >> (8 * 1));
				byte b = (byte)(rgb >> (8 * 0));
				var result = new Color4(r, g, b, 255);
				return result;
			}
		}

		public static Vector2 Parse2f(XElement vectorElement, bool allowNull = false)
		{
			if (null == vectorElement)
			{
				if (allowNull) return default;
				throw new ArgumentNullException(nameof(vectorElement));
			}

			var attributes = vectorElement.Attributes().XAttributeToDictionary();
			attributes.TryGetFloat("x", out float x);
			attributes.TryGetFloat("y", out float y);
			return new Vector2(x, y);
		}
		public static Vector2d Parse2d(XElement vectorElement, bool allowNull = false)
		{
			if (null == vectorElement)
			{
				if (allowNull) return default;
				throw new ArgumentNullException(nameof(vectorElement));
			}

			var attributes = vectorElement.Attributes().XAttributeToDictionary();
			attributes.TryGetDouble("x", out double x);
			attributes.TryGetDouble("y", out double y);
			return new Vector2d(x, y);
		}

		public static Vector3i Parse3i(XElement vectorElement, bool allowNull = false)
		{
			if (null == vectorElement)
			{
				if (allowNull) return default;
				throw new ArgumentNullException(nameof(vectorElement));
			}

			var attributes = vectorElement.Attributes().XAttributeToDictionary();
			attributes.TryGetInt32("x", out int x);
			attributes.TryGetInt32("y", out int y);
			attributes.TryGetInt32("z", out int z);
			return new Vector3i(x, y, z);
		}

		public static Vector3 Parse3f(XElement vectorElement, bool allowNull = false, Vector3 nullValue = default)
		{
			if (null == vectorElement)
			{
				if (allowNull) return nullValue;
				throw new ArgumentNullException(nameof(vectorElement));
			}

			var attributes = vectorElement.Attributes().XAttributeToDictionary();
			attributes.TryGetFloat("x", out float x);
			attributes.TryGetFloat("y", out float y);
			attributes.TryGetFloat("z", out float z);
			return new Vector3(x, y, z);
		}
		public static Vector3d Parse3d(XElement vectorElement, bool allowNull = false)
		{
			if (null == vectorElement)
			{
				if (allowNull) return default;
				throw new ArgumentNullException(nameof(vectorElement));
			}

			var attributes = vectorElement.Attributes().XAttributeToDictionary();
			attributes.TryGetDouble("x", out double x);
			attributes.TryGetDouble("y", out double y);
			attributes.TryGetDouble("z", out double z);
			return new Vector3d(x, y, z);
		}

		public static Vector4 Parse4i(XElement vectorElement, bool allowNull = false)
		{
			if (null == vectorElement)
			{
				if (allowNull) return default;
				throw new ArgumentNullException(nameof(vectorElement));
			}

			var attributes = vectorElement.Attributes().XAttributeToDictionary();
			attributes.TryGetInt32("x", out int x);
			attributes.TryGetInt32("y", out int y);
			attributes.TryGetInt32("z", out int z);
			attributes.TryGetInt32("w", out int w);
			return new Vector4(x, y, z, w);
		}

		public static Vector4 Parse4f(XElement vectorElement, bool allowNull = false)
		{
			if (null == vectorElement)
			{
				if (allowNull) return default;
				throw new ArgumentNullException(nameof(vectorElement));
			}

			var attributes = vectorElement.Attributes().XAttributeToDictionary();
			attributes.TryGetFloat("x", out float x);
			attributes.TryGetFloat("y", out float y);
			attributes.TryGetFloat("z", out float z);
			attributes.TryGetFloat("w", out float w);
			return new Vector4(x, y, z, w);
		}


		public static Vector4d Parse4d(XElement vectorElement, bool allowNull = false)
		{
			if (null == vectorElement)
			{
				if (allowNull) return default;
				throw new ArgumentNullException(nameof(vectorElement));
			}

			var attributes = vectorElement.Attributes().XAttributeToDictionary();
			attributes.TryGetDouble("x", out double x);
			attributes.TryGetDouble("y", out double y);
			attributes.TryGetDouble("z", out double z);
			attributes.TryGetDouble("w", out double w);
			return new Vector4d(x, y, z, w);
		}
		public static XElement ToXElement(this Vector2 v, string elementName)
		{
			var e = new XElement(elementName);
			e.SetAttributeValue("x", v.X);
			e.SetAttributeValue("y", v.Y);
			return e;
		}

		public static XElement ToXElement(this Vector3 v, string elementName)
		{
			var e = new XElement(elementName);
			e.SetAttributeValue("x", v.X);
			e.SetAttributeValue("y", v.Y);
			e.SetAttributeValue("z", v.Z);
			return e;
		}

		public static XElement ToXElement(this Vector4 v, string elementName)
		{
			var e = new XElement(elementName);
			e.SetAttributeValue("x", v.X);
			e.SetAttributeValue("y", v.Y);
			e.SetAttributeValue("z", v.Z);
			e.SetAttributeValue("w", v.W);
			return e;
		}

		public static XElement ToXElement(this Vector2i v, string elementName)
		{
			var e = new XElement(elementName);
			e.SetAttributeValue("x", v.X);
			e.SetAttributeValue("y", v.Y);
			return e;
		}

		public static XElement ToXElement(this Vector3i v, string elementName, bool excludeDefaultValues = false)
		{
			var e = new XElement(elementName);
			if (excludeDefaultValues)
			{
				e.SetAttributeValueIND("x", v.X);
				e.SetAttributeValueIND("y", v.Y);
				e.SetAttributeValueIND("z", v.Z);
			}
			else
			{
				e.SetAttributeValue("x", v.X);
				e.SetAttributeValue("y", v.Y);
				e.SetAttributeValue("z", v.Z);
			}
			return e;
		}

		public static XElement ToXElement(this Vector4i v, string elementName)
		{
			var e = new XElement(elementName);
			e.SetAttributeValue("x", v.X);
			e.SetAttributeValue("y", v.Y);
			e.SetAttributeValue("z", v.Z);
			e.SetAttributeValue("w", v.W);
			return e;
		}

		public static OverrideXml AddOverrides(OverrideXml o)
		{
			return
				o
					.Override<Vector2>()
					.Member(nameof(Vector2.X))
					.XmlAttribute("x")
					.Member(nameof(Vector2.Y))
					.XmlAttribute("y")

					.Override<Vector2d>()
					.Member(nameof(Vector2.X))
					.XmlAttribute("x")
					.Member(nameof(Vector2.Y))
					.XmlAttribute("y")

					.Override<Vector2i>()
					.Member(nameof(Vector2i.X))
					.XmlAttribute("x")
					.Member(nameof(Vector2i.Y))
					.XmlAttribute("y")


					.Override<Vector3>()
					.Member(nameof(Vector3.X))
					.XmlAttribute("x")
					.Member(nameof(Vector3.Y))
					.XmlAttribute("y")
					.Member(nameof(Vector3.Z))
					.XmlAttribute("z")

					.Override<Vector3d>()
					.Member(nameof(Vector3.X))
					.XmlAttribute("x")
					.Member(nameof(Vector3.Y))
					.XmlAttribute("y")
					.Member(nameof(Vector3.Z))
					.XmlAttribute("z")

					.Override<Vector3i>()
					.Member(nameof(Vector3i.X))
					.XmlAttribute("x")
					.Member(nameof(Vector3i.Y))
					.XmlAttribute("y")
					.Member(nameof(Vector3i.Z))
					.XmlAttribute("z")

					.Override<Vector4>()
					.Member(nameof(Vector4.X))
					.XmlAttribute("x")
					.Member(nameof(Vector4.Y))
					.XmlAttribute("y")
					.Member(nameof(Vector4.Z))
					.XmlAttribute("z")
					.Member(nameof(Vector4.W))
					.XmlAttribute("w")

					.Override<Vector4d>()
					.Member(nameof(Vector4.X))
					.XmlAttribute("x")
					.Member(nameof(Vector4.Y))
					.XmlAttribute("y")
					.Member(nameof(Vector4.Z))
					.XmlAttribute("z")
					.Member(nameof(Vector4.W))
					.XmlAttribute("w")

					.Override<Vector4i>()
					.Member(nameof(Vector4i.X))
					.XmlAttribute("x")
					.Member(nameof(Vector4i.Y))
					.XmlAttribute("y")
					.Member(nameof(Vector4i.Z))
					.XmlAttribute("z")
					.Member(nameof(Vector4i.W))
					.XmlAttribute("w");
		}
	}
}
