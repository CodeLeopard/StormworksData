﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;

using OpenToolkit.Mathematics;

namespace Shared.Serialization.DataTypes
{
	public static class ColorSer
	{
		public static Color4 ParseRGBA(XElement vectorElement, bool allowNull = false)
		{
			if (null == vectorElement)
			{
				if (allowNull) return default;
				throw new ArgumentNullException(nameof(vectorElement));
			}

			var attributes = vectorElement.Attributes().XAttributeToDictionary();
			attributes.TryGetFloat("r", out float r);
			attributes.TryGetFloat("g", out float g);
			attributes.TryGetFloat("b", out float b);
			attributes.TryGetFloat("a", out float a);
			return new Color4(r, g, b, a);
		}
	}
}
