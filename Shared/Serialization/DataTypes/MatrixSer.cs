﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Xml.Linq;

using OpenToolkit.Mathematics;

using Shared.Exceptions;
using Shared.Extensions;

namespace Shared.Serialization.DataTypes
{
	public static class MatrixSer
	{
		private static List<(sbyte x, sbyte y, float v)> ParseAttributes(IEnumerable<XAttribute> attributes, byte expectedColumnCount, byte expectedRowCount = 0, bool strict = true)
		{
			if (expectedRowCount == 0) expectedRowCount = expectedColumnCount;
			var expectedValuesCount = expectedColumnCount * expectedRowCount;
			byte numValuesParsed = 0;

			var result = new List<(sbyte, sbyte, float)>();

			foreach (var attribute in attributes)
			{
				var name = attribute.Name.LocalName.Split('_')[1];
				var value = attribute.Value;

				bool pix = sbyte.TryParse(name[0] + "", out sbyte ix);
				bool piy = sbyte.TryParse(name[1] + "", out sbyte iy);
				bool pv = float.TryParse(value, NumberStyles.Float, CultureInfo.InvariantCulture, out float v);

				if (!pix | !piy)
					throw new XmlDataException($"Parsing failed for transform: index failed to parse for attribute {name} with value {value}.");

				if (!pv)
					throw new XmlDataException($"Parsing failed for transform at index x{ix} y{iy}: input '{value}' did not parse to Float.");

				if (float.IsNaN(v) || float.IsInfinity(v))
					throw new XmlDataException($"Parsing failed for transform at index x{ix} y{iy}: input '{value}' resulted in forbidden value '{v}'.");

				if (ix > expectedColumnCount || iy > expectedColumnCount)
					throw new XmlDataException($"Parsing failed for transform: index x{ix} y{iy}: is out of bounds, expected {expectedColumnCount} columns by {expectedRowCount} rows. Input: '{value}'.");

				result.Add((ix, iy, v));
			}

			// Xml Compression by Stormworks omits values that match Identity.
			if(result.Count != expectedValuesCount && strict)
				throw new XmlDataException($"Parsing transform did not yield expected number of values, expected: {expectedValuesCount} but got: {numValuesParsed}");

			return result;
		}

		public static Matrix4 ParseMatrix4(XElement XElement, bool allowNull = false)
		{
			try
			{
				Matrix4 result = Matrix4.Identity;
				if (XElement == null)
				{
					if(allowNull) return result;

					throw new ArgumentNullException(nameof(XElement));
				}

				var attr = XElement.Attributes().ToArray();
				foreach (var vt in ParseAttributes(attr, 4, 4, strict: false))
				{
					(sbyte x, sbyte y, float v) = vt;

					result[x, y] = v;
				}

				return result;
			}
			catch (Exception e)
			{
				throw new XmlDataException
					(
					 $"Failed to get {nameof(Matrix4)} from {XElement.Name} at {XElement.GetLineInfoString()}"
				   , XElement
				   , e
					);
			}
		}

		public static Matrix3 ParseMatrix3(XElement XElement, bool allowNull = false)
		{
			try
			{
				var result = Matrix3.Identity;
				if (XElement == null)
				{
					if (allowNull) return result;

					throw new ArgumentNullException(nameof(XElement));
				}

				var attr = XElement.Attributes().ToArray();

				foreach (var vt in ParseAttributes(attr, 3, 3, strict: false))
				{
					(sbyte x, sbyte y, float v) = vt;

					result[x, y] = v;
				}

				return result;
			}
			catch (Exception e)
			{
				throw new XmlDataException
					(
					 $"Failed to get {nameof(Matrix3)} from {XElement.Name} at {XElement.GetLineInfoString()}"
				   , XElement
				   , e
					);
			}
		}

		public static Matrix3 ParseMatrix3(XAttribute xAttribute, bool allowNull)
		{
			if (null == xAttribute && allowNull)
			{
				return Matrix3.Identity;
			}

			var strings = xAttribute.Value.Split(',');
			var ints = strings.Select(int.Parse).ToArray();

			var result = new Matrix3();

			int i = 0;

			result[0, 0] = ints[i++];
			result[0, 1] = ints[i++];
			result[0, 2] = ints[i++];
			result[1, 0] = ints[i++];
			result[1, 1] = ints[i++];
			result[1, 2] = ints[i++];
			result[2, 0] = ints[i++];
			result[2, 1] = ints[i++];
			result[2, 2] = ints[i++];

			return result;
		}

		public static void AddAttribute(this XElement element, string key, Matrix3 matrix)
		{
			//"-0,-0,-1,1,-0,-0,-0,-1,-0"
			const int elements = 3;
			const float charsPerElement = 2.5f; // A little larger than needed is not so bad.
			const int initialCapacity = (int) (elements * charsPerElement);
			var sb = new StringBuilder(initialCapacity);

			void Append(float f)
			{
				sb.Append((int)f);
				sb.Append(',');
			}

			Append(matrix[0, 0]);
			Append(matrix[0, 1]);
			Append(matrix[0, 2]);

			Append(matrix[1, 0]);
			Append(matrix[1, 1]);
			Append(matrix[1, 2]);

			Append(matrix[2, 0]);
			Append(matrix[2, 1]);
			// Skip final ','
			sb.Append(matrix[2, 2]);

			element.SetAttributeValue(key, sb.ToString());
		}

		public static XElement ToXElement(this Matrix4 matrix, string elementName = "transform", bool no_compression = false)
		{
			var element = new XElement(elementName);
			for (int x = 0; x < 4; x++)
			for (int y = 0; y < 4; y++)
			{
				if (no_compression || Matrix4.Identity[x, y] != matrix[x, y])
				{
					element.SetAttributeValue($"_{x}{y}", matrix[x, y]);
				}
			}
			return element;
		}

		public static XElement ToXElement(this Matrix3 matrix, string elementName = "transform", bool no_compression = false)
		{
			var element = new XElement(elementName);
			for (int x = 0; x < 3; x++)
			for (int y = 0; y < 3; y++)
			{
				if (no_compression || Matrix4.Identity[x, y] != matrix[x, y])
				{
					element.SetAttributeValue($"_{x}{y}", matrix[x, y]);
				}
			}
			return element;
		}

		public static OverrideXml AddOverrides(OverrideXml o)
		{
			return
				o
					// ##############################################
					.Override<Matrix3>()
					.Member(nameof(Matrix3.Column0))
					.XmlIgnore()
					.Member(nameof(Matrix3.Column1))
					.XmlIgnore()
					.Member(nameof(Matrix3.Column2))
					.XmlIgnore()

					.Member(nameof(Matrix3.Row0))
					.XmlIgnore()
					.Member(nameof(Matrix3.Row1))
					.XmlIgnore()
					.Member(nameof(Matrix3.Row2))
					.XmlIgnore()

					.Member(nameof(Matrix3.Diagonal))
					.XmlIgnore()

					.Member(nameof(Matrix3.M11))
					.XmlAttribute("_00")
					.Member(nameof(Matrix3.M12))
					.XmlAttribute("_01")
					.Member(nameof(Matrix3.M13))
					.XmlAttribute("_02")

					.Member(nameof(Matrix3.M21))
					.XmlAttribute("_10")
					.Member(nameof(Matrix3.M22))
					.XmlAttribute("_11")
					.Member(nameof(Matrix3.M23))
					.XmlAttribute("_12")

					.Member(nameof(Matrix3.M31))
					.XmlAttribute("_20")
					.Member(nameof(Matrix3.M32))
					.XmlAttribute("_21")
					.Member(nameof(Matrix3.M33))
					.XmlAttribute("_22")

					// ##############################################
					.Override<Matrix4>()

					.Member(nameof(Matrix4.Column0))
					.XmlIgnore()
					.Member(nameof(Matrix4.Column1))
					.XmlIgnore()
					.Member(nameof(Matrix4.Column2))
					.XmlIgnore()
					.Member(nameof(Matrix4.Column3))
					.XmlIgnore()

					.Member(nameof(Matrix4.Row0))
					.XmlIgnore()
					.Member(nameof(Matrix4.Row1))
					.XmlIgnore()
					.Member(nameof(Matrix4.Row2))
					.XmlIgnore()
					.Member(nameof(Matrix4.Row3))
					.XmlIgnore()

					.Member(nameof(Matrix4.Diagonal))
					.XmlIgnore()

					.Member(nameof(Matrix4.M11))
					.XmlAttribute("_00")
					.Member(nameof(Matrix4.M12))
					.XmlAttribute("_01")
					.Member(nameof(Matrix4.M13))
					.XmlAttribute("_02")
					.Member(nameof(Matrix4.M14))
					.XmlAttribute("_03")

					.Member(nameof(Matrix4.M21))
					.XmlAttribute("_10")
					.Member(nameof(Matrix4.M22))
					.XmlAttribute("_11")
					.Member(nameof(Matrix4.M23))
					.XmlAttribute("_12")
					.Member(nameof(Matrix4.M24))
					.XmlAttribute("_13")

					.Member(nameof(Matrix4.M31))
					.XmlAttribute("_20")
					.Member(nameof(Matrix4.M32))
					.XmlAttribute("_21")
					.Member(nameof(Matrix4.M33))
					.XmlAttribute("_22")
					.Member(nameof(Matrix4.M34))
					.XmlAttribute("_23")

					.Member(nameof(Matrix4.M41))
					.XmlAttribute("_30")
					.Member(nameof(Matrix4.M42))
					.XmlAttribute("_31")
					.Member(nameof(Matrix4.M43))
					.XmlAttribute("_32")
					.Member(nameof(Matrix4.M44))
					.XmlAttribute("_33");
		}
	}
}
