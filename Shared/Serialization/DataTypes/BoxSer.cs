﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Text;

using OpenToolkit.Mathematics;

namespace Shared.Serialization.DataTypes
{
	public static class BoxSer
	{
		public static OverrideXml AddOverrides(OverrideXml o)
		{
			return
				o
				   .Override<Box2>()
				   .Member(nameof(Box2.Min))
				   .XmlElement("min")
				   .Member(nameof(Box2.Max))
				   .XmlElement("max")
				   .Member(nameof(Box2.Size))
				   .XmlIgnore()

				   .Override<Box2i>()
				   .Member(nameof(Box2i.Min))
				   .XmlElement("min")
				   .Member(nameof(Box2i.Max))
				   .XmlElement("max")
				   .Member(nameof(Box2i.Size))
				   .XmlIgnore()

				   .Override<Box2d>()
				   .Member(nameof(Box2d.Min))
				   .XmlElement("min")
				   .Member(nameof(Box2d.Max))
				   .XmlElement("max")
				   .Member(nameof(Box2d.Size))
				   .XmlIgnore()


				   .Override<Box3>()
				   .Member(nameof(Box3.Min))
				   .XmlElement("min")
				   .Member(nameof(Box3.Max))
				   .XmlElement("max")
				   .Member(nameof(Box3.Size))
				   .XmlIgnore()

				   .Override<Box3i>()
				   .Member(nameof(Box3i.Min))
				   .XmlElement("min")
				   .Member(nameof(Box3i.Max))
				   .XmlElement("max")
				   .Member(nameof(Box3i.Size))
				   .XmlIgnore()

				   .Override<Box3d>()
				   .Member(nameof(Box3d.Min))
				   .XmlElement("min")
				   .Member(nameof(Box3d.Max))
				   .XmlElement("max")
				   .Member(nameof(Box3d.Size))
				   .XmlIgnore()



				   .Override<Bounds3>()
				   .Member(nameof(Bounds3.Min))
				   .XmlElement("min")
				   .Member(nameof(Bounds3.Max))
				   .XmlElement("max")
				   .Member(nameof(Bounds3.Size))
				   .XmlIgnore()

				   .Override<Bounds3i>()
				   .Member(nameof(Bounds3i.Min))
				   .XmlElement("min")
				   .Member(nameof(Bounds3i.Max))
				   .XmlElement("max")
				   .Member(nameof(Bounds3i.Size))
				   .XmlIgnore();
		}
	}
}
