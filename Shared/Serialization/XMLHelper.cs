﻿// Copyright 2022-2025 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;

using Shared.Exceptions;

namespace Shared.Serialization
{
	public static class XMLHelper
	{
		private const RegexOptions regexOptions = RegexOptions.Compiled | RegexOptions.Multiline;

		private static readonly Regex fromDialect_TransformComponents = new Regex("(\\s)(\\d+=\")", regexOptions);

		public static string FromStormworksDialectXml(string invalidXML)
		{
			invalidXML = invalidXML.TrimStart('\r', '\n', '\t', ' ');

			string _byteOrderMarkUtf8 = Encoding.UTF8.GetString(Encoding.UTF8.GetPreamble());
			if (_byteOrderMarkUtf8.Length > 0)
			{

				bool hasBOM = invalidXML.StartsWith(_byteOrderMarkUtf8, StringComparison.Ordinal);
				if (hasBOM)
				{
					invalidXML = invalidXML.Remove(0, _byteOrderMarkUtf8.Length);
				}
			}

			bool missingInitialChar = !invalidXML.StartsWith("<");
			if (missingInitialChar)
			{
				invalidXML = "<" + invalidXML;
			}

			var clean = fromDialect_TransformComponents.Replace(invalidXML, "$1_$2");

			return clean;
		}

		private static readonly Regex transformComponents = new Regex("(\\s)_(\\d+=\")", regexOptions);
		private static readonly Regex spaceToTab = new Regex("  ", regexOptions);
		private static readonly Regex endOfTagNoWhiteSpace = new Regex(" />", regexOptions);
		private static readonly Regex newlineTransform = new Regex("\r\n", regexOptions);

		public static string ToStormworksDialectXml(string compliantXml, bool newLineStyleWindows = true)
		{
			// Change xml to Stormworks' dialect.
			var nonCompliantXML = transformComponents.Replace(compliantXml, "$1$2");
			nonCompliantXML = spaceToTab.Replace(nonCompliantXML, "\t");
			nonCompliantXML = endOfTagNoWhiteSpace.Replace(nonCompliantXML, "/>");

			if (newLineStyleWindows)
			{
				nonCompliantXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n" + nonCompliantXML + "\r\n\r\n";
				var dialectXml = nonCompliantXML;
				return dialectXml;
			}
			else
			{
				nonCompliantXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + nonCompliantXML + "\n\n";
				var dialectXml = newlineTransform.Replace(nonCompliantXML, "\n");
				return dialectXml;
			}
		}

		/// <summary>
		/// Load a Storworks dialect XDocument from the stream.
		/// </summary>
		/// <param name="stream">Stream to read from</param>
		/// <returns></returns>
		public static XDocument LoadFromStream(Stream stream)
		{
			if(null == stream) throw new ArgumentNullException(nameof(stream));

			var reader = new StreamReader(stream); // no using, because we don't own the stream, and the reader would close it.
			var dialectXml = reader.ReadToEnd();

			var clean = FromStormworksDialectXml(dialectXml);

			XDocument doc;
			try
			{
				doc = XDocument.Parse(clean, LoadOptions.SetLineInfo);
				clean = null;

				return doc;
			}
			catch (Exception e)
			{
				throw new DialectXmlLoadException(dialectXml, clean, e);
			}
		}

		public static XDocument LoadFromFile(string filePath)
		{
			if(String.IsNullOrWhiteSpace(filePath)) throw new ArgumentNullException(nameof(filePath));

			try
			{
				using (var file = File.OpenRead(filePath))
				{
					return LoadFromStream(file);
				}
			}
			catch (Exception e)
			{
				throw new FileInteractionException(filePath, e);
			}
		}

		public static void SaveToStream(Stream stream, XDocument document)
		{
			if (null == stream) throw new ArgumentNullException(nameof(stream));
			if (null == document) throw new ArgumentNullException(nameof(document));

			var dialectXml = ToStormworksDialectXml(document.ToString());

			var writer = new StreamWriter(stream); // no using, because we don't own the stream, and the writer would close it.
			writer.Write(dialectXml);
			writer.Flush();
		}

		/// <summary>
		/// Save an <see cref="XDocument"/> <paramref name="document"/> to <paramref name="filePath"/>
		/// </summary>
		/// <param name="document"></param>
		/// <param name="filePath"></param>
		public static void SaveToFile(XDocument document, string filePath)
		{
			if (String.IsNullOrWhiteSpace(filePath)) throw new ArgumentNullException(nameof(filePath));

			try
			{
				using (var file = File.Open(filePath, FileMode.Create)) // Create mode will truncate existing file
				{
					SaveToStream(file, document);
				}
			}
			catch (Exception e)
			{
				throw new FileInteractionException(filePath, e);
			}
		}

		/// <summary>
		/// Create a <see cref="Dictionary{String, String}"/> from an <see cref="IEnumerable{XAttribute}"/>
		/// </summary>
		/// <param name="enumerable"></param>
		/// <returns></returns>
		public static Dictionary<string, string> XAttributeToDictionary(this IEnumerable<XAttribute> enumerable)
		{
			var result = new Dictionary<string, string>();

			foreach (XAttribute xAttribute in enumerable)
			{
				result.Add(xAttribute.Name.LocalName, xAttribute.Value);
			}

			return result;
		}

		#region TryGet

		public static bool TryGetFloat(this Dictionary<string, string> instance, string key, out float value, NumberStyles style = NumberStyles.Any)
		{
			value = default;
			if (instance.TryGetValue(key, out string result))
			{
				if (float.TryParse(result, style, CultureInfo.InvariantCulture, out value))
				{
					return true;
				}
			}

			return false;
		}

		public static bool TryGetDouble(this Dictionary<string, string> instance, string key, out double value, NumberStyles style = NumberStyles.Any)
		{
			value = default;
			if (instance.TryGetValue(key, out string result))
			{
				if (double.TryParse(result, style, CultureInfo.InvariantCulture, out value))
				{
					return true;
				}
			}

			return false;
		}

		public static bool TryGetByte(this Dictionary<string, string> instance, string key, out byte value, NumberStyles style = NumberStyles.Any)
		{
			value = default;
			if (instance.TryGetValue(key, out string result))
			{
				if (byte.TryParse(result, style, CultureInfo.InvariantCulture, out value))
				{
					return true;
				}
			}

			return false;
		}

		/*
		public static bool TryGetByteEnum<TEnum>(this Dictionary<string, string> instance, string key, out TEnum result, NumberStyles style = NumberStyles.Any) where TEnum : System.Enum
		{
			result = default;
			byte value = default;
			if (instance.TryGetValue(key, out string strValue))
			{
				if (! byte.TryParse(strValue, style, CultureInfo.InvariantCulture, out value))
				{
					return false;
				}

				result = (TEnum)value;
			}

			return false;
		}*/

		public static bool TryGetInt32(this Dictionary<string, string> instance, string key, out Int32 value, NumberStyles style = NumberStyles.Any)
		{
			value = default;
			if (instance.TryGetValue(key, out string result))
			{
				if (Int32.TryParse(result, style, CultureInfo.InvariantCulture, out value))
				{
					return true;
				}
			}

			return false;
		}

		public static bool TryGetUInt32(this Dictionary<string, string> instance, string key, out UInt32 value, NumberStyles style = NumberStyles.Any)
		{
			value = default;
			if (instance.TryGetValue(key, out string result))
			{
				if (UInt32.TryParse(result, style, CultureInfo.InvariantCulture, out value))
				{
					return true;
				}
				else if(result == "")
				{
					// todo: have this case in all variants.
					value = 0;
					return true;
				}
			}

			return false;
		}

		public static bool TryGetBool(this Dictionary<string, string> instance, string key, out bool value)
		{
			value = default;
			if (instance.TryGetValue(key, out string result))
			{
				if (bool.TryParse(result, out value))
				{
					return true;
				}
			}

			return false;
		}

		#endregion TryGet

		#region GetOrDefault
		public static float GetFloat(this Dictionary<string, string> instance, string key, float defaultValue = default, NumberStyles style = NumberStyles.Any)
		{
			if(TryGetFloat(instance, key, out float result, style))
			{
				return result;
			}

			return defaultValue;
		}

		public static double GetDouble(this Dictionary<string, string> instance, string key, double defaultValue = default, NumberStyles style = NumberStyles.Any)
		{
			if (TryGetDouble(instance, key, out double result, style))
			{
				return result;
			}

			return defaultValue;
		}

		public static byte GetByte(this Dictionary<string, string> instance, string key, byte defaultValue = default, NumberStyles style = NumberStyles.Any)
		{
			if (TryGetByte(instance, key, out byte result, style))
			{
				return result;
			}

			return defaultValue;
		}

		/*
		public static bool TryGetByteEnum<TEnum>(this Dictionary<string, string> instance, string key, out TEnum result, NumberStyles style = NumberStyles.Any) where TEnum : System.Enum
		{
			result = default;
			byte value = default;
			if (instance.TryGetValue(key, out string strValue))
			{
				if (! byte.TryParse(strValue, style, CultureInfo.InvariantCulture, out value))
				{
					return false;
				}

				result = (TEnum)value;
			}

			return false;
		}*/

		public static int GetInt32(this Dictionary<string, string> instance, string key, Int32 defaultValue = default, NumberStyles style = NumberStyles.Any)
		{
			if (TryGetInt32(instance, key, out Int32 result, style))
			{
				return result;
			}

			return defaultValue;
		}

		public static uint GetUInt32(this Dictionary<string, string> instance, string key, UInt32 defaultValue = default, NumberStyles style = NumberStyles.Any)
		{
			if (TryGetUInt32(instance, key, out UInt32 result, style))
			{
				return result;
			}

			return defaultValue;
		}

		public static bool GetBool(this Dictionary<string, string> instance, string key, bool defaultValue = default)
		{
			if (TryGetBool(instance, key, out bool result))
			{
				return result;
			}

			return defaultValue;
		}

		public static bool? GetNullableBool(this Dictionary<string, string> instance, string key, bool? defaultValue = default)
		{
			if (TryGetBool(instance, key, out bool result))
			{
				return result;
			}

			return defaultValue;
		}

		public static float? GetNullableFloat(this Dictionary<string, string> instance, string key, float? defaultValue = default)
		{
			if (TryGetFloat(instance, key, out float result))
			{
				return result;
			}

			return defaultValue;
		}

		public static string GetString(this Dictionary<string, string> instance, string key, string defaultValue = "")
		{
			if (instance.TryGetValue(key, out string result))
			{
				return result;
			}

			return defaultValue;
		}

		#endregion GetOrDefault


		#region Set If Not Default

		/// <summary>
		/// Set Attribute Value If Not Default<br/>
		/// Set the value of the attribute at <paramref name="key"/> to value
		/// only if it's not <paramref name="defaultValue"/>, in that case do nothing.
		/// </summary>
		/// <param name="instance">The <see cref="XElement"/> to set the attribute on.</param>
		/// <param name="key">The <see cref="XName"/> of the attribute.</param>
		/// <param name="value">The value to be set.</param>
		/// <param name="defaultValue">The default value that will cause <paramref name="value"/> to not be set.</param>
		public static void SetAttributeValueIND<T>(this XElement instance, XName key, T value, T defaultValue = default)
		where T : IEquatable<T>
		{
			if (EqualityComparer<T>.Default.Equals(value, defaultValue)) return;
			instance.SetAttributeValue(key, value);
		}

		/// <summary>
		/// Set Attribute Value If Not Default Formatted<br/>
		/// Set the value of the attribute at <paramref name="key"/> to value
		/// only if it's not <paramref name="defaultValue"/>, in that case do nothing.
		/// The value is formatted according to <paramref name="format"/> after it's compared.
		/// </summary>
		/// <param name="instance">The <see cref="XElement"/> to set the attribute on.</param>
		/// <param name="key">The <see cref="XName"/> of the attribute.</param>
		/// <param name="format">The format to pass to <paramref name="value"/>'s ToString method.</param>
		/// <param name="value">The value to be set.</param>
		/// <param name="defaultValue">The default value that will cause <paramref name="value"/> to not be set.</param>
		public static void SetAttributeValueIND_Formatted<T>(this XElement instance, XName key, string format, T value, T defaultValue = default)
			where T : IEquatable<T>, IFormattable
		{
			if (EqualityComparer<T>.Default.Equals(value, defaultValue)) return;
			instance.SetAttributeValue(key, value.ToString(format, CultureInfo.InvariantCulture));
		}

		/// <summary>
		/// Set Attribute Value If Not Default Hexadecimal<br/>
		/// Set the value of the attribute at <paramref name="key"/> to value
		/// only if it's not <paramref name="defaultValue"/>, in that case do nothing.
		/// </summary>
		/// <param name="instance">The <see cref="XElement"/> to set the attribute on.</param>
		/// <param name="key">The <see cref="XName"/> of the attribute.</param>
		/// <param name="value">The value to be set.</param>
		/// <param name="defaultValue">The default value that will cause <paramref name="value"/> to not be set.</param>
		public static void SetAttributeValueIND_Hex<T>(this XElement instance, XName key, T value, T defaultValue = default)
			where T : IEquatable<T>, IFormattable
		{
			instance.SetAttributeValueIND_Formatted(key, "X", value, defaultValue);
		}

		#endregion Set If Not Default
	}
}
