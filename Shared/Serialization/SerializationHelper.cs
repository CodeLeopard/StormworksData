﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

using Shared.Exceptions;
using Shared.Serialization.DataTypes;

namespace Shared.Serialization
{
	public static class SerializationHelper
	{
		private static readonly Dictionary<Type, XmlSerializer> serializers = new Dictionary<Type, XmlSerializer>();
		private static readonly XmlSerializerNamespaces namespaces = new XmlSerializerNamespaces();
		private static readonly XmlReaderSettings readerSettings = new XmlReaderSettings();
		private static readonly XmlWriterSettings pureWriterSettings = new XmlWriterSettings();
		private static readonly XmlWriterSettings dialectWriterSettings = new XmlWriterSettings();
		private static readonly XmlWriterSettings dataContractWriterSettings = new XmlWriterSettings();

		static SerializationHelper()
		{
			namespaces.Add("", "");

			readerSettings.IgnoreComments = false;
			readerSettings.IgnoreWhitespace = false;
			readerSettings.CloseInput = false;


			pureWriterSettings.OmitXmlDeclaration = false;
			pureWriterSettings.Indent = true;
			pureWriterSettings.IndentChars = "\t";
			pureWriterSettings.NewLineHandling = NewLineHandling.None;
			pureWriterSettings.CloseOutput = false;


			dialectWriterSettings.OmitXmlDeclaration = true;
			dialectWriterSettings.Indent = true;
			dialectWriterSettings.IndentChars = "\t";
			dialectWriterSettings.NewLineHandling = NewLineHandling.None;
			dialectWriterSettings.CloseOutput = false;

			dataContractWriterSettings.OmitXmlDeclaration = false;
			dataContractWriterSettings.Indent = false;
			dataContractWriterSettings.IndentChars = "\t";
			dataContractWriterSettings.NewLineHandling = NewLineHandling.None;
			dataContractWriterSettings.CloseOutput = false;
		}


		/// <summary>
		/// Get a serializer for <paramref name="type"/> with overrides already applied.
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		public static XmlSerializer GetSerializer(Type type)
		{
			if (serializers.TryGetValue(type, out var result))
			{
				return result;
			}
			else
			{
				var overrides = new OverrideXml();

				// todo: use attribute to find the AddOverrides function automagically.
				DataTypes.VectorSer.AddOverrides(overrides);
				DataTypes.MatrixSer.AddOverrides(overrides);
				DataTypes.BoxSer.AddOverrides(overrides);

				var serializer = new XmlSerializer(type, overrides.Commit());

				serializers[type] = serializer;

				return serializer;
			}
		}

		/// <summary>
		/// Serialize using <see cref="XmlSerializer"/>.
		/// </summary>
		public static class Pure
		{
			#region Load

			/// <summary>
			/// Read a <typeparamref name="TSerialized"/> from <paramref name="filePath"/> using <see cref="XmlSerializer"/>
			/// </summary>
			/// <typeparam name="TSerialized"></typeparam>
			/// <param name="filePath"></param>
			/// <returns></returns>
			public static TSerialized LoadFromFile<TSerialized>(string filePath)
			{
				try
				{
					using (var fileStream = File.OpenRead(filePath))
					{
						return LoadFromStream<TSerialized>(fileStream, filePath);
					}
				}
				catch (Exception e)
				{
					throw new FileInteractionException(filePath, e);
				}
			}

			/// <summary>
			/// Read a <typeparamref name="TSerialized"/> from <param name="stream"></param> optionally provide the original file path in <paramref name="filePath"/> to populate exception info using <see cref="XmlSerializer"/>
			/// </summary>
			/// <typeparam name="TSerialized"></typeparam>
			/// <param name="stream"></param>
			/// <param name="filePath"></param>
			/// <returns></returns>
			public static TSerialized LoadFromStream<TSerialized>(Stream stream, string filePath = null)
			{
				if (stream == null) throw new ArgumentNullException(nameof(stream));

				var serializer = GetSerializer(typeof(TSerialized));

				using (var xmlReader = XmlReader.Create(stream, readerSettings))
				{
					var result = serializer.Deserialize<TSerialized>(xmlReader);

					if (result is ISerializationEventReceiver eventSubscriber)
					{
						eventSubscriber.AfterDeserialize(filePath);
					}

					return result;
				}
			}

			#endregion Load

			#region Save

			/// <summary>
			/// Write a <typeparamref name="TSerialized"/> to <paramref name="filePath"/> using <see cref="XmlSerializer"/>
			/// </summary>
			/// <typeparam name="TSerialized"></typeparam>
			/// <param name="filePath"></param>
			/// <param name="data">The object to be serialized</param>
			/// <returns></returns>
			public static void SaveToFile<TSerialized>(TSerialized data, string filePath)
			{
				try
				{
					using (var file = File.Open(filePath, FileMode.Create))
					{
						SaveToStream<TSerialized>(file, data);
					}
				}
				catch (Exception e)
				{
					throw new FileInteractionException(filePath, e);
				}
			}


			/// <summary>
			/// Write a <typeparamref name="TSerialized"/> to <paramref name="outputStream"/> using <see cref="XmlSerializer"/>
			/// </summary>
			/// <typeparam name="TSerialized"></typeparam>
			/// <param name="outputStream">The stream to write to</param>
			/// <param name="data">The object to be serialized</param>
			/// <returns></returns>
			public static void SaveToStream<TSerialized>(Stream outputStream, TSerialized data)
			{
				if (data is ISerializationEventReceiver eventSubscriber)
				{
					eventSubscriber.BeforeSerialize();
				}

				var serializer = GetSerializer(typeof(TSerialized));
				using (var writer = XmlWriter.Create(outputStream, pureWriterSettings))
				{
					serializer.Serialize(writer, data, namespaces);
				}
			}

			#endregion Save
		}

		/// <summary>
		/// Serialize using <see cref="XmlSerializer"/> with Stormworks Dialect Transformation.
		/// </summary>
		public static class Dialect
		{
			#region Load

			/// <summary>
			/// Read a <typeparamref name="TSerialized"/> from <paramref name="filePath"/> using <see cref="XmlSerializer"/>
			/// </summary>
			/// <typeparam name="TSerialized"></typeparam>
			/// <param name="filePath"></param>
			/// <returns></returns>
			public static TSerialized LoadFromFile<TSerialized>(string filePath)
			{
				try
				{
					using (var fileStream = File.OpenRead(filePath))
					{
						return LoadFromStream<TSerialized>(fileStream, filePath);
					}
				}
				catch (Exception e)
				{
					throw new FileInteractionException(filePath, e);
				}
			}

			/// <summary>
			/// Read a <typeparamref name="TSerialized"/> from <param name="inputStream"></param> optionally provide the original file path in <paramref name="filePath"/> to populate exception info using <see cref="XmlSerializer"/>
			/// </summary>
			/// <typeparam name="TSerialized"></typeparam>
			/// <param name="inputStream"></param>
			/// <param name="filePath"></param>
			/// <returns></returns>
			public static TSerialized LoadFromStream<TSerialized>(Stream inputStream, string filePath = null)
			{
				if (inputStream == null) throw new ArgumentNullException(nameof(inputStream));

				string dialectXml = null;
				string compliantXml = null;
				try
				{
					var inputReader = new StreamReader(inputStream);
					dialectXml = inputReader.ReadToEnd();
					compliantXml = XMLHelper.FromStormworksDialectXml(dialectXml);

					using (var compliantUnderlyingStream = new MemoryStream())
					using (var compliantStreamWriter = new StreamWriter(compliantUnderlyingStream))
					{
						compliantStreamWriter.Write(compliantXml);
						compliantStreamWriter.Flush();
						compliantUnderlyingStream.Seek(0, SeekOrigin.Begin);

						return Pure.LoadFromStream<TSerialized>(compliantUnderlyingStream, filePath);
					}
				}
				catch (Exception e)
				{
					throw new DialectXmlLoadException(dialectXml, compliantXml, e);
				}
			}

			#endregion Load

			#region Save

			/// <summary>
			/// Write a <typeparamref name="TSerialized"/> to <paramref name="filePath"/> using <see cref="XmlSerializer"/>
			/// </summary>
			/// <typeparam name="TSerialized"></typeparam>
			/// <param name="filePath"></param>
			/// <param name="data">The object to be serialized</param>
			/// <returns></returns>
			public static void SaveToFile<TSerialized>(TSerialized data, string filePath)
			{
				try
				{
					using (var stream = File.Open(filePath, FileMode.Create))
					{
						SaveToStream<TSerialized>(stream, data);
					}
				}
				catch (Exception e)
				{
					throw new FileInteractionException(filePath, e);
				}
			}


			/// <summary>
			/// Write a <typeparamref name="TSerialized"/> to <paramref name="outputStream"/> using <see cref="XmlSerializer"/>
			/// </summary>
			/// <typeparam name="TSerialized"></typeparam>
			/// <param name="outputStream">The stream to write to</param>
			/// <param name="data">The object to be serialized</param>
			/// <returns></returns>
			public static void SaveToStream<TSerialized>(Stream outputStream, TSerialized data)
			{
				using (var compliantStream = new MemoryStream())
				{
					using (var writer = XmlWriter.Create(compliantStream, dialectWriterSettings))
					{
						var serializer = GetSerializer(typeof(TSerialized));
						serializer.Serialize(writer, data, namespaces);
					}

					compliantStream.Position = 0;

					using (var compliantReader = new StreamReader(compliantStream))
					{
						var dialectWriter = new StreamWriter
							(
							 outputStream
							); // No using: we don't own the underlying outputStream, which would be closed.

						var dialectText = XMLHelper.ToStormworksDialectXml(compliantReader.ReadToEnd());

						dialectWriter.Write(dialectText);
						dialectWriter.Flush();
					}
				}
			}

			#endregion Save
		}

		/// <summary>
		/// Serialize using <see cref="DataContractSerializer"/>.
		/// </summary>
		public static class DContract
		{
			private static readonly Dictionary<Type, DataContractSerializer> serializers =
				new Dictionary<Type, DataContractSerializer>();

			private static readonly Dictionary<Type, HashSet<Type>> knownTypesMap =
				new Dictionary<Type, HashSet<Type>>();

			public static void AddKnownTypes(Type serializerType, IEnumerable<Type> knownTypes)
			{
				if (! knownTypesMap.TryGetValue(serializerType, out var set))
				{
					set = new HashSet<Type>();
					knownTypesMap[serializerType] = set;
				}

				foreach (Type type in knownTypes)
				{
					set.Add(type);
				}
			}

			/// <summary>
			/// Get a serializer for <paramref name="type"/> with overrides already applied.
			/// </summary>
			/// <param name="type"></param>
			/// <returns></returns>
			public static DataContractSerializer GetSerializer(Type type)
			{
				if (serializers.TryGetValue(type, out var result))
				{
					return result;
				}
				else
				{
					var settings = new DataContractSerializerSettings();
					settings.RootNamespace = XmlDictionaryString.Empty;

					if (knownTypesMap.TryGetValue(type, out HashSet<Type> hashSet))
					{
						settings.KnownTypes = hashSet;
					}

					var serializer = new DataContractSerializer(type, settings);

					serializers[type] = serializer;

					return serializer;
				}
			}

			#region Load

			/// <summary>
			/// Read a <typeparamref name="TSerialized"/> from <paramref name="filePath"/> using <see cref="XmlSerializer"/>
			/// </summary>
			/// <typeparam name="TSerialized"></typeparam>
			/// <param name="filePath"></param>
			/// <returns></returns>
			public static TSerialized LoadFromFile<TSerialized>(string filePath)
			{
				try
				{
					using (var fileStream = File.OpenRead(filePath))
					{
						return LoadFromStream<TSerialized>(fileStream, filePath);
					}
				}
				catch (Exception e)
				{
					throw new FileInteractionException(filePath, e);
				}
			}

			/// <summary>
			/// Read a <typeparamref name="TSerialized"/> from <param name="stream"></param> optionally provide the original file path in <paramref name="filePath"/> to populate exception info using <see cref="XmlSerializer"/>
			/// </summary>
			/// <typeparam name="TSerialized"></typeparam>
			/// <param name="stream"></param>
			/// <param name="filePath"></param>
			/// <returns></returns>
			public static TSerialized LoadFromStream<TSerialized>(Stream stream, string filePath = null)
			{
				if (stream == null) throw new ArgumentNullException(nameof(stream));

				var serializer = GetSerializer(typeof(TSerialized));

				using (var xmlReader = XmlReader.Create(stream, readerSettings))
				{
					var result = serializer.Deserialize<TSerialized>(xmlReader);

					if (result is ISerializationEventReceiver eventSubscriber)
					{
						eventSubscriber.AfterDeserialize(filePath);
					}

					return result;
				}
			}

			#endregion Load

			#region Save

			/// <summary>
			/// Write a <typeparamref name="TSerialized"/> to <paramref name="filePath"/> using <see cref="XmlSerializer"/>
			/// </summary>
			/// <typeparam name="TSerialized"></typeparam>
			/// <param name="filePath"></param>
			/// <param name="data">The object to be serialized</param>
			/// <returns></returns>
			public static void SaveToFile<TSerialized>(TSerialized data, string filePath)
			{
				try
				{
					using (var file = File.Open(filePath, FileMode.Create))
					{
						SaveToStream<TSerialized>(file, data);
					}
				}
				catch (Exception e)
				{
					throw new FileInteractionException(filePath, e);
				}
			}


			/// <summary>
			/// Write a <typeparamref name="TSerialized"/> to <paramref name="outputStream"/> using <see cref="XmlSerializer"/>
			/// </summary>
			/// <typeparam name="TSerialized"></typeparam>
			/// <param name="outputStream">The stream to write to</param>
			/// <param name="data">The object to be serialized</param>
			/// <returns></returns>
			public static void SaveToStream<TSerialized>(Stream outputStream, TSerialized data)
			{
				if (data is ISerializationEventReceiver eventSubscriber)
				{
					eventSubscriber.BeforeSerialize();
				}

				var serializer = GetSerializer(typeof(TSerialized));
				using (var writer = XmlWriter.Create(outputStream, dataContractWriterSettings))
				{
					serializer.Serialize(writer, data);
				}
			}

			#endregion Save
		}
	}
}
