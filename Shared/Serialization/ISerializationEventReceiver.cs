﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



namespace Shared.Serialization
{
	public interface ISerializationEventReceiver
	{
		/// <summary>
		/// Called just before serialization, on the root object.
		/// </summary>
		void BeforeSerialize();

		/// <summary>
		/// Called just after deserialization, on the root object.
		/// </summary>
		/// <param name="filePath">The path from where the object was deserialized</param>
		void AfterDeserialize(string filePath);
	}
}
