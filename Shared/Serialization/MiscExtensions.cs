﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System.IO;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Serialization;

namespace Shared.Serialization
{
	public static class MiscExtensions
	{
		#region XmlSerializer
		public static TResult Deserialize<TResult>(this XmlSerializer instance, Stream stream)
		{
			return (TResult) instance.Deserialize(stream);
		}

		public static TResult Deserialize<TResult>(this XmlSerializer instance, TextReader stream)
		{
			return (TResult)instance.Deserialize(stream);
		}

		public static TResult Deserialize<TResult>(this XmlSerializer instance, XmlReader stream)
		{
			return (TResult)instance.Deserialize(stream);
		}
		#endregion XmlSerializer


		#region DataContractSerializer
		public static TResult Deserialize<TResult>(this DataContractSerializer instance, Stream stream)
		{
			return (TResult)instance.ReadObject(stream);
		}

		public static TResult Deserialize<TResult>(this DataContractSerializer instance, XmlReader stream)
		{
			return (TResult)instance.ReadObject(stream);
		}


		public static void Serialize<TData>(this DataContractSerializer instance, Stream stream, TData data)
		{
			instance.WriteObject(stream, data);
		}
		public static void Serialize<TData>(this DataContractSerializer instance, XmlWriter stream, TData data)
		{
			instance.WriteObject(stream, data);
		}
		public static void Serialize<TData>(this DataContractSerializer instance, XmlDictionaryWriter stream, TData data)
		{
			instance.WriteObject(stream, data);
		}

		#endregion DataContractSerializer
	}
}
