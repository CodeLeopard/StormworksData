﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

namespace Shared
{
	/// <summary>
	/// Synchronized <see cref="HashSet{T}"/> implemented using <see cref="ReaderWriterLockSlim"/> with <see cref="LockRecursionPolicy.SupportsRecursion"/>.
	/// </summary>
	/// <typeparam name="TValue"></typeparam>
	public class SynchronizedHashSet<TValue> : ISet<TValue>, IReadOnlyCollection<TValue>, IDisposable
	{
		private readonly ReaderWriterLockSlim Lock;
		private readonly HashSet<TValue> Data;

		/// <summary>
		/// Creates an empty <see cref="SynchronizedHashSet{TValue}"/>.
		/// </summary>
		/// <param name="recursionPolicy">
		/// The <see cref="LockRecursionPolicy"/> of the internal <see cref="ReaderWriterLockSlim"/> that is used to synchronize access.
		/// </param>
		public SynchronizedHashSet(LockRecursionPolicy recursionPolicy = LockRecursionPolicy.NoRecursion)
		{
			Lock = new ReaderWriterLockSlim(recursionPolicy);
			Data = new HashSet<TValue>();
		}

		/// <summary>
		/// Creates a <see cref="SynchronizedHashSet{TValue}"/> containing the elements of <paramref name="collection"/>.
		/// </summary>
		/// <param name="collection">The initial content of the <see cref="SynchronizedHashSet{TValue}"/>.</param>
		/// <param name="recursionPolicy">
		/// The <see cref="LockRecursionPolicy"/> of the internal <see cref="ReaderWriterLockSlim"/> that is used to synchronize access.
		/// </param>
		public SynchronizedHashSet(IEnumerable<TValue> collection, LockRecursionPolicy recursionPolicy = LockRecursionPolicy.NoRecursion)
		{
			Lock = new ReaderWriterLockSlim(recursionPolicy);
			Data = new HashSet<TValue>(collection);
		}


		/// <inheritdoc cref="HashSet{T}.Count" />
		public int Count
		{
			get
			{
				Lock.EnterReadLock();
				try
				{
					return Data.Count;
				}
				finally
				{
					Lock.ExitReadLock();
				}
			}
		}

		/// <inheritdoc />
		public bool IsReadOnly => false;

		/// <inheritdoc />
		public bool Add(TValue item)
		{
			Lock.EnterWriteLock();
			try
			{
				return Data.Add(item);
			}
			finally
			{
				Lock.ExitWriteLock();
			}
		}


		/// <inheritdoc />
		void ICollection<TValue>.Add(TValue item)
		{
			Lock.EnterWriteLock();
			try
			{
				Data.Add(item);
			}
			finally
			{
				Lock.ExitWriteLock();
			}
		}

		/// <inheritdoc />
		public void Clear()
		{
			Lock.EnterWriteLock();
			try
			{
				Data.Clear();
			}
			finally
			{
				Lock.ExitWriteLock();
			}
		}

		/// <inheritdoc />
		public bool Contains(TValue item)
		{
			Lock.EnterReadLock();
			try
			{
				return Data.Contains(item);
			}
			finally
			{
				Lock.ExitReadLock();
			}
		}

		/// <inheritdoc />
		public bool Remove(TValue item)
		{
			Lock.EnterWriteLock();
			try
			{
				return Data.Remove(item);
			}
			finally
			{
				Lock.ExitWriteLock();
			}
		}

		/// <inheritdoc />
		public void CopyTo(TValue[] array, int arrayIndex)
		{
			Lock.EnterReadLock();
			try
			{
				Data.CopyTo(array, arrayIndex);
			}
			finally
			{
				Lock.ExitReadLock();
			}
		}

		/// <inheritdoc />
		public void CopyTo(TValue[] array, int arrayIndex, int count)
		{
			Lock.EnterReadLock();
			try
			{
				Data.CopyTo(array, arrayIndex, count);
			}
			finally
			{
				Lock.ExitReadLock();
			}
		}

		#region  Entire Set Modifications

		/// <inheritdoc />
		public void ExceptWith(IEnumerable<TValue> other)
		{
			Lock.EnterWriteLock();
			try
			{
				Data.ExceptWith(other);
			}
			finally
			{
				Lock.ExitWriteLock();
			}
		}

		/// <inheritdoc />
		public void IntersectWith(IEnumerable<TValue> other)
		{
			Lock.EnterWriteLock();
			try
			{
				Data.IntersectWith(other);
			}
			finally
			{
				Lock.ExitWriteLock();
			}
		}

		/// <inheritdoc />
		public void SymmetricExceptWith(IEnumerable<TValue> other)
		{
			Lock.EnterWriteLock();
			try
			{
				Data.SymmetricExceptWith(other);
			}
			finally
			{
				Lock.ExitWriteLock();
			}
		}

		/// <inheritdoc />
		public void UnionWith(IEnumerable<TValue> other)
		{
			Lock.EnterWriteLock();
			try
			{
				Data.UnionWith(other);
			}
			finally
			{
				Lock.ExitWriteLock();
			}
		}

		#endregion Entire Set Modifications


		#region Entire Set Checks


		/// <inheritdoc />
		public bool IsProperSubsetOf(IEnumerable<TValue> other)
		{
			Lock.EnterReadLock();
			try
			{
				return Data.IsProperSubsetOf(other);
			}
			finally
			{
				Lock.ExitReadLock();
			}
		}

		/// <inheritdoc />
		public bool IsProperSupersetOf(IEnumerable<TValue> other)
		{
			Lock.EnterReadLock();
			try
			{
				return Data.IsProperSupersetOf(other);
			}
			finally
			{
				Lock.ExitReadLock();
			}
		}

		/// <inheritdoc />
		public bool IsSubsetOf(IEnumerable<TValue> other)
		{
			Lock.EnterReadLock();
			try
			{
				return Data.IsSubsetOf(other);
			}
			finally
			{
				Lock.ExitReadLock();
			}
		}

		/// <inheritdoc />
		public bool IsSupersetOf(IEnumerable<TValue> other)
		{
			Lock.EnterReadLock();
			try
			{
				return Data.IsSupersetOf(other);
			}
			finally
			{
				Lock.ExitReadLock();
			}
		}

		/// <inheritdoc />
		public bool Overlaps(IEnumerable<TValue> other)
		{
			Lock.EnterReadLock();
			try
			{
				return Data.Overlaps(other);
			}
			finally
			{
				Lock.ExitReadLock();
			}
		}

		/// <inheritdoc />
		public bool SetEquals(IEnumerable<TValue> other)
		{
			Lock.EnterReadLock();
			try
			{
				return Data.SetEquals(other);
			}
			finally
			{
				Lock.ExitReadLock();
			}
		}

		#endregion Entire Set Checks


		/// <inheritdoc />
		public IEnumerator<TValue> GetEnumerator()
		{
			return Data.GetEnumerator();
		}

		/// <inheritdoc />
		IEnumerator IEnumerable.GetEnumerator()
		{
			return ((IEnumerable)Data).GetEnumerator();
		}


		/// <inheritdoc />
		public void Dispose()
		{
			Lock.Dispose();
		}
	}
}
