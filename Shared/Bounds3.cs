﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/


// This file incorporates work covered by the following copyright.
// The original can be found here: https://github.com/opentk/opentk

//
// Box3.cs
//
// Copyright (C) 2019 OpenTK
//
// This software may be modified and distributed under the terms
// of the MIT license. See the MIT_LICENSE.txt file for details.
//


using System;
using System.Diagnostics.Contracts;
using System.Runtime.InteropServices;

using OpenToolkit.Mathematics;

namespace Shared
{
	/// <summary>
	/// Defines an axis-aligned bounding box.
	/// </summary>
	[StructLayout(LayoutKind.Sequential)]
	public struct Bounds3 : IEquatable<Bounds3>
	{
		private Vector3 _min;

		/// <summary>
		/// Gets or sets the minimum boundary of the structure.
		/// </summary>
		public Vector3 Min
		{
			get => _min;
			set
			{
				if (value.X > _max.X)
				{
					_max.X = value.X;
				}
				if (value.Y > _max.Y)
				{
					_max.Y = value.Y;
				}
				if (value.Z > _max.Z)
				{
					_max.Z = value.Z;
				}

				_min = value;
			}
		}

		private Vector3 _max;

		/// <summary>
		/// Gets or sets the maximum boundary of the structure.
		/// </summary>
		public Vector3 Max
		{
			get => _max;
			set
			{
				if (value.X < _min.X)
				{
					_min.X = value.X;
				}
				if (value.Y < _min.Y)
				{
					_min.Y = value.Y;
				}
				if (value.Z < _min.Z)
				{
					_min.Z = value.Z;
				}

				_max = value;
			}
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="Bounds3"/> struct.
		/// When <paramref name="initEmpty"/> is specified <see cref="Min"/> and <see cref="Max"/> will be set to infinity so
		/// that the <see cref="Inflate"/> operation can be used to populate the bounds.
		/// </summary>
		/// <param name="initEmpty"></param>
		public Bounds3(bool initEmpty)
		{
			if (initEmpty)
			{
				_min = Vector3.PositiveInfinity;
				_max = Vector3.NegativeInfinity;
			}
			else
			{
				_min = Vector3.Zero;
				_max = Vector3.Zero;
			}
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="Bounds3"/> struct.
		/// </summary>
		/// <param name="min">The minimum point on the XY plane this box encloses.</param>
		/// <param name="max">The maximum point on the XY plane this box encloses.</param>
		public Bounds3(Vector3 min, Vector3 max)
		{
			_min = Vector3.ComponentMin(min, max);
			_max = Vector3.ComponentMax(min, max);
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="Bounds3"/> struct.
		/// </summary>
		/// <param name="minX">The minimum X value to be enclosed.</param>
		/// <param name="minY">The minimum Y value to be enclosed.</param>
		/// <param name="minZ">The minimum Z value to be enclosed.</param>
		/// <param name="maxX">The maximum X value to be enclosed.</param>
		/// <param name="maxY">The maximum Y value to be enclosed.</param>
		/// <param name="maxZ">The maximum Z value to be enclosed.</param>
		public Bounds3(float minX, float minY, float minZ, float maxX, float maxY, float maxZ)
			: this(new Vector3(minX, minY, minZ), new Vector3(maxX, maxY, maxZ))
		{
		}

		/// <summary>
		/// Gets or sets a vector describing the size of the Bounds3 structure.
		/// </summary>
		public Vector3 Size
		{
			get => Max - Min;
			set
			{
				Vector3 center = Center;
				_min = center - (value * 0.5f);
				_max = center + (value * 0.5f);
			}
		}

		/// <summary>
		/// Gets or sets a vector describing half the size of the box.
		/// </summary>
		public Vector3 HalfSize
		{
			get => Size / 2;
			set => Size = value * 2;
		}

		/// <summary>
		/// Gets or sets a vector describing the center of the box.
		/// </summary>
		public Vector3 Center
		{
			get => HalfSize + _min;
			set => Translate(value - Center);
		}

		/// <summary>
		/// Returns whether the box contains the specified point (borders inclusive).
		/// </summary>
		/// <param name="point">The point to query.</param>
		/// <returns>Whether this box contains the point.</returns>
		[Pure]
		public bool Contains(Vector3 point)
		{
			return _min.X < point.X && point.X < _max.X &&
				   _min.Y < point.Y && point.Y < _max.Y &&
				   _min.Z < point.Z && point.Z < _max.Z;
		}

		/// <summary>
		/// Returns whether the box contains the specified point (borders inclusive).
		/// </summary>
		/// <param name="point">The point to query.</param>
		/// <param name="boundaryInclusive">
		/// Whether points on the box boundary should be recognised as contained as well.
		/// </param>
		/// <returns>Whether this box contains the point.</returns>
		[Pure]
		public bool Contains(Vector3 point, bool boundaryInclusive)
		{
			if (boundaryInclusive)
			{
				return _min.X <= point.X && point.X <= _max.X &&
					   _min.Y <= point.Y && point.Y <= _max.Y &&
					   _min.Z <= point.Z && point.Z <= _max.Z;
			}
			return _min.X < point.X && point.X < _max.X &&
				   _min.Y < point.Y && point.Y < _max.Y &&
				   _min.Z < point.Z && point.Z < _max.Z;
		}

		/// <summary>
		/// Returns whether the box contains the specified box (borders inclusive).
		/// </summary>
		/// <param name="other">The box to query.</param>
		/// <returns>Whether this box contains the other box.</returns>
		[Pure]
		public bool Contains(Bounds3 other)
		{
			return _max.X >= other._min.X && _min.X <= other._max.X &&
				   _max.Y >= other._min.Y && _min.Y <= other._max.Y &&
				   _max.Z >= other._min.Z && _min.Z <= other._max.Z;
		}

		/// <summary>
		/// Returns the distance between the nearest edge and the specified point.
		/// </summary>
		/// <param name="point">The point to find distance for.</param>
		/// <returns>The distance between the specified point and the nearest edge.</returns>
		[Pure]
		public float DistanceToNearestEdge(Vector3 point)
		{
			var distX = new Vector3(
				Math.Max(0f, Math.Max(_min.X - point.X, point.X - _max.X)),
				Math.Max(0f, Math.Max(_min.Y - point.Y, point.Y - _max.Y)),
				Math.Max(0f, Math.Max(_min.Z - point.Z, point.Z - _max.Z)));
			return distX.Length;
		}

		/// <summary>
		/// Translates this Bounds3 by the given amount.
		/// </summary>
		/// <param name="distance">The distance to translate the box.</param>
		public void Translate(Vector3 distance)
		{
			_min += distance;
			_max += distance;
		}

		/// <summary>
		/// Returns a Bounds3 translated by the given amount.
		/// </summary>
		/// <param name="distance">The distance to translate the box.</param>
		/// <returns>The translated box.</returns>
		[Pure]
		public Bounds3 Translated(Vector3 distance)
		{
			// create a local copy of this box
			Bounds3 box = this;
			box.Translate(distance);
			return box;
		}

		/// <summary>
		/// Scales this Bounds3 by the given amount.
		/// </summary>
		/// <param name="scale">The scale to scale the box.</param>
		/// <param name="anchor">The anchor to scale the box from.</param>
		public void Scale(Vector3 scale, Vector3 anchor)
		{
			_min = anchor + ((_min - anchor) * scale);
			_max = anchor + ((_max - anchor) * scale);
		}

		/// <summary>
		/// Returns a Bounds3 scaled by a given amount from an anchor point.
		/// </summary>
		/// <param name="scale">The scale to scale the box.</param>
		/// <param name="anchor">The anchor to scale the box from.</param>
		/// <returns>The scaled box.</returns>
		[Pure]
		public Bounds3 Scaled(Vector3 scale, Vector3 anchor)
		{
			// create a local copy of this box
			Bounds3 box = this;
			box.Scale(scale, anchor);
			return box;
		}

		/// <summary>
		/// Inflate this Bounds3 to encapsulate a given point.
		/// </summary>
		/// <param name="point">The point to query.</param>
		public void Inflate(Vector3 point)
		{
			_min = Vector3.ComponentMin(_min, point);
			_max = Vector3.ComponentMax(_max, point);
		}

		/// <summary>
		/// Inflate this Bounds3 to encapsulate a given point.
		/// </summary>
		/// <param name="point">The point to query.</param>
		/// <returns>The inflated box.</returns>
		[Pure]
		public Bounds3 Inflated(Vector3 point)
		{
			// create a local copy of this box
			Bounds3 box = this;
			box.Inflate(point);
			return box;
		}

		[Pure]
		public bool IsFinite()
		{
			return VectorExt.IsFinite(_min) && VectorExt.IsFinite(_max);
		}

		/// <summary>
		/// Equality comparator.
		/// </summary>
		/// <param name="left">The left operand.</param>
		/// <param name="right">The right operand.</param>
		public static bool operator ==(Bounds3 left, Bounds3 right)
		{
			return left.Equals(right);
		}

		/// <summary>
		/// Inequality comparator.
		/// </summary>
		/// <param name="left">The left operand.</param>
		/// <param name="right">The right operand.</param>
		public static bool operator !=(Bounds3 left, Bounds3 right)
		{
			return !(left == right);
		}

		/// <inheritdoc/>
		public override bool Equals(object obj)
		{
			return obj is Bounds3 && Equals((Bounds3)obj);
		}

		/// <inheritdoc/>
		public bool Equals(Bounds3 other)
		{
			return _min.Equals(other._min) &&
				   _max.Equals(other._max);
		}

		/// <inheritdoc/>
		public override int GetHashCode()
		{
			return HashCodeBuilder.Combine(_min, _max);
		}

		/// <inheritdoc/>
		public override string ToString()
		{
			return $"{Min} - {Max}";
		}
	}
}
