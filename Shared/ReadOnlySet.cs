﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Shared
{
	/// <summary>
	/// Wrapper around an <see cref="ISet{T}"/> that only allows read operations.
	/// Note that the owner of the underlying <see cref="ISet{T}"/> can still modify it, and those changes will be reflected by this wrapper.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class ReadOnlySet<T> : IReadOnlyCollection<T>, ISet<T>
	{
		private const string notSupportedBecauseReadonly = "Set is a read only set.";
		private readonly ISet<T> _set;

		/// <summary>
		/// Create a new wrapper around <paramref name="set"/>.
		/// </summary>
		/// <param name="set"></param>
		public ReadOnlySet(ISet<T> set)
		{
			_set = set;
		}

		/// <inheritdoc />
		public IEnumerator<T> GetEnumerator()
		{
			return _set.GetEnumerator();
		}

		/// <inheritdoc />
		IEnumerator IEnumerable.GetEnumerator()
		{
			return ((IEnumerable)_set).GetEnumerator();
		}

		/// <summary>
		/// Not supported.
		/// </summary>
		/// <param name="item"></param>
		/// <exception cref="NotSupportedException"></exception>
		void ICollection<T>.Add(T item)
		{
			throw new NotSupportedException(notSupportedBecauseReadonly);
		}


		/// <summary>
		/// Not supported.
		/// </summary>
		/// <param name="other"></param>
		/// <exception cref="NotSupportedException"></exception>
		public void UnionWith(IEnumerable<T> other)
		{
			throw new NotSupportedException(notSupportedBecauseReadonly);
		}


		/// <summary>
		/// Not supported.
		/// </summary>
		/// <param name="other"></param>
		/// <exception cref="NotSupportedException"></exception>
		public void IntersectWith(IEnumerable<T> other)
		{
			throw new NotSupportedException(notSupportedBecauseReadonly);
		}


		/// <summary>
		/// Not supported.
		/// </summary>
		/// <param name="other"></param>
		/// <exception cref="NotSupportedException"></exception>
		public void ExceptWith(IEnumerable<T> other)
		{
			throw new NotSupportedException(notSupportedBecauseReadonly);
		}


		/// <summary>
		/// Not supported.
		/// </summary>
		/// <param name="item"></param>
		/// <exception cref="NotSupportedException"></exception>
		public void SymmetricExceptWith(IEnumerable<T> other)
		{
			throw new NotSupportedException(notSupportedBecauseReadonly);
		}

		/// <inheritdoc />
		public bool IsSubsetOf(IEnumerable<T> other)
		{
			return _set.IsSubsetOf(other);
		}

		/// <inheritdoc />
		public bool IsSupersetOf(IEnumerable<T> other)
		{
			return _set.IsSupersetOf(other);
		}

		/// <inheritdoc />
		public bool IsProperSupersetOf(IEnumerable<T> other)
		{
			return _set.IsProperSupersetOf(other);
		}

		/// <inheritdoc />
		public bool IsProperSubsetOf(IEnumerable<T> other)
		{
			return _set.IsProperSubsetOf(other);
		}

		/// <inheritdoc />
		public bool Overlaps(IEnumerable<T> other)
		{
			return _set.Overlaps(other);
		}

		/// <inheritdoc />
		public bool SetEquals(IEnumerable<T> other)
		{
			return _set.SetEquals(other);
		}


		/// <summary>
		/// Not supported.
		/// </summary>
		/// <param name="item"></param>
		/// <exception cref="NotSupportedException"></exception>
		public bool Add(T item)
		{
			throw new NotSupportedException(notSupportedBecauseReadonly);
		}


		/// <summary>
		/// Not supported.
		/// </summary>
		/// <exception cref="NotSupportedException"></exception>
		public void Clear()
		{
			throw new NotSupportedException(notSupportedBecauseReadonly);
		}

		/// <inheritdoc />
		public bool Contains(T item)
		{
			return _set.Contains(item);
		}

		/// <inheritdoc />
		public void CopyTo(T[] array, int arrayIndex)
		{
			_set.CopyTo(array, arrayIndex);
		}


		/// <summary>
		/// Not supported.
		/// </summary>
		/// <param name="item"></param>
		/// <exception cref="NotSupportedException"></exception>
		public bool Remove(T item)
		{
			throw new NotSupportedException(notSupportedBecauseReadonly);
		}

		/// <summary>
		/// The number of elements in the <see cref="ReadOnlySet{T}"/>.
		/// </summary>
		public int Count => _set.Count;

		/// <inheritdoc />
		public bool IsReadOnly => true;
	}
}
