﻿using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using System.Threading;

namespace Shared
{
	public static class CommandLineProgramProcess
	{
		/// <summary>
		/// Executes a command-line program, specifying a maximum time to wait
		/// for it to complete.
		/// </summary>
		/// <param name="command">
		/// The path to the program executable.
		/// </param>
		/// <param name="workingDirectory">
		/// The path of the working directory for the process executing the program executable.
		/// </param>
		/// <param name="args">
		/// The command-line arguments for the program.
		/// </param>
		/// <param name="timeout">
		/// The maximum time to wait for the subProcess to complete, in milliseconds.
		/// </param>
		/// <returns>
		/// A <see cref="CommandLineProgramProcessResult"/> containing the results of
		/// running the program.
		/// </returns>
		public static CommandLineProgramProcessResult RunProgram(string command, string workingDirectory, string args, int timeout)
		{
			return RunProgram(command, workingDirectory, args, timeout, null);
		}

		/// <summary>
		/// Executes a command-line program, specifying a maximum time to wait
		/// for it to complete.
		/// </summary>
		/// <param name="command">
		/// The path to the program executable.
		/// </param>
		/// <param name="workingDirectory">
		/// The path of the working directory for the process executing the program executable.
		/// </param>
		/// <param name="args">
		/// The command-line arguments for the program.
		/// </param>
		/// <param name="timeout">
		/// The maximum time to wait for the subProcess to complete, in milliseconds.
		/// </param>
		/// <param name="input">
		/// Input to be sent via standard input.
		/// </param>
		/// <returns>
		/// A <see cref="CommandLineProgramProcessResult"/> containing the results of
		/// running the program.
		/// </returns>
		public static CommandLineProgramProcessResult RunProgram(
			string command,
			string workingDirectory,
			string args,
			int timeout,
			string input,
			Encoding inputEncoding = null)
		{
			bool timedOut = false;
			ProcessStartInfo pinfo = new ProcessStartInfo(command);
			pinfo.Arguments = args;
			pinfo.UseShellExecute = false;
			pinfo.CreateNoWindow = true;
			pinfo.WorkingDirectory = workingDirectory;
			pinfo.RedirectStandardInput = true;
			pinfo.RedirectStandardOutput = true;
			pinfo.RedirectStandardError = true;
			Process process = Process.Start(pinfo);
			ProcessStream processStream = new ProcessStream();

			StreamWriter inputStreamWriter;

			if (inputEncoding == null)
			{
				inputStreamWriter = process.StandardInput;
			}
			else
			{
				inputStreamWriter = new StreamWriter(process.StandardInput.BaseStream, inputEncoding);
			}

			Encoding standardInputEncoding = inputStreamWriter.Encoding;

			try
			{
				if (!String.IsNullOrEmpty(input))
					inputStreamWriter.Write(input);

				inputStreamWriter.Close();
				processStream.Read(process);
				process.WaitForExit(timeout);
				processStream.Stop();

				if (!process.HasExited)
				{
					// OK, we waited until the timeout but it still didn't exit; just kill the process now
					timedOut = true;

					try
					{
						process.Kill();
						processStream.Stop();
					}
					catch { }

					process.WaitForExit();
				}
			}
			catch (Exception ex)
			{
				process.Kill();
				processStream.Stop();
				throw ex;
			}
			finally
			{
				processStream.Stop();
			}

			TimeSpan duration = process.ExitTime - process.StartTime;
			float executionTime = (float)duration.TotalSeconds;

			CommandLineProgramProcessResult result = new CommandLineProgramProcessResult(
			 command,
			 workingDirectory,
			 args,
				executionTime,
				processStream.StandardOutput.Trim(),
				processStream.StandardError.Trim(),
				process.ExitCode,
				timedOut,
				standardInputEncoding);

			return result;
		}
	}

	/// <summary>
	/// Represents the result of executing a command-line program.
	/// </summary>
	public class CommandLineProgramProcessResult
	{
		private readonly string _command;
		private readonly string _workigDirectory;
		private readonly string _arguments;
		readonly float _executionTime;
		private readonly Encoding _standardInputEncoding;
		private readonly string _standardOutputString;
		private readonly string _standardErrorString;
		private readonly int _exitCode;
		private readonly bool _hasTimedOut;

		internal CommandLineProgramProcessResult(string command, string workingDirectory, string arguments, float executionTime, string stdout, string stderr, int exitCode, bool timedOut, Encoding standardInputEncoding)
		{
			this._command = command;
			this._workigDirectory = workingDirectory;
			this._arguments = arguments;
			this._executionTime = executionTime;
			this._standardOutputString = stdout;
			this._standardErrorString = stderr;
			this._exitCode = exitCode;
			this._hasTimedOut = timedOut;
			_standardInputEncoding = standardInputEncoding;
		}

		/// <summary>
		/// The command that executed.
		/// </summary>
		public string Command => _command;

		/// <summary>
		/// WorkingDirectory
		/// </summary>
		public string WorkigDirectory => _workigDirectory;

		/// <summary>
		/// The arguments that ware provided.
		/// </summary>
		public string Arguments => _arguments;


		/// <summary>
		/// Gets the total wall time that the subProcess took, in seconds.
		/// </summary>
		public float ExecutionTime => _executionTime;

		/// <summary>
		/// Gets the output that the subProcess wrote to its standard output stream.
		/// </summary>
		public string StandardOutput => _standardOutputString;

		/// <summary>
		/// Gets the output that the subProcess wrote to its standard error stream.
		/// </summary>
		public string StandardError => _standardErrorString;

		public Encoding StandardInputEncoding => _standardInputEncoding;

		/// <summary>
		/// Gets the subProcess's exit code.
		/// </summary>
		public int ExitCode => _exitCode;

		/// <summary>
		/// Gets a flag indicating whether the subProcess was aborted because it
		/// timed out.
		/// </summary>
		public bool HasTimedOut => _hasTimedOut;


		/// <summary>
		/// Throw an Exception when this result indicates that running the program failed, or a non-zero exit code was returned, or the program wrote to the Error Stream.
		/// </summary>
		/// <exception cref="ProgramInvocationFailedException"></exception>
		public void ThrowIfFailed()
		{
			if (HasTimedOut || ExitCode != 0 || StandardError.Length > 0)
			{
				throw new ProgramRunFailedException($"Failed to run command: '{Command}' with arguments: '{Arguments}', exit code: {ExitCode}, runtime {ExecutionTime}, wasTimeout: {HasTimedOut}" +
					$"\nStandard Output:\n{StandardOutput}" +
					$"\n----------\nStandard Error:\n{StandardError}" +
					$"\n----------");
			}
		}
	}


	/// <summary>
	/// Exception thrown when something went wrong with <see cref="CommandLineProgramProcess"/>.
	/// </summary>
	[Serializable]
	public class CommandLineProgramException : Exception
	{
		public CommandLineProgramException()
		{
		}

		public CommandLineProgramException(string message) : base(message)
		{
		}

		public CommandLineProgramException(string message, Exception innerException) : base(message, innerException)
		{
		}

		protected CommandLineProgramException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}

	/// <summary>
	/// Exception thrown when the program did ot run succesfully, inidcated an error (non-zero exit code) or wrote to the Error Stream.
	/// </summary>
	[Serializable]
	public class ProgramRunFailedException : CommandLineProgramException
	{

		public ProgramRunFailedException()
		{
		}

		public ProgramRunFailedException(string message) : base(message)
		{
		}

		public ProgramRunFailedException(string message, Exception innerException) : base(message, innerException)
		{
		}

		protected ProgramRunFailedException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}

	internal class ProcessStream
	{
		/*
		 * Class to get process stdout/stderr streams
		 * Author: SeemabK (seemabk@yahoo.com)
		 * Usage:
			//create ProcessStream
			ProcessStream myProcessStream = new ProcessStream();
			//create and populate Process as needed
			Process myProcess = new Process();
			myProcess.StartInfo.FileName = "myexec.exe";
			myProcess.StartInfo.Arguments = "-myargs";
			//redirect stdout and/or stderr
			myProcess.StartInfo.UseShellExecute = false;
			myProcess.StartInfo.RedirectStandardOutput = true;
			myProcess.StartInfo.RedirectStandardError = true;
			//start Process
			myProcess.Start();
			//connect to ProcessStream
			myProcessStream.Read(ref myProcess);
			//wait for Process to end
			myProcess.WaitForExit();
			//get the captured output :)
			string output = myProcessStream.StandardOutput;
			string error = myProcessStream.StandardError;
		 */
		private Thread _standardOutputReaderThread;
		private Thread _standardErrorReaderThread;
		private Process _process;
		private string _standardOutputString = "";
		private string _standardErrorString = "";

		public string StandardOutput => _standardOutputString;

		public string StandardError => _standardErrorString;

		public ProcessStream()
		{
			Init();
		}

		public void Read(Process process)
		{
			try
			{
				Init();
				_process = process;

				if (_process.StartInfo.RedirectStandardOutput)
				{
					_standardOutputReaderThread = new Thread(new ThreadStart(ReadStandardOutput));
					_standardOutputReaderThread.Start();
				}

				if (_process.StartInfo.RedirectStandardError)
				{
					_standardErrorReaderThread = new Thread(new ThreadStart(ReadStandardError));
					_standardErrorReaderThread.Start();
				}

				int readTimeout = 1 * 60 * 1000; // one minute

				if (_standardOutputReaderThread != null)
					_standardOutputReaderThread.Join(readTimeout);

				if (_standardErrorReaderThread != null)
					_standardErrorReaderThread.Join(readTimeout);

			}
			catch { }
		}

		private void ReadStandardOutput()
		{
			if (_process == null)
				return;

			try
			{
				StringBuilder sb = new StringBuilder();
				string line = null;

				while ((line = _process.StandardOutput.ReadLine()) != null)
				{
					sb.Append(line);
					sb.Append(Environment.NewLine);
				}

				_standardOutputString = sb.ToString();
			}
			catch { }
		}

		private void ReadStandardError()
		{
			if (_process == null)
				return;

			try
			{
				StringBuilder sb = new StringBuilder();
				string line = null;

				while ((line = _process.StandardError.ReadLine()) != null)
				{
					sb.Append(line);
					sb.Append(Environment.NewLine);
				}

				_standardErrorString = sb.ToString();
			}
			catch { }
		}

		private void Init()
		{
			_standardErrorString = "";
			_standardOutputString = "";
			_process = null;
			Stop();
		}

		public void Stop()
		{
			try
			{
				if (_standardOutputReaderThread != null)
					_standardOutputReaderThread.Abort();
			}
			catch { }

			try
			{
				if (_standardErrorReaderThread != null)
					_standardErrorReaderThread.Abort();
			}
			catch { }

			_standardOutputReaderThread = null;
			_standardErrorReaderThread = null;
		}
	}
}