﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shared
{
	/// <summary>
	/// Compares using the default comparer, except that <see langword="null"/> is ordered last instead of first.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class NullLastComparer<T> : IComparer<T?>
		where T : struct, IComparable<T>
	{
		private NullLastComparer() { } // Hide constructor to force using the static

		/// <summary>
		/// The Comparer instance. Can be shared across Threads without limitation.
		/// </summary>
		public static readonly IComparer<T?> Comparer = new NullLastComparer<T>();


		/// <inheritdoc />
		public int Compare(T? x, T? y)
		{
			if (ReferenceEquals(x, y)) return 0;
			if (! y.HasValue) return 1;
			if (! x.HasValue) return -1;
			return x.Value.CompareTo(y.Value);
		}
	}
}
