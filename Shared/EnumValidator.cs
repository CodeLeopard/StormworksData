﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Linq;

namespace Shared
{
	/// <summary>
	/// Validates an enum WAY faster than <see cref="Enum.IsDefined"/>.
	/// </summary>
	public static class EnumValidator
	{
		/// <summary>
		/// Checks if the provided <paramref name="value"/> is actually part of the enum.
		/// That is: there is a name in the enum that maps to that value.
		/// It does not work on Flags enums: it requires that every value has a defined name.
		/// </summary>
		/// <typeparam name="TEnum"></typeparam>
		/// <param name="value"></param>
		/// <returns></returns>
		public static bool IsDefined<TEnum>(this TEnum value) where TEnum : System.Enum
		{
			return EnumValidator<TEnum>.IsDefined(value);
		}
	}

	public static class EnumValidator<TEnum> where TEnum : System.Enum
	{
		/// <summary>
		/// Sorted array of values so BinarySearch can be used.
		/// </summary>
		// We sort it again because Enum.GetValues sorts as unsigned, which may or may not be correct for the actual type.
		private static readonly TEnum[] values = Enum.GetValues(typeof(TEnum)).Cast<TEnum>().OrderBy(e => e).ToArray();

		/// <inheritdoc cref="EnumValidator.IsDefined{T}"/>
		public static bool IsDefined(TEnum value)
		{
			int index = Array.BinarySearch(values, value);
			return index >= 0;
		}
	}
}
