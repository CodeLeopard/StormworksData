# Changelog

## [0.22.3] - UNRELEASED

### Added
- StormworksPaths.GetRelativePath (for StormworksEditor).
- Added TrainTrackLine class that models the links between track nodes. To be used by the Signalling addon to store additional properties on the track line XML so these can be set much more easily.

## [0.22.2] - 2025-02-08

### Fixed
- ply2mesh failing with non-obvious error when converting a ply without color (it should default to white).

## [0.22.1] - 2024-12-23

### Changed
- Improved `help ply2mesh` to mention also specifying `Shader` in the file name.
- Improved `help ply2mesh` to mention also how to combine `SubMesh` and `Shader`.

## [0.22.0] - 2024-12-15

### Added
- For the custom Serializer to be able to handle default values that cannot appear in attributes added a way for a class to modify the specification using a static method.
  Such method should be `static void` and take `TypeSerializationSpecification` as the only parameter. It should be annotated with the new attribute `PatchSerializationSpecAttribute`.
  Calling the patch method can be disabled in the `SpecGeneratorSettings` but is on by default.
- Added support for `map_geometry.bin` files (the files used to create the in-game map view, both in the UI and on Lua screens).
- Add parameter `withPhysicsAndSurfaces` to command `GenerateLODVehicle`.

### Fixed
- Playlist location and component ids, did not start at 1 when the playlist was created from C#.
- Command `TrackExport` did not take quoted parameter.

## [0.21.11] - 2024-08-07

### Added
- Support for the Creature component type in missions.

## [0.21.10] - 2024-05-31

### Added
- An explicit error will now fire when trying to save a phys that contains more vertices or indices that can be stored in the binary file format.
- Added `large` property in `VisiblePropertyAttribute`, to be used in StormworksEditor Playlist Component Tags.


### Fixed
- Arithmetic overflow in phys data model when trying to count total vertices or indices and a submesh contained.
- Missing Tags property on many Playlist Component Wrappers.
- Playlist Component MissionZone `tags` property used `name` instead of correct `cargo_zone_import_cargo_type`.


## [0.21.9] - 2024-02-19

### Added
- The model for TrainTrackNodes has been improved with more input validation: it fixes self references and one-sided links so the model presented is consistent.
- Add command to export trainTrackNodes from xml to Lua tables.

### Changed
- Submesh detection no longer accepts _ as a separator. This was not documented to begin with, so it's not a breaking change.

### Fixed
- Crash when loading definition of rocket with fins component.

## [0.21.8] - 2024-01-26

### Added
- More output to ValidateCurveSpec

### Fixed
- CurveDecororator.Clear() didn't work properly causing errors if the CurveDecorator was re-used.

## [0.21.7] - 2023-11-22

### Added
- Spotlight setts ies texture to the default `graphics/ies/ies_default.txtr`
- Spotlight fov split into fov_rad and fov_deg for intuitive usage without conversions.

### Fixed
- Commands with multiple quoted string parameters with at least one optional one that was omitted would fail to parse.
- Fixed (harmless) error when navigating to folder that was not a stormworks directory.


## [0.21.6] - 2023-11-19

### Added
- InterpolationSettings.SegmentCenterAngle: The maximum angle between the tangent of the curve and the direction of the segment, measured in the center.

### Fixed
- Extrusion making straight segment over S bend. Fixed by the InterpolationSettings.SegmentCenterAngle feature.


## [0.21.5] - 2023-11-18

### Added
- ID property to ConnectionCustomData for the Editor to use to ensure Export consistency.
- Ply converter can also use `_` as separator (besides current `.`).
- Ply converter can infer shader from file name: `path/name.S2.ply` Means: Shader 2, which is emissive. This can work in combination with multiple input files to single output file.

### Fixed
- LodVehicleGenerator command did not apply proper transformation.

## [0.21.4] - 2023-09-19

### Added
- Support for game version 1.6.4 Frontier DLC.
	- Tile loading can deal with loops in parent hierarchy.
	- Added handling for now-optional property 'category' in Definitions.
- Added settings for Ply converter: use `help SetPlyConversionSettings` for usage.
	Note: settings are not yet saved.
		- ReverseTriangles
		- ReverseNormals
		- TransformUpConvention (Blender z-up <==> Stormworks y-up)
- Command SetWorkingDirectory will check if the new workingDirectory is inside a Stormworks install and if so use it (as if `SetStormworksInstallation` was used).
- CurveSpec / Extrusion added ability to split the curve into multiple segments each generating mesh and phys for the result. This will prevent a single mesh from being too big on very large or detailed curves.
- Ply loader ignores extra properties such as UV coordinates instead of failing.
- Playlist objects can be moved between the env and mission variants of the location they are on.
- Tile supports lights_omni, lights_spot and lights_tube.

### Removed
- Support for NetStandard2.0

### Fixed
- `ply2phys` was broken and saved `.ply` data into the `.phys` file.
- `ply2phys` would create a `.phys` with the triangles reversed.
- `ply2phys` would not apply up convention setting.
- Vehicle Mesh generator bugged surface orientation/rotation in some cases. #4
- Tile parent hierarchy detects loops.
- ply converter will give clear message when file contains no vertices. (This can happen in Blender when 'selection only' is checked but nothing was selected.)
- ply converter would write `Shader` but expected `ShaderId`. It now only uses `Shader`. #39
- Broken Equality implementation for MeshOrPhysSharing.
- race/threading issue in CurveDecorator that sometimes caused an exception during track generation.
- CurveSpec.Clone() not cloning the Transform properly, causing for example two duplicated CurveSpecs to be in the same position even if one was moved.
- Playlist location did not normalize tile path with backslash causing Stormworks to be unable to find the tile.
- Missing Tags on Playlist vehicle wrapper.
### Changed
- `.ply` converter has settings that can be read and changed with `[Set]PlyConversionSettings`.
- `.ply` converter default settings are now consistent with Blender workflow: reverse triangles & normals, and swap Y and Z axis (Blender has Z-up & Y-forward).
- Temporarily changed converters to always save `.phys` files as ordered vertices because it seems the game is ignoring indices if we save them. This is tracked in #36
- Improved LuaCurveSpec:
	- MeshSpecification has full Transform instead of hard to use Matrix4
	- Make PeriodicGroup usable.
	- Transform can add rotations, as quaternion, radians or degrees, this makes the API nicer to use.
- StormworksPaths API exposes some of it's members so that it's search mechanisms can be used without actually setting the install location.
## [0.18.22] - 2022-09-15

### Added
- Allow silent (no events) access to `ConnectionCustomData.CurveSpec`
- File paths in Xml object properties are now corrected to only use forward slash / for OS compatibility.
- `CurveSpec.WillGenerateMesh` and `WillGeneratePhys` that check if a `CurveSpec` will generate mesh or phys data.
- `AbstractTileItem` will remember it's order relative to other elements in the xml document and gets saved back in that order.
- Improved `Connection.Split` API so it takes a time or distance parameter.
- Add commands `InvisifyVehicleBlocks` and `InvisifyVehicleBlocksOnlyOnly` to mess with the orientation of blocks in a vehicle to make them invisible (or undo such operation).
- Added API to save a vehicle back to file. Note! because Vehicles are not fully implemented some data may be lost on a round trip.
- Added handling of paintable signs and indicators in VehicleComponents.

### Changed
- A vehicle can now be created from scratch. Note that consistency is NOT enforced yet, API changes are needed to be able to do that. You'll be able to break things.
- Playlist/Mission Location.name is apparently optional now 🥴

### Depreciated
- `Connection.Split()` is obsolete, convert to `Connection.SplitAtTime(0.5f)`.
- Vehicle API things will change in the future so consistency can be enforced under modifications.

### Fixed
- NullReferenceException when Clone()-ing a `CurveSpec` due to a `MeshSharing` or `PhysSharing` member being null (this should be allowed).
- `LoopSet.Clone()` did not copy property `ReverseTriangles`.
- `CurveDecorator` ignored `CurveSpecs.SubSpecs`.
- Incorrect extension of inferred destination file for file conversion commands involving `.ply` #25.
- Playlists not saving correctly.

### known issues
- Vehicle saving removes microcontrollers and other complicated components, as well as less common attributes from the vehicle. This is due to those parts of Vehicle not yet being implemented.
- Vehicle modification does not enforce consistency.

## [0.18.21] - 2022-07-25

### Fixed
- Commandline's `ls aFolder` would not load metaData for .mesh and .phys files.

## [0.18.20] - 2022-07-03

### Fixed
- Incorrect type name in Header.GraphRootType saved by CustomSerializer.
- MeshCombiner did not preserve ShaderId.

### Added
- LazyCache added Async editions for methods that return a TValue.
- MeshCache added Async editions for GetOrLoad

### Changed
- LazyCache now has explicit constructor variations for all possible usages: default parameter values ware removed. Throws ArgumentNullException when any parameter is null.

## [0.18.19] - 2022-06-20

### Fixed
- ConvertPlyToPhys would throw error when normal or color data was detected when it should be ignored.
- Typo in command alias ply2phys (ConvertPlyToPhys).
- Incorrectly marked Tile.AllTileItemsTyped() as obsolete.

## [0.18.18] - 2022-06-18
Reworked Extrusion system to be more extensible.

### Added
#### Tile
- Tile TrainTrack is now modeled as a proper graph structure.
- Tile added property IsTerrain, a hint for the 3D World Editor.
- Tile added property LoadIgnore, a hint for the 3D World Editor.
- Tile.Add(TileItem) returns bool

#### CommandLine
- CommandLine can validate lua files that generate a CurveSpec.
- CommandLine `ls` shows information about `.mesh` and `.phys` files.

#### Misc
- CubicBezierCurve reverse CurveSamples, a way to iterate over the curve in reverse.
- CurveSample add Transformation() which returns the Transformation matrix for the sample (a combination of position, rotation and scale)
- `.ply` converter now supports submeshes through special file naming scheme.
- `.ply` converter has better error messages.
- `.ply` converter can now convert to/from `.phys` directly.
- Add Clone() to `Mesh`
- Add Clone() to `Phys`
- Add Nuget packages for all library projects.
- Move LazyCache from Unity project to this repo.
- Move MeshCache from Unity project to this repo.
- Add ReadonlySet.
- Add SynchronizedHashSet.
- StormworksPaths add best-effort Linux support using hardcoded guess.
- Generated code is now included in git because it was causing unnecessary complication to build process for freshly cloned projects and CI.
- Add IValidatable and a implementation to validate CurveSpec entries.
- Release process in CI with CommandLine binaries for both Windows and Linux.

#### Extrusion
- The way extrusions work has been rebuilt from the ground up to be more extensible and maintainable.
- Add Lua integration to create the CurveSpec data structures that define how to extrude.
#### Tests
- Converter for `.mesh`, `.phys` to and from `.ply` is now under test coverage for single submesh use cases.

### Changed
- Tile collection properties now stay updated when tile is modified, but as a consequence are not writable anymore.
- Renamed `PhyMesh` -> `Phys`.
- `Mesh` and `Phys` now use List instead of Array as their internal data structure.
- `VertexRecord` change name of field `colorRGBA` to `color`
- Change dataType of members that relate to colors from `Vector4` to `Color4`.
- MeshConverter takes IEnumerable instead of Array.
- Methods like `IsFinite` for float and other structs changed to extension methods.
- InteractivePromptLib is now open source and can be fetched from [it's own repo](https://gitlab.com/CodeLeopard/InteractivePrompLib/)


### Depreciated

### Removed
- Old extrusion system was completely reworked to the extent there is no compatibility with the new system.
- Removed header datastructure from `Mesh` and `Phys` classes. The header data is now part of the class directly. Direct references to the header ware removed, the passthrough properties on the class are now fields.
- Converters.Binary removed header related diag.
- Redundant dependencies.


### Fixed
- Missing Flush() in file operations in various places.
- File overwrite not truncating in various places.
- ExceptionExt missing newline in ToString()

## [0.18.17.1] - 2022-05-16
Initial public release.

### Added
- License: `LGPL-3.0-or-later`

