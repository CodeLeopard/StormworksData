﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Reflection;

namespace CustomSerialization.MemberInfoExt
{
	public abstract class ValueMemberInfo : MemberInfo
	{
		/// <summary>
		/// The <see cref="Type"/> of the value held by the Member.
		/// </summary>
		public abstract Type MemberValueType { get; }

		/// <summary>
		/// Retrieve the value of the member for <paramref name="instance"/>.
		/// </summary>
		/// <param name="instance"></param>
		/// <returns></returns>
		public abstract object GetValue(object instance);

		/// <summary>
		/// Set the value of the member on <paramref name="instance"/> to <paramref name="value"/>
		/// </summary>
		/// <param name="instance"></param>
		/// <param name="value"></param>
		public abstract void SetValue(object   instance, object value);

	}
}
