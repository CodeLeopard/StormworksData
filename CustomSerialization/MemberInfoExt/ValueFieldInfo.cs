﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Reflection;

namespace CustomSerialization.MemberInfoExt
{
	public class ValueFieldInfo : ValueMemberInfo
	{
		public readonly FieldInfo Field;

		public ValueFieldInfo(FieldInfo field)
		{
			Field = field ?? throw new ArgumentNullException(nameof(field));
		}

		/// <inheritdoc />
		public override Type MemberValueType => Field.FieldType;

		/// <inheritdoc />
		public override object GetValue(object instance)
		{
			return Field.GetValue(instance);
		}

		public override void SetValue(object instance, object value)
		{
			Field.SetValue(instance, value);
		}

		/// <inheritdoc />
		public override object[] GetCustomAttributes(bool inherit)
		{
			return Field.GetCustomAttributes(inherit);
		}

		/// <inheritdoc />
		public override object[] GetCustomAttributes(Type attributeType, bool inherit)
		{
			return Field.GetCustomAttributes(attributeType, inherit);
		}

		/// <inheritdoc />
		public override bool IsDefined(Type attributeType, bool inherit)
		{
			return Field.IsDefined(attributeType, inherit);
		}

		/// <inheritdoc />
		public override Type DeclaringType => Field.DeclaringType;

		/// <inheritdoc />
		public override MemberTypes MemberType => Field.MemberType;

		/// <inheritdoc />
		public override string Name => Field.Name;

		/// <inheritdoc />
		public override Type ReflectedType => Field.ReflectedType;
	}
}
