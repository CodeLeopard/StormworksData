﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;

using CustomSerialization.Specification;

namespace CustomSerialization
{
	internal class Header
	{
		internal const string ElementName = "Head";
		internal const string AttributeName_SerializerVersion = "SerializerVersion";
		internal const string AttributeName_GraphRootType = "GraphRootType";
		internal const string AttributeName_GraphSpecHash = "GraphSpecHash";


		public Version SerializerVersion = Serializer.Version;
		public string GraphRootType;
		public int GraphSpecHash;




		internal Header(GraphSerializationSpecification spec)
		{
			GraphRootType = spec.RootSpec.SerializedName;
			GraphSpecHash = 0; //spec.GetHashCode(); // HashCode needs a custom implementation that is stable.
		}

		private Header(Version serializerVersion, string graphRootType, int graphSpecHash)
		{
			SerializerVersion = serializerVersion;
			GraphRootType = graphRootType;
			GraphSpecHash = graphSpecHash;
		}

		internal static Header FromXElement(XElement element)
		{
			string versionString = element.Attribute(AttributeName_SerializerVersion).Value;
			string graphRootType = element.Attribute(AttributeName_GraphRootType).Value;
			int graphSpecHash = int.Parse(element.Attribute(AttributeName_GraphSpecHash).Value);

			var version = new Version(versionString);

			return new Header(version, graphRootType, graphSpecHash);
		}

		internal XElement ToXElement()
		{
			var e = new XElement(ElementName);
			e.SetAttributeValue(AttributeName_SerializerVersion, SerializerVersion.ToString());
			e.SetAttributeValue(AttributeName_GraphRootType, GraphRootType);
			e.SetAttributeValue(AttributeName_GraphSpecHash, GraphSpecHash);
			return e;
		}
	}
}
