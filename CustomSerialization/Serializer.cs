﻿// Copyright 2022-2025 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Linq;

using CustomSerialization.BuiltIn;
using CustomSerialization.Exceptions;
using CustomSerialization.Specification;

using Shared.Serialization;

namespace CustomSerialization
{
	public class Serializer
	{
		public static readonly Version Version = new Version(0, 0, 0);


		public static void Serialize(Stream stream, object rootObject, StreamingContext context)
		{
			if (! SpecGenerator.GraphSpecifications.TryGetValue(rootObject.GetType(), out var spec))
			{
				throw new ArgumentException($"There is no specification for how to serialize {rootObject.GetType()}");
			}

			Serialize(stream, rootObject, spec, context);
		}

		public static void Serialize(Stream stream, object rootObject, GraphSerializationSpecification graphSpec, StreamingContext context)
		{
			var serializer = new Serializer(graphSpec);
			serializer.SerializeObjectGraph(stream, rootObject, context);
		}

		public static XDocument Serialize(object rootObject, GraphSerializationSpecification graphSpec, StreamingContext context)
		{
			var serializer = new Serializer(graphSpec);
			return serializer.SerializeObjectGraph(rootObject, context);
		}

		public static TData Deserialize<TData>(Stream stream, GraphSerializationSpecification graphSpec, StreamingContext context)
		{
			var serializer = new Serializer(graphSpec);

			return serializer.Deserialize<TData>(stream, context);
		}

		public static TData Deserialize<TData>(XDocument doc, GraphSerializationSpecification graphSpec, StreamingContext context)
		{
			var serializer = new Serializer(graphSpec);

			return serializer.Deserialize<TData>(doc, context);
		}

		internal const string DocumentRootName = "Document";
		internal const string HeaderName = Header.ElementName;
		internal const string ContentName = "Content";
		internal const string RefContentName = "References";
		internal const string ContainerElementName = "Entry";
		internal const string ReferencesElementNamePrefix = "References_to_";

		internal const string RefIdAttributeName = "ref_id";
		internal const string TypeAttributeName = "type";
		internal const string PrimitiveValueAttributeName = "value";
		internal const string IsNullAttributeName = "isNull";

		internal const int StringAsContentThreshold = 60;


		public readonly GraphSerializationSpecification graphSpec;
		internal StreamingContext context;

		internal ByRefContainer instanceToRefIdMap = new ByRefContainer();

		internal DictionaryOfContainer<Type, Dictionary<uint, object>> refIdToInstanceMap =
			new DictionaryOfContainer<Type, Dictionary<uint, object>>();

		internal HashSet<object> alreadyDeserializedRefObjects = new HashSet<object>();
		internal HashSet<object> toDeserializeRefObjects = new HashSet<object>();
		internal HashSet<object> refDiscovery = new HashSet<object>();
		internal HashSet<object> graphDeserializedEventReceivers = new HashSet<object>();

		private Serializer(GraphSerializationSpecification graphSpec)
		{
			this.graphSpec = graphSpec ?? throw new ArgumentNullException(nameof(graphSpec));
			graphSpec.MakeReady();
		}


		private void CleanSlate()
		{
			instanceToRefIdMap.Clear();
			refIdToInstanceMap.Clear();
			alreadyDeserializedRefObjects.Clear();
			toDeserializeRefObjects.Clear();
			refDiscovery.Clear();
			graphDeserializedEventReceivers.Clear();
		}


		#region Serialize implementation

		internal void SerializeObjectGraph(Stream stream, object rootObject, StreamingContext context)
		{
			if (null == stream) throw new ArgumentNullException(nameof(stream));
			if (null == rootObject) throw new ArgumentNullException(nameof(rootObject));

			var settings = new XmlWriterSettings();
			settings.Indent = true;
			if (!stream.CanSeek)
			{
				// Console cannot seek, and it's tab width is too wide.
				settings.IndentChars = "    ";
			}
			else
			{
				settings.IndentChars = "\t";
			}

			settings.CheckCharacters = true;

			var writer = XmlWriter.Create(stream, settings);

			var doc = SerializeObjectGraph(rootObject, context);

			doc.WriteTo(writer);
			writer.Flush();
		}


		internal XDocument SerializeObjectGraph(object rootObject, StreamingContext context)
		{
			this.context = context;

			var doc = new XDocument();

			var rootSpec = graphSpec.TypeSpecifications[graphSpec.Root];
			string rootName = DocumentRootName;
			if (graphSpec.NoHeader)
			{
				rootName = rootSpec.SerializedName;
			}
			XElement docRoot = new XElement(rootName);
			doc.Add(docRoot);

			XElement rootElem;

			if (! graphSpec.NoHeader)
			{
				var header = new Header(graphSpec);
				docRoot.Add(header.ToXElement());

				rootElem = new XElement(ContentName);
				docRoot.Add(rootElem);
			}
			else
			{
				rootElem = docRoot;
			}

			CleanSlate();
			try
			{
				SerializeType(rootObject, rootSpec, rootElem);

				// ByRef stuff.
				if (graphSpec.ByRefTypes.Count > 0)
				{
					var refElem = new XElement(RefContentName);
					docRoot.Add(refElem);

					foreach (Type refType in graphSpec.ByRefTypes)
					{
						var typeSpec = graphSpec.TypeSpecifications[refType];

						var containerElem = new XElement($"{ReferencesElementNamePrefix}{typeSpec.SerializedName}");
						refElem.Add(containerElem);

						foreach (var kvp2 in instanceToRefIdMap.Values(refType))
						{
							var instance = kvp2.Key;
							var refId = kvp2.Value;

							var elem = new XElement(refType.Name);
							containerElem.Add(elem);
							elem.SetAttributeValue(RefIdAttributeName, refId);
							SerializeType(instance, typeSpec, elem, ignoreByRef: true);
						}
					}
				}

				return doc;
			}
			finally
			{
				// Ensure that no matter what we don't keep references to the the objectGraph.
				CleanSlate();
			}
		}

		/// <summary>
		/// Serialize <paramref name="instance"/> into an existing empty <see cref="XElement"/> <paramref name="element"/>.
		/// </summary>
		/// <param name="instance"></param>
		/// <param name="typeSpec"></param>
		/// <param name="element"></param>
		/// <param name="ignoreByRef">Ignore <see cref="TypeSerializationSpecification.ByRef"/> and serialize the object anyway.</param>
		public void SerializeType(object instance, TypeSerializationSpecification typeSpec, XElement element, bool ignoreByRef = false)
		{
			if (instance == null) throw new ArgumentNullException(nameof(instance));
			if (typeSpec == null) throw new ArgumentNullException(nameof(typeSpec));
			if (element  == null) throw new ArgumentNullException(nameof(element));

			Type specType = typeSpec.Type;
			Type realType = instance.GetType();
			if (realType != typeSpec.Type)
			{
				// Abstract/interface or inheritance crap.

				if (! graphSpec.TypeSpecifications.TryGetValue(realType, out var realTypeSpec))
				{
					if (specType.IsAbstract || specType.IsInterface)
					{
						throw new MissingAbstractOrInterfaceImplementationSpecificationException
							(
							 $"Spec defined type '{specType.Name}' is {(specType.IsAbstract ? "an abstract type" : "a interface")}, the real type '{realType.Name}' has no {nameof(TypeSerializationSpecification)} and can therefore not be serialized."
						   + $" You may need to manually add a Specification to the {nameof(GraphSerializationSpecification)} that was passed to the {nameof(Serializer)}."
							);
					}
					else
					{
						throw new MissingAbstractOrInterfaceImplementationSpecificationException
							(
							 $"Encountered a unknown type '{realType}' where '{realTypeSpec.SerializedName}' was expected."
						   + $" You may need to manually add a Specification to the {nameof(GraphSerializationSpecification)} that was passed to the {nameof(Serializer)}."
							);
					}
				}


				element.SetAttributeValue(TypeAttributeName, realTypeSpec.SerializedName);
				typeSpec = realTypeSpec;
			}

			if (null != typeSpec.OverrideSerialize)
			{
				typeSpec.OverrideSerialize(this, element, instance);
				return;
			}

			if (typeSpec is PrimitiveSpec primSpec)
			{
				SerializePrimitive(instance, primSpec, element);
				return;
			}

			if (typeSpec.IsCollection)
			{
				typeSpec.OnSerializing?.Invoke(instance, context);
				CollectionSerializer.SerializeCollection(this, instance, element);
				typeSpec.OnSerialized?.Invoke(instance, context);
				return;
			}

			if (typeSpec.ByRef && ! ignoreByRef)
			{
				uint refId = instanceToRefIdMap.GetOrAdd(typeSpec.Type, instance);
				element.SetAttributeValue(RefIdAttributeName, refId);

				ExamineByRef(typeSpec, instance);
				return;
			}

			typeSpec.OnSerializing?.Invoke(instance, context);
			typeSpec.BeforeMembersSerialized?.Invoke(instance, this, element);

			foreach (var memberSpec in typeSpec.Members)
			{
				try
				{
					SerializeMember(memberSpec, instance, element);
				}
				catch (Exception e)
				{
					// todo: wrong
					throw new UnknownTypeException
						(
						 $"Encountered unknown type '{e.GetType().FullName}' while serializing member '{memberSpec.SerializedName}' in type '{typeSpec.SerializedName}'."
						);
				}
			}

			typeSpec.OnSerialized?.Invoke(instance, context);
		}

		/// <summary>
		/// Serialize a primitive type.
		/// </summary>
		/// <param name="instance"></param>
		/// <param name="primitiveSpec"></param>
		/// <param name="parent"></param>
		public void SerializePrimitive(object instance, PrimitiveSpec primitiveSpec, XElement parent)
		{
			primitiveSpec.OnSerializing?.Invoke(instance, context);

			string value = primitiveSpec.ValueToString(instance);

			parent.SetAttributeValue(PrimitiveValueAttributeName, value);

			primitiveSpec.OnSerialized?.Invoke(instance, context);
		}

		/// <summary>
		/// Serialize a member
		/// </summary>
		/// <param name="memberSpec">Specification of the member</param>
		/// <param name="memberOwnerInstance">The object that owns the member</param>
		/// <param name="memberParent">The <see cref="XElement"/> that will represent the owner of the member</param>
		public void SerializeMember(MemberSerializationSpecification memberSpec, object memberOwnerInstance, XElement memberParent)
		{
			if (memberSpec.ByReference)
			{
				memberParent.Add(SerializeMemberAsReference(memberSpec, memberOwnerInstance));

				ExamineByRef(memberSpec, memberOwnerInstance);
				return;
			}



			object instance = memberSpec.GetValue(memberOwnerInstance);

			if (memberSpec.AsAttribute)
			{
				SerializeMemberAsAttribute(memberSpec, instance, memberParent);
				return;
			}

			if (memberSpec.OmitIfDefault && (instance == null || instance.Equals(memberSpec.DefaultValue)))
			{
				return;
			}

			var element = new XElement(memberSpec.SerializedName);
			memberParent.Add(element);

			if (instance == null)
			{
				element.SetAttributeValue(IsNullAttributeName, true);
				return;
			}

			SerializeType(instance, graphSpec.TypeSpecifications[memberSpec.Member.MemberValueType], element);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberSpec"></param>
		/// <param name="instance"></param>
		/// <param name="memberParent"></param>
		internal void SerializeMemberAsAttribute
			(MemberSerializationSpecification memberSpec, object instance, XElement memberParent)
		{
			if (instance == null)
			{
				if (memberSpec.Optional)
				{
					return;
				}

				throw new InvalidOperationException
					(
					 $"Encountered null value for non-optional member '{memberSpec.RealName}' (to be serialized as '{memberSpec.SerializedName}') from type '{memberSpec.ParentSpecification.RealName}'."
					);
			}

			if (memberSpec.OmitIfDefault && instance.Equals(memberSpec.DefaultValue))
			{
				return;
			}

			var spec = graphSpec.GetSpec(instance.GetType()) as PrimitiveSpec ?? throw new ArgumentException(); // todo: what about interface implementors?

			spec.OnSerializing?.Invoke(instance, context);
			string value = spec.ValueToString(instance);

			memberParent.SetAttributeValue(memberSpec.SerializedName, value);

			spec.OnSerialized?.Invoke(instance, context);
		}

		internal XElement SerializeMemberAsReference(MemberSerializationSpecification memberSpec, object instance)
		{
			uint refId = instanceToRefIdMap.GetOrAdd(memberSpec.ValueType, memberSpec.GetValue(instance));

			var element = new XElement(memberSpec.Member.MemberValueType.Name);

			Type realType = instance.GetType();
			if (realType != memberSpec.ValueType)
			{
				// Abstract/interface or inheritance crap.

				var realSpec = graphSpec.TypeSpecifications[realType];

				element.SetAttributeValue(TypeAttributeName, realSpec.SerializedName);
			}
			element.SetAttributeValue(RefIdAttributeName, refId);

			return element;
		}



		#region ByRef Helpers

		/// <summary>
		/// Examine a object that is to be serialized as reference to discover any data inside that also needs to be serialized
		/// so that internal data structures are kept up to date.
		/// </summary>
		/// <param name="instance"></param>
		/// <param name="spec"></param>
		public void ExamineByRef(TypeSerializationSpecification spec, object instance)
		{
			if (spec.IsPrimitive) return;
			if (spec.Type.IsValueType) return;
			if (!refDiscovery.Add(instance)) return;

			if (spec.ByRef)
			{
				instanceToRefIdMap.GetOrAdd(spec.Type, instance);
			}

			if (spec.IsCollection)
			{
				CollectionSerializer.RefDiscoverCollection(this, instance);
				return;
			}


			foreach (var memberSpec in spec.Members)
			{
				if (memberSpec.ByReference)
				{
					instanceToRefIdMap.GetOrAdd(memberSpec.Member.MemberValueType, memberSpec.GetValue(instance));
					ExamineByRef(memberSpec, instance);
				}
				else
				{
					ExamineByRef(memberSpec, instance);
				}
			}
		}

		/// <summary>
		/// Examine a object that is to be serialized as reference to discover any data inside that also needs to be serialized
		/// so that internal data structures are kept up to date.
		/// </summary>
		/// <param name="memberSpec"></param>
		/// <param name="memberOwnerInstance"></param>
		public void ExamineByRef(MemberSerializationSpecification memberSpec, object memberOwnerInstance)
		{
			object instance = memberSpec.GetValue(memberOwnerInstance);

			if (instance == null)
			{
				return;
			}

			ExamineByRef(graphSpec.TypeSpecifications[memberSpec.Member.MemberValueType], instance);
		}

		#endregion ByRef Helper

		#endregion Serialize implementation

		#region Deserialize Implementation

		/// <summary>
		/// Deal with the bookKeeping and events that need to trigger after Deserialization of an object.
		/// </summary>
		/// <param name="spec"></param>
		/// <param name="instance"></param>
		public void HandleDeserialized(TypeSerializationSpecification spec, object instance)
		{
			if (null == spec) throw new ArgumentNullException(nameof(spec));
			if (null == instance) return;

			spec.OnDeserialized?.Invoke(instance, context);
			if (spec.OnGraphDeserialized != null)
			{
				graphDeserializedEventReceivers.Add(instance);
			}
		}

		internal TData Deserialize<TData>(Stream stream, StreamingContext context)
		{
			if (typeof(TData) != graphSpec.Root)
				throw new ArgumentException($"The generic Type argument {nameof(TData)} does not match with the Root type specified in {nameof(graphSpec)}");

			this.context = context;

			// This indirection is needed because the debugger cannot modify the content of a generic method.
			return (TData)Deserialize(stream);
		}

		internal TData Deserialize<TData>(XDocument doc, StreamingContext context)
		{
			if (typeof(TData) != graphSpec.Root)
				throw new ArgumentException($"The generic Type argument {nameof(TData)} does not match with the Root type specified in {nameof(graphSpec)}");

			this.context = context;

			// This indirection is needed because the debugger cannot modify the content of a generic method.
			return (TData)Deserialize(doc);
		}

		internal object Deserialize(Stream stream)
		{
			if (null == stream) throw new ArgumentNullException(nameof(stream));

			var doc = XDocument.Load(stream, LoadOptions.SetLineInfo);

			return Deserialize(doc);
		}

		internal object Deserialize(XDocument doc)
		{
			var rootSpec = graphSpec.TypeSpecifications[graphSpec.Root];

			string rootElemName = DocumentRootName;
			string contentElemName = ContentName;
			if (graphSpec.NoHeader)
			{
				rootElemName = rootSpec.SerializedName;
				contentElemName = rootElemName;
			}


			XElement docRoot = doc.Element(rootElemName)
			                ?? throw new InvalidFormatException
				                   (
				                    $"Did not find root element '{rootElemName}'."
				                   );

			XElement content;
			XElement refContent = null;
			if (! graphSpec.NoHeader)
			{

				var headerElement = docRoot.Element(HeaderName)
				                 ?? throw new InvalidFormatException
					                    (
					                     $"Did not find element '{HeaderName}'. Is the stream meant for this serializer?"
					                    );

				content = docRoot.Element(ContentName);
				refContent = docRoot.Element(RefContentName);

				var header = Header.FromXElement(headerElement);

				if (header.SerializerVersion != Version)
				{
					// todo: warn
				}

				if (header.GraphRootType != graphSpec.RootSpec.SerializedName)
				{
					throw new TypeMismatchException
						(
						 $"The Type of the object expected to be deserialized '{graphSpec.RootSpec.SerializedName}' does not match the type of object found in the stream '{header.GraphRootType}'."
						);
				}

				// todo: implement the hashcode thing.
				if (header.GraphSpecHash != graphSpec.GetHashCode())
				{
					// todo: complain? throw?
				}
			}
			else
			{
				content = docRoot;
			}

			if (null == content)
			{
				throw new Exceptions.InvalidDataException($"Missing element '{contentElemName}'.");
			}

			var contentAttributes = content.Attributes().XAttributeToDictionary();

			if (contentAttributes.TryGetValue(IsNullAttributeName, out string result) && bool.Parse(result))
			{
				return default;
			}

			CleanSlate();
			try
			{
				object root = Deserialize(content, rootSpec);

				if (graphSpec.ByRefTypes.Count > 0)
				{
					if (null == refContent)
					{
						throw new Exceptions.InvalidDataException($"Missing element '{RefContentName}'.");
					}

					toDeserializeRefObjects.Clear();
					foreach (object o in instanceToRefIdMap.AllObjects())
					{
						toDeserializeRefObjects.Add(o);
					}

					// Go through the ByRef items and read their data into the already created instances.
					foreach (Type type in graphSpec.ByRefTypes)
					{
						var spec = graphSpec.TypeSpecifications[type];
						string elemName = $"{ReferencesElementNamePrefix}{spec.SerializedName}";

						var container = refContent.Element(elemName)
						             ?? throw new Exceptions.InvalidDataException($"Missing element '{elemName}'.");

						foreach (XElement elem in container.Elements())
						{
							var attributes = elem.Attributes().XAttributeToDictionary();
							uint refId = uint.Parse(attributes[RefIdAttributeName]);

							if (refIdToInstanceMap[type].TryGetValue(refId, out object instance))
							{
								if (! alreadyDeserializedRefObjects.Add(instance))
								{
									// Deserialize only once.
									// todo: is getting here a sign of badness?
									continue;
								}

								Deserialize(elem, spec, instance: instance, forceReadRef: true);

								toDeserializeRefObjects.Remove(instance);
							}
							else
							{
								// Unused data.
								// todo: warn? throw?
							}
						}

						if (toDeserializeRefObjects.Count > 0)
						{
							throw new MissingDataException
								(
								 $"After Deserializing, {toDeserializeRefObjects.Count} objects that ware serialized by reference have not been deserialized because their data was not present."
								);
						}
					}
				}

				// Fire OnDeserialized event.
				foreach (object instance in graphDeserializedEventReceivers)
				{
					var spec = graphSpec.TypeSpecifications[instance.GetType()];
					spec.OnGraphDeserialized?.Invoke(instance, null);
				}
				return root;
			}
			finally
			{
				// Ensure that no matter what we don't keep references to the the objectGraph.
				CleanSlate();
			}
		}

		public TData Deserialize<TData>
			(XElement element, TypeSerializationSpecification spec, object instance = null, bool forceReadRef = false)
		{
			return (TData)Deserialize(element, spec, instance: instance, forceReadRef: forceReadRef);
		}

		/// <summary>
		/// Deserialize the data in <see cref="element"/> into <see cref="instance"/> or a new object.
		/// </summary>
		/// <param name="element"></param>
		/// <param name="spec"></param>
		/// <param name="instance">a empty instance that was created to preserve reference identity or <see langword="null"/></param>
		/// <param name="forceReadRef">Ignore <see cref="TypeSerializationSpecification.ByRef"/> and deserialize the object anyway.</param>
		/// <returns></returns>
		public object Deserialize(XElement element, TypeSerializationSpecification spec, object instance = null, bool forceReadRef = false)
		{
			try
			{
				if (spec.IsCollection)
				{
					// todo: preserve reference equality
					instance = CollectionSerializer.DeserializeCollection
						(this, element, spec as CollectionTypeSpecification);
					HandleDeserialized(spec, instance);
					return instance;
				}

				Dictionary<string, string> attributes = element.Attributes().XAttributeToDictionary();
				if (attributes.TryGetValue(TypeAttributeName, out string realTypeName)
				 && realTypeName != spec.SerializedName)
				{
					try
					{
						var realType = graphSpec.GetTypeFromName(spec.Type, realTypeName);
						var realSpec = graphSpec.GetSpec(realType);


						spec = realSpec;
					}
					catch (Exception e)
					{
						throw new UnknownTypeException
							(
							 $"Attempting to Deserialize unknown type '{realTypeName}' into specified type '{spec.SerializedName}'."
						   + $" You may need to manually add a {nameof(TypeSerializationSpecification)} for '{realTypeName}' to the {nameof(GraphSerializationSpecification)} that was passed to the {nameof(Serializer)}."
						   , e
							);
					}
				}

				if (attributes.TryGetValue(IsNullAttributeName, out string isNullValue))
				{
					if (bool.Parse(isNullValue))
					{
						return null;
					}
				}

				if (null != spec.OverrideDeserialize)
				{
					instance = spec.OverrideDeserialize(this, element, attributes, instance);

					HandleDeserialized(spec, instance);
					return instance;
				}


				if (spec.IsPrimitive)
				{
					return DeserializeFromAttribute(attributes, spec);
				}

				if (spec.ByRef && ! forceReadRef)
				{
					uint refId = uint.Parse(attributes[RefIdAttributeName]);

					if (! refIdToInstanceMap[spec.Type].TryGetValue(refId, out instance))
					{
						instance = spec.Create(context);
						refIdToInstanceMap[spec.Type].Add(refId, instance);
					}

					return instance;
				}

				instance = instance ?? spec.Create(context);

				foreach (var member in spec.Members)
				{
					var memberTypeSpec = graphSpec.TypeSpecifications[member.ValueType];
					if (member.AsAttribute)
					{
						object value = DeserializeFromAttribute(attributes, member);
						member.SetValue(instance, value);
					}
					else
					{
						var elem = element.Element(member.SerializedName);

						if (elem == null)
						{
							if (member.Optional)
							{
								member.SetValue(instance, member.DefaultValue);
								continue;
							}

							throw new MissingDataException
								(
								 $"Missing element '{member.SerializedName}' to deserialize type '{memberTypeSpec.RealName}' for type '{spec.RealName}'."
							   , element
								);
						}

						object value = Deserialize(elem, graphSpec.TypeSpecifications[member.ValueType]);
						member.SetValue(instance, value);
					}
				}

				spec.OnMembersDeserialized?.Invoke(instance, this, element, attributes);
				HandleDeserialized(spec, instance);
				return instance;
			}
			catch (Exception e)
			{
				throw new XmlDataException(element, e);
			}
		}

		/// <summary>
		/// Deserialize a Primitive Type from a <paramref name="attributes"/> using <paramref name="spec"/> to find the <see cref="TypeSerializationSpecification"/> and attributeName
		/// </summary>
		/// <param name="attributes"></param>
		/// <param name="spec"></param>
		/// <returns></returns>
		public object DeserializeFromAttribute(Dictionary<string, string> attributes, MemberSerializationSpecification spec)
		{
			var typeSpec = graphSpec.GetSpec(spec.ValueType);

			if (attributes.TryGetValue(spec.SerializedName, out string result))
			{
				return DeserializeFromAttribute(result, typeSpec);
			}
			if (spec.Optional)
			{
				return spec.DefaultValue;
			}

			throw new MissingDataException(
				$"Missing required attribute '{spec.SerializedName}' for member '{spec.RealName}' for type '{spec.ParentSpecification.RealName}'");
		}

		/// <summary>
		/// Deserialize a Primitive Type from a standalone element's attributes.
		/// </summary>
		/// <param name="attributes"></param>
		/// <param name="spec"></param>
		/// <returns></returns>
		public object DeserializeFromAttribute(Dictionary<string, string> attributes, TypeSerializationSpecification spec)
		{
			string attributeValue = attributes[PrimitiveValueAttributeName];
			return DeserializeFromAttribute(attributeValue, spec);
		}

		/// <summary>
		/// Deserialize a primitive type from a string.
		/// </summary>
		/// <param name="data"></param>
		/// <param name="spec"></param>
		/// <returns></returns>
		public object DeserializeFromAttribute(string data, TypeSerializationSpecification spec)
		{
			// todo: we don't invoke OnDeserializing here, but is that even needed for primitive types?
			var primSpec = spec as PrimitiveSpec;
			if (primSpec == null)
			{
				if (spec.Type.IsGenericType)
				{
					var g = spec.Type.GetGenericTypeDefinition();
					if (g == typeof(Nullable<>))
					{
						var realType = spec.Type.GetGenericArguments()[0];

						var realTypeSpec = graphSpec.TypeSpecifications[realType];
						return DeserializeFromAttribute(data, realTypeSpec);
					}
				}

				throw new ArgumentException($"Expected a {nameof(PrimitiveSpec)}. But the received spec was not: {spec} ", nameof(spec));
			}

			object instance = primSpec.FromString(data);

			spec.OnDeserialized?.Invoke(instance, context);

			if (spec.OnGraphDeserialized != null)
			{
				graphDeserializedEventReceivers.Add(instance);
			}

			return instance;
		}

		#endregion Deserialize Implementation
	}
}
