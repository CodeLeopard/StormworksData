﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CustomSerialization
{
	public class ByRefContainer
	{
		private uint nextID = 0;
		private uint NextID => (uint)nextID++;

		public int Count => ReCount();


		private DictionaryOfContainer<Type, Dictionary<object, uint>> objectToId = new DictionaryOfContainer<Type, Dictionary<object, uint>>();

		public uint GetOrAdd(Type key, object instance)
		{
			var values = objectToId[key];

			if (values.TryGetValue(instance, out uint id))
			{
				return id;
			}

			id = NextID;

			values.Add(instance, id);
			return id;
		}

		public void Clear()
		{
			foreach (var dict in objectToId.Values)
			{
				dict.Clear();
			}
			objectToId.Clear();
			nextID = 0;
		}


		public bool TryGet(Type key, object instance, out uint id)
		{
			var values = objectToId[key];

			return values.TryGetValue(instance, out id);
		}



		private int ReCount()
		{
			int counter = 0;
			foreach (var dict in objectToId.Values)
			{
				counter += dict.Count;
			}

			return counter;
		}

		/// <summary>
		/// Returns all object, ID pairs for the given <see cref="Type"/> <paramref name="key"/>.
		/// </summary>
		/// <param name="key"></param>
		/// <returns></returns>
		public IEnumerable<KeyValuePair<object, uint>> Values(Type key)
		{
			var values = objectToId[key];

			foreach (var elem in values.OrderBy(kvp => kvp.Value))
			{
				yield return elem;
			}
		}

		public IEnumerable<object> AllObjects()
		{
			foreach (var dict in objectToId.Values)
			{
				foreach (object instance in dict.Keys)
				{
					yield return instance;
				}
			}
		}
	}
}
