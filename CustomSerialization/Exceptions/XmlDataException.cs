﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Linq;

using Shared.Extensions;

namespace CustomSerialization.Exceptions
{
	public class XmlDataException : InvalidDataException
	{

		/// <summary>
		/// The element that contained incorrect data, may be null if unknown.
		/// </summary>
		public readonly XElement element;

		public XmlDataException() { }


		public XmlDataException(string message) : base(message) { }

		public XmlDataException(string message, XElement element) : base(message)
		{
			this.element = element;
		}

		public XmlDataException(string message, Exception innerException) : base(message, innerException) { }

		/// <summary>
		/// Get a new <see cref="XmlDataException"/> with auto formatted message like:
		/// $"{innerException.GetType().Name} occurred getting data from {element.Name} at {element.GetLineInfoString()}".
		/// </summary>
		/// <param name="element"></param>
		/// <param name="innerException"></param>
		public XmlDataException(XElement element, Exception innerException)
			: base
				(
				 $"{innerException.GetType().Name} occurred getting data from {element.Name} at {element.GetLineInfoString()}"
			   , innerException
				)
		{
			this.element = element;
		}

		/// <summary>
		/// Get a new <see cref="XmlDataException"/> with auto formatted message like:
		/// $"{innerException.GetType().Name} occurred getting '{dataName}' from {element.Name} at {element.GetLineInfoString()}".
		/// </summary>
		/// <param name="dataName">The name of the DataType to put in the message</param>
		/// <param name="element"></param>
		/// <param name="innerException"></param>
		public static XmlDataException AutoFormatDataName(string dataName, XElement element, Exception innerException)
		{
			return new XmlDataException
				(
				 $"{innerException.GetType().Name} occurred getting '{dataName}' from {element.Name} at {element.GetLineInfoString()}"
			   , innerException
				);
		}

		public XmlDataException(string message, XElement element, Exception innerException) : base
			(message, innerException)
		{
			this.element = element;
		}

		protected XmlDataException(SerializationInfo info, StreamingContext context) : base(info, context) { }
	}
}
