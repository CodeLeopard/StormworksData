﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Linq;

namespace CustomSerialization.Exceptions
{
	public class MissingDataException : XmlDataException
	{
		public MissingDataException()
		{
		}

		public MissingDataException(string message) : base(message)
		{
		}

		public MissingDataException(string message, Exception innerException) : base(message, innerException)
		{
		}

		public MissingDataException(string message, XElement element) : base(message, element)
		{
		}

		public MissingDataException(XElement element, Exception innerException) : base(element, innerException)
		{
		}

		public MissingDataException(string message, XElement element, Exception innerException) : base(message, element, innerException)
		{
		}

		protected MissingDataException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
