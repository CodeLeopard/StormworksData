﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace CustomSerialization
{
	public class DictionaryOfContainer<TKey, TValue> : IDictionary<TKey, TValue> where TValue : new()
	{
		private readonly Dictionary<TKey, TValue> containers = new Dictionary<TKey, TValue>();


		/// <inheritdoc />
		public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
		{
			return containers.GetEnumerator();
		}

		/// <inheritdoc />
		IEnumerator IEnumerable.GetEnumerator()
		{
			return ((IEnumerable)containers).GetEnumerator();
		}

		

		/// <inheritdoc />
		public void Clear()
		{
			containers.Clear();
		}

		/// <inheritdoc />
		public void Add(KeyValuePair<TKey, TValue> item)
		{
			throw new NotSupportedException();
		}

		/// <inheritdoc />
		public bool Contains(KeyValuePair<TKey, TValue> item)
		{
			throw new NotSupportedException();
		}

		/// <inheritdoc />
		public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
		{
			throw new NotSupportedException();
		}

		/// <inheritdoc />
		public bool Remove(KeyValuePair<TKey, TValue> item)
		{
			throw new NotSupportedException();
		}

		/// <inheritdoc />
		public int Count => containers.Count;

		/// <inheritdoc />
		public bool IsReadOnly => false;

		/// <inheritdoc />
		public void Add(TKey         key, TValue value)
		{
			containers.Add(key, value);
		}

		/// <inheritdoc />
		public bool ContainsKey(TKey key)
		{
			return containers.ContainsKey(key);
		}

		/// <inheritdoc />
		public bool Remove(TKey      key)
		{
			return containers.Remove(key);
		}

		/// <inheritdoc />
		public bool TryGetValue(TKey key, out TValue value)
		{
			return containers.TryGetValue(key, out value);
		}

		/// <inheritdoc />
		public TValue this[TKey      key]
		{
			get
			{
				if (containers.TryGetValue(key, out var container))
				{
					return container;
				}

				container = new TValue();
				containers.Add(key, container);
				return container;
			}

			set => containers[key] = value;
		}

		/// <inheritdoc />
		public ICollection<TKey> Keys => containers.Keys;

		/// <inheritdoc />
		public ICollection<TValue> Values => containers.Values;
	}
}
