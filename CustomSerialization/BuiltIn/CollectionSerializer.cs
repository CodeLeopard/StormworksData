﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;

using CustomSerialization.Specification;

namespace CustomSerialization.BuiltIn
{
	public static class CollectionSerializer
	{
		public const string KeyValuePairName = "KeyValuePair";
		public const string KeyValueKeyName = "Key";
		public const string KeyValueValueName = "Value";


		public static bool IsSupported(Type type, SpecGenerator.SpecGeneratorSettings settings, out CollectionTypeSpecification spec)
		{
			spec = null;
			if (type.IsGenericType)
			{
				var g = type.GetGenericTypeDefinition();

				if (g == typeof(List<>) || g == typeof(Dictionary<,>)) // || g == typeof(HashSet<>) || g == typeof(Array)
				{
					spec = new CollectionTypeSpecification(type, settings);
					return true;
				}
			}

			return false;
		}





		public static void SerializeCollection
			(Serializer serializer, object instance, XElement parent)
		{
			var collectionType = instance.GetType();
			var genericType = collectionType.GetGenericTypeDefinition();

			if (genericType == typeof(List<>))
			{
				SerializeList(serializer, instance, parent);
			}
			else if (genericType == typeof(Dictionary<,>))
			{
				SerializeDictionary(serializer, instance, parent);
			}
			else
			{
				throw new NotSupportedException($"Type {collectionType} is not implemented yet.");
			}
		}


		private static void SerializeList(Serializer serializer, object instance, XElement parent)
		{
			var collectionType = instance.GetType();
			if (collectionType.GetGenericTypeDefinition() != typeof(List<>))
			{
				throw new ArgumentException("This method expects a type of: List`1.");
			}
			var elemType = collectionType.GetGenericArguments()[0];
			var elementSpec = serializer.graphSpec.TypeSpecifications[elemType];

			var collection = instance as ICollection;

			foreach (object o in collection)
			{
				XElement e = new XElement(elementSpec.SerializedName);
				parent.Add(e);

				if (o == null)
				{
					e.SetAttributeValue(Serializer.IsNullAttributeName, true);
					continue;
				}

				serializer.SerializeType(o, elementSpec, e);
			}
		}

		private static void SerializeDictionary(Serializer serializer, object instance, XElement parent)
		{
			var collectionType = instance.GetType();
			if (collectionType.GetGenericTypeDefinition() != typeof(Dictionary<,>))
			{
				throw new ArgumentException("This method expects a type of: Dictionary`2.");
			}
			var keyType = collectionType.GetGenericArguments()[0];
			var valueType = collectionType.GetGenericArguments()[1];

			var keySpec   = serializer.graphSpec.TypeSpecifications[keyType];
			var valueSpec = serializer.graphSpec.TypeSpecifications[valueType];

			var collection = instance as IDictionary;

			foreach (DictionaryEntry entry in collection)
			{
				var e = new XElement(KeyValuePairName);
				parent.Add(e);

				var k = new XElement(KeyValueKeyName);
				e.Add(k);
				serializer.SerializeType(entry.Key, keySpec, k);

				if (entry.Value == null)
				{
					e.SetAttributeValue(Serializer.IsNullAttributeName, true);
				}
				else
				{
					var v = new XElement(KeyValueValueName);
					e.Add(v);
					serializer.SerializeType(entry.Value, valueSpec, v);
				}
			}
		}

		public static void RefDiscoverCollection(Serializer serializer, object instance)
		{
			var collectionType = instance.GetType();
			var genericType = collectionType.GetGenericTypeDefinition();

			if (genericType == typeof(List<>))
			{
				RefDiscoverList(serializer, instance);
			}
			else if (genericType == typeof(Dictionary<,>))
			{
				RefDiscoverDictionary(serializer, instance);
			}
		}

		private static void RefDiscoverList(Serializer serializer, object instance)
		{
			var collectionType = instance.GetType();
			if (collectionType.GetGenericTypeDefinition() != typeof(List<>))
			{
				throw new NotSupportedException();
			}
			var elemType = collectionType.GetGenericArguments()[0];
			var elementSpec = serializer.graphSpec.TypeSpecifications[elemType];

			var collection = instance as ICollection;

			foreach (object o in collection)
			{
				if (o == null)
				{
					continue;
				}

				serializer.ExamineByRef(elementSpec, o);
			}
		}

		private static void RefDiscoverDictionary(Serializer serializer, object instance)
		{
			var collectionType = instance.GetType();
			if (collectionType.GetGenericTypeDefinition() != typeof(Dictionary<,>))
			{
				throw new NotSupportedException();
			}
			var keyType = collectionType.GetGenericArguments()[0];
			var valueType = collectionType.GetGenericArguments()[1];

			var keySpec = serializer.graphSpec.TypeSpecifications[keyType];
			var valueSpec = serializer.graphSpec.TypeSpecifications[valueType];

			var collection = instance as IDictionary;

			foreach (DictionaryEntry entry in collection)
			{
				serializer.ExamineByRef(keySpec, entry.Key);

				if (entry.Value != null)
				{
					serializer.ExamineByRef(valueSpec, entry.Value);
				}
			}
		}



		public static object DeserializeCollection(Serializer serializer, XElement element, CollectionTypeSpecification spec)
		{
			var collectionType = spec.Type;
			var genericType = collectionType.GetGenericTypeDefinition();

			if (genericType == typeof(List<>))
			{
				return DeserializeList(serializer, element, spec);
			}
			else if (genericType == typeof(Dictionary<,>))
			{
				return DeserializeDictionary(serializer, element, spec);
			}
			else
			{
				throw new NotSupportedException();
			}
		}

		private static object DeserializeList(Serializer serializer, XElement element, CollectionTypeSpecification spec)
		{
			IList list = (IList)spec.Create(serializer.context);

			var childType = spec.ContainedTypes[0];
			var childSpec = serializer.graphSpec.TypeSpecifications[childType];

			foreach (XElement childElement in element.Elements())
			{
				list.Add(serializer.Deserialize(childElement, childSpec));
			}

			return list;
		}

		private static object DeserializeDictionary(Serializer serializer, XElement element, CollectionTypeSpecification spec)
		{
			IDictionary dict = (IDictionary)spec.Create(serializer.context);

			var keyType = spec.ContainedTypes[0];
			var keySpec = serializer.graphSpec.TypeSpecifications[keyType];

			var valueType = spec.ContainedTypes[1];
			var valueSpec = serializer.graphSpec.TypeSpecifications[valueType];

			foreach (XElement keyValuePairElement in element.Elements())
			{
				// todo: support attributes on the KeyValuePair

				var keyElement = keyValuePairElement.Element(KeyValueKeyName);
				var valueElement = keyValuePairElement.Element(KeyValueValueName);

				dict.Add(serializer.Deserialize(keyElement, keySpec), serializer.Deserialize(valueElement, valueSpec));
			}

			return dict;
		}
	}
}
