﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

using CustomSerialization.Specification;

using OpenToolkit.Mathematics;

using Shared;

namespace CustomSerialization.BuiltIn
{
	public static class OpenTKMathTypes
	{
		internal static Regex MatrixTypeNameRegex = new Regex("Matrix(\\d)(x(\\d))?d?", RegexOptions.Compiled);
		internal static Regex BoxTypeNameRegex = new Regex("Box\\d[di]?",           RegexOptions.Compiled);
		internal static Regex VectorTypeNameRegex = new Regex("Vector\\d[dih]?",    RegexOptions.Compiled);

		private static Regex matrixEntryRegex = new Regex("^M(\\d)(\\d)$",             RegexOptions.Compiled);
		public static TypeSerializationSpecification CreateMatrixSpec(Type type, Match typeNameMatch = null)
		{
			typeNameMatch = typeNameMatch ?? OpenTKMathTypes.MatrixTypeNameRegex.Match(type.Name);
			int expectedRows    = int.Parse(typeNameMatch.Groups[1].Value);
			int expectedColumns;

			if (typeNameMatch.Groups[2].Success)
			{
				expectedColumns = int.Parse(typeNameMatch.Groups[3].Value);
			}
			else
			{
				expectedColumns = expectedRows;
			}

			int expectedTotalValues = expectedRows * expectedColumns;
			int foundValues = 0;


			var spec = new TypeSerializationSpecification(type, true);

			var properties = type.GetProperties(BindingFlags.Public | BindingFlags.Instance);

			foreach (PropertyInfo property in properties)
			{
				var match = matrixEntryRegex.Match(property.Name);
				if(! match.Success) continue;

				var x = int.Parse(match.Groups[1].Value) - 1;
				var y = int.Parse(match.Groups[2].Value) - 1;

				int defaultValue = 0;
				if (x == y)
				{
					// Default is identity matrix.
					defaultValue = 1;
				}

				object boxedDefaultValue = toBuiltInTypeValue[property.PropertyType](defaultValue);

				var member = new MemberSerializationSpecification
					(spec, property, name: $"_{x}{y}", defaultValue: boxedDefaultValue, omitIfDefault: true);
				spec.Members.Add(member);

				foundValues++;
			}

			if (foundValues != expectedTotalValues)
			{
				throw new Exception
					(
					 $"Failed to generate spec for {type.Name}, expected {expectedRows} * {expectedColumns} = {expectedTotalValues}"
				   + $" entries but found {foundValues}."
					);
			}

			return spec;
		}

		public static TypeSerializationSpecification CreateBoxSpec(Type type)
		{
			var spec = new TypeSerializationSpecification(type, true);

			var min = type.GetField("_min",  BindingFlags.NonPublic | BindingFlags.Instance);
			var max = type.GetField("_max", BindingFlags.NonPublic | BindingFlags.Instance);

			var minMem = new MemberSerializationSpecification(spec, min, name: "min") { OmitIfDefault = true };
			var maxMem = new MemberSerializationSpecification(spec, max, name: "max") { OmitIfDefault = true };

			spec.Members.Add(minMem);
			spec.Members.Add(maxMem);


			return spec;
		}


		private static SpecGenerator.SpecGeneratorSettings vectorSettings =
			new SpecGenerator.SpecGeneratorSettings() { AllowUnguided = true, Mode = SpecGenerator.OperatingMode.Automatic};
		public static TypeSerializationSpecification CreateVectorSpec(Type type)
		{
			var spec = SpecGenerator.CreateTypeSpecification(type, vectorSettings);

			foreach (var member in spec.Members)
			{
				member.SerializedName = member.RealName.ToLowerInvariant();
				member.OmitIfDefault = true;
			}


			return spec;
		}

		private static Dictionary<Type, Func<int, object>> toBuiltInTypeValue = new Dictionary<Type, Func<int, object>>();

		static OpenTKMathTypes()
		{
			toBuiltInTypeValue[typeof(float)] = (i) => (float)i;
			toBuiltInTypeValue[typeof(short)] = (i) => (short)i;
			toBuiltInTypeValue[typeof(double)] = (i) => (double)i;
			toBuiltInTypeValue[typeof(decimal)] = (i) => (decimal)i;
		}

		public static void SetStormworksMathTypes(GraphSerializationSpecification spec)
		{
			{
				Type initialType = typeof(Matrix4);
				string initialNamespace = initialType.Namespace;
				foreach (Type type in initialType.Assembly.GetExportedTypes())
				{
					if (type.Namespace != initialNamespace) continue;

					if (OpenTKMathTypes.VectorTypeNameRegex.IsMatch((type.Name)))
					{
						spec.TypeSpecifications[type] = OpenTKMathTypes.CreateVectorSpec(type);
						continue;
					}

					var match = OpenTKMathTypes.MatrixTypeNameRegex.Match(type.Name);
					if (match.Success)
					{
						spec.TypeSpecifications[type] = OpenTKMathTypes.CreateMatrixSpec(type, match);
						continue;
					}

					if (OpenTKMathTypes.BoxTypeNameRegex.IsMatch(type.Name))
					{
						spec.TypeSpecifications[type] = OpenTKMathTypes.CreateBoxSpec(type);
						continue;
					}
				}

				foreach (Type t in new []{typeof(Bounds3), typeof(Bounds3i)})
				{
					spec.TypeSpecifications[t] = OpenTKMathTypes.CreateBoxSpec(t);
				}
			}
		}
	}
}
