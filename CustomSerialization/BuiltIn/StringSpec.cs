﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;

using CustomSerialization.Specification;

namespace CustomSerialization.BuiltIn
{
	public class StringSpec : PrimitiveSpec
	{
		internal StringSpec() : base(typeof(string))
		{
			OverrideDeserialize = Deserialize;
			OverrideSerialize = Serialize;
		}

		/// <inheritdoc />
		public override object FromString(string value)
		{
			return value;
		}

		/// <inheritdoc />
		public override string ValueToString(object instance)
		{
			return (string) instance;
		}


		private object Deserialize(Serializer serializer, XElement element, Dictionary<string, string> attributes, object _ = null)
		{
			// parameter _ represents the instance in case referenceEquality needs to be preserved.
			// however string in C# is interned so we don't need to deal with that.
			// (and it's readonly too so we couldn't even if we wanted to).

			// todo: are there cases where we want to override the behaviour when the serialized value is null?
			// currently this method simply won't get called in this case.

			if (attributes.TryGetValue(Serializer.PrimitiveValueAttributeName, out string value))
			{
				return value;
			}

			return element.Value;
		}

		private void Serialize(Serializer serializer, XElement element, object instance)
		{
			// todo: are there cases where we want to override the behaviour when the instance is null?
			// currently this method simply won't get called in this case.
			/*
			if (instance == null)
			{
				element.SetAttributeValue(Serializer.IsNullAttributeName, true);
				return;
			}
			*/

			string value = (string)instance;

			if (value.Length > Serializer.StringAsContentThreshold)
			{
				element.SetValue(value);
			}
			else
			{
				element.SetAttributeValue(Serializer.PrimitiveValueAttributeName, value);
			}
		}
	}
}
