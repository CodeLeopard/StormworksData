﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Xml.Linq;

namespace CustomSerialization.BuiltIn
{
	public class PrimitiveSpec : BuiltInTypeSpec
	{


		internal delegate object PrimitiveDeserializerDelegate(string value);

		private readonly PrimitiveDeserializerDelegate primitiveDeserializer;

		protected PrimitiveSpec(Type type) : base(type)
		{
			IsPrimitive = true;
		}

		internal PrimitiveSpec(Type type, PrimitiveDeserializerDelegate deserializer, Func<object, IFormatProvider, string> toString = null) : this(type)
		{
			primitiveDeserializer = deserializer;

			FindSpecialMethods();

			if (null != toString)
			{
				ToStringWithFormatProvider = toString;
			}
		}

		public virtual object FromString(string value)
		{
			return primitiveDeserializer(value);
		}

		public virtual string ValueToString(object instance)
		{
			if (ToStringWithFormatProvider != null)
			{
				return ToStringWithFormatProvider.Invoke(instance, CultureInfo.InvariantCulture);
			}
			else
			{
				return instance.ToString();
			}
		}
	}
}
