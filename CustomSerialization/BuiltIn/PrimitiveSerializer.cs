﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace CustomSerialization.BuiltIn
{
	static class PrimitiveSerializer
	{
		internal static object ParseByte(string s) => (object) byte.Parse(s,       CultureInfo.InvariantCulture);
		internal static object ParseSByte(string s) => (object) sbyte.Parse(s,     CultureInfo.InvariantCulture);
		internal static object ParseUInt16(string s) => (object) UInt16.Parse(s,   CultureInfo.InvariantCulture);
		internal static object ParseInt16(string s) => (object) Int16.Parse(s,     CultureInfo.InvariantCulture);
		internal static object ParseUInt32(string s) => (object) UInt32.Parse(s,   CultureInfo.InvariantCulture);
		internal static object ParseInt32(string s) => (object) Int32.Parse(s,     CultureInfo.InvariantCulture);
		internal static object ParseUInt64(string s) => (object) UInt64.Parse(s,   CultureInfo.InvariantCulture);
		internal static object ParseInt64(string s) => (object) Int64.Parse(s,     CultureInfo.InvariantCulture);
		internal static object ParseFloat(string s) => (object) float.Parse(s,    CultureInfo.InvariantCulture);
		internal static object ParseDouble(string s) => (object) double.Parse(s,   CultureInfo.InvariantCulture);
		internal static object ParseDecimal(string s) => (object) decimal.Parse(s, CultureInfo.InvariantCulture);
		internal static object ParseChar(string s) => (object) char.Parse(s);
		internal static object ParseBool(string s) => (object) bool.Parse(s);
	}
}
