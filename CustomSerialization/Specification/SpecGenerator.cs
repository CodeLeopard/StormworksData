﻿// Copyright 2022-2025 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Linq;

using CustomSerialization.Attributes;
using CustomSerialization.BuiltIn;
using CustomSerialization.Exceptions;

namespace CustomSerialization.Specification
{
	public static class SpecGenerator
	{
		public enum OperatingMode : byte
		{
			Automatic = 0,
			ExplicitAttributes = 1,
			DataContractAttributes = 2,
		}


		public class SpecGeneratorSettings
		{
			public OperatingMode Mode = OperatingMode.Automatic;


			/// <summary>
			/// When <see cref="Mode"/> is <see cref="OperatingMode.Automatic"/>: allow selecting <see cref="SerializationAttribute"/> to create a specification with.
			/// </summary>
			public bool AllowCustomSerializerAttributes = true;

			/// <summary>
			/// When <see cref="Mode"/> is <see cref="OperatingMode.Automatic"/>: allow selecting <see cref="DataContractAttribute"/> to create a specification with.
			/// </summary>
			public bool AllowDataContractAttributes = true;

			/// <summary>
			/// When <see cref="Mode"/> is <see cref="OperatingMode.Automatic"/>: generating a specification if <see cref="SerializableAttribute"/> is on the <see cref="Type"/>.
			/// </summary>
			public bool AllowSerializableAttribute = true;

			/// <summary>
			/// When <see cref="Mode"/> is <see cref="OperatingMode.Automatic"/>: generating a specification for a <see langword="class"/> even when there is no attribute of any kind on the <see cref="Type"/> to offer guidance.
			/// </summary>
			public bool AllowUnguided = false;

			/// <summary>
			/// Use a parameterless constructor if possible.
			/// </summary>
			public bool UseParameterLessConstructor = false;

			/// <summary>
			/// Call the method with the <see cref="PatchSerializationSpecAttribute"/>.
			/// If false it will be ignored.
			/// </summary>
			public bool CallPatchMethod = true;
		}


		public static SpecGeneratorSettings DefaultSettings => new SpecGeneratorSettings();


		private static readonly Dictionary<Type, TypeSerializationSpecification> BuiltInTypes =
			new Dictionary<Type, TypeSerializationSpecification>();


		internal static void AddBuiltin(TypeSerializationSpecification spec)
		{
			BuiltInTypes.Add(spec.Type, spec);
		}

		private static string ToStringNoExponentNotationFloat(object instance, IFormatProvider provider)
		{
			float value = (float)instance;
			string attempt1 = value.ToString(provider);
			if (! attempt1.Contains("E"))
			{
				return attempt1;
			}
			return ((float)value).ToString("0.##########", provider);
		}

		private static string ToStringNoExponentNotationDouble(object value, IFormatProvider provider)
		{
			return ((double)value).ToString("F99", provider).TrimEnd('0');
		}

		static SpecGenerator()
		{
			AddBuiltin(new StringSpec());
			AddBuiltin(new PrimitiveSpec(typeof(Byte),     PrimitiveSerializer.ParseByte));
			AddBuiltin(new PrimitiveSpec(typeof(SByte),    PrimitiveSerializer.ParseSByte));
			AddBuiltin(new PrimitiveSpec(typeof(UInt16),   PrimitiveSerializer.ParseUInt16));
			AddBuiltin(new PrimitiveSpec(typeof(Int16),    PrimitiveSerializer.ParseInt16));
			AddBuiltin(new PrimitiveSpec(typeof(UInt32),   PrimitiveSerializer.ParseUInt32));
			AddBuiltin(new PrimitiveSpec(typeof(Int32),    PrimitiveSerializer.ParseInt32));
			AddBuiltin(new PrimitiveSpec(typeof(UInt64),   PrimitiveSerializer.ParseUInt64));
			AddBuiltin(new PrimitiveSpec(typeof(Int64),    PrimitiveSerializer.ParseInt64));
			AddBuiltin(new PrimitiveSpec(typeof(float),    PrimitiveSerializer.ParseFloat,  ToStringNoExponentNotationFloat));
			AddBuiltin(new PrimitiveSpec(typeof(double),   PrimitiveSerializer.ParseDouble, ToStringNoExponentNotationDouble));
			AddBuiltin(new PrimitiveSpec(typeof(decimal),  PrimitiveSerializer.ParseDecimal));
			AddBuiltin(new PrimitiveSpec(typeof(char),     PrimitiveSerializer.ParseChar));
			AddBuiltin(new PrimitiveSpec(typeof(bool),     PrimitiveSerializer.ParseBool, (o, provider) => o.ToString().ToLowerInvariant()));

			// todo: add handler for generics such as List<T> and Dictionary<T1, T2>
		}


		/// <summary>
		/// Generates a full <see cref="GraphSerializationSpecification"/> by inspecting the members of <paramref name="rootType"/> in accordance with <paramref name="settings"/>.
		/// Note that only statically discoverable <see cref="Type"/>s will be considered. Implementations of abstract types or interfaces need to be added manually or (de)serialization will fail.
		/// </summary>
		/// <param name="rootType"></param>
		/// <param name="settings"></param>
		/// <returns></returns>
		public static GraphSerializationSpecification CreateGraphSerializationSpecification(Type rootType, SpecGeneratorSettings settings = null)
		{
			if (null == rootType) throw new ArgumentNullException(nameof(rootType));

			settings = settings ?? DefaultSettings;

			var typeSpecs = new Dictionary<Type, TypeSerializationSpecification>();

			var todoSpecs = new HashSet<TypeSerializationSpecification>();
			var todoTypes = new HashSet<Type>();
			todoTypes.Add(rootType);

			bool anyNew = true;
			while (anyNew)
			{
				anyNew = false;
				foreach (Type t in todoTypes)
				{
					var spec = CreateTypeSpecification(t, settings);

					todoSpecs.Add(spec);
					typeSpecs.Add(t, spec);
				}
				todoTypes.Clear();


				foreach (var spec in todoSpecs)
				{
					foreach (Type knownType in spec.introducedTypes)
					{
						if (typeSpecs.ContainsKey(knownType)) continue;
						todoTypes.Add(knownType);

						anyNew = true;
					}

					foreach (var member in spec.Members)
					{
						if(typeSpecs.ContainsKey(member.ValueType)) continue;

						todoTypes.Add(member.ValueType);

						anyNew = true;
					}

					if (spec is CollectionTypeSpecification col && col.ContainedTypes != null && col.ContainedTypes.Length > 0)
					{
						foreach (Type containedType in col.ContainedTypes)
						{
							if (typeSpecs.ContainsKey(containedType)) continue;

							todoTypes.Add(containedType);

							anyNew = true;
						}
					}
				}
				todoSpecs.Clear();
			}

			return new GraphSerializationSpecification(rootType, typeSpecs);
		}



		/// <summary>
		/// Generate a <see cref="TypeSerializationSpecification"/> for <paramref name="type"/>.
		/// Note that this Method merely selects one of the other Methods based on the <see cref="Attribute"/>s it finds on <paramref name="type"/> and what is specified in <paramref name="settings"/>.
		/// </summary>
		/// <param name="type">The <see cref="Type"/> to generate a specification for</param>
		/// <param name="settings">Optional settings for the generation.</param>
		/// <returns>A <see cref="TypeSerializationSpecification"/> for <paramref name="type"/></returns>
		public static TypeSerializationSpecification CreateTypeSpecification(Type type, SpecGeneratorSettings settings = null)
		{
			if (null == type) throw new ArgumentNullException(nameof(type));
			settings = settings ?? DefaultSettings;

			if (CommonBuiltInSpecs(type, settings, out var builtInSpec))
			{
				return builtInSpec;
			}

			if (settings.Mode == OperatingMode.ExplicitAttributes)
			{
				return CreateSpecificationFromSerializerAttributes(type, settings);
			}
			else if (settings.Mode == OperatingMode.DataContractAttributes)
			{
				return CreateSpecificationFromDataContractAttributes(type, settings);
			}
			else if (settings.Mode != OperatingMode.Automatic)
			{
				throw new ArgumentOutOfRangeException($"Unexpected {nameof(OperatingMode)} '{settings.Mode}'.");
			}

			if (settings.AllowCustomSerializerAttributes
			 && type.GetCustomAttribute<SerializerExplicitAttribute>() != null)
			{
				return CreateSpecificationFromSerializerAttributes(type);
			}

			if (settings.AllowDataContractAttributes
			 && type.GetCustomAttribute<DataContractAttribute>() != null)
			{
				return CreateSpecificationFromDataContractAttributes(type, settings);
			}

			if (settings.AllowSerializableAttribute
			 && type.GetCustomAttribute<SerializableAttribute>() != null)
			{
				return CreateImplicitSpecification(type, settings);
			}

			if (!settings.AllowUnguided)
			{
				throw new InvalidOperationException
					(
					 $"Found no suitable way to generate a {nameof(TypeSerializationSpecification)} for '{type.Name}'. Check what is or isn't allowed by {nameof(settings)}."
					);
			}

			return CreateImplicitSpecification(type, settings);
		}

		internal static bool CommonBuiltInSpecs(Type type, SpecGenerator.SpecGeneratorSettings settings, out TypeSerializationSpecification spec)
		{
			if (BuiltInTypes.TryGetValue(type, out spec))
			{
				return true;
			}
			if (type.IsInterface)
			{
				spec = new TypeSerializationSpecification(type, settings);
				return true;
			}
			if (CollectionSerializer.IsSupported(type, settings, out var colSpec))
			{
				spec = colSpec;
				return true;
			}
			if (PrimitiveSpec(type, settings, out spec))
			{
				return true;
			}

			return false;
		}

		internal static bool PrimitiveSpec(Type type, SpecGenerator.SpecGeneratorSettings settings, out TypeSerializationSpecification spec)
		{
			spec = null;
			if (type.IsPrimitive)
			{
				throw new UnknownTypeException($"Unknown Primitive Type '{type.FullName}'");
			}

			return false;
		}


		public static bool ImplicitIgnore(MemberInfo member)
		{
			if (member.GetCustomAttribute<SerializerIgnoreAttribute>() != null) return true;
			if (member.GetCustomAttribute<NonSerializedAttribute>() != null) return true;

			return false;
		}


		private static Regex IsBackingFieldName = new Regex
			("^<(?:((?!\\d)\\w+(?:\\.(?!\\d)\\w+)*)\\.)?((?!\\d)\\w+)>k__BackingField$", RegexOptions.Compiled);

		public static readonly Dictionary<Type, GraphSerializationSpecification> GraphSpecifications = new Dictionary<Type, GraphSerializationSpecification>();
		public static readonly Dictionary<Type, TypeSerializationSpecification> TypeSpecifications = new Dictionary<Type, TypeSerializationSpecification>();


		internal static string SafeMemberName(string name)
		{
			// todo: make this 100% safe wrt similar names, including of both the property and backing field etc.
			return name.Replace('<', '_').Replace('>', '_');
		}

		#region Specific Implementations

		/// <summary>
		/// Best-Guess our way to a Specification.
		/// </summary>
		/// <param name="type"></param>
		/// <param name="settings"></param>
		/// <returns></returns>
		/// <exception cref="ArgumentNullException"></exception>
		/// <exception cref="UnknownTypeException"></exception>
		public static TypeSerializationSpecification CreateImplicitSpecification
			(Type type, SpecGeneratorSettings settings = null)
		{
			if (null == type) throw new ArgumentNullException(nameof(type));
			settings = settings ?? DefaultSettings;

			if (CommonBuiltInSpecs(type, settings, out var builtInSpec))
			{
				return builtInSpec;
			}

			bool isValueType = type.IsValueType;

			var specification = new TypeSerializationSpecification(type, settings);
			if (type.IsPrimitive)
			{
				throw new UnknownTypeException($"Unknown Primitive Type '{type.FullName}'");
			}


			specification.Explicit = false;
			var members = specification.Members;


			if (true)
			{
				foreach (var field in type.GetFields
					(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic))
				{
					if (field.IsSpecialName) continue;
					if (ImplicitIgnore(field)) continue;

					// todo: explicit include informations

					members.Add(new MemberSerializationSpecification(specification, field));
				}
			}

			if (false)
			{
				foreach (var property in type.GetProperties
					(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic))
				{
					if (!property.CanWrite) continue; // If we can't write it's value there is no point in saving it.
					if (property.IsSpecialName) continue;
					if (property.GetIndexParameters().Length != 0) continue; // Indexer.
					if (ImplicitIgnore(property)) continue;



					// todo: explicit include informations

					members.Add(new MemberSerializationSpecification(specification, property));
				}
			}

			return specification;
		}

		/// <summary>
		/// Create a specification by only considering the attributes that derive from <see cref="SerializationAttribute"/>.
		/// </summary>
		/// <param name="type"></param>
		/// <param name="settings"></param>
		/// <returns></returns>
		/// <exception cref="ArgumentNullException"></exception>
		/// <exception cref="InvalidOperationException"></exception>
		public static TypeSerializationSpecification CreateSpecificationFromSerializerAttributes(Type type, SpecGeneratorSettings settings = null)
		{
			if (null == type) throw new ArgumentNullException(nameof(type));

			settings = settings ?? DefaultSettings;

			if (CommonBuiltInSpecs(type, settings, out var builtInSpec))
			{
				return builtInSpec;
			}

			// todo: throw if attribute missing

			var explicitAttr = type.GetCustomAttribute<SerializerExplicitAttribute>() ?? throw new InvalidOperationException($"Missing required type attribute {nameof(SerializerExplicitAttribute)}");
			var knownTypesAttr = type.GetCustomAttribute<SerializerKnownTypeAttribute>();




			var specification = new TypeSerializationSpecification(type, settings, typeNameOverride: explicitAttr.XmlTypeName);
			specification.Explicit = true;


			if (null != knownTypesAttr)
			{
				specification.introducedTypes.AddRange(knownTypesAttr.KnownTypes);
			}


			var members = specification.Members;


			foreach (var property in type.GetProperties
				(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic))
			{
				if (!property.CanWrite) continue; // If we can't write it's value there is no point in saving it.
				if (property.IsSpecialName) continue;

				var attr = property.GetCustomAttribute<SerializerIncludeAttribute>();
				if (null == attr) continue;

				members.Add(new MemberSerializationSpecification(specification, property, attr));
			}


			foreach (var field in type.GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic))
			{
				if (field.IsSpecialName) continue;

				var attr = field.GetCustomAttribute<SerializerIncludeAttribute>();
				if (null == attr) continue;

				members.Add(new MemberSerializationSpecification(specification, field, attr));
			}

			var method = type
				.GetMethods(BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic)
				.Where(m => m.GetCustomAttribute<PatchSerializationSpecAttribute>() != null)
				.FirstOrDefault()
				;

			if (method != null && settings.CallPatchMethod)
			{
				if (method.ReturnType != typeof(void))
				{
					throw new InvalidOperationException($"Method '{method.Name}' decoreated with {nameof(PatchSerializationSpecAttribute)} should return void.");
				}

				if (!method.IsStatic)
				{
					throw new InvalidOperationException($"Method '{method.Name}' decoreated with {nameof(PatchSerializationSpecAttribute)} should be static.");
				}

				var parameters = method.GetParameters();

				if (parameters.Length != 1 || parameters[0].ParameterType != typeof(TypeSerializationSpecification))
				{
					throw new InvalidOperationException($"Method '{method.Name}' decoreated with {nameof(PatchSerializationSpecAttribute)} should take a single parameter of type {nameof(TypeSerializationSpecification)}.");
				}

				method.Invoke(null, new[] { specification });
			}

			return specification;
		}

		public static TypeSerializationSpecification CreateSpecificationFromDataContractAttributes(Type type, SpecGeneratorSettings settings = null)
		{
			if (null == type) throw new ArgumentNullException(nameof(type));

			settings = settings ?? DefaultSettings;


			if (CommonBuiltInSpecs(type, settings, out var builtInSpec))
			{
				return builtInSpec;
			}

			// todo: throw if attribute missing
			// todo: look for CustomSerializerAttributes on the type if allowed.

			var specification = new TypeSerializationSpecification(type, settings);
			specification.Explicit = true;
			var members = specification.Members;

			foreach (var property in type.GetProperties
				(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic))
			{
				if (!property.CanWrite) continue; // If we can't write its value there is no point in saving it.
				if (property.IsSpecialName) continue;

				if (settings.AllowCustomSerializerAttributes)
				{
					var attr = property.GetCustomAttribute<SerializerIncludeAttribute>();
					if (attr != null)
					{
						members.Add(new MemberSerializationSpecification(specification, property, attr));
						continue;
					}
				}

				{
					var attr = property.GetCustomAttribute<DataMemberAttribute>();
					if (null == attr) continue;

					members.Add(new MemberSerializationSpecification(specification, property, name: attr.Name, optional: !attr.IsRequired));
				}
			}


			foreach (var field in type.GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic))
			{
				if (field.IsSpecialName) continue;

				if (settings.AllowCustomSerializerAttributes)
				{
					var attr = field.GetCustomAttribute<SerializerIncludeAttribute>();
					if (attr != null)
					{
						members.Add(new MemberSerializationSpecification(specification, field, attr));
						continue;
					}
				}

				{
					var attr = field.GetCustomAttribute<DataMemberAttribute>();
					if (null == attr) continue;

					members.Add(new MemberSerializationSpecification(specification, field, name: attr.Name));
				}
			}

			return specification;
		}

		#endregion Specific Implementations
	}
}
