﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Linq;

using CustomSerialization.Attributes;
using CustomSerialization.Specification;

namespace CustomSerialization
{
	/// <summary>
	/// Specification for how a <see cref="Type"/> should be serialized.
	/// </summary>
	public class TypeSerializationSpecification
	{
		public readonly Type Type;
		public string SerializedName;
		public string RealName => Type.Name;
		public readonly bool IsBuiltIn;


		public bool Explicit { get; internal set; }

		public bool IsCollection { get; protected set; }

		public bool IsPrimitive { get; protected set; }

		/// <summary>
		/// Signals that this Type should be serialized by reference no matter where it is encountered.
		/// </summary>
		public bool ByRef { get; set; }

		public readonly List<MemberSerializationSpecification> Members = new List<MemberSerializationSpecification>();

		/// <summary>
		/// List of known types that this type introduced.
		/// </summary>
		internal readonly List<Type> introducedTypes = new List<Type>();

		/// <summary>
		/// Set of types that derive from this type.
		/// </summary>
		internal readonly HashSet<Type> knownDerivatives = new HashSet<Type>();

		/// <summary>
		/// The parameterless constructor used to create an instance, or <see langword='null'/>
		/// if it does not exist or should not be used.
		/// </summary>
		public readonly ConstructorInfo Constructor;

		/// <summary>
		/// The action to call on the instance before Serializing the instance, or <see langword='null'/>.
		/// </summary>
		public Action<object, StreamingContext> OnSerializing;

		/// <summary>
		/// The action to call on the instance after Serializing the instance, or <see langword='null'/>.
		/// </summary>
		public Action<object, StreamingContext> OnSerialized;

		/// <summary>
		/// The action to call on the uninitialized instance before DeSerializing data into the instance, or <see langword='null'/>.
		/// </summary>
		public Action<object, StreamingContext> OnDeserializing;

		/// <summary>
		/// The action to call on the instance when Deserialization of the object and it's members is complete, or <see langword='null'/>.
		/// Note that when there are cyclic object references those cyclic members may not have been deserialized yet, if you need them use <see cref="OnGraphDeserialized"/> instead.
		/// </summary>
		public Action<object, StreamingContext> OnDeserialized;

		/// <summary>
		/// The action to call on the instance when DeSerializing the entire object graph is complete, or <see langword='null'/>.
		/// </summary>
		public Action<object, object> OnGraphDeserialized;

		/// <summary>
		/// The ToString method that takes a <see cref="IFormatProvider"/> parameter, or <see langword='null'/>.
		/// </summary>
		public Func<object, IFormatProvider, string> ToStringWithFormatProvider;


		internal TypeSerializationSpecification(Type type, bool isBuiltIn = false, string typeNameOverride = null)
		{
			Type = type ?? throw new ArgumentNullException(nameof(type));

			SerializedName = typeNameOverride ?? Type.ToString();

			IsBuiltIn = isBuiltIn;
		}

		public TypeSerializationSpecification(Type type, SpecGenerator.SpecGeneratorSettings settings, string typeNameOverride = null)
			: this (type, settings, forceTryConstructor: false, typeNameOverride: typeNameOverride)
		{

		}

		protected TypeSerializationSpecification(Type type, SpecGenerator.SpecGeneratorSettings settings, bool forceTryConstructor, string typeNameOverride = null)
			: this(type, typeNameOverride: typeNameOverride)
		{

			var constructor = GetConstructor(type);
			var cAttribute = constructor?.GetCustomAttribute<SerializerIncludeAttribute>();
			if (settings.UseParameterLessConstructor || forceTryConstructor || null != cAttribute)
			{
				Constructor = constructor;
			}

			FindSpecialMethods(); // todo: deal with inheritance where multiple levels define special behaviour methods.
		}

		protected ConstructorInfo GetConstructor(Type t)
		{
			var c = t.GetConstructor
				(
				 BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic
			   , null
			   , CallingConventions.Standard | CallingConventions.HasThis
			   , Array.Empty<Type>()
			   , Array.Empty<ParameterModifier>()
				);

#if DEBUG
			if (c == null)
			{
				var all = t.GetConstructors();
				// For debugging
			}
#endif

			return c;
		}

		/// <summary>
		/// Find various special methods related to serialization such as events before and after (de) serialization, and constructors.
		/// </summary>
		protected void FindSpecialMethods()
		{
			var interfaces = Type.GetInterfaces();
			if (interfaces.Contains(typeof(IDeserializationCallback)))
			{
				var method = typeof(IDeserializationCallback).GetMethod
					(nameof(IDeserializationCallback.OnDeserialization));

				OnGraphDeserialized = GetFastInstanceActionInvoker<object>(method);
			}

			// todo: is there a ISerializationCallback ?
			// todo: other interfaces to check.

			foreach (var method in Type.GetMethods
				(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static))
			{
				var parameters = method.GetParameters();
				if (! method.IsStatic
				 && parameters.Length           == 1
				 && parameters[0].ParameterType == typeof(StreamingContext))
				{
					{
						var attr = method.GetCustomAttribute<OnSerializingAttribute>();
						if (attr != null)
						{
							OnSerializing = GetFastInstanceActionInvoker<StreamingContext>(method);
						}
					}
					{
						var attr = method.GetCustomAttribute<OnSerializedAttribute>();
						if (attr != null)
						{
							OnSerialized = GetFastInstanceActionInvoker<StreamingContext>(method);
						}
					}
					{
						var attr = method.GetCustomAttribute<OnDeserializingAttribute>();
						if (attr != null)
						{
							OnDeserializing = GetFastInstanceActionInvoker<StreamingContext>(method);
						}
					}
					{
						var attr = method.GetCustomAttribute<OnDeserializedAttribute>();
						if (attr != null)
						{
							OnDeserialized = GetFastInstanceActionInvoker<StreamingContext>(method);
						}
					}
				}

				if (! method.IsStatic
				 && method.Name                 == nameof(ToString)
				 && method.ReturnType           == typeof(string)
				 && parameters.Length           == 1
				 && parameters[0].ParameterType == typeof(IFormatProvider))
				{
					ToStringWithFormatProvider = GetFastInstanceFuncInvoker<IFormatProvider, string>(method);
				}

				if (method.IsStatic)
				{
					TryFindDelegate<OverrideDeserializeAttribute, DeserializeOverrideDelegate>
						(method, parameters, ref OverrideDeserialize);

					TryFindDelegate<OverrideSerializeAttribute, SerializeOverrideDelegate>
						(method, parameters, ref OverrideSerialize);
				}
				else
				{
					if (TryFindDelegate<AugmentDeserializeAttribute, DeserializeAugmentDelegate>(method, parameters))
					{
						OnMembersDeserialized = new DeserializeAugmentDelegate
							(GetFastInstanceActionInvoker<Serializer, XElement, Dictionary<string, string>>(method));
					}

					if (TryFindDelegate<AugmentSerializeAttribute, SerializeAugmentDelegate>(method, parameters))
					{
						BeforeMembersSerialized = new SerializeAugmentDelegate
							(GetFastInstanceActionInvoker<Serializer, XElement>(method));
					}
				}
			}
		}

		/// <summary>
		/// Check if <paramref name="method"/> is decorated with <typeparamref name="TAttribute"/> and has a signature that matches <typeparamref name="TDelegate"/>.
		/// </summary>
		/// <typeparam name="TAttribute"></typeparam>
		/// <typeparam name="TDelegate"></typeparam>
		/// <param name="method"></param>
		/// <param name="methodParameters"></param>
		/// <returns></returns>
		private bool TryFindDelegate<TAttribute, TDelegate>
			(MethodInfo method, ParameterInfo[] methodParameters)
			where TAttribute : System.Attribute where TDelegate : Delegate
		{
			var attr = method.GetCustomAttribute<TAttribute>();
			if (null == attr) return false;

			var delType = typeof(DeserializeOverrideDelegate);
			var delMethod = delType.GetMethod("Invoke");
			var delParameters = delMethod.GetParameters();

			for (int i = 0; i < Math.Min(methodParameters.Length, delParameters.Length); i++) // todo: find out why this needs to be wrong like this.
			{
				if (methodParameters[i].ParameterType != delParameters[i].ParameterType)
					return false;
			}

			return true;
		}

		/// <summary>
		/// Check if <paramref name="method"/> is decorated with <typeparamref name="TAttribute"/> and has a signature that matches <typeparamref name="TDelegate"/>.
		/// If it does then create a delegate to call that method and assign it to <paramref name="destination"/>.
		/// </summary>
		/// <typeparam name="TAttribute"></typeparam>
		/// <typeparam name="TDelegate"></typeparam>
		/// <param name="method"></param>
		/// <param name="methodParameters"></param>
		/// <param name="destination"></param>
		/// <returns></returns>
		private bool TryFindDelegate<TAttribute, TDelegate>(MethodInfo method, ParameterInfo[] methodParameters, ref TDelegate destination) where TAttribute : System.Attribute where TDelegate : Delegate
		{
			if (! TryFindDelegate<TAttribute, TDelegate>(method, methodParameters))
			{
				return false;
			}

			destination = (TDelegate)Delegate.CreateDelegate(typeof(TDelegate), method);

			return true;
		}

		/// <summary>
		/// Generates a delegate that can call an instance method where the instance is provided as <see cref="object"/>.
		/// </summary>
		/// <param name="method"></param>
		/// <returns></returns>
		private Action<object, TParam0> GetFastInstanceActionInvoker<TParam0>(MethodInfo method)
		{
			// https://stackoverflow.com/questions/2933221/can-you-get-a-funct-or-similar-from-a-methodinfo-object

			// The instance is provided as object because we can't pass it in statically typed.
			var paramInstance = Expression.Parameter(typeof(object),           "instance");

			var param0 = Expression.Parameter(typeof(TParam0), method.GetParameters()[0].Name);

			// Generate an expression like:
			// (object instance, StreamingContext context) => { ((RealInstanceType) instance).TheMethod(param0); }
			var callExpr = Expression.Call(Expression.Convert(paramInstance, method.DeclaringType), method, param0);
			var expression = Expression.Lambda<Action<object, TParam0>>(callExpr, paramInstance, param0);

			return expression.Compile();
		}


		#region Reflection magic
		// todo: move these methods to a separate class

		/// <summary>
		/// Generates a delegate that can call an instance method where the instance is provided as <see cref="object"/>.
		/// </summary>
		/// <param name="method"></param>
		/// <returns></returns>
		private Action<object, TParam0, TParam1> GetFastInstanceActionInvoker<TParam0, TParam1>(MethodInfo method)
		{
			// https://stackoverflow.com/questions/2933221/can-you-get-a-funct-or-similar-from-a-methodinfo-object

			// The instance is provided as object because we can't pass it in statically typed.
			var paramInstance = Expression.Parameter(typeof(object), "instance");

			var param0 = Expression.Parameter(typeof(TParam0), method.GetParameters()[0].Name);
			var param1 = Expression.Parameter(typeof(TParam1), method.GetParameters()[1].Name);

			// Generate an expression like:
			// (object instance, StreamingContext context) => { ((RealInstanceType) instance).TheMethod(param0, param1); }
			var callExpr = Expression.Call(Expression.Convert(paramInstance, method.DeclaringType), method, param0, param1);
			var expression = Expression.Lambda<Action<object, TParam0, TParam1>>(callExpr, paramInstance, param0, param1);

			return expression.Compile();
		}

		/// <summary>
		/// Generates a delegate that can call an instance method where the instance is provided as <see cref="object"/>.
		/// </summary>
		/// <param name="method"></param>
		/// <returns></returns>
		private Action<object, TParam0, TParam1, TParam2> GetFastInstanceActionInvoker<TParam0, TParam1, TParam2>(MethodInfo method)
		{
			// https://stackoverflow.com/questions/2933221/can-you-get-a-funct-or-similar-from-a-methodinfo-object

			// The instance is provided as object because we can't pass it in statically typed.
			var paramInstance = Expression.Parameter(typeof(object), "instance");

			var param0 = Expression.Parameter(typeof(TParam0), method.GetParameters()[0].Name);
			var param1 = Expression.Parameter(typeof(TParam1), method.GetParameters()[1].Name);
			var param2 = Expression.Parameter(typeof(TParam2), method.GetParameters()[2].Name);

			// Generate an expression like:
			// (object instance, StreamingContext context) => { ((RealInstanceType) instance).TheMethod(param0, param1, param2); }
			var callExpr = Expression.Call(Expression.Convert(paramInstance, method.DeclaringType), method, param0, param1, param2);
			var expression = Expression.Lambda<Action<object, TParam0, TParam1, TParam2>>(callExpr, paramInstance, param0, param1, param2);

			return expression.Compile();
		}

		/// <summary>
		/// Generates a delegate that can call an instance method where the instance is provided as <see cref="object"/>.
		/// </summary>
		/// <param name="method"></param>
		/// <returns></returns>
		private Func<object, TParam0, TResult> GetFastInstanceFuncInvoker<TParam0, TResult>(MethodInfo method)
		{
			// https://stackoverflow.com/questions/2933221/can-you-get-a-funct-or-similar-from-a-methodinfo-object

			// The instance is provided as object because we can't pass it in statically typed.
			var paramInstance = Expression.Parameter(typeof(object), "instance");

			var param0 = Expression.Parameter(typeof(TParam0), method.GetParameters()[0].Name);

			// Generate an expression like:
			// (object instance, TParam1 param1) => { return ((RealInstanceType) instance).TheMethod(param0); }
			var callExpr = Expression.Call(Expression.Convert(paramInstance, method.DeclaringType), method, param0);
			var expression = Expression.Lambda<Func<object, TParam0, TResult>>(callExpr, paramInstance, param0);

			return expression.Compile();
		}

		/// <summary>
		/// Generates a delegate that can call an instance method where the instance is provided as <see cref="object"/>.
		/// </summary>
		/// <param name="method"></param>
		/// <returns></returns>
		private Func<object, TParam0, TParam1, TResult> GetFastInstanceFuncInvoker<TParam0, TParam1, TResult>(MethodInfo method)
		{
			// https://stackoverflow.com/questions/2933221/can-you-get-a-funct-or-similar-from-a-methodinfo-object

			// The instance is provided as object because we can't pass it in statically typed.
			var paramInstance = Expression.Parameter(typeof(object), "instance");

			var param0 = Expression.Parameter(typeof(TParam0), method.GetParameters()[0].Name);
			var param1 = Expression.Parameter(typeof(TParam1), method.GetParameters()[1].Name);

			// Generate an expression like:
			// (object instance, TParam1 param1) => { return ((RealInstanceType) instance).TheMethod(param0, param1); }
			var callExpr = Expression.Call(Expression.Convert(paramInstance, method.DeclaringType), method, param0, param1);
			var expression = Expression.Lambda<Func<object, TParam0, TParam1, TResult>>(callExpr, paramInstance, param0, param1);

			return expression.Compile();
		}

		/// <summary>
		/// Generates a delegate that can call an instance method where the instance is provided as <see cref="object"/>.
		/// </summary>
		/// <param name="method"></param>
		/// <returns></returns>
		private Func<object, TParam0, TParam1, TParam2, TResult> GetFastInstanceFuncInvoker<TParam0, TParam1, TParam2, TResult>(MethodInfo method)
		{
			// https://stackoverflow.com/questions/2933221/can-you-get-a-funct-or-similar-from-a-methodinfo-object

			// The instance is provided as object because we can't pass it in statically typed.
			var paramInstance = Expression.Parameter(typeof(object), "instance");

			var param0 = Expression.Parameter(typeof(TParam0), method.GetParameters()[0].Name);
			var param1 = Expression.Parameter(typeof(TParam1), method.GetParameters()[1].Name);
			var param2 = Expression.Parameter(typeof(TParam2), method.GetParameters()[2].Name);

			// Generate an expression like:
			// (object instance, TParam1 param1) => { return ((RealInstanceType) instance).TheMethod(param0, param1, param2); }
			var callExpr = Expression.Call(Expression.Convert(paramInstance, method.DeclaringType), method, param0, param1, param2);
			var expression = Expression.Lambda<Func<object, TParam0, TParam1, TParam2, TResult>>(callExpr, paramInstance, param0, param1, param2);

			return expression.Compile();
		}
		#endregion Reflection magic

		/// <inheritdoc />
		public override string ToString()
		{
			return $"TypeSpec: {SerializedName}, {Members.Count} members";
		}

		#region Serialization Inplementation

		/// <summary>
		/// Returns a new instance of <see cref="Type"/>, created either by the parameterless constructor or by <see cref="FormatterServices"/>.
		/// <see cref="OnDeserializing"/> will be called if applicable.
		/// </summary>
		/// <param name="context"></param>
		/// <returns></returns>
		public virtual object Create(StreamingContext context)
		{
			object instance = Constructor?.Invoke(null) ?? FormatterServices.GetUninitializedObject(Type);

			OnDeserializing?.Invoke(instance, context);

			return instance;
		}


		/// <summary>
		/// Delegate that can be called to override Deserialization process.
		/// </summary>
		/// <param name="serializer"></param>
		/// <param name="element">The <see cref="XElement"/> to deserialize from</param>
		/// <param name="attributes">The attributes of <paramref name="element"/></param>
		/// <param name="instance">When reference identity is required to be preserved an object (as created by <see cref="TypeSerializationSpecification.Create"/>) will be passed here.</param>
		/// <returns></returns>
		public delegate object DeserializeOverrideDelegate
			(Serializer serializer, XElement element, Dictionary<string, string> attributes, object instance = null);

		/// <summary>
		/// Override the deserialization process completely.
		/// Responsible for calling <see cref="TypeSerializationSpecification.OnDeserializing"/> and <see cref="TypeSerializationSpecification.OnMembersDeserialized"/> if applicable.
		/// </summary>
		public DeserializeOverrideDelegate OverrideDeserialize;


		/// <summary>
		/// Delegate that can be called to modify or override the serialization process.
		/// </summary>
		/// <param name="serializer"></param>
		/// <param name="element">The <see cref="XElement"/> to serialize into.</param>
		/// <param name="instance">The instance of <see cref="Type"/> to serialize.</param>
		public delegate void SerializeOverrideDelegate(Serializer serializer, XElement element, object instance);

		/// <summary>
		/// Override the serialization process completely.
		/// Responsible for calling
		/// <see cref="TypeSerializationSpecification.OnSerializing"/> and <see cref="TypeSerializationSpecification.OnSerialized"/> if applicable.
		/// </summary>
		public SerializeOverrideDelegate OverrideSerialize;


		#region Serialization Augment

		// todo?: is it needed to have both cases (before and after members are done automatically)?

		/// <summary>
		/// <see cref="OnMembersDeserialized"/>.
		/// </summary>
		/// <param name="instance">The instance being deserialized</param>
		/// <param name="serializer">The serializer</param>
		/// <param name="element">The <see cref="XElement"/> containing data for <paramref name="instance"/>.</param>
		/// <param name="attributes">The attributes of <paramref name="element"/>.</param>
		public delegate void DeserializeAugmentDelegate(object instance, Serializer serializer, XElement element, Dictionary<string, string> attributes);
		/// <summary>
		/// Will be called after members have been deserialized.
		/// This is called just before <see cref="OnDeserialized"/> and has a signature that allows reading the serialization stream.
		/// It is also possible to call on the <see cref="Serializer"/> to do additional work.
		/// </summary>
		public DeserializeAugmentDelegate OnMembersDeserialized;

		/// <summary>
		/// <see cref="SerializeAugmentDelegate"/>.
		/// </summary>
		/// <param name="instance">The instance to be serialized</param>
		/// <param name="serializer">The serializer</param>
		/// <param name="element">The element that will contain the data for <paramref name="instance"/></param>
		public delegate void SerializeAugmentDelegate(object instance, Serializer serializer, XElement element);
		/// <summary>
		/// Will be called before members are serialized.
		/// This is called just after <see cref="OnSerializing"/> and has a signature that allows modifying the serialization stream.
		/// It is also possible to call on the <see cref="Serializer"/> to do additional work.
		/// </summary>
		public SerializeAugmentDelegate BeforeMembersSerialized;



		#endregion Serialization Augment

		#endregion Serialization Inplementation
	}
}
