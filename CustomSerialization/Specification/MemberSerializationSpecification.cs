﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq.Expressions;
using System.Reflection;

using CustomSerialization.Attributes;
using CustomSerialization.MemberInfoExt;

namespace CustomSerialization.Specification
{
	public class MemberSerializationSpecification
	{
		/// <summary>
		/// The specification of the <see cref="Type"/> that holds this Member.
		/// </summary>
		public readonly TypeSerializationSpecification ParentSpecification;
		public readonly ValueMemberInfo Member;

		/// <summary>
		/// The <see cref="Type"/> of the value of the <see cref="Member"/>.
		/// </summary>
		public Type ValueType => Member.MemberValueType;

		/// <summary>
		/// The name as it will appear in the serialized stream.
		/// </summary>
		public string SerializedName;

		public string RealName => Member.Name;

		/// <summary>
		/// Serialize this member by reference: it will be replace by a reference and serialized in a global list.
		/// </summary>
		public bool ByReference;

		/// <summary>
		/// Serialize this member into an attribute rather than an element.
		/// </summary>
		public readonly bool AsAttribute;

		/// <summary>
		/// The value of <see cref="AsAttribute"/> was automatically assigned.
		/// </summary>
		public readonly bool AsAttributeAutomaticallyAssigned;

		private bool _optional;

		/// <summary>
		/// If optional it is allowed to not find a value for this member in the serialized data, a default value will be loaded instead. See: <see cref="DefaultValue"/>.
		/// When set to <see langword="false"/> <see cref="OmitIfDefault"/> is set to  <see langword="false"/> and <see cref="DefaultValue"/> is removed.
		/// </summary>
		public bool Optional
		{
			get => _optional;
			set
			{
				_optional = value;
				if (! _optional)
				{
					_defaultValue = null;
					_omitIfDefault = false;
				}
			}
		}

		private object _defaultValue;

		/// <summary>
		/// A default value to use when the member is <see cref="Optional"/>.
		/// When any assignment is made to this property, <see cref="Optional"/> will be set to <see langword="true"/>.
		/// </summary>
		public object DefaultValue
		{
			get => _defaultValue;
			set
			{
				_defaultValue = value;
				Optional = true;
			}
		}

		private bool _omitIfDefault;

		/// <summary>
		/// Do not serialize the value if it is equal to the <see cref="DefaultValue"/>. When set to <see langword="true"/> <see cref="Optional"/> is also set to <see langword="true"/>.
		/// </summary>
		public bool OmitIfDefault
		{
			get => _omitIfDefault;
			set
			{
				_omitIfDefault = value;
				if (_omitIfDefault)
				{
					Optional = true;
				}
			}
		}

		/// <summary>
		/// Number used to sort the members into a consistent order.
		/// </summary>
		public int Order;


		public MemberSerializationSpecification(
			  TypeSerializationSpecification parentSpecification
			, MemberInfo member
			, SerializerIncludeAttribute attr)
			: this(
			     parentSpecification
			   , member
			   , byReference: attr.ByReference
			   , name: attr.NameOverride
			   , asAttribute: attr.XmlAsAttribute
			   , optional: attr.Optional
			   , defaultValue: attr.DefaultValue
			   , omitIfDefault: attr.OmitIfDefault
			   , order: attr.Order)
		{ }

		public MemberSerializationSpecification(
			  TypeSerializationSpecification parentSpecification
			, MemberInfo member
			, bool byReference = false
			, string name = null
			, bool asAttribute = false
			, bool optional = false
			, object defaultValue = null
			, bool omitIfDefault = false
			, int order = 0)
		{
			ParentSpecification = parentSpecification ?? throw new ArgumentNullException(nameof(parentSpecification));
			member              = member              ?? throw new ArgumentNullException(nameof(member));
			if (member is FieldInfo fieldInfo)
			{
				Member = new ValueFieldInfo(fieldInfo);
			}
			else if (member is PropertyInfo propertyInfo)
			{
				Member = new ValuePropertyInfo(propertyInfo);
			}
			else
			{
				throw new ArgumentException
					($"Expected either {nameof(FieldInfo)} or {nameof(PropertyInfo)}.", nameof(member));
			}

			if (asAttribute)
			{
				if (Member.MemberValueType != typeof(string) && ! ValueType.IsValueType)
				{
					throw new InvalidOperationException($"Only ValueTypes (structs) can be serialized as an Attribute");
				}

				AsAttribute = true;
			}
			else
			{
				// When not specified assume that primitive types (int, float, bool, etc.) are serialized into attributes.
				AsAttribute = ValueType.IsPrimitive && ValueType.IsValueType;
				AsAttributeAutomaticallyAssigned = true;
			}

			Optional = optional;

			if(defaultValue != null)
			{
				// Because there is no type check on defaultValue
				// the compiler will allow entering an int default value for a uint field.
				// We should catch that ASAP and not during serialization.
				DefaultValue = Convert.ChangeType(defaultValue, ValueType);
			}

			OmitIfDefault = omitIfDefault;

			if (ValueType.IsValueType && DefaultValue == null)
			{
				// ValueTypes can't be null
				// Note: the setter for a field or property will convert to a non-boxed value for us.
				// However for the OmitIfDefault functionality we need to do a comparison check.
				// Thus the default value needs to actually exist.
				DefaultValue = GetDefaultValue(Member.MemberValueType);
			}

			Order = order;

			SerializedName = name ?? SpecGenerator.SafeMemberName(Member.Name);
			ByReference = byReference;
		}

		[DebuggerStepThrough]
		public object GetValue(object instance)
		{
			return Member.GetValue(instance);
		}

		[DebuggerStepThrough]
		public void SetValue(object instance, object value)
		{
			Member.SetValue(instance, value);
		}

		/// <inheritdoc />
		public override string ToString()
		{
			return $"MemberSpec: {SerializedName} [{(ByReference ? "Ref " : "")}{ParentSpecification.SerializedName}]";
		}


		private static readonly Dictionary<Type, Func<object>> defaultValueTypeRetriever =
			new Dictionary<Type, Func<object>>();

		/// <summary>
		/// Retrieve the <see langword="default"/> value for the given <paramref name="type"/>.
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		private static object GetDefaultValue(Type type)
		{
			if (defaultValueTypeRetriever.TryGetValue(type, out var func))
			{
				return func.Invoke();
			}

			// We want an Func<object> which returns the default.
			Expression<Func<object>> e = Expression.Lambda<Func<object>>(
			 Expression.Convert(Expression.Default(type), typeof(object)));

			// Compile and return the value.
			func = e.Compile();
			defaultValueTypeRetriever[type] = func;

			return func.Invoke();
		}


		public static readonly MemberSerializationSpecificationComparer OrderComparer = new MemberSerializationSpecificationComparer();

		public class MemberSerializationSpecificationComparer : IEqualityComparer<MemberSerializationSpecification>, IComparer<MemberSerializationSpecification>
		{
			public bool Equals(MemberSerializationSpecification x, MemberSerializationSpecification y)
			{
				if (ReferenceEquals(x, y)) return true;
				if (ReferenceEquals(x, null)) return false;
				if (ReferenceEquals(y, null)) return false;
				if (x.GetType() != y.GetType()) return false;
				return x.Order  == y.Order;
			}

			public int GetHashCode(MemberSerializationSpecification obj)
			{
				return obj.Order;
			}

			public int Compare(MemberSerializationSpecification x, MemberSerializationSpecification y)
			{
				if (ReferenceEquals(x,    y)) return 0;
				if (ReferenceEquals(null, y)) return 1;
				if (ReferenceEquals(null, x)) return -1;
				return x.Order.CompareTo(y.Order);
			}
		}
	}
}
