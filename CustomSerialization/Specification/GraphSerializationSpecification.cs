﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;

using CustomSerialization.Exceptions;

namespace CustomSerialization.Specification
{
	/// <summary>
	/// Holds the information required to serialize the object graph represented by <see cref="Root"/>.
	/// </summary>
	public class GraphSerializationSpecification
	{
		/// <summary>
		/// The <see cref="Type"/> that this <see cref="GraphSerializationSpecification"/> is for.
		/// </summary>
		public readonly Type Root;

		public TypeSerializationSpecification RootSpec => TypeSpecifications[Root];

		public readonly Dictionary<Type, TypeSerializationSpecification> TypeSpecifications;

		/// <summary>
		/// Map from (Expected_Type + Name) -> Real_Type
		/// </summary>
		private readonly DictionaryOfContainer<Type, Dictionary<string, Type>> typeNameMap = new DictionaryOfContainer<Type, Dictionary<string, Type>>();

		/// <summary>
		/// <see cref="Type"/>s that will be serialized by reference.
		/// </summary>
		public readonly HashSet<Type> ByRefTypes = new HashSet<Type>();

		/// <summary>
		/// Do not include the serializer's header.
		/// </summary>
		public bool NoHeader = false;

		public GraphSerializationSpecification(Type root, Dictionary<Type, TypeSerializationSpecification> typeSpecifications)
		{
			Root = root ?? throw new ArgumentNullException(nameof(root));
			TypeSpecifications = typeSpecifications ?? throw new ArgumentNullException(nameof(typeSpecifications));

			MakeReady();
		}

		/// <summary>
		/// Add a <see cref="TypeSerializationSpecification"/> to <see cref="TypeSpecifications"/>. This may be required if the <see cref="Root"/>
		/// has members typed as interfaces or abstract classes. The concrete implementations of those are not added automatically.
		/// </summary>
		/// <param name="t"></param>
		/// <param name="settings"></param>
		/// <returns></returns>
		public bool AddType(Type t, SpecGenerator.SpecGeneratorSettings settings = null)
		{
			if (TypeSpecifications.ContainsKey(t)) return false;

			var tempGraph = SpecGenerator.CreateGraphSerializationSpecification(t, settings);

			foreach (var kvp in tempGraph.TypeSpecifications)
			{
				var type = kvp.Key;
				var spec = kvp.Value;

				if (TypeSpecifications.ContainsKey(type)) continue;

				TypeSpecifications.Add(type, spec);
			}

			return true;
		}



		public TypeSerializationSpecification GetSpec(Type t)
		{
			if (t.IsGenericType)
			{
				// todo: Replace the CollectionSerializer crap with a TypeSerializationSpecification that can deal with generic collections.
			}

			return TypeSpecifications[t];
		}

		public TypeSerializationSpecification GetSpec(Type expectedType, string serializedName)
		{
			return GetSpec(GetTypeFromName(expectedType, serializedName));
		}

		public Type GetTypeFromName(Type expectedType, string serializedName)
		{
			return typeNameMap[expectedType][serializedName];
		}

		/// <summary>
		/// Perform some bookKeeping steps to make the <see cref="GraphSerializationSpecification"/> ready to be used.
		/// </summary>
		public void MakeReady()
		{
			DiscoverByReferenceTypes();
			SetupTypeNameMap();
			SortMembers();
		}

		/// <summary>
		/// Populate the list of types that will be serialized by reference.
		/// </summary>
		public void DiscoverByReferenceTypes()
		{
			ByRefTypes.Clear();
			foreach (var spec in TypeSpecifications.Values)
			{
				if (spec.ByRef)
				{
					ByRefTypes.Add(spec.Type);
				}

				foreach (var memberSpec in spec.Members)
				{
					if (memberSpec.ByReference)
					{
						ByRefTypes.Add(memberSpec.ValueType);
					}
				}
			}
		}

		/// <summary>
		/// Populate the Mapping from TypeNames to <see cref="Type"/>s.
		/// </summary>
		public void SetupTypeNameMap()
		{
			typeNameMap.Clear();

			// Discover inheritance and interface implementations.
			// This is needed because the SerializedName is not guaranteed to be unique across the entire graph.
			// Thus we need to know, for each type as expected by the specification what real types we can actually deserialize there.
			foreach (var specA in TypeSpecifications.Values)
			{
				var typeA = specA.Type;

				foreach (var specB in TypeSpecifications.Values)
				{
					var typeB = specB.Type;
					if(typeA == typeB) continue;

					if (typeA.IsAssignableFrom(typeB))
					{
						// TypeB can be assigned to a variable of TypeA
						// This means that TypeB is derives from TypeA (directly or indirectly)
						// Or when TypeA is an interface TypeB implements that interface.

						specA.knownDerivatives.Add(typeB);
					}
				}
			}


			// Assign the names
			foreach (var spec in TypeSpecifications.Values)
			{
				var rootType = spec.Type;

				var rootSpec = TypeSpecifications[rootType];

				typeNameMap[rootType].Add(spec.SerializedName, rootType);

				foreach (Type derivative in spec.knownDerivatives)
				{
					var derivedSpec = TypeSpecifications[derivative];

					var map = typeNameMap[rootType];

					if (map.TryGetValue(derivedSpec.SerializedName, out var existingType))
					{
						if (existingType == derivative)
						{
							// This is ok
							// todo: is it an error that this can happen?
						}
						else
						{
							throw new SerializerException
								(
								 $"Unable to create typeNameMap for '{rootType.FullName}': the key '{derivedSpec.SerializedName}'"
							   + $" is already in use by '{existingType.FullName}' but another type '{derivative.FullName}' also registers that SerializedName."
								);
						}
					}

					typeNameMap[rootType].Add(derivedSpec.SerializedName, derivative);
				}
			}
		}

		public void SortMembers()
		{
			foreach (var spec in TypeSpecifications.Values)
			{
				spec.Members.Sort(MemberSerializationSpecification.OrderComparer);
			}
		}
	}
}
