﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Text;

namespace CustomSerialization.Attributes
{
	/// <summary>
	/// Signal to the Serializer that the serialization specification of this <see cref="Type"/>
	/// should be made by only looking at members decorated with <see cref="SerializerIncludeAttribute"/>.
	/// </summary>
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct, AllowMultiple = false, Inherited = true)]
	public class SerializerExplicitAttribute : SerializationAttribute
	{
		public readonly string XmlTypeName;

		/// <summary>
		/// Always serialize this type as a reference and store it's real value in a centralized list.
		/// </summary>
		public readonly bool ByReference;

		public SerializerExplicitAttribute(string xmlTypeName = null, bool byReference = false)
		{
			XmlTypeName = xmlTypeName;
			ByReference = byReference;
		}
	}
}
