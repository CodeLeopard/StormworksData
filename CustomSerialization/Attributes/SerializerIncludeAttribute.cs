﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace CustomSerialization.Attributes
{
	[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property | AttributeTargets.Constructor, AllowMultiple = false, Inherited = true)]
	public class SerializerIncludeAttribute : SerializationAttribute
	{
		/// <summary>
		/// The name of the member to use during serialization.
		/// </summary>
		public readonly string NameOverride;
		/// <summary>
		/// Serialize this member by reference: it's values will be stored elsewhere, either in another member (of another type) or in a centralized container.
		/// </summary>
		public readonly bool ByReference;
		/// <summary>
		/// Serialize this member as a Xml Attribute
		/// </summary>
		public readonly bool XmlAsAttribute;
		/// <summary>
		/// If this member is missing assign <see cref="DefaultValue"/> or <see langword="default"/> instead of throwing an <see cref="Exception"/>.
		/// </summary>
		public readonly bool Optional;
		/// <summary>
		/// The default value to assign when the member is missing.
		/// Note that assigning a value other than <see langword="null"/> will set <see cref="Optional"/> to <see langword="true"/>.
		/// </summary>
		public readonly object DefaultValue;

		/// <summary>
		/// Do not serialize the value if it is equal to <see cref="DefaultValue"/>. Implies <see cref="Optional"/>.
		/// </summary>
		public readonly bool OmitIfDefault;

		public readonly int Order;

		public SerializerIncludeAttribute(
			string nameOverride = null,
			bool byReference = false,
			bool xmlAsAttribute = false,
			bool optional = false,
			object defaultValue = null,
			bool omitIfDefault = false,
			[CallerLineNumber]int order = 0)
		{
			NameOverride = nameOverride;
			ByReference = byReference;
			XmlAsAttribute = xmlAsAttribute;
			Optional = optional || null != defaultValue || omitIfDefault;
			DefaultValue = defaultValue;
			OmitIfDefault = omitIfDefault;
			Order = order;
		}
	}
}
